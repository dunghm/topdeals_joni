﻿<?php
	require_once(dirname(__FILE__) . '/app.php');
	if ( $login_user ) {
		redirect(WEB_ROOT . '/account/refer.php');
	} 
  
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>TopDeal</title>

<link href="static/css/template.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="/static/icon/favicon.ico" />
<script type="text/javascript" src="static/js/jquery.js"></script>
<script type="text/javascript" src="static/js/jquery.countdown.js"></script>
<script type="text/javascript">
$(function () {
	var austDay = new Date('2011,11,22');
	$('#clock').countdown({until: austDay});
});
</script>


<script type="text/javascript">
$(document).ready(function(){

	$(".btn-slide").click(function(){
		$("#panel").slideToggle("slow");
		$(this).toggleClass("active"); return false;
	});
	
	 
});
</script>

<style type="text/css">

a:focus {
	outline: none;
}
#panel {
	background: #FFFFFF;
	height: 200px;
	width:234px;
	display: none;
	height:70px;
	float:right;
	margin-top:35px;
	margin-right:1px;
	padding-bottom:31px;
	border:1px solid #7F2452;
	
}
.slide {
	margin: 0;
	padding: 0;
	background: url(images/btn-slide.gif) no-repeat center top;
	
}
.btn-slide {
	

	width: 239px;
	height: 42px;
	float:right;
	background:url(static/imges/btn.png) no-repeat;
	text-indent:-9999;
	margin-left:962px;
	position:absolute;

	
	
	
}

</style>
</head>
<body>
<div id="wrapper">
	<div id="top_bar">
	  <p id="language"><span>FR </span>/ EN </p>
		</div><!--eof_top_bar-->
		
				<div id="panel">
				        <form id="login-user-form" action="account/login.php" method="post">
						
						<input class="username" type="text"  value="E-mail" name="email" onClick="this.value=''"/>
						<input class="password" type="text"  value="Mot de passe" name="password" onClick="this.value='';this.type='password';" onfocus="this.value='';this.type='password';"/>
						
						
						<input  class="logon" type="button" onClick="submit();"/>
						</form>
				</div>

				
<p class="slide"><a href="#" class="btn-slide"></a></p>
		
		<div id="banner">
		


		
		
		</div><!--eof_banner-->
		<div id="main_body">
			<div id="topdeal_left">
				<img id="c_header" src="static/imges/clock_header.png" width="594" height="31" alt="topdel_clock" />
				<div id="clock">
				</div>
					
				<!--eof_clock-->
					
				<div id="news_letter">
				<div class="n_top"></div>
				<div class="n_body">
				 
				<div class="m1">  
				<p id="m1">Enregistrez-vous dès à présent et bénéficier de crédits à utiliser sur TopDeal.ch dès l’ouverture du site!</p>
				</div><!--eof_m1-->
				<div id="message_box">
				
						<form id="message" action="/account/signup.php" method="post">
						<input class="email" type="text"  value="E-mail" name="email" onClick="this.value=''" />
						<input class="email" type="text"  value="Mot de passe" name="password" onClick="this.value='';this.type='password';" onfocus="this.value='';this.type='password';" />
						<input class="email" type="text"  value="Confirmation du mot de passe" name="password2" onClick="this.value='';this.type='password';" onfocus="this.value='';this.type='password';" />
						<input  class="submit" type="submit" value=""/>
						</form>

					</div><!--eof_message_box-->
					
					<div id="message_box1">
					
					<div class="nm_top"></div>
					<div class="nm_body">
					<p id="nm">
					Enregistrez-vous maintenant et accédez à votre lien personnel que vous pourrez partager à vos amis afin de gagner des crédits TopDeal à utiliser dès l'ouverture le 22 novembre 2011.
					<br>
					Plus vous ramenez d'amis et plus vous gagnez des crédits. Les récompenses se présentent comme suite:
					<br>
					5 amis = CHF 10.-
					<br>
					10 amis = CHF 20.-
					<br>
					20 amis = CHF 30.-
					</p>
					
					</div><!--eof_nm_body-->
					<div class="nm_bottom"></div>
					
					
					
					</div><!--eof_message_box1-->
					
					<div id="subscribe">	
						<div id="subscribe_header"><p id="s_txt">Pour être informé du lancement du site, saisissez votre adresse e-mail : 
</p></div>
						<form id="subscribe" action="/subscribe.php" method="post">
						<input class="email1" type="text"  value="E-mail" name="email" onClick="this.value=''" />
						<input type="hidden" name="city_id" value="{$city['id']}" />
						<input  class="submit1" type="submit" value="" />
						</form>
						`</div><!--eof_subscribe-->
					
					
					
					</div><!--eof_n_body-->
				<div class="n_bottom"></div>				
					</div>
				</div><!--eof_left-->
				
				
				
			<div id="topdeal_right">
			  <div class="r_top"></div>
					<div class="r_body">
					<p id="r_message">Un voyage découverte? Une saut en parachute? Un abonnement dans un centre de bien-être? Des cadeaux pour vos enfants et votre famille avec les fêtes qui se profilent à l'horizon?  <br>
Nous-y voilà! TopDeal.ch vous proposera dès le 22 novembre des offres quotidiennes dans toute la Suisse Romande afin de vous faire découvrir de nouveaux services et produits de grande qualité à des prix exceptionnels! En effet nous sélectionnons les meilleurs offres auprès de nos partenaires et les proposons avec un rabais d'au moins 50%! De plus, chaque semaine, une offre "spéciale" et "limitée" sera publiée. <br>
Vous aurez également la possibilité de contribuer à l'apport d'idées pour les offres futures et serez récompensé pour votre contribution, rendez-vous le 22 novembre pour en savoir plus!  
</p>
				<img id="r_image" src="static/imges/img_td1.png" width="196" height="130" alt="top_deal photo" />
				<img id="r_image" src="static/imges/img_td2.png" width="196" height="130" alt="top_deal photo" />
					</div>
			           <div class="r_bottom"></div>	
			    </div><!--eof_topdeal_right-->
		
			</div><!--eof_main_body-->
			
			<div id="footer">
			<div id="footer_container">
			    <img id="f_image" src="static/imges/address.png" width="217" height="148" alt="top deal footer" />
				
				<div id="fmnu1">
				<ul id="footer_menu">
				<h3>TOP DEAL</h3>
				<li>Nous Contacter</li>
				<li>A propose de nous</li>
				<li>Presse</li>
				<li>Conditions generales</li>
				<li>Confidentialite</li>
				
				</ul>
				
			    <ul id="footer_menu">
				<h3>EN SAVIOR PLUS</h3>
				<li>Questions Et Responses</li>
				<li>Recommander a un ami</li>
				
				</ul>
				
				<ul id="footer_menu1">
				<h3>PARTENAIRES</h3>
				<li>Publier votre offre</li>
				<li>Suggerez un deal</li>
				
				</ul>
				</div>
				
				
				
				</div>
			
			    </div><!--eof_footer-->
		

	</div><!--eof_wrapper-->


</body>
</html>
