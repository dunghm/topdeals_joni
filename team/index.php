<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');
$daytime = strtotime(date('Y-m-d'));
$type = '';

$now = time();
$condition = array(
	'system' => 'Y',
);

$selector = strval($_GET['t']);
if ( $selector == 'w' )
    $type = 'weekly';
else if ( $selector == 'd' )
    $type = 'normal';


if ( $selector == 'archive' ){
  $date_from =  strtotime("24 november 2014");
   $condition[] ="end_time < {$now}";  
   $condition[] ="begin_time > {$date_from}";  
}else{
     $condition[] ="end_time > {$now}";  
      $condition[] ="begin_time <= {$now}";  
}
    

/*$condition = array( 
	'city_id' => array(0, abs(intval($city['id']))),
	"begin_time <= '{$daytime}'",
	"now_number < max_number",
);

	
if ( $type != '' )
    $condition['team_type'] = $type;
*/
if (!option_yes('displayfailure') && $selector != 'archive') {   // if Failed deals are not allowed to display then cut those
	$condition['OR'] = array(
		"now_number >= min_number",
		"end_time > '{$daytime}'",
	);
}

$group_id = abs(intval($_GET['gid']));
if ($group_id) $condition['group_id'] = $group_id;
$pg=1;
if ($_REQUEST['pg']){
	$pg=$_REQUEST['pg'];
}
$count = Table::Count('team', $condition);
//list($pagesize, $offset, $pagestring) = pagestring($count, $pg*8);
list($pagesize, $offset, $pagestring) = pagestring($count, 12);

$pagesize=12;
$offset=$pg*$pagesize-$pagesize;

$mode = $_REQUEST['mode'];
if($mode && $mode=='ajaxRequest')
{	
	$pagesize = 12;	
	$offset=$_REQUEST['offset'];
}

$orderby = 'ORDER BY begin_time DESC, sort_order DESC, id DESC';

//SET CURRENT DEAL ID FOR AVOID DATA DUPLICATION ON MOBILE SITE
if(!$mode)
{
	$_SESSION['currentDealIdOnAjaxRequest'] = $group_id;
}


$direction='down';
$d=$_REQUEST['d'];
if ($_REQUEST['d']){
	$direction=$_REQUEST['d'];
	if($direction=='up'){
		$direction=$icon='down';
		$order_phrase='DESC';
          	
	}
	else{
		$direction=$icon='up';
		$order_phrase='ASC';
	}
}

$s = $_REQUEST['s'];
if($_REQUEST['s']=='p') {
	$orderby = 'ORDER BY team_price '.$order_phrase;
  	$p_icon=$icon;
}
else if($_REQUEST['s']=='t') {
	$orderby = 'ORDER BY end_time '.$order_phrase; 
  	$t_icon=$icon;
}

if($selector != 'archive'){
    $sql_feature = "SELECT * from team "
        . "WHERE";
        if( $group_id ){
            $sql_feature.= " group_id = {$group_id} AND ";
        }
        $sql_feature.= " end_time > {$now} AND begin_time <= {$now} AND featured = 1 ORDER BY begin_time DESC";
       
    $feature_team = DB::GetQueryResult($sql_feature , true);
    if($feature_team){
        $condition[] = "id != {$feature_team['id']}";
    }
}
$teams = DB::LimitQuery('team', array(
	'condition' => $condition,
	'order' => $orderby,
	'size' => $pagesize,
	'offset' => $offset,
));
foreach($teams AS $id=>$one){
	team_state($one);
	if (!$one['close_time']) $one['picclass'] = 'isopen';
	if ($one['state']=='soldout') $one['picclass'] = 'soldout';
	$teams[$id] = $one;
}

//$category_count = DB::GetQueryResult("SELECT group_id, count( * ) sum
//FROM team WHERE end_time >  {$now} AND begin_time <= {$now}
//GROUP BY group_id" , false);
$category_count = DB::GetQueryResult("select category.id as group_id, count(title) sum from category left join team on category.id = group_id  and end_time >  {$now} AND begin_time <= {$now} 
where zone = 'group'
   
GROUP BY category.id" , false);

$category_count = Utility::AssColumn($category_count, "group_id");

$category = Table::Fetch('category', $group_id);
$pagetitle = 'TopDeal.ch - Deals en cours';

if($mode && $mode=='ajaxRequest')
{	

	$currentDealIdOnAjaxRequest	=	0;
	if(isset($_SESSION['currentDealIdOnAjaxRequest'])){
		$currentDealIdOnAjaxRequest = $_SESSION['currentDealIdOnAjaxRequest'];
	}

	if($currentDealIdOnAjaxRequest == $group_id){
		die (include template_sub('content/content_actuels_extended'));
	}else{
		die();
	}
	
}
else if($mode && $mode=='new')
{	
  die (include template_ex('content_actuels_new'));
}

include template_ex('content_actuels');

function current_teamcategory($gid='0') {
	global $city;
	$a = array(
			'/team/index.php' => 'Tous',
			);
	foreach(option_hotcategory('group') AS $id=>$name) {
		$a["/team/index.php?gid={$id}"] = $name;
	}
	$l = "/team/index.php?gid={$gid}";
	if (!$gid) $l = "/team/index.php";
	return current_link($l, $a, true);
}
