<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

need_login();

$id = abs(intval($_GET['id']));
if(isset($_POST['action']))
{
    $order_item_id = $_POST['order_item_id'];
   // $order_id = $_GET['order_id'];
    Table::Delete('order_item', $order_item_id);
    exit;
}
if ( !$id )
{ 
    // Create the Order from Basket
    $basket = new Basket($login_user_id);
    $basket_items = $basket->GetItems();
	
	//** Express/Pickup delivery Choice **//
	foreach($basket_items as $item)
    {
	   $team = Table::Fetch('team', $item['team_id']);
	   
	   $item_delivery = $team['delivery'];
	   if ($item_delivery == 'express_pickup' && $item['delivery'] != 'express' && $item['delivery'] != 'pickup')
	   {
			// gardbar hai
			redirect(WEB_ROOT.'/basket.php');			
	   }
    }
	/***/
    if ( count($basket_items) > 0 )
    {
        $order = array(
            "user_id" => $login_user_id,
            "gender" => $login_user['gender'],
            "create_time" => time(),
            "service" => "paypal",
        );
        
       $order_id = DB::Insert('order', $order);
       $order['origin'] = 0;
       $team_fare_once = array();
       $fare_charged = 0;
       foreach($basket_items as $item)
       {
           $team = Table::Fetch('team', $item['team_id']);
           $option = Table::Fetch('team_multi', $item['option_id']);
           $fare = 0;
           $price =  $option ? $option['team_price'] : $team['team_price'];
           
		   $item_delivery = $team['delivery'];
		   if ($item_delivery == 'express_pickup')
		   {
				$item_delivery = $item['delivery'];
				
		   }
		   
           if ( $item_delivery == 'express' && in_array($team['id'], $team_fare_once) == false)
           {
               if ( $team['fare'] > $fare_charged )
               {
                   // if the Fare is greator then fare charged so far
                   $fare = $team['fare'] - $fare_charged;  // Charge the remaining amount
                   $fare_charged += $fare;                 // Add to total Fare
               }
               //$fare =  $team['fare'];
               $team_fare_once[] = $team['id'];
           }
           
           $order_item = array(
               'order_id' => $order_id,
               'team_id' => $item['team_id'],
               'mode' => $item['mode'],
               'quantity' => $item['quantity'],
               'price' => $price,
               'fare' => $fare,
               'delivery' => $item_delivery,
               'total' => ($fare + ($item['quantity'] * $price)),
               'option_id' => $option ? $option['id']: NULL,
                
           );
           
          
           
           DB::Insert('order_item', $order_item);
           $order['origin'] += $order_item['total'];
           $order['quantity'] += $order_item['quantity'];
           $order['fare'] += $order_item['fare'];
         
       }
       //BASKET WILL CLEAR WHEN ORDER SUCCESS
       // $basket->ClearBasket();
       Table::UpdateCache("order", $order_id, 
               array("origin" => $order['origin'],
                   "quantity" => $order['quantity'],
                   "fare" => $order['fare']));

       //redirect(WEB_ROOT .'/team/checkout.php?id='.$order_id);
?>
	   <form method="get" action="<?php echo $INI['system']['secure_wwwprefix']; ?>/team/checkout.php" name='form1'>
	   <input type="hidden" name="id" value="<?php echo $order_id; ?>" />
           <?php 
            if(isset($_GET['name'])){
                ?>
           <input type="hidden" name="name" value="<?php echo $_GET['name']; ?>" />
           <?php 
            }
           ?>
           <?php 
            if(isset($_GET['last_name'])){
                ?>
           <input type="hidden" name="last_name" value="<?php echo $_GET['last_name']; ?>" />
           <?php 
            }
           ?>
	   </form>
	   <script type="text/javascript">
	   document.form1.submit();
	   </script>
<?php
	exit;
    }
    else
    {
        redirect($INI['system']['wwwprefix']);
    }
}

$order = Table::Fetch('order', $id);
if ($order['user_id'] != $login_user_id)
{
    redirect($INI['system']['wwwprefix']);
}

$order_items  = Table::Fetch('order_item', array($id), 'order_id');

function SortItemsByTeamAndFare($lhs, $rhs){
    if ( $lhs['team_id'] == $rhs['team_id']  ) {
        if ($lhs['fare'] == $lhs['fare'])
            return 0;
        else
            return ($lhs['fare'] < $rhs['fare'] ? -1 : 1);
    }   
    
    return ($lhs['team_id'] < $rhs['team_id'] ? -1 : 1);
}

usort($order_items,'SortItemsByTeamAndFare');

$charges = $order['origin'] - $login_user['money'];

if ( $charges <= 0 ) {
	$charges = 0;
	$credit = $order['origin'];
}
else if ( $charges <= $order['origin'] ) {
	$credit = $login_user['money'];
}

$team_ids = Utility::GetColumn($order_items, 'team_id');
$option_ids = Utility::GetColumn($order_items, 'option_id');

$teams = Table::Fetch('team', $team_ids);
$options = Table::Fetch('team_multi', $option_ids);


$parent_ids = Utility::GetColumn($options, 'parent_option_id');
$parent_options = Table::Fetch('team_multi', $parent_ids);

$addresses = Table::Fetch('user_address', array($login_user['id']), "user_id");
$cc_alias = Table::Fetch('user_cc_alias', $login_user_id, 'user_id');
$expiry_month = ((($cc_alias['expm']+1)-1)%12+1);
if ($cc_alias && mktime(0,0,0, $expiry_month, 1, $cc_alias['expy']+($cc_alias['expm']==12?1:0) ) < time() )
        $cc_alias = false;

include template_ex('content_checkout');

?>
