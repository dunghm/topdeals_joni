<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

$id = abs(intval($_GET['id']));
$op = abs(intval($_REQUEST['op']));

$team = Table::Fetch('team', $id);

$option = Table::Fetch('team_multi', $op);

if($option && count($option)>0){
        $multiOption = true;

        $team['team_price'] = $option['team_price'];
        $team['title'] = $option['title'];
        $team['title_fr'] = $option['title_fr'];
        $team['image'] = $option['image'];
        $team['option_id'] = $op;
        
        
}

	
if ( !$team || $team['begin_time']>time() ) {
		if($lang=="en")
			Session::Set('error', 'Buy item does not exist');
		elseif($lang =='fr')
			Session::Set('error', 'Le produit n\'existe pas');	
	
	redirect( WEB_ROOT . '/index.php' );
}
  
if ( ($team['max_number']>0 && $team['now_number'] >= $team['max_number']) ||
    ($option && $option['max_number']>0 && $option['now_number'] >= $option['max_number']) ) 
{
    if($lang=="en")
        Session::Set('error', 'your purchase of this deal has finished, please have a look at other products.');
    elseif($lang =='fr')
        Session::Set('error', 'Vous avez d&eacute;j&agrave; achet&eacute; ce deal et celui-ci est malheureusement limit&eacute; en quantit&eacute; par personne');
    
    redirect( WEB_ROOT . "/team.php?id={$id}"); 
}

//whether buy
$ex_con = array(
		'user_id' => $login_user_id,
		'team_id' => $team['id'],
		'state' => 'unpay',
		);
$order = DB::LimitQuery('order', array(
	'condition' => $ex_con,
	'one' => true,
));

//is this a buy once deal?
if (strtoupper($team['buyonce'])=='Y') {
	$ex_con['state'] = 'pay';
	if ( Table::Count('order', $ex_con) ) {
		if($lang=="en")
			Session::Set('error', 'your purchase of this deal has finished, please have a look at other products.');
		elseif($lang =='fr')
			Session::Set('error', 'Vous avez d&eacute;j&agrave; achet&eacute; ce deal et celui-ci est malheureusement limit&eacute; en quantit&eacute; par personne');
		redirect( WEB_ROOT . "/team.php?id={$id}"); 
	}
}

//is there a limit per user?
 $cond_peruser = array ('user_id' => $login_user_id,
                        'team_id' => $id,
                        'state' => 'pay',);
 
if ($team['per_number']>0) {
       
        
	$now_count = Table::Count('order', $cond_peruser, 'quantity');
	$team['per_number'] -= $now_count;
	if ($team['per_number']<=0) {
		if($lang=="en")
			Session::Set('error', 'your purchase of this deal has finished, please have a look at other products.');
		elseif($lang =='fr')
			Session::Set('error', 'Vous avez d&eacute;j&agrave; achet&eacute; ce deal et celui-ci est malheureusement limit&eacute; en quantit&eacute; par personne');
		redirect( WEB_ROOT . "/team.php?id={$id}"); 
	}      
}

// User is not violating the Limit Defined in Global Deal Settings
// Now check if there is a Limit Defined in the Selected Deal Option

if ( $multiOption && $option['per_number'] > 0 )
{
    $cond_peruser['option_id'] = $op;
    $option_now_count = Table::Count('order', $cond_peruser, 'quantity');
    $option['per_number'] -= $option_now_count;
    if ($option['per_number']<=0) {
        if($lang=="en")
            Session::Set('error', 'your purchase of this deal has finished, please have a look at other products.');
        elseif($lang =='fr')
            Session::Set('error', 'Vous avez d&eacute;j&agrave; achet&eacute; ce deal et celui-ci est malheureusement limit&eacute; en quantit&eacute; par personne');
        redirect( WEB_ROOT . "/team.php?id={$id}"); 
    }            
}

team_state($team);
/* noneed pay where goods soldout or end */
if ($team['close_time']) {
	Session::Set('notice', 'the deal is closed. You can not pay now');
	redirect(WEB_ROOT  . "/team.php?id={$order['team_id']}");
}
/* end */

//post buy
if ( $_POST ) {
	need_login();
	
        // 1. Find Unpaid orders for this Deal and Calculate the Quantity
        $current_time = time();
        
	$table = new Table('order', $_POST);
	$table->quantity = abs(intval($table->quantity));

	if ( $table->quantity == 0 ) {
		if($lang=="en")
			Session::Set('error', 'quantity should not be less than 1');
		elseif($lang =='fr')
			Session::Set('error', 'la quantit&eacute; choisie ne peut &acirc;tre en dessous de 1');			
		redirect( WEB_ROOT . "/team/buy.php?id={$team['id']}");
	} 
	elseif ( ($team['per_number']>0 && $table->quantity > $team['per_number']) ||
                ( $option && $option['per_number']>0 && $table->quantity > $option['per_number'])) {
		
		if($lang=="en")
			Session::Set('error', 'your purchase has gone beyond the limit');
		elseif($lang =='fr')
			Session::Set('error', 'vous avez d&eacute;pass&eacute; le nombre d\'achat ');			
		redirect( WEB_ROOT . "/team.php?id={$id}"); 
	}

	if ($order && $order['state']=='unpay') {
		$table->SetPk('id', $order['id']);
	}
	$upd_user = "Update user SET realname = '".$table->realname."', mobile='".$table->mobile."', address='".$table->address."', zipcode='".$table->zipcode."', region='".$table->region."', country='".$table->country."' WHERE id = '".$login_user_id."'";
	//die($upd_user);
	DB::Query($upd_user);
	
	$table->user_id = $login_user_id;
	$table->state = 'unpay';
	$table->team_id = $team['id'];
	$table->city_id = $team['city_id'];
	$table->express = ($team['delivery']=='express') ? 'Y' : 'N';
	$table->fare = $table->express=='Y' ? $team['fare'] : 0;
	$table->price = $team['team_price'];
	$table->credit = 0;
    $table->service = "paypal";

	//multi buy option override
	if($op){
            $table->option_id = $op;
	}
	
	if ( $table->id ) {
            $eorder = Table::Fetch('order', $table->id);
            if ($eorder['state']=='unpay' 
                            && $eorder['team_id'] == $id
                            && $eorder['user_id'] == $login_user_id
               ) {

                    // Calculate Total Order Cost.
                    $table->origin = team_origin($team, $table->quantity);
                    // Subtract 'Coupon' Value from Previous Order
                    $table->origin -= $eorder['card'];
            } else {
                    $eorder = null;
            }
	} 
	if (!$eorder){
		$table->SetPk('id', null);
		$table->create_time = $current_time;
		$table->origin = team_origin($team, $table->quantity);
	}

	$insert = array(
			'user_id', 'team_id', 'city_id', 'state', 
			'fare', 'express', 'origin', 'price', 'gender',
			'address', 'zipcode', 'realname', 
			'quantity', 'create_time', 'remark', 'condbuy',
            'country', 'mobile', 'service','region','option_id'
		);
	if ($flag = $table->update($insert)) 
        {
            $order_id = abs(intval($table->id));
           
            $is_available = ZTeam::CheckInventoryAvailability($team, $order_id, $option);

            if ( $is_available == false )
            {
                Table::Delete('order', $order_id);
                if($lang=="en")
                    Session::Set('error', 'Order system busy, please retry again in few minutes. Thanks!');
                elseif($lang =='fr')
                    Session::Set('error', 'Order system busy, please retry again in few minutes. Thanks!');			
                redirect( WEB_ROOT . "/team.php?id={$id}"); 
            }   
            
            
            $cid = strval($_POST['cid']);
            if  ( strlen($cid) > 0 )
            {
                $order = Table::Fetch('order', $order_id);
                 $ret = ZCard::UseCard($order,$cid);
                 if ( ! ($ret === true) )
                 {
                    Table::Delete('order', $order_id);
                    if($lang=="en")
                        Session::Set('error', ZCard::Explain($ret));
                    elseif($lang =='fr')
                        Session::Set('error', ZCard::Explain($ret));			
                    redirect( WEB_ROOT . "/team.php?id={$id}");  
                 }
            }
           
            
            redirect(WEB_ROOT."/order/check.php?id={$order_id}&paytype={$table->paytype}&op={$op}");
	}
}

//fix
$order['quantity'] = 1;


//each user per day per buy
if (!$order) { 
	$order = json_decode(Session::Get('loginpagepost'),true);
	settype($order, 'array');
	if ($order['mobile']) $login_user['mobile'] = $order['mobile'];
	if ($order['zipcode']) $login_user['zipcode'] = $order['zipcode'];
	if ($order['address']) $login_user['address'] = $order['address'];
	if ($order['realname']) $login_user['realname'] = $order['realname'];
	$order['quantity'] = 1;
}
//end;

$order['origin'] = team_origin($team, $order['quantity']);

if ($team['max_number']>0 && $team['conduser']=='N') {
	$left = $team['max_number'] - $team['now_number'];
	if ($team['per_number']>0) {
		$team['per_number'] = min($team['per_number'], $left);
	} else {
		$team['per_number'] = $left;
	}
}
$team['per_number'];
$tmp = array();
for($i=0; $i<$team['per_number']; $i++)
{
	$k = $i+1;
	$tmp[] = $k;
}

$countries = array();
GetCountries($countries);


include template('team_buy');
