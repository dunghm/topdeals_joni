<?php

/**
 * Adds the Customize page to the WordPress admin area
 */
function ht_customizer_menu() {
    add_theme_page( 'Customize', 'Customize', 'edit_theme_options', 'customize.php' );
}
add_action( 'admin_menu', 'ht_customizer_menu' );


/**
 * Adds the individual sections, settings, and controls to the theme customizer
 */
function ht_customizer( $wp_customize ) {
	
	/**
 	* Adds textarea support to the theme customizer
 	*/
	class ht_Customize_Textarea_Control extends WP_Customize_Control {
    public $type = 'textarea';
 
    public function render_content() {
        ?>

<label> <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
  <textarea rows="5" style="width:100%;" <?php $this->link(); ?>><?php echo esc_textarea( $this->value() ); ?></textarea>
</label>
<?php
    }
}
	
	/**
 	* Header Section
 	*/
	$wp_customize->add_section('ht_header', array(
		'title' => __( 'Header', 'framework' ),
		'description' => '',
		'priority' => 30,
	) );
	
	$wp_customize->add_setting( 'blogname', array(
		'default'    => get_option( 'blogname' ),
		'type'       => 'option',
		'capability' => 'manage_options',
	) );

	$wp_customize->add_control( 'blogname', array(
		'label'      => __( 'Site Title', 'framework' ),
		'section'    => 'ht_header',
	) );

	$wp_customize->add_setting( 'blogdescription', array(
		'default'    => get_option( 'blogdescription' ),
		'type'       => 'option',
		'capability' => 'manage_options',
	) );

	$wp_customize->add_control( 'blogdescription', array(
		'label'      => __( 'Tagline', 'framework' ),
		'section'    => 'ht_header',
	) );
	
	
	// Add logo to Site Title & Tagline Section
	$wp_customize->add_setting( 'ht_site_logo', array('default' => get_template_directory_uri() . '/images/logo.png') );
 
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'ht_site_logo', array(
		'label' => 'Site Logo',
		'section' => 'ht_header',
		'settings' => 'ht_site_logo')
	) );
	
	// Add avatar
	$wp_customize->add_setting( 'ht_site_avatar');
 
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'ht_site_avatar', array(
		'label' => 'Site Avatar (100px x 100px)',
		'section' => 'ht_header',
		'settings' => 'ht_site_avatar')
	) );
	
	// RSS Option
	$wp_customize->add_setting('ht_header_rss');

	$wp_customize->add_control('ht_header_rss',	array(
		'type' => 'checkbox',
		'label' => 'Hide RSS link',
		'section' => 'ht_header',
	) );
	
	// Email Option
	$wp_customize->add_setting('ht_header_email');

	$wp_customize->add_control('ht_header_email',	array(
		'type' => 'text',
		'label' => 'Email Link',
		'section' => 'ht_header',
	));
	
	// Twitter Option
	$wp_customize->add_setting('ht_header_twitter');

	$wp_customize->add_control('ht_header_twitter',	array(
		'type' => 'text',
		'label' => 'Twitter Link',
		'section' => 'ht_header',
	));
	
	// Facebook Option
	$wp_customize->add_setting('ht_header_facebook');

	$wp_customize->add_control('ht_header_facebook', array(
		'type' => 'text',
		'label' => 'Facebook Link',
		'section' => 'ht_header',
	));
	
	// Google+ Option
	$wp_customize->add_setting('ht_header_google');

	$wp_customize->add_control('ht_header_google',	array(
		'type' => 'text',
		'label' => 'Google+ Link',
		'section' => 'ht_header',
	));
	
	// Pinterest Option
	$wp_customize->add_setting('ht_header_pinterest');

	$wp_customize->add_control('ht_header_pinterest',	array(
		'type' => 'text',
		'label' => 'Pinterest Link',
		'section' => 'ht_header',
	));
	
	// LinkedIn Option
	$wp_customize->add_setting('ht_header_linkedin');

	$wp_customize->add_control('ht_header_linkedin',	array(
		'type' => 'text',
		'label' => 'LinkedIn Link',
		'section' => 'ht_header',
	));
	
	// Flickr Option
	$wp_customize->add_setting('ht_header_flickr');

	$wp_customize->add_control('ht_header_flickr',	array(
		'type' => 'text',
		'label' => 'Flickr Link',
		'section' => 'ht_header',
	));
	
	/**
 	* Footer Section
 	*/
    $wp_customize->add_section('ht_footer', array(
		'title' => 'Footer',
		'description' => '',
		'priority' => 35,
	) );
	
	// footer widget layout option
	$wp_customize->add_setting('ht_style_footerwidgets', array('default' => '3col') );
 
	$wp_customize->add_control('ht_style_footerwidgets', array(
			'type' => 'select',
			'label' => 'Footer Widget Layout',
			'section' => 'ht_footer',
			'choices' => array(
				'off' => 'Off',
				'2col' => 'Two Columns',
				'3col' => 'Three Columns',
				'4col' => 'Four Columns',
			),
	) );
	
	// site coypright
	$wp_customize->add_setting( 'ht_copyright', array(
		'default'        => '&copy; Copyright A Hero Theme.',
	) );
	 
	$wp_customize->add_control( new ht_Customize_Textarea_Control( $wp_customize, 'ht_copyright', array(
		'label'   => 'Site Copyright',
		'section' => 'ht_footer',
		'settings'   => 'ht_copyright',
	) ) );
	
	
	/**
 	* Styling Section
 	*/
    $wp_customize->add_section( 'ht_styling', array(
		'title' => 'Styling',
		'description' => 'Change the look of the theme.',
		'priority' => 40,
	) );	

	// theme color option
	$wp_customize->add_setting( 'ht_styling_themecolor', array('default' => '#dd5136') );
 
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'ht_styling_themecolor', array(
        'label'   => 'Theme Color',
        'section' => 'ht_styling',
        'settings'   => 'ht_styling_themecolor',)
	) );

	
	
	
	/* Custom Background */
	
	
        $wp_customize->add_section( 'background_image', array(
            'title'          => __( 'Background', 'framework' ),
            'theme_supports' => 'custom-background',
            'priority'       => 80,
        ) );
		
		// BG Color
		$wp_customize->add_setting( 'background_color', array('default' => get_theme_support( 'custom-background', 'default-color' ),'theme_supports' => 'custom-background',) );
		
		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'background_color', array(
		'label' => __( 'Background Color', 'framework' ),
		'section' => 'background_image',
		) ) );
		
		
		// BG Image
        $wp_customize->add_setting( 'background_image', array(
            'default'        => get_theme_support( 'custom-background', 'default-image' ),
            'theme_supports' => 'custom-background',
        ) );

        $wp_customize->add_setting( new WP_Customize_Background_Image_Setting( $wp_customize, 'background_image_thumb', array(
            'theme_supports' => 'custom-background',
        ) ) );

        $wp_customize->add_control( new WP_Customize_Background_Image_Control( $wp_customize ) );

	
}
add_action( 'customize_register', 'ht_customizer' );



