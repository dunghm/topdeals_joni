<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 * @link http://www.deluxeblogtips.com/meta-box/
 */

/********************* META BOX DEFINITIONS ***********************/

/**
 * Prefix of meta keys (optional)
 * Use underscore (_) at the beginning to make keys hidden
 * Alt.: You also can make prefix empty to disable it
 */
// Better has an underscore as last sign
$prefix = '_ht_';

global $meta_boxes;

$meta_boxes = array();

// Post Options
$meta_boxes[] = array(
	
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'post_meta',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => __( 'Post Options', 'framework' ),

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'post' ),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',

	// Auto save: true, false (default). Optional.
	'autosave' => true,

	'fields' => array(
			// SELECT BOX
			array(
			'name' => __( 'Sidebar Position', 'framework' ),
			'id' => "{$prefix}post_sidebar",
			'type' => 'select',
			// Array of 'value' => 'Label' pairs for select box
			'options' => array(
				'sidebar-left' => __( 'Sidebar Left', 'framework' ),
				'sidebar-right' => __( 'Sidebar Right', 'framework' ),
			),
			// Select multiple values, optional. Default is false.
			'multiple' => false,
			'std'	=> __( 'Sidebar Off', 'framework' ),
			),
			// CHECKBOX
			array(
			'name' => __( 'Show "View Post" on Timeline?', 'framework' ),
			'desc' => __( 'Check to show the "view post" link below the post on the blog timeline.', 'framework' ),
			'id' => "{$prefix}post_view_single",
			'type' => 'checkbox',
			// Value can be 0 or 1
			'std' => 1,
			),

	)
);

// Page Options
$meta_boxes[] = array(
	
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'page_meta',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => __( 'Page Options', 'framework' ),

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'page' ),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',

	// Auto save: true, false (default). Optional.
	'autosave' => true,

	'fields' => array(
			// SELECT BOX
			array(
			'name' => __( 'Sidebar Position', 'framework' ),
			'id' => "{$prefix}page_sidebar",
			'type' => 'select',
			// Array of 'value' => 'Label' pairs for select box
			'options' => array(
				'sidebar-left' => __( 'Sidebar Left', 'framework' ),
				'sidebar-right' => __( 'Sidebar Right', 'framework' ),
			),
			// Select multiple values, optional. Default is false.
			'multiple' => false,
			'std'	=> __( 'Sidebar Off', 'framework' ),
			),
	)
);

// Gallery Post Format
$meta_boxes[] = array(
	
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'pf_gallery',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => __( 'Gallery Post Format', 'framework' ),

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'post' ),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',

	// Auto save: true, false (default). Optional.
	'autosave' => true,

	'fields' => array(
		// PLUPLOAD IMAGE UPLOAD (WP 3.3+)
		array(
			'name'             => __( 'Gallery Image Upload', 'framework' ),
			'id'               => "{$prefix}pf_gallery_image",
			'type'             => 'plupload_image',
			'max_file_uploads' => 0,
		),
		
			// CHECKBOX
			array(
			'name' => __( 'Show Title on Timeline?', 'framework' ),
			'desc' => __( 'Check to show the post title on the blog timeline.', 'framework' ),
			'id' => "{$prefix}pf_gallery_post_title",
			'type' => 'checkbox',
			// Value can be 0 or 1
			'std' => 1,
			),
			// CHECKBOX
			array(
			'name' => __( 'Show Excerpt on Timeline?', 'framework' ),
			'desc' => __( 'Check to show the post excerpt on the blog timeline.', 'framework' ),
			'id' => "{$prefix}pf_gallery_post_excerpt",
			'type' => 'checkbox',
			// Value can be 0 or 1
			'std' => 1,
			),

	)
);


// Image Post Format
$meta_boxes[] = array(
	
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'pf_image',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => __( 'Image Post Format', 'framework' ),

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'post' ),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',

	// Auto save: true, false (default). Optional.
	'autosave' => true,

	'fields' => array(
		// OEMBED
			array(
			'name' => __( 'Image URL (oEmbed)', 'framework' ),
			'id' => "{$prefix}pf_image_oembed",
			'desc' => __( 'Enter the URL to your image. (Supported services: Instagram, Photobucket, SmugMug, Flickr)', 'framework' ),
			'type' => 'oembed',
		),
		
			// CHECKBOX
			array(
			'name' => __( 'Show Title on Timeline?', 'framework' ),
			'desc' => __( 'Check to show the post title on the blog timeline.', 'framework' ),
			'id' => "{$prefix}pf_image_post_title",
			'type' => 'checkbox',
			// Value can be 0 or 1
			'std' => 1,
			),
			// CHECKBOX
			array(
			'name' => __( 'Show Excerpt on Timeline?', 'framework' ),
			'desc' => __( 'Check to show the post excerpt on the blog timeline.', 'framework' ),
			'id' => "{$prefix}pf_image_post_excerpt",
			'type' => 'checkbox',
			// Value can be 0 or 1
			'std' => 1,
			),

	)
);

// Video Post Format
$meta_boxes[] = array(
	
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'pf_video',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => __( 'Video Post Format', 'framework' ),

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'post' ),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',

	// Auto save: true, false (default). Optional.
	'autosave' => true,

	'fields' => array(
		// OEMBED
			array(
			'name' => __( 'Video URL (oEmbed)', 'framework' ),
			'id' => "{$prefix}pf_video_oembed",
			'desc' => __( 'Enter the URL to your video. (Supported services: YouTube, Vimeo, Hulu, Instagram, Qik)', 'framework' ),
			'type' => 'oembed',
		),
		// FILE ADVANCED (WP 3.5+)
		//array(
		//	'name' => __( 'Video File Upload', 'framework' ),
		//	'id' => "{$prefix}pf_video_file",
		//	'type' => 'file_advanced',
		//	'max_file_uploads' => 1,
		//	'mime_type' => 'video', // Leave blank for all file types
		//),
		// IMAGE ADVANCED (WP 3.5+)
		//array(
		//'name' => __( 'Video Poster Upload', 'framework' ),
		//'id' => "{$prefix}pf_video_file_poster",
		//'desc' => __( 'The video poster is displayed on the video player until the user hits the play button. (Optional).', 'framework' ),
		//'type' => 'image_advanced',
		//'max_file_uploads' => 1,
		//),
		
			// CHECKBOX
			array(
			'name' => __( 'Show Title on Timeline?', 'framework' ),
			'desc' => __( 'Check to show the post title on the blog timeline.', 'framework' ),
			'id' => "{$prefix}pf_video_post_title",
			'type' => 'checkbox',
			// Value can be 0 or 1
			'std' => 1,
			),
			// CHECKBOX
			array(
			'name' => __( 'Show Excerpt on Timeline?', 'framework' ),
			'desc' => __( 'Check to show the post excerpt on the blog timeline.', 'framework' ),
			'id' => "{$prefix}pf_video_post_excerpt",
			'type' => 'checkbox',
			// Value can be 0 or 1
			'std' => 1,
			),

	)
);

// Audio Post Format
$meta_boxes[] = array(
	
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'pf_audio',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => __( 'Audio Post Format', 'framework' ),

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'post' ),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',

	// Auto save: true, false (default). Optional.
	'autosave' => true,

	'fields' => array(
		// OEMBED
			array(
			'name' => __( 'SoundCloud URL', 'framework' ),
			'id' => "{$prefix}pf_audio_oembed",
			'desc' => __( 'Enter the URL to your SoundCloud track.', 'framework' ),
			'type' => 'oembed',
		),
		// FILE ADVANCED (WP 3.5+)
		//array(
		//	'name' => __( 'Audio File Upload', 'framework' ),
		//	'id' => "{$prefix}pf_audio_file",
		//	'type' => 'file_advanced',
		//	'max_file_uploads' => 1,
		//	'mime_type' => 'audio', // Leave blank for all file types
		//),
		
			// CHECKBOX
			array(
			'name' => __( 'Show Title on Timeline?', 'framework' ),
			'desc' => __( 'Check to show the post title on the blog timeline.', 'framework' ),
			'id' => "{$prefix}pf_audio_post_title",
			'type' => 'checkbox',
			// Value can be 0 or 1
			'std' => 1,
			),
			// CHECKBOX
			array(
			'name' => __( 'Show Excerpt on Timeline?', 'framework' ),
			'desc' => __( 'Check to show the post excerpt on the blog timeline.', 'framework' ),
			'id' => "{$prefix}pf_audio_post_excerpt",
			'type' => 'checkbox',
			// Value can be 0 or 1
			'std' => 1,
			),

	)
);

// Quote Post Format
$meta_boxes[] = array(
	
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'pf_quote',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => __( 'Quote Post Format', 'framework' ),

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'post' ),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',

	// Auto save: true, false (default). Optional.
	'autosave' => true,

	'fields' => array(
			// WYSIWYG/RICH TEXT EDITOR
			array(
				'name' => __( 'Quote', 'framework' ),
				'id' => "{$prefix}pf_quote",
				'type' => 'wysiwyg',
				// Set the 'raw' parameter to TRUE to prevent data being passed through wpautop() on save
				'raw' => false,
				'std' => __( '', 'framework' ),
					// Editor settings, see wp_editor() function: look4wp.com/wp_editor
					'options' => array(
					'textarea_rows' => 4,
					'teeny' => true,
					'media_buttons' => false,
					),
			),
			// TEXT
			array(
				// Field name - Will be used as label
				'name' => __( 'Quote Citation', 'framework' ),
				// Field ID, i.e. the meta key
				'id' => "{$prefix}pf_quote_cite",
				// Field description (optional)
				'desc' => __( '(Optional)', 'framework' ),
				'type' => 'text',
			),
			// COLOR
			array(
			'name' => __( 'Quote Color', 'framework' ),
			'desc' => __( 'Use a custom color for the quote background.(Optional - theme color will be used by default)', 'framework' ),
			'id' => "{$prefix}pf_quote_color",
			'type' => 'color',
			),
			
			// CHECKBOX
			array(
			'name' => __( 'Show Title on Timeline?', 'framework' ),
			'desc' => __( 'Check to show the post title on the blog timeline.', 'framework' ),
			'id' => "{$prefix}pf_quote_post_title",
			'type' => 'checkbox',
			// Value can be 0 or 1
			'std' => 1,
			),
			// CHECKBOX
			array(
			'name' => __( 'Show Excerpt on Timeline?', 'framework' ),
			'desc' => __( 'Check to show the post excerpt on the blog timeline.', 'framework' ),
			'id' => "{$prefix}pf_quote_post_excerpt",
			'type' => 'checkbox',
			// Value can be 0 or 1
			'std' => 1,
			),

	)
);


// Link Post Format
$meta_boxes[] = array(
	
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'pf_link',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => __( 'Link Post Format', 'framework' ),

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'post' ),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',

	// Auto save: true, false (default). Optional.
	'autosave' => true,

	'fields' => array(
				// TEXT
			array(
			// Field name - Will be used as label
			'name' => __( 'Link Title', 'framework' ),
			// Field ID, i.e. the meta key
			'id' => "{$prefix}pf_link_text",
			'type' => 'text',
			),
			// URL
			array(
				'name' => __( 'Link URL', 'framework' ),
				'id' => "{$prefix}pf_link_url",
				'type' => 'url',
			),
			// COLOR
			array(
			'name' => __( 'Link BG Color', 'framework' ),
			'desc' => __( 'Use a custom color for the link background.(Optional - theme color will be used by default)', 'framework' ),
			'id' => "{$prefix}pf_link_color",
			'type' => 'color',
			),
			
			// CHECKBOX
			array(
			'name' => __( 'Show Title on Timeline?', 'framework' ),
			'desc' => __( 'Check to show the post title on the blog timeline.', 'framework' ),
			'id' => "{$prefix}pf_link_post_title",
			'type' => 'checkbox',
			// Value can be 0 or 1
			'std' => 1,
			),
			// CHECKBOX
			array(
			'name' => __( 'Show Excerpt on Timeline?', 'framework' ),
			'desc' => __( 'Check to show the post excerpt on the blog timeline.', 'framework' ),
			'id' => "{$prefix}pf_link_post_excerpt",
			'type' => 'checkbox',
			// Value can be 0 or 1
			'std' => 1,
			),

	)
);

// Status Post Format
$meta_boxes[] = array(
	
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'pf_status',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => __( 'Status Post Format', 'framework' ),

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'post' ),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',

	// Auto save: true, false (default). Optional.
	'autosave' => true,

	'fields' => array(
		// OEMBED
			array(
			'name' => __( 'Twitter Status', 'framework' ),
			'id' => "{$prefix}pf_status_oembed",
			'desc' => __( 'Enter the URL to your Tweet.', 'framework' ),
			'type' => 'oembed',
		),
		// WYSIWYG/RICH TEXT EDITOR
			array(
				'name' => __( 'Custom Status', 'framework' ),
				'id' => "{$prefix}pf_status",
				'desc' => __( 'Enter a custom status.', 'framework' ),
				'type' => 'wysiwyg',
				// Set the 'raw' parameter to TRUE to prevent data being passed through wpautop() on save
				'raw' => false,
				'std' => __( '', 'framework' ),
					// Editor settings, see wp_editor() function: look4wp.com/wp_editor
					'options' => array(
					'textarea_rows' => 4,
					'teeny' => true,
					'media_buttons' => false,
					),
			),
			// COLOR
			array(
			'name' => __( 'Custom Status Color', 'framework' ),
			'desc' => __( 'Use a custom color for the status background.(Optional - theme color will be used by default)', 'framework' ),
			'id' => "{$prefix}pf_status_color",
			'type' => 'color',
			),
			
			// CHECKBOX
			array(
			'name' => __( 'Show Title on Timeline?', 'framework' ),
			'desc' => __( 'Check to show the post title on the blog timeline.', 'framework' ),
			'id' => "{$prefix}pf_status_post_title",
			'type' => 'checkbox',
			// Value can be 0 or 1
			'std' => 1,
			),
			// CHECKBOX
			array(
			'name' => __( 'Show Excerpt on Timeline?', 'framework' ),
			'desc' => __( 'Check to show the post excerpt on the blog timeline.', 'framework' ),
			'id' => "{$prefix}pf_status_post_excerpt",
			'type' => 'checkbox',
			// Value can be 0 or 1
			'std' => 1,
			),

	)
);


// Chat Post Format
$meta_boxes[] = array(
	
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'pf_chat',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => __( 'Chat Post Format', 'framework' ),

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'post' ),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',

	// Auto save: true, false (default). Optional.
	'autosave' => true,

	'fields' => array(
		// WYSIWYG/RICH TEXT EDITOR
			array(
				'name' => __( 'Chat Transcript', 'framework' ),
				'id' => "{$prefix}pf_chat_transcript",
				'type' => 'wysiwyg',
				// Set the 'raw' parameter to TRUE to prevent data being passed through wpautop() on save
				'raw' => false,
				'std' => __( '', 'framework' ),
					// Editor settings, see wp_editor() function: look4wp.com/wp_editor
					'options' => array(
					'textarea_rows' => 4,
					'teeny' => true,
					'media_buttons' => false,
					),
			),
			
			// CHECKBOX
			array(
			'name' => __( 'Show Title on Timeline?', 'framework' ),
			'desc' => __( 'Check to show the post title on the blog timeline.', 'framework' ),
			'id' => "{$prefix}pf_chat_post_title",
			'type' => 'checkbox',
			// Value can be 0 or 1
			'std' => 1,
			),
			// CHECKBOX
			array(
			'name' => __( 'Show Excerpt on Timeline?', 'framework' ),
			'desc' => __( 'Check to show the post excerpt on the blog timeline.', 'framework' ),
			'id' => "{$prefix}pf_chat_post_excerpt",
			'type' => 'checkbox',
			// Value can be 0 or 1
			'std' => 1,
			),

	)
);

/********************* META BOX REGISTERING ***********************/

/**
 * Register meta boxes
 *
 * @return void
 */
function ht_register_meta_boxes()
{
	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( !class_exists( 'RW_Meta_Box' ) )
		return;

	global $meta_boxes;
	foreach ( $meta_boxes as $meta_box )
	{
		new RW_Meta_Box( $meta_box );
	}
}
// Hook to 'admin_init' to make sure the meta box class is loaded before
// (in case using the meta box class in another plugin)
// This is also helpful for some conditionals like checking page template, categories, etc.
add_action( 'admin_init', 'ht_register_meta_boxes' );
