<?php
/**
 * Register Sidebars 
 */
 
add_action( 'widgets_init', 'st_register_sidebars' );

function st_register_sidebars() {
	
	register_sidebar(array(
		'name' => __( 'Default Sidebar', 'framework' ),
		'id' => 'st_sidebar_primary',
		'before_widget' => '<div id="%1$s" class="widget %2$s clearfix">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
		)
	);	
	
	// Setup footer widget column option variable
	$footer_widget_layout = get_theme_mod( 'ht_style_footerwidgets' );
	if ($footer_widget_layout == '2col') {
		$footer_widget_col = 'ht-col-half';
		$footer_widget_col_descirption = 'Two Columns';
	} elseif ($footer_widget_layout == '3col') {
		$footer_widget_col = 'ht-col-third';
		$footer_widget_col_descirption = 'Three Columns';
	} elseif ($footer_widget_layout == '4col') {
		$footer_widget_col = 'ht-col-fourth';
		$footer_widget_col_descirption = 'Fours Columns';
	} else {
		$footer_widget_col = 'ht-col-third';
		$footer_widget_col_descirption = 'Three Columns';
	}
	
	register_sidebar(array(
		'name' => __( 'Footer Widgets', 'framework' ),
		'description'   => 'The footer widget area is currently set to: '.$footer_widget_col_descirption.'. To change it go to the theme options panel.',
		'id' => 'st_sidebar_footer',
		'before_widget' => '<div id="%1$s" class="ht-column '.$footer_widget_col.' widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title"><span>',
		'after_title' => '</span></h4>',
		)
	);

}

