<?php
function current_frontend() {
	global $INI;
	$a = array(
			'/index.php' => 'NoOpY du jour',
                        '/index.php?t=w' => 'NoOpY de la semaine',
			'/team/index.php' => 'NoOpYs récents',
			//'/help/tour.php' => 'Tour',
			//'/forum/index.php' => 'Discuss',
			'/help/faqs.php' => 'FAQs',
			);
	if(option_yes('navforum')) $a['/forum/index.php'] = 'Discuss';
	$r = $_SERVER['REQUEST_URI'];
	
        if(preg_match('#index.php\?t=w#',$r))
                $l = '/index.php?t=w';
        else if (preg_match('#/team#',$r))
                $l = '/team/index.php';
	else if (preg_match('#/help#',$r))
                $l = '/help/tour.php';
	else if (preg_match('#/subscribe#',$r))
                $l = '/subscribe.php';
        else
            $l = '/index.php';
	return current_link($l, $a);
}

function current_backend() {
	global $INI;
	$a = array(
			'/manage/misc/index.php' => 'Home',
			'/manage/team/index.php' => 'Hype',
			'/manage/order/index.php' => 'Order',
			'/manage/coupon/index.php' => 'Coupon',
			'/manage/user/index.php' => 'User',
			'/manage/market/index.php' => 'Marketing',
			'/manage/category/index.php' => 'Category',
			'/manage/vote/index.php' => 'Survey',
			'/manage/credit/index.php' => 'Credit',
			'/manage/system/index.php' => 'Setting',
			);
	$r = $_SERVER['REQUEST_URI'];
	if (preg_match('#/manage/(\w+)/#',$r, $m)) {
		$l = "/manage/{$m[1]}/index.php";
	} else $l = '/manage/misc/index.php';
	return current_link($l, $a);
}

function current_biz() {
	global $INI;
	$a = array(
			'/biz/index.php' => 'Home',
			'/biz/settings.php' => 'Biz info',
			'/biz/coupon.php' => $INI['system']['couponname'] . 'list',
			);
	$r = $_SERVER['REQUEST_URI'];
	if (preg_match('#/biz/coupon#',$r)) $l = '/biz/coupon.php';
	elseif (preg_match('#/biz/settings#',$r)) $l = '/biz/settings.php';
	else $l = '/biz/index.php';
	return current_link($l, $a);
}

function current_forum($selector='index') {
	global $city;
	$a = array(
			'/forum/index.php' => 'Toutes',
			'/forum/city.php' => "{$city['name']} Forum",
			'/forum/public.php' => 'Public forum',
			);
	if (!$city) unset($a['/forum/city.php']);
	$l = "/forum/{$selector}.php";
	return current_link($l, $a, true);
}

function current_invite($selector='refer') {
	$a = array(
			'/account/refer.php' => 'Toutes',
			'/account/referpending.php' => "En attente D'achat",
			'/account/referdone.php' => 'Rabais utilisé',
			);
	$l = "/account/{$selector}.php";
	return current_link($l, $a, true);
}

function current_partner($gid='0') {
	$a = array(
			'/partner/index.php' => 'Toutes',
			);
	foreach(option_category('partner') AS $id=>$name) {
		$a["/partner/index.php?gid={$id}"] = $name;
	}
	$l = "/partner/index.php?gid={$gid}";
	if (!$gid) $l = "/partner/index.php";
	return current_link($l, $a, true);
}

function current_city($cename, $citys) {
	$link = "/city.php?ename={$cename}";
	$links = array();
	foreach($citys AS $city) {
		$links["/city.php?ename={$city['ename']}"] = $city['name'];
	}
	return current_link($link, $links);
}

function current_coupon_sub($selector='index') {
	$selector = $selector ? $selector : 'index';
	$a = array(
		'/coupon/index.php' => 'Usable',
		'/coupon/consume.php' => 'Used',
		'/coupon/expire.php' => 'Expired',
	);
	$l = "/coupon/{$selector}.php";
	return current_link($l, $a);
}

function current_account($selector='/account/settings.php') {
	global $INI;
	$a = array(
		//'/coupon/index.php' => 'My ' . $INI['system']['couponname'],
		'/order/index.php' => 'Mes NoOpYs',
		'/account/refer.php' => 'Mes invitations',
		'/credit/index.php' => 'Mon solde?',
		'/account/settings.php' => 'Mon profil',
		'/account/myask.php' => 'Mes questions et réponses?',
	);
	if (option_yes('usercredit')) {
		$a['/credit/score.php'] = 'My Points';
	}
	return current_link($selector, $a, true);
}

function current_about($selector='us') {
	global $INI;
	$a = array(
		'/about/us.php' => 'About' . $INI['system']['abbreviation'],
		'/about/contact.php' => 'Contact',
		'/about/job.php' => 'Jobs',
		'/about/terms.php' => 'User terms',
		'/about/privacy.php' => 'Privacy statement',
	);
	$l = "/about/{$selector}.php";
	return current_link($l, $a, true);
}

function current_help($selector='faqs') {
	global $INI;
	$a = array(
		'/help/tour.php' => 'Play with ' . $INI['system']['abbreviation'],
		'/help/faqs.php' => 'FAQ',
		'/help/wroupon.php' => 'What is ' . $INI['system']['abbreviation'],
	);
	$l = "/help/{$selector}.php";
	return current_link($l, $a, true);
}

function current_recent_index($selector='index',$groupid = 0) {
	$selector = $selector ? $selector : 'index';
        $groupSelector = $groupid ? "&gid={$groupid}" : "";
	$a = array(
		"/team/index.php?t=index{$groupSelector}" => 'Tous',
		"/team/index.php?t=d{$groupSelector}" => 'Du jour',
		"/team/index.php?t=w{$groupSelector}" => 'De la semaine',
	);
	$l = "/team/index.php?t={$selector}{$groupSelector}";
	return current_link($l, $a);
}

function current_order_index($selector='index') {
	$selector = $selector ? $selector : 'index';
	$a = array(
		'/order/index.php?s=index' => 'Toutes',
		'/order/index.php?s=unpay' => 'To be paid',
		'/order/index.php?s=pay' => 'Paid',
	);
	$l = "/order/index.php?s={$selector}";
	return current_link($l, $a);
}

function current_credit_index($selector='index') {
	$selector = $selector ? $selector : 'index';
	$a = array(
		'/credit/score.php' => 'My Points',
		'/credit/goods.php' => 'Convert Points',
	);
	$l = "/credit/{$selector}.php";
	return current_link($l, $a);
}

function current_link($link, $links, $span=false) {
	$html = '';
	$span = $span ? '<span></span>' : '';
	foreach($links AS $l=>$n) {
		if (trim($l,'/')==trim($link,'/')) {
			$html .= "<li class=\"current\"><a href=\"{$l}\">{$n}</a>{$span}</li>";
		}
		else $html .= "<li><a href=\"{$l}\">{$n}</a>{$span}</li>";
	}
	return $html;
}

/* manage current */
function mcurrent_misc($selector=null) {
	$a = array(
		'/manage/misc/index.php' => 'Home',
		'/manage/misc/ask.php' => 'Q & A',
		'/manage/misc/feedback.php' => 'Feedback',
		'/manage/misc/subscribe.php' => 'Email',
		'/manage/misc/smssubscribe.php' => 'SMS',
		'/manage/misc/invite.php' => 'Rebate',
		'/manage/misc/money.php' => 'Finance',
		'/manage/misc/link.php' => 'Friend link',
		'/manage/misc/backup.php' => 'Backup',
	);
	$l = "/manage/misc/{$selector}.php";
	return current_link($l,$a,true);
}

function mcurrent_misc_money($selector=null){
	$selector = $selector ? $selector : 'store';
	$a = array(
		'/manage/misc/money.php?s=store' => 'Offline topup',
		'/manage/misc/money.php?s=charge' => 'Online topup',
		'/manage/misc/money.php?s=withdraw' => 'Withdraw records',
		'/manage/misc/money.php?s=cash' => 'Pay in cach',
		'/manage/misc/money.php?s=refund' => 'Refund records',
	);
	$l = "/manage/misc/money.php?s={$selector}";
	return current_link($l, $a);
}

function mcurrent_misc_backup($selector=null){
	$selector = $selector ? $selector : 'backup';
	$a = array(
		'/manage/misc/backup.php' => 'Database backup',
		'/manage/misc/restore.php' => 'Database restore',
	);
	$l = "/manage/misc/{$selector}.php";
	return current_link($l, $a);
}

function mcurrent_misc_invite($selector=null){
	$selector = $selector ? $selector : 'index';
	$a = array(
		'/manage/misc/invite.php?s=index' => 'Pendings',
		'/manage/misc/invite.php?s=record' => 'Approved',
		'/manage/misc/invite.php?s=cancel' => 'Fouls',
	);
	$l = "/manage/misc/invite.php?s={$selector}";
	return current_link($l, $a);
}
function mcurrent_order($selector=null) {
	$a = array(
		'/manage/order/index.php' => 'Order of today\'s Hype',
		'/manage/order/pay.php' => 'Order paid',
		'/manage/order/credit.php' => 'Pay with balance',
		'/manage/order/unpay.php' => 'Order to be paid',
	);
	$l = "/manage/order/{$selector}.php";
	return current_link($l,$a,true);
}

function mcurrent_user($selector=null) {
	$a = array(
		'/manage/user/index.php' => 'User list',
		'/manage/user/manager.php' => 'Manager list',
		'/manage/partner/index.php' => 'Partner list',
		'/manage/partner/create.php' => 'New Partner',
	);
	$l = "/manage/user/{$selector}.php";
	return current_link($l,$a,true);
}
function mcurrent_team($selector=null) {
	$a = array(
		'/manage/team/index.php' => 'Today\'s Hype',
                '/manage/team/index.php?team_type=weekly' => 'Weekly Hype',
		'/manage/team/success.php' => 'Successful Hype',
		'/manage/team/failure.php' => 'Unsuccessful Hype',
		'/manage/team/edit.php' => 'New Hype',
	);
        if ( $selector == "weekly" )
            $l = "/manage/team/index.php?team_type=weekly";
        else
            $l = "/manage/team/{$selector}.php";
	return current_link($l,$a,true);
}

function mcurrent_feedback($selector=null) {
	$a = array(
		'/manage/feedback/index.php' => 'Overview',
	);
	$l = "/manage/feedback/{$selector}.php";
	return current_link($l,$a,true);
}
function mcurrent_coupon($selector=null) {
	$a = array(
		'/manage/coupon/index.php' => 'Usable',
		'/manage/coupon/consume.php' => 'Used',
		'/manage/coupon/expire.php' => 'Expired',
		'/manage/coupon/card.php' => 'Coupon',
		'/manage/coupon/cardcreate.php' => 'New coupon',
	);
	$l = "/manage/coupon/{$selector}.php";
	return current_link($l,$a,true);
}
function mcurrent_category($selector=null) {
	$zones = get_zones();
	$a = array();
	foreach( $zones AS $z=>$o ) {
		$a['/manage/category/index.php?zone='.$z] = $o;
	}
	$l = "/manage/category/index.php?zone={$selector}";
	return current_link($l,$a,true);
}
function mcurrent_partner($selector=null) {
	$a = array(
		'/manage/user/index.php' => 'User list',
		'/manage/user/manager.php' => 'Manager list',
		'/manage/partner/index.php' => 'Biz list',
		'/manage/partner/create.php' => 'New biz',
	);
	$l = "/manage/partner/{$selector}.php";
	return current_link($l,$a,true);
}
function mcurrent_market($selector=null) {
	$a = array(
		'/manage/market/index.php' => 'Email marketing',
		'/manage/market/sms.php' => 'SMS group-sending',
		'/manage/market/down.php' => 'Data download',
	);
	$l = "/manage/market/{$selector}.php";
	return current_link($l,$a,true);
}
function mcurrent_market_down($selector=null) {
	$a = array(
		'/manage/market/down.php' => 'Mobile number',
		'/manage/market/downemail.php' => 'Email address',
		'/manage/market/downorder.php' => 'Hype order',
		'/manage/market/downcoupon.php' => 'Coupon',
		'/manage/market/downuser.php' => 'User info',
	);
	$l = "/manage/market/{$selector}.php";
	return current_link($l,$a,true);
}

function mcurrent_system($selector=null) {
	$a = array(
		'/manage/system/index.php' => 'Basic',
		'/manage/system/option.php' => 'Option',
		'/manage/system/bulletin.php' => 'Bulletin',
		'/manage/system/pay.php' => 'Pay',
		'/manage/system/email.php' => 'Email',
		'/manage/system/sms.php' => 'SMS',
		'/manage/system/page.php' => 'Webpage',
		'/manage/system/cache.php' => 'Cache',
		'/manage/system/skin.php' => 'Skin',
		'/manage/system/template.php' => 'Template',
		'/manage/system/upgrade.php' => 'Upgrade',
	);
	$l = "/manage/system/{$selector}.php";
	return current_link($l,$a,true);
}

function mcurrent_credit($selector=null) {
	$a = array(
		'/manage/credit/index.php' => 'Credit records',
		'/manage/credit/settings.php' => 'Credit rules',
		'/manage/credit/goods.php' => 'Convert credits',
	);
	$l = "/manage/credit/{$selector}.php";
	return current_link($l,$a,true);
}
