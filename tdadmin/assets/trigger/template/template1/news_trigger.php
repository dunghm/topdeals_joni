<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
$imageUrl	=	"http://".$_SERVER['HTTP_HOST']."/tdadmin/assets/trigger/template/template1/images/";
?>
</head>

<body style="padding: 0; margin: 0;">
	<table style="width: 600px; margin: 0 auto;" cellspacing="0" cellpadding="0">
		<tr>
			<td style="text-align: center;border: 2px solid #7f7f7f; border-bottom: none; padding: 15px 0;">
				<img src="<?php echo $imageUrl.'logo.png'; ?>"/>
			</td>
		</tr>
		<tr>
			<td style="text-align:center;background: #26C6AF;">
				<img src="<?php echo $imageUrl."promo-code.png"; ?>" width="599" height="96" style="margin: 20px 1px;"/>							
			</td>
		</tr><!-- Header -->
		<tr>
			<td style="background: #26C6AF; padding: 0 20px;">
				<table style="width: 100%;" cellspacing="0" cellpadding="0">					
					<tr>
						<td style="background: #fff; padding: 40px 40px 30px;">
							<table style="width: 100%;" cellpadding="0" cellspacing="0">		
								<tr>
									<td style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 16px;">
										<p>Bonjour Aurore,</p>
										<p>Voici quelques temps que vous n'êtes pas revenu profiter des offres de Topdeal. Il y a plein de nouvelles offres!</p>
										<p>Nous serions heureux de vous faire profiter d'une <strong>remise exceptionnelle de 10%</strong> sur tout le site pour un achat réalisé dans les 48h prochaines heures et vous avons préparé un code promo rien que pour vous: <strong style="color:#26C6AF">WELCOMEBACK2534RT</strong>.</p>
									</td>
								</tr>
								<tr>
									<td style="text-align: center;">
										<img src="<?php echo $imageUrl.'profile.png'; ?>" width="74"/>
									</td>
								</tr>
								<tr><td style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 16px;">
									<p>Nous espérons que vous trouverez votre bonheur dans nos offres et vous souhaitons une plaisante visite.</p>
									<p>A tout bientôt,</p>
									<p><strong>L’équipe Topdeal </strong></p>
								</td></tr>
								<tr>
									<td style="text-align:center;font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 14px;">
										<img src="<?php echo $imageUrl.'bullet.png'; ?>" width="146" height="14" style="margin: 4px 0 10px;"/>
										<p style="margin: 10px 0 0;"><em>Voici nos bestsellers du moment:</em></p>
									</td>
								</tr>
								<tr><td style="padding: 20px 0;">
									<table style="width: 100%">
										<tr>
											<td style="padding-right: 11px; vertical-align: top;">
												<a href="#" style="border: 0; display: block;"><img src="<?php echo $imageUrl.'product.png'; ?>" width="230" height="154" style="border: none; display: block;"/></a>
												<p style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 14px; padding: 0;  margin: 10px 0 15px;"><a href="#" style="text-decoration: none; color: #000;">2 entrées pour Happyland, le plus grand parc d'attractions ...</a></p>
												<a href="#" style="text-decoration: none; display:block;width: 151px; height: 59px; margin: 0 auto; border: 2px solid #7a7978; background-color: #f5f3f1; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; padding-left: 10px;">
													<div style="color: #c92525; margin: 7px 0 0;">
														<span style="font-size: 18px;">Dès </span><span style="font-size: 24px;">CHF 29.90</span>
													</div>
													<div style="font-size: 14px;  margin: 0; color: #000;">au lieu de CHF 50.-</div>
												</a>
											</td>											
											<td style="vertical-align: top;">
												<a href="#" style="border: 0; display: block;"><img src="<?php echo $imageUrl.'product.png'; ?>" width="230" height="154" style="border: 0; display: block;"/></a>
												<p style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 14px; padding: 0;  margin: 10px 0 15px;"><a href="#" style="text-decoration: none; color: #000;">2 entrées pour Happyland, le plus grand parc d'attractions ...</a></p>
												<a href="#" style="text-decoration: none; display:block;width: 151px; height: 59px; margin: 0 auto; border: 2px solid #7a7978; background-color: #f5f3f1; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; padding-left: 10px;">
													<div style="color: #c92525; margin: 7px 0 0;">
														<span style="font-size: 18px;">Dès </span><span style="font-size: 24px;">CHF 29.90</span>
													</div>
													<div style="font-size: 14px;  margin: 0; color: #000;">au lieu de CHF 50.-</div>
												</a>
											</td>
										</tr>
									</table>
								</td></tr>
								
								<tr>
									<td colspan="2">
										<table style="width: 100%;" cellspacing="0" cellpadding="0">
											<tr>
												<td><p style="text-align: center; font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 10px; text-decoration: none;">Vous aimez <a href="" style="text-decoration: underline; color: #000;">www.topdeal.ch?</a><br>Faites-le savoir!</p></td>
												
											</tr>
										</table>
									</td>									
								</tr>
							</table>
						</td>
					</tr><!-- Main -->
				</table>
				<a href="#" style="display:block; text-align: center; color: #fff;font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 10px; text-decoration: underline; padding: 3px 0;">Vous désabonner</a>
			</td>
		</tr><!-- Content -->
	</table>
	
</body>
</html>
