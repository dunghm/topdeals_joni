$('#collapse-menu').click(function() {
	// Add or Remove Class on Body to Collapse Admin Menu
	$('body').toggleClass('folded');
})


// Add/Remove Class on Hover Admin Menu for testing development, Disable it when finish testing and make it by on click event
$('#adminmenu li.menu-top').hover(function() {
	$(this).addClass('opensub');
        
	if ( !$(this).hasClass('opensub') ) {
	    $(this).delay(200).queue(function(){
	        $(this).addClass('opensub').clearQueue();
	    });
	}

}, function() {
	$(this).removeClass('opensub');
})



// Show/Hide Admin Menu
$('#adminmenu .tt-has-submenu a.menu-top').click(function(e) {
	$(this).parent('li').toggleClass('selected');
	e.preventDefault();
})


// http://stackoverflow.com/questions/19970608/remove-class-when-browser-loads-at-width-at-less-than-700px
// Add class on window size dependant
$(document).ready(function(e) {
    dothis();
    
});

$(window).resize(function(e) {
    dothis();
});

function dothis(){
    console.log($(window).width());
    if($(window).width() < 641){
        $('body').addClass('mobile auto-fold');
        $('body').removeClass('desktop');
    } 
    else {
        $('body').removeClass('mobile auto-fold');
        $('body').addClass('desktop');
    }
}

/*
 * adding notification message at create.blade.php
 */

function show_message(msg_show){
	$('.alert-container').fadeIn('slow');
	$('.alert-container').addClass('in').find('.tt-alert-success').html(msg_show);
	setTimeout(function(){$('.alert-container').fadeOut('slow')}, 10000);
}

/*
 * add new contact at contact info page
 */

function show_hide(show_div,hide_div){
	if(show_div != '')
		$('#'+show_div).slideDown('slow');
	
	if(hide_div != '')
	$('#'+hide_div).slideUp('slow');
}

/*
 * Remove Contact at Contact Info Template
 */

function remove_form(hide_div,show_div,element_group){
	if(hide_div != '')
            $( '#'+hide_div ).slideUp();
        
        if(show_div != '')
		$('#'+show_div).slideDown('slow');
        
	$( '#'+element_group ).find('input , select').each(function(){
		$( this).val('');
	});
}

$('.bs-tooltip').tooltip()

/*
* NEWSLETTER
*/
//PREVIEW DEAL
jQuery(document).ready(function(){
	
	//Preview  Newsletter on Create Screen
	jQuery(document).on("click", "#preview-newsletter-edit", function(e) {
		
		//HIDE TEST EMAIL FORM
		jQuery('.preview-newletter-form').hide();
		
		href = jQuery(this).attr('href');
		if(jQuery('.deals-hidden').length == 0){
			alert('Please add a deal in newsletter');
		}
		else{
			//SHOW PLEASE WAIT MESSAGE
			jQuery('.overlay-ajax-region').show();
		
			jQuery.ajax({
				url: href,
				data: jQuery("#create-form :not(#id) > :input").serialize(),
				success: function(events) {
					jQuery("#modal-body-preview").html(events);
					jQuery('#preview-modal').modal('show');
					//HIDE PLEASE WAIT MESSAGE
					jQuery('.overlay-ajax-region').hide();
				}
			});
		}
	});
	
	//Preview Newsletter on LIsting Screen
	jQuery(document).on("click", "#preview-newsletter-list", function(e) {
		
		//SHOW TEST EMAIL FORM
		jQuery('.preview-newletter-form').show();
		//SET NEWSLETTER ID ON FORM
		var newsletter_id = jQuery(this).attr('newsletter_id');
		jQuery('#newsletter_id').val(newsletter_id);
		
		//SHOW PLEASE WAIT MESSAGE
		jQuery('.overlay-ajax-region').show();
		href = jQuery(this).attr('href');

		jQuery.ajax({
			url: href,
			success: function(events) {
				jQuery("#modal-body-preview").html(events);
				jQuery('#preview-modal').modal('show');
				//HIDE PLEASE WAIT MESSAGE
				jQuery('.overlay-ajax-region').hide();
			}
		});
		
		return false;
	});
	
});

jQuery(document).on("click", ".deal-remove", function(e) {
	id = jQuery(this).attr('id');
	jQuery('#deal-' + id).remove();
});

//DELETE NEWSLETTER RECORD
function deleteNewsletterRecord(){
	// alert(newsLetterId);
    if (confirm("Are you sure, you want to delete this newsletter!") == true) {
       return true;
    } 
	return false;
}


//GET DEAL BY CATEGORY
function getDealByCategory(action_url){
	
		var selectedVal	=	jQuery('#deal_category').val();
		// var action_url 	= "<?php echo URL::to('admin/getdealbycategory'); ?>/";
		
		queryData = 'action=calldeals&id='+selectedVal;
			
		jQuery.ajax({
					type: "GET",
					url: action_url,
					data: queryData,
					cache: false,
					success: function(html) 
					{
						if(html && html!=""){
						

							jQuery('#deal_titles')
								.find('option')
								.remove()
								.end()
								.append(html);
								// .val()

						}else{
							jQuery('#deal_titles')
								.find('option')
								.remove()
								.end('<option value="">--</option>')
								.append(html);
						}
					}
				});
}

//ADD DEAL AND CLOSE DIALOG
function addDealAndClose(action_url){
	
	deal_titles = jQuery('#deal_titles').val();
	if (deal_titles) {

		add_deal = true;
		jQuery('.deals-hidden').each(function() {
			id = jQuery(this).val();

			if (id == deal_titles) {
				alert('You have already added the deal in newsletter');
				add_deal = false;
			}
		});
		if (add_deal == true) {

			//SHOW PLEASE WAIT MESSAGE
			jQuery('.overlay-ajax-region').show();
			
			jQuery.ajax({
				url: action_url,
				type: "GET",
				cache: false,
				data: {
					id: deal_titles,
					action: 'add-deal'
				},
				success: function(events) {
					
					jQuery(".sortable").append(events);
					jQuery('#add-deal-modal').modal('hide');
					
					//SHOW PLEASE WAIT MESSAGE
					jQuery('.overlay-ajax-region').hide();
				}
			});
		}
	}
	else {
		alert('Please select a deal');
	}
}

//ADD DEAL AND CONTINUE WITH DIALOG
function addDealAndContinue(action_url){
	
	deal_titles = $('#deal_titles').val();

	if (deal_titles) {

		add_deal = true;
		jQuery('.deals-hidden').each(function() {
			id = $(this).val();

			if (id == deal_titles) {
				alert('You have already added the deal in newsletter');
				add_deal = false;
			}
		});
		if (add_deal == true) {

			//SHOW PLEASE WAIT MESSAGE
			jQuery('.overlay-ajax-region').show();
			
			jQuery.ajax({
				url: action_url,
				type: "GET",
				cache: false,
				data: {
					id: deal_titles,
					action: 'add-deal'
				},
				success: function(events) {
					jQuery(".sortable").append(events);
					//SHOW PLEASE WAIT MESSAGE
					jQuery('.overlay-ajax-region').hide();
				}
			});
		}
	}
	else {
		alert('Please select a deal');
	}
}

//AJAX REQUEST BIND & DISABLE SCREEN
jQuery("body").prepend("<div class=\"overlay-ajax-region\"><span class=\"overlay-content\">Please Wait...</span></div>");

jQuery(".overlay-ajax-region").css({
    "position": "fixed", 
    "width": jQuery(document).width(), 
    "height": jQuery(document).height(),
    "z-index": 99999, 
    "background-color":"grey",
	"color" : "white"
}).fadeTo(0, 0.5);

jQuery('.overlay-ajax-region').css("display","none");

jQuery(".overlay-content").css({
	  "margin": "0px 0px 0px -50px", /* left margin is half width of the div, to centre it */
	  "padding": "30px 10px 10px 10px",
	  "position": "fixed",
	  "left": "35%",
	  "top": "325px",
	  "width": "500px",
	  "height": "150px",
	  "text-align": "center",
});

// jQuery(document).ajaxStart(function () {
	// jQuery('.overlay-ajax-region').show();
// }).ajaxStop(function () {
	// jQuery('.overlay-ajax-region').hide();
// });

//PREVIEW TEST EMAIL SEND
jQuery('#send_email').on('click',function(){
	
	var email_sent_to = jQuery.trim(jQuery('#email_sent_to').val());
	
	var err_Message	=	"";
	
	if(email_sent_to==""){
		err_Message += "Please Enter Email Address";
	}
	
	// if(email_sent_to!=""){
		// if( !isValidEmailAddress( email_sent_to ) ) {
			// err_Message += "Please Enter Valid Email Address";
		// }
	// }
	
	if(err_Message==""){
		jQuery('.preview_email_err_region').css("display","none");
		jQuery('.preview_email_err_region').html("");
		
		//HIDE BUTTON SHOW PELASE WAIT MESSAGE
		jQuery('.sbmt-btn-regionnewprev-form').hide();
		jQuery('.waiting-regionnewprev-form').show();
		
		return true;
	}else{
		jQuery('.preview_email_err_region').css("display","block");
		jQuery('.preview_email_err_region').html(err_Message);
		
		//HIDE PELASE WAIT MESSAGE SHOW  BUTTON
		jQuery('.sbmt-btn-regionnewprev-form').show();
		jQuery('.waiting-regionnewprev-form').hide();
		
		return false;
	}
	
	
	
	
});


function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
}

//SHOW / HIde Email Form on Template Preview
jQuery(".send-template-email").on("click",function(){
	
	//TOGGLE ANCHOR CLICK
	if(jQuery('.send-test-mail-region').is(':hidden')){
		
		jQuery('.send-test-mail-region').show( "slow", function() {
				jQuery('.send-template-email-click-region').removeClass('fa-chevron-down');
				jQuery('.send-template-email-click-region').addClass('fa-chevron-up');
		});
		
	}else{
		
		jQuery('.send-test-mail-region').hide( "slow", function() {
				jQuery('.send-template-email-click-region').removeClass('fa-chevron-up');
				jQuery('.send-template-email-click-region').addClass('fa-chevron-down');
		});
	
	}
});

jQuery(document).ready(function(){
	//Delivery Method Change Popup on LIsting Screen
	jQuery(document).on("click", "#delivery_method_change", function(e) {
		
		jQuery('.overlay-ajax-region').show();
		href = jQuery(this).attr('href');

		jQuery.ajax({
			url: href,
			success: function(events) {
				//REMOVE MODAL HEADER AND FOOTER
				jQuery('.modal-header').remove();
				jQuery('.modal-footer').remove();
				
				jQuery("#modal-body-popup").html(events);
				jQuery('#modal-popup-region').modal('show');
				// HIDE PLEASE WAIT MESSAGE
				jQuery('.overlay-ajax-region').hide();
			}
		});
		
		return false;
	});
	
});

//deleteConfirmation 
function deleteConfirmation(msg){

	// alert(newsLetterId);
    if (confirm(msg) == true) {
       return true;
    } 
	return false;
}


//Preview Newsletter on LIsting Screen
jQuery(document).on("click", "#preview-action-item", function(e) {
	
	//SHOW PLEASE WAIT MESSAGE
	jQuery('.overlay-ajax-region').show();
	href = jQuery(this).attr('href');

	jQuery.ajax({
		url: href,
		success: function(events) {
			
			//REMOVE MODAL HEADER AND FOOTER

			jQuery('.modal-header').remove();
			jQuery('.modal-footer').remove();


			jQuery("#modal-body-preview").html(events);
			jQuery('#preview-modal').modal('show');
			jQuery('.overlay-ajax-region').hide();
		}
	});
	
	return false;
});