// http://stackoverflow.com/questions/19970608/remove-class-when-browser-loads-at-width-at-less-than-700px
// Add class on window size dependant

function dothis(){
    var ww = jQuery(window).width();
    console.log(ww);
    if( ww < 768 ) {
        jQuery('body').removeClass();
        jQuery('body').addClass('mobile');
    }
    else if( ww == 768 ) {
        jQuery('body').removeClass();
        jQuery('body').addClass('ipad-portrait');
    }
    else if( ww == 1024 ) {
        jQuery('body').removeClass();
        jQuery('body').addClass('ipad-landscape');
    }
    else if( ww > 768 && ww < 992 ) {
        jQuery('body').removeClass();
        jQuery('body').addClass('sm');
    }
    else if( ww > 992 && ww < 1200 ) {
        jQuery('body').removeClass();
        jQuery('body').addClass('md');
    }
    else if ( ww > 1200 ) {
        jQuery('body').removeClass();
        jQuery('body').addClass('desktop');
    }
}

jQuery(document).ready(function(e) {
    // jQuery("html").niceScroll();
    dothis();
});

jQuery(window).resize(function(e) {
    dothis();
});



// ---------------------------------------------------------
// Back to Top 
// Source: http://www.templatemonster.com/demo/49003.html
// ---------------------------------------------------------
jQuery(window).scroll(function () {
    if (jQuery(this).scrollTop() > 100) {
        // jQuery('#back-top').fadeIn();
    } else {
        // jQuery('#back-top').fadeOut();
    }
});
jQuery('.back-to-top').click(function () {
    jQuery('body,html').stop(false, false).animate({
        scrollTop: 0
    }, 800);
    return false;
});


// Preloader
jQuery(window).load(function() { // makes sure the whole site is loaded
    //jQuery('#status').fadeOut(); // will first fade out the loading animation
    //jQuery('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
    //jQuery('body').delay(350).css({'overflow':'visible'});
})


$('#collapse-menu').click(function() {
    // Add or Remove Class on Body to Collapse Admin Menu
    $('body').toggleClass('folded');
})


// https://github.com/adamcoulombe/jquery.customSelect
$('select.customSelect').customSelect();

//Initiate Custom Date Picker - http://bootstrap-datepicker.readthedocs.org/en/release/
$('.input-group.date').datepicker({
    autoclose: true
});
$('.input-daterange').datepicker();







// ##################################
// INICTIALIZE CAROUSEL DETAILS PAGE
// #################################

        $(function() {
            var $carousel = $('#carousel'),
                $pager = $('#pager');

            function getCenterThumb() {
                var $visible = $pager.triggerHandler( 'currentVisible' ),
                    center = Math.floor($visible.length / 2);
                
                return center;
            }

            $carousel.carouFredSel({
                responsive: true,
                items: {
                    visible: 1,
                    width: 800,
                    height: (500/800*100) + '%'
                },
                scroll: {
                    fx: 'crossfade',
                    onBefore: function( data ) {
                        var src = data.items.visible.first().attr( 'src' );
                        src = src.split( '/large/' ).join( '/small/' );

                        $pager.trigger( 'slideTo', [ 'img[src="'+ src +'"]', -getCenterThumb() ] );
                        $pager.find( 'img' ).removeClass( 'selected' );
                    },
                    onAfter: function() {
                        $pager.find( 'img' ).eq( getCenterThumb() ).addClass( 'selected' );
                    }
                },
                prev: {
                    button: "#prev",
                    key: "left"
                },
                next: {
                    button: "#next",
                    key: "right"
                },  
            });
            $pager.carouFredSel({
                width: '100%',
                auto: false,
                height: 120,
                items: {
                    visible: 'odd'
                },
                onCreate: function() {
                    var center = getCenterThumb();
                    $pager.trigger( 'slideTo', [ -center, { duration: 0 } ] );
                    $pager.find( 'img' ).eq( center ).addClass( 'selected' );
                }
            });
            $pager.find( 'img' ).click(function() {
                var src = $(this).attr( 'src' );
                src = src.split( '/small/' ).join( '/large/' );
                $carousel.trigger( 'slideTo', [ 'img[src="'+ src +'"]' ] );
            });
        });





// Used on Event Page
$('.amenities .icon').tooltip();


