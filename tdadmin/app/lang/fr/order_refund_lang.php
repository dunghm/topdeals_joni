<?php
return array(
    'title'							=> 'Order refund',
	'select'						=> 'Please select a way of refunding',
	'refund_policy'					=> 'Refund policy',
	'reason'						=> 'Reason',
	'quantity'						=> 'Quantity',
	'deal'							=> 'Deal',
	'option'						=> 'Option',
	'total_amount'					=> 'Total Amount',
	'refund_amount'					=> 'Refund Amount'	
);
?>