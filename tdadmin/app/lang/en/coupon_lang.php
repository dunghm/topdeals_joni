<?php
return array(
    'coupon_validate_button'			=> 'Check and confirm',
	'coupon_validate_title'				=> 'Check and validate coupon',
	'coupon_validate_heading'			=> 'No. Enter the good and the corresponding PIN',
	'coupon_validate_no_good'			=> 'No. good',
	'coupon_validate_pin_good'			=> 'PIN good',
);
?>