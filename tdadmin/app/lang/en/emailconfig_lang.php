<?php
return array(
    'title'							=> 'Email Configuration',
	'event_key'						=> 'Event Key',
    'label' 						=> 'Label',
	'status'						=> 'Status',
	'confirm_message_enabled' 		=> 'Are you really want to Enabled this record?',
	'confirm_message_disabled'		=> 'Are you really want to Disabled this record?',
	'settings'						=> 'Settings',
);
?>