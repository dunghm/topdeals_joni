<!--button-->
<span class="bs-tooltip " data-toggle="tooltip" data-placement="top" >
 <a class="btn btn-default" href="#" data-toggle="modal" data-target="#confRModal">
  <i>{{trans('coupon_lang.coupon_validate_button')}}</i>
 </a>
</span>
<!--//button-->


<!-- Coupon validator Modal -->
<div class="modal fade" id="confRModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
<!--Header -->
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">{{trans('coupon_lang.coupon_validate_button')}}</h4>
		 </div>
<!--//Header -->		 
		 <div class="modal-body">
			  <div id="order-pay-dialog" class="order-pay-dialog-c" style="width:380px;">
			  <strong>{{trans('coupon_lang.coupon_validate_heading')}}</strong>
				
				
			  <p class="notice">{{trans('coupon_lang.coupon_validate_no_good')}}:
				<input id="coupon-dialog-input-id" value="" type="text" name="id" class="f-input" style="text-transform:uppercase;" maxLength="12" onkeyup="X.coupon.dialoginputkeyup(this);" />
			  </p> 
				
			  <p class="notice">{{trans('coupon_lang.coupon_validate_pin_good')}}:
				<input id="coupon-dialog-input-secret" value="" type="text" name="secret" style="text-transform:uppercase;" class="f-input" maxLength="8" onkeyup="X.coupon.dialoginputkeyup(this);" />
			  </p>
			  
			  
			  <p class="act">    
				<input id="coupon-dialog-consume-more" name="consume" class="formbutton" value="Validate"  ask=""  type="button" onclick="dialogconsume_more();"/>
			  </p>
			</div>     
		</div>
  </div>
</div>
<!--// Coupon validator Modal -->


<script>
//DEP 
function _dialogconsume_more()
{
	 var action_url = "<?php echo URL::to('partner/coupon/couponvalidator').'/'?>";
	
	var id 				= $('#coupon-dialog-input-id').val();
	var secret 		    = $('#coupon-dialog-input-secret').val();
	
		/*var queryData = '';
	
 queryData = "id="+id;
	queryData += "&secret="+encodeURIComponent(secret);
	alert(action_url); */
	
	var data = {     // create object
            id    		: id,
            secret      : secret
        }
	
	$.ajax({
		type:'POST',
		url: action_url,
		data: data,
		dataType: "html",
		async: false,
		cache: false,
		success: function(response)
		{
			response = jQuery.parseJSON(response);
		}
	});
}
</script>