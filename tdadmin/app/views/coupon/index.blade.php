<script src="<?=URL::asset('assets/datatable/jquery.dataTables.js');?>"></script>
<script src="<?=URL::asset('assets/yadcf/jquery_datatables_yadcf.js');?>"></script>
<style>
div.dataTables_length select { display: none; }
.dataTables_filter { display: none }
</style>

		
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<div class="portlet pd-30">
				<div class="page-heading">{{ trans('localization.CouponList') }}</div>
					
					<div class="ho-lala">		
						  {{ Datatable::table()
							  ->addColumn('No.', 'Deal Name', 'Buyer','Code', 'Duetime', 'State')
							  ->setUrl(route('api.coupons'))
							  ->render()
						  }}
					</div>
				
			</div>
		</div>
	</div>
</div
<!-- .container-fluid -->
</div>
<!-- #tt-body -->
</div>
<!-- #tt-content -->
<div id="tt-footer"> </div>

<script>
  $(document).ready(function(){
    var oTable = $('.table').dataTable().yadcf([
  	  	{column_number : 0, filter_type: "text"},
  	  	{column_number : 1, filter_type: "text"},
		{column_number : 2, filter_type: "text"},
		// {column_number : 4, filter_type: "text"},
		{column_number : 5, filter_type: "select"}
    ]);
	  var oSettings = oTable.fnSettings();
	  oSettings._iDisplayLength =  50;
	  oTable.fnDraw();
	  $('.dataTables_wrapper').find('select').val(50).text();
    $('table').removeClass().addClass('tt-table dataTable laravel-table').wrap('<div class="table-responsive"></div>');
    $('.dataTables_wrapper').wrap('<div class="portlet pd-30"></div>');
    $('.yadcf-filter').addClass('tt-form-control');
    $(".dataTables_length").addClass('clearfix');
	
	//
	selectValues = { "Y": "Used","N":"Usable","E":"Expired" };
    $.each(selectValues, function(key, value) {   
        $('#yadcf-filter--table-5')
            .append($("<option></option>")
            .attr("value",key)
            .text(value)); 
   });
  });
</script>