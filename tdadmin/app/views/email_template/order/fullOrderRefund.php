<?php
	#top deal url
	$topDealLink = TOPDEAL_URL_LINK;
	
	#get param 
	$orderId = $orderItemId =$refundQuantity=0;
	$refundPolicy ='';
	
	#get order detail
	$realname =$lastname=$payTime=$orderService='';
	$payId =$origin=$orderCredit=$orderCard=$totalAmount=$orderFare=0;
	
	#$orderItems 	= OrderItems:: getOrderItemsByOrderId($orderId);
	#$orders 		= Order::getOrderById($orderId);
	
	
	#refund policy
	if($orderRefundEmailParam['refundPolicy'])
	{
		$refundPolicy = $orderRefundEmailParam['refundPolicy'];
	}
	

	if(!empty($refundedOrders))
	{
		#realname
		if($refundedOrders['realname'])
		{
			$realname = $refundedOrders['realname'];
		}
		
		#lastname
		if($refundedOrders['lastname'])
		{
			$lastname = $refundedOrders['lastname'];
		}
		
		#pay_id
		if($refundedOrders['payId'])
		{
			$payId = $refundedOrders['payId'];
		}
		
		#origin
		if($refundedOrders['origin'])
		{
			$origin = moneyit($refundedOrders['origin']);
		}
		
		#fare
		 if($refundedOrders['orderFare'])
		{
			$orderFare = moneyit($refundedOrders['orderFare']);
		}
		
		#credit
		if($refundedOrders['orderCredit'])
		{
			$orderCredit = $refundedOrders['orderCredit'];
		}
		
		#card
		if($refundedOrders['orderCard'])
		{
			$orderCard = $refundedOrders['orderCard'];
		}
		
		#service
		if($refundedOrders['orderService'])
		{
			$orderService = $refundedOrders['orderService'];
		}
		
		#pay_time
		
		if($refundedOrders['payTime'])
		{
			$payTime = $refundedOrders['payTime'];
		}
		
		$totalAmount = moneyit(($origin));
		/* if($origin>$orderCard)
		{
			$totalAmount = moneyit(($origin-$orderCard));
		} */
		
		$total =0;
			
	}
	
?>
			<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			</head>

			<body>
			<table border="0" cellpadding="0" cellspacing="0" style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 14px; line-height: 130%; width: 100%;">
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" style="width: 580px; padding: 0 10px;" align="center">
					<tr>
						<td align="center">
							<h1 style="margin: 0;"><a href="#"><img src="<?php echo $topDealLink ?>/static/v4/img/logo.jpg" width="100" height="50" alt="Topdeal" style="vertical-align: middle; border: 0;" /></a></h1>
						</td>
					</tr>
					<tr>
						<td align="center" style="background-color: #ded7cf; height: 80px; vertical-align: middle;">
							<h2 style="margin: 0;"><a href="#"><img src="<?php echo $topDealLink ?>/static/v4/img/annulation-de-commande.jpg" width="370" height="42" alt="Annulation de commande" style="vertical-align: middle; border: 0;" /></a></h2>
						</td>
					</tr>
					<tr>
						<td style="padding: 15px 40px;">
							<h4 style="margin: 0 0 5px; font-weight: normal; font-size: 14px;">Cher/Ch�re <?php echo $realname." ".$lastname ?>, </h4>
							Ceci est un accus� de r�ception de votre demande d�annulation de commande suivante: 
						</td>
					</tr>
					
					<tr>
						<td style="padding: 0 40px;">
							<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" align="center">
								<tr>
									<td style="padding: 25px 10px; font-size: 12px; background-color: #f4f2f0;">
										N&deg;  commande:  <?php echo $payId; ?><br />
										Date de paiment: <?php echo $payTime; ?>
									</td>
								</tr>
								<tr>
									<td style="font-size: 12px; padding: 0 10px; background-color: #f4f2f0;">
										<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" align="center">
											<tr>
												<th style="text-align: left; border-bottom: #7f7f7f solid 1px;">Article</th>
												<th style="text-align: left; width: 130px; padding: 0 10px; border-bottom: #7f7f7f solid 1px;">Livraison</th>
												<th style="text-align: right; width: 80px;  border-bottom: #7f7f7f solid 1px;"> Total (CHF)</th>
											</tr>
											
<?php
										$sumCashBack = 0;
										foreach($refundedOrderItems as $orderItem)
										{
											$orderItemQuantity=$teamId =$optionId=$orderItemTotal=0;
											$teamTitle = $optionSummary = '';
											
											#quantity
											if($orderItem['orderItemQuantity'])
											{
												$orderItemQuantity =$orderItem['orderItemQuantity'];
											}
											
											#total
											if($orderItem['orderItemTotal'])
											{
												$orderItemTotal =moneyit($orderItem['orderItemTotal']);
											}
											
											#TeamId
											if($orderItem['teamId'])
											{
												$teamId =$orderItem['teamId'];
											}
											
											#optionId
											if($orderItem['optionId'])
											{
												$optionId =$orderItem['optionId'];
											}
											#team title
											if($orderItem['teamTitle'])
											{
												$teamTitle =$orderItem['teamTitle'];
											}
											
											#option Summary
											if($orderItem['optionSummary'])
											{
												$optionSummary =$orderItem['optionSummary'];
											}
											
											#cash back AMount
											if($orderItem['cashBack'])
											{
												$cashbackAmount =$orderItem['cashBack'];
												$sumCashBack+= $cashbackAmount;
											}
											#$sumCashBack+= $cashback_amount;
											
											$total += $orderItemTotal;
?>											

											<tr>
												<td style="text-align: left; padding: 10px 0; border-bottom: #cccccc solid 1px; vertical-align: top;">
												<?php echo $orderItemQuantity." x ".$teamTitle; 
												
												if($optionId)
												{
													echo "<br />".$optionSummary; 
												}
?>												
												<br /></td>
												<td style="text-align: left; padding: 10px; border-bottom: #cccccc solid 1px; vertical-align: top;">													
													<p style="margin: 0;">Retrait chez: <br />
													TopDeal<br />
													Rue du Centre 136<br />
													1025 St-Sulpice
													</p>
												</td>
												<td style="text-align: right; padding: 10px 0; border-bottom: #cccccc solid 1px; vertical-align: top;"><?php  echo $orderItemTotal; ?></td>
											</tr>
<?php
										}
?>										

									<tr>
												<td style="padding-bottom: 15px">&nbsp;</td>
												<td colspan="2" style="text-align: right; vertical-align: top; padding: 0 0 15px 10px;">
													<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" align="right">
														<tr>												
															<td style="text-align: right; vertical-align: top; padding: 10px 0 0;">
																Total : 
															</td>
															<td style="text-align: right; width: 80px; vertical-align: top; padding: 10px 0 0;"><?php echo $total; ?> </td>
														</tr>
<?php
														if($orderFare>0)
														{
?>
														<tr>												
															<td style="text-align: right; vertical-align: top; border-bottom: #cccccc solid 1px; padding: 0 0 10px;">
																Livraison: 
															</td>
															<td style="text-align: right; vertical-align: top; border-bottom: #cccccc solid 1px; padding: 0 0 10px;"><?php echo $orderFare; ?></td>
														</tr>
<?php
														}
													

														if($orderCard>0)
														{
?>	
														<tr>												
															<td style="text-align: right; vertical-align: top; border-bottom: #7f7f7f solid 1px; padding: 0 0 10px;">
																D�duction de code promo: 
															</td>
															<td style="text-align: right; vertical-align: top; border-bottom: #7f7f7f solid 1px; padding: 0 0 10px;">-<?php echo $orderCard; ?></td>
														</tr>
<?php
														}
														if($sumCashBack>0)
														{
															if($totalAmount>$sumCashBack)
															{
																$totalAmount = moneyit($totalAmount- $sumCashBack);
															}
?>
														<tr>												
															<td style="text-align: right; vertical-align: top; border-bottom: #7f7f7f solid 1px; padding: 0 0 10px;">
																Credit Back: 
															</td>
															<td style="text-align: right; vertical-align: top; border-bottom: #7f7f7f solid 1px; padding: 0 0 10px;">- <?php echo $sumCashBack; ?></td>
														</tr>
<?php
														
														}
?>														
														
														<tr>												
															<th style="text-align: right; vertical-align: top; padding: 10px 0;">
																Total � payer : 
															</th>
															<th style="text-align: right; vertical-align: top; padding: 10px 0;"><?php echo $totalAmount; ?></th>
														</tr>

													 </table>
												</td>
											</tr>								
										</table>
									</td>
								</tr>												
							</table>
						</td>
					</tr>
					<tr>
						<td style="padding: 25px 40px 0;">
							<p style="margin: 0; font-weight: 700;">
<?php

							if($refundPolicy == 'credit')
							{	

							echo "Le montant de CHF $totalAmount a �t� cr�dit� sur votre compte Topdeal";

							}
							else
							{							

							echo "Le montant de CHF $totalAmount a �t� cr�dit� sur carte de cr�dit";

							}	
?>
							</p>
							<p>Pour toute question relative � votre commande, n�hesitez pas � prendre contact avec le support TopDeal.ch en mentionnant le num�ro de votre commande � l�adresse suivante: <a style="color: #000000;" href="mailto: support@topdeal.ch">support@topdeal.ch</a>.</p>
						</td>
					</tr>	
					<tr>
						<td style="padding: 0 40px 15px;">
							<p style="margin: 0;">En esp�rant que nous aurons prochainement le plaisir de vous revoir.<br />
							Votre �quipe Topdeal.ch
							</p>
						</td>
					</tr>		
				</table>
			</td>
		</tr>
	</table>
</body>
</html>