<script src="<?php echo URL::asset('assets/datatable/jquery.dataTables.js');?>"></script>
<script src="<?php echo URL::asset('assets/yadcf/jquery_datatables_yadcf.js');?>"></script>
<style>
div.dataTables_length select { display: none; }
.dataTables_filter { display: none }
</style>

		
		
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<div class="portlet pd-30">
				<div class="page-heading">{{ trans('emailconfig_lang.title') }}</div>
					
					<div class="ho-lala">		
			
			
					  {{ Datatable::table()
						  ->addColumn(trans('emailconfig_lang.event_key'), trans('emailconfig_lang.label'), trans('emailconfig_lang.status'))
						  ->setUrl(route('api.emailConfiguration'))
						  ->setOptions('order', array([2 ,"asc"]))
						  ->render()
					  }}
					</div>
				
			</div>
		</div>
	</div>
</div>


<script>
  $(document).ready(function(){
    var oTable = $('.table').dataTable().yadcf([
  	  	{column_number : 0, filter_type: "text"},
  	  	{column_number : 1, filter_type: "text"},
		{column_number : 2, filter_type: "select"}
    ]);
	  var oSettings = oTable.fnSettings();
	  oSettings._iDisplayLength = 50;
	  oTable.fnDraw();
	  $('.dataTables_wrapper').find('select').val(50).text();
    $('table').removeClass().addClass('tt-table dataTable laravel-table').wrap('<div class="table-responsive"></div>');
    $('.dataTables_wrapper').wrap('<div class="portlet pd-30"></div>');
    $('.yadcf-filter').addClass('tt-form-control');
    $(".dataTables_length").addClass('clearfix');
	
	// set Options for Select tag
	selectValues = { "1": "Enabled", "0": "Disabled" };
    $.each(selectValues, function(key, value) {   
        $('#yadcf-filter--table-2')
            .append($("<option></option>")
            .attr("value",key)
            .text(value)); 
   });
  });
  
  $(window).load(function(){

   $('.dataTables_wrapper').on("click", ".enable_disable_emails", function() {

  var email_id = $(this).attr('id');
  var cell_txt = $(this).attr('txt');
  var action_url = "<?php echo URL::to('admin/email_config/enable_disable').'/'?>";
  
  if (cell_txt == 'Disabled'){
	cell_txt = 'Enabled';
	message ='<?php echo trans('emailconfig_lang.confirm_message_enabled');?>'
  } else {
	cell_txt = 'Disabled';
	message = '<?php echo trans('emailconfig_lang.confirm_message_disabled');?>';
  }
    
  if (confirm(message) ){   
	   $.ajax({
		type:'POST',
		url: action_url,
		data: {email_id:email_id},
		dataType: "html",
		async: false,
		cache: false,
		success: function(response) {
			if (response)
			{
				$('#span_'+email_id).html( cell_txt );
				$('#'+email_id).attr("txt", cell_txt);
			}
		 return false;   
		}
	   });
  }
  else{
	return false;
  }
   });
 });
</script>