<div id="login" class="cf">
    <div class="title">Turon Travel</div>
	    @if (Session::get("error"))
			<p class="alert">{{ Session::get("error") }}</p>
	    @endif
	    @if (Session::get("status"))
			<p class="alert">{{ Session::get("status") }}</p>
	    @endif
		{{ Form::open() }}
	        {{ Form::text('email', Input::old('email'), array('placeholder' => 'Username or Email', 'class' => 'input email')) }}
	        {{ Form::submit('Get New Password', ['class' => 'btn-submit', 'name' => 'reset', 'id' => 'tt-submit']) }}
	        <div class="row">
	            <div class="col-xs-6"> <a href="login" title="Sign In">← Back to Login</a> </div>
	        </div>
	    {{ Form::close() }}
    <script type="text/javascript">
function wp_attempt_focus(){
  setTimeout( function(){ try{
  d = document.getElementById('user_email');
  d.focus();
  d.select();
  } catch(e){}
  }, 200);
}

wp_attempt_focus();
if(typeof wpOnload=='function')wpOnload();
</script>
</div>