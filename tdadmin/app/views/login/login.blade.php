<div id="login" class="cf">

	 <div class="title">
		<?php
			$segment = Request::segment(1);
			if ( !empty($segment) && $segment == 'admin'){
		?>
			{{ trans('localization.login_page_admin_heading') }}
		<?php } else { ?>
	   
			{{ trans('localization.login_page_heading') }}
		<?php } ?>
	</div>
    @if(Session::has('message'))
		<p class="alert alert-danger" role="alert"">{{ Session::get('message') }}</p>
    @endif
    <?php
	 	$segment = Request::segment(1);
		if ( !empty($segment) && $segment == 'admin'){
    ?>
    {{ Form::open(array('url' => 'admin/login','data-toggle'=>"validator")) }}
    <?php } else { ?>
    {{ Form::open(array('url' => 'partner/login','data-toggle'=>"validator")) }}
    <?php } ?>
        {{ Form::text('username', Input::old('username'), array('placeholder' => trans('localization.username'), 'class' => 'input user', 'required')) }}
        @if ($errors->has('username'))<p class="help-block">{{ $errors->first('username') }}</p>@endif
        {{ Form::password('password', array('placeholder' => trans('localization.password'), 'class' => 'input pass',  'required')) }}
        @if ($errors->has('password'))<p class="help-block">{{ $errors->first('password') }}</p>@endif
        {{ Form::submit(trans('localization.sign_in'), ['class' => 'btn-submit', 'name' => 'tt-submit', 'id' => 'tt-submit']) }}

        <div class="row">
            <div class="col-xs-6 custom-rc">
                <!-- <input type="checkbox" name="rememberme" id="rememberme" value="forever"> -->
                <input type="checkbox" name="remember" id="rememberme">
                <label for="rememberme"><span></span>{{ trans('localization.rememberMe') }}</label>
            </div>
				<?php /*
            <div class="col-xs-6 text-right">
            	<!-- <a href="forgot-password.php" title="Password Lost and Found">Forgot Password?</a> -->
            	<a href="request" title="Password Lost and Found">{{ trans('localization.forgotPassword') }}</a>
            </div>
			*/?>
        </div>
    {{ Form::close() }}
    <script type="text/javascript">
function wp_attempt_focus(){
  setTimeout( function(){ try{
  d = document.getElementById('user_login');
  d.focus();
  d.select();
  } catch(e){}
  }, 200);
}

wp_attempt_focus();
if(typeof wpOnload=='function')wpOnload();
</script>
</div>