<script src="<?php echo URL::asset('assets/datatable/jquery.dataTables.js');?>"></script>
<script src="<?php echo URL::asset('assets/yadcf/jquery_datatables_yadcf.js');?>"></script>
<style>
div.dataTables_length select { display: none; }
.dataTables_filter { display: none }
</style>
@if(Session::has('successMessage'))
    <div class="alert alert-success">
        {{ Session::get('successMessage') }}
    </div>
@endif

@if(Session::has('errorMessage'))
    <div class="alert alert-danger">
        {{ Session::get('errorMessage') }}
    </div>
@endif		

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<div class="portlet pd-30">
				<div class="page-heading">{{ trans('supplieremail_lang.title') }}</div>
					
				<div class="ho-lala">
				  {{ Datatable::table()
					  ->addColumn(trans('supplieremail_lang.ref_no'), trans('supplieremail_lang.team_title'), trans('supplieremail_lang.option_title'), trans('supplieremail_lang.recipient'))
					  ->setUrl(route('api.supplierEmail'))
					  ->setOptions('order', array([0 ,"desc"]))
					  ->render()
				  }}
				</div>
			</div>
		</div>
	</div>
</div>


<script>
  $(document).ready(function(){
    var oTable = $('.table').dataTable().yadcf([
  	  	{column_number : 0, filter_type: "text"},
  	  	{column_number : 1, filter_type: "text"},
		{column_number : 2, filter_type: "text"},
		{column_number : 3, filter_type: "text"}
    ]);
	  var oSettings = oTable.fnSettings();
	  oSettings._iDisplayLength = 50;
	  oTable.fnDraw();
	  $('.dataTables_wrapper').find('select').val(50).text();
    $('table').removeClass().addClass('tt-table dataTable laravel-table').wrap('<div class="table-responsive"></div>');
    $('.dataTables_wrapper').wrap('<div class="portlet pd-30"></div>');
    $('.yadcf-filter').addClass('tt-form-control');
    $(".dataTables_length").addClass('clearfix');
  
	jQuery( ".head4" ).unbind();
  });

/*function deleteRecord(id)
{
	var action_url = "<?php echo URL::to('admin/warehousesectiondelete').'/' ?>"+id;
	var xConfirm = confirm("Are you sure, you want to delete this record?");
    if (xConfirm)
    {
        window.location = action_url;
    }
    return false;
}*/
</script>