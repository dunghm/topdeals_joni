<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<div class="portlet pd-30">
				<div class="page-heading">{{ trans('trigger_lang.add_event') }}</div>
				<form id="create-trigger-form" role="form" method="post" action="<?php echo URL::to('admin/updateEventSubmit'); ?>">
				  <div class="box-body">
					<div class="form-group">
					  <label for="exampleInputEmail1">{{ trans('trigger_lang.event_name') }}</label>
					  <input type="text" class="form-control"  id="event_name" name="event_name" placeholder="{{ trans('trigger_lang.event_name') }}" value="<?php echo $data['getEventData']->name;?>">
					</div>
					<div class="form-group">
					  <label for="exampleInputEmail1">{{ trans('trigger_lang.action_name') }}</label>
					  <select name="action_name"  id="action_name" class="form-control">
							<option value="">Please Select Action</option>
							<?php
								if(!empty($data['getActionList'])){
									foreach($data['getActionList'] as $getActionTypeRow ){
										
										$selected = "";
										if($getActionTypeRow->id == $data['getEventData']->action_id){
											$selected = "selected='selected'";
										}
							?>
										<option <?php echo $selected;?> value="<?php echo $getActionTypeRow->id;?>"><?php echo $getActionTypeRow->name;?></option>
							<?php
									}
								}
							?>
						</select>
					</div>
					<div class="form-group promo_code">
					  <label for="exampleInputEmail1">{{ trans('trigger_lang.enable_event') }}</label>
					  <select name="is_enable"  id="is_enable" class="form-control">
							<option value="0" <?php if($data['getEventData']->is_active==0){ echo "selected='selected'";} ?> >No</option>
							<option value="1" <?php if($data['getEventData']->is_active==1){ echo "selected='selected'";} ?>>Yes</option>
						</select>
					</div>
					 <div class="form-group">
						 <label for="exampleInputEmail1">{{ trans('trigger_lang.start_date_time') }} </label>
						 <div class='input-group date' id='datetimepicker1'>
							<input type='text' class="form-control" id="start_date_time" name="start_date_time" placeholder="{{ trans('trigger_lang.start_date_time') }}" value=" ">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
					<div class="form-group ">
					  <label for="exampleInputEmail1">{{ trans('trigger_lang.end_date_time') }}</label>
					   <div class='input-group date' id='end_time_picker'>
							<input type='text' class="form-control" id="end_date_time" name="end_date_time" placeholder="{{ trans('trigger_lang.end_date_time') }}" value=" ">
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
						<input type="hidden" name="event_id" value="<?php echo $data['getEventData']->id;?>"/>
					</div>

					</div>
					
					 <div class="box-footer">
					<span class='frm-btn-region'>
						<button id="trigger-action-btn" type="submit" class="btn btn-primary">{{ trans('warehouse_lang.submit') }}</button>
					</span>
					<span class='waiting-region' style='display:none;'>
						Please Wait...
					</span>
                  </div>
				  <div class="alert alert-danger" role="alert" id='err-region' style="display:none;"></div>
				  </div>

				 
				</form>
			</div>
		</div>
	</div>
</div>
<?php
	if($data['getEventData']->start_time>0 && !empty($data['getEventData']->start_time) && $data['getEventData']->start_time != NULL){
?>
<script>
 jQuery(function () {
			jQuery('#datetimepicker1').datetimepicker({
					 defaultDate: "<?php echo date('m/d/Y H:i A', $data['getEventData']->start_time);?>"
                });
 });
</script>
<?php
	}else{
?>
<script>
 jQuery(function () {
	jQuery('#datetimepicker1').datetimepicker();
 });
</script>
<?php
	}
?>

<?php
	if($data['getEventData']->end_time > 0 && !empty($data['getEventData']->end_time) && $data['getEventData']->end_time != NULL){
?>
<script>
 jQuery(function () {
			jQuery('#end_time_picker').datetimepicker({
					 defaultDate: "<?php echo date('m/d/Y H:i A', $data['getEventData']->end_time);?>"
                });
 });
</script>
<?php
	}else{
?>
<script>
 jQuery(function () {
	jQuery('#end_time_picker').datetimepicker();
 });
</script>
<?php
	}
?>
        <script type="text/javascript">
            jQuery(function () {
				
				
				//FORM VALIDATE
				jQuery('#create-trigger-form').on('submit',function(){
					
					var event_name 	= jQuery.trim(jQuery('#event_name').val());
					var action_name = jQuery.trim(jQuery('#action_name').val());
					var is_enable 	= jQuery.trim(jQuery('#is_enable').val());
					var start_time 	= jQuery.trim(jQuery('#start_date_time').val());
					var end_time 	= jQuery.trim(jQuery('#end_date_time').val());
					
					
					var errMessage	=	"";
			
					if(event_name==""){
						errMessage += " - Event Name is required. <br/>";
					}
					
					if(action_name==""){
						errMessage += " - Action Name is required. <br/>";
					}
					
					if(is_enable==""){
						errMessage += " - Please Select Trigger Enable Option. <br/>";
					}
					
					if(end_time != ""){
						
						var start_timeVal 	= Date.parse(start_time);
						var end_timeVal	 	= Date.parse(end_time);
						
						if(start_time!=""){
							if(start_timeVal >= end_timeVal){
								errMessage += " - Please Select Valid Date Range. <br/>";
							}
						}else{
							errMessage += " - Please Select Start Date. <br/>";
						}
					}
					
					
					if(errMessage != ""){
						jQuery('#err-region').html(errMessage);
						jQuery('#err-region').css('display','block');
						return false;
					}else{
						jQuery('#err-region').html('');
						jQuery('#err-region').css('display','none');
						
						jQuery('.frm-btn-region').css('display','none');
						jQuery('.waiting-region').css('display','block');
						
						return true;
					}
				});
            });
        </script>