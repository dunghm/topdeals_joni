<style>
.promo_code{display:none;}
.promo_code_percent{display:none;}
</style>
<?php
		$promo_code_attribute	=	unserialize($data['getActionData']->promo_code_attribute);
		 $promo_code_type	=	"-";
		 $promo_amount		=	"-";
		 $promo_percent		=	"-";
		 $minimum_order		=	"-";
		 $coupon_validity	=	"-";
		 
		 if(isset($promo_code_attribute['promo_code_type'])){
			$promo_code_type	=	$promo_code_attribute['promo_code_type'];
		 }
		 
		 if(isset($promo_code_attribute['promo_amount'])){
			$promo_amount	=	$promo_code_attribute['promo_amount'];
		 }
		 
		 if(isset($promo_code_attribute['promo_percent'])){
			$promo_percent	=	$promo_code_attribute['promo_percent'];
		 }
		 
		 if(isset($promo_code_attribute['minimum_order'])){
			$minimum_order	=	$promo_code_attribute['minimum_order'];
		 }
		 
		 if(isset($promo_code_attribute['coupon_validity'])){
			$coupon_validity	=	$promo_code_attribute['coupon_validity'];
		 }
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<div class="portlet pd-30">
				<div class="page-heading">{{ trans('trigger_lang.update_action') }}</div>
				<form id="create-action-form" role="form" method="post" action="<?php echo URL::to('admin/updateActionSubmit'); ?>">
				  <div class="box-body">
					<div class="form-group">
					  <label for="exampleInputEmail1">{{ trans('trigger_lang.action_name') }}</label>
					  <input type="text" class="form-control"  id="action_name" name="action_name" placeholder="{{ trans('trigger_lang.action_name') }}" value="<?php echo $data['getActionData']->name;?>">
					</div>
					<div class="form-group">
					  <label for="exampleInputEmail1">{{ trans('trigger_lang.action_type') }}</label>
					  <select name="action_type"  id="action_type" class="form-control">
							<?php
								if(!empty($data['getActionType'])){
									foreach($data['getActionType'] as $getActionTypeRow ){
										
										$selected = "";
										if($data['getActionData']->type_id ==  $getActionTypeRow->id){
											$selected = "selected='selected'";
										}
							?>
										<option <?php echo $selected;?> value="<?php echo $getActionTypeRow->id;?>"><?php echo $getActionTypeRow->name;?></option>
							<?php
									}
								}
							?>
						</select>
					</div>
					<div class="form-group promo_code">
					  <label for="exampleInputEmail1">{{ trans('trigger_lang.promo_code_type') }}</label>
					  <select name="promo_code_type"  id="promo_code_type" class="form-control">
							<option value="face_value" <?php if($promo_code_type=='face_value'){ echo "selected='selected'";}?>>Face Value</option>
							<option value="percentage" <?php if($promo_code_type=='percentage'){ echo "selected='selected'";}?>>Discount %age</option>
						</select>
					</div>
					<div class="form-group promo_code promo_code_face">
					  <label for="exampleInputEmail1">{{ trans('trigger_lang.promo_code_face_value') }}</label>
					  <input type="text" class="form-control"  id="promo_code_face_value" name="promo_code_face_value" placeholder="{{ trans('trigger_lang.promo_code_face_value') }}" value="<?php echo $promo_amount;?>">
					</div>
					<div class="form-group promo_code_percent">
					  <label for="exampleInputEmail1">{{ trans('trigger_lang.promo_code_percentage') }}</label>
					  <input type="text" class="form-control"  id="promo_code_percentage" name="promo_code_percentage" placeholder="{{ trans('trigger_lang.promo_code_percentage') }}" value="<?php echo $promo_percent;?>">
					</div>
					<div class="form-group promo_code">
					  <label for="exampleInputEmail1">{{ trans('trigger_lang.minimum_order') }}</label>
					  <input type="text" class="form-control"  id="minimum_order" name="minimum_order" placeholder="{{ trans('trigger_lang.minimum_order') }}" value="<?php echo $minimum_order;?>">
					</div>
					<div class="form-group promo_code">
					  <label for="exampleInputEmail1">{{ trans('trigger_lang.coupon_validity') }}</label>
					  <input type="text" class="form-control"  id="coupon_validity" name="coupon_validity" placeholder="{{ trans('trigger_lang.coupon_validity') }}" value="<?php echo $coupon_validity;?>">
					  <p class="help-block">Hint : Coupon validity number in hours</p>
					</div>
					<div class="form-group">
					  <label for="exampleInputEmail1">{{ trans('trigger_lang.email_subject') }}</label>
					  <input type="text" class="form-control"  id="email_subject" name="email_subject" placeholder="{{ trans('trigger_lang.email_subject') }}" value="<?php echo $data['getActionData']->subject;?>">
					</div>
					<div class="form-group">
					  <label for="exampleInputEmail1">{{ trans('trigger_lang.email_body') }}</label>
					  <textarea class="form-control"  id="email_body" name="email_body" placeholder="{{ trans('trigger_lang.email_body') }}" ><?php echo $data['getActionData']->email_body;?></textarea>
						<p class="help-block">
							<a id="preview-action-item" href='<?php echo URL::to('admin/templatePattern').'?'.time();?>'>Hint : You can use system pattern in email template for Customer Name etc. </a>
						</p>
					</div>
				  </div>

				  <div class="box-footer">
					<span class='frm-btn-region'>
						<input type="hidden" name="action_id" value="<?php echo $data['getActionData']->id;?>"/>
						<button id="trigger-action-btn" type="submit" class="btn btn-primary">{{ trans('warehouse_lang.submit') }}</button>
					</span>
					<span class='waiting-region' style='display:none;'>
						Please Wait...
					</span>
                  </div>
				  <div class="alert alert-danger" role="alert" id='err-region' style="display:none;"></div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	tinymce.init({
		selector: "textarea",
		theme: "modern",
		relative_urls: false,
		remove_script_host: false,
		plugins: [
			"advlist autolink lists link image charmap print preview hr anchor pagebreak",
			"searchreplace wordcount visualblocks visualchars code fullscreen",
			"insertdatetime media nonbreaking save table contextmenu directionality",
			"emoticons template paste textcolor colorpicker textpattern"
		],
		toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
		toolbar2: "print preview media | forecolor backcolor emoticons",
		image_advtab: true,
		templates: [
			{title: "Template 1", description: "Sample Template 1", url: "<?php echo URL::to('assets/trigger/template/template1/news_trigger.php'); ?>"} ,
			{title: 'Test template 2', content: 'Test 2'}
		]
	});
	
	//SHOW PROMO CODE REGION
	jQuery("#action_type").on("change",function(){
		action_type();
	});
	
	function action_type(){
		var action_type	=	jQuery.trim(jQuery('#action_type').val());
		
		if(action_type=='2'){
			jQuery('.promo_code').css('display','block');
		}else{
			jQuery('.promo_code').css('display','none');
		}
		
		jQuery('.promo_code_percent').css('display','none');
	}
	
	//SHOW PROMO CODE Percentage/Full Face Field
	jQuery("#promo_code_type").on("change",function(){
		promo_code_type();
	});
	
	function promo_code_type(){
		
		var promo_code_type	=	jQuery.trim(jQuery('#promo_code_type').val());
		
		if(promo_code_type=='face_value'){
			jQuery('.promo_code_face').css('display','block');
			jQuery('.promo_code_percent').css('display','none');
		}else{
			jQuery('.promo_code_face').css('display','none');
			jQuery('.promo_code_percent').css('display','block');
		}
	}
	//SUBMIT TRIIGER ACTION
	jQuery(document).ready(function(){
		
		action_type();
		promo_code_type();
		
		jQuery('#create-action-form').on('submit',function(){
			
			
			var action_name				=	jQuery.trim(jQuery('#action_name').val());
			var action_type				=	jQuery.trim(jQuery('#action_type').val());
			var email_subject			=	jQuery.trim(jQuery('#email_subject').val());
			//VALIDATE TINYMCE
			tinyMCE.triggerSave();
			var email_body				=	jQuery.trim(jQuery('#email_body').val());
			
			var promo_code_type			=	jQuery.trim(jQuery('#promo_code_type').val());
			var promo_code_face_value	=	jQuery.trim(jQuery('#promo_code_face_value').val());
			var promo_code_percentage	=	jQuery.trim(jQuery('#promo_code_percentage').val());
			var minimum_order			=	jQuery.trim(jQuery('#minimum_order').val());
			var coupon_validity			=	jQuery.trim(jQuery('#coupon_validity').val());
			
			
			var errMessage	=	"";
			
			if(action_name==""){
				errMessage += " - Action Name is required. <br/>";
			}
			
			if(action_type==""){
				errMessage += " - Action Type is required. <br/>";
			}
			
			if(email_subject==""){
				errMessage += " - Email Subject is required. <br/>";
			}
			
			if(email_body == ""){
				errMessage += " - Email Body is required. <br/>";
			}
			
			
			if(action_type=="2"){
				
				if(promo_code_type==""){
					errMessage += " - Promo Code Type is required. <br/>";
				}
				
				if(promo_code_type=="face_value"){
					
					if(promo_code_face_value==""){
						errMessage += " - Promo Code Face is required. <br/>";
					}
					
					if(promo_code_face_value!=""){
						if(!jQuery.isNumeric(promo_code_face_value) || promo_code_face_value<=0){
							errMessage += " - Promo Code Face should be valid. <br/>";
						}
					}
				}
				
				if(promo_code_type=="percentage"){
					if(promo_code_percentage==""){
						errMessage += " - Promo Card Discount %. <br/>";
					}
					
					if(promo_code_percentage!=""){
						if(!jQuery.isNumeric(promo_code_percentage) || promo_code_percentage<=0){
							errMessage += " - Promo Code Discount should be valid. <br/>";
						}
					}
				}
				
				if(minimum_order!=""){
					if(!jQuery.isNumeric(minimum_order)){
						errMessage += " - Minimum Order Number should be valid. <br/>";
					}
				}
				
				if(coupon_validity!=""){
					if(!jQuery.isNumeric(coupon_validity)){
						errMessage += " - Coupon Validity should be valid. <br/>";
					}
				}
			}

			if(errMessage != ""){
				jQuery('#err-region').html(errMessage);
				jQuery('#err-region').css('display','block');
				return false;
			}else{
				jQuery('#err-region').html('');
				jQuery('#err-region').css('display','none');
				
				jQuery('.frm-btn-region').css('display','none');
				jQuery('.waiting-region').css('display','block');
				
				return true;
			}
		});
	});
</script>

