<div >
  <div class="panel panel-info">
    <div class="panel-heading">
		{{ trans('trigger_lang.view_action') }}
	    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	</div>
    <div class="panel-body">
			<div class="portlet pd-30">
				<div class="page-heading"></div>
				  <table class="table table-striped">
					<tbody>
						<tr>
						  <td width="30%">
							<label for="exampleInputEmail1">{{ trans('trigger_lang.action_name') }}</label>
						  </td>
						  <td width="70%">
							<?php echo $data['getActionData']->name;?>
						  </td>
						</tr>
						<tr>
							<td>
								<label for="exampleInputEmail1">{{ trans('trigger_lang.action_type') }}</label>
							</td>
							<td>
								<?php
									if(!empty($data['getActionType'])){
										foreach($data['getActionType'] as $getActionTypeRow ){
											
											$value = "";
											if($data['getActionData']->type_id ==  $getActionTypeRow->id){
												$value =  $getActionTypeRow->name;
												break;
											}
										}
										
										echo $value;
									}
								?>
						
							</td>
						</tr>
						<?php
						 if($data['getActionData']->type_id==2){
							 
							 $promo_code_attribute	=	unserialize($data['getActionData']->promo_code_attribute);
							 
							 $promo_code_type	=	"-";
							 $promo_amount		=	"-";
							 $promo_percent		=	"-";
							 $minimum_order		=	"-";
							 $coupon_validity	=	"-";
							 
							 if(isset($promo_code_attribute['promo_code_type'])){
								$promo_code_type	=	$promo_code_attribute['promo_code_type'];
							 }
							 
							 if(isset($promo_code_attribute['promo_amount'])){
								$promo_amount	=	$promo_code_attribute['promo_amount'];
							 }
							 
							 if(isset($promo_code_attribute['promo_percent'])){
								$promo_percent	=	$promo_code_attribute['promo_percent'];
							 }
							 
							 if(isset($promo_code_attribute['minimum_order'])){
								$minimum_order	=	$promo_code_attribute['minimum_order'];
							 }
							 
							 if(isset($promo_code_attribute['coupon_validity'])){
								$coupon_validity	=	$promo_code_attribute['coupon_validity'];
							 }
							 
						?>
							<tr>
								<td>
									<label for="exampleInputEmail1">{{ trans('trigger_lang.promo_code_type') }}</label>
								</td>
								<td>
								 <?php if($promo_code_type=='face_value'){ ?>
									Face Value
								 <?php }elseif($promo_code_type=='percentage'){ ?>
								 Discount %age
								 <?php } ?>
								 </td>
							</tr>
							 <?php if($data['getActionData']->promo_code_type=='face_value'){ ?>
							<tr>
								<td>
									<label for="exampleInputEmail1">{{ trans('trigger_lang.promo_code_face_value') }}</label>
								</td>
								<td>
									<?php echo $promo_amount;?>
								</td>
							</tr>
							<?php }elseif($data['getActionData']->promo_code_type=='percentage'){ ?>
							<tr>
								<td>
									<label for="exampleInputEmail1">{{ trans('trigger_lang.promo_code_percentage') }}</label>
								</td>
								<td>
									<?php echo $promo_percent;?>
								</td>
							</tr>
							 <?php } ?>
							<tr>
								<td>
									<label for="exampleInputEmail1">{{ trans('trigger_lang.minimum_order') }}</label>
								</td>
								<td>
									<?php echo $minimum_order;?>
								</td>
							</tr>
							<tr>
								<td>
									<label for="exampleInputEmail1">{{ trans('trigger_lang.coupon_validity') }}</label>
								</td>
								<td>
									<?php echo $coupon_validity;?>
								</td>
							</tr>
						<?php
						 }
						?>
						<tr>
							<td>
								<label for="exampleInputEmail1">{{ trans('trigger_lang.email_subject') }}</label>
							</td>
							<td>
								<?php echo $data['getActionData']->subject;?>
							</td>
						</tr>
						<tr>
							<td colspan="2">
							  <label for="exampleInputEmail1">{{ trans('trigger_lang.email_body') }}</label>
							</td>
						</tr>
						<tr>
							<td colspan="2" style="border:1px solid #C9D4EF; padding:15px;">
							  <?php echo $data['getActionData']->email_body;?>
							</td>
						</tr>
						</tbody>
					</table>
					
		  </div>
		</div>
	</div>
</div>