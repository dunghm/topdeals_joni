<div >
  <div class="panel panel-info">
    <div class="panel-heading">
		{{ trans('trigger_lang.mapping_convention') }}
	    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	</div>
    <div class="panel-body">
			<div class="portlet pd-30">
				<div class="page-heading"></div>
				   <table class="table table-striped">
						<thead>
							<tr>
								<th>Mapping Conventions</th>
								<th>Pattern</th>
							</tr>
						</thead>
						<tbody>
							<?php
								if(!empty($data['templatePattern'])){
									foreach($data['templatePattern'] as $key=>$value ){
							?>
										<tr>
										  <td width="30%">
											<?php echo $value?>
										  </td>
										  <td width="70%">
											<?php echo $key?>
										  </td>
										</tr>
							<?php
									}
								}
							?>
						</tbody>
					</table>
						
		  </div>
		</div>
	</div>
</div>