<script src="<?php echo URL::asset('assets/datatable/jquery.dataTables.js');?>"></script>
<script src="<?php echo URL::asset('assets/yadcf/jquery_datatables_yadcf.js');?>"></script>


<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<div class="portlet pd-30">
				<div class="page-heading">
					{{ trans('trigger_lang.trigger_action') }}
					<span style="float:right;">
						<a type="button"  href="<?php echo URL::to('/').'/admin/addNewAction'; ?>" class="btn btn-primary ">Add New Action</a>
					</span>
				</div>
					
				<div class="ho-lala">
				  {{ Datatable::table()
					  ->addColumn(trans('trigger_lang.action_id'), trans('trigger_lang.action_name'), trans('trigger_lang.action_type'), trans('trigger_lang.operation'))
					  ->setUrl(route('api.triggerAction'))
					  ->setOptions('order', array([0 ,"desc"]))
					  ->render()
				  }}
				</div>
			</div>
		</div>
	</div>
</div>


<script>
  $(document).ready(function(){
    var oTable = $('.table').dataTable().yadcf([
  	  	{column_number : 0, filter_type: "text"},
  	  	{column_number : 1, filter_type: "text"},
		{column_number : 2, filter_type: "text"}
    ]);
	  var oSettings = oTable.fnSettings();
	  oSettings._iDisplayLength = 50;
	  oTable.fnDraw();
	  $('.dataTables_wrapper').find('select').val(50).text();
    $('table').removeClass().addClass('tt-table dataTable laravel-table').wrap('<div class="table-responsive"></div>');
    $('.dataTables_wrapper').wrap('<div class="portlet pd-30"></div>');
    $('.yadcf-filter').addClass('tt-form-control');
    $(".dataTables_length").addClass('clearfix');
  });
  
</script>