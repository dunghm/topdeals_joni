<div >
<style>

.bs-example::after {
    color: #959595;
    content: "Subject : <?php echo $data['getEventActionData']->email_subject;?>";
    font-size: 12px;
    font-weight: 700;
    left: 15px;
    letter-spacing: 1px;
    position: absolute;
    text-transform: uppercase;
    top: 15px;
}

.bs-example {
    background-color: #fff;
    border-radius: 4px 4px 0 0;
    border-color: #e5e5e5 #eee #eee;
    border-style: solid;
    border-width: 1px 0;
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.05) inset;
    margin: 0 -15px 15px;
    padding: 45px 15px 15px;
    position: relative;
}

</style>
  <div class="panel panel-info">
    <div class="panel-heading">
		{{ trans('trigger_lang.sample_email') }}
	    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	</div>
    <div class="panel-body">
			<div class="portlet pd-30">
				<div class="page-heading"></div>
				 <div class="box-body">
					<div class="form-group">
					<form id="trigger_sample_email" method="post" action="<?php echo URL::to('admin/triggerEmailSend'); ?>">
						<input type="text" name="email_sent_to" style="width:200px; display:inline !important;" class="form-control" placeholder="Enter email" id="email_sent_to"/>
						<input type="hidden" value="<?php echo $data['getEventActionData']->event_id; ?>" name="event_id" />
						<span class='sbmt-btn-regionnewprev-form'>
							<input type="submit" class="btn btn-primary" value="Send Email" name="send_email" id="send_email" />
						</span>
						<span class='waiting-regionnewprev-form' style='display:none;'>
							Please Wait...
						</span>
						<span class="preview_email_err_region" style="display:none; color:red;"></span>
						<div style="color:grey; font-size:11px; padding:10px;">Note : For Multiple Recipient Use ',' Seprator. For Example :  abx@xyz.com , def@xyz.com </div>
					</form>
					
				  </div>
				  <div data-example-id="simple-jumbotron" class="bs-example">
					<hr/>
					<div class="">
						<?php
							echo $data['getEventActionData']->email_body;
						?>
						
					</div>
				  </div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
jQuery(document).ready(function(){
	jQuery('#trigger_sample_email').on('submit',function(){
		
		var email_sent_to = jQuery.trim(jQuery('#email_sent_to').val());
		
		var err_Message	=	"";
		
		if(email_sent_to==""){
			err_Message += "Please Enter Email Address";
		}
		
		if(err_Message==""){
			jQuery('.preview_email_err_region').css("display","none");
			jQuery('.preview_email_err_region').html("");
			
			//HIDE BUTTON SHOW PELASE WAIT MESSAGE
			jQuery('.sbmt-btn-regionnewprev-form').hide();
			jQuery('.waiting-regionnewprev-form').show();
			
			return true;
		}else{
			jQuery('.preview_email_err_region').css("display","block");
			jQuery('.preview_email_err_region').html(err_Message);
			
			//HIDE PELASE WAIT MESSAGE SHOW  BUTTON
			jQuery('.sbmt-btn-regionnewprev-form').show();
			jQuery('.waiting-regionnewprev-form').hide();
			
			return false;
		}
		
		
		
		
	});
});
</script>