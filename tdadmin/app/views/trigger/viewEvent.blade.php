<div >
  <div class="panel panel-info">
    <div class="panel-heading">
		{{ trans('trigger_lang.view_event') }}
	    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	</div>
    <div class="panel-body">
			<div class="portlet pd-30">
				<div class="page-heading"></div>
				  <table class="table table-striped">
					<tbody>
						<tr>
						  <td width="30%">
							<label for="exampleInputEmail1">{{ trans('trigger_lang.event_name') }}</label>
						  </td>
						  <td width="70%">
							<?php echo $data['getEventData']->name;?>
						  </td>
						</tr>
						<tr>
							<td>
								<label for="exampleInputEmail1">{{ trans('trigger_lang.action_name') }}</label>
							</td>
							<td>
								<?php
									$value = "";
									if(!empty($data['getActionList'])){
										foreach($data['getActionList'] as $getActionTypeRow ){
											
											
											if($data['getEventData']->action_id ==  $getActionTypeRow->id){
												$value =  $getActionTypeRow->name;
												break;
											}
										}
									}
									echo $value;
								?>
						
							</td>
						</tr>
						<tr>
							<td>
								<label for="exampleInputEmail1">{{ trans('trigger_lang.enable_event') }}</label>
							</td>
							<td>
							 <?php if($data['getEventData']->is_active==1){ ?>
								Yes
							 <?php }else{ ?>
								No
							 <?php } ?>
							 </td>
						</tr>
						<tr>
							<td>
								<label for="exampleInputEmail1">{{ trans('trigger_lang.start_date_time') }}</label>
							</td>
							<td>
								<?php
									$startTime	=	"N/A";
									if ($data['getEventData']->start_time > 0 && !empty($data['getEventData']->start_time) && $data['getEventData']->start_time != NULL){
										$startTime	=	date('m/d/Y H:i A', $data['getEventData']->start_time);
									}
						
									echo $startTime;
								?>
							</td>
						</tr>
						<tr>
							<td>
								<label for="exampleInputEmail1">{{ trans('trigger_lang.end_date_time') }}</label>
							</td>
							<td>
								<?php 
									
									$end_time	=	"N/A";
									if ($data['getEventData']->end_time > 0 && !empty($data['getEventData']->end_time) && $data['getEventData']->end_time != NULL){
										$end_time	=	date('m/d/Y H:i A', $data['getEventData']->end_time);
									}
						
									echo $end_time;
								?>
							</td>
						</tr>
						</tbody>
					</table>
					
		  </div>
		</div>
	</div>
</div>