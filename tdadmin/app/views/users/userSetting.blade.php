<?php //var_debug($user_setting); exit;?>

@include ('layout/greeting')

  <div id="tt-body">

    <div class="container-fluid">

      <div class="row">
        <div class="col-xs-12">
          <div class="tt-page-header">
            <div class="row">
              <div class="col-sm-6 title">
                  Edit Setting
              </div>
            </div>
          </div><!-- .tt-page-header -->
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          
          <div class="portlet widget-location">
            <div class="portlet-content-wrap">
                <form class="form-horizontal tt-form" role="form" action="<?=URL::to('user-update-setting'); ?>" method="post">
              <div class="portlet-content">
                <h3>Application Setting</h3>
                <div class="form-group">
                  <label for="result_page" class="col-sm-3 control-label">Results per page</label>
                  <div class="col-sm-5">
                      <select name="result_page" id="result_page" class="selectpicker show-tick show-menu-arrow"  data-width="100%">
                          <option value="50" <?php if($user_setting[0]->perpage == '50'){echo 'selected="selected"';}?>>50</option>
                          <option value="100" <?php if($user_setting[0]->perpage == '100'){echo 'selected="selected"';}?>>100</option>
                          <option value="150" <?php if($user_setting[0]->perpage == '150'){echo 'selected="selected"';}?>>150</option>
                          <option value="200" <?php if($user_setting[0]->perpage == '200'){echo 'selected="selected"';}?>>200</option>
                          <option value="250" <?php if($user_setting[0]->perpage == '250'){echo 'selected="selected"';}?>>250</option>
                      </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="new_tab" class="col-sm-3 control-label">Open shortcuts in new tab</label>
                  <div class="col-sm-5 custom-rc">
                      <input type="checkbox" class="tt-form-control" name="new_tab" id="new_tab" value="1" placeholder="" tabindex="2" <?php if($user_setting[0]->newtab == '1'){echo 'checked="checked"';}?>>
                      <label for="new_tab"><span></span>New Tab</label>
                  </div>
                </div>
                <div class="form-group">
                  <label for="new_tab" class="col-sm-3 control-label">Credit card days</label>
                  <div class="col-sm-5 custom-rc">
                      <input type="text" class="tt-form-control" name="cc_days" id="cc_days" value="<?=isset($user_setting[0]->cc_days) ? $user_setting[0]->cc_days : '30'?>" placeholder="" tabindex="3">
                  </div>
                </div>
                <!--<div class="form-group">
                  <label for="warnings" class="col-sm-3 control-label">Show warnings on login</label>
                  <div class="col-sm-5 custom-rc">
                      <input type="checkbox" class="tt-form-control" name="warnings" id="warnings" value="1" placeholder="" tabindex="2" <?php #if($user_setting[0]->show_warning == '1'){echo 'checked="checked"';}?>>
                      <label for="warnings"><span></span>Warnings</label>
                  </div>
                </div>
              -->
              
              </div><!-- .portlet-content -->
              
              <div class="portlet-footer">
                  <button type="submit" class="tt-btn tt-btn-primary">Save</button>
              </div>
              </form>
            </div><!-- .portlet-content-wrap -->
          </div><!-- .portlet .widget-location -->

        </div><!-- .col-sm-12 -->
      </div><!-- .row -->

    </div><!-- .container-fluid -->

  </div><!-- #tt-body -->

</div><!-- #tt-content -->

<div id="tt-footer">
</div>