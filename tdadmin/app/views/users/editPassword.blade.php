@include ('layout/greeting')
  <div id="tt-body">

    <div class="container-fluid">

      <div class="row">
        <div class="col-xs-12">
          <div class="tt-page-header">
            <div class="row">
              <div class="col-sm-6 title">
				{{ trans('localization.loginInfo') }}
              </div>
            </div>
          </div><!-- .tt-page-header -->
        </div>
      </div>
	<?php
		//IF SESSION MENTAIN THEN REDIRECTED TO ADMIN/PARTNER DASHBOARD
		$userType = getCurrentUserRole();
	?>
      <div class="row">
        <div class="col-sm-12">

          <div class="portlet widget-location">
            <div class="portlet-content-wrap">
              <form class="form-horizontal tt-form" role="form" name="userProfile" id="userProfile" method="post" action="{{ URL::to($userType.'/edit-update-password') }}" enctype="multipart/form-data">
              <div class="portlet-content">
                <h3>1. {{ trans('localization.loginInfo') }}</h3>
                <div class="form-group">
                  <label for="username" class="col-sm-3 control-label">{{ trans('localization.username') }}</label>
                  <div class="col-sm-5">
                    <input type="text" class="tt-form-control" readonly="true" id="username_readonly" name="username_readonly" value="<?php echo $user->username; ?>" tabindex="1">
                  </div>
                  <div class="col-sm-4">
                    
                  </div>
                </div>
				<div class="form-group">
                  <label for="password" class="col-sm-3 control-label">{{ trans('localization.CurrentPass') }}</label>
                  <div class="col-sm-5">
                    <input type="password" class="tt-form-control" id="password" name="password" tabindex="1">
                  </div>
                  <div class="col-sm-4">
                    
                  </div>
                </div>
                <div class="form-group">
                  <label for="newpassword" class="col-sm-3 control-label">{{ trans('localization.NewPass') }}</label>
                  <div class="col-sm-5">
                    <input type="password" class="tt-form-control" id="newpassword" name="newpassword" tabindex="1">
                  </div>
                  <div class="col-sm-4">
                    
                  </div>
                </div>
                <div class="form-group">
                  <label for="repeatnewpassword" class="col-sm-3 control-label">{{ trans('localization.ConNewPass') }}</label>
                  <div class="col-sm-5">
                    <input type="password" class="tt-form-control" id="repeatnewpassword" name="repeatnewpassword"  placeholder="" tabindex="2">
                  </div>
                  <div class="col-sm-4">
                  </div>
                </div>

              </div><!-- .portlet-content -->
              
              <div class="portlet-footer">
              	  <input type="hidden" name="id" id="id" value="<?php echo $user->id?>">
                  <button type="submit" class="tt-btn tt-btn-primary">{{ trans('localization.Confirm') }}</button>
              </div>
              </form>
            </div><!-- .portlet-content-wrap -->
          </div><!-- .portlet .widget-location -->

        </div><!-- .col-sm-12 -->
      </div><!-- .row -->

    </div><!-- .container-fluid -->

  </div><!-- #tt-body -->

</div><!-- #tt-content -->

<div id="tt-footer">
</div>