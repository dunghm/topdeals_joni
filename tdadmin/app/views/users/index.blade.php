
@include ('layout/greeting')

  <div id="tt-body">

    <div class="container-fluid">

      <div class="row">
        <div class="col-xs-12">
          <div class="tt-page-header">
            <div class="row">
              <div class="col-sm-6 title">
                  User Profile
              </div>
            </div>
          </div><!-- .tt-page-header -->
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">

          <div class="portlet widget-location">
            <div class="portlet-content-wrap">
              <form class="form-horizontal tt-form" role="form">
              <div class="portlet-content">
                <h3>Personal</h3>
                <div class="form-group">
                  <label for="f1" class="col-sm-3 control-label">Email</label>
                  <div class="col-sm-5">
                    <input type="text" class="tt-form-control" id="f1" value="" disabled="disabled" tabindex="1">
                  </div>
                  <div class="col-sm-4">
                    <div class="pt-7">Email address cannot be changed.</div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="f2" class="col-sm-3 control-label">First Name</label>
                  <div class="col-sm-5">
                    <input type="text" class="tt-form-control" id="f2" value="Usman" placeholder="" tabindex="2">
                  </div>
                  <div class="col-sm-4">
                  </div>
                </div>
                <div class="form-group">
                  <label for="f3" class="col-sm-3 control-label">Last Name</label>
                  <div class="col-sm-5">
                    <input type="text" class="tt-form-control" id="f3" value="Arshad" placeholder="" tabindex="3">
                  </div>
                  <div class="col-sm-4">
                  </div>
                </div>
                <div class="form-group">
                  <label for="f4" class="col-sm-3 control-label">Website</label>
                  <div class="col-sm-5">
                    <input type="text" class="tt-form-control" id="f4" value="" placeholder="" tabindex="4">
                  </div>
                  <div class="col-sm-4">
                  </div>
                </div>
                <div class="form-group">
                  <label for="f5" class="col-sm-3 control-label">Change Photo</label>
                  <div class="col-sm-5">
                    <input type="file" class="tt-form-control" id="f5" value="" placeholder="" tabindex="5">
                    <div class=""><small>Size should be 50x50 or 100x100</small></div>
                  </div>
                  <div class="col-sm-4">
                    <img class="user-photo" src="img/user.jpg" alt="user-photo">
                  </div>
                </div>

                <h3>Change Password</h3>
                <div class="form-group">
                  <label for="f6" class="col-sm-3 control-label">Old Password</label>
                  <div class="col-sm-5">
                    <input type="text" class="tt-form-control" id="f6" placeholder="" tabindex="6">
                  </div>
                  <div class="col-sm-4">
                  </div>
                </div>
                <div class="form-group">
                  <label for="f7" class="col-sm-3 control-label">New Password</label>
                  <div class="col-sm-5">
                    <input type="text" class="tt-form-control" id="f7" placeholder="" tabindex="7">
                  </div>
                  <div class="col-sm-4">
                  </div>
                </div>
                <div class="form-group">
                  <label for="f8" class="col-sm-3 control-label">Repeat New Password</label>
                  <div class="col-sm-5">
                    <input type="text" class="tt-form-control" id="f8" placeholder="" tabindex="8">
                  </div>
                  <div class="col-sm-4">
                  </div>
                </div>

              </div><!-- .portlet-content -->
              </form>
              <div class="portlet-footer">
                  <button type="submit" class="tt-btn tt-btn-primary">Save</button>
              </div>
            </div><!-- .portlet-content-wrap -->
          </div><!-- .portlet .widget-location -->

        </div><!-- .col-sm-12 -->
      </div><!-- .row -->

    </div><!-- .container-fluid -->

  </div><!-- #tt-body -->

</div><!-- #tt-content -->

<div id="tt-footer">
</div>