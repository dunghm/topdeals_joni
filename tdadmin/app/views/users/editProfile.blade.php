@include ('layout/greeting')
<script>
        tinymce.init({
			 mode : "specific_textareas",
        	editor_selector : "mceEditor"
		});
</script>

  <div id="tt-body">

    <div class="container-fluid">

      <div class="row">
        <div class="col-xs-12">
          <div class="tt-page-header">
            <div class="row">
              <div class="col-sm-6 title">
				{{ trans('localization.partnerProfileTitle') }}
              </div>
            </div>
          </div><!-- .tt-page-header -->
        </div>
      </div>
	<?php
		//IF SESSION MENTAIN THEN REDIRECTED TO ADMIN/PARTNER DASHBOARD
		$userType = getCurrentUserRole();
	?>
      <div class="row">
        <div class="col-sm-12">

          <div class="portlet widget-location">
            <div class="portlet-content-wrap">
              <form class="form-horizontal tt-form" role="form" name="userProfile" id="userProfile" method="post" action="{{ URL::to($userType.'/edit-update-profile') }}" enctype="multipart/form-data">
              <div class="portlet-content">
                <h3>1. {{ trans('localization.partnerBasicSettingTitle') }}</h3>
                <div class="form-group">
                  <label for="title" class="col-sm-3 control-label">{{ trans('localization.BizName') }}</label>
                  <div class="col-sm-5">
                    <input type="text" class="tt-form-control" id="title" name="title" value="<?php echo $user->title; ?>" tabindex="1">
                  </div>
                  <div class="col-sm-4">
                    
                  </div>
                </div>
                <div class="form-group">
                  <label for="homepage" class="col-sm-3 control-label">{{ trans('localization.WebsiteURL') }}</label>
                  <div class="col-sm-5">
                    <input type="text" class="tt-form-control" id="homepage" name="homepage" value="<?php echo $user->homepage; ?>" tabindex="1">
                  </div>
                  <div class="col-sm-4">
                    
                  </div>
                </div>
				 <div class="form-group">
                  <label for="contact" class="col-sm-3 control-label">{{ trans('localization.PersonToContact') }}</label>
                  <div class="col-sm-5">
                    <input type="text" class="tt-form-control" id="contact" name="contact" value="<?php echo $user->contact; ?>" placeholder="" tabindex="3">
                  </div>
                  <div class="col-sm-4">
                  </div>
                </div>
                <div class="form-group">
                  <label for="address" class="col-sm-3 control-label">{{ trans('localization.BizLocation') }}</label>
                  <div class="col-sm-5">
                    <input type="text" class="tt-form-control" id="address" name="address" value="<?php echo $user->address; ?>" placeholder="" tabindex="2">
                  </div>
                  <div class="col-sm-4">
                  </div>
                </div>
                <div class="form-group">
                  <label for="phone" class="col-sm-3 control-label">{{ trans('localization.PhoneNumber') }}</label>
                  <div class="col-sm-5">
                    <input type="text" class="tt-form-control" id="phone" name="phone" value="<?php echo $user->phone; ?>" placeholder="" tabindex="3">
                  </div>
                  <div class="col-sm-4">
                  </div>
                </div>
                <div class="form-group">
                  <label for="mobile" class="col-sm-3 control-label">{{ trans('localization.Mobile') }}</label>
                  <div class="col-sm-5">
                    <input type="text" class="tt-form-control" id="mobile" name="mobile" value="<?php  echo $user->mobile; ?>" placeholder="" tabindex="4">
                  </div>
                  <div class="col-sm-4">
                  </div>
                </div>
				 <div class="form-group">
					<label for="location" class="control-label">{{ trans('localization.Address') }}</label>
					<textarea class="tt-form-control mceEditor" id="location" rows="5" name="location">{{{ $user->location }}}</textarea>
				</div>
				<div class="form-group">
					<label for="business_address" class="control-label">{{ trans('localization.BusinessAddress') }}</label>
					<textarea class="tt-form-control mceEditor" id="business_address" rows="5" name="business_address">{{{ $user->business_address }}}</textarea>
				</div>
				<div class="form-group">
					<label for="other" class="control-label">{{ trans('localization.OtherInfo') }}</label>
					<textarea class="tt-form-control mceEditor" id="other" rows="5" name="other">{{{ $user->other }}}</textarea>
				</div>

				
                <h3>2. {{ trans('localization.partnerBank') }}</h3>
				 <div class="form-group">
                  <label for="bank_name" class="col-sm-3 control-label">{{ trans('localization.AccBank') }}</label>
                  <div class="col-sm-5">
                    <input type="text" class="tt-form-control" id="bank_name" name="bank_name" value="<?php  echo $user->bank_name; ?>" placeholder="" tabindex="4">
                  </div>
                  <div class="col-sm-4">
                  </div>
                </div>
                <div class="form-group">
                  <label for="bank_user" class="col-sm-3 control-label">{{ trans('localization.AccName') }}</label>
                  <div class="col-sm-5">
                    <input type="text" class="tt-form-control" id="bank_user" name="bank_user" value="<?php  echo $user->bank_user; ?>" placeholder="" tabindex="4">
                  </div>
                  <div class="col-sm-4">
                  </div>
                </div>
				 <div class="form-group">
                  <label for="bank_no" class="col-sm-3 control-label">{{ trans('localization.BankNo') }}</label>
                  <div class="col-sm-5">
                    <input type="text" class="tt-form-control" id="bank_no" name="bank_no" value="<?php  echo $user->bank_no; ?>" placeholder="" tabindex="4">
                  </div>
                  <div class="col-sm-4">
                  </div>
                </div>

              </div><!-- .portlet-content -->
              
              <div class="portlet-footer">
              	  <input type="hidden" name="id" id="id" value="<?php echo $user->id?>">
                  <button type="submit" class="tt-btn tt-btn-primary">{{ trans('localization.Confirm') }}</button>
              </div>
              </form>
            </div><!-- .portlet-content-wrap -->
          </div><!-- .portlet .widget-location -->

        </div><!-- .col-sm-12 -->
      </div><!-- .row -->

    </div><!-- .container-fluid -->

  </div><!-- #tt-body -->

</div><!-- #tt-content -->

<div id="tt-footer">
</div>