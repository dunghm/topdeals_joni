<div >
	<?php 
		$firstName	=	"";
		$lastName	=	"";
		$email		=	"";
		$mobile		=	"";
		$address	=	"";
		$address2	=	"";
		$zipcode	=	"";
		$region	=	"";
		
		if($data['getOrderItemDetail']->first_name){
			$firstName = $data['getOrderItemDetail']->first_name;}
		else{ 
			$firstName = $data['getOrderById']->realname; 
		} 
		
		
		if($data['getOrderItemDetail']->last_name){
			$lastName = $data['getOrderItemDetail']->last_name;
		}else{
			$lastName = $data['getOrderById']->lastname; 
		}
		
		
		
		if($data['getOrderItemDetail']->email){
			$email = $data['getOrderItemDetail']->email;
		}else{
			$email = $data['getOrderById']->email; 
		}
		
		
		if($data['getOrderItemDetail']->mobile){
			$mobile = $data['getOrderItemDetail']->mobile;
		}else{
			$mobile = $data['getOrderById']->mobile; 
		}
		
		
		if($data['getOrderItemDetail']->address){
			$address = $data['getOrderItemDetail']->address;
		}else{
			$address = $data['getOrderById']->address; 
		}
		
		
		if($data['getOrderItemDetail']->address2){
			$address2 = $data['getOrderItemDetail']->address2;
		}else{
			$address2 = $data['getOrderById']->address2; 
		}
		
		
		if($data['getOrderItemDetail']->zipcode){
			$zipcode = $data['getOrderItemDetail']->zipcode;
		}else{
			$zipcode = $data['getOrderById']->zipcode; 
		}
		
		
		if($data['getOrderItemDetail']->region){
			$region = $data['getOrderItemDetail']->region;
		}else{
			$region = $data['getOrderById']->region; 
		}
	?>
  <div class="panel panel-info">
    <div class="panel-heading">
		Order No : 
	    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	</div>
    <div class="panel-body">
      <form role="form" method="post" action="<?php echo URL::to('admin/deliveryAddressSubmit'); ?>"  >
		  <div class="form-group">
			<label for="first_name" class="control-label"><abbr title="required" >*</abbr> {{ trans('localization.first_name') }}: </label>
			<input type="text" placeholder="{{ trans('localization.first_name') }}" name="first_name" id="first_name" class="form-control" value="<?php echo $firstName;?>">
		  </div>
		  <div class="form-group">
			<label for="last_name" class="control-label"><abbr title="required">*</abbr> {{ trans('localization.last_name') }}: </label>
			<input type="text" placeholder="{{ trans('localization.last_name') }}" name="last_name" id="last_name" class="form-control" value="<?php echo $lastName; ?>">
		  </div>
		  <div class="form-group">
			<label for="email" class="control-label"><abbr title="required">*</abbr> {{ trans('localization.email') }}: </label>
			<input type="text" placeholder="{{ trans('localization.email') }}" name="email" id="email" class="form-control" value="<?php echo $email;  ?>" readonly="readonly">
		  </div>
		  <div class="form-group">
			<label for="mobile" class="control-label"><abbr title="required">*</abbr> {{ trans('localization.mobile_number') }}: </label>
			<input type="text" placeholder="{{ trans('localization.mobile_number') }}" name="mobile" id="mobile" class="form-control" value="<?php echo $mobile; ?>">
			<p class="help-block">{{ trans('localization.help_shipping_address_contact_message') }}</p>
		  </div>
		  <div class="form-group">
			<label for="address" class="control-label"><abbr title="required">*</abbr> {{ trans('localization.address1') }}: </label>
			<input type="text" placeholder="{{ trans('localization.address1') }}" name="address" id="address" class="form-control" value="<?php echo $address;   ?>">
		  </div>
		  <div class="form-group">
			<label for="adresse2" class="control-label"> {{ trans('localization.address2') }}: </label>
			<input type="text" placeholder="{{ trans('localization.address2') }}" name="address2" id="address2" class="form-control" value="<?php echo $address2;  ?>">
		  </div>
		  <div class="form-group">
			<label for="zipcode" class="control-label"><abbr title="required">*</abbr> {{ trans('localization.zipcode') }}: </label>
			<input type="text" placeholder="{{ trans('localization.zipcode') }}" name="zipcode" id="zipcode" class="form-control" value="<?php echo $zipcode; ?>">
		  </div>
		  <div class="form-group">
			<label for="region" class="control-label"> <abbr title="required">*</abbr> {{ trans('localization.region') }}: </label>
			<input type="text" placeholder="{{ trans('localization.region') }}" name="region" id="region" class="form-control" value="<?php echo $region; ?>">
			<input type="hidden" name="order_item_id" value="<?php echo $data['getOrderItemDetail']->id;?>" />
		  </div>
		<div class="form-group btn-region">
			<div  class="err-region-popup alert alert-danger" role="alert" style="display:none;"></div>
			<button class="btn btn-primary" type="submit" name="update" value="update" id="update-order">Update</button>
		</div>
		<div class="wait-region-order-delivery" style="display:none;">Please Wait...</div>
		</form>

    </div>
  </div>
</div>
<script>
	jQuery(document).ready(function(){
		jQuery('#update-order').on('click',function(){
			
			var first_name 	= jQuery.trim(jQuery('#first_name').val());
			var last_name 	= jQuery.trim(jQuery('#last_name').val());
			var email 		= jQuery.trim(jQuery('#email').val());
			var mobile 		= jQuery.trim(jQuery('#mobile').val());
			var address 	= jQuery.trim(jQuery('#address').val());
			var zipcode 	= jQuery.trim(jQuery('#zipcode').val());
			var region 		= jQuery.trim(jQuery('#region').val());
			
			var errMessage	=	"";
			
			if(first_name==""){
				errMessage += " - {{ trans('localization.first_name') }} is required.<br/>";
			}
			
			if(last_name==""){
				errMessage += " - {{ trans('localization.last_name') }} is required.<br/>";
			}
			
			if(email==""){
				errMessage += " - {{ trans('localization.email') }} is required.<br/>";
			}
			//VALIDATE EMAIL
			if(email != ""){
				if(!isValidEmailAddress(email)){
					errMessage += " - Please enter valid {{ trans('localization.email') }} Address. <br/>";
				}
			}
			
			if(mobile==""){
				errMessage += " - {{ trans('localization.mobile_number') }} is required.<br/>";
			}
			
			if(address==""){
				errMessage += " - {{ trans('localization.address') }} is required.<br/>";
			}
			
			if(zipcode==""){
				errMessage += " - {{ trans('localization.zipcode') }} is required.<br/>";
			}
			
			if(region==""){
				errMessage += " - {{ trans('localization.region') }} is required.<br/>";
			}
			

			
			if(errMessage!=""){
				jQuery(".err-region-popup").html(errMessage);
				jQuery(".err-region-popup").css('display','block');
				
				return false;
			}else{
				jQuery(".err-region-popup").html("");
				jQuery(".err-region-popup").css('display','none');
				
				jQuery(".btn-region").css('display','none');
				jQuery(".wait-region-order-delivery").css('display','block');
				return true;
			}
		});
	});
</script>