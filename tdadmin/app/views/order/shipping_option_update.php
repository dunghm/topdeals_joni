<div >
  <div class="panel panel-info">
    <div class="panel-heading">
		Order No : <?php echo $data['getOrderItemDetail']->order_id; ?>
	    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	</div>
    <div class="panel-body">
      <form role="form" method="post" action="<?php echo URL::to('admin/deliveryOptionEditSubmit'); ?>"  >
		  <div class="form-group">
			<label for="exampleInputEmail1" class="control-label"> Deal: </label>
			<?php echo $data['team'][0]->title; ?>
		  </div>

		<?php
			//IF SELECTED ORDEAR ITEM DEAL HAS OPTION 
			if(isset($data['team_option_info'][0]->title)){
		?>
		  <div class="form-group">
			<label for="exampleInputPassword1" class="control-label">Options</label>
			<?php 
			
				$selected_option_id	=	$data['getOrderItemDetail']->option_id;
				$team_option_text	=	" - ";
				if(isset($data['team_option_info'][0]->title)){
					$team_option_text =  $data['team_option_info'][0]->title;
				}
				
				echo $team_option_text;
			?>
			<input type="hidden"  id="team_option" name="team_option" value="<?php echo $selected_option_id; ?>" class="form-control">
			<?php
				/*
			?>

			<select class="form-control" name="team_option">
				<?php 
					$team_multi	=	$data['team_multi'];
					if(!empty($team_multi)){
						
						$selected_option_id	=	$data['getOrderItemDetail']->option_id;
						
						foreach($team_multi as $team_multiRow){
							;
							$selected = "";
							if($team_multiRow->id == $selected_option_id){
								$selected = "selected='selected'";
							}
				?>
						<option <?php echo $selected;?> value="<?php echo $team_multiRow->id;?>"><?php echo $team_multiRow->title;?></option>
				<?php
						}
					}
				?>
			</select>
			<?php
				*/
			?>
		</div>
		<?php
			}
		?>
		<div class="form-group">
			<label for="exampleInputEmail1" class="control-label"> Quantity: <?php echo $data['getOrderItemDetail']->quantity; ?> </label>
			<input type="hidden"  id="quantity" name="quantity" value="<?php echo $data['getOrderItemDetail']->quantity; ?>" class="form-control">
		 </div>
		<div class="form-group">
			<label for="exampleInputPassword1" class="control-label">Shipment Type</label>
		
			<select class="form-control" name="delivery_method">
				<?php 
					$optionDelivery	=	$data['optionDelivery'];
					if(!empty($optionDelivery)){
						
						$selected_delivery	=	$data['getOrderItemDetail']->delivery;
						unset($optionDelivery['coupon']);
						unset($optionDelivery['express_pickup']);
						
						foreach($optionDelivery as $key=>$optionDeliveryRow){
							
							$selected = "";
							if($key == $selected_delivery){
								$selected = "selected='selected'";
							}
				?>
						<option <?php echo $selected;?> value="<?php echo $key;?>"><?php echo $optionDeliveryRow;?></option>
				<?php
						}
					}
				?>
			</select>
			
			<input type="hidden"  name="order_item_id" value="<?php echo $data['getOrderItemDetail']->id;?>" />
			<input type="hidden"  name="order_id" value="<?php echo $data['getOrderItemDetail']->order_id;?>" />
		</div>
		<div class="form-group">
			<label for="exampleInputEmail1" class="control-label"> Select Method for charging additional Cost: </label>
			 <div >
			 <?php
				//SET SELECTED VALUE
				$shipment_additional_cost = $data['getOrderItemDetail']->shipment_additional_cost;
				
				if($shipment_additional_cost == ""){
					//SET DEFAULT SHIP COSE
					$shipment_additional_cost = "user_balance_cash";
				}
				
				$method_additional_costArr	=	additionalPaymentMethod();
			 ?>
			<table>
				<?php
					if(!empty($method_additional_costArr)){
						foreach($method_additional_costArr as $keyVal => $method_additional_costRow){
							
							$selected = "";
							if($keyVal == $shipment_additional_cost){
								$selected = "checked='checked'";
							}
				?>
				<tr>
					<td><input name='method_additional_cost' <?php echo $selected;?> value="<?php echo $keyVal;?>" type="radio"/><?php echo $method_additional_costRow;?></td>
				</tr>
				<?php
						}
					}
				?>
			</table>
		 </div>
		<div class="form-group btn-region">
			<div  class="err-region-popup alert alert-danger" role="alert" style="display:none;"></div>
			<button class="btn btn-primary" type="submit" name="update" value="update" id="update-order">Update</button>
		</div>
		<div class="wait-region-order-delivery" style="display:none;">Please Wait...</div>
		</form>

    </div>
  </div>
</div>
<script>
	jQuery(document).ready(function(){
		
		jQuery('#update-order').on('click',function(){
			
			var quantity	=	jQuery.trim(jQuery('#quantity').val());
			
			var errMessage	=	"";
			
			if(quantity==""){
				errMessage += "Please Enter Quantity";
			}
			
			if(quantity!=""){
				if(!jQuery.isNumeric( quantity) || quantity<=0){
					errMessage += "Please Enter Valid Quantity";
				}
			}
			
			if(errMessage!=""){
				jQuery(".err-region-popup").html(errMessage);
				jQuery(".err-region-popup").css('display','block');
				
				return false;
			}else{
				jQuery(".err-region-popup").html("");
				jQuery(".err-region-popup").css('display','none');
				
				jQuery(".btn-region").css('display','none');
				jQuery(".wait-region-order-delivery").css('display','block');
				return true;
			}
				
		});
	});
</script>