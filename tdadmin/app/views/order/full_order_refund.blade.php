<div >
  <div class="panel panel-info">
    <div class="panel-heading">
		Order No : {{$orderId}}
	    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	</div>
    <div class="panel-body">
      
		<table width="100%" border="0">
			<tr>
				<td width="40%"><strong>Deal</strong></td>
				<td width="40%"><strong>Option</strong></td>
				<td width="10%"><strong>quantity</strong></td>
				<td width="10%"><strong>Price</strong></td>
			</tr>
			<?php
				$totalAmount =$sumCashBack = 0;
				if(is_array($orderItemDetails) && count($orderItemDetails)>0)
				{
					
					foreach($orderItemDetails as $orderItemDetail)
					{
						$teamTitle 			= $orderItemDetail->teamTitle;
						$optionTitle 		= $orderItemDetail->optionTitle;
						$orderQuantity 		= $orderItemDetail->orderQuantity;
						$totalpicebyDeal 	= $orderItemDetail->totalPice;
						$orderfare 			= $orderItemDetail->orderfare;
						$cashbackAmount	= $orderItemDetail->cashback_amount;
						
						#$sumCashBack+= $cashback_amount;
						$totalAmount =$totalAmount+$totalpicebyDeal;
						#$totalAmount =$totalAmount+$orderfare;
						
						#cash back
						if($totalAmount>$cashbackAmount && $cashbackAmount>0)
						{
							$totalAmount = $totalAmount - $cashbackAmount;
						}
						
						?>
						<tr>
						<td><?php echo $teamTitle; ?></td>
						<td><?php echo $optionTitle; ?></td>
						<td><?php echo $orderQuantity; ?></td>
						<td><?php echo $totalpicebyDeal; ?></td>
						</tr>
						<?php
					} 
				}
				
				#orderCard
				if($totalAmount>$orderCard && $orderCard>0)
				{
					$totalAmount = $totalAmount - $orderCard;
				}
				
			?>
			
		</table>
	
	
	<br/><br/>
	<div id="requiredError" ></div>
	<div class="form-group">
		<label for="exampleInputPassword1" class="control-label">{{ trans('order_refund_lang.refund_policy') }}:</label>
			<select name="refund" id="order-dialog-refund-id" class="form-control">
										
				<option value="0">{{ trans('order_refund_lang.select') }}</option>
				<?php
						if(is_array($refundOption) && count($refundOption)>0)
						{
							
							foreach ($refundOption as $key=>$val)
							{
								?>
								<option value="<?php echo $key; ?>"><?php echo $val; ?></option>
								<?php
							}
							
						} 
				?>
			</select>
			
			<input name="order_id" id="order_id" type="hidden" value="{{$orderId}}" />
	</div>
	<div class="form-group">
		<label for="exampleInputEmail1" class="control-label"> {{ trans('order_refund_lang.reason') }}: </label>
		<input name="reason_refund" id="reason_refund" type="text" class="form-control" placeholder="{{ trans('order_refund_lang.reason') }}" />
	 </div>
	 
	 <div class="form-group">
		<label for="exampleInputEmail1" class="control-label"> {{ trans('order_refund_lang.refund_amount') }}:</label>
		<span id="totalAmountSpan">{{$totalAmount}}</span>{{CURRENCY}}
		<div id="quantityError" class="text-danger"></div>
	  </div>
	
	<div class="form-group btn-region">
		<div  class="err-region-popup alert alert-danger" role="alert" style="display:none;"></div>
		<button class="btn btn-primary" type="submit" name="update" value="update" id="refund-order" onclick="fullOrderRefund();">Refund</button>
	</div>
	<div class="wait-region-order-delivery text-success" style="display:none;">Please Wait...</div>
		

    </div>
  </div>
</div>

<script>
// order refund

function fullOrderRefund()
{
	$('.wait-region-order-delivery').show();
	$('#refund-order').attr('disabled', 'disabled');
	
	var order_id 				= jQuery('#order_id').val();
    var reason_refund 			= jQuery('#reason_refund').val();
    var refundpolicy			= jQuery('#order-dialog-refund-id').val();

	var action_url = "<?php echo URL::to('admin/fullorderrefund'); ?>/";
	

	if (order_id)
	{ 
		queryData = 'orderId='+order_id;
		queryData += '&reasonRefund='+reason_refund;
		queryData += '&refundPolicy='+refundpolicy;
		
		
		 $.ajax({
				type: "GET",
				url: action_url,
				data: queryData,
				cache: false,
				success: function(response) 
				{
					 $('.wait-region-order-delivery').hide();
					 response = jQuery.parseJSON(response);
					 
					 if (response.error)
					 {
						 $('#requiredError').html(response.error);
						 $('#requiredError').addClass( "alert alert-danger" );
					 }
					 else
					 {
						$('#requiredError').html('');
						$('#requiredError').removeClass( "alert alert-danger" );
					 }
					 
					 if(response.errorMessage)
					 {
						$('#requiredError').removeClass( "alert alert-success" );
						$('#requiredError').html('');
						$('#requiredError').html(response.errorMessage);
						$('#requiredError').addClass("alert alert-danger");
					 }
					 
					  if(response.successMessage)
					 {
						$('#requiredError').removeClass( "alert alert-danger" );
						$('#requiredError').html('');
						$('#requiredError').html(response.successMessage);
						$('#requiredError').addClass("alert alert-success");
						//reload page
						window.location.reload(1);
					 }
					 else
					 {
						$('#refund-order').removeAttr('disabled');
					 }
				}
			});
	}	
 }
 

</script>