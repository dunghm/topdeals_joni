<script src="<?php echo URL::asset('assets/datatable/jquery.dataTables.js');?>"></script>
<script src="<?php echo URL::asset('assets/yadcf/jquery_datatables_yadcf.js');?>"></script>
<style>
div.dataTables_length select { display: none; }
.dataTables_filter { display: none }
</style>
@if(Session::has('successMessage'))
    <div class="alert alert-success">
        {{ Session::get('successMessage') }}
    </div>
@endif

@if(Session::has('errorMessage'))
    <div class="alert alert-danger">
        {{ Session::get('errorMessage') }}
    </div>
@endif		

<div class="container-fluid">
	<div class="row">	
		<div class="col-xs-12">

			<div class="portlet pd-30">
				<div class="page-heading">
					{{ trans('localization.paid_order') }}
				</div>
				<!-- SEARCH FORM -->
				<div class="panel panel-info">
					<div class="panel-heading">Search Order Paid</div>
					<div class="panel-body">
					  <form role="form" method="post" action="<?php echo URL::to('admin/orderlist'); ?>" class="form-inline">
						  <div class="form-group">
							<label for="order_num" class="sr-only">Order No.</label>
							<input type="text" placeholder="Order No." id="order_num" value="<?php if (Session::has('sr_order_num')){ echo Session::get('sr_order_num'); }?>" class="form-control" name="order_num">
						  </div>
						   <div class="form-group">
							<label for="user" class="sr-only">User:</label>
							<input type="text" placeholder="User" value="<?php if (Session::has('sr_user')){ echo Session::get('sr_user'); }?>"  id="user" class="form-control" name="user">
						  </div>
						   <div class="form-group">
							<label for="deal_num" class="sr-only">Deal No.</label>
							<input type="text" placeholder="Deal No." value="<?php if (Session::has('sr_deal_num')){ echo Session::get('sr_deal_num'); }?>"  id="deal_num" class="form-control" name="deal_num">
						  </div>
						   <div class="form-group">
							<label for="deal_num" class="sr-only">Delivery Type:</label>
							<select class="form-control" name="delivery_type">
								<option value="">Choice Delivery Type</option>
								<?php
									if(isset($data['deliveryStatusArray'])){
										if(!empty($data['deliveryStatusArray'])){
											
											//CHECK DEFAULT SELECTED ALUE
											$selectedValue	=	"";
											if (Session::has('sr_delivery_type')){ $selectedValue = Session::get('sr_delivery_type'); }
											
											foreach($data['deliveryStatusArray'] as $value=>$deliveryStatusRow){
												
												$selected	=	"";
												if($selectedValue==$value){
													$selected	=	"selected='selected'";
												}
								?>
												<option value="<?php echo $value;?>" <?php echo $selected;?>><?php echo $deliveryStatusRow;?></option>
								<?php
											}
										}
									}
								?>
							</select>
						  </div>
						  <div class="form-group">
							<label for="deal_num" class="sr-only">Status:</label>
							<select class="form-control" name="delivery_status">
								<option value="">Choice Delivery Status</option>
								<?php
									if(isset($data['optionDelivery'])){
										if(!empty($data['optionDelivery'])){
											

											//CHECK DEFAULT SELECTED ALUE
											$selectedValue	=	"";
											if (Session::has('sr_delivery_status')){ $selectedValue = Session::get('sr_delivery_status'); }
											
											foreach($data['optionDelivery'] as $value=>$optionDeliveryRow){
												
												$selected	=	"";
												if($selectedValue==$value){
													$selected	=	"selected='selected'";
												}
								?>
												<option value="<?php echo $value;?>" <?php echo $selected;?>><?php echo $optionDeliveryRow;?></option>
								<?php
											}
										}
									}
								?>
							</select>
						  </div>
						   
						  <button class="btn btn-primary" type="submit" name="search" value="search">Search</button>
						  <button class="btn btn-default" type="submit" name="reset" value="reset">Reset</button>
					</form>
					</div>
				</div>
				<!-- SEARCH FORM -->
				<div class="ho-lala">
				  {{ Datatable::table()
					  ->addColumn(trans('localization.id'),trans('localization.deal'), trans('localization.users'), trans('localization.quantity'), trans('localization.total'),trans('localization.balance'),trans('localization.promo_card'),trans('localization.payment'),trans('localization.fare'),trans('localization.shipment_satus'),trans('localization.payment_method'),trans('localization.operation'))
					  ->setUrl(route('api.orderlist'))
					  ->setOptions('order', array())
					  ->render()
				  }}
				</div>
				<div class="custom-pagination-region dataTables_info">
					<div class="pagination-summary">
						Showing <?php echo $data['pagination']->getFrom();?> to <?php echo $data['pagination']->getTo();?> of <?php echo $data['pagination']->getTotal();?> entries
					</div>
					<div class="dataTables_paginate paging_full_numbers">
						<?php 
							echo $data['pagination']->links()."<br/>"; 
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
  $(document).ready(function(){
    var oTable = $('.table').dataTable().yadcf([
  	  	// {column_number : 0, filter_type: "text"},
  	  	// {column_number : 1, filter_type: "text"},
		// {column_number : 2, filter_type: "text"},
		// {column_number : 3, filter_type: "text"},
  	  	// {column_number : 4, filter_type: "text"},
		// {column_number : 5, filter_type: "text"},
		// {column_number : 6, filter_type: "text"},
  	  	// {column_number : 7, filter_type: "text"},
		// {column_number : 8, filter_type: "text"},
		// {column_number : 9, filter_type: "text"},
		// {column_number : 10, filter_type: "text"},
    ]);
	  var oSettings = oTable.fnSettings();
	  oSettings._iDisplayLength = 50;
	  oTable.fnDraw();
	  $('.dataTables_wrapper').find('select').val(50).text();
    $('table').removeClass().addClass('tt-table dataTable laravel-table').wrap('<div class="table-responsive"></div>');
    $('.dataTables_wrapper').wrap('<div class="portlet pd-30"></div>');
    $('.yadcf-filter').addClass('tt-form-control');
    $(".dataTables_length").addClass('clearfix');
	
	
	//HIDE BUILTIN PAGINATION
	jQuery('.dataTables_info').css('display','none');
	jQuery('.dataTables_paginate').css('display','none');
	jQuery('.dataTables_length').css('display','none');
	
	//SHOW CUSTOM PAGINATION
	jQuery('.custom-pagination-region').css('display','block');
	jQuery('.custom-pagination-region>.dataTables_paginate').css('display','block');
	
	//JQUERY SORTING FUNCTIONALITY REMOVE
	jQuery( "th" ).unbind();
	jQuery("th").css('cursor','default');
  });
  
</script>