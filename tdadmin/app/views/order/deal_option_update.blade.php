<div >
  <div class="panel panel-info">
    <div class="panel-heading">
		Order No : <?php echo $data['getOrderItemDetail']->order_id; ?>
	    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	</div>
    <div class="panel-body">
      <form role="form" method="post" action="<?php echo URL::to('admin/dealOptionSubmit'); ?>"  >
		  <div class="form-group">
			<label for="exampleInputEmail1" class="control-label"> Deal: </label>
			<?php echo $data['team'][0]->title; ?>
		  </div>

		<?php
			//IF SELECTED ORDEAR ITEM DEAL HAS OPTION 
			if(isset($data['team_option_info'][0]->title)){
		?>
		  <div class="form-group">
			<label for="exampleInputPassword1" class="control-label">Options</label>
			<?php 
			/*
				$selected_option_id	=	$data['getOrderItemDetail']->option_id;
				$team_option_text	=	" - ";
				if(isset($data['team_option_info'][0]->title)){
					$team_option_text =  $data['team_option_info'][0]->title;
				}
				
				echo $team_option_text;
			?>
			<input type="hidden"  id="team_option" name="team_option" value="<?php echo $selected_option_id; ?>" class="form-control">
			*/
			?>

			<select class="form-control" name="team_option">
				<?php 
					$team_multi	=	$data['team_multi'];
					if(!empty($team_multi)){
						
						$selected_option_id	=	$data['getOrderItemDetail']->option_id;
						
						foreach($team_multi as $team_multiRow){
							;
							$selected = "";
							if($team_multiRow->id == $selected_option_id){
								$selected = "selected='selected'";
							}
							
							//DISABLE OPTION IF MAX_ALLOWED IS LESS
							$disabled = "";
							if($team_multiRow->max_number>0 && $team_multiRow->max_number < $data['getOrderItemDetail']->quantity){
								$disabled	=	"disabled='disabled'";
							}
				?>
						<option <?php echo $selected; echo $disabled;?>  value="<?php echo $team_multiRow->id;?>"><?php echo $team_multiRow->title;?></option>
				<?php
						}
					}
				?>
			</select>
			
		</div>
		<?php
			}else{
		?>		
		<input type="hidden"  id="team_option" name="team_option" value="0" class="form-control">
		<?php
			}
		?>
		<div class="form-group">
			<label for="exampleInputEmail1" class="control-label"> Quantity: </label>
			<input type="text" id="quantity"  name="quantity"  default="<?php echo $data['getOrderItemDetail']->quantity; ?>" value="<?php echo $data['getOrderItemDetail']->quantity; ?>" class="form-control quan-numeric">
		 </div>
		<div class="form-group">
			<label for="exampleInputPassword1" class="control-label">Shipment Type</label>
			
			<?php 
				$optionDelivery	=	$data['optionDelivery'];
				$deliveryOption	=	"";
				if(!empty($optionDelivery)){
					
					$selected_delivery	=	$data['getOrderItemDetail']->delivery;
					
					foreach($optionDelivery as $key=>$optionDeliveryRow){
						
						$selected = "";
						if($key == $selected_delivery){
							$selected = "selected='selected'";
							$deliveryOption = $optionDeliveryRow;
							break;
						}
					}
				}
			?>
						
			<input type="text" name="delivery_method" readonly="readonly" value="<?php echo $deliveryOption;?>"/>
			<input type="hidden"  name="order_item_id" value="<?php echo $data['getOrderItemDetail']->id;?>" />
			<input type="hidden"  name="order_id" value="<?php echo $data['getOrderItemDetail']->order_id;?>" />
			
		</div>
		
		<div class="form-group">
		
			<label for="exampleInputEmail1" class="control-label"> Select Method for charging additional Cost: </label>
			 <div >
			 <?php
				//SET SELECTED VALUE
				$shipment_additional_cost = $data['getOrderItemDetail']->shipment_additional_cost;
				
				if($shipment_additional_cost == ""){
					//SET DEFAULT SHIP COSE
					$shipment_additional_cost = "user_balance_cash";
				}
				
				$method_additional_costArr	=	additionalPaymentMethod();
			 ?>
			<table>
				<?php
					if(!empty($method_additional_costArr)){
						foreach($method_additional_costArr as $keyVal => $method_additional_costRow){
							
							$selected = "";
							if($keyVal == $shipment_additional_cost){
								$selected = "checked='checked'";
							}
				?>
				<tr>
					<td><input name='method_additional_cost' <?php echo $selected;?> value="<?php echo $keyVal;?>" type="radio"/><?php echo $method_additional_costRow;?></td>
				</tr>
				<?php
						}
					}
				?>
			</table>
		 </div>
		
		<div class="form-group btn-region">
			<?php
				/*if($data['parentDealCloseOrOutOfStock']==true){
					
					if($data['dealLimitPerUserErr']==true){
						//echo '<div  class="alert alert-danger" role="alert" >'.$data['parentDealCloseOrOutOfStockMsg'].'</div>';
			?>
						<div  class="err-region-popup alert alert-danger" role="alert" style="display:none;"></div>
						<button class="btn btn-primary" type="submit" name="update" value="update" id="update-order">Update</button>
			<?php
					}else{
						echo '<div  class="alert alert-danger" role="alert" >'.$data['parentDealCloseOrOutOfStockMsg'].'</div>';
					}
					
			?>
			<?php
				}else{
					*/
			?>
			<?php
				if($data['cashbackErrorMessage']!=""){
			?>
				<div  class="err-region-popup alert alert-danger" role="alert" ><?php echo $data['cashbackErrorMessage'];?></div>
			<?php
				}else{
			?>
				<div  class="err-region-popup alert alert-danger" role="alert" style="display:none;"></div>
				<button class="btn btn-primary" type="submit" name="update" value="update" id="update-order">Update</button>
			<?php
				}
			?>
			<?php
				// }
			?>
		</div>
		<div class="wait-region-order-delivery" style="display:none;">Please Wait...</div>
		</form>

    </div>
  </div>
</div>
<script>
	jQuery(document).ready(function(){
		
		jQuery('#update-order').on('click',function(){
			
			var quantity	=	jQuery.trim(jQuery('#quantity').val());
			
			var errMessage	=	"";
			
			if(quantity==""){
				errMessage += "Please Enter Quantity";
			}
			
			if(quantity!=""){
				if(!jQuery.isNumeric( quantity) || quantity<=0){
					errMessage += "Please Enter Valid Quantity";
				}
			}
			
			if(errMessage!=""){
				jQuery(".err-region-popup").html(errMessage);
				jQuery(".err-region-popup").css('display','block');
				
				return false;
			}else{
				jQuery(".err-region-popup").html("");
				jQuery(".err-region-popup").css('display','none');
				
				jQuery(".btn-region").css('display','none');
				jQuery(".wait-region-order-delivery").css('display','block');
				return true;
			}
				
		});
		
		
		 jQuery(".quan-numeric").keydown(function (e) {
			// Allow: backspace, delete, tab, escape, enter and .
			if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
				 // Allow: Ctrl+A
				(e.keyCode == 65 && e.ctrlKey === true) || 
				 // Allow: home, end, left, right
				(e.keyCode >= 35 && e.keyCode <= 39)) {
					 // let it happen, don't do anything
					 return;
			}
			// Ensure that it is a number and stop the keypress
			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
		});
	
	   jQuery(".quan-numeric").keyup(function(e){
        
			default_val = jQuery(this).attr('default');
			qty = jQuery(this).val();
			if(qty == 0 ){
				alert('Please Refund the item.');
				jQuery(this).val(default_val);
				e.preventDefault();
			}
			else if(qty > default_val){
				alert('Quantity cant be increased');
				jQuery(this).val(default_val);
				e.preventDefault();
			}
			
		});
	});
</script>