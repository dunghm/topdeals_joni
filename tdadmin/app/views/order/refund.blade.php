<div >
  <div class="panel panel-info">
    <div class="panel-heading">
		Order No : {{$orderId}}
	    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
	</div>
    <div class="panel-body">
      
	  <div class="form-group">
		<label for="exampleInputEmail1" class="control-label"> {{ trans('order_refund_lang.deal') }}: </label>
		{{$title}}
	  </div>
	  
@if($optionTitle)	  
	  <div class="form-group">
		<label for="exampleInputEmail1" class="control-label"> {{ trans('order_refund_lang.option') }}: </label>
		{{$optionTitle}}
	  </div>
@endif	  
	 <div class="form-group">
		<label for="exampleInputEmail1" class="control-label"> {{ trans('order_refund_lang.total_amount') }}: </label>
		{{$totalAmount}} {{CURRENCY}}
	  </div>
	  
	  <div id="requiredError" ></div>
	   <div id="responseMessage" ></div>
	  <div class="form-group">
		<label for="exampleInputPassword1" class="control-label">{{ trans('order_refund_lang.refund_policy') }}:</label>
			<select name="refund" id="order-dialog-refund-id" class="form-control">
										
				<option value="0">{{ trans('order_refund_lang.select') }}</option>
				<?php
						if(is_array($refundOption) && count($refundOption)>0)
						{
							
							foreach ($refundOption as $key=>$val)
							{
								?>
								<option value="<?php echo $key; ?>"><?php echo $val; ?></option>
								<?php
							}
							
						} 
				?>
			</select>
			
			<input name="order_id" id="order_id" type="hidden" value="{{$orderId}}" />
		    <input name="refund_order_item" id="refund_order_item_id" type="hidden" value="{{$orderItemId}}" />
			<input name="amount" id="amount" type="hidden" value="{{$price}}" />
	</div>
	<div class="form-group">
		<label for="exampleInputEmail1" class="control-label"> {{ trans('order_refund_lang.reason') }}: </label>
		<input name="reason_refund" id="reason_refund" type="text" class="form-control" placeholder="{{ trans('order_refund_lang.reason') }}" />
	 </div>
	
	 <div class="form-group">
		<label for="exampleInputEmail1" class="control-label"> {{ trans('order_refund_lang.quantity') }}: </label>
		<input name="quantity" id="quantity" type="text" class="form-control" autocomplete="off" value="{{$quantity}}" onkeyup = "refundAmount()" placeholder="{{ trans('order_refund_lang.quantity') }}" />
	 </div>	
	 
	 <div class="form-group">
		<label for="exampleInputEmail1" class="control-label"> {{ trans('order_refund_lang.refund_amount') }}:</label>
		<span id="totalAmountSpan">{{moneyit($totalPaid)}}</span>{{CURRENCY}}
		<div id="quantityError" class="text-danger"></div>
	  </div>
	 
	<div class="form-group btn-region">
		<div  class="err-region-popup alert alert-danger" role="alert" style="display:none;"></div>
		<button class="btn btn-primary" type="submit" name="update" value="update" id="refund-order" onclick="orderRefund();">Refund</button>
	</div>
	<div class="wait-region-order-delivery text-success" style="display:none;">Please Wait...</div>
		

    </div>
  </div>
</div>







<script>
// order refund

function orderRefund()
{
	$('.wait-region-order-delivery').show();
	$('#refund-order').attr('disabled', 'disabled');
	
	var order_id 				= jQuery('#order_id').val();
	var refund_order_item_id 	= jQuery('#refund_order_item_id').val();
    var reason_refund 			= jQuery('#reason_refund').val();
    var refundpolicy			= jQuery('#order-dialog-refund-id').val();
	var refundQuantity			= jQuery('#quantity').val();
	
	var action_url = "<?php echo URL::to('admin/refund'); ?>/";
	

	if (order_id && refund_order_item_id)
	{ 
		queryData = 'orderId='+order_id;
		queryData += '&refundOrderItemId='+refund_order_item_id;
		queryData += '&reasonRefund='+reason_refund;
		queryData += '&refundPolicy='+refundpolicy;
		queryData += '&refundQuantity='+refundQuantity;
		
		 $.ajax({
				type: "GET",
				url: action_url,
				data: queryData,
				cache: false,
				success: function(response) 
				{
					 $('.wait-region-order-delivery').hide();
					 
					 response = jQuery.parseJSON(response);
					 
					 if (response.error)
					 {
						 $('#requiredError').html(response.error);
						 $('#requiredError').addClass( "alert alert-danger" );
					 }
					 else
					 {
						$('#requiredError').html('');
						$('#requiredError').removeClass( "alert alert-danger" );
					 }
					 
					 if(response.errorMessage)
					 {
						$('#requiredError').removeClass( "alert alert-success" );
						$('#requiredError').html('');
						$('#requiredError').html(response.errorMessage);
						$('#requiredError').addClass("alert alert-danger");
					 }
					 
					 if(response.successMessage)
					 {
						$('#requiredError').removeClass( "alert alert-danger" );
						$('#requiredError').html('');
						$('#requiredError').html(response.successMessage);
						$('#requiredError').addClass("alert alert-success");
						//reload page
						window.location.reload(1);
						
						
					 }
					 else
					 {
						$('#refund-order').removeAttr('disabled');
					 }	
					 
				}
			});
	}	
 }
 
 
 function refundAmount()
 {
	
	var savedQuantity           = {{$quantity}};
	var orderfare           	= {{$orderfare}};
	var promoCode				= {{$promoCode}};
	var cashback				= {{$totalCashBackAmount}}
	var quantity 				= jQuery('#quantity').val();
	var amount 					= jQuery('#amount').val();
	
	if(quantity <= savedQuantity)	
	{
		$('#quantityError').text("");
		$('#refund-order').removeAttr('disabled');
		
		if(quantity)
		{
			var totalAmount = (amount*quantity);
			if(quantity ==savedQuantity)
			{
				totalAmount = totalAmount+orderfare;
			}
			
			if(promoCode)
			{
				promoCode = promoCode*quantity;
				totalAmount = totalAmount - promoCode;
			}
			
			if(cashback)
			{
				cashback = cashback*quantity;
				totalAmount = totalAmount - cashback;
			}
			
		}
		
		$('#totalAmountSpan').text(totalAmount); 
		
	}
	else
	{
		$('#quantityError').text("Quantity cannot be increased"); 
		$('#refund-order').attr('disabled', 'disabled');
	}	
 }
</script>
