<?php 
$newsletter	=	$data['newsletter'];
$newsletter_node = $newsletter['newsletter'];
$deals = $newsletter['deals'];
$teams = $newsletter['teams']; 
// $config = get_config_array();
//print_r($newsletter_node);
if(isset($export)){
    $export = true;
    $twitter_img_url = '/images/twitter.jpg';
    $facebook_img_url = '/images/fb.jpg';
    $mail_img_url = '/images/mail.jpg';
    $line_image_url = '/images/line.jpg';
}
else{
    $export = false;
    $twitter_img_url = URL::to('/').'/assets/images/twitter.jpg';
    $facebook_img_url = URL::to('/').'/assets/images/fb.jpg';
    $mail_img_url = URL::to('/').'/assets/images/mail.jpg';
    $line_image_url = URL::to('/').'/assets/images/line.jpg';
}
?>
<?php 
if($export == true){
    echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
</head>

<body>';
}

?>
<table border="0" cellpadding="0" cellspacing="0" style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 14px; line-height: 130%; width: 100%;">
	<tr>
		<td><table border="0" cellpadding="0" cellspacing="0" style="width: 600px;" align="center">
				<tr>
					<td align="center" style="padding-bottom: 10px;"><h1 style="margin: 0;">
                                                <a href="<?php echo URL::to('/'); ?>">
                                                    <?php 
                                                        
			    $logo_img = "top.png";          
				if($newsletter_node['team_category'] == 40){
					$logo_img = 'topkids.png';
				}else if($newsletter_node['team_category'] == 20){
					$logo_img = 'topbaby.png';
				}
				else if($newsletter_node['team_category'] == 19){
					$logo_img = 'topzen.png';
				}
				else if($newsletter_node['team_category'] == 8){
					$logo_img = 'topfun.png';
				}
				else if($newsletter_node['team_category'] == 11){
					$logo_img = 'topmaison.png';
				}else if($newsletter_node['team_category'] == 9){
					$logo_img = 'topgourmet.png';
				}
				
                                if($export == true){
                                    $logo_img_src = 'images/'.$logo_img;
                                }else{
                                    $logo_img_src = str_replace('tdadmin','',URL::to('/')).'/static/images/'.$logo_img;
                                }
								  
                                                    ?>
                                                    <img src="<?php echo $logo_img_src; ?>" height="60" alt="topzen" style="vertical-align: middle; border: 0;" /></a>
                                            </h1>
                                        </td>
				</tr>
				<!-- <tr>
					<td align="center" style="background-color: #1e1e1e; height: 80px; vertical-align: middle;"><h2 style="margin: 0;">
                                                <a href="#"><img src="../../img/jeudeal.jpg" width="365" height="43" alt="Le jeudi, c'est Topzen" style="vertical-align: middle; border: 0;" /></a><a href="http://www.topdeal.ch/deals/zen"></a></h2></td>
				</tr>
				
				<tr>
					<td style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; padding: 50px 25px 20px; text-align: center; background-color: #f5f3f0;">
						<h2 style="font-size: 26px; font-weight: 400; margin: 0 0 15px; line-height: 30px;">Un set de 10 huiles essentielles 100% naturelles Odesens accompagné d'un livre </h2>
                        <a href="http://www.topdeal.ch/deals/zen/set-10-huiles-essentielles-naturelles-odesens"><img src="../../img/featured_deal.png" width="550" height="368" alt="huiles essentielles odesens" /></a>
					  <p style="font-size: 16px; margin: 10px 0 20px; color: #1a1a1a;">Initiez-vous à l'aromathérapie et laissez-vous guider par le pouvoir des plantes.</p>
						<a href="http://www.topdeal.ch/deals/zen/set-10-huiles-essentielles-naturelles-odesens" style="font-size: 20px; padding: 10px 15px; color: #ffffff; background-color: #1e1e1e; display: inline-block; text-decoration: none;">Pour s'initier!</a>
					</td>
				</tr>
				<tr>
					<td style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; padding: 20px 25px 30px; text-align: center;">
						<h2 style="font-size: 50px; font-weight: 400; margin: 0 0 15px;"><a href="http://www.topdeal.ch/deals/zen"><img src="../../img/des-offres.jpg" width="550" height="96" alt="Des offres pratiques et astucieuses pour la maison" /></a></h2>						
					</td>
				</tr>-->
				<?php   
                                   foreach ($deals as $deal) { 
                                            $team = $teams[$deal['deal_id']];
                                            
                                ?>
				<tr>
					<td style="padding: 0 25px 30px;"><table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" align="center">
							<tr>								
								<td style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 16px; padding-right: 18px; vertical-align: top; text-align: center; color: #1a1a1a;"><?php echo $team->title;?></td>
                                                                <td style="text-align: left; vertical-align: middle;" width="326" rowspan="3"><a href="<?php echo GetTeamUrl($team); ?>"><img src="<?php if($export ==true){ $image_arr = explode('/', $team->image); echo 'images/'.$image_arr[count($image_arr)-1]; }else{ echo team_image($team->image);} ?>" width="326" height="218" alt="Lissage japonais Mike P" /></a></td>
							</tr>
							<tr>								
								<td style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 12px; padding-right: 18px;  vertical-align: middle; text-align: center; height: 145px"><span style="color: #EE0E1D; font-size: 14px;">Dès CHF <span style="font-size: 26px;"><?php echo moneyit($team->market_price, true); ?></span></span><br />
<i>au lieu de CHF <?php echo moneyit($team->team_price, true); ?></i></td>
							</tr>
							<tr>								
								<td style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; padding-right: 18px;  vertical-align: bottom; text-align: center;"><a href="<?php echo GetTeamUrl($team); ?>" style="font-size: 16px; padding: 10px 15px; color: #ffffff; background-color: #1e1e1e; display: inline-block; text-decoration: none;">Pour se faciliter la tâche!</a></td>
							</tr>
						</table></td>
				</tr>
								
				<tr>
					<td align="center">
						<img src="<?php echo $line_image_url; ?>" width="582" height="6" alt="" />
					</td>
				</tr>
                                   <?php } ?>
				<tr>
					<td align="center" style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; padding-right: 12px; color: #7c7c7c; padding-bottom: 10px;">
						<p>Rendez-vous sur <a href="http://www.topdeal.ch" target="_blank" style="color: #7c7c7c;"><strong>www.topdeal.ch</strong></a><br />
					    pour découvrir des offres originales pour toute la famille</p>
						<a href="https://twitter.com/topdealch" target="_blank"><img src="<?php echo $twitter_img_url; ?>" width="39" height="29" alt="" style="border: none" /></a>
						<a href="https://www.facebook.com/TopDeal.ch" target="_blank"><img src="<?php echo $facebook_img_url; ?>" width="39" height="29" alt="" style="border: none" /></a>
					  <forwardtoafriend><img src="<?php echo $mail_img_url; ?>" width="39" height="29" alt="" style="border: none" /></forwardtoafriend>
						<p style="margin-top: 0;">Vous désabonner  <unsubscribe style="text-decoration:none;" target="_blank"><span style="color:#7c7c7c">cliquez ici</span></unsubscribe></p>
					</td>
				</tr>
			</table></td>
	</tr>
</table>
<?php 
if($export == true){
    echo "</body></html>";
}

?>