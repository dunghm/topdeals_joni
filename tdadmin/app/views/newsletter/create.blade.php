<div class="container-fluid">
	<div class="row">
		<style>
		  .sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
		  .sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; }
		  .sortable li span { position: absolute; margin-left: -1.3em; }
		  </style>
		<div class="col-xs-12">
			<div class="portlet pd-30">
				<div class="page-heading">{{ trans('localization.newsletter_create') }}</div>
				<form id="create-form" role="form" method="post" action="<?php echo URL::to('admin/newsletter/save'); ?>">
				  <div class="box-body">
					<div class="form-group">
					  <label for="exampleInputEmail1">{{ trans('localization.title') }}</label>
					  <input type="text" class="form-control" required="required" id="title" name="title" placeholder="{{ trans('localization.title') }}" value="">
					</div>
					<div class="form-group">
						<label>{{ trans('localization.newsletter_type') }}</label>
						<select name="newsletterType"  id="newsletterType" class="form-control">
							<option>Daily</option>
							<option>Weekly</option>
						</select>
					 </div>
					  <div class="form-group"  >
						<label>{{ trans('localization.select_category') }}</label>
						
						<select name="deal_category" id="deal_category" class="form-control">
								<?php
									if(isset($data['getAllCategory']) && !empty($data['getAllCategory'])){
										$getAllCategory	=	$data['getAllCategory'];
										foreach($getAllCategory as $getAllCategoryRow){
								?>
									<option value="<?php echo $getAllCategoryRow->id;?>"><?php echo $getAllCategoryRow->name;?></option>
								<?php
									
										}
									}
								?>
						</select>
					</div>
					<div class="form-group">
                        <button id="add-deal"  type="button" class="btn  btn-info">{{ trans('localization.add_deal') }}</button>
                    </div>
					 <div class="form-group">
                          <ul class="sortable" id="deal-list"></ul>
                     </div>
				  </div><!-- /.box-body -->

				  <div class="box-footer">
					<span class='frm-btn-region'>
						<button id="preview-newsletter-edit" href="<?php echo URL::to('/').'/admin/newsletter_preview'; ?>" type="button" class="btn btn-primary">{{ trans('localization.preview') }}</button>  
						<button id="newsletter-create-btn" type="submit" class="btn btn-primary">{{ trans('warehouse_lang.submit') }}</button>
					</span>
					<span class='waiting-region' style='display:none;'>
						Please Wait...
					</span>
                  </div>
				  <div class="alert alert-danger" role="alert" id='err-region' style="display:none;"></div>
				</form>
			</div>
		</div>
	</div>
</div>
   <div class="modal" id="add-deal-modal">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
			<h4 class="modal-title">Select Deals</h4>
		  </div>
		  <div class="modal-body">
			
			  <div class="form-group">
				  <label>Select deals</label>
				  <select id="deal_titles" class="form-control">
					  <option>--</option>
				  </select>
			  </div>  
		  </div>
			
		  <div class="modal-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
			<button type="button" id="add-deal-continue-btn" class="btn btn-primary">Add deal and continue</button>
			<button type="button" id="add-deal-exit-btn" class="btn btn-primary">Add deal and exit</button>
		  </div>
		</div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
<script>
	var action_url 	= "<?php echo URL::to('admin/getdealbycategory'); ?>/";
	jQuery(document).ready(function(){
		
		jQuery(".sortable").sortable();
		
		jQuery('#deal_category').on('change',function(){
			getDealByCategory(action_url);
		});
		getDealByCategory(action_url);

		jQuery(document).on("click", "#add-deal", function(e) {
			jQuery('#add-deal-modal').modal('show'); 
		});
		
		//ADD DEAL AND CONTINUE WITH DIALOG
		
		 jQuery(document).on("click", "#add-deal-continue-btn", function(e) {
			addDealAndContinue(action_url);
		});
		
		//ADD DEAL AND CLOSE DIALOG
		jQuery(document).on("click", "#add-deal-exit-btn", function(e) {
			addDealAndClose(action_url);
		});
	
		//VALIDATE FORM
		jQuery('#newsletter-create-btn').on('click',function(){
			
			var title	=	jQuery.trim(jQuery('#title').val());
			var errMessage	=	"";
			
			if(title==""){
				errMessage	+= 'Newsletter title is required<br/>';
			}
			
			if(jQuery('.deals-hidden').length == 0){
				errMessage	+= 'Please add a deal in newsletter<br/>';
			}
			
			if(errMessage!=""){
				jQuery('#err-region').html(errMessage);
				jQuery('#err-region').css('display','block');
				return false;
			}else{
				jQuery('#err-region').html('');
				jQuery('#err-region').css('display','none');
				
				//SUBMIT FORM
				jQuery('.frm-btn-region').css('display','none');
				jQuery('.waiting-region').css('display','block');
				
				return true;
			}
		});
	});
	

</script>
