<script src="<?php echo URL::asset('assets/datatable/jquery.dataTables.js');?>"></script>
<script src="<?php echo URL::asset('assets/yadcf/jquery_datatables_yadcf.js');?>"></script>
<style>
div.dataTables_length select { display: none; }
.dataTables_filter { display: none }
</style>
@if(Session::has('successMessage'))
    <div class="alert alert-success">
        {{ Session::get('successMessage') }}
    </div>
@endif

@if(Session::has('errorMessage'))
    <div class="alert alert-danger">
        {{ Session::get('errorMessage') }}
    </div>
@endif		

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<div class="portlet pd-30">
				<div class="page-heading">
					{{ trans('localization.newletter_template') }}
					<a type="button" style="margin-left: 50px; width:100px;" href="<?php echo URL::to('/').'/admin/newsletter/create'; ?>" class="btn btn-primary btn-lg">Add new</a>
				</div>
					
				<div class="ho-lala">
				  {{ Datatable::table()
					  ->addColumn(trans('localization.id'), trans('localization.title'), trans('localization.category'), trans('localization.update_time'),trans('localization.operation'))
					  ->setUrl(route('api.newsletter'))
					  ->setOptions('order', array([0 ,"desc"]))
					  ->render()
				  }}
				</div>
			</div>
		</div>
	</div>
</div>


<script>
  $(document).ready(function(){
    var oTable = $('.table').dataTable().yadcf([
  	  	{column_number : 0, filter_type: "text"},
  	  	{column_number : 1, filter_type: "text"},
		{column_number : 2, filter_type: "text"}
    ]);
	  var oSettings = oTable.fnSettings();
	  oSettings._iDisplayLength = 50;
	  oTable.fnDraw();
	  $('.dataTables_wrapper').find('select').val(50).text();
    $('table').removeClass().addClass('tt-table dataTable laravel-table').wrap('<div class="table-responsive"></div>');
    $('.dataTables_wrapper').wrap('<div class="portlet pd-30"></div>');
    $('.yadcf-filter').addClass('tt-form-control');
    $(".dataTables_length").addClass('clearfix');
  });
  
</script>