<?php
$newsletter			= $data['newsletter'];
$newsletter_node 	= $newsletter['newsletter'];
$deals 				= $newsletter['deals'];
$teams 				= $newsletter['teams'];

//DEFAULT PARAMETER
$newsletter_name		=	"";
$selectedTeamCategory	=	"";
$newsLetterId			=	"";
$newsletter_type		=	"";

if(!empty($newsletter_node)){
	$newsletter_name		=	$newsletter_node['newsletter_name'];
	$selectedTeamCategory	=	$newsletter_node['team_category'];
	$newsletter_type		=	$newsletter_node['newsletter_type'];
	$newsLetterId			=	$newsletter_node['id'];
}
?>
<div class="container-fluid">
	<div class="row">
		<style>
		  .sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
		  .sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; }
		  .sortable li span { position: absolute; margin-left: -1.3em; }
		  </style>
		<div class="col-xs-12">
			<div class="portlet pd-30">
				<div class="page-heading">{{ trans('localization.newsletter_update') }}</div>
				<form id="create-form" role="form" method="post" action="<?php echo URL::to('admin/newsletter/save'); ?>">
				  <div class="box-body">
					<div class="form-group">
					  <label for="exampleInputEmail1">{{ trans('localization.title') }}</label>
					  <input type="text" class="form-control" required="required" id="title" name="title" placeholder="{{ trans('localization.title') }}" value="<?php echo $newsletter_name;?>">
					</div>
					<div class="form-group">
						<label>{{ trans('localization.newsletter_type') }}</label>
						<select name="newsletterType"  id="newsletterType" class="form-control">
							<?php
								$newsletterTypeArr	=	array('Daily','Weekly');
								foreach($newsletterTypeArr as $newsletterTypeRow){
									$selected	=	"";
									if($newsletter_type == $newsletterTypeRow){ $selected = "selected='selected'";}
							?>
								<option <?php echo $selected; ?>><?php echo $newsletterTypeRow;?></option>
							<?php
								}
							?>
						</select>
					 </div>
					  <div class="form-group"  >
						<label>{{ trans('localization.select_category') }}</label>
						
						<select name="deal_category" id="deal_category" class="form-control">
								<?php
									if(isset($data['getAllCategory']) && !empty($data['getAllCategory'])){
										$getAllCategory	=	$data['getAllCategory'];
										foreach($getAllCategory as $getAllCategoryRow){
											
											$selected = "";
											if($selectedTeamCategory == $getAllCategoryRow->id){
												$selected = "selected='selected'";
											}
								?>
									<option <?php echo $selected;?> value="<?php echo $getAllCategoryRow->id;?>"><?php echo $getAllCategoryRow->name;?></option>
								<?php
									
										}
									}
								?>
						</select>
					</div>
					<div class="form-group">
                        <button id="add-deal"  type="button" class="btn  btn-info">{{ trans('localization.add_deal') }}</button>
                    </div>
					 <div class="form-group">
                          <ul class="sortable" id="deal-list">
							<?php foreach ($deals as $deal) { 
									$team = $teams[$deal['deal_id']];
									?>
								<li id="deal-<?php echo $team->id; ?>" class="ui-state-default">
								<input class="deals-hidden" name="deals[]" type="hidden" value="<?php echo $team->id; ?>" />
								<div class="box box-solid box-default"> 
									<div class="box-header">
										<h3 class="box-title"><?php echo $team->title; ?></h3>
										<div class="box-tools pull-right">
											<button id="<?php echo $deal['deal_id']; ?>" type="button" class="btn btn-box-tool deal-remove">
													<i class="fa fa-close"></i>
											</button>

										</div>
									</div>
									<div class="box-body">
									<table border="0" cellpadding="0" cellspacing="0" style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 14px; line-height: 130%; width: 100%;">
										<tr>
											<td style="padding: 0 25px 30px;"><table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" align="center">
													<tr>								
														<td style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 16px; padding-right: 18px; vertical-align: top; text-align: center; color: #1a1a1a;">
															Un lissage japonais ou un soin B&#244;-Toxe capillaire chez Mike P
														</td>
														<td style="text-align: left; vertical-align: middle;" width="326" rowspan="3">
															<a href="<?php echo GetTeamUrl($team); ?>">
																<img src="<?php echo team_image($team->image); ?>" width="326" height="218" alt="Lissage japonais Mike P" />
															</a>
														</td>
													</tr>
													<tr>								
														<td style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 12px; padding-right: 18px;  vertical-align: middle; text-align: center; height: 145px"><span style="color: #EE0E1D; font-size: 14px;">D&#232;s CHF &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size: 26px;"><?php echo moneyit($team->market_price, true); ?></span><br />
																<i>au lieu de CHF <?php echo moneyit($team->team_price, true); ?></i></td>
													</tr>
													<tr>								
														<td style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; padding-right: 18px;  vertical-align: bottom; text-align: center;"><a href="<?php echo GetTeamUrl($team); ?>" style="font-size: 16px; padding: 10px 15px; color: #ffffff; background-color: #1e1e1e; display: inline-block; text-decoration: none;">Pour se faciliter la t&#226;che!</a></td>
													</tr>
												</table></td>
										</tr>
									</table>
									</div>
								</div>
								</li>
								<?php } ?> 
						  </ul>
                     </div>
				  </div><!-- /.box-body -->

				  <div class="box-footer">
				   <input type="hidden" name="newsLetterId" value="<?php echo $newsLetterId;?>" />
				   <span class='frm-btn-region'>
                    <button id="preview-newsletter-edit" href="<?php echo URL::to('/').'/admin/newsletter_preview'; ?>" type="button" class="btn btn-primary">{{ trans('localization.preview') }}</button>  
                    <button id="newsletter-create-btn" type="submit" class="btn btn-primary">{{ trans('localization.update') }}</button>
					</span>
					<span class='waiting-region' style='display:none;'>
						Please Wait...
					</span>
                  </div>
				  <div class="alert alert-danger" role="alert" id='err-region' style="display:none;"></div>
				</form>
			</div>
		</div>
	</div>
</div>
   <div class="modal" id="add-deal-modal">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
			<h4 class="modal-title">Select Deals</h4>
		  </div>
		  <div class="modal-body">
			
			  <div class="form-group">
				  <label>Select deals</label>
				  <select id="deal_titles" class="form-control">
					  <option>--</option>
				  </select>
			  </div>  
		  </div>
			
		  <div class="modal-footer">
			<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
			<button type="button" id="add-deal-continue-btn" class="btn btn-primary">Add deal and continue</button>
			<button type="button" id="add-deal-exit-btn" class="btn btn-primary">Add deal and exit</button>
		  </div>
		</div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
<script>
	var action_url 	= "<?php echo URL::to('admin/getdealbycategory'); ?>/";
	jQuery(document).ready(function(){
		
		jQuery(".sortable").sortable();
		
		jQuery('#deal_category').on('change',function(){
			getDealByCategory(action_url);
		});
		getDealByCategory(action_url);

		jQuery(document).on("click", "#add-deal", function(e) {
			jQuery('#add-deal-modal').modal('show'); 
		});
		
		//ADD DEAL AND CONTINUE WITH DIALOG
		
		 jQuery(document).on("click", "#add-deal-continue-btn", function(e) {
			addDealAndContinue(action_url);
		});
		
		//ADD DEAL AND CLOSE DIALOG
		jQuery(document).on("click", "#add-deal-exit-btn", function(e) {
			addDealAndClose(action_url);
		});
	
		//VALIDATE FORM
		jQuery('#newsletter-create-btn').on('click',function(){
			
			var title	=	jQuery.trim(jQuery('#title').val());
			var errMessage	=	"";
			
			if(title==""){
				errMessage	+= 'Newsletter title is required<br/>';
			}
			
			if(jQuery('.deals-hidden').length == 0){
				errMessage	+= 'Please add a deal in newsletter<br/>';
			}
			
			if(errMessage!=""){
				jQuery('#err-region').html(errMessage);
				jQuery('#err-region').css('display','block');
				return false;
			}else{
				jQuery('#err-region').html('');
				jQuery('#err-region').css('display','none');
				
				//SUBMIT FORM
				jQuery('.frm-btn-region').css('display','none');
				jQuery('.waiting-region').css('display','block');
				
				return true;
			}
		});
	});
	

</script>
