<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js tt-toolbar"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>Topdeal - {{ isset($title) ? ' | '.$title : '' }}</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="<?=URL::asset('assets/css/bootstrap.min.css');?>">
	<link rel="stylesheet" href="<?=URL::asset('assets/css/main.css');?>">
	<script src="<?=URL::asset('assets/js/modernizr-2.6.2-respond-1.1.0.min.js');?>"></script>
	<link rel="stylesheet" href="<?=URL::asset('assets/font-awesome/css/font-awesome.min.css');?>">
</head>
<body class="">
<!--[if lt IE 7]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->
<div id="tt-wrap" class="clearfix">