<!DOCTYPE html>
<html>
<head>
	<title>..:: Laravel Training ::..</title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<nav class="navbar navbar-inverse">
			<ul class="nav navbar-nav">
				<li><a href="<?=URL::to('event')?>">View All Events</a></li>
				<li><a href="<?=URL::to('event/create')?>">Create new Event</a>
			</ul>
		</nav>
	</div>
</body>
</html>