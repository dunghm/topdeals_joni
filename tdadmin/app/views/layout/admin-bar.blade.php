<?php if ( Request::Segment(1) !="reset" ) { ?>

<?php
	$auth = Auth::user();
	if ( isset($auth) )
	{
?>
<div id="tt-adminbar" class="hidden-print">
    <div id="collapse-menu"> <span class="fa fa-bars"></span> </div>
    <div class="nav-header">
       
       <div class="nav-home-link"> <a href="{{URL::to('/')}}" >
           <i class="fa fa-home"></i></a> 
        </div>
        
        <div class="nav-user"> <a class="dropdown-toggle user-link" data-toggle="dropdown" href="#"> 
        <?php
				if(Auth::user()->username){
					echo 'Welcome : '.Auth::user()->username.'!';
				}else{
					echo 'Welcome : Guest!';
				}
				
				$accessRole	=	"partner";
				if(Session::has('user_type')){
					$accessRole = Session::get('user_type');
				}
				
		?>
			<span class="user-info">{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</span> <span class="fa fa-caret-down"></span> </a>
            <ul class="pull-right user-menu dropdown-menu dropdown-caret">
            	<?php if( $accessRole=="partner"){ ?>
				<li><a href="{{ URL::to($accessRole.'/edit-profile') }}">Edit Profile</a></li>         
				<li><a href="{{ URL::to($accessRole.'/edit-password') }}">Change Password</a></li>   
				<?php } ?>
                <li><a href="{{ URL::to($accessRole.'/logout') }}">Log Out</a></li>
            </ul>
		
        </div>
    </div>
    <!-- .nav-header -->
</div>
<!-- .tt-adminbar -->
<?php } ?>
<?php } ?>