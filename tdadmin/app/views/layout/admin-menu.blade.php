<?php if ( Request::Segment(1) !="reset" ) { ?>

<?php
	$auth = Auth::user();
	if ( isset($auth) )
	{
		$userType	=	"partner";
		if(Session::has('user_type')){
			$userType	=	Session::get('user_type');
		}
?>
	<div id="adminmenuback" class="hidden-print"></div>
<div id="adminmenu-wrap" class="hidden-print">
	<div id="main-logo"><a href="{{ TOPDEAL_MANAGE_URL.'misc' }}"><img class="w80p" src="<?php echo URL::to('/')."/assets/img/logo-white.png";?>"></img></a></div>
	<ul id="adminmenu">
	
		<?php
			if($userType=="partner"){
		?>
		 <li class="menu-top {{ ($title == trans('localization.DealTitle') ) ? 'current' : '' }}" id="menu-customers">
            <a data-toggle="tooltip" title="{{ trans('localization.deals') }}" href="{{ Url::to($userType.'/deals') }}" class="menu-top" target="<?php echo (Session::get('newtab')==1) ? '_blank' : 
               '_self'?>">
                <div class="tt-menu-arrow"><span class="fa fa-angle-right"></span></div>
                <div class="tt-menu-image"><span class="icon icon-dashboard"></span></div>
                <div class="tt-menu-name">{{ trans('localization.deals') }}</div>
            </a>
        </li>
		 <li class="menu-top {{ ($title == trans('localization.CouponList')) ? 'current' : '' }}" id="menu-customers">
            <a data-toggle="tooltip" title="{{ trans('localization.coupon') }}" href="{{ Url::to($userType.'/coupons') }}" class="menu-top" target="<?php echo (Session::get('newtab')==1) ? '_blank' : 
               '_self'?>">
                <div class="tt-menu-arrow"><span class="fa fa-angle-right"></span></div>
                <div class="tt-menu-image"><span class="icon icon-events"></span></div>
                <div class="tt-menu-name">{{ trans('localization.coupon') }}</div>
            </a>
        </li>
		<?php
			}
		?>
		
		<?php
			if($userType=="admin"){
		?>
		
			<li class="menu-top {{ ($title == trans('localization.dashboard')) ? 'current' : '' }}" id="menu-customers">
			 <a data-toggle="tooltip" title="{{ trans('localization.dashboard') }}" href="{{ Url::to($userType.'/dashboard') }}" class="tooltip-info" >
			  <div class="tt-menu-arrow"><span class="fa fa-angle-right"></span></div>
			  <div class="tt-menu-image"><span class="icon icon-dashboard"></span></div>
			  <div class="tt-menu-name">{{ trans('localization.dashboard') }}</div>
			 </a>
			</li>
			
			<!-- Supplier Email List -->
			<li class="menu-top {{ ($title == trans('supplieremail_lang.title') ) ? 'current' : '' }}" id="menu-customers">
				<a data-toggle="tooltip" title="{{ trans('supplieremail_lang.title') }}" href="{{ Url::to($userType.'/supplieremail') }}" class="menu-top">
					<div class="tt-menu-arrow"><span class="fa fa-angle-right"></span></div>
					<div class="tt-menu-image"><span class="glyphicon glyphicon-tasks"></span></div>
					<div class="tt-menu-name">{{ trans('supplieremail_lang.title') }}</div>
				</a>
			</li>
			<!--// Supplier Email List -->
			
			<li class="menu-top tt-has-submenu {{ ($title == trans('trigger_lang.trigger_setting'))? 'current' : '' }}" id="menu-customers">
				<a data-toggle="tooltip" title="{{ trans('trigger_lang.trigger') }}" href="#" class="menu-top" target="_self">
					<div class="tt-menu-arrow"><span class="fa fa-angle-right"></span></div>
					<div class="tt-menu-image"><span class="glyphicon glyphicon-cog"></span></div>
					<div class="tt-menu-name">{{ trans('trigger_lang.trigger') }}</div>
				</a>
				<ul class="tt-submenu">
					<li class="tt-submenu-head">{{ trans('trigger_lang.trigger') }}</li>
					<li class="{{ ($title == trans('trigger_lang.trigger_setting')) ? 'current' : '' }}"><a href="{{ Url::to($userType.'/trigger') }}" target="_self">{{ trans('trigger_lang.trigger_setting') }}</a></li>
					<li class="{{ ($title == trans('trigger_lang.trigger_action')) ? 'current' : '' }}"><a href="{{ Url::to($userType.'/triggerAction') }}" target="_self">{{ trans('trigger_lang.trigger_action') }}</a></li>
				</ul>
			</li>
			<li class="menu-top {{ ($title == trans('emailconfig_lang.title')) ? 'current' : '' }}" id="menu-customers">
			 <a data-toggle="tooltip" title="{{ trans('emailconfig_lang.title') }}" href="{{ Url::to($userType.'/emailconfiguration') }}" class="menu-top" target="<?php echo (Session::get('newtab')==1) ? '_blank' : 
				'_self'?>">
			  <div class="tt-menu-arrow"><span class="fa fa-angle-right"></span></div>
			  <div class="tt-menu-image"><span class="icon icon-customers"></span></div>
			  <div class="tt-menu-name">{{ trans('emailconfig_lang.settings') }}</div>
			 </a>
			</li>
			<!-- sub-menu-->
				<li class="menu-top tt-has-submenu {{ ($title == trans('warehouse_lang.title')) ||
													($title == trans('warehouse_lang.createsection')) ||		
													($title == trans('warehouse_lang.assignstock'))? 'current' : '' }}" id="menu-customers">
				<a data-toggle="tooltip" title="{{ trans('warehouse_lang.warehouse') }}" href="#" class="menu-top" target="_self">
					<div class="tt-menu-arrow"><span class="fa fa-angle-right"></span></div>
					<div class="tt-menu-image"><span class="glyphicon glyphicon-hdd"></span></div>
					<div class="tt-menu-name">{{ trans('warehouse_lang.warehouse') }}</div>
				</a>
				<ul class="tt-submenu">
					<li class="tt-submenu-head">{{ trans('warehouse_lang.warehouse') }}</li>
					<li class="{{ ($title == trans('warehouse_lang.title')) ? 'current' : '' }}"><a href="{{ Url::to($userType.'/warehousesection') }}" target="_self">{{ trans('warehouse_lang.list') }}</a></li>					
					<li class="{{ ($title == trans('warehouse_lang.createsection')) ? 'current' : '' }}"><a href="{{ Url::to($userType.'/warehousesectioncreate') }}" target="_self">{{ trans('warehouse_lang.createsection') }}</a></li>					
					<li class="{{ ($title == trans('warehouse_lang.assignstock')) ? 'current' : '' }}"><a href="{{ Url::to($userType.'/warehousesectionassignstock') }}" target="_self">{{ trans('warehouse_lang.assignstock') }}</a></li>
				</ul>
			</li>
			
			<!-- NEWSLETTER MENU-->
			<li class="menu-top tt-has-submenu {{ ($title == trans('localization.newletter_title'))||
													($title == trans('localization.newsletter_create')) ||
													($title == trans('localization.newsletter_update'))
													? 'current' : '' }}" id="menu-customers">
				<a data-toggle="tooltip" title="{{ trans('localization.newletter_title') }}" href="#" class="menu-top" target="_self">
					<div class="tt-menu-arrow"><span class="fa fa-angle-right"></span></div>
					<div class="tt-menu-image"><span class="glyphicon glyphicon-envelope"></span></div>
					<div class="tt-menu-name">{{ trans('localization.newletter_title') }}</div>
				</a>
				<ul class="tt-submenu">
					<li class="tt-submenu-head">{{ trans('localization.newletter_title') }}</li>
					<li class="{{ ($title == trans('localization.newsletter_list')) ? 'current' : '' }}"><a href="{{ Url::to($userType.'/newsletter') }}" target="_self">{{ trans('localization.newsletter_list') }}</a></li>
					
					<li class="{{ ($title == trans('localization.newsletter_create')) ? 'current' : '' }}"><a href="{{ Url::to($userType.'/newsletter/create') }}" target="_self">{{ trans('localization.newsletter_create') }}</a></li>
				</ul>
			</li>
			<!-- NEWSLETTER MENU END-->
			<!-- ORDER MENU-->
			<li class="menu-top tt-has-submenu {{ ($title == trans('localization.paid_order'))
													? 'current' : '' }}" id="menu-customers">
				<a data-toggle="tooltip" title="{{ trans('localization.paid_order') }}" href="#" class="menu-top" target="_self">
					<div class="tt-menu-arrow"><span class="fa fa-angle-right"></span></div>
					<div class="tt-menu-image"><span class="glyphicon  glyphicon-shopping-cart"></span></div>
					<div class="tt-menu-name">{{ trans('localization.paid_order') }}</div>
				</a>
				<ul class="tt-submenu">
					<li class="tt-submenu-head">
						{{ trans('localization.order') }}
					</li>
					<li class="{{ ($title == trans('localization.paid_order')) ? 'current' : '' }}"><a href="{{ Url::to($userType.'/orderlist') }}" target="_self">{{ trans('localization.paid_order') }}</a></li>
				</ul>
			</li>
			<!-- ORDER MENU END-->
<!--manage menu -->			
			<!--deals-->
			<li class="menu-top " id="menu-customers">
			 <a data-toggle="tooltip" title="{{ trans('localization.deals') }}" href="{{TOPDEAL_MANAGE_URL.'team'}}" class="menu-top" target="<?php echo (Session::get('newtab')==1) ? '_blank' : 
				'_self'?>">
			  <div class="tt-menu-arrow"><span class="fa fa-angle-right"></span></div>
			  <div class="tt-menu-image"><span class="glyphicon glyphicon-tag"></span></div>
			  <div class="tt-menu-name">{{ trans('localization.deals') }}</div>
			 </a>
			</li>
			<!--//deals-->
			
			<!--Order-->
			<li class="menu-top " id="menu-customers">
			 <a data-toggle="tooltip" title="{{ trans('localization.orders') }}" href="{{TOPDEAL_MANAGE_URL.'order'}}" class="menu-top" target="<?php echo (Session::get('newtab')==1) ? '_blank' : 
				'_self'?>">
			  <div class="tt-menu-arrow"><span class="fa fa-angle-right"></span></div>
			  <div class="tt-menu-image"><span class="glyphicon glyphicon-book"></span></div>
			  <div class="tt-menu-name">{{ trans('localization.orders') }}</div>
			 </a>
			</li>
			<!--//Order-->
			
			<!--users-->
			<li class="menu-top " id="menu-customers">
			 <a data-toggle="tooltip" title="{{ trans('localization.users') }}" href="{{TOPDEAL_MANAGE_URL.'user'}}" class="menu-top" target="<?php echo (Session::get('newtab')==1) ? '_blank' : 
				'_self'?>">
			  <div class="tt-menu-arrow"><span class="fa fa-angle-right"></span></div>
			  <div class="tt-menu-image"><span class="glyphicon glyphicon-user"></span></div>
			  <div class="tt-menu-name">{{ trans('localization.users') }}</div>
			 </a>
			</li>
			<!--//users-->
			
			
<!--//manage menu -->			
			
			<!-- //sub-menu-->
		<?php
			}
		?>
		

	</ul>
</div>
<!-- #adminmenu-wrap -->
	<?php } ?>
<?php } ?>


<script>
/*$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});*/
</script>