  </div><!-- #tt-wrap -->
  
  <script src="<?=URL::asset('assets/js/jquery-ui.js');?>"></script>
  <script src="<?=URL::asset('assets/js/bootstrap.min.js');?>"></script>
  <script src="<?=URL::asset('assets/js/bootstrapValidator.js');?>"></script>
  <script src="<?=URL::asset('assets/js/bootstrap-select.min.js');?>"></script>
  <script src="<?=URL::asset('assets/js/bootstrap-datepicker.js');?>"></script>
  <script src="<?=URL::asset('assets/js/plugins.js');?>"></script>
  <script src="<?=URL::asset('assets/js/jquery.ba-throttle-debounce.min.js');?>"></script>
  <script src="<?=URL::asset('assets/js/jquery.stickyheader.js');?>"></script>
  <script src="<?=URL::asset('assets/js/main.js').VER_JS;?>"></script>
  
  <!-- DASHBOARD JS START-->
  <script  href="<?php echo URL::asset('assets/dashboard/jQuery-slimScroll/jquery-ui-1.9.2.custom.min.js').VER_JS;?>" ></script>
    <script  href="<?php echo URL::asset('assets/dashboard/jQuery-slimScroll/slimScroll.js').VER_JS;?>" ></script>
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="<?php echo URL::asset('assets/dashboard/js/excanvas.js').VER_JS;?>"></script>	
	<script src="<?php echo URL::asset('assets/dashboard/js/respond.js').VER_JS;?>"></script>
	<![endif]-->	
	<script src="<?php echo URL::asset('assets/dashboard/js/jquery.peity.min.js').VER_JS;?>"></script>	
	<script src="<?php echo URL::asset('assets/dashboard/chosen/chosen.jquery.js').VER_JS;?>"></script>
	<script src="<?php echo URL::asset('assets/dashboard/js/custom.js').VER_JS;?>"></script>
	<script src="<?php echo URL::asset('assets/dashboard/flot/jquery.flot.js').VER_JS;?>"></script>
	<!-- DASHBOARD JS END-->
	
  <!-- DATETIME PICKER START-->
  <script src="<?php echo URL::asset('assets/js/datetimepicker/moment/min/moment-with-locales.js');?>"></script>
  <script src="<?php echo URL::asset('assets/js/datetimepicker/bootstrap-datetimepicker.min.js');?>"></script>
  <link rel="stylesheet" href="<?php echo URL::asset('assets/css/datetimepicker/bootstrap-datetimepicker.min.css');?>">      
   <!-- DATETIME PICKER END-->
  <script>
 $(document).ready(function(){

	 

	
	
  //Initiate Custom Date Picker - http://bootstrap-datepicker.readthedocs.org/en/release/
  // $('.input-group.date, .input-daterange').datepicker({
    // autoclose: true
  // });


    //adding custom classes in Data Table for UI customization
   
    $(".dataTables_length select").addClass('selectpicker show-tick show-menu-arrow');
    $('.dataTables_length select').selectpicker();
    $('.dataTables_info').parent('.col-xs-6').parent('.row').addClass('dataTables_info_wrap');
    $('.dataTables_length').parent('.col-xs-6').parent('.row').addClass('dataTables_length_wrap'); 


  //Initiate Custom Select Picker
  $('.selectpicker').selectpicker();
  stickyTable();
 
 
  });
  
  $(document).on('hide.bs.modal','#confRModal', function () {
        $( "#alert-message" ).addClass(" alert-info ");
		$( "#alert-message" ).removeClass("alert-success");
		$( "#alert-message" ).removeClass("alert-danger");
 //Do stuff here
});
</script>
  
<!-- Coupon validator Modal -->
<div class="modal fade" id="confRModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
<!--Header -->
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">{{trans('coupon_lang.coupon_validate_button')}}</h4>
		 </div>
<!--//Header -->		 
		 <div class="modal-body">
			<div id="order-pay-dialog" class="order-pay-dialog-c" >
			<div role="alert" class="alert alert-info coupon-validator-region" id="alert-message">
			  {{trans('coupon_lang.coupon_validate_heading')}}
			</div>
			<div class="form-group">
				<label for="exampleInputFile">{{trans('coupon_lang.coupon_validate_no_good')}}</label>
				<input id="coupon-dialog-input-id" value="" type="text" name="id" class="form-control" style="text-transform:uppercase;" maxLength="12" onkeyup="X.coupon.dialoginputkeyup(this);" />
			</div>
			  
			<div class="form-group">
				<label for="exampleInputFile">{{trans('coupon_lang.coupon_validate_pin_good')}}</label>
				<input id="coupon-dialog-input-secret" value="" type="text" name="secret" style="text-transform:uppercase;" class="form-control" maxLength="8" onkeyup="X.coupon.dialoginputkeyup(this);" />

			</div> 
			  
			  <p class="act">    
				<input id="coupon-dialog-consume-more" name="consume" class="btn btn-primary" value="Validate"  ask=""  type="button" onclick="dialogconsume_more();"/>
			  </p>
			</div>     
		</div>
  </div>
</div>
<!--// Coupon validator Modal -->


<script>
function dialogconsume_more()
{
	 var action_url = "<?php echo URL::to('partner/coupon/couponvalidator').'/'?>";
	
	var id 				= $('#coupon-dialog-input-id').val();
	var secret 		    = $('#coupon-dialog-input-secret').val();
	
	
	if(id == "" && secret == "" ){
		return true;
	}
		/*var queryData = '';
	
 queryData = "id="+id;
	queryData += "&secret="+encodeURIComponent(secret);
	alert(action_url); */

	
	var data = {     // create object
            id    		: id,
            secret      : secret
        }
	
	jQuery.ajax({
		type:'POST',
		url: action_url,
		data: data,
		dataType: "html",
		async: false,
		cache: false,
		success: function(response)
		{
			var responseArray = response.split('--')
			if(responseArray[0])
			{
				// alert(JSON.parse(response));
				var responsePartner	=	jQuery.trim(JSON.parse(responseArray[0]));
				
				if(responsePartner != "")
				{
					jQuery('.coupon-validator-region').html(responsePartner);
				}
			}
			
			if(responseArray[1]=="alert-danger")
			{
				 $( "#alert-message" ).addClass(" alert-danger ");
				 $( "#alert-message" ).removeClass("alert-success");
				 $( "#alert-message" ).removeClass("alert-info");
			}
			else
			{
				$( "#alert-message" ).addClass(" alert-success ");
				$( "#alert-message" ).removeClass("alert-danger");
				$( "#alert-message" ).removeClass("alert-info");
			}
			
		}
	});
}


	
		// destroy the content of the modal upon hiding the modal
	jQuery('.modal').on('hidden.bs.modal', function (e) {
		// loader.hide();
		jQuery('#coupon-dialog-input-id').val('');
		jQuery('#coupon-dialog-input-secret').val('');
		jQuery('.coupon-validator-region').text('<?php echo trans('coupon_lang.coupon_validate_heading'); ?>');
		// jQuery(e.target).removeData('bs.modal').find(".modal-content").empty();
	});
</script>

<?php
	if(Session::has('consume_id')&&Session::has('consume_secret')){
?>
	<script>
		jQuery(window).load(function(){
			jQuery('#confRModal').modal('show');
			jQuery('#coupon-dialog-input-id').val('<?php echo Session::get('consume_id');?>');
			jQuery('#coupon-dialog-input-secret').val('<?php echo Session::get('consume_secret');?>');
		});
	</script>
<?php
		//UNSET SESSION	
		Session::forget('consume_id');
		Session::forget('consume_secret');
	
	}
?>
</body>
</html>