<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js tt-toolbar"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title>{{ isset($title) ? $title : 'Biz' }}</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="language" content="fr" />
	<link rel="stylesheet" href="<?php echo URL::asset('assets/css/combine.css').VER_CSS;?>">
	<link rel="stylesheet" href="<?php echo URL::asset('assets/css/fixed-header-component.css').VER_CSS;?>">
	<link rel="stylesheet" href="<?php echo URL::asset('assets/css/main.css').VER_CSS;?>">
    <link rel="stylesheet" href="<?php echo URL::asset('assets/css/newsletter.css').VER_CSS;?>">    
	
	<script src="<?php echo URL::asset('assets/js/modernizr-2.6.2-respond-1.1.0.min.js').VER_JS;?>"></script>
	<script src="<?php echo URL::asset('assets/js/jquery.min.js').VER_JS;?>"></script>
	<!-- 
		Nouman: Added cookie file
		https://github.com/carhartl/jquery-cookie/blob/master/src/jquery.cookie.js
	-->
	<script src="<?php echo URL::asset('assets/js/jquery.cookie.js').VER_JS;?>"></script>
	

	
	<link rel="shortcut icon" href="<?php echo URL::to('/')."/assets/img/favicon.ico";?>">
	
	<!-- SO Events view special css & js -->
	<link rel="stylesheet" href="<?php echo URL::asset('assets/css/basic.css').VER_CSS;?>">
	<script src="<?php echo URL::asset('assets/js/dropzone.js').VER_JS;?>"></script>
	<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
	
	<?php 
	/*
	 * Nouman: Feb09,2015
	 * Store collapse-menu event triggered in cookies
	 * TODO: Code can be optimized to be place on proper place if needed.
	 * */
	$collapse_menu_value = 0;
	if(isset($_COOKIE['collapse_menu'])){
		$collapse_menu_value = (int) $_COOKIE['collapse_menu']; // get collapse-menu value
	}
	
	$menu_states = array(0=>'unfolded', 1=>'folded');
	?>
	<script>
		jQuery(document).ready(function (){
			
			jQuery('#collapse-menu .fa-bars').click(function(){
				if(jQuery('body').hasClass('unfolded'))
					jQuery.cookie("collapse_menu", 1, {path: '/'});
				else
					jQuery.cookie("collapse_menu", 0, {path: '/'});
			});
			
		});
	</script>
	<!-- EO Events view special css & js -->

	
	
</head>
<body class="<?php echo $menu_states[$collapse_menu_value];?> folded">
<!--[if lt IE 7]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->
<div class="alert-container fade @if(Session::has('message')) in @endif">
	<div class="tt-alert tt-alert-success">
            @if(Session::has('message'))
				{{ Session::get('message') }}
            @endif
	</div>
</div>
<div class="alert-container fade @if(Session::has('errmessage')) in @endif">
	<div class="tt-alert tt-alert-error">
            @if(Session::has('errmessage'))
				{{ Session::get('errmessage') }}
            @endif
	</div>
</div>
<div id="tt-wrap" class="clearfix">
	@include ('layout/admin-menu')
	<div id="tt-content">
	    @include ('layout/admin-bar')
	    <div id="tt-body">
		<?php
			//IF SESSION MENTAIN THEN REDIRECTED TO ADMIN/PARTNER DASHBOARD
			if(Session::has('user_type')){
				$userType	=	Session::get('user_type');
				if( !empty($userType) && $userType=='partner' ){
				  
		?>
			<!--Coupon Validator-->
			<div class="check-conf">
				<span class="bs-tooltip " data-toggle="tooltip" data-placement="top" >
				 <a class="btn btn-success" href="#" data-toggle="modal" data-target="#confRModal">
				  <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>&nbsp;&nbsp;{{trans('coupon_lang.coupon_validate_button')}}
				 </a>
				</span>
			</div>
		<?php
				}
			}

		?>
		<!--//Coupon Validator-->
		<!-- NEWLETTER MODAL PREVIEW -->
		<style>
        .modal .modal-body {
			max-height: 820px;
			overflow-y: auto;
		}
        </style>
    <div class="modal" id="preview-modal">
        <div class="modal-dialog" style="width:850px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h4 class="modal-title" >
						Preview 
						<span class="preview-newletter-form">|</span> <span class="preview-newletter-form send-template-email"> Send Sample Email <i style="font-size:13px;" class="fa fa-chevron-down send-template-email-click-region"></i></span>
					</h4>
					<span class="preview-newletter-form" style="display:none;">
						<span class='send-test-mail-region' style="display:none;">
							<hr/>
							<form id="preview-newletter-form" method="post" action="<?php echo URL::to('admin/newsletter_sendEmail'); ?>">
								<input type="text" name="email_sent_to" style="width:200px; display:inline !important;" class="form-control" placeholder="Enter email" id="email_sent_to"/>
								<input type="hidden" name="newsletter_id" id="newsletter_id" />
								
								<span class='sbmt-btn-regionnewprev-form'>
									<input type="submit" class="btn btn-primary" value="Send Email" name="send_email" id="send_email" />
								</span>
								<span class='waiting-regionnewprev-form' style='display:none;'>
									Please Wait...
								</span>
								<span class="preview_email_err_region" style="display:none; color:red;"></span>
								<div style="color:grey; font-size:11px; padding:10px;">Note : For Multiple Recipient Use ',' Seprator. For Example :  abx@xyz.com , def@xyz.com </div>
							</form>
						</span>
					</span>
                </div>
                <div id="modal-body-preview" class="modal-body">
                    
                    
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
	
	
	<!-- COMMON MODAL PREVIEW -->
		<style>
        .modal .modal-body {
			max-height: 820px;
			overflow-y: auto;
		}
        </style>
    <div class="modal" id="modal-popup-region">
        <div class="modal-dialog" style="width:850px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <h4 class="modal-title" >
						Preview 
					</h4>
                </div>
                <div id="modal-body-popup" class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
