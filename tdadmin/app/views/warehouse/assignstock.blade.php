<!-- Content Wrapper. Contains page content -->
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<div class="portlet pd-30">
				<div class="page-heading">
					{{ trans('warehouse_lang.assignstocktowarehousesection') }}
					<span id="editteam" style="display:none"></span>
				</div>
				<div class="box-body">
@if(Session::has('successMessage'))
    <div class="alert alert-success">
        {{ Session::get('successMessage') }}
    </div>
@endif

@if(Session::has('errorMessage'))
    <div class="alert alert-danger">
        {{ Session::get('errorMessage') }}
    </div>
@endif						  
					  <div class="form-group">
							
							<label for="exampleInputEmail1">{{ trans('warehouse_lang.section') }}</label>
							<select name="warehouse_sections" id="warehouse_sections" class="form-control" onchange="getDealsBySection();">
								<option value="0">-{{ trans('warehouse_lang.select') }}-</option>
									<?php
											if(is_array($warehouseSection) && count($warehouseSection)>0)
											{
												
												foreach ($warehouseSection as $row)
												{
													$id = 0;
													$name = '';
													if(!empty($row->id))
													{
														$id = $row->id;
													}
													
													if(!empty($row->name))
													{
														$name = $row->name;
													}
													
													$selected = '';
													if($sectionId ==$id )
													{
														$selected = " selected = selected";
													}
													
													?>
													<option value="<?php echo $id; ?>" <?php echo $selected; ?>><?php echo $name; ?></option>
												<?php	
												}
												
											} 
									?>
							</select>
							<div id="pleasewait-section" class="pleasewait" style="display:none">{{ trans('warehouse_lang.pleasewait') }}</div>
					  </div>
		<!--Grid -->				
					 <div  class="row">
							<div id="section-grid" class="col-xs-12"></div>
					 </div>
		<!--//Grid -->				
					  <div class="form-group">
						  <div class="row">
					    
						<!--search box -->
							<div class="col-xs-3">
								<input type="text" name="search" id="search" class="form-control" placeholder="{{ trans('warehouse_lang.search') }}"/>
								<div id="pleasewait-search" class="pleasewait" style="display:none">{{ trans('warehouse_lang.pleasewait') }}</div>
							</div>
						<!--//search box -->

						<!--Stock filter-->
							<div class="col-xs-3" >	
								<select id="filterStock" class="form-control">
									<option value='in'>{{ trans('warehouse_lang.instock') }}</option>
									<option value='out'>{{ trans('warehouse_lang.outofstock') }}</option>
									<option value='all'>{{ trans('warehouse_lang.all') }}</option>
								</select>
							</div>
						<!--//Stock filter-->		
							
							<div class="col-xs-3">
								
								<select name="warehouse_team_category" id="warehouse_team_category" class="form-control" >
									
									<option value="0">-{{ trans('warehouse_lang.selectcategory') }}-</option>
									<?php
											if(is_array($categoryByZone) && count($categoryByZone)>0)
											{
												
												foreach ($categoryByZone as $row)
												{
													$id = 0;
													$name = '';
													if(!empty($row->id))
													{
														$id = $row->id;
													}
													
													if(!empty($row->name))
													{
														$name = $row->name;
													}
													?>
													<option value="<?php echo $id; ?>"><?php echo $name; ?></option>
												<?php	
												}
												
											} 
									?>
								</select>
								
								<!--<div id="pleasewait-category" class="pleasewait" style="display:none">{{ trans('warehouse_lang.pleasewait') }}</div>-->
								
								
							 </div>
							 <div class="col-xs-3">
								<button id="add-warehouse-deal"  type="button" class="btn  btn-info" onclick="getAssignStockList();">{{ trans('warehouse_lang.search') }}</button>
							 
								<button id="add-warehouse-deal"  type="button" class="btn  btn-info" onClick="window.location.reload();" >{{ trans('warehouse_lang.reset') }}</button>
							 </div>
							
							
							<!--<div class="col-xs-3">
								<label for="exampleInputEmail1">{{ trans('warehouse_lang.team') }}</label>
								<select name="warehouse_team_id" id="warehouse_team_id" class="form-control" onchange ="getOptionsByTeam();">
											
								</select>
								<div id="pleasewait-team" class="pleasewait" style="display:none">{{ trans('warehouse_lang.pleasewait') }}</div>
							</div>
							 
							<div class="col-xs-3">
								<label for="exampleInputEmail1">{{ trans('warehouse_lang.options') }}</label>
								<select name="warehouse_option_id" id="warehouse_option_id" class="form-control" >
									
								</select>
							</div>
						   -->
						   
						 </div>
					  </div>  
					  
					  <div id="pleasewait-team" class="pleasewait" style="display:none">{{ trans('warehouse_lang.pleasewait') }}</div>
				</div><!-- /.box-body -->
				
				<div id="assignStockListId"></div>
				
				<div class="box-footer clearfix">
					<?php //echo $pagestring;?>
				</div>
			</div>
		</div>
	</div>
</div>
  
    <script>
		getDealsBySection();
        function getDealsBySection()
		{
			jQuery("#pleasewait-section").show();
			var queryData = '';
			var warehouseSectionId = $('#warehouse_sections').val();    
			 
			 if(warehouseSectionId>0)
			 {
				// show edit link
				var link ="<?php echo URL::to('admin/warehousesectionupdate'); ?>/"+warehouseSectionId;
				
				$('#editteam').html('<a href="'+link+'" class="glyphicon glyphicon-edit"></a>');
				jQuery("#editteam").show();
				
				
				
				var action_url = "<?php echo URL::to('admin/getandsaveteambysection'); ?>/";
			 
				queryData = 'warehouseSectionId='+warehouseSectionId;
				queryData += '&action=view';
				
				 $.ajax({
						type: "GET",
						url: action_url,
						data: queryData,
						cache: false,
						success: function(events) 
						{
							 jQuery("#pleasewait-section").hide();
							 $('#section-grid').html('');
							 $('#section-grid').html(events);
						}
					});
			 }
			 else
			 {
				 jQuery("#pleasewait-section").hide();
			 }
		}
		
		function getTeamByCategory()
		{
			 jQuery("#pleasewait-category").show();
			 
			 var queryData = '';
			 var categoryId 	 = $('#warehouse_team_category').val();
			 action_url = "<?php echo URL::to('admin/getteambycategory'); ?>/"+categoryId;
	
			  $.ajax({
						type: "GET",
						url: action_url,
						data: queryData,
						cache: false,
						success: function(response)
						{
							jQuery("#pleasewait-category").hide();
							response = jQuery.parseJSON(response);
							
							 if(response.teamDropDownTpl)
							{
								$('#warehouse_team_id').html('');
								$('#warehouse_team_id').html(response.teamDropDownTpl);
							}
							else
							{
								$('#warehouse_team_id').html('');
								$('#warehouse_team_id').html('<option value=0>- Select -</option>');
							} 
							
							
						}
				}); 
		}
		
		
		
		function getOptionsByTeam()
		{
			jQuery("#pleasewait-team").show();
			
			 var queryData = '';
			 var teamId 	 = $('#warehouse_team_id').val();
			 action_url = "<?php echo URL::to('admin/multiteambyteamid'); ?>/"+teamId;
	
			  $.ajax({
						type: "GET",
						url: action_url,
						data: queryData,
						cache: false,
						success: function(response)
						{
							jQuery("#pleasewait-team").hide();
							response = jQuery.parseJSON(response);
							
							if(response.multiteamDropDownTpl)
							{
								$('#warehouse_option_id').html('');
								$('#warehouse_option_id').html(response.multiteamDropDownTpl);
							}
							else
							{
								$('#warehouse_option_id').html('');
								$('#warehouse_option_id').html('<option value=0>- Select -</option>');
							} 
							
							
						}
				});
			
		}
		
		
		function saveDeal()
		{
			jQuery("#pleasewait-section").show();
            var warehouseSectionId 			= $('#warehouse_sections').val();
			
			//Deal id with option id
			var assignStockCheckBox 			= $('input:checkbox:checked.assignStockCheckBox').map(function () {
				return this.value;
				}).get(); 
			
			 
			var queryData  = '';
			var action_url = "<?php echo URL::to('admin/getandsaveteambysection'); ?>/";
			 
			
			postData = { warehouseSectionId: warehouseSectionId, assignStockCheckBox:assignStockCheckBox,action:'save'};
			
			
			 $.ajax({
					type: "POST",
					url: action_url,
					data: postData,
					cache: false,
					success: function(events) 
					{
						 jQuery("#pleasewait-section").hide();
						 $('#section-grid').html('');
						 $('#section-grid').html(events);
						 $('#selectAllCheckBox').attr('checked', false);
						 $('.assignStockCheckBox').attr('checked', false);
						 $("#assignStockButton").attr("disabled","disabled");
					}
				});
			
		}
		
		 $(document).on("click", ".delete-deal-option", function(e) {
				e.preventDefault();
				confirm_result = confirm($(this).attr('ask'));
				
			   
				if (confirm_result) {
					 $(this).parent('td').parent('tr').remove();
				   $.ajax({
						url: $(this).attr('href'),
						data: {
						   action: 'deletem'
							
						},
						success: function(events) {
							hideDeleteButton();
						}
					});
				}
				else {
					return false;
				}
				
				
				
		});
		
		
		// delete multiple by check box
	   $(document).on("click", "#delete-warehouse-deal", function(e) {
			warehouse_items = $("input[name=warehouse_items]:checked").map(function() {
				$(this).parent('td').parent('tr').remove();
				return this.value;
			}).get().join(",");
			
			$("#delete-warehouse-deal").attr("disabled","disabled");
			$(".move-button").attr("disabled","disabled");
			
			$.ajax({
					url: '<?php echo URL::to('admin/deleteteam') ?>',
					data: {
						warehouse_items: warehouse_items,
						action: 'deletemultiple'
						
					},
					success: function(events) {
					
						
						hideDeleteButton();
						
					}
				});
			
	});
	
function hideDeleteButton()
{
	var rowCount = $('#tableId tr').length;
	if(rowCount>1)
	{
		//deletebuttondiv
		jQuery("#deletebuttondiv").show();
	}
	else
	{
		
		$('#selectAll').attr('checked', false);
		$("#selectAll").attr("disabled","disabled");
		jQuery("#deletebuttondiv").hide();
		$('#tableId thead:last').after('<tbody><tr><td colspan="6" align="center">{{ trans('warehouse_lang.norecordfound') }}</td></tr></tbody>');
		
	}
}	

function getAssignStockList()
{
	
	$('#pleasewait-team').show();
	
	var categoryId 	 = $('#warehouse_team_category').val();
	var filterStock = $('#filterStock').val();	
	var search 	     = $('#search').val();
	
			 action_url = "<?php echo URL::to('admin/getassignstocklist'); ?>";
			 
	 var queryData = '';		 
	 
	queryData = 'filterStock='+filterStock;
	
	 if(categoryId>0)
	 {
		queryData += '&categoryId='+categoryId;
		
	 }
	 
	 if(search)
	 {
		
		queryData += '&search='+search;		
	 }
	 
	  
	  if(categoryId>0 || search)
	  {
			
			$.ajax({
					type: "GET",
					url: action_url,
					data: queryData,
					cache: false,
					success: function(response)
					{
						$('#pleasewait-team').hide();
						$('#assignStockListId').html('');
						$('#assignStockListId').html(response);
					}
			}); 
	  }
	  else
	  {
		$('#pleasewait-team').hide();
	  }
	
	  
	
}
		
    </script>
<?php //$this->load->view('dashboard/footer', $data); ?>