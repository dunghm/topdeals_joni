<?php
$style=" disabled=disabled ";
if(is_array($teamsection) && count($teamsection)>0)
{
	$style = '';
}
			
?>
<table class="tt-table" id="tableId">
	<thead>
		<tr>
			<th > <input type="checkbox" id="selectAll" <?php echo $style; ?> class="selectAll" /></th>
			<th>{{ trans('warehouse_lang.dealid') }}</th>
			<th >{{ trans('warehouse_lang.deal') }}</th>
			<th >{{ trans('warehouse_lang.optionid') }}</th>
			<th >{{ trans('warehouse_lang.option') }}</th>
			<th >{{ trans('warehouse_lang.stock') }}</th>
			<th >{{ trans('warehouse_lang.operation') }}</th>
		</tr>
	 </thead>
		<?php
			$deleteButtonShow = false;
			if(is_array($teamsection) && count($teamsection)>0)
			{
				$deleteButtonShow = true;
				foreach($teamsection as  $wt )
				{
			?>
			<tr>
				<td><input type="checkbox" name="warehouse_items" id="<?php echo $wt->id; ?>" value="<?php echo $wt->id; ?>" class="warehouse_items"  onclick = "enabledButton();"/></td>
				<td><?php echo $wt->team_id; ?></td>
				<td><?php echo $wt->team_title; ?></td>
				<td><?php echo $wt->option_id; ?></td>
				<td><?php echo $wt->option_title; ?></td>
				<td><?php echo $wt->stock; ?></td>
				<td>
					<a title="Delete" href="<?php echo URL::to('admin/deleteteam').'/'.$wt->id; ?>" class="delete-deal-option" ask="sure to delete this deal?" ><i class="fa fa-fw fa-minus-square"></i></a>
					
					<a title="Move"  onclick="popupByLink('<?php echo $wt->id; ?>')" class="move" ><i class="fa fa-arrow-right"></i></a>
				</td>
			</tr>
			<?php 
				}
			}
			 else{
				?>
		<tr>
			<td colspan="7" align="center"> {{ trans('warehouse_lang.norecordfound') }}</td>
		</tr>
		<?php
			}
		?>
</table>
<?php
if($deleteButtonShow)
{
?>	
	<div class="form-group" id="deletebuttondiv">
		 <button id="delete-warehouse-deal"  type="button" disabled="disabled" class="btn  btn-info">{{ trans('warehouse_lang.delete') }}</button>
		 
		 <button id=""  type="button" disabled="disabled" class="btn btn-success move-button" onclick="popupByButton()">{{ trans('warehouse_lang.move') }}</button>
		 
		 <!--<a title="Move" href="<?php //echo URL::to('admin/warehousesectionmove').'/'.$wt->warehouse_section_id; ?>" class="btn btn-success move-button" disabled="disabled" id="preview-newsletter-list">{{ trans('warehouse_lang.move') }}</a>-->
	</div>
<?php
}
?>
<script>
function enabledButton()
{
	var values = $('input:checkbox:checked.warehouse_items').map(function () {
				  return this.value;
				}).get(); 
		if(values=='')
		{
			$("#delete-warehouse-deal").attr("disabled","disabled");
			$(".move-button").attr("disabled","disabled");
		}
		else
		{
			$("#delete-warehouse-deal").removeAttr("disabled");
			$(".move-button").removeAttr("disabled");
		}
		
			
}

function popupByButton()
{
	//jQuery("#pleasewait-section").show();
	var warehouseSectionId 				= $('#warehouse_sections').val();
	var warehouseSectionItemId 			= $('input:checkbox:checked.warehouse_items').map(function () {
	  return this.value;
	}).get(); 
	
	openMovesection(warehouseSectionId,warehouseSectionItemId);
}

function popupByLink(warehouseSectionItemId)
{
	var warehouseSectionId 				= $('#warehouse_sections').val();
	
	openMovesection(warehouseSectionId,warehouseSectionItemId);
}


function openMovesection(warehouseSectionId,warehouseSectionItemId)
{
	var queryData  = '';
	var action_url = "<?php echo URL::to('admin/warehousesectionmoveform'); ?>/";
	
	queryData = 'warehouseSectionId='+warehouseSectionId;
	queryData += '&warehouseSectionItemId='+warehouseSectionItemId;
	
	$.ajax({
		type: "GET",
		url: action_url,
		data: queryData,
		cache: false,
		success: function(response) 
		{
			$('#preview-modal').html(response);
            $('#preview-modal').modal();
		}
	});
}

$('#selectAll').click(function (e) {
    $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
	enabledButton();
});

$('.warehouse_items').change(function () {
    var check = ($('.warehouse_items').filter(":checked").length == $('.warehouse_items').length);
    $('#selectAll').prop("checked", check);
});
</script>