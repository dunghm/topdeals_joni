<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<div class="portlet pd-30">
				<div class="page-heading">{{ trans('warehouse_lang.addwarehousesection') }}</div>
				<form id="create-form" role="form" method="post" action="<?php echo URL::to('admin/warehousesectionsave'); ?>">
				  <div class="box-body">
					<div class="form-group">
					  <label for="exampleInputEmail1">{{ trans('warehouse_lang.name') }}</label>
					  <input type="text" class="form-control" required="required" id="title" name="name" placeholder="Warehouse Sections Name" value="">
					</div>
					
					  <div class="form-group" style="display:none">
						<label>{{ trans('warehouse_lang.selectwarehouse') }}</label>
						
						<select name="warehouse" id="deal_category" class="form-control">
							
							<?php
								if(is_array($warehouseList) && count($warehouseList)>0)
								{
									foreach($warehouseList as $row)
									{
										$id = 0;
										$name = '';
										
										#id
										if(!empty($row->id))
										{
											$id = $row->id;
										}
										
										#name
										if(!empty($row->name))
										{
											$name = $row->name;
										}
										?>
										<option value='<?php echo $id; ?>'><?php echo $name; ?></option>
		<?php										
									}
								}
								
							?>	
						</select>
					</div>
					<div class="form-group">
						<label>{{ trans('warehouse_lang.description') }}</label>
						<textarea name="description" class="form-control" placeholder="Description" rows="3"></textarea>
					</div>
					
				  </div><!-- /.box-body -->

				  <div class="box-footer">
					
					<button name="mode" value="Save" id="newsletter-create-btn" type="submit" class="btn btn-primary">{{ trans('warehouse_lang.submit') }}</button>
				  </div>
				</form>
			</div>
		</div>
	</div>
</div>
