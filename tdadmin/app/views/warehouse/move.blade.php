<div class="modal-dialog" style="width:600px;">
	<div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title"> {{ trans("warehouse_lang.movesection") }} </h4>
		</div>
		<div id="modal-body-preview" class="modal-body">
			<div class="form-group">
				<div id="errorMessage" class="text-danger"></div>
				<div id="existMessage" class="text-danger"></div>
				
				<label>{{ trans('warehouse_lang.selecttargetsection') }}:</label>
				
				<select name="warehouse_section_id" id="warehouseSectionId" class="form-control">
					<option value="0">-{{ trans('warehouse_lang.select') }}-</option>
						<?php
								if(is_array($warehouseSection) && count($warehouseSection)>0)
								{
									
									foreach ($warehouseSection as $row)
									{
										$id = 0;
										$name = '';
										if(!empty($row->id))
										{
											$id = $row->id;
										}
										
										if(!empty($row->name))
										{
											$name = $row->name;
										}
										
										?>
										<option value="<?php echo $id; ?>"><?php echo $name; ?></option>
									<?php	
									}
									
								} 
						?>
				</select>
				<input type="hidden" value="{{$warehouseSectionId}}"  id="oldwarehouseSectionId" />
				<input type="hidden" value="{{$warehouseSectionItemId}}"  id="warehouseSectionItemIds" />
			</div>
			<div id="pleasewait-section" class="pleasewait" style="display:none">{{ trans('warehouse_lang.pleasewait') }}</div>
			<div class="form-group btn-region pull-right">
				<button class="btn btn-primary" type="submit" name="Confirm" value="Confirm" id="Confirm" onclick="movesection()" >Confirm</button>
				<span><button type="button" class="btn btn-default " data-dismiss="modal">Cancel</button></span>
			</div>
			
		</div>
		
			
	</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<script>
function movesection()
{
	var oldwarehouseSectionId 			= $('#oldwarehouseSectionId').val();
	var warehouseSectionId 				= $('#warehouseSectionId').val();
	var warehouseSectionItemIds 		= $('#warehouseSectionItemIds').val();
	
	var queryData  = '';
	var action_url = "<?php echo URL::to('admin/warehousesectionmove'); ?>/";
	
	queryData = 'warehouseSectionId='+warehouseSectionId;
	queryData += '&warehouseSectionItemIds='+warehouseSectionItemIds;
	queryData += '&oldwarehouseSectionId='+oldwarehouseSectionId;
	
	$.ajax({
		type: "GET",
		url: action_url,
		data: queryData,
		cache: false,
		success: function(response) 
		{
			response = jQuery.parseJSON(response);
			$('#errorMessage').html('');
			$('#existMessage').html('');
            
			if (response.error)
            {
                $('#errorMessage').html(response.error);
            }
			
			if (response.error)
			 {
				 $('#errorMessage').html(response.error);
				 $('#errorMessage').addClass( "alert alert-danger" );
			 }
			 else
			 {
				$('#errorMessage').html('');
				$('#errorMessage').removeClass( "alert alert-danger" );
			 }
			
			var status = false;
			if (response.successMessage)
            {
               $('#existMessage').removeClass( "alert alert-danger" );
			   $('#errorMessage').removeClass( "alert alert-danger" );
			   $('#errorMessage').html('');
			   $('#errorMessage').html(response.successMessage);
			   $('#errorMessage').addClass("alert alert-success");
			   $("#Confirm").attr("disabled","disabled");
			   
			   status = true;
            }
			
			if(response.errorMessage)
			 {
				$('#existMessage').removeClass( "alert alert-danger" );
				$('#errorMessage').removeClass( "alert alert-success" );
				$('#errorMessage').html('');
				$('#errorMessage').html(response.errorMessage);
				$('#errorMessage').addClass("alert alert-danger");
				
				 status = false;
			 }
			 
			 
			if(response.existMessage)
			{
				$('#existMessage').removeClass( "alert alert-danger" );
				$('#existMessage').html(response.existMessage);
				$('#existMessage').addClass("alert alert-danger");
				
				if(status)
				{
					$('#pleasewait-section').show();
					var delay=3000; //3 seconds

					setTimeout(function(){
					  //your code to be executed after 1 seconds
					  if (response.redirect)
					  {
							window.location = response.redirect;
					  }
						
					}, delay);
				}				
					
			}
				
			
			if(status && !response.existMessage)
			{
				if (response.redirect)
				{
					window.location = response.redirect;
				}
			}
				
			
		}
	});
}


</script>