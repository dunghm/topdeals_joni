<script src="<?php echo URL::asset('assets/datatable/jquery.dataTables.js');?>"></script>
<script src="<?php echo URL::asset('assets/yadcf/jquery_datatables_yadcf.js');?>"></script>
<style>
div.dataTables_length select { display: none; }
.dataTables_filter { display: none }
</style>
@if(Session::has('successMessage'))
    <div class="alert alert-success">
        {{ Session::get('successMessage') }}
    </div>
@endif

@if(Session::has('errorMessage'))
    <div class="alert alert-danger">
        {{ Session::get('errorMessage') }}
    </div>
@endif		

<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<div class="portlet pd-30">
				<?php 
						$searchUrl= '';
						$sign = '?';
						$searchByDeal   	= Input::get('searchByDeal');
						$searchByOption     = Input::get('searchByOption');
						
						if(!empty($searchByDeal)) 
						{
							$searchUrl.= $sign."searchByDeal=".$searchByDeal;
							$sign = '&';
						}
						
						if(!empty($searchByOption)) 
						{
							$searchUrl.= $sign."searchByOption=".$searchByOption;
						}
				?>
				<div class="page-heading">{{ trans('warehouse_lang.title') }}</div>	
				<div>
					<!--search box -->
							<form method='post' action="<?php echo URL::to('admin/warehousesection')?>/data">
								<div class="row">
									<div class="col-md-4">
										<input type="text" name="searchByDeal" id="searchByDeal" class="form-control" value="<?php echo $searchByDeal;  ?>"   placeholder="{{ trans('warehouse_lang.searchbyteamid') }}"/>
									</div>
									<div class="col-md-4">
										<input type="text" name="searchByOption" id="searchByOption" class="form-control" value="<?php echo $searchByOption;  ?>" placeholder="{{ trans('warehouse_lang.searchbyoptionid') }}"/>
									</div>					
									<div class="col-md-4">								
										<input type="submit" class="btn btn-primary" value="search" />
									</div>
								</div>
							</form>
					<!--//search box -->
				</div>
				<div>&nbsp;</div>
				<div class="ho-lala">
				  {{ Datatable::table()
					  ->addColumn(trans('warehouse_lang.id'), trans('warehouse_lang.section'), trans('warehouse_lang.description'),trans('warehouse_lang.totalProduct'), trans('warehouse_lang.operation'))
					  ->setUrl(route('api.warehouseSection').$searchUrl)
					  ->setOptions('order', array([0 ,"desc"]))
					  ->render()
				  }}
				</div>
			</div>
		</div>
	</div>
</div>


<script>
  $(document).ready(function(){
    var oTable = $('.table').dataTable().yadcf([
  	  	{column_number : 0, filter_type: "text"},
  	  	{column_number : 1, filter_type: "text"},
		{column_number : 2, filter_type: "text"}
    ]);
	  var oSettings = oTable.fnSettings();
	  oSettings._iDisplayLength = 50;
	  oTable.fnDraw();
	  $('.dataTables_wrapper').find('select').val(50).text();
    $('table').removeClass().addClass('tt-table dataTable laravel-table').wrap('<div class="table-responsive"></div>');
    $('.dataTables_wrapper').wrap('<div class="portlet pd-30"></div>');
    $('.yadcf-filter').addClass('tt-form-control');
    $(".dataTables_length").addClass('clearfix');
  
	jQuery( ".head4" ).unbind();
  });
  
  
  
function deleteRecord(id)
{
    
	var action_url = "<?php echo URL::to('admin/warehousesectiondelete').'/' ?>"+id;
	var xConfirm = confirm("Are you sure, you want to delete this record?");

    if (xConfirm)
    {
        window.location = action_url;
    }

    return false;
}
</script>