<script src="<?php echo URL::asset('assets/datatable/jquery.dataTables.js');?>"></script>
<script src="<?php echo URL::asset('assets/yadcf/jquery_datatables_yadcf.js');?>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        initTable();
    });

    /*initialize data table*/
    function initTable()
    {
		<?php
			$url= URL::to('admin/getassignstockdata/?');
			
			if(!empty($filterStock))
			{
				
				$url.="filterStock=".$filterStock;
			}
			else
			{
				$url.="filterStock=in";
			}
			
			if(!empty($categoryId))
			{
				
				$url.="&filterByCategory=".$categoryId;
			}
			
			if(!empty($search))
			{
				$url.="&filterBySearch=".$search;
			}
		?>
		$('#assignstockList').dataTable({
			"bDestroy": true,
			"columnDefs": [ { "targets": 0, "orderable": false } ],
			"order": [[ 1, "asc" ]],
			//"aaSorting": [], // disable default sorting!
			"bProcessing": true,
			"bServerSide": true,
			"sAjaxSource": "<?php echo $url; ?>",
			"iDisplayLength": <?php echo PAGGINATION_LENGTH ?>,
			"bLengthChange" : false, //thought this line could hide the LengthMenu
			"fnDrawCallback": function () {
             var totalRows = this.fnSettings().fnRecordsTotal();
			  if(totalRows>0)
			  {
				$("#selectAllCheckBox").removeAttr("disabled");
			  }
			 
			}
		});
		
		//alert(oTable.fnGetData().length);
		// Initialize your table
		/* var oTable = $('#assignstockList').dataTable();

		// Get the length
		alert(oTable.fnGetData().length); */
        
    }
</script>

<div class="ho-lala">		
	  <table id="assignstockList" class='table table-bordered table-striped' data='' style='margin-bottom:0;'>
		<!---table head---->
		<thead>
			<tr>
				<!--<th width="4%">{{ trans('warehouse_lang.select') }}</th>-->
				<th width="4%"><input type="checkbox" id="selectAllCheckBox" style="position:relative;right:9px;" disabled="disabled"/></th>
				<th width="15%">{{ trans('warehouse_lang.category') }}</th>
				<th width="8%">{{ trans('warehouse_lang.dealid') }}</th>
				<th width="20%">{{ trans('warehouse_lang.dealtitle') }}</th>
				<th width="10%">{{ trans('warehouse_lang.optionid') }}</th>
				<th width="20%">{{ trans('warehouse_lang.optiontitle') }}</th>
				<th width="4%">{{ trans('warehouse_lang.stock') }}</th>
				<th width="20%">{{ trans('warehouse_lang.section') }}</th>
			</tr>
		</thead>
		<!---//table head---->  
		<!---table body---->

	</table>
<div class="form-group"></div>
<div> 						 
  <button id="assignStockButton"  type="button" class="btn  btn-info" disabled="disabled"  onclick="saveDeal();">{{ trans('warehouse_lang.assignstock') }}</button>
</div>
</div>

<script type="text/javascript">
	function enabledAssignButton()
{
	var values = $('input:checkbox:checked.assignStockCheckBox').map(function () {
				  return this.value;
				}).get(); 
				
		if(values=='')
		{
			$("#assignStockButton").attr("disabled","disabled");
			
		}
		else
		{
			$("#assignStockButton").removeAttr("disabled");
			
		}
		
			
}

$('#selectAllCheckBox').click(function (e) {
    $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
	enabledAssignButton();
});

$(document).on('change','.assignStockCheckBox',function() {
    var check = ($('.assignStockCheckBox').filter(":checked").length == $('.assignStockCheckBox').length);
    $('#selectAllCheckBox').prop("checked", check);
});
</script>

    
      
