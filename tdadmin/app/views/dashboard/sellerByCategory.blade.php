<table class="table table-striped table-bordered table-advance table-hover  bestSellerWidgetBody-table">
	<thead>
		<tr>
			<th width="75%"><i class="icon-question-sign"></i> <span class="hidden-phone">Deal Name</span></th>
			<th><i class="icon-shopping-cart"> </i><span class="hidden-phone">Total Sale</span></th>
			
		</tr>
	</thead>
	<tbody>
		<?php
			if(!empty($data['bestSellerByCategory'])){
				foreach($data['bestSellerByCategory'] as $key=>$bestSellerByCategoryRow){
					
					$teamUrl	=	TOPDEAL_URL_LINK."/team.php?id=".$bestSellerByCategoryRow->team_id;
		?>
		<tr>
			<td class="highlight">
				<div class="<?php echo $data['colorArr'][$key];?>"></div>
				<a href="<?php echo $teamUrl;?>" target='_blank'><?php echo $bestSellerByCategoryRow->title;?></a>
			</td>
			<td><?php echo $bestSellerByCategoryRow->numberOfPurchase;?></td>
		</tr>
		<?php
				}
			}else{
		?>
			<tr><td colspan="2">No Sale Found.</td></tr>
		<?php
			}
		?>
	</tbody>
</table>