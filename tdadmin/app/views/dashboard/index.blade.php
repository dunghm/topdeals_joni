<link rel="stylesheet" href="<?php echo URL::asset('assets/dashboard/bootstrap/css/bootstrap.css').VER_CSS;?>">
<link rel="stylesheet" href="<?php echo URL::asset('assets/dashboard/css/style.css').VER_CSS;?>">
<link rel="stylesheet" href="<?php echo URL::asset('assets/dashboard/chosen/chosen.css').VER_CSS;?>">

<!-- Date Range Picker -->
<!-- Include Required Prerequisites -->
<script type="text/javascript" src="<?php echo URL::asset('assets/dashboard/bootstrap_latest/jquery.min.js').VER_JS;?>"></script>
<script type="text/javascript" src="<?php echo URL::asset('assets/dashboard/bootstrap_latest/moment.min.js').VER_JS;?>"></script>
<link rel="stylesheet" href="<?php echo URL::asset('assets/dashboard/bootstrap_latest/bootstrap.css').VER_CSS;?>">
 
<!-- Include Date Range Picker -->
<script type="text/javascript" src="<?php echo URL::asset('assets/dashboard/bootstrap_latest/daterangepicker.js').VER_JS;?>"></script>
<link rel="stylesheet" href="<?php echo URL::asset('assets/dashboard/bootstrap_latest/daterangepicker.css').VER_CSS;?>">

<style>
input#dateRangeBtn {
    width: 152px;
    font-size: 12px;
}
.daterangepicker_input{display:none !important;}
</style>
<!--// Date Range Picker -->

<?php
$start_date = date('d-m-Y', strtotime(date('Y-m')." -1 month"));
$end_date = date('d-m-Y');
?>
<!-- Dashboard Index -->
      <div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
            <div class="portlet pd-30">
				<div class="page-heading">
				{{ trans('localization.dashboard') }}
				</div>
				<hr/>
				<div id="page" class="dashboard">
					<!-- BEGIN OVERVIEW STATISTIC BARS-->
					<div class="widget">
						<div class="widget-title">
							<h4>Basic Statisics</h4>
						</div>
						<div class="widget-body">
							<div class="row-fluid stats-overview-cont">						
								<?php
									if(isset($data['todaySale'])){
								?>
								<div class="span2 responsive" data-tablet="span4" data-desktop="span2">
									<div class="stats-overview block clearfix">
										<div class="display stat ok huge">
											<span class="line-chart">5, 6, 7, 11, 14, 10, 15, 19, 15, 2</span>
											<div class="percent"><?php echo $data['todaySalePercentage'];?></div>
										</div>
										<div class="details">
											<div class="title">Daily Sales</div>
											<div class="numbers"><?php echo number_format($data['todaySale'],2);?></div>
										</div>
										<div class="progress progress-info">
											<div class="bar" style="width: <?php echo $data['todaySalePercentage'];?> "></div>
										</div>
									</div>
								</div>
								<?php
									}
								?>
		
								<?php
									if(isset($data['dailyMargin'])){
								?>
								<div class="span2 responsive" data-tablet="span4" data-desktop="span2">
									<div class="stats-overview block clearfix">
										<div class="display stat good huge">
											<span class="line-chart">2,6,8,12, 11, 15, 16, 11, 16, 11, 10, 3, 7, 8, 12, 19</span>
											<div class="percent"> <?php echo $data['dailyMarginPercentage'];?></div>
										</div>
										<div class="details">
											<div class="title">Daily Margin</div>
											<div class="numbers"><?php echo $data['dailyMargin'];?></div>
											<div class="progress progress-warning">
												<div class="bar" style="width: <?php echo $data['dailyMarginPercentage'];?>"></div>
											</div>
										</div>
									</div>
								</div>
								<?php
									}
								?>
								
								<?php
									if(isset($data['totalPlaceOrder'])){
								?>
								<div class="span2 responsive " data-tablet="span4" data-desktop="span2">
									<div class="stats-overview block clearfix">
										<div class="display stat bad huge">
											<span class="line-chart">2,6,8,11, 14, 11, 12, 13, 15, 12, 9, 5, 11, 12, 15, 9,3</span>
											<!-- <div class="percent">+6%</div> -->
										</div>
										<div class="details">
											<div class="title">Today Orders</div>
											<div class="numbers"><?php echo $data['totalPlaceOrder'];?></div>
											<div class="progress progress-success">
												<div class="bar" style="width: 100%"></div>
											</div>
										</div>
									</div>
								</div>
								<?php
									}
								?>
								
								<?php
									if(isset($data['todayNewUser'])){
								?>
								<div class="span2 responsive " data-tablet="span4 fix-margin" data-desktop="span2">
									<div class="stats-overview block clearfix">
										<div class="display stat good huge">
											<span class="bar-chart">1,4,9,12, 10, 11, 12, 15, 12, 11, 9, 12, 15, 19, 14, 13, 15</span>
											<!-- <div class="percent">+86%</div>-->
										</div>
										<div class="details">
											<div class="title">Today User</div>
											<div class="numbers"><?php echo $data['todayNewUser'];?></div>
											<div class="progress progress-danger">
												<div class="bar" style="width: 100%"></div>
											</div>
										</div>
									</div>
								</div>
								<?php
									}
								?>						
							
								<?php
									if(isset($data['total_reg_users'])){
								?>
									<div class="span2 responsive " data-tablet="span4" data-desktop="span2">
										<div class="stats-overview block clearfix">
											<div class="display stat ok huge">
												<span class="line-chart">2,6,8,11, 14, 11, 12, 13, 15, 12, 9, 5, 11, 12, 15, 9,3</span>
												<!-- <div class="percent">+6%</div> -->
											</div>
											<div class="details">
												<div class="title">Reg. Users</div>
												<div class="numbers">{{ number_format($data['total_reg_users']) }}</div>
												<div class="progress">
													<div class="bar" style="width: 100%"></div>
												</div>
											</div>
										</div>
									</div>
								<?php
									}
								?>								
								
								<?php
									if(isset($data['offlineTopUp'])){
								?>
									<div class="span2 responsive " data-tablet="span4" data-desktop="span2">
										<div class="stats-overview block clearfix">
											<div class="display stat good huge">
												<span class="line-chart">2,6,8,11, 14, 11, 12, 13, 15, 12, 9, 5, 11, 12, 15, 9,3</span>
												<!-- <div class="percent">+6%</div> -->
											</div>
											<div class="details">
												<div class="title">Offline Topup</div>
												<div class="numbers">{{ number_format($data['offlineTopUp']) }}</div>
												<div class="progress progress-success">
													<div class="bar" style="width: 100%"></div>
												</div>
											</div>
										</div>
									</div>
								<?php
									}
								?>
							</div>
					
							<div class="row-fluid stats-overview-cont">
							<?php
								if(isset($data['total_team'])){
							?>
								<div class="span2 responsive " data-tablet="span4" data-desktop="span2">
									<div class="stats-overview block clearfix">
										<div class="display stat ok huge">
											<span class="line-chart">2,6,8,11, 14, 11, 12, 13, 15, 12, 9, 5, 11, 12, 15, 9,3</span>
											<!-- <div class="percent">+6%</div> -->
										</div>
										<div class="details">
											<div class="title">Total Deals</div>
											<div class="numbers">{{ number_format($data['total_team']) }}</div>
											<div class="progress">
												<div class="bar" style="width: 100%"></div>
											</div>
										</div>
									</div>
								</div>
							<?php
								}
							?>
							
							<?php
								if(isset($data['active_teams'])){
							?>
								<div class="span2 responsive " data-tablet="span4" data-desktop="span2">
									<div class="stats-overview block clearfix">
										<div class="display stat good huge">
											<span class="line-chart">2,6,8,11, 14, 11, 12, 13, 15, 12, 9, 5, 11, 12, 15, 9,3</span>
											<div class="percent"> <?php echo $data['activeTeamsPercentage'];?></div>
										</div>
										<div class="details">
											<div class="title">Active Team</div>
											<div class="numbers">{{ number_format($data['active_teams']) }}</div>
											<div class="progress progress-success">
												<div class="bar" style="width: <?php echo $data['activeTeamsPercentage'];?>"></div>
											</div>
										</div>
									</div>
								</div>
							<?php
								}
							?>
							
							<?php
								if(isset($data['expired_teams'])){
							?>
								<div class="span2 responsive " data-tablet="span4" data-desktop="span2">
									<div class="stats-overview block clearfix">
										<div class="display stat bad huge">
											<span class="line-chart">2,6,8,11, 14, 11, 12, 13, 15, 12, 9, 5, 11, 12, 15, 9,3</span>
											<div class="percent"> <?php echo $data['expiredTeamsPercentage'];?></div>
										</div>
										<div class="details">
											<div class="title">Exp. Team</div>
											<div class="numbers">{{ number_format($data['expired_teams']) }}</div>
											<div class="progress progress-danger">
												<div class="bar" style="width: <?php echo $data['expiredTeamsPercentage'];?>"></div>
											</div>
										</div>
									</div>
								</div>
							<?php
								}
							?>
							
							<?php
								if(isset($data['total_order'])){
							?>
								<div class="span2 responsive " data-tablet="span4" data-desktop="span2">
									<div class="stats-overview block clearfix">
										<div class="display stat ok huge">
											<span class="line-chart">2,6,8,11, 14, 11, 12, 13, 15, 12, 9, 5, 11, 12, 15, 9,3</span>
											<!-- <div class="percent">+6%</div> -->
										</div>
										<div class="details">
											<div class="title">Total Orders</div>
											<div class="numbers">{{ number_format($data['total_order']) }}</div>
											<div class="progress">
												<div class="bar" style="width: 100%"></div>
											</div>
										</div>
									</div>
								</div>
							<?php
								}
							?>							
							
							<?php
								if(isset($data['order_paid'])){
							?>
								<div class="span2 responsive " data-tablet="span4" data-desktop="span2">
									<div class="stats-overview block clearfix">
										<div class="display stat good huge">
											<span class="line-chart">2,6,8,11, 14, 11, 12, 13, 15, 12, 9, 5, 11, 12, 15, 9,3</span>
											<div class="percent"> <?php echo $data['orderToBePaidPercentage'];?></div>
										</div>
										<div class="details">
											<div class="title">Paid Orders</div>
											<div class="numbers">{{ number_format($data['order_paid']) }}</div>
											<div class="progress progress-success">
												<div class="bar" style="width: <?php echo $data['orderToBePaidPercentage'];?>"></div>
											</div>
										</div>
									</div>
								</div>
							<?php
								}
							?>
							
							<?php
								if(isset($data['order_to_be_paid'])){
							?>
								<div class="span2 responsive " data-tablet="span4" data-desktop="span2">
									<div class="stats-overview block clearfix">
										<div class="display stat bad huge">
											<span class="line-chart">2,6,8,11, 14, 11, 12, 13, 15, 12, 9, 5, 11, 12, 15, 9,3</span>
											<div class="percent"> <?php echo $data['orderToBePaidPercentage'];?></div>
										</div>
										<div class="details">
											<div class="title">Due Orders</div>
											<div class="numbers">{{ number_format($data['order_to_be_paid']) }}</div>
											<div class="progress progress-danger">
												<div class="bar" style="width: <?php echo $data['orderToBePaidPercentage'];?>"></div>
											</div>
										</div>
									</div>
								</div>
							<?php
								}
							?>
							</div>
							
							<div class="row-fluid stats-overview-cont">
								<?php
									if(isset($data['today_paid_order'])){
								?>
									<div class="span2 responsive " data-tablet="span4" data-desktop="span2">
										<div class="stats-overview block clearfix">
											<div class="display stat bad huge">
												<span class="line-chart">2,6,8,11, 14, 11, 12, 13, 15, 12, 9, 5, 11, 12, 15, 9,3</span>
												<div class="percent"> <?php #echo $data['orderToBePaidPercentage'];?></div>
											</div>
											<div class="details">
												<div class="title">Today Paid Orders</div>
												<div class="numbers">{{ number_format($data['today_paid_order']) }}</div>
												<div class="progress progress-success">
													<div class="bar" style="width:100%"></div>
												</div>
											</div>
										</div>
									</div>
								<?php
									}
								?>
								
								<?php
									if(isset($data['today_unpaid_order'])){
								?>
									<div class="span2 responsive " data-tablet="span4" data-desktop="span2">
										<div class="stats-overview block clearfix">
											<div class="display stat bad huge">
												<span class="line-chart">2,6,8,11, 14, 11, 12, 13, 15, 12, 9, 5, 11, 12, 15, 9,3</span>
												<div class="percent"> <?php #echo $data['orderToBePaidPercentage'];?></div>
											</div>
											<div class="details">
												<div class="title">Today UnPaid Orders</div>
												<div class="numbers">{{ number_format($data['today_unpaid_order']) }}</div>
												<div class="progress progress-success">
													<div class="bar" style="width:100%"></div>
												</div>
											</div>
										</div>
									</div>
								<?php
									}
								?>
								
								<?php
									if(isset($data['today_top_up'])){
								?>
									<div class="span2 responsive " data-tablet="span4" data-desktop="span2">
										<div class="stats-overview block clearfix">
											<div class="display stat bad huge">
												<span class="line-chart">2,6,8,11, 14, 11, 12, 13, 15, 12, 9, 5, 11, 12, 15, 9,3</span>
												<div class="percent"> <?php #echo $data['orderToBePaidPercentage'];?></div>
											</div>
											<div class="details">
												<div class="title">Today Topup</div>
												<div class="numbers">{{ number_format($data['today_top_up']) }}</div>
												<div class="progress progress-success">
													<div class="bar" style="width:100%"></div>
												</div>
											</div>
										</div>
									</div>
								<?php
									}
								?>
								
								<?php
									if(isset($data['total_top_up'])){
								?>
									<div class="span2 responsive " data-tablet="span4" data-desktop="span2">
										<div class="stats-overview block clearfix">
											<div class="display stat bad huge">
												<span class="line-chart">2,6,8,11, 14, 11, 12, 13, 15, 12, 9, 5, 11, 12, 15, 9,3</span>
												<div class="percent"> <?php #echo $data['orderToBePaidPercentage'];?></div>
											</div>
											<div class="details">
												<div class="title">Total Topup</div>
												<div class="numbers">{{ number_format($data['total_top_up']) }}</div>
												<div class="progress progress-success">
													<div class="bar" style="width:100%"></div>
												</div>
											</div>
										</div>
									</div>
								<?php
									}
								?>								
							</div>							
						</div>
					</div>
					
					<div class="widget">
						<div class="widget-title">
							<h4>Statisics with Custom Selection</h4>
						</div>
						<div class="widget-body">
							<div>
								<span class="icon-calendar" style="margin-top:-3px;">&nbsp;</span><input readonly="readonly" type="text" name="dateRangeBtn" id="dateRangeBtn" value="{{$start_date}} - {{$end_date}}" />
							</div>					
							<div class="row-fluid stats-overview-cont">
								<?php
									if(isset($data['reg_users'])){
								?>
								<div class="span2 responsive " data-tablet="span4" data-desktop="span2">
									<div class="stats-overview block clearfix">
										<div class="display stat bad huge">
											<span class="line-chart">2,6,8,11, 14, 11, 12, 13, 15, 12, 9, 5, 11, 12, 15, 9,3</span>
											<div class="percent" id="per_div_reg_users"> {{ $data['percentage_user'] }}</div>
										</div>
										<div class="details">
											<div class="title">Reg. Users</div>
											<div class="display stat bad huge"></div>
											<div class="numbers" id="div_reg_users">{{ number_format($data['reg_users']) }}</div>
										</div>
									</div>
								</div>
								<?php }?>
								
								<?php
									if(isset($data['total_sales'])){
								?>
								<div class="span2 responsive " data-tablet="span4" data-desktop="span2">
									<div class="stats-overview block clearfix">
										<div class="display stat bad huge">
											<span class="line-chart">2,6,8,11, 14, 11, 12, 13, 15, 12, 9, 5, 11, 12, 15, 9,3</span>
											<div class="percent" id="per_div_sales"> {{ $data['percentage_sales'] }}</div>
										</div>
										<div class="details">
											<div class="title">Paid Orders</div>
											<div class="display stat bad huge"></div>
											<div class="numbers" id="div_sales">{{ number_format( $data['total_sales'], 2 ) }}</div>
										</div>
									</div>
								</div>
								<?php }?>						
								
								<?php
									if(isset($data['total_si'])){
								?>						
								<div class="span2 responsive " data-tablet="span4" data-desktop="span2">
									<div class="stats-overview block clearfix">
										<div class="display stat bad huge">
											<span class="line-chart">2,6,8,11, 14, 11, 12, 13, 15, 12, 9, 5, 11, 12, 15, 9,3</span>
											<div class="percent" id="per_div_si"> {{ $data['percentage_si'] }}</div>
										</div>
										<div class="details">
											<div class="title">Sold Items</div>
											<div class="display stat bad huge"></div>
											<div class="numbers" id="div_si">{{ number_format( $data['total_si'], 2 ) }}</div>
										</div>
									</div>
								</div>
								<?php }?>
								
								<?php
									if(isset($data['total_rev'])){
								?>
								<div class="span2 responsive " data-tablet="span4" data-desktop="span2">
									<div class="stats-overview block clearfix">
										<div class="display stat bad huge">
											<span class="line-chart">2,6,8,11, 14, 11, 12, 13, 15, 12, 9, 5, 11, 12, 15, 9,3</span>
											<div class="percent" id="per_div_rev"></div>
										</div>
										<div class="details">
											<div class="title">Revenue</div>
											<div class="display stat bad huge"></div>
											<div class="numbers" id="div_rev">{{ number_format( $data['total_rev'], 2 ) }}</div>
										</div>
									</div>
								</div>
								<?php }?>
								
								<?php
									if(isset($data['total_dm'])){
								?>
								<div class="span2 responsive " data-tablet="span4" data-desktop="span2">
									<div class="stats-overview block clearfix">
										<div class="display stat bad huge">
											<span class="line-chart">2,6,8,11, 14, 11, 12, 13, 15, 12, 9, 5, 11, 12, 15, 9,3</span>
											<div class="percent" id="per_div_dm"> {{ $data['percentage_dm'] }}</div>
										</div>
										<div class="details">
											<div class="title">Margin</div>
											<div class="display stat bad huge"></div>
											<div class="numbers" id="div_dm">{{ number_format( $data['total_dm'], 2 ) }}</div>
										</div>
									</div>
								</div>
								<?php }?>
						</div>
					</div>
					
						
					</div>
						
					<!-- END OVERVIEW STATISTIC BARS-->
					</div>
					
					<!-- CHART -->
					<div class="row-fluid">
						<div class="span12">
							<!-- BEGIN SITE VISITS PORTLET-->
							<div class="widget">
								<div class="widget-title">
									<h4>Revenue Chart</h4>
								</div>
								<div class="widget-body">
									<div class="btn-toolbar no-top-space clearfix">
										<div class="btn-group pull-right" data-toggle="buttons-radio">
											<button class="btn btn-mini  weekly_revenue">Weekly</button>
											<button class="btn btn-mini active monthly_revenue">Monthly</button>
											<button class="btn btn-mini yearly_revenue">Yearly</button>		
										</div>
									</div>
									<div id="chart_2" class="chart"></div>
								</div>
							</div>
						</div>
					</div>
					<!-- CHART -->
					
					<!--User Chart-->
					<div class="row-fluid">
						<div class="span12">
							<!-- BEGIN SITE VISITS PORTLET-->
							<div class="widget">
								<div class="widget-title">
									<h4>User Chart</h4>
								</div>
								<div class="widget-body">
									<div class="btn-toolbar no-top-space clearfix">
										<div class="btn-group pull-left">
											<!-- user filter-->
											<span id="userFilters" >
												<select class="span13" id="year" onchange="registerUserChart()">
													<?php
														$currentYear = date('Y');
														for($y=$currentYear; $y>=$data['startYear']; $y--)
														{
															?>
															<option value="<?php echo $y; ?>""><?php echo $y; ?></option>
															<?php
														}
													?>
													
												</select>
											</span>
											<!-- //user filter-->
											
											<!--Sales -->
											<span id="salesFilters" style="display:none;">
												<label>Time period:</label>
												<select class="span8" id="fromYear" onchange="monthlySalesChart();disabledOption('fromYear');">
													<?php
														$currentYear = date('Y');
														for($y=$currentYear; $y>=$data['startYear']; $y--)
														{
															
															$disabled = '';
															if($y==$currentYear-1)
															{
																$disabled = "disabled=disabled";
															}
															?>
															<option value="<?php echo $y; ?>"<?php echo $disabled; ?>><?php echo $y; ?></option>
															<?php
														}
													?>
													
												</select>
												
												<select class="span8" id="toYear" onchange="monthlySalesChart();disabledOption('toYear');">
													<?php
														$currentYear = date('Y');
														for($y=$currentYear; $y>=$data['startYear']; $y--)
														{
															$disabled = '';
															if($y==$currentYear)
															{
																$disabled = "disabled=disabled";
															}
															
															?>
															<option value="<?php echo $y; ?>"<?php echo $disabled; ?>><?php echo $y; ?></option>
															<?php
														}
													?>
													
												</select>
											</span>
											<!--//Sales-->
											
											<!--margin -->
											<span id="marginFilters" style="display:none;">
												<label>Time period:</label>
												<select class="span8" id="marginFromYear" onchange="monthlyMarginChart();disabledMarginOption('marginFromYear');">
													<?php
														$currentYear = date('Y');
														for($y=$currentYear; $y>=$data['startYear']; $y--)
														{
															
															$disabled = '';
															if($y==$currentYear-1)
															{
																$disabled = "disabled=disabled";
															}
															?>
															<option value="<?php echo $y; ?>"<?php echo $disabled; ?>><?php echo $y; ?></option>
															<?php
														}
													?>
													
												</select>
												
												<select class="span8" id="marginToYear" onchange="monthlyMarginChart();disabledMarginOption('marginToYear');">
													<?php
														$currentYear = date('Y');
														for($y=$currentYear; $y>=$data['startYear']; $y--)
														{
															$disabled = '';
															if($y==$currentYear)
															{
																$disabled = "disabled=disabled";
															}
															
															?>
															<option value="<?php echo $y; ?>"<?php echo $disabled; ?>><?php echo $y; ?></option>
															<?php
														}
													?>
													
												</select>
											</span>
											<!--//margin-->
											
											
										</div>
										
										<div class="btn-group pull-right" data-toggle="buttons-radio">
											<button class="btn btn-mini  active users_chart">Users</button>
											<button class="btn btn-mini  sales_chart">Sales</button>
											<button class="btn btn-mini  margin_chart">Margin</button>		
										</div>
										
									</div>
									<div id="user_chart" class="chart"></div>
								</div>
							</div>
						</div>
					</div>
					<!--//User Chart-->
				<div class="row-fluid">
						<div class="span5">
								<div class="widget">
									<div class="widget-title">
										<h4><i class="icon-shopping-cart"></i>Best Sellers By Category</h4>
										<select class="chosen category_by_seller" data-placeholder="Choose a Category" tabindex="1">
										   <option value="all">All Category</option>
										   <?php 
											if(!empty($data['getCategoryList'])){
												foreach($data['getCategoryList'] as $getCategoryList){
													
													$selected = "";
													if($getCategoryList->id == $data['selectedCategoyrId']){
														$selected = "selected='selected'";
													}
										   ?>
													<option <?php echo $selected;?> value="<?php echo $getCategoryList->id;?>"><?php echo $getCategoryList->name;?></option>
										   <?php
												}
											}
										   ?>
										</select>									
									</div>
									<div class="widget-body bestSellerWidgetBody">
										<table class="table table-striped table-bordered table-advance table-hover bestSellerWidgetBody-table">
											<thead>
												<tr>
													<th width="75%"><i class="icon-question-sign"></i> <span class="hidden-phone">Deal Name</span></th>
													<th><i class="icon-shopping-cart"> </i><span class="hidden-phone">Total Sale</span></th>
													
												</tr>
											</thead>
											<tbody>
												<?php
													if(!empty($data['bestSellerByCategory'])){
														foreach($data['bestSellerByCategory'] as $key=>$bestSellerByCategoryRow){
															
															$teamUrl	=	TOPDEAL_URL_LINK."/team.php?id=".$bestSellerByCategoryRow->team_id;
												?>
												<tr>
													<td class="highlight">
														<div class="<?php echo $data['colorArr'][$key];?>"></div>
														<a href="<?php echo $teamUrl;?>" target='_blank'><?php echo $bestSellerByCategoryRow->title;?></a>
													</td>
													<td><?php echo $bestSellerByCategoryRow->numberOfPurchase;?></td>
												</tr>
												<?php
														}
													}else{
												?>
													<tr><td colspan="2">No Sale Found.</td></tr>
												<?php
													}
												?>
											</tbody>
										</table>
									</div>
							</div>
						
						</div>
						<div class="span7">
							<div class="widget">
								<div class="widget-title">
									<h4><i class="icon-tasks"></i>Summary</h4>
								</div>
								<div class="widget-body">
								<div>

								  <!-- Nav tabs -->
								  <ul class="nav nav-tabs" role="tablist">
									<li role="presentation" class="active"><a href="#top_deal_view" aria-controls="top_deal_view" role="tab" data-toggle="tab">Top View Product</a></li>
									<li role="presentation"><a href="#top_purchase_product" aria-controls="top_purchase_product" role="tab" data-toggle="tab">Top Purchased Products </a></li>
									<li role="presentation"><a href="#top_purchase_category" aria-controls="top_purchase_category" role="tab" data-toggle="tab">Top Purchased Category</a></li>
									<li role="presentation"><a href="#top_buyer" aria-controls="top_buyer" role="tab" data-toggle="tab">Top Buyers</a></li>
								  </ul>

								  <!-- Tab panes -->
								  <div class="tab-content" style="padding:20px;">
									<div role="tabpanel" class="tab-pane active" id="top_deal_view">
									<table class="table table-striped table-bordered table-advance table-hover">
										<thead>
											<tr>
												<th> <span class="hidden-phone">Deal Name</span></th>
												<th><span class="hidden-phone">Total View</span></th>
											</tr>
										</thead>
										<tbody>
										<?php
								
											if(!empty($data['topViewProduct'])){
												foreach($data['topViewProduct'] as $key=>$topViewProductRow){
													$teamUrl	=	TOPDEAL_URL_LINK."/team.php?id=".$topViewProductRow->deal_id;
										?>
											<tr>
												<td class="highlight">
													<div class="<?php echo $data['colorArr'][$key];?>"></div>
													<a href="<?php echo $teamUrl;?>" target='_blank'><?php echo $topViewProductRow->deal_title;?></a>
												</td>
												<td><?php echo $topViewProductRow->numberOfView;?></td>
											</tr>
										<?php
												}
											}else{
										?>
											<td colspan="2">No Top View Deal found</td>
										<?php
											}
										?>
										</tbody>
									</table>
									
									
									</div>
									<div role="tabpanel" class="tab-pane" id="top_purchase_product">
									<table class="table table-striped table-bordered table-advance table-hover">
											<thead>
												<tr>
													<th><i class="icon-question-sign"></i> <span class="hidden-phone">Deal Name</span></th>
													<th><i class="icon-shopping-cart"> </i><span class="hidden-phone">Total Sales</span></th>
												</tr>
											</thead>
											<tbody>
											<?php
									
												if(!empty($data['topPurchasedProduct'])){
												foreach($data['topPurchasedProduct'] as $key=>$topPurchasedProductRow){
													$teamUrl	=	TOPDEAL_URL_LINK."/team.php?id=".$topPurchasedProductRow->team_id;
											?>
												<tr>
													<td class="highlight">
														<div class="<?php echo $data['colorArr'][$key];?>"></div>
														<a href="<?php echo $teamUrl;?>" target='_blank'><?php if($topPurchasedProductRow->deal_title==""){ echo $topPurchasedProductRow->team_id; }else{ echo $topPurchasedProductRow->deal_title;}?></a>
													</td>
													<td><?php echo $topPurchasedProductRow->numberOfPurchase;?></td>
												</tr>
											<?php
													}
												}else{
											?>
												<td colspan="2">No Purchased Today</td>
											<?php
												}
											?>
											</tbody>
										</table>
									
									</div>
									<div role="tabpanel" class="tab-pane" id="top_purchase_category">
										<table class="table table-striped table-bordered table-advance table-hover">
											<thead>
												<tr>
													<th><i class="icon-question-sign"></i> <span class="hidden-phone">Group Name</span></th>
													<th><i class="icon-shopping-cart"> </i><span class="hidden-phone">Total Sales</span></th>
												</tr>
											</thead>
											<tbody>
											<?php
									
												if(!empty($data['topPurchasedCategory'])){
												foreach($data['topPurchasedCategory'] as $key=>$topPurchasedCategoryRow){
													$groupUrl	=	TOPDEAL_URL_LINK."/team/index.php?gid=".$topPurchasedCategoryRow->group_id;
											?>
												<tr>
													<td class="highlight">
														<div class="<?php echo $data['colorArr'][$key];?>"></div>
														<a href="<?php echo $groupUrl;?>" target='_blank'><?php if($topPurchasedCategoryRow->categoryName==""){ echo $topPurchasedCategoryRow->group_id; }else{ echo $topPurchasedCategoryRow->categoryName;}?></a>
													</td>
													<td><?php echo $topPurchasedCategoryRow->numberOfPurchase;?></td>
												</tr>
											<?php
													}
												}else{
											?>
												<td colspan="2">No Purchased Today</td>
											<?php
												}
											?>
											</tbody>
										</table>
									
									</div>
									<div role="tabpanel" class="tab-pane" id="top_buyer">
										<table class="table table-striped table-bordered table-advance table-hover">
											<thead>
												<tr>
													<th><i class="icon-user"></i> <span class="hidden-phone">Buyer Name</span></th>
													<th><i class="fa fa-money"> </i> <span class="hidden-phone">Total Amount</span></th>
												</tr>
											</thead>
											<tbody>
											<?php
									
												if(!empty($data['topBuyer'])){
													foreach($data['topBuyer'] as $key=>$topBuyerRow){
											?>
												<tr>
													<td class="highlight">
														<div class="<?php echo $data['colorArr'][$key];?>"></div>
														<a href="#"><?php 
															if($topBuyerRow->realName!=""){
																echo $topBuyerRow->realName; 
															}elseif($topBuyerRow->fullName!=""){ 
																echo $topBuyerRow->fullName;
															}elseif($topBuyerRow->userName!=""){ 
																echo $topBuyerRow->userName;
															}else{ 
																echo $topBuyerRow->email;
															}
														?>
														</a>
													</td>
													<td><?php echo "CHF ".number_format($topBuyerRow->totalPurchase,2);?></td>
												</tr>
											<?php
													}
												}else{
											?>
												<td colspan="2">No Top Buyer Found</td>
											<?php
												}
											?>
											</tbody>
										</table>
									
									</div>
								  </div>

								</div>
									<div class="space5"></div>
																			
									<div class="clearfix no-top-space no-bottom-space"></div>
								</div>
							</div>
						</div>
					
						</div>
					</div>
		
			</div>
        </div>
        <!-- .container-fluid -->
    </div>
</div>
<script>
jQuery(document).ready(function(){

	jQuery('input[name="dateRangeBtn"]').daterangepicker(
	{
	    locale: {
	      format: 'DD-MM-YYYY'
	    }
	});
	
	//jQuery('.applyBtn').on('click',function(){
	jQuery('#dateRangeBtn').on('change',function(){
		//alert( $('#dateRangeBtn').val() );
		regUsers();
		sales();
		dailyMargin();
		salesItem();
		totalRevenue();
	});
	
	jQuery('.category_by_seller').on('change',function(){
		var category_id	=	jQuery(this).val();
		
		var action_url = "<?php echo URL::to('admin/best_seller_category').'/';?>"+category_id;
		
		//FADE SCREE
		jQuery(".bestSellerWidgetBody").prepend("<div class=\"overlayBestSeller\"><div style=\"margin-top:27%;margin-left:40%;color:white;font-weight:blod;z-index:99999;\">Please Wait....</div></div>");
		jQuery(".overlayBestSeller").css({
			"position": "absolute", 
			"width": jQuery(".bestSellerWidgetBody-table").width(), 
			"height": jQuery(".bestSellerWidgetBody-table").height(),
			"z-index": 99999, 
			"background-color":"grey",

		}).fadeTo(0, 0.3);
		
		// jQuery('.bestSellerWidgetBody').html('Please Wait...');
		jQuery.ajax({
				url: action_url,
				type: "GET",
				cache: false,
				success: function(events) {
					jQuery('.bestSellerWidgetBody').html(events);
				}
			});
	});
	
	
	 //Interactive Chart
	function chart2(pageviews,labelArray) {
		function randValue() {
			return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
		}
		/*
		console.log(jQuery.parseJSON(pageviews));
		var pageviews = [
			[0, randValue()],
			[1, randValue()],
			[3, 2 + randValue()],
			[4, 3 + randValue()],
			[11, 95 + randValue()]
		];
		console.log(pageviews);
		*/
		var pageviews = jQuery.parseJSON(pageviews);
		var labelArray = jQuery.parseJSON(labelArray);
						
			
		var plot = $.plot($("#chart_2"), [{
			data: pageviews,
			label: "Revenue "
		}], {
			series: {
				lines: {
					show: true,
					lineWidth: 2,
					fill: true,
					fillColor: {
						colors: [{
							opacity: 0.05
						}, {
							opacity: 0.01
						}]
					}
				},
				points: {
					show: true
				},
				shadowSize: 2
			},
			grid: {
				hoverable: true,
				clickable: true,
				tickColor: "#eee",
				borderWidth: 0
			},
			colors: ["#d12610", "#37b7f3", "#52e136"],
			xaxis: {
				ticks: 11,
				tickDecimals: 0,
				tickFormatter:function(label, series) {
					return labelArray[label];
				}
			},
			yaxis: {
				ticks: 11,
				tickDecimals: 0
			}
		});


		function showTooltip(x, y, contents) {
			$('<div id="tooltip">' + contents + '</div>').css({
				position: 'absolute',
				display: 'none',
				top: y + 5,
				left: x + 15,
				border: '1px solid #333',
				padding: '4px',
				color: '#fff',
				'border-radius': '3px',
				'background-color': '#333',
				opacity: 0.80
			}).appendTo("body").fadeIn(200);
		}

		var previousPoint = null;
		$("#chart_2").bind("plothover", function (event, pos, item) {
			$("#x").text(pos.x.toFixed(2));
			$("#y").text(pos.y.toFixed(2));

			if (item) {
				if (previousPoint != item.dataIndex) {
					previousPoint = item.dataIndex;

					$("#tooltip").remove();
					var x = item.datapoint[0].toFixed(2),
						y = item.datapoint[1].toFixed(2);
					var formatedY	=	number_format(y, 2, ".", ",");
					showTooltip(item.pageX, item.pageY, item.series.label + " of " + labelArray[item.datapoint[0]] + " = " + formatedY);
				}
			} else {
				$("#tooltip").remove();
				previousPoint = null;
			}
		});
		
	}
	
	//DRAW	CHART
	<?php
		if(!empty($data['chartData'])){
	?>
		chart2('<?php echo json_encode($data['chartData']);?>','<?php echo json_encode($data['labelArray']);?>');
	<?php
		}
	?>

	//WHEN MONTHLY REGION CLICK
	jQuery('.monthly_revenue').on('click',function(){
		
		jQuery('.weekly_revenue').removeClass('active');
		jQuery('.yearly_revenue').removeClass('active');
		
		jQuery('.monthly_revenue').addClass('active');
		
		
		var action_url = "<?php echo URL::to('admin/get_revenue_chart').'?data=monthly';?>";
		
		loadChartByRange(action_url)
		
		return false;
	});
	
	//WHEN YEARLY REGION CLICK
	jQuery('.yearly_revenue').on('click',function(){
		
		jQuery('.weekly_revenue').removeClass('active');
		jQuery('.monthly_revenue').removeClass('active');
		
		jQuery('.yearly_revenue').addClass('active');
		
		var action_url = "<?php echo URL::to('admin/get_revenue_chart').'?data=yearly';?>";
		
		loadChartByRange(action_url)
		
		return false;
	});
	
	//WHEN WEEKLY REGION CLICK
	jQuery('.weekly_revenue').on('click',function(){
		
		jQuery('.yearly_revenue').removeClass('active');
		jQuery('.monthly_revenue').removeClass('active');
		
		jQuery('.weekly_revenue').addClass('active');
		
		var action_url = "<?php echo URL::to('admin/get_revenue_chart').'?data=weekly';?>";
		
		loadChartByRange(action_url)
		
		return false;
	});
	
	
	//LOAD CHART
	function loadChartByRange(url){
		
		//FADE SCREE
		jQuery("#chart_2").prepend("<div class=\"overlayChart\"><div style=\"margin-top:27%;margin-left:40%;color:white;font-weight:blod;z-index:99999;\">Please Wait....</div></div>");
		jQuery(".overlayChart").css({
			"position": "absolute", 
			"width": jQuery("#chart_2").width(), 
			"height": jQuery("#chart_2").height(),
			"z-index": 99999, 
			"background-color":"grey",

		}).fadeTo(0, 0.3);
		
		
		jQuery.ajax({
				url: url,
				type: "GET",
				cache: false,
				success: function(events) {
					jQuery('#chart_2').html(events);
				}
			});
	}
	

	function number_format(number, decimals, dec_point, thousands_sep) {

	  number = (number + '')
		.replace(/[^0-9+\-Ee.]/g, '');
	  var n = !isFinite(+number) ? 0 : +number,
		prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
		sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
		dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
		s = '',
		toFixedFix = function(n, prec) {
		  var k = Math.pow(10, prec);
		  return '' + (Math.round(n * k) / k)
			.toFixed(prec);
		};
	  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
	  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
		.split('.');
	  if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	  }
	  if ((s[1] || '')
		.length < prec) {
		s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1)
		  .join('0');
	  }
	  return s.join(dec);
	}
	
/////////////////////////////////////////bar chart code/////////////////////////////////////////////////////////////	

//WHEN users REGION CLICK
	jQuery('.users_chart').on('click',function(){
		
		jQuery('.sales_chart').removeClass('active');
		jQuery('.margin_chart').removeClass('active');
		
		jQuery('.users_chart').addClass('active');
		
		//show & hide filters
		jQuery('#salesFilters').hide();
		jQuery('#marginFilters').hide();
		jQuery('#userFilters').show();
		
		//call for user graph
		registerUserChart();
		
		return false;
	});
	
	
	//WHEN sales REGION CLICK
	jQuery('.sales_chart').on('click',function(){
		
		jQuery('.users_chart').removeClass('active');
		jQuery('.margin_chart').removeClass('active');
		
		jQuery('.sales_chart').addClass('active');
		
		
		//show & hide filters
		jQuery('#userFilters').hide();
		jQuery('#marginFilters').hide();
		jQuery('#salesFilters').show();
		
		//call for sales graph
		monthlySalesChart();
		
		return false;
	});
	
	
	//WHEN margin REGION CLICK
	jQuery('.margin_chart').on('click',function(){
		
		jQuery('.users_chart').removeClass('active');
		jQuery('.sales_chart').removeClass('active');
		
		jQuery('.margin_chart').addClass('active');
		
		//show & hide filters
		jQuery('#marginFilters').show();
		jQuery('#userFilters').hide();
		jQuery('#salesFilters').hide();
		
		//call for sales graph
		monthlyMarginChart();
		
		return false;
	});

	function regUsers(){
		var action_url = "<?php echo URL::to('admin/reg_users')?>";
		$.ajax({
			url: action_url,
			type: "POST",
			//cache: false,
			dataType: 'json',
			data: {date: $('#dateRangeBtn').val()},
			success: function(ruData) {
				var arr = ruData.split('|');
				$('#div_reg_users').html(arr[0]);
				$('#per_div_reg_users').html(arr[1]);
			}
		});
	}

	function sales(){
		var action_url = "<?php echo URL::to('admin/sales')?>";
		$.ajax({
			url: action_url,
			type: "POST",
			//cache: false,
			dataType: 'json',
			data: {date: $('#dateRangeBtn').val()},
			success: function(sData) {
				var sArr = sData.split('|');
				$('#div_sales').html(sArr[0]);
				$('#per_div_sales').html(sArr[1]);
			}
		});
	}

	function dailyMargin(){
		var action_url = "<?php echo URL::to('admin/daily_margin')?>";
		$.ajax({
			url: action_url,
			type: "POST",
			//cache: false,
			dataType: 'json',
			data: {date: $('#dateRangeBtn').val()},
			success: function(dmData) {
				var dm_Arr = dmData.split('|');
				$('#div_dm').html(dm_Arr[0]);
				$('#per_div_dm').html(dm_Arr[1]);
			}
		});
	}

	function salesItem(){
		var action_url = "<?php echo URL::to('admin/sold_item')?>";
		$.ajax({
			url: action_url,
			type: "POST",
			//cache: false,
			dataType: 'json',
			data: {date: $('#dateRangeBtn').val()},
			success: function(siData) {
				var siArr = siData.split('|');
				$('#div_si').html(siArr[0]);
				$('#per_div_si').html(siArr[1]);
			}
		});
	}

	function totalRevenue(){
		var action_url = "<?php echo URL::to('admin/total_revenue')?>";
		$.ajax({
			url: action_url,
			type: "POST",
			//cache: false,
			dataType: 'json',
			data: {date: $('#dateRangeBtn').val()},
			success: function(revData) {
				var revArr = revData.split('|');
				$('#div_rev').html(revArr[0]);
				$('#per_div_rev').html(revArr[1]);
			}
		});
	}
	
});
//call on load
registerUserChart();
function registerUserChart()
{
	var action_url = "<?php echo URL::to('admin/get_users_chart');?>";
	
	var year = $('#year').val();
	var queryData = '';
	var queryData = 'year='+year;
	
	loadUserChartByRange(action_url,queryData)
}

function monthlySalesChart()
{
	var action_url = "<?php echo URL::to('admin/get_sales_chart');?>";
	
	var fromYear = $('#fromYear').val();
	var toYear = $('#toYear').val();
	
	var queryData = '';
		queryData += 'fromYear='+fromYear;
		queryData += '&toYear='+toYear;
	
	loadUserChartByRange(action_url,queryData)
}


function monthlyMarginChart()
{
	var action_url = "<?php echo URL::to('admin/get_margin_chart');?>";
	
	var fromYear = $('#marginFromYear').val();
	var toYear = $('#marginToYear').val();
	
	var queryData = '';
		queryData += 'fromYear='+fromYear;
		queryData += '&toYear='+toYear;
	
	loadUserChartByRange(action_url,queryData)
}


//LOAD CHART
function loadUserChartByRange(url,queryData){
	
	//FADE SCREE
	jQuery("#user_chart").prepend("<div class=\"overlayChart\"><div style=\"margin-top:27%;margin-left:40%;color:white;font-weight:blod;z-index:99999;\">Please Wait....</div></div>");
	jQuery(".overlayChart").css({
		"position": "absolute", 
		"width": jQuery("#user_chart").width(), 
		"height": jQuery("#user_chart").height(),
		"z-index": 99999, 
		"background-color":"grey",

	}).fadeTo(0, 0.3);
	
	
	jQuery.ajax({
			url: url,
			data: queryData,
			type: "GET",
			cache: false,
			success: function(events) {
				jQuery('#user_chart').html(events);
			}
		});
}

////disabled option when select one dropdown//////
function disabledOption(callFrom)
{
	if(callFrom=="fromYear")
	{
		//get selected value 
		var formYear = $('#fromYear').val();
		
		// remove disable attribute from second dropdown//////
		var elems = document.getElementById("toYear");
		for (var i = 0; i < elems.length; i++) {
			elems[i].removeAttribute('disabled');
		}
		
		
		// disable selected value of second drop down
		$('#toYear option[value="'+formYear+'"]').attr("disabled", true);
	}
	else
	{
		//get selected value 
		var toYear = $('#toYear').val();
		
		// remove disable attribute from second dropdown//////
		var elems = document.getElementById('fromYear');
		for (var i = 0; i < elems.length; i++) {
			elems[i].removeAttribute('disabled');
		}
		
		// disable selected value of second drop down
		$('#fromYear option[value="'+toYear+'"]').attr("disabled", true);
	} 	
}


////disabled option when select one dropdown//////
function disabledMarginOption(callFrom)
{
	if(callFrom=="marginFromYear")
	{
		//get selected value 
		var formYear = $('#marginFromYear').val();
		
		// remove disable attribute from second dropdown//////
		var elems = document.getElementById("marginToYear");
		for (var i = 0; i < elems.length; i++) {
			elems[i].removeAttribute('disabled');
		}
		
		
		// disable selected value of second drop down
		$('#marginToYear option[value="'+formYear+'"]').attr("disabled", true);
	}
	else
	{
		//get selected value 
		var toYear = $('#marginToYear').val();
		
		// remove disable attribute from second dropdown//////
		var elems = document.getElementById('marginFromYear');
		for (var i = 0; i < elems.length; i++) {
			elems[i].removeAttribute('disabled');
		}
		
		// disable selected value of second drop down
		$('#marginFromYear option[value="'+toYear+'"]').attr("disabled", true);
	} 	
}
</script>
<!-- #tt-content -->
<div id="tt-footer"> </div>