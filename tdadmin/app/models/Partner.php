<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Partner extends Eloquent implements UserInterface, RemindableInterface {
	
	public $timestamps = false;

	protected $primaryKey = 'id';
	
	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'partner';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	protected function updatePartner($postData) {

		$id 					= $postData['id'];
		$title 					= $postData['title'];
		$homepage 				= $postData['homepage'];
		$address 				= $postData['address'];
		$contact 				= $postData['contact'];
		$phone 					= $postData['phone'];
		$mobile 				= $postData['mobile'];
		$location 				= $postData['location'];
		$business_address 		= $postData['business_address'];
		$other 					= $postData['other'];
		$bank_name 				= $postData['bank_name'];
		$bank_user 				= $postData['bank_user'];
		$bank_no 				= $postData['bank_no'];
		
		if(!empty($id) && $id>0){
			
			$sql			= "
									UPDATE 
										partner
									SET
										title				=	'$title',
										homepage			=	'$homepage',
										address				=	'$address',
										contact				=	'$contact',
										phone				=	'$phone',
										mobile				=	'$mobile',
										location			=	'$location',
										business_address	=	'$business_address',
										other				=	'$other',
										bank_name			=	'$bank_name',
										bank_user			=	'$bank_user',
										bank_no				=	'$bank_no'
									WHERE
										id = '$id'
";
			// var_debug($sql);exit();
			DB::update($sql);
		}
	
		

	}	
	
	
}