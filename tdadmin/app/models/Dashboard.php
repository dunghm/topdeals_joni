<?php
class Dashboard extends \Eloquent 
{
	/**
	* DAILY SALE START
	*/
	#get today sale which is piad
	protected function todaySale($daytime)
	{
		$sql			=	"SELECT count(id) AS todaySale FROM `order` WHERE `create_time` >= $daytime AND `state` = 'pay' ";
		$todaySaleCount = DB::select($sql);
		
		$todaySale	=	0;
		if(isset($todaySaleCount[0]->todaySale)){
			$todaySale = $todaySaleCount[0]->todaySale;
		}
		
		return $todaySale;
	}
	
	//GET TOTAL NUMBER OF PLACE ORDER TODAY
	protected function totalOrderPlaceToday($daytime){
		
		$sql			=	"SELECT count(id) AS totalPlaceOrder FROM `order` WHERE `create_time` >= $daytime AND `state` NOT IN ('temporary','refund') ";
		$todaySaleCount = DB::select($sql);
		
		$totalPlaceOrder	=	0;
		if(isset($todaySaleCount[0]->totalPlaceOrder)){
			$totalPlaceOrder = $todaySaleCount[0]->totalPlaceOrder;
		}
		
		return $totalPlaceOrder;
	}
	
	/*
	 * @name:	todayPaidOrder
	 * @author:	M.Sajid
	 */
	protected function todayPaidOrder($daytime){
		
		$sql			=	"SELECT count(id) AS todayPaidOrder 
							FROM `order` 
							WHERE `create_time` >= $daytime AND `state` = 'pay' ";
		$todaySaleCount = DB::select($sql);
		
		$todayPaidOrder	=	0;
		if(isset($todaySaleCount[0]->todayPaidOrder)){
			$todayPaidOrder = $todaySaleCount[0]->todayPaidOrder;
		}
		
		return $todayPaidOrder;
	}
	
	/*
	 * @name:	todayUnPaidOrder
	 * @author:	M.Sajid
	 */
	protected function todayUnPaidOrder($daytime){
		
		$sql			=	"SELECT count(id) AS todayUnPaidOrder 
							FROM `order` 
							WHERE `create_time` >= $daytime AND `state` = 'unpay' ";
		$todaySaleCount = DB::select($sql);
		
		$todayUnPaidOrder	=	0;
		if(isset($todaySaleCount[0]->todayUnPaidOrder)){
			$todayUnPaidOrder = $todaySaleCount[0]->todayUnPaidOrder;
		}
		
		return $todayUnPaidOrder;
	}
	
	//GET TOTAL NUMBER OF PLACE ORDER AMOUNT TODAY
	protected function totalAmountOrderPlaceToday($daytime){
		
		$sql			=	"SELECT  sum(`origin`+`card`) AS todaySale FROM `order` WHERE `create_time` >= $daytime AND `state` = 'pay' ";
		
		$todaySaleCount = DB::select($sql);
		
		$todaySale	=	0;
		if(isset($todaySaleCount[0]->todaySale)){
			$todaySale = $todaySaleCount[0]->todaySale;
		}
		
		return $todaySale;
	}
	
	/**
	* DAILY SALE END
	*/
	
	/**
	* PERCENTAGE CALCULATION START
	*/
	protected function calculatePercentage($count, $total){
		
		$totalPerc						=	($count/$total) * 100;
		$totalPerc						= 	number_format($totalPerc, 2, '.', '');
		
		return $totalPerc;
	}
	
	/**
	* PERCENTAGE CALCULATION END
	*/
	
	/**
	* CALCULATE DAILY MARGIN START
	*/
	
	#Daily margin = Total Topdeal Margin Today ( Sum of Margin from Paid Order, Margin = TopDeal Team Price - Partner Revenue )
	protected function dailyMargin($daytime){
		
		
		$sql	=	<<<SQL
						SELECT 
							SUM(CASE 
								WHEN 
									oi.option_id > 0
								THEN
									(SELECT (team_price - partner_revenue) AS teamMargin FROM team_multi WHERE id = oi.option_id )
								ELSE
									(SELECT  (team_price - partner_revenue) AS teamMargin  FROM team WHERE id = oi.team_id )
								END ) AS dailyMargin
						 FROM 
							`order` AS o
						 INNER JOIN
							`order_item` AS oi
							ON
							oi.order_id = o.id	
						 WHERE
							o.`create_time` >= $daytime 
						 AND
							o.`state` = 'pay' 

SQL;

		$dailyMargin = DB::select($sql);
		
		$dailyMarginCount	=	0;
		if(isset($dailyMargin[0]->dailyMargin)){
			$dailyMarginCount = $dailyMargin[0]->dailyMargin;
		}
		
		return $dailyMarginCount;
	}
	
	// Total Amount Received in Paid Orders 
	protected function dailyMarginTotal($daytime){
		
		$sql	=	<<<SQL
						SELECT 
							SUM(o.origin) AS dailyMargin
						FROM 
						`order` AS o
						WHERE
							o.`create_time` >= $daytime 
						AND
							o.`state` = 'pay'

SQL;

		$dailyMargin = DB::select($sql);
		
		$dailyMarginCount	=	0;
		if(isset($dailyMargin[0]->dailyMargin)){
			$dailyMarginCount = $dailyMargin[0]->dailyMargin;
		}
		
		return $dailyMarginCount;
	}
	
	/**
	* CALCULATE DAILY MARGIN END
	*/
	
	
	/*
	* Today Create Account
	*/
	protected function todayNewUser($daytime){
		
		$sql = <<<SQL
					SELECT count(id) AS todayUser FROM user WHERE create_time >= $daytime
SQL;
	
		$todayNewUser = DB::select($sql);
		
		$todayUser	=	0;
		if(isset($todayNewUser[0]->todayUser)){
			$todayUser = $todayNewUser[0]->todayUser;
		}
		
		return $todayUser;
	}
	
	/**
	* TOP VIEW PRODUCT
	*/
	
	protected function topViewProduct(){
		
		$sql	=	"SELECT
							dh.deal_id,
							(SELECT title_fr FROM team WHERE id = dh.deal_id) AS deal_title,
							count(dh.id) AS numberOfView
						FROM
							deal_view_history AS dh
						GROUP BY 
							dh.deal_id
						ORDER BY numberOfView DESC LIMIT 5";

		$topViewProduct = DB::select($sql);		
		return $topViewProduct;
	}
	
	/**
	* Top Purchased products = Top 5 list of Products sold today order by # of Sales
	*/
	
	protected function topPurchasedProduct($daytime){
		
		$sql	=	<<<SQL
						SELECT
							oi.team_id 		AS team_id,
							(SELECT title_fr FROM team WHERE id = oi.team_id ) AS deal_title,
							SUM(oi.quantity)	AS numberOfPurchase
						FROM
							`order` AS o
						LEFT JOIN
							order_item AS oi
						ON 
							oi.order_id = o.id
						WHERE
							o.`state` = 'pay'
						AND
						`create_time` >= $daytime
						GROUP BY oi.team_id 
						ORDER BY numberOfPurchase DESC
						LIMIT 5
SQL;
		$topPurchasedProduct = DB::select($sql);
		
		return $topPurchasedProduct;
	}
	
	/**
	* Top Purchased category = Top 5 list of Product categories sold today order by # of sales
	*/
	
	protected function topPurchasedCategory($daytime){
		
		$sql	=	<<<SQL
						SELECT
							oi.team_id 		AS team_id,
							(SELECT t.group_id FROM team AS t WHERE t.id = oi.team_id ) AS group_id,
							(SELECT c.name FROM team AS t LEFT JOIN category AS c ON c.id = t.group_id WHERE t.id = oi.team_id ) AS categoryName,
							SUM(oi.quantity)	AS numberOfPurchase
						FROM
							`order` AS o
						LEFT JOIN
							order_item AS oi
						ON 
							oi.order_id = o.id
						WHERE
							o.`state` = 'pay'
						AND
						`create_time` >= $daytime
						GROUP BY categoryName
						ORDER BY numberOfPurchase DESC
						LIMIT 5
SQL;
		$topPurchasedProduct = DB::select($sql);
		
		return $topPurchasedProduct;
	}
	
	/**
	* Top buyers = top 5  buyers list
	*/
	
	protected function topBuyer(){
		
		$sql	=	<<<SQL
						SELECT
							o.user_id 					AS user_id,
							u.email						AS email,
							u.username 					AS userName,
							u.realname 					AS realName,
							CONCAT(u.first_name,' ',u.last_name) 		AS fullName,
							SUM(o.money + o.credit + o.card)				AS totalPurchase
						FROM
							`order` AS o
						LEFT JOIN
						 `user` AS u
						ON u.id = o.user_id
						WHERE
							o.`state` = 'pay'
						GROUP BY o.user_id 
						ORDER BY totalPurchase DESC
						LIMIT 5
SQL;
		$topBuyer = DB::select($sql);
		
		return $topBuyer;
	}
	
	//GET CATEGORY LIST
	protected function getCategoryList(){
		
		$sql	=	<<<SQL
						SELECT
							id,
							`name`
						FROM 
							category
						WHERE
							zone = 'group'
						AND
							display = 'Y'
						ORDER BY id DESC
SQL;
		$getCategoryList = DB::select($sql);
		
		return $getCategoryList;
	}
	
	/**
	* Best sellers by category = Top product sold per category. It will create list when we take a Top product from each 
	* Category
	*/
	
	protected function bestSellerByCategory($categoryId){
		
		
		$categoryCondition	=	"";
		//GET RECORD BY CATEGORY ID
		if($categoryId!="all"){
			
			if (is_numeric($categoryId) && $categoryId > 0) {
				$categoryCondition	=	"  AND  group_id = $categoryId";
			}
		}
		
		$sql	=	<<<SQL
						SELECT
							oi.team_id 			AS team_id,	
							t.group_id			AS group_id,
							t.title				AS title,
							SUM(oi.quantity)	AS numberOfPurchase
						FROM
							`order` AS o
						LEFT JOIN
							order_item AS oi
						ON 
							oi.order_id = o.id
						LEFT JOIN 
							team AS t
						 ON
							t.id = oi.team_id
						WHERE
							o.`state` = 'pay'
						$categoryCondition
						GROUP BY oi.team_id 
						ORDER BY numberOfPurchase DESC
						LIMIT 5
SQL;
		$bestSellerByCategory = DB::select($sql);
		
		return $bestSellerByCategory;
	}
	
		//GET REVENUE BY DATE RANGE
	
	protected function getRevenueByRange($range = "1 WEEK", $recordShow = ""){
		
		$groupBy 			= 	"GROUP BY order_day";
		$orderBy 			= 	"ORDER BY order_day";
		$coloumnSelection	=	"CONCAT(MONTHNAME(FROM_UNIXTIME(o.create_time)),' ',DAY(FROM_UNIXTIME(o.create_time)),', ',YEAR(FROM_UNIXTIME(o.create_time))) as order_day,";
		$dateCondition		=	" AND DATE(FROM_UNIXTIME(o.create_time)) >= DATE(DATE_SUB(NOW(), INTERVAL $range))";
		$dateRange			=	"DATE(DATE_SUB(NOW(), INTERVAL $range))";
		//IF RECORD FOR MONTH BASIS
		if($recordShow == 'month'){
			$coloumnSelection	=	"CONCAT(MONTHNAME(FROM_UNIXTIME(o.create_time)),', ',YEAR(FROM_UNIXTIME(o.create_time))) as order_day,";
			$orderBy 			= 	"ORDER BY o.create_time DESC";
		}
		
		//IF RECORD FOR YEAR BASIS
		if($recordShow == 'yearly'){
			$coloumnSelection	=	"YEAR(FROM_UNIXTIME(o.create_time)) as order_day,";
			$dateRange			=	"YEAR(FROM_UNIXTIME(o.create_time))";
			$orderBy 			= 	"ORDER BY o.create_time DESC";
			$dateCondition = "";
		}
		
		$sql	=	<<<SQL
						SELECT 
							IFNULL(SUM(CASE 
								WHEN 
									oi.option_id > 0
								THEN
									(SELECT (team_price - partner_revenue) AS teamMargin FROM team_multi WHERE id = oi.option_id )
								ELSE
									(SELECT  (team_price - partner_revenue) AS teamMargin  FROM team WHERE id = oi.team_id )
								END ),0) AS dailyMargin,
								o.create_time,
								DATE(FROM_UNIXTIME(o.create_time)) as order_formattedDate,
								$coloumnSelection
								$dateRange
						 FROM 
							`order` AS o
						 INNER JOIN
							`order_item` AS oi
							ON
							oi.order_id = o.id	

						 AND
							o.`state` = 'pay'						
							$dateCondition
						$groupBy
						$orderBy
SQL;

		// echo "<pre>";print_r($sql);exit();
		$getRevenueByRange = DB::select($sql);
		
		return $getRevenueByRange;
	}
	
	
	protected function offlineTopUp()
	{
		$now = date('Y-m-d');
		$total = 0;
		
		$offlineTopUp = DB::select("select ifnull(sum(money),0) as total from flow 
									where action = 'store' and DATE_FORMAT(FROM_UNIXTIME(create_time), '%Y-%m-%d') <'$now' ");
		if(is_array($offlineTopUp) && count($offlineTopUp)>0)
		{
			$offlineTopUp = $offlineTopUp[0];
			$total = $offlineTopUp->total;
		}
		return $total;
	}
	
	/*
	 * @name:	todayTopUp
	 * @author:	M.Sajid
	 */
	protected function todayTopUp()
	{
		$now = date('Y-m-d');
		$total = 0;
		
		$todayTopUp = DB::select("select ifnull(sum(money),0) as total from flow 
									where action = 'store' and DATE_FORMAT(FROM_UNIXTIME(create_time), '%Y-%m-%d') >= '$now' ");
		if(is_array($todayTopUp) && count($todayTopUp)>0)
		{
			$todayTopUp = $todayTopUp[0];
			$total = $todayTopUp->total;
		}
		return $total;
	}
	
	/*
	 * @name:	totalTopUp
	 * @author:	M.Sajid
	 */
	protected function totalTopUp()
	{
		$now = date('Y-m-d');
		$total = 0;
		
		$totalTopUp = DB::select("select ifnull(sum(money),0) as total from flow 
									where action = 'store' ");
		if(is_array($totalTopUp) && count($totalTopUp)>0)
		{
			$totalTopUp = $totalTopUp[0];
			$total = $totalTopUp->total;
		}
		return $total;
	}	
	
	//GET TOTAL NUMBER OF PLACE ORDER AMOUNT BY MONTHLY WISE
	protected function totalNumberMonthlySales($year=0){
		
		$sql =	"SELECT
					MONTH(FROM_UNIXTIME(create_time)) AS month_num,YEAR(FROM_UNIXTIME(create_time)) AS YEAR,
					IFNULL(SUM(`origin`+`card`),0) AS total
				FROM 
					`order`
				WHERE 
					YEAR(FROM_UNIXTIME(create_time)) = '$year'
				AND 
					state = 'pay'
				GROUP BY
					MONTH(FROM_UNIXTIME(create_time))";
		
		$totalNumberMonthlySales = DB::select($sql);
		
		return $totalNumberMonthlySales;
	}
	
	//get min user register year
	protected function mimmumSalesYear()
	{
	    $sql = "SELECT
					MIN(FROM_UNIXTIME(create_time, '%Y')) AS startYear
				FROM
					`order`
				WHERE
					state = 'pay'	
					";
		
		// echo "<pre>";print_r($sql);exit();
		$getStartRegisterYear = DB::select($sql);
		
		return $getStartRegisterYear;
    } 

	//GET TOTAL NUMBER OF PLACE ORDER AMOUNT BY MONTHLY WISE
	protected function totalNumberMonthlyMargin($year=0){
		
		$sql =	"SELECT 
				IFNULL(SUM(CASE 
					WHEN 
						oi.option_id > 0
					THEN
						(SELECT (team_price - partner_revenue) AS teamMargin FROM team_multi WHERE id = oi.option_id )
					ELSE
						(SELECT  (team_price - partner_revenue) AS teamMargin  FROM team WHERE id = oi.team_id )
					END ),0) AS total,
					MONTH(FROM_UNIXTIME(create_time)) AS month_num,YEAR(FROM_UNIXTIME(create_time)) AS YEAR
			 FROM 
				`order` AS o
			 INNER JOIN
				`order_item` AS oi
				ON
				oi.order_id = o.id
			WHERE 
				YEAR(FROM_UNIXTIME(create_time)) = '$year'
			AND 
				state = 'pay'
			GROUP BY
				MONTH(FROM_UNIXTIME(create_time))";
		
		$totalNumberMonthlyMargin = DB::select($sql);
		
		return $totalNumberMonthlyMargin;
	}	
}