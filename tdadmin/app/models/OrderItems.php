<?php
class OrderItems extends \Eloquent 
{
	protected function getOrderItemsById($orderItemId = 0)
	{
		$getOrderItemsById = DB::select("select * from `order_item` WHERE `id` = '$orderItemId'");
		return $getOrderItemsById;
	}
	
	protected function getOrderItemsByOrderId($orderId = 0)
	{
		$getOrderItemsByOrderId = DB::select("select * from `order_item` WHERE `order_id` = '$orderId'");
		return $getOrderItemsByOrderId;
	}
	
	#delete record
	protected function deleteOrderItem($id=0)
	{
		$deleted = DB::delete('delete from order_item where id ='.$id);
		return $deleted;
	}
	
	protected function getOrderItemCountByOrderId($orderId=0)
	{
		$OrderItemCount = DB::select('select count(*) as Totalcount from order_item where order_id ='.$orderId);
		return $OrderItemCount;
	}
	
	
	protected function getOrderItemDetailByOrderId($orderId=0)
	{
		$OrderItems = DB::select(
		"select 
			t.title as teamTitle, 
				tm.title as optionTitle,
			oi.quantity as orderQuantity,oi.total as totalPice,oi.fare as orderfare,oi.cashback_amount
			
				
		from 
			order_item oi 
		Inner join team t
		On 
		t.id = oi.team_id
		left join team_multi tm
		on 
		t.id =tm.team_id
		where 
		oi.order_id ='$orderId'
		group by oi.id
		"
		);
		return $OrderItems;
	}
	
	
	protected function getOrderItemByTeamId($teamId=0)
	{
		$getOrderItemsByteamId = DB::select("select * from `order_item` WHERE `team_id` = '$teamId' AND delivery_status=1 AND option_id is NULL");
		return $getOrderItemsByteamId;
	}
	
	protected function getOrderItemByTeamOptionId($optionId=0)
	{
		$getOrderItemsByteamOptionId = DB::select("select * from `order_item` WHERE `option_id` = '$optionId' AND delivery_status=1");
		return $getOrderItemsByteamOptionId;
	}

}