<?php

class Coupon extends \Eloquent {
	
	protected $primaryKey = 'id';
	
	protected $table = 'coupon';
	
	protected function getCouponById($couponId=0) {
		$coupon = DB::select("SELECT * 
						FROM coupon c
						WHERE c.id = '$couponId'");
		return $coupon;
	}

	protected function getOrderItemByCouponOrderId($couponOrderId=0) {
		$coupon = DB::select("SELECT * 
						FROM order_item oi
						WHERE id = '$couponOrderId'");
		return $coupon;
	}	
	
	protected function Consume($coupon) {
		
		if ( !$coupon->consume =='N' ) return false;
		
		
		$ip 			= GetRemoteIp();
		$consume_time	= time();
		$consume		= 'Y';
		$couponId		= $coupon->id;	
		
		
		$sql			= "
									UPDATE 
										coupon
									SET
										ip					=	'$ip',
										consume_time		=	'$consume_time',
										consume				=	'$consume'
									WHERE
										id = '$couponId'
							";
			 // var_debug($sql);exit();
		DB::update($sql);
		$this->CreateFromCoupon($coupon);
		return true;
	}
	
	
	protected function CreateFromCoupon($coupon) {
	
		if ( $coupon->credit <= 0 ){ return 0; }

		if( $coupon->user_id == "" ){ return 0; } 
		//update user money;
		$userId				=	$coupon->user_id;
		$cp_credit			=	$coupon->credit;
		$cp_id				=	$coupon->id;
		$currentDateTime	=	time();
		
		//GET USER INFO BY USER ID
		$sql		=	"SELECT money FROM user WHERE id = '$userId'";
		$userData	=	DB::select($sql);
		
			if(!empty($userData)&&isset($userData[0])){
				
				$userMoney	=	$userData[0]->money;
				$userMoney	=	$userMoney	+ $cp_credit;
				
				$couponUpdate	=	<<<SQL
											UPDATE
													user
											SET 
												money	=	$userMoney
											WHERE 
												id = $userId
										
SQL;
				DB::update($sql);
				
				//ADD FLOW DATA
				
				$sqlInsert	=	<<<SQL
									
										INSERT
											INTO
										flow
											(
												user_id,
												money,
												direction,
												action,
												detail_id,
												create_time
											)
											VALUES
											(
												'$userId',
												'$cp_credit',
												'income',
												'coupon',
												'$cp_id',
												'$currentDateTime'
											)
SQL;
				
				return DB::insert($sqlInsert);
				
			}

	}
	
	//UPDATE UpdateShippingState
	protected function UpdateShippingState($state,$order_id){
		
		if(!empty($state) && !empty($order_id)){
			
				$orderStateUpdate	=	<<<SQL
											UPDATE
													order_item
											SET 
												delivery_status	=	$state
											WHERE 
												id = $order_id
										
SQL;
				DB::update($orderStateUpdate);
		}
	}
	
	
	
	protected function getWarehouseSectionByTeam($teamId=0)
	{
		$wareHouseSql = "Select name , description
					From
					   warehouse_section ws
					INNER JOIN 
						warehouse_section_items wsi
					ON
					   wsi.warehouse_section_id = ws.id
					Where
						wsi.team_id =".$teamId;
		$wareHouseData	=	DB::select($wareHouseSql);	
		return 	$wareHouseData;
	
	}

	//Get Coupon Delivery Type 
	protected function getCouponDeliveryType($couponId=0)
	{
		$couponDeliveryTypeSql = "SELECT 
										oi.delivery  AS delivery
									FROM 
										coupon AS c
									INNER JOIN
										order_item AS oi
									ON 
										oi.id = c.order_id
									WHERE 
										c.id = $couponId";
						
		$couponDeliveryTypeSqlData	=	DB::select($couponDeliveryTypeSql);	
		
		$deliveryType	=	"";
		if(!empty($couponDeliveryTypeSqlData) && isset($couponDeliveryTypeSqlData[0])){
			$deliveryType	=	$couponDeliveryTypeSqlData[0]->delivery;
		}
		
		return 	$deliveryType;
	
	}
	
	
	//DELETE COUPON FOR ORDER
	protected function deleteCouponByOrderItemId($order_item_id){
		
		$sql	=	"DELETE from coupon WHERE order_id='$order_item_id'";
		DB::delete($sql);
	}
	
	//delete coupon FOR refund Item
	protected function deleteRefundItemsCoupon($params=array())
	{
		$teamId=$optionId=$quantity=$orderItemId=0;
		$callFrom= $optionCondition =$teamCondition=$limitCondition='';
		
		if(is_array($params) && count($params)>0)
		{
			#orderItemId
			if(!empty($params['orderItemId']))
			{
				$orderItemId = $params['orderItemId'];
			}
			
			#quantity
			if(!empty($params['quantity']))
			{
				$quantity = $params['quantity'];
			}
			
			#teamId
			if(!empty($params['teamId']))
			{
				$teamId = $params['teamId'];
			}
			
			#optionId
			if(!empty($params['optionId']))
			{
				if(is_numeric($params['optionId']))
				{
					$optionId = $params['optionId'];
				}
				
			}
			
			#callFrom
			if(!empty($params['callFrom']))
			{
				$callFrom = $params['callFrom'];
			}
		}
		
		//condition only apply on single order item refund or quantity
		if($callFrom=='orderRefund')
		{
			# optionCondition
			if($optionId)
			{
				$optionCondition = " and option_id ='$optionId' ";
			} 
			
			#teamCondition
			if($teamId)
			{
				$teamCondition = " and team_id = '$teamId' ";
			} 
			
			#quantity
			if($quantity)
			{
				$limitCondition = " limit $quantity ";
			} 
		}	
			
		$sql	=	"DELETE from coupon WHERE order_id='$orderItemId' 
					$optionCondition
					$teamCondition
					$limitCondition
		";
		DB::delete($sql);
		
		return true;	
		
	}

}