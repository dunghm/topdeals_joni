<?php
use Illuminate\Support\Collection;

class Order extends \Eloquent 
{
	protected $table = 'order';
	
	#protected function warehouse($order_by='',$sSortDir_0='')
	protected function getSections($get)
	{
		$data = array();
		
		//SET COLLECTION FOR CHUMPER
		$getOrderData	=	Order::getOrderData();
		$collection 	=	Collection::make($getOrderData);


		return Datatable::collection($collection)
				->showColumns(array('id','deal_id', 'user_id','quantity','origin','credit','card','money','fare','shipment_eta','service'))
				->addColumn('id', function($model) {
						if($model->id != ""){
							return "<span style='color:#24BEE5'><b>".$model->id."</b></span>";
						}
					})
				->addColumn('user_id', function($model) {
						return $model->email."<br/>".$model->username;
					})
				->addColumn('origin', function($model) {
						return CURRENCY." ".number_format($model->origin + $model->card ,2);
					})
				->addColumn('credit', function($model) {
						if($model->id != ""){
							return CURRENCY." ".number_format($model->credit ,2);
						}
					})
				->addColumn('card', function($model) {
						if($model->id != ""){
							return CURRENCY." ".number_format($model->card ,2);
						}
					})
				->addColumn('money', function($model) {
						if($model->id != ""){
							return CURRENCY." ".number_format($model->money ,2);
						}
					})
				->addColumn('fare', function($model) {
						if($model->id != ""){
							return CURRENCY." ".number_format($model->fare ,2);
						}
					})
				->addColumn('operation', function($model) {

						$currentTime	=	time();

						$link		=	"";
						//BIND DEAL  SHIPPING METHOD CHANGE LINK
						//@Author: Arshad
						if($model->deal_id!=""){
							
							$dealIdArr	=	explode("-",$model->deal_id);
							$dealId		=	end($dealIdArr);
							$orderId 	=   reset($dealIdArr);
							
							if($model->teamDelivery == 'express_pickup'){
								$link .= '<a title="Delivery Edit" id="delivery_method_change"  href="'.URL::to('admin/delivery_option_edit').'/'.$dealId.'?'.$currentTime.'"><i class="fa fa-fw fa-edit"></i></a>
							';
							}
							
							$link .= '<a title="Update Delivery Address" id="delivery_method_change"  href="'.URL::to('admin/update_delivery_address').'/'.$dealId.'?'.$currentTime.'"><i class="fa fa-fw fa-truck "></i></a>
							';
							$link .= '<a title="Order Items" id="delivery_method_change"  href="'.URL::to('admin/deal_option_edit').'/'.$dealId.'?'.$currentTime.'"><i class="fa fa-fw fa-shopping-cart "></i></a>
							';
							$link .= '<a title="Refund Item" id="delivery_method_change"  href="'.URL::to('admin/refundform').'?orderId='.$orderId.'&orderItemId='.$dealId.'&t='.$currentTime.'"><i class="fa fa-retweet"></i></a>
							';
						}
						else
						{
							$orderId 	=   $model->id;
							
							$link .= '<a title="Refund Order" id="delivery_method_change"  href="'.URL::to('admin/fullorderrefundform').'?orderId='.$orderId.'&t='.$currentTime.'"><i class="fa fa-retweet"></i></a>
							';
						}
						
						return $link;

		})		
		// ->orderColumns(array('o.id','deal_id','o.user_id','o.quantity','o.origin','o.credit','o.card','o.money','o.fare','shipment_eta','o.service'))
		->make();
	}
	
	/*
	 * @name:	soldItemDR
	 */
	protected function soldItemDR($start, $end){
		
		$sql = "select sum(oi.quantity) as count from order_item oi
				join `order` o on o.id = oi.order_id
				where o.state = 'pay' and
				DATE_FORMAT(FROM_UNIXTIME(o.create_time), '%Y-%m-%d') BETWEEN '".$start."' AND '".$end."'"; 

		$soldItemDR = DB::select($sql);
		
		$soldItemDRCount	=	0;
		if(isset($soldItemDR[0]->count)){
			$soldItemDRCount = $soldItemDR[0]->count;
		}		
		return $soldItemDRCount;
	}
	
	/*
	 * @name:	totalRevenue
	 */
	protected function totalRevenue($start, $end){

		$sql = "SELECT 
					SUM(CASE 
						WHEN 
							order_item.option_id > 0
						THEN
							(SELECT (team_multi.team_price - team_multi.partner_revenue) AS TDR FROM team_multi WHERE team_multi.id = order_item.option_id )
						ELSE
							(SELECT  (team.team_price - team.partner_revenue) AS TDR  FROM team WHERE team.id = order_item.team_id ) 
						END) AS TDR
				FROM 
					`order`
				INNER JOIN
					`order_item` ON order_item.order_id = `order`.id	
				WHERE
					`order`.state = 'pay'
				AND
				 	DATE_FORMAT(FROM_UNIXTIME(`order`.create_time), '%Y-%m-%d') BETWEEN '".$start."' AND '".$end."'";

		$totalRevenue = DB::select($sql);
		$totalRevenueCount	=	0;
		if(isset($totalRevenue[0]->TDR)){
			$totalRevenueCount = $totalRevenue[0]->TDR;
		}		
		return $totalRevenueCount;
	}
	
	/*
	 * @name:	dailyMarginDR
	 */
	protected function dailyMarginDR($start, $end){		
		
		$sql	=	"SELECT 
							SUM(CASE 
								WHEN 
									oi.option_id > 0
								THEN
									(SELECT (team_price - partner_revenue) AS teamMargin FROM team_multi WHERE id = oi.option_id )
								ELSE
									(SELECT  (team_price - partner_revenue) AS teamMargin  FROM team WHERE id = oi.team_id )
								END ) AS dailyMargin
						 FROM 
							`order` AS o
						 INNER JOIN
							`order_item` AS oi
							ON
							oi.order_id = o.id	
						 WHERE
							o.`state` = 'pay'
						 AND
						 	DATE_FORMAT(FROM_UNIXTIME(o.`create_time`), '%Y-%m-%d') BETWEEN '".$start."' AND '".$end."'";

		$dailyMargin = DB::select($sql);
		
		$dailyMarginCount	=	0;
		if(isset($dailyMargin[0]->dailyMargin)){
			$dailyMarginCount = $dailyMargin[0]->dailyMargin;
		}		
		return $dailyMarginCount;
	}
	
	/*
	 * @name:	this_order
	 */
	protected function this_order($start, $end){
		$sql = "SELECT COUNT(*) as count 
								FROM `$this->table` 
								WHERE state = 'pay' AND DATE_FORMAT(FROM_UNIXTIME(`create_time`), '%Y-%m-%d') BETWEEN '".$start."' AND '".$end."'";
		$this_order = DB::select($sql)[0]->count;
		return $this_order;
	}
	
	//GET ORDER DATA
	protected function getOrderData()
	{
		//CUSTOM PAGINATION FOR ORDER LISTING
		$perPage    =    RECORD_PERPAGE;
		$offset     =    "";
		
		if (Session::has('order_perPage')){
			$perPage = Session::get('order_perPage');
		}
		
		if (Session::has('order_offset')){
			$offset = Session::get('order_offset');
		}
		
		//GET RESULT BY SEARCH
		$searchQuery	=	"";
		if (Session::has('sr_order_num')){
			$sr_order_num = Session::get('sr_order_num');
			$searchQuery  .= " AND `o`.id = '$sr_order_num'";
		}
		
		if (Session::has('sr_user')){
			$sr_user = Session::get('sr_user');
			$searchQuery  .= " AND (`u`.`email` LIKE '%%%$sr_user%%%' OR `u`.`username` LIKE '%%%$sr_user%%%')";
		}
		
		$DealSearch	=	"";
		
		if (Session::has('sr_deal_num')){
			$sr_deal_num = Session::get('sr_deal_num');
			$DealSearch  = " WHERE team_id = '$sr_deal_num'";
		}
		
		if (Session::has('sr_delivery_status')){
			$sr_delivery_status = Session::get('sr_delivery_status');
			
			if($DealSearch==""){
				$DealSearch  = " WHERE delivery = '$sr_delivery_status'";
			}else{
				$DealSearch  .= " AND delivery = '$sr_delivery_status'";
			}
		}
		
		if (Session::has('sr_delivery_type')){
			$sr_delivery_type = Session::get('sr_delivery_type');
			
			if($DealSearch==""){
				$DealSearch  = " WHERE delivery_status = '$sr_delivery_type'";
			}else{
				$DealSearch  .= " AND delivery_status = '$sr_delivery_type'";
			}
		}
		
		
		//SET ORDER DATA
		$getOrderData	=	array();
		
		$sqlData	=	<<<SQL
							SELECT 
								`o`.`id`,
								'' AS deal_id,
								`u`.`email`,
								`u`.`username`,
								`o`.`quantity`,
								`o`.`origin`,
								`o`.`credit`,
								`o`.`card`,
								`o`.`money`,
								`o`.`fare`,
								'' AS shipment_eta,
								`o`.`service`,
								''	AS  teamDelivery
							FROM 
								`order` AS `o`
							LEFT JOIN 
								`user` AS `u`
							ON 
								`u`.id = `o`.user_id
							WHERE 
							`o`.id IN (SELECT DISTINCT order_id FROM order_item $DealSearch ) 
								AND 
							`o`.state = 'pay' 
							$searchQuery
							ORDER BY 
								`o`.pay_time
							DESC LIMIT $perPage $offset
SQL;
			$sqlDataResult = DB::select($sqlData);
			
			//SET ORDER DATA
			if(!empty($sqlDataResult)){
				
				foreach($sqlDataResult as $sqlDataResultRow){
					
					$getOrderData[]	=	$sqlDataResultRow;
					
					if(isset($sqlDataResultRow->id)){
						$orderId	=	$sqlDataResultRow->id;
						if($orderId>0){
							//GET ORDER ITEM RECORD
							$sqlOrderItem	=	<<<SQL
													SELECT
														'' AS id,
														CONCAT(oi.order_id,' - ',oi.id) AS deal_id,
														oi.team_id AS `user_id`,
														oi.team_id AS `email`,
														t.title   	 AS team_title,
														tm.title_fr   AS team_multi_title,
														oi.quantity AS quantity,
														oi.price	 AS origin,
														0 AS credit,
														0 AS card,
														0 AS money,
														 oi.fare	 AS fare,
														 oi.delivery 				AS tmDelivery,
														 oi.delivery_status 		AS tmDeliveryStatus,
														 t.fare 					AS tmFare,
														 oi.delivery_time		 	AS oiDeliveryTime,
														 t.delivery					AS  teamDelivery

													FROM
														order_item AS oi
													LEFT JOIN
														team AS t
													  ON
														t.id = oi.team_id
													LEFT JOIN
														team_multi AS tm
												    ON
														oi.option_id = tm.id
													WHERE 
														oi.order_id = $orderId
SQL;
							$sqlOrderItemData = DB::select($sqlOrderItem);
							
							//SET ORDER ITEM DATA IN ARRAY
							if(!empty($sqlOrderItemData)){
								foreach($sqlOrderItemData as $sqlOrderItemDataRow){
									
									//SET TEAM TITLE LINK
									$team_title	=	$sqlOrderItemDataRow->team_title;
									if(!empty($sqlOrderItemDataRow->team_multi_title) && $sqlOrderItemDataRow->team_multi_title!="" && $sqlOrderItemDataRow->team_multi_title != NULL){
										$team_title	=	$sqlOrderItemDataRow->team_multi_title;
									}
									
									$teamiId	=	$sqlOrderItemDataRow->user_id;
									$teamUrl	=	TOPDEAL_URL_LINK."/team.php?id=$teamiId";
									$sqlOrderItemDataRow->username	=	" ( <a href='$teamUrl' target='_blank'>".$team_title."</a> ) ";
									
									
									//SET DELIVERY STATUS NAME & TIME FOR PAYMENT COLOUMN									
									$sqlOrderItemDataRow->service	=	"";
									if( $sqlOrderItemDataRow->tmDelivery == 'express' || $sqlOrderItemDataRow->tmDelivery == 'pickup'){
										$deliveryStatusArray	=	deliveryStatusArray();
										
										$tmDelviery	=	$sqlOrderItemDataRow->tmDeliveryStatus;
										$oiDeliveryTime	=	$sqlOrderItemDataRow->oiDeliveryTime;
										
										$service	=	"";
										if(isset($deliveryStatusArray[$tmDelviery])){
											$service	=	$deliveryStatusArray[$tmDelviery];
										}
										
										if(!empty($oiDeliveryTime) && $oiDeliveryTime!=""){
											$service	.=	"<br/> ( ".date("d-m-Y",$oiDeliveryTime). " ) ";
										}
										$sqlOrderItemDataRow->service	=	$service;
									}
									
									//SET SHIPING ETA
									$optionDelivery	=	optionDelivery();
									if(isset($optionDelivery[$sqlOrderItemDataRow->tmDelivery])){
										
										$sqlOrderItemDataRow->shipment_eta	=	$optionDelivery[$sqlOrderItemDataRow->tmDelivery];//. "<br/> ( CHF ".number_format($sqlOrderItemDataRow->tmFare,2).")";
										
									}else{
										$sqlOrderItemDataRow->shipment_eta	=	"";//"<br/> ( CHF ".number_format($sqlOrderItemDataRow->tmFare,2).")";
									}
									
									//SET DEAL OPTION TOTAL AMOUNT AGAINST ORDER SHIPPING
									// $orderFare		=	$sqlDataResultRow->fare;
									// $orderTmFare	=	$sqlOrderItemDataRow->tmFare;
									// if($orderFare > 0){
										// if($orderFare != $orderTmFare){
											// $sqlOrderItemDataRow->origin	=	$sqlOrderItemDataRow->credit;
										// }
									// }
									
									$getOrderData[]	=	$sqlOrderItemDataRow;
								}
							}
							
						}
					}
				}
			}
			// echo "<pre>";print_r($getOrderData);exit();
			return $getOrderData;
	}
	
	//ORDER LISTING CUSTOM PAGINATION
	protected function orderCustomPagination($perPage, $offset){
		

		//GET RESULT BY SEARCH
		$searchQuery	=	"";
		if (Session::has('sr_order_num')){
			$sr_order_num = Session::get('sr_order_num');
			$searchQuery  .= " AND `o`.id = '$sr_order_num'";
		}
		
		if (Session::has('sr_user')){
			$sr_user = Session::get('sr_user');
			$searchQuery  .= " AND (`u`.`email` LIKE '%%%$sr_user%%%' OR `u`.`username` LIKE '%%%$sr_user%%%')";
		}
		
		$DealSearch	=	"";
		
		if (Session::has('sr_deal_num')){
			$sr_deal_num = Session::get('sr_deal_num');
			$DealSearch  = " WHERE team_id = '$sr_deal_num'";
		}
		
		if (Session::has('sr_delivery_status')){
			$sr_delivery_status = Session::get('sr_delivery_status');
			
			if($DealSearch==""){
				$DealSearch  = " WHERE delivery = '$sr_delivery_status'";
			}else{
				$DealSearch  .= " AND delivery = '$sr_delivery_status'";
			}
		}
		
		if (Session::has('sr_delivery_type')){
			$sr_delivery_type = Session::get('sr_delivery_type');
			
			if($DealSearch==""){
				$DealSearch  = " WHERE delivery_status = '$sr_delivery_type'";
			}else{
				$DealSearch  .= " AND delivery_status = '$sr_delivery_type'";
			}
		}
		
		$sql	=	<<<SQL
							SELECT 
								`o`.`id`
							FROM 
								`order` AS `o`
							LEFT JOIN 
								`user` AS `u`
							ON 
								`u`.id = `o`.user_id
							WHERE 
							`o`.id IN (SELECT DISTINCT order_id FROM order_item $DealSearch ) 
								AND 
							`o`.state = 'pay' 
							$searchQuery
							ORDER BY 
								`o`.pay_time
							DESC LIMIT $perPage $offset
SQL;
		 $items        =    DB::select($sql);

// echo "<pre>";print_r($sql);exit();
		$totalItemRecord    =    DB::select("SELECT 
												count(`o`.`id`) AS totalRecord
											FROM 
												`order` AS `o`
											LEFT JOIN 
												`user` AS `u`
											ON 
												`u`.id = `o`.user_id
											WHERE 
											`o`.id IN (SELECT DISTINCT order_id FROM order_item $DealSearch ) 
												AND 
											`o`.state = 'pay' 
											$searchQuery");
		
		$totalItems = 0;
		if(!empty($totalItemRecord)&&isset($totalItemRecord[0]->totalRecord)){
			$totalItems	=	$totalItemRecord[0]->totalRecord;
		}
		
		return Paginator::make($items, $totalItems, $perPage);

	}
	
	
##########################################################Order refund#####################################################

	protected function getOrderById($orderId = 0)
	{
		$getOrderById = DB::select("select * from `order` WHERE `id` = '$orderId'");
		return $getOrderById;
	}
	
	protected function getOrderCardByOrderId($orderId = 0)
	{
		$getOrderCardByOrderId = DB::select("select * from `order_card` WHERE `order_id` = '$orderId'");
		return $getOrderCardByOrderId;
	}
	
	protected function insertRecord($tableName ='',$params=array())
	{
		DB::table($tableName)->insert(
						$params
						);
		return true;				
	}
	
	#update order 
	protected function updateQuantity($tableName ='',$id=0,$fieldsWithValue='')
	{
		$updated = DB::update("update `$tableName` set $fieldsWithValue  where id = ?", array($id));
		return $updated;
	}
	
	
	//GET ORDER ITEM DETAIL
	protected function getOrderItemDetail($order_item_id){
		
		$sql	=	<<<SQL
							SELECT
								*
							FROM 
								order_item AS oi
							WHERE
								oi.id = $order_item_id
SQL;
		$orderItemDetail = DB::select($sql);
		
		$orderItemDetailArr	=	array();
		if(isset($orderItemDetail[0]) && !empty($orderItemDetail[0])){
			$orderItemDetailArr	=	$orderItemDetail[0];
		}
		
		return $orderItemDetailArr;
	}
	
	//UPDATE SHIPPING METHOD
	protected function UpdateShippingMethod($order_item_id,$delivery_method,$orderItemDeliveryStatus){
		
		DB::update("UPDATE order_item SET delivery = ?, delivery_status = ? WHERE id= ?", array($delivery_method,$orderItemDeliveryStatus, $order_item_id));
	}
	
	//GET COUNT for 'Express' METHOD 
	protected function getShippingMethodOfOrder($order_id){
		
		$orderItemShippingMethod	=	DB::select('SELECT COUNT(delivery) AS orderItemShippingMethod FROM `order_item` WHERE order_id = ? AND delivery = ? ', array($order_id,'express'));
		
		$orderItemShippingMethodArr	=	array();
			if(isset($orderItemShippingMethod[0]->orderItemShippingMethod)){
			$orderItemShippingMethodArr	=	$orderItemShippingMethod[0]->orderItemShippingMethod;
		}
		
		return $orderItemShippingMethodArr;
	}
	
	//GET SUBMITTED ORDER ITEM HAS PREVIOUS METHOD EXPRESS
	protected function getShippingMethodOfOrderItem($order_item_id){
		
		$orderItemShippingMethod	=	DB::select('SELECT delivery FROM `order_item` WHERE id = ? ', array($order_item_id));
		
		$deliveryMethod	=	"";
		if(isset($orderItemShippingMethod[0]->delivery)){
			$deliveryMethod	=	$orderItemShippingMethod[0]->delivery;
		}
		return $deliveryMethod;
	}
	
	//TOTAL NUMBER OF ORDER ITEM
	protected function getTotalOrderItem($order_id){
		
		$orderItemShippingMethod	=	DB::select('SELECT COUNT(delivery) AS orderItemShippingMethod FROM `order_item` WHERE order_id = ? ', array($order_id));
		
		$orderItemShippingMethodArr	=	array();
			if(isset($orderItemShippingMethod[0]->orderItemShippingMethod)){
			$orderItemShippingMethodArr	=	$orderItemShippingMethod[0]->orderItemShippingMethod;
		}
		
		return $orderItemShippingMethodArr;
	}
	
		//UPDATE ORDER AMOUNT FOR ALL ITEM TRANSFER PICKUP FROM EXPRESS
	protected function orderAmountTransferFromExpressToPickup($order_id,$getOrderItemDetail){
		
		//GET ORDER & ORDER ITEM INFO
		$getOrderById					=	Order::getOrderById($order_id);
		
		$getOrderItemFareTotalByOrderId	=	Order::getOrderItemFareTotalByOrderId($order_id);
					
		//SELECTED ORDER ITEM
		$orderItemId	=	$getOrderItemDetail->id;
		
		if(isset($getOrderById[0]->id)){
			//GET ORDER FARE AMOUNT TO SET ON CUSTOMER CREDIT 
		
			$user_id	=	$getOrderById[0]->user_id;
			$orderFare	=	$getOrderById[0]->fare;
			$money		=	$getOrderById[0]->money;
			$origin		=	$getOrderById[0]->origin;
			$credit		=	$getOrderById[0]->credit;
			$card		=	$getOrderById[0]->card;
			
			
			//CREATE ARRAY OF ORIGIN,CREDIT,CARD TO GET MAX VALUE AND MINUS FARE FROM IT TO AVOID NEGATIVE VALUE
			$maxAmountArr	=	array('money'=>$money,'credit'=>$credit,'card'=>$card);
			$maxAmount 		=  max($maxAmountArr);
			//GET NAME OF VARIABLE WHICH HAS MAX VALUE
			$maxKey = array_search($maxAmount, $maxAmountArr); 
			//MINUS FARE BY VARIABLE VARIABLE
			$$maxKey	=  $$maxKey - $orderFare;
			//NEW ORDER TOTAL
			if( $maxKey	!=	'card'){
				if($origin >= $orderFare){
					$origin 	= $origin - $orderFare;
				}else{
					$origin 	= $orderFare - $origin;
				}
			}
			
			// echo $$maxKey."<br/>";
			//PATCH TO SOLVE CALUCLATION ISSUE 
			if($card <= 0){
				if(array_sum(array($money,$credit,$card)) != $origin ){
					$$maxKey	=  $$maxKey + $orderFare;
				}
			}
			// echo $$maxKey;
			// echo "<br/>".$origin;
			// exit();
			// $money			=	$money - $orderFare;
			$orderFareUpd	=	$orderFare - $orderFare;
			
			/**
			* UPDATE ORDER FARE WITH RESPECT TO OTEHR EXPRESS ITEM IF ANY
			*/
			//GET MAX ORDER ITEMS FARE FOR OTHER EXPRESS ITEM
			$orderFareUpd	=	Order::getExpressItemsFare($order_id, " AND oi.id != $orderItemId");

			// if($orderFareUpd<=0){
				// $orderFareUpd = DEFAULT_EXPRESS_SHIPPING;
			// }
			
			$$maxKey	=  $$maxKey + $orderFareUpd;
			if( $maxKey!='card'){
				$origin		=	$origin + $orderFareUpd;
			}
			
			//UPDATE AMOUNT FOR ORDER
			Order::updateOrderAmount($order_id,$orderFareUpd,$money,$origin,$credit,$card);
			
			
			//UPDATE AMOUNT FOR ORDER ITEM
			if(!empty($getOrderItemFareTotalByOrderId)){
				foreach($getOrderItemFareTotalByOrderId as $getOrderItemFareTotalByOrderRow){
					
					$itemId			=	$getOrderItemFareTotalByOrderRow->id;
					$itemFare		=	$getOrderItemFareTotalByOrderRow->fare;
					$itemTotal		=	$getOrderItemFareTotalByOrderRow->total;
					$itemPrice		=	$getOrderItemFareTotalByOrderRow->price;
					$itemQuantity	=	$getOrderItemFareTotalByOrderRow->quantity;
				
					//UPDATE INFO FOR ONLY SUBMIITED ITEM
					if($orderItemId != $itemId){
						continue;
					}
					
					
					$itemFare	=	$itemFare - $itemFare;
					$itemTotal	=	$itemQuantity * $itemPrice;
					$itemTotal	=	$itemTotal + $itemFare;

					//method_additional_cost not change when PICKUP method selected
					$method_additional_cost	=	$getOrderItemFareTotalByOrderRow->shipment_additional_cost;
					
					Order::updateOrderItemAmount($itemId,$itemFare,$itemTotal,$method_additional_cost);
					
				}
			}
			
			//ADD CREDIT AMOUNT
			if($orderFare>0){
				
				if($orderFareUpd >= $orderFare){
					$orderFare	=	$orderFareUpd - $orderFare;
				}else{
					$orderFare	=	$orderFare - $orderFareUpd;
				}
				
				User::updateUserCredit($orderFare,$user_id);
				//LOG CREDIT FLOW
				$admin_id	=	Auth::user()->id;
				User::logUserCreditFlow($user_id,$admin_id,$orderFare,'expense','refund','Fare Amount Refund due to change in delivery type',$order_id, time());
			}
		}
	}
	
	//UPDATE ORDER AMOUNT
	protected function updateOrderAmount($order_id,$orderFare,$money,$origin,$credit,$card, $updateQuantity = ""){
		
		$sql	=	<<<SQL
								UPDATE
									`order`
								SET
									fare 	= '$orderFare',
									money 	= '$money',
									origin 	= '$origin',
									credit 	= '$credit',
									card 	= '$card'
									$updateQuantity
								WHERE
									id = $order_id
SQL;
		DB::update($sql);

	}
	
	//UPDATE ORDER ITEM AMOUNT
	protected function updateOrderItemAmount($itemId,$itemFare,$itemTotal,$method_additional_cost, $orderQuantity = ""){
		
		$sql	=	<<<SQL
								UPDATE
									`order_item`
								SET
									fare 	= '$itemFare',
									total 	= '$itemTotal',
									shipment_additional_cost	=	'$method_additional_cost'
									$orderQuantity
								WHERE
									id = $itemId
SQL;
		DB::update($sql);

	}
	
	//GET ORDER ITEM FARE,TOTAL BY ORDER ID
	protected function getOrderItemFareTotalByOrderId($order_id){
		
		$sql	=	"SELECT id,fare,price,quantity,total,shipment_additional_cost FROM `order_item` WHERE order_id = $order_id";
		$orderItemFare	=	DB::select($sql);
		
		
		$orderItemFareArr	=	array();
		if(!empty($orderItemFare)){
			foreach($orderItemFare as $orderItemFareRow){
				
				$fare	=	$orderItemFareRow->fare;
				$total	=	$orderItemFareRow->total;
				$price	=	$orderItemFareRow->price;
				
				if($fare<=0 || $total <= 0 || $price <= 0){
					continue;
				}
				
				$orderItemFareArr[]	=	$orderItemFareRow;
			}
		}
		
		return $orderItemFareArr;
	}
	
	//GET ORDER ITEM FARE OF EXPRESS ITEM
	protected function getExpressItemsFare($order_id , $additionalWhereClause = ""){
		
		$sql	= <<<SQL
						SELECT
							max(t.fare) AS itemFare
						FROM
							order_item AS oi
						LEFT JOIN 
							team AS t
						ON
							t.id = oi.team_id
						WHERE
							oi.delivery = 'express'
						AND 
							oi.order_id = $order_id
						$additionalWhereClause
SQL;

		$expressItemsFare = DB::select($sql);
		
		$expressItemsFareAmount	=	0;
		if(isset($expressItemsFare[0]->itemFare)){
			$expressItemsFareAmount	=	$expressItemsFare[0]->itemFare;
		}
		
		return $expressItemsFareAmount;
	}
	
	//UPDATE ORDER AMOUNT FOR ALL ITEM TRANSFER EXPRESS FROM PICKUP
	protected function orderAmountTransferFromPickupToExpress($order_id,$getOrderItemDetail,$orderFareAmount,$currentItemFare,$customerHasToPay,$method_additional_cost){
		
		//GET ORDER & ORDER ITEM INFO
		$getOrderById					=	Order::getOrderById($order_id);
		
		if(isset($getOrderById[0]->id)){
			//GET ORDER FARE AMOUNT TO SET ON CUSTOMER CREDIT 
		
			$user_id	=	$getOrderById[0]->user_id;
			$orderFare	=	$getOrderById[0]->fare;
			$money		=	$getOrderById[0]->money;
			$origin		=	$getOrderById[0]->origin;
			$credit		=	$getOrderById[0]->credit;
			$card		=	$getOrderById[0]->card;
			
			
			//GET USER AMOUNT
			$userCredit	=	Order::getUserCreditAmount($user_id);
			
			//UPDATE ORIGIN PRICE 
			//DEDUCT PREVIOUS FARE AMOUNT
			$origin		=	$origin - $orderFare;
			//ADD NEW FARE IN ORDER TOTOL
			$origin		=	$origin + $orderFareAmount;
			
			if($method_additional_cost == 'user_balance_cash'){
				
				if($userCredit > 0 ){

					if($customerHasToPay == $userCredit){						
						$credit	+= $userCredit;
					}elseif($customerHasToPay > $userCredit){
						$credit += $userCredit;
						$money += $customerHasToPay - $userCredit;
					}else{
						$credit += $customerHasToPay;
					}
					
				}else{
					$money += $customerHasToPay;
				}
				
			}else{
				$money += $customerHasToPay;
			}
			//UPDATE AMOUNT FOR ORDER
			Order::updateOrderAmount($order_id,$orderFareAmount,$money,$origin,$credit,$card);
			
			//UPDATE ORDER ITEM AMOUNT
			$itemId			=	$getOrderItemDetail->id;
			$itemPrice		=	$getOrderItemDetail->price;
			$itemQuantity	=	$getOrderItemDetail->quantity;
		
			
			$itemFare	=	$currentItemFare;
			$itemTotal	=	$itemQuantity * $itemPrice;
			$itemTotal	=	$itemTotal + $itemFare;
			
			
			
			
			Order::updateOrderItemAmount($itemId,$itemFare,$itemTotal,$method_additional_cost);
			
			
			
			//CHECK PAYMENT METHOD FOR CHARGING
			if($method_additional_cost == 'user_balance_cash'){
			
				//UPDATE USER CREDIT AMOUNT
				if($customerHasToPay>0 && $userCredit > 0 ){
					
					//UPDATE USER CREDIT
					$updateUserCredit	=	0;
					if($customerHasToPay>=$userCredit){
						// $updateUserCredit = $customerHasToPay - $userCredit;
						$updateUserCredit = 0;
					}else{
						$updateUserCredit = $userCredit - $customerHasToPay;
					}
					
					User::simpleUpdateUserCredit($updateUserCredit,$user_id);
					//LOG CREDIT FLOW
					$admin_id	=	Auth::user()->id;
					User::logUserCreditFlow($user_id,$admin_id,$customerHasToPay,'expense','deduct','Fare Amount Deducted due to change in delivery type',$order_id, time());
				}
			}
		}
	}
	
	//CHECK USER HAS SUFFICENT BALANCE
	protected function checkCustomerHasSufficientBalance($order_id,$orderFareAmount){
		
		$isUserHasEnoughBalance	=	false;
		//GET ORDER & ORDER ITEM INFO
		$getOrderById					=	Order::getOrderById($order_id);
		
		if(isset($getOrderById[0]->id)){
			//GET ORDER FARE AMOUNT TO SET ON CUSTOMER CREDIT 
			$user_id	=	$getOrderById[0]->user_id;
			
			$userCredit	=	Order::getUserCreditAmount($user_id);
				
			if($userCredit>=$orderFareAmount){
				$isUserHasEnoughBalance	=	true;
			}
			
		}
		
		return $isUserHasEnoughBalance;
	}
	
	//GET USER CREDIT AMOUNT
	protected function getUserCreditAmount($user_id){
		
		$userInfo	=	User::getUserByUserId($user_id);
		$userCredit	=	0;
		if(isset($userInfo[0]->id)){
			
			$userCredit = $userInfo[0]->money;
		}
		
		return $userCredit;
	}
	
	//UPDATE SHIPPING ADDRESS
	protected function updateShippingAddress($dataArr){
		
		$first_name		=	$dataArr['first_name'];
		$last_name		=	$dataArr['last_name'];
		$mobile			=	$dataArr['mobile'];
		$address		=	$dataArr['address'];
		$address2		=	$dataArr['address2'];
		$zipcode		=	$dataArr['zipcode'];
		$region			=	$dataArr['region'];
		$order_item_id	=	$dataArr['order_item_id'];
		
		
		$sql	=	<<<SQL
							UPDATE
								order_item
							SET
								first_name	=	'$first_name',
								last_name	=	'$last_name',
								mobile		=	'$mobile',
								address		=	'$address',
								address2	=	'$address2',
								zipcode		=	'$zipcode',
								region		=	'$region'
							WHERE
								id = '$order_item_id'
SQL;
		DB::update($sql);
	}
	
	//GET REDIRECTION URL FOR ORDERLISTING 
	protected function getOrderListingRedirectionUrl(){
		
		//PREVIOUS URL
		$previousUrl	=	URL::previous();
		
		//CHECK PREVIOUS URL HAS PAGE PARAMETER WHICH IS USE TO KEEP SEARCH SESSION
		$pos = strpos($previousUrl, 'page');
		//SET REDIRECTION URL
		if ($pos === false) {
			$redirectionUrl	=	'admin/orderlist?page=1';
		}else{
			$redirectionUrl	=	$previousUrl;
		}
		
		return $redirectionUrl;
	}
	
	//GET BASKET ITEM QUANTITY FOR CURRENT USER
	protected function baskteGetQuantity($team_id, $option = false,$user_id){
		$qty = 0;
		
		$items	=	Order::getBasketItem($user_id);
		
		if(!empty($items)){
			foreach( $items as $key => $item )
			{
				if ( $item->team_id == $team_id && 
					(!$option || ($option && $option > 0 && $item->option_id == $option)))
				{
				   $qty += $item->quantity;
				}
			}
		}
        return $qty;
	}
	
	//GET BASKET ITEMS FOR USER
	protected function getBasketItem($user_id){
		
		$sql	=	"SELECT * FROM basket WHERE user_id = $user_id ORDER BY create_time ASC";
		$result	=	DB::select($sql);
		
		$itemsArr		=	array();
		if(!empty($result)){
			$itemsArr	=	$result;
		}
		
		return $itemsArr;
	}
	
	//CHECK USER PURCHASE PREVIOUSLY ORDER
	protected function userPrevioslyPurhchasedDeal($user_id, $team_id, $optionCondition = ""){
		
			$sql = "SELECT ifnull(SUM(order_item.quantity),0) AS now_count 
						FROM order_item LEFT JOIN `order` ON order_id = `order`.id 
						WHERE user_id = {$user_id} AND state = 'pay' AND order_item.team_id = {$team_id} $optionCondition" ;

			$count_result = DB::select($sql);
			
			$totalcount_result	=	0;
			if(!empty($count_result)&&isset($count_result[0]->now_count)){
				$totalcount_result	=	$count_result[0]->now_count;
			}
			
			return $totalcount_result;
	}
	
	//GET TOAL QUANITY FOR ORDER ITEM
	protected function getUpdatedTotalQuantity($order_id){
		
		$sql	=	<<<SQL
						SELECT
							sum(quantity) AS totalQuantity
						FROM 
							order_item
						WHERE
							order_id	=	$order_id
SQL;
		$totalOiQuantity = DB::select($sql);

		$totalQuantity	=	0;
		if(isset($totalOiQuantity[0]->totalQuantity)){
			$totalQuantity	=	$totalOiQuantity[0]->totalQuantity;
		}
		
		return $totalQuantity;
	}

	protected function getMaxFareByOrderId($orderId = 0,$orderItem=0)
	{
		#get max fare from other orderitems
	
		$orderItemCondition = '';
		if($orderItem)
		{
			$orderItemCondition = " AND oi.id !='$orderItem' ";
		}
		
		
		
		$getMaxFare = DB::select("SELECT
								   IFNULL(max(t.fare),0) AS fare
								  FROM
								   order_item AS oi
								  LEFT JOIN 
								   team AS t
								  ON
								   t.id = oi.team_id
								  WHERE
								   oi.delivery = 'express'
								  AND 
								   oi.order_id='$orderId' 
								   
								   $orderItemCondition
								   ");
		return $getMaxFare;
	}
	
	protected function ordersPaidCount()
	{
		$now = date('Y-m-d');
		$count = 0;
		
		$ordersPaidCount = DB::select("SELECT COUNT(id) AS count FROM `order` 
										WHERE `state` = 'pay' AND 
										DATE_FORMAT(FROM_UNIXTIME(create_time), '%Y-%m-%d') <'$now'");
		if(is_array($ordersPaidCount) && count($ordersPaidCount)>0)
		{
			$ordersPaidCount = $ordersPaidCount[0];
			$count = $ordersPaidCount->count;
		}
		return $count;
	}
	
	
	protected function ordersToBePaidCount()
	{
		$now = date('Y-m-d');
		$count = 0;
		
		$ordersToBePaidCount = DB::select("SELECT COUNT(id) AS count 
											FROM `order` 
											WHERE `state` = 'unpay' AND 
											DATE_FORMAT(FROM_UNIXTIME(create_time), '%Y-%m-%d') <'$now'");
		if(is_array($ordersToBePaidCount) && count($ordersToBePaidCount)>0)
		{
			$ordersToBePaidCount = $ordersToBePaidCount[0];
			$count = $ordersToBePaidCount->count;
		}
		return $count;
	}
	
}