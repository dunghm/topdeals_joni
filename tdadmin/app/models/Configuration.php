<?php

class Configuration extends \Eloquent {

	protected function updateStatusById($id=0)
	{
		$sql = "UPDATE email_config SET value = IF (value = '".'1'."', '".'0'."', '".'1'."') 
				WHERE id = $id";
			
		  #DB::statement( DB::raw( $sql ));	#TODO: Use statement for Live
		  $result = DB::update( $sql );		#TODO: For local select
		  return $result;
	}
	
	
	#protected function getEmailConfiguration($order_by='',$sSortDir_0='')
	protected function getEmailConfiguration($get)
	{
		$order_by	= 'value';
		$sSortDir_0 = 'asc';
		$data = array();
		$column_names = array('e.event_key', 'e.label', 'e.value');
		
		$iSortCol_0			= $get['iSortCol_0'];
        $sSortDir_0			= $get['sSortDir_0'];
        
        $sSearch_0		= $get['sSearch_0'];	#	Event Key
        $sSearch_1		= $get['sSearch_1'];	#	Label
        $sSearch_2		= $get['sSearch_2'];	#	Status -> BD field name: Value

		
		$email_config = DB::table('email_config as e');
					
		if( ($iSortCol_0 != '') && (isset($column_names[$iSortCol_0])) )
        {
        	$order_by = $column_names[$iSortCol_0];
        }
        
		if ($sSearch_0 != ''){
			$email_config->where('e.event_key', 'like', $sSearch_0.'%');
		}
		
		if ($sSearch_1 != ''){
			$email_config->where('e.label', 'like', $sSearch_1.'%');
		}
		
		if ($sSearch_2 != '')
		{
			
			$email_config->where('e.value', '=', $sSearch_2);
		}
		
		$email_config->select('e.id', 'e.event_key', 'e.label', 
						DB::raw('IF(e.value = 1, \'Enabled\', \'Disabled\') AS value'))
					 ->orderBy($order_by, $sSortDir_0); 
		return Datatable::query($email_config)
				->showColumns(array('event_key', 'label'))
				->addColumn('edit', function($model) {
						return '<a txt="'.$model->value.'" class="enable_disable_emails" id="'.$model->id.'" style="text-decoration: none;"><span  data-original-title="Enable/Disable" id="span_'.$model->id.'">'.$model->value.'</span></a>';

		})		
		->orderColumns(array('e.event_key', 'e.label'))
		->make();		
		
		/*$emailConfigurationList = $email_config->select('e.id', 'e.event_key', 'e.label', 
						DB::raw('IF(e.value = 1, \'Enabled\', \'Disabled\') AS value'))
					 ->orderBy($order_by, $sSortDir_0);
		return 	$emailConfigurationList;*/
	}
	
	
	protected function getEmailConfigurationByEvent($eventKey='')
	{
		$emailConfiguration = DB::select("select * from email_config  where event_key = '$eventKey'");
		return $emailConfiguration;
	}
	
}