<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {
	
	public $timestamps = false;

	protected $primaryKey = 'id';
	
	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	/*
	 * @name:	reg_users
	 */
	protected function reg_users($start, $end){
		$sql = "SELECT COUNT(*) as count 
								FROM $this->table 
								WHERE enable = 'Y' AND
								DATE_FORMAT(FROM_UNIXTIME(`create_time`), '%Y-%m-%d') BETWEEN '".$start."' AND '".$end."'"; 
		#var_debug($sql);
		$reg_users = DB::select($sql)[0]->count;
		return $reg_users;
	}
	
	protected function regUsers(){
		$users = DB::select("select count(*) as count from $this->table where enable = 'Y'")[0]->count;
		return $users;
	}
	
	public function get_user($user_id) {
		$acl = DB::select("SELECT u.id as user_id, u.email, CONCAT (u.firstname,' ',u.lastname) as user_name
						FROM users u
						WHERE u.id = $user_id");
		return $acl;
	}

	protected function updateUserCredit($money=0,$userId=0)
	{
	    $updated = DB::update("update user set money=money+$money  where id = ?", array($userId));
		return $updated;
    } 	
	
	
	protected function getUserByUserId($userId = 0)
	{
		$getUserByUserId = DB::select("select * from `user` WHERE `id` = '$userId'");
		return $getUserByUserId;
	}
	
	//LOG USER AMOUNT FLOW
	protected function logUserCreditFlow($userId,$admin_id,$cp_credit,$direction,$action,$detail, $detail_id,$create_time){
		//ADD FLOW DATA
				
				$sqlInsert	=	<<<SQL
									
										INSERT
											INTO
										flow
											(
												user_id,
												admin_id,
												money,
												direction,
												action,
												detail,
												detail_id,
												create_time
											)
											VALUES
											(
												'$userId',
												'$admin_id',
												'$cp_credit',
												'$direction',
												'$action',
												'$detail',
												'$detail_id',
												'$create_time'
											)
SQL;
				
			return DB::insert($sqlInsert);
	}
	
	//UPDATE USER CREDIT
	protected function simpleUpdateUserCredit($money=0,$userId=0)
	{
	    $updated = DB::update("update user set money=$money  where id = ?", array($userId));
		return $updated;
    } 

	//get register user count w.r.t year 
	protected function registeredUsersMonthly($year=0)
	{
	    $sql = "SELECT
					MONTH(FROM_UNIXTIME(create_time)) AS month_num,YEAR(FROM_UNIXTIME(create_time)) AS YEAR,
					IFNULL(COUNT(*),0) AS total 
				FROM 
					user 
				WHERE 
					YEAR(FROM_UNIXTIME(create_time)) = '$year'
				GROUP BY
					MONTH(FROM_UNIXTIME(create_time))";
		
		// echo "<pre>";print_r($sql);exit();
		$registeredUsersMonthly = DB::select($sql);
		
		return $registeredUsersMonthly;
    } 
	
	//get min user register year
	protected function mimmumUserRegisterYear()
	{
	    $sql = "SELECT
					MIN(FROM_UNIXTIME(create_time, '%Y')) AS startYear
				FROM
					user";
		
		// echo "<pre>";print_r($sql);exit();
		$getStartRegisterYear = DB::select($sql);
		
		return $getStartRegisterYear;
    } 	
}