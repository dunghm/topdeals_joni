<?php
class Team extends \Eloquent 
{
	protected $table = 'team';
	
	protected function activeTeams()
	{
		$now = date('Y-m-d');
		$count = 0;

		$activeTeams = DB::select("select count(*) as count from team 
									where system ='Y' AND 
									DATE_FORMAT(FROM_UNIXTIME(end_time), '%Y-%m-%d') >'$now' AND 
									DATE_FORMAT(FROM_UNIXTIME(begin_time), '%Y-%m-%d') <='$now'");
		if(is_array($activeTeams) && count($activeTeams)>0)
		{
			$activeTeams = $activeTeams[0];
			$count = $activeTeams->count;
		}
		return $count;
	}
	
	protected function expiredTeams()
	{
		$now = date('Y-m-d');
		$count = 0;
		
		$expiredTeams = DB::select("select count(*) as count from team where DATE_FORMAT(FROM_UNIXTIME(end_time), '%Y-%m-%d') < '$now'");
		if(is_array($expiredTeams) && count($expiredTeams)>0)
		{
			$expiredTeams = $expiredTeams[0];
			$count = $expiredTeams->count;
		}
		return $count;
	}
	
	protected function getTeamByCategoryId($id=0)
	{
		$now = time();
		$teamList = DB::select("select id , title from team where system ='Y' and end_time >'$now' and begin_time <='$now' and group_id ='$id' and delivery in ('express','express_pickup','pickup') ORDER BY id DESC");
		return $teamList;
	}
	
	protected function getMultiTeamByTeamId($teamId=0)
	{
		$multiTeamList = DB::select("select id , title from team_multi where team_id ='$teamId'");
		return $multiTeamList;
	}
	
	protected function getTeamById($teamId=0)
	{
		$getTeamById = DB::select("select * from team where id ='$teamId'");
		return $getTeamById;
	}
	//GET CATEGORY BY TEAM GROUP ID
	protected function getCategoryById($id=0)
	{
		$getCategoryById = DB::select("select * from category where id ='$id'");
		return $getCategoryById;
	}
	
	//GET CATEGORY Name BY TEAM GROUP ID
	protected function getCategoryNameById($id=0)
	{
		$getCategoryNameById = DB::select("select name from category where id ='$id'");
		
		$categoryName	=	"";
		if(!empty($getCategoryNameById)&&isset($getCategoryNameById[0])){
			$categoryName	=	$getCategoryNameById[0]->name;
		}
		return $categoryName;
	}
	
	//GET CATEGORY Name BY TEAM GROUP ID
	protected function getCategoryIdByName($name)
	{
		$categoryId	=	array();
		if($name!=""){
			
			$getCategoryId = DB::select("select id from category where name LIKE '%%%$name%%%'");
			
			$categoryName	=	"";
			if(!empty($getCategoryId)){
				foreach($getCategoryId as $getCategoryIdRow){
					$categoryId[]	=	$getCategoryIdRow->id;
				}
			}
		}
		return $categoryId;
	}
	
	protected function getMultiTeamInfoById($teamId=0)
	{
		$multiTeamList = DB::select("select * from team_multi where id ='$teamId'");
		return $multiTeamList;
	}
	
	//GET TEAM OPTION INFOR BY PARENT ID
	protected function getMultiTeamInfoByTeamId($teamId=0)
	{
		$multiTeamList = DB::select("select id , title, per_number,max_number from team_multi where team_id ='$teamId'");
		return $multiTeamList;
	}
	
	/**
	* GET STOCK LEFT
	*/
	
	protected  function GetStockLeft($team, $option)
    {
        $stock_left = -1;
        if($option){
            
            $upper_limit = ( $option['max_number'] > 0 ) ? $option['max_number'] : $team['max_number'];
        }
        else{
            $upper_limit = $team['max_number'];
        }
        
        if ( $upper_limit > 0 )
        {
            $option_id = ($option && $option['max_number'] > 0) ? $option['id'] : false; 
            $in_basket = Team::GetDealCountInBasket($team['id'], $option_id);
            $in_payment = Team::GetDealCountInPaymentProcessing($team['id'], $option_id);
            
            
            if($option){
                
                $stock_left = $upper_limit - ($option['now_number'] + $in_basket + $in_payment);
               // echo 'stock left ='.$stock_left.'<br>';
            }
            else{
                $stock_left = $team['max_number'] - ($team['now_number'] + $in_basket + $in_payment);
            }
            
        }
        //echo $stock_left;
        
        return $stock_left;
    }
	
	protected  function GetDealCountInBasket($team_id, $option_id = false)
    {
        $cut_off = time();
        $sql = "select sum(quantity) AS total from basket where team_id = {$team_id} AND expire_time > {$cut_off}";
        
        if ($option_id){
            $sql .= " AND option_id = {$option_id}";
		}
		
        $result = DB::select($sql);
		
		$totalBasketCount	=	0;
		if(!empty($result)&&isset($result[0])){
			if($result[0]->total != NULL){
				$totalBasketCount	=	$result[0]->total;
			}
		}
       
        return $totalBasketCount;
    }
	
	 
	protected  function GetDealCountInPaymentProcessing($team_id, $option_id = false)
	{
		$cut_off = time() - 600;
		$sql = "select sum(order_item.quantity) total from 
			order_item left join `order` on order_item.order_id = `order`.id 
			where state != 'pay' AND `state` != 'temporary'
			AND order_item.team_id = {$team_id} AND create_time > {$cut_off}";

		if ($option_id){
			$sql .= " AND order_item.option_id = {$option_id}";
		}
		
		$result = DB::select($sql);

		$totalDealCountInPaymentProcessing	=	0;
		if(!empty($result)&&isset($result[0])){
			if($result[0]->total != NULL){
				$totalDealCountInPaymentProcessing	=	$result[0]->total;
			}
		}
		return $totalDealCountInPaymentProcessing;
	}
	
	//UPDATE STOCK QUANTITY
	protected function updateTeamStock($team_id,$addQuantityStock, $tableName =	"team"){
			
			$sql	=	<<<SQL
								SELECT
									now_number,
									stock_count
								FROM
									$tableName
								WHERE
									id = $team_id
SQL;
			$result	=	DB::select($sql);

			$now_number		=	0;
			$stock_count	=	0;
			
			if(isset($result[0]->now_number)){
				$now_number		= $result[0]->now_number;	
				$stock_count	= $result[0]->stock_count;	
			}
			//UPDATE NOW NUMBER
			if($addQuantityStock >= $now_number){
				$now_number	=	$addQuantityStock - $now_number;
			}else{
				$now_number	=	$now_number - $addQuantityStock;
			}

			$stock_count	=	$stock_count + $addQuantityStock;
			
			$sql	=	<<<SQL
								UPDATE
									$tableName
								SET
									now_number 	= $now_number,
									stock_count	=	$stock_count
								WHERE
									id = $team_id
SQL;

			DB::update($sql);
	}
	
	
	//SUBTRACT STOCK QUANTITY
	protected function subtractTeamStock($team_id,$addQuantityStock, $tableName =	"team"){
			
			$sql	=	<<<SQL
								SELECT
									now_number,
									stock_count
								FROM
									$tableName
								WHERE
									id = $team_id
SQL;
			$result	=	DB::select($sql);

			$now_number		=	0;
			$stock_count	=	0;
			
			if(isset($result[0]->now_number)){
				$now_number		= $result[0]->now_number;	
				$stock_count	= $result[0]->stock_count;	
			}
			
			//UPDATE NOW NUMBER
			if($addQuantityStock >= $stock_count){
				$stock_count	=	$addQuantityStock - $stock_count;
			}else{
				$stock_count	=	$stock_count - $addQuantityStock;
			}
			
			//UPDATE NOW NUMBER
			$now_number		=	$now_number + $addQuantityStock;
			
			$sql	=	<<<SQL
								UPDATE
									$tableName
								SET
									now_number 	= $now_number,
									stock_count	=	$stock_count
								WHERE
									id = $team_id
SQL;

			DB::update($sql);
	}
	
	protected function getMultiTeamById($Id=0)
	{
		$multiTeamList = DB::select("select id , title,now_number from team_multi where id ='$Id'");
		return $multiTeamList;
	}
	
	/**
	* CALCULATE TEAM CASHBACK AMOUNT
	*/
	
	protected function teamCashbackAmount($team_id, $option_id, $quantity){
		
		
		//if option id is not empty then check team_multi table else on team table

		if(!empty($team_id) && $team_id != NULL && $team_id !=""){
			
			$sqlOption	=	"SELECT is_cash_back_allowed,cb_amount,cb_start_time,cb_end_time FROM team WHERE id = $team_id";
		}
		
		if(!empty($option_id) && $option_id != NULL && $option_id !=""){
			
			//CHECK OPTION HASE cb_amount OR NOT
			// $sqlOption	=	"SELECT is_cash_back_allowed,cb_amount,cb_start_time,cb_end_time FROM team_multi WHERE id = $option_id";
			$sqlOptionQry	=	"SELECT is_cash_back_allowed,cb_amount,cb_start_time,cb_end_time FROM team_multi WHERE id = $option_id";
			$sqlOptionData 		= DB::select($sqlOptionQry);
			
			$is_cash_back_allowed	=	false;
			$cb_amount				=	0;

			if(!empty($sqlOptionData)&&isset($sqlOptionData[0]->is_cash_back_allowed)){
				 $is_cash_back_allowed 	= $sqlOptionData[0]->is_cash_back_allowed;
				 $cb_amount 			= $sqlOptionData[0]->cb_amount;
			}
			//IF CHILD HAS CASH BACK AMOUNT THEN CHILD CB VALUE WILL BE USED 
			if($is_cash_back_allowed && $cb_amount>=0){
				 $sqlOption	=	"SELECT is_cash_back_allowed,cb_amount,cb_start_time,cb_end_time FROM team_multi WHERE id = $option_id";
			}
			
		}
		
		
		//GET DATA BY TEAM ID OR OPTION ID
		 $sqlTeamData = DB::select($sqlOption);
		 
		 //SET CASHBACK AMOUNT FOR EACH ITEM
		 $itemCashbackAmount	=	0;
		
		if(!empty($sqlTeamData)&&isset($sqlTeamData[0]->is_cash_back_allowed)){
			 
			 
			 
			 foreach($sqlTeamData as $sqlTeamDataRow){
				 
				 $is_cash_back_allowed 	= $sqlTeamDataRow->is_cash_back_allowed;
				 $cb_amount 			= $sqlTeamDataRow->cb_amount;
				 $cb_start_time 		= $sqlTeamDataRow->cb_start_time;
				 $cb_end_time 			= $sqlTeamDataRow->cb_end_time;
				 

				 
				 //CHECK Cash BAck allowed
				 if($is_cash_back_allowed == 1 && $cb_amount > 0 && !empty($cb_start_time)){
					 
					 $cb_start_timeFormated	=	date("Y-m-d",$cb_start_time);
					 $currentDate			=	date("Y-m-d");
					 
					 
					 $cb_end_timeFormated	=	"";
					 //FORMAT END DATE
					 if(!empty($cb_end_time)){
						  $cb_end_timeFormated	=	date("Y-m-d",$cb_end_time);
					 }
					
					 
					 //DATE EXPIRY VALIDATION
					 //CHECK IF END DATE ARE NOT SET THEN CHECK ONLY START DATE ELSE CHECK FOR START & END DATE RANGE
					 if($cb_end_timeFormated==""){
						 
						 if($currentDate >= $cb_start_timeFormated){
							 $itemCashbackAmount		=	$cb_amount * $quantity;
						 }
					 }else{
						 
						 //EXPIRY
						 if($currentDate >= $cb_start_timeFormated && $currentDate <= $cb_end_timeFormated){
							 $itemCashbackAmount		=	$cb_amount * $quantity;
						 }
					 }
					 
				 }
				 
			 }
		 }
		 
		 return $itemCashbackAmount;
				 
	}
}