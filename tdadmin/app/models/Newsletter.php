<?php
class Newsletter extends \Eloquent 
{
	#protected function warehouse($order_by='',$sSortDir_0='')
	protected function getSections($get)
	{
		$order_by	= 'id';
		$sSortDir_0 = 'desc';
		$data = array();
		
		$column_names 	= array('nl.id', 'nl.newsletter_name', 'nl.team_category','nl.update_time','nl.create_time');
		
		$iSortCol_0			= $get['iSortCol_0'];
        $sSortDir_0			= $get['sSortDir_0'];
        
        $sSearch_0		= $get['sSearch_0'];	#	Id
        $sSearch_1		= $get['sSearch_1'];	#	Section->BD field name: name
        $sSearch_2		= $get['sSearch_2'];	#	Description
		#$sSearch_3		= $get['sSearch_3'];	#	operation->BD field name: status
		

		
		$warehouse_section = DB::table('newsletters as nl');
					
		if( ($iSortCol_0 != '') && (isset($column_names[$iSortCol_0])) )
        {
        	$order_by = $column_names[$iSortCol_0];
        }
        
		if ($sSearch_0 != ''){
			$warehouse_section->where('nl.id', '=', $sSearch_0);
		}
		
		if ($sSearch_1 != ''){
			$warehouse_section->where('nl.newsletter_name', 'like', $sSearch_1.'%');
		}
		
		if ($sSearch_2 != '')
		{
			$categporyIDSer	=	Team::getCategoryIdByName($sSearch_2);
			if(!empty($categporyIDSer)){
				// $categporyIDSer	=	implode(",",$categporyIDSer);
				// echo $categporyIDSer;exit();
				$warehouse_section->whereIn('nl.team_category',  $categporyIDSer);
			}
		}
				
		$warehouse_section->select('nl.id', 'nl.newsletter_name', 'nl.team_category','nl.update_time','nl.create_time')
					 ->orderBy($order_by, $sSortDir_0); 
		return Datatable::query($warehouse_section)
				->showColumns(array('id', 'newsletter_name','team_category','update_time'))
				->addColumn('team_category', function($model) {
						return Team::getCategoryNameById($model->team_category);
					})
				->addColumn('update_time', function($model) {
					
						$updateTime	=	"-";
						if($model->update_time>0){
							$updateTime = date("Y-m-d",$model->update_time);
						}elseif($model->create_time>0){
							$updateTime = date("Y-m-d",$model->create_time);
						}
						
						return $updateTime;
					})
				->addColumn('operation', function($model) {

						$currentTime	=	time();

						return '<a title="Edit"  href="'.URL::to('admin/newsletter_edit').'/'.$model->id.'"><i class="fa fa-fw fa-edit"></i></a>&nbsp;<a title="list" newsletter_id="'.$model->id.'" href="'.URL::to('admin/newsletter_preview').'?id='.$model->id.'&t='.$currentTime.'" id="preview-newsletter-list"><i class="glyphicon glyphicon-search"  ></i></a>
						&nbsp;<a title="Delete" href="'.URL::to('admin/newsletter_delete').'/'.$model->id.'"  onclick="Javascript: return deleteNewsletterRecord();" ><i class="fa fa fa-trash-o"></i></a>&nbsp;<a title="Export" href="'.URL::to('admin/newsletter_export').'/'.$model->id.'" ><i class="fa fa-fw fa-download"></i></a>';

		})		
		->orderColumns(array('nl.id', 'nl.newsletter_name', 'nl.team_category'))
		->make();
	}
	
	//GET ALL CATEGORY
	protected function getAllCategory(){
		$getAllCategory = DB::select("select `id`, `name` from category WHERE `zone` = 'group'");
		return $getAllCategory;
	}

	//GET ALL CATEGORY
	protected function getAllDealByCategory($id){
		
		$now = time();
		
		$getAllCategory = DB::select("SELECT id AS id,title AS name FROM team WHERE `system` = 'Y' AND (end_time > $now)AND (begin_time <= $now) AND `group_id` = '$id' ORDER BY id DESC");
		
		return $getAllCategory;
	}
	
	//SAVE NEWSLETTER
	protected function insertNewsletter($name, $team_category,$newsletter_type){
		
		
		DB::insert('insert into newsletters (newsletter_name, team_category,newsletter_type,create_time) values (?, ?,?, ?)', array($name, $team_category, $newsletter_type, time()));
        return $this->newsletterLastInsertedId();
    }
	
	//SAVE DEAL FOR NEWSLETTER
    protected  function add_deals($deals, $id){
        $i = 0;
        foreach($deals as $d){

			DB::insert('insert into newsletter_deals (newsletter_id, deal_id,deal_order) values (?, ?, ?)', array($id, $d, $i));
            $i++;
        }
    }
	
	//GET NEWSLETTER LAST INSERTED ID
	protected function newsletterLastInsertedId(){
		
		
		$newsletterLastInsertedId = DB::select("select max(id) as maxID from newsletters");
		
		$lastInsretedId	=	0;
		if(!empty($newsletterLastInsertedId)){
			if(isset($newsletterLastInsertedId[0]->maxID)){
				$lastInsretedId = $newsletterLastInsertedId[0]->maxID;
			}
		}

		return $lastInsretedId;
	}
	
	#delete record
	protected function deleteRecord($id=0)
	{
		DB::delete('delete from newsletters where id ='.$id);
		DB::delete('delete from newsletter_deals where newsletter_id ='.$id);
		return true;
	}
	
	#getNewsletter Info By Id
	protected function getNewsletterInfo($id){
		
        $newsletter 	= $this->getAndCheckNewsLetter($id);
        $query_deals 	= $this->getNewsLetterDealById($id);
		
        $rows = array();
        $teams = array();
        if(isset($newsletter['id']))
        {
			if(!empty($query_deals)){
				foreach ($query_deals as $row)
				{
					$query_team = Team::getTeamById($row->deal_id);
				
					if(isset($query_team[0])){
						$teams[$row->deal_id] = $query_team[0];
					}
					//convert object to array
					$row = json_decode(json_encode($row), true);
					$rows[] = $row;
				}
			}
            
        }
        return array('newsletter' => $newsletter, 'deals' => $rows,'teams'=> $teams);
    }
	
	//GET AND CHECK NEWSLETTER BY ID
	protected function getAndCheckNewsLetter($id){
		
		$getNewsLetterInfo = DB::select("SELECT * FROM newsletters WHERE id = $id");
		$newsletter	=	array();
		if(!empty($getNewsLetterInfo)&&isset($getNewsLetterInfo[0])){
			$newsletter	=	$getNewsLetterInfo[0];
			
			//convert object to array
			$newsletter = json_decode(json_encode($newsletter), true);
		}
		return $newsletter;
	}
	
	//GET NEWSLETTER DEAL BY ID
	function getNewsLetterDealById($id){
		
		$getNewsLetterDealInfo = DB::select("SELECT * FROM newsletter_deals WHERE newsletter_id = $id ORDER BY deal_order ASC");
		
		return $getNewsLetterDealInfo;
	}
	
	
	//UPDATE NEWSLETTER
	protected function updateNewsletter($title, $deal_category, $id, $newsletter_type){
		
		$currentTime	=	time();
		
		DB::update("update newsletters set newsletter_name = ?, team_category=?,newsletter_type=?, update_time=? where id = ?", array($title,$deal_category,$newsletter_type,$currentTime,$id));
	}
	
	//UPDATE NEWSLETTER
	protected function updateNewsletterDeal($deals,$id){
		
		DB::delete('delete from newsletter_deals where newsletter_id ='.$id);
		$this->add_deals($deals, $id);
	}
}