<?php
class WarehouseSection extends \Eloquent 
{
	#protected function warehouse($order_by='',$sSortDir_0='')
	protected function getSections($get)
	{
		
		$order_by	= 'id';
		$sSortDir_0 = 'asc';
		$data = array();
		
		$column_names = array('ws.id', 'ws.name', 'ws.description','total','ws.status');
		
		$iSortCol_0			= $get['iSortCol_0'];
        $sSortDir_0			= $get['sSortDir_0'];
        
        $sSearch_0		= $get['sSearch_0'];	#	Id
        $sSearch_1		= $get['sSearch_1'];	#	Section->BD field name: name
        $sSearch_2		= $get['sSearch_2'];	#	Description
		#$sSearch_3		= $get['sSearch_3'];	#	total
		#$sSearch_4		= $get['sSearch_4'];	#	operation->BD field name: status
		
		###########Search filter #####################
		$searchByDeal = '';
		if(!empty($get['searchByDeal']))
		{
			$searchByDeal  		=  $get['searchByDeal'];
		}
		
		$searchByOption = '';
		if(!empty($get['searchByOption']))
		{
			$searchByOption  		=  $get['searchByOption'];
		}
		
		
		$warehouse_section = DB::table('warehouse_section as ws');
					
		if( ($iSortCol_0 != '') && (isset($column_names[$iSortCol_0])) )
        {
        	$order_by = $column_names[$iSortCol_0];
        }
        
		if ($sSearch_0 != ''){
			$warehouse_section->where('ws.id', '=', $sSearch_0);
		}
		
		if ($sSearch_1 != ''){
			$warehouse_section->where('ws.name', 'like', $sSearch_1.'%');
		}
		
		if ($sSearch_2 != '')
		{
			$warehouse_section->where('ws.description', 'like', $sSearch_2.'%');
		}
		
		if($searchByDeal!='')
		{
			$warehouse_section->where('wsi.team_id', '=', $searchByDeal);
			//->orWhere('wsi.option_id', '=', $searchByDeal)
		}
		
		if($searchByOption!='')
		{
			$warehouse_section->where('wsi.option_id', '=', $searchByOption);
		}
		
		$productSumQuery = "(select IFNULL(sum(t.stock_count),0) from warehouse_section_items wsi,team t where wsi.team_id = t.id AND wsi.option_id=0 AND wsi.warehouse_section_id = ws.id) 
		+
		(select IFNULL(sum(tm.stock_count),0)  from  warehouse_section_items wsi,team_multi tm where wsi.option_id = tm.id AND wsi.warehouse_section_id = ws.id)
		as total";
		
		$warehouse_section->select('ws.id', 'ws.name', 'ws.description',
		DB::raw($productSumQuery),'ws.status')
		->join('warehouse_section_items as wsi', 'wsi.warehouse_section_id', '=', 'ws.id')
					 ->orderBy($order_by, $sSortDir_0); 
		//dd($warehouse_section->toSql());			 
		return Datatable::query($warehouse_section)
				->showColumns(array('id', 'name','description','total'))
				->addColumn('operation', function($model) {
						return '<a title="Edit" href="'.URL::to('admin/warehousesectionassignstock').'/'.$model->id.'"><i class="glyphicon glyphicon-zoom-out"></i></a>
						<a title="Delete" href="#"  onclick="Javascript: return deleteRecord('.$model->id.');" ><i class="fa fa-fw fa-trash"></i></a>';

		})		
		->orderColumns(array('ws.id', 'ws.name', 'ws.description','total'))
		->make();
	}
	
	
	#save record
	protected function saveRecord($tableName='', $fields='', $param=array())
	{
		$Inserted = DB::insert("insert into $tableName ($fields) values (?, ?,?)",$param);
		
		return $Inserted;
	}
	
	#update record
	protected function updateRecord($id=0,$fieldsWithValue='')
	{
		$updated = DB::update("update warehouse_section set $fieldsWithValue  where id = ?", array($id));
		return $updated;
	}

	#delete record
	protected function deleteRecord($id=0)
	{
		$warehouseList = DB::delete('delete from warehouse_section where id ='.$id);
		return $warehouseList;
	}
	
	
	
	#ware house section id
	protected function getwarehouseSectionById($id=0)
	{
		$warehouseList = DB::select('select * from warehouse_section where id ='.$id);
		return $warehouseList;
	}
	
	#get all warehouse section
	protected function getAllWarehouseSection($sectionId=0)
	{
		$sectionIdCondition = '';
		if($sectionId)
		{
					$sectionIdCondition = " where id !='$sectionId' ";
		}
		
		$warehouseList = DB::select('select id,name from warehouse_section'.$sectionIdCondition);
		return $warehouseList;
	}
	
	#get teamsections
	protected function getTeamSection($sectionId=0)
	{
		$teamSectionList = DB::select('SELECT si.id,si.team_id, si.option_id, t.title AS team_title, tm.title AS option_title,
										(CASE WHEN (tm.id IS NULL) 
											THEN
												IFNULL(t.stock_count,0)
											ELSE
												IFNULL(tm.stock_count,0)
											END) AS stock
										FROM warehouse_section_items si INNER JOIN 
										team t ON t.id=si.team_id
										LEFT JOIN team_multi tm ON tm.id = si.option_id WHERE si.warehouse_section_id = '.$sectionId
									  );
		return $teamSectionList;
	}	
	
	#checkTeamSection
	
	protected function checkTeamSection($sectionId=0, $teamId=0, $optionId=0)
	{
		
		$checkTeamSection = DB::select('select count(*) as Totalcount from warehouse_section_items where warehouse_section_id ='.$sectionId.' AND team_id= '.$teamId.' AND option_id = '.$optionId);
		
		return $checkTeamSection;
	}
	
	
	#delete record
	protected function deleteItem($id=0)
	{
		$warehouseList = DB::delete('delete from warehouse_section_items where id ='.$id);
		return $warehouseList;
	}
	
	#update record
	protected function updatesection($warehouseSectionId=0,$warehouseSectionItemIds=0)
	{
		$updated = false;
		if(!empty($warehouseSectionId) && !empty($warehouseSectionItemIds))
		{
			$updateQuery ="update warehouse_section_items set warehouse_section_id ='$warehouseSectionId'  where id IN ($warehouseSectionItemIds)";
			
			$updated =DB::update(DB::raw($updateQuery));
		}
		return $updated;
		
		
	}
	
	
	#function 
	protected function getAssignStockInfo($arrQueryParams=array())
	{
		####################################################Paggination#################################################################
		$setFirstResult = 0;
		$setMaxResults  = PAGGINATION_LENGTH;
		$pagginationLimit = '';
		$sortCondition = 'c.name';
		 if(isset($arrQueryParams['setFirstResult']))
		{
			$setFirstResult = $arrQueryParams['setFirstResult'];
		}

		if(isset($arrQueryParams['setMaxResults']))
		{
			$setMaxResults = $arrQueryParams['setMaxResults'];
		} 
		if( (isset($arrQueryParams['setFirstResult']))  && isset($arrQueryParams['setMaxResults']) )
		{
			$pagginationLimit.=" limit $setFirstResult , $setMaxResults  ";
		}	
		############################################ordering#######################################################
		############################################ordering#######################################################
			
		if(!empty($arrQueryParams['sOrder']))
		{
			 $sortCondition = $arrQueryParams['sOrder'];
		}
		############################################//ordering#######################################################
		
		############################################Filters#######################################################
		$filterBySearch=$filterByCategory=$filterStock='';
		$searchCondition=$categoryCondition=$stockCondition ='';
		
		if(isset($arrQueryParams['filterStock']))
		{
			$filterStock = $arrQueryParams['filterStock'];
		}
		
		if(isset($arrQueryParams['filterBySearch']))
		{
			$filterBySearch = $arrQueryParams['filterBySearch'];
		}

		if(isset($arrQueryParams['filterByCategory']))
		{
			$filterByCategory = $arrQueryParams['filterByCategory'];
		} 
		
		#Condition
		if($filterBySearch)
		{
			$filterBySearch = wildcarded($filterBySearch);
			$searchCondition =" AND (t.id= '$filterBySearch' OR MATCH(t.title) AGAINST('{$filterBySearch}' in boolean mode)) ";
		}
		
		if($filterByCategory>0)
		{
			$categoryCondition = " AND c.id= '$filterByCategory' ";
		}
		
		if($filterStock=='all')
		{
			$stockCondition = '';
		}
		elseif($filterStock=='out')
		{
			$stockCondition = " AND
					case 
						when tm.id is NULL 
					THEN
						t.stock_count=0
					ELSE
						tm.stock_count=0
					end ";
		}
		else
		{
			$stockCondition = " AND
					case 
						when tm.id is NULL 
					THEN
						t.stock_count>0
					ELSE
						tm.stock_count>0
					end ";
		}
		
		
		############################################//Filters#######################################################
		#Group by wsi.option_id ,Group by wsi.team_id
		$sectionField = ",
						(case 
							when (tm.id is NULL) 
							THEN
								(SELECT 
									group_concat(ws.name)
								FROM
									warehouse_section_items wsi,warehouse_section ws
								where 
									ws.id = wsi.warehouse_section_id
								AND	
									wsi.team_id = t.id
								)
							ELSE
								(SELECT 
									group_concat(ws.name)
								FROM
									warehouse_section_items wsi,warehouse_section ws
								where 
									ws.id = wsi.warehouse_section_id
								AND				
									wsi.option_id = tm.id
								)
							end	
						) AS section";
		#fields
			$fields="c.name as category,
					 t.id as dealId,t.title as dealTitle,
					 tm.id as optionId, tm.title as optionTitle,
					 (case when (tm.id is NULL) 
					THEN
						IFNULL(t.stock_count,0)
					ELSE
						IFNULL(tm.stock_count,0)
					END) AS stock
					$sectionField
					";
			
			$isQueryForCount = false;
			if(!empty($arrQueryParams['queryForCount']))
			{
				$isQueryForCount = true;
				$fields = " COUNT(t.id) as totalCount ";
				
			}	
		
		$now = time();
/* 
AND
	system ='Y'
AND 
	end_time >'$now' 
AND 
	begin_time <='$now' 
*/
		
		$sql = "SELECT 
					 $fields	
				FROM  
					team t
				LEFT JOIN
					team_multi tm
				ON
					t.id = tm.team_id
				LEFT JOIN
					category c
				ON
				   c.id = t.group_id
				WHERE 
					zone ='group'
				AND
					delivery in ('express','express_pickup','pickup')	
				$searchCondition
				$categoryCondition
				$stockCondition
		";
		//echo $sql;exit;
		
		if(!$isQueryForCount)
		{			
			$sql.= " ORDER BY $sortCondition ";
			$sql.= $pagginationLimit;
		
		}
		
		
		$sqlDataResult = DB::select($sql);
		
		return $sqlDataResult;
	}
	
	
	protected function getAssignStockCount($arrQueryParams)
	{
		$records = 0;
		$arrQueryParams['queryForCount'] = true;
		
		$totalCountArray = $this->getAssignStockInfo($arrQueryParams);
		if(is_array($totalCountArray) && count($totalCountArray)>0)
		{
			$totalCountArray = $totalCountArray[0];
			$records = $totalCountArray->totalCount;
		}
		return $records;
	} 
	
	
	#get team and option from warehouse_section_items by id
	protected function getWarehouseSectionItemById($id=0)
	{
		$wrehouseSectionItemList = DB::select('select team_id,option_id from warehouse_section_items where id ='.$id);
		return $wrehouseSectionItemList;
	}

}