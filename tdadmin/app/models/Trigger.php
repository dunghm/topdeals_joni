<?php
class Trigger extends \Eloquent 
{
	#protected function warehouse($order_by='',$sSortDir_0='')
	protected function getSections($get)
	{
			$order_by = 'te.id';
			$sSortDir_0 = 'desc';
					//RECORD BY STATE
			$daytime = strtotime(date('Y-m-d'));
			$ser_event_id			=	Input::get('sSearch_0');
			$ser_event_name			=	Input::get('sSearch_1');
			$ser_action_name		=	Input::get('sSearch_2');
			$ser_action_type		=	Input::get('sSearch_3');

			
			//CREATE QUERY FOR GET DATA
			$data = DB::table('trigger_event as te')
						->leftJoin('trigger_action as ta', 'ta.id', '=', 'te.action_id')
						->leftJoin('trigger_action_type as tp', 'tp.id', '=', 'ta.type_id');
						
			
			
			$data->select(
							"te.id AS sno",
							"te.name AS eventName",
							"ta.name AS actionName",
							"tp.name AS actionType",
							"te.start_time AS startTime",
							"te.end_time AS endTime"
							);
							
			if ($ser_event_id) {
				$data->where('te.id', 'like', '%'.$ser_event_id.'%');
			}
			
			if ($ser_event_name) {
				$data->where('te.name', 'like', '%'.$ser_event_name.'%');
			}
			
			if ($ser_action_name) {
				$data->where('ta.name', 'like', '%'.$ser_action_name.'%');
			}
			
			if ($ser_action_type) {
				$data->where('tp.name', 'like', '%'.$ser_action_type.'%');
			}
			
			$data->orderBy($order_by, $sSortDir_0);
			// echo $dueTimeSearch."<br/>";
			// var_debug($deals->toSql());exit();
					
			return Datatable::query($data)
					->showColumns(array("sno", "eventName", "actionName", "actionType", "startTime","endTime"))
					->addColumn('startTime', function($model) {
						
						$startTime	=	"N/A";
						if ($model->startTime > 0 && !empty($model->startTime) && $model->startTime != NULL){
							$startTime	=	date('m/d/Y H:i A', $model->startTime);
						}
						
						return $startTime;
					})
					->addColumn('endTime', function($model) {
						
						$endTime	=	"N/A";
						if ($model->endTime > 0 && !empty($model->endTime) && $model->endTime != NULL){
							$endTime	=	date('m/d/Y H:i A', $model->endTime);
						}
						
						return $endTime;
					})
					->addColumn('operation', function($model) {

						$currentTime	=	time();
						$confimrationMessage	=	"'Are you sure, you want to delete this Event!'";
						
						return '<a title="Edit"  href="'.URL::to('admin/updateEvent').'/'.$model->sno.'"><i class="fa fa-fw fa-edit"></i></a>&nbsp;<a title="View" id="preview-action-item" href="'.URL::to('admin/eventView').'/'.$model->sno.'?'.$currentTime.'"><i class="glyphicon glyphicon-search"></i></a>&nbsp;<a title="Delete" onclick="Javascript: return deleteConfirmation('.$confimrationMessage.');" href="'.URL::to('admin/deleteTriggerEvent').'/'.$model->sno.'"><i class="fa fa fa-trash-o"></i></a>&nbsp;<a title="Test Email" id="preview-action-item" href="'.URL::to('admin/sendSampleEmail').'/'.$model->sno.'?'.$currentTime.'"><i class="fa fa-envelope"></i></a>';
						

					})
					// ->searchColumns(array("cp.id", "title", "first_name", "secret"))
					 // ->searchColumns(array("cp.id", "title", "secret"))
					->orderColumns(array("sno", "eventName", "actionName", "actionType", "startTime","endTime"))
					->make();
	}
	
	/**
	* TRIGGER ACTION DATATABLE
	*/
	protected function triggerActionDatatable($get)
	{
			$order_by = 'ta.id';
			$sSortDir_0 = 'desc';
					//RECORD BY STATE
			$daytime = strtotime(date('Y-m-d'));
			$ser_action_id			=	Input::get('sSearch_0');
			$ser_action_name		=	Input::get('sSearch_1');
			$ser_action_type		=	Input::get('sSearch_2');
			

			
			//CREATE QUERY FOR GET DATA
			$data = DB::table('trigger_action as ta')
						->leftJoin('trigger_action_type as tp', 'tp.id', '=', 'ta.type_id');
						
			
			
			$data->select(
							"ta.id AS sno",
							"ta.name AS actionName",
							"tp.name AS actionType"
							);
							
							
			if ($ser_action_id) {
				$data->where('ta.id', 'like', '%'.$ser_action_id.'%');
			}
			
			if ($ser_action_name) {
				$data->where('ta.name', 'like', '%'.$ser_action_name.'%');
			}
			
			if ($ser_action_type) {
				$data->where('tp.name', 'like', '%'.$ser_action_type.'%');
			}
			
			$data->orderBy($order_by, $sSortDir_0);
			// echo $dueTimeSearch."<br/>";
			// var_debug($deals->toSql());exit();
					
			return Datatable::query($data)
					->showColumns(array("sno", "actionName", "actionType"))
					// ->addColumn('consume_time', function($model) {
						
						// $stateStr	=	"";
						// if ($model->consume == 'Y'){
							// $stateStr	=	"used <br/>".date('d-m-Y H:i', $model->consume_time);
						// }elseif ($model->expire_time < time() && $model->consume = 'N'){
							// $stateStr	=	EXPIRED;
						// }else{
							// $stateStr	=	VALID;
						// }
						
						// return $stateStr;
					// })
					->addColumn('operation', function($model) {

						$currentTime	=	time();
						$confimrationMessage	=	"'Are you sure, you want to delete this Action!'";
						return '<a title="Edit"  href="'.URL::to('admin/updateNewAction').'/'.$model->sno.'"><i class="fa fa-fw fa-edit"></i></a>&nbsp;<a title="View" id="preview-action-item" href="'.URL::to('admin/actionView').'/'.$model->sno.'?'.$currentTime.'"><i class="glyphicon glyphicon-search"></i></a>&nbsp;<a title="Delete" onclick="Javascript: return deleteConfirmation('.$confimrationMessage.');" href="'.URL::to('admin/deleteTriggerAction').'/'.$model->sno.'"><i class="fa fa fa-trash-o"></i></a>';
					})
					// ->searchColumns(array("cp.id", "title", "first_name", "secret"))
					 // ->searchColumns(array("cp.id", "title", "secret"))
					->orderColumns(array("sno",  "actionName", "actionType"))
					->make();
	}
	
	/**
	* GET TRIGGER ACTION TYPE
	*/
	
	protected function getActionType(){
		
		$sql	=	<<<SQL
							SELECT
								id,
								`name`
							FROM
								trigger_action_type
SQL;

		$getActionType	=	DB::select($sql);
		
		return $getActionType;
	}
	
	/**
	* SAVE TRIGGER ACTION 
	*/
	
	protected function saveTriggerAction($data){
		
		
		 $action_name			=	rtrim($data['action_name']);
		 $action_type			=	rtrim($data['action_type']);
		 $promo_code_type		=	rtrim($data['promo_code_type']);
		 $promo_code_face_value	=	rtrim($data['promo_code_face_value']);
		 $promo_code_percentage	=	rtrim($data['promo_code_percentage']);
		 $email_subject			=	rtrim($data['email_subject']);
		 $email_body			=	DB::connection()->getPdo()->quote(rtrim($data['email_body']));
		 $minimum_order			=	rtrim($data['minimum_order']); 
		 $coupon_validity		=	rtrim($data['coupon_validity']);
		 
		 
		 //FORCE FULLY SET MINIMUM ORDER 0
		 if($minimum_order < 0){
			 $minimum_order = 0;
		 }
		 
		 //SET promo_code_face_value 0 if its percentage
		if($promo_code_type == "percentage"){
			$promo_code_face_value = 0;
		}
		//SET promo_code_percentage 0 if its face_value
		if($promo_code_type == "face_value"){
			$promo_code_percentage = 0;
		}
		
		//FORCE FULLY SET coupon_validity  0
		 if($coupon_validity < 0 || $coupon_validity == ""){
			 $coupon_validity = 0;
		 }
		
		$promo_code_attributeArr							=	array();
		$promo_code_attributeArr['promo_code_type']			=	$promo_code_type;
		$promo_code_attributeArr['promo_amount']			=	$promo_code_face_value;
		$promo_code_attributeArr['promo_percent']			=	$promo_code_percentage;
		$promo_code_attributeArr['minimum_order']			=	$minimum_order;
		$promo_code_attributeArr['coupon_validity']			=	$coupon_validity;
		
		$promo_code_attribute								=	serialize($promo_code_attributeArr);
		 
		$currentTime			=	time();
		$sql	=	<<<SQL
							INSERT INTO trigger_action 
							(
								`name`,
								`subject`,
								`email_body`,
								`type_id`,
								`promo_code_type`,
								`promo_percent`,
								`promo_amount`,
								`minimum_order`,
								`promo_code_attribute`,
								`created`
							)
							VALUES
							(
								'$action_name',
								'$email_subject',
								 $email_body,
								'$action_type',
								'$promo_code_type',
								'$promo_code_percentage',
								'$promo_code_face_value',
								'$minimum_order',
								'$promo_code_attribute',
								'$currentTime'
							)
SQL;

		DB::insert($sql);
		
	}
	
	/**
	* GET DATA 
	*/
	
	protected function  getActionData($id){
	
		$sql	=	<<<SQL
							SELECT
								*
							FROM
								trigger_action
							WHERE
								id = $id
								
SQL;
		$getActionType	=	DB::select($sql);
		
		
		$dataArr	=	array();
		if(isset($getActionType[0]->id)){
			$dataArr	=	$getActionType[0];
		}
		return $dataArr;
	}
	
	/**
	* VALIDATE ACTION SUBMITTED FORM
	*/
	
	protected function validateActionForm($data){
		
		$errMessage	=	"";
		
		 $action_name			=	rtrim($data['action_name']);
		 $action_type			=	rtrim($data['action_type']);
		 $promo_code_type		=	rtrim($data['promo_code_type']);
		 $promo_code_face_value	=	rtrim($data['promo_code_face_value']);
		 $promo_code_percentage	=	rtrim($data['promo_code_percentage']);
		 $email_subject			=	rtrim($data['email_subject']);
		 $email_body			=	rtrim($data['email_body']);
		 $coupon_validity		=	rtrim($data['coupon_validity']);
		 
		 /**
		 * VALIDATE INPUt
		 */
		 
		
		if($action_name==""){
			$errMessage .= " - Action Name is required. <br/>";
		}
		
		if($action_type==""){
			$errMessage .= " - Action Type is required. <br/>";
		}
		
		if($email_subject==""){
			$errMessage .= " - Email Subject is required. <br/>";
		}
		
		if($email_body == ""){
			$errMessage .= " - Email Body is required. <br/>";
		}
		
		
		if($action_type=="2"){
			
			if($promo_code_type==""){
				$errMessage .= " - Promo Code Type is required. <br/>";
			}
			
			if($promo_code_type=="face_value"){
				if($promo_code_face_value==""){
					$errMessage .= " - Promo Code Face is required. <br/>";
				}
				
				if($promo_code_face_value!=""){
					if(!is_numeric($promo_code_face_value) || $promo_code_face_value<=0){
						$errMessage .= " - Promo Code Face should be valid. <br/>";
					}
				}
			}
			
			if($promo_code_type=="percentage"){
				if($promo_code_percentage==""){
					$errMessage .= " - Promo Card Discount %. <br/>";
				}
				
				if($promo_code_percentage!=""){
					if(!is_numeric($promo_code_percentage) || $promo_code_percentage<=0){
						$errMessage .= " - Promo Card Discount % should be valid. <br/>";
					}
				}
			}
			
			
			if($coupon_validity!=""){
				if(!is_numeric($coupon_validity) || $coupon_validity<=0){
					$errMessage .= " - Coupon Validity should be valid. <br/>";
				}
			}
		}
		
		return $errMessage;
	}
	
	//UPDATE TRIGGER ACTION
	protected function updateTriggerAction($data){

		 $action_name			=	rtrim($data['action_name']);
		 $action_type			=	rtrim($data['action_type']);
		 $promo_code_type		=	rtrim($data['promo_code_type']);
		 $promo_code_face_value	=	rtrim($data['promo_code_face_value']);
		 $promo_code_percentage	=	rtrim($data['promo_code_percentage']);
		 $email_subject			=	rtrim($data['email_subject']);
		 $email_body			=	DB::connection()->getPdo()->quote(rtrim($data['email_body']));
		 $action_id				=	rtrim($data['action_id']);
		 $minimum_order			=	rtrim($data['minimum_order']); 
		 $coupon_validity		=	rtrim($data['coupon_validity']);
	
		 
		 //FORCE FULLY SET MINIMUM ORDER 0
		 if($minimum_order < 0){
			 $minimum_order = 0;
		 }
		 
		//SET promo_code_face_value 0 if its percentage
		if($promo_code_type == "percentage"){
			$promo_code_face_value = 0;
		}
		//SET promo_code_percentage 0 if its face_value
		if($promo_code_type == "face_value"){
			$promo_code_percentage = 0;
		}
		
		//FORCE FULLY SET coupon_validity  0
		 if($coupon_validity < 0 || $coupon_validity == ""){
			 $coupon_validity = 0;
		 }
		
		$promo_code_attributeArr							=	array();
		$promo_code_attributeArr['promo_code_type']			=	$promo_code_type;
		$promo_code_attributeArr['promo_amount']			=	$promo_code_face_value;
		$promo_code_attributeArr['promo_percent']			=	$promo_code_percentage;
		$promo_code_attributeArr['minimum_order']			=	$minimum_order;
		$promo_code_attributeArr['coupon_validity']			=	$coupon_validity;
		
		$promo_code_attribute								=	serialize($promo_code_attributeArr);
		 
		 $currentTime			=	time();
		$sql	=	<<<SQL
							UPDATE 
								trigger_action 
							SET
								`name` 						= '$action_name',
								`subject`					=	'$email_subject',
								`email_body`				=	$email_body,
								`type_id`					=	'$action_type',
								`promo_code_type`			=	'$promo_code_type',
								`promo_percent`				=	'$promo_code_percentage',
								`promo_amount`				=	'$promo_code_face_value',
								`minimum_order`				=	'$minimum_order',
								`promo_code_attribute`		=	'$promo_code_attribute',
								`updated`					=	'$currentTime'
							WHERE
								id = $action_id
SQL;

		DB::update($sql);
	}
	
	
	/*
	* TRIGGER ACTION DELETED
	*/
	protected function deleteActionTrigger($id){
		
		$sql	=	<<<SQL
							DELETE 
								FROM
							trigger_action 
							WHERE
								id = $id
SQL;

		DB::delete($sql);
	}
	
	/*
	* GET ACTION LIST
	*/
	protected function getActionList(){
		
		$sql	=	<<<SQL
							SELECT
							id,name
								FROM
							trigger_action 
SQL;

		$getActionList	=	DB::select($sql);
		
		$getActionListArr	=	array();
		if(isset($getActionList[0]->id)){
			$getActionListArr	=	$getActionList;
		}
		return $getActionListArr;
	}
	
	/**
	* VALIDATE TRIGGER FORM
	*/
	protected function validateTriggerForm($data){
		
		
		 $event_name			=	rtrim($data['event_name']);
		 $action_name			=	rtrim($data['action_name']);
		 $is_enable				=	rtrim($data['is_enable']);
		 $start_time			=	rtrim($data['start_date_time']);
		 $end_time				=	rtrim($data['end_date_time']);
		 
		$errMessage	=	"";
			
		if($event_name==""){
			$errMessage .= " - Event Name is required. <br/>";
		}
		
		if($action_name==""){
			$errMessage .= " - Action Name is required. <br/>";
		}
		
		if($is_enable==""){
			$errMessage .= " - Please Select Trigger Enable Option. <br/>";
		}
		
		if($end_time != ""){
			
			$start_timeVal 		= strtotime($start_time);
			$end_timeVal	 	= strtotime($end_time);
			
			if($start_time!=""){
				if($start_timeVal >= $end_timeVal){
					$errMessage .= " - Please Select Valid Date Range. <br/>";
				}
			}else{
				$errMessage .= " - Please Select Start Date. <br/>";
			}
		}
		 
		return $errMessage;
	}
	
	/**
	* ADD TRIGGER EVENT
	*/
	
	protected function addTriggerEvent($data){
		
		 $event_name			=	rtrim($data['event_name']);
		 $action_name			=	rtrim($data['action_name']);
		 $is_enable				=	rtrim($data['is_enable']);
		 $start_time			=	rtrim($data['start_date_time']);
		 $end_time				=	rtrim($data['end_date_time']);
		 
		 if(!empty($start_time)){
			 $start_time	=	strtotime($start_time);
		 }
		 
		 if(!empty($end_time)){
			 $end_time	=	strtotime($end_time);
		 }
		 
		 $currentDate	=	time();
		 
		 $sql	=	<<<SQL
						INSERT
							INTO
						trigger_event
							(
							`name`,
							`action_id`,
							`start_time`,
							`end_time`,
							`is_active`,
							`created`
							)
						VALUES
							(
								'$event_name',
								'$action_name',
								'$start_time',
								'$end_time',
								'$is_enable',
								'$currentDate'
							)
SQL;
		DB::insert($sql);
	}
	
	/**
	* GET getEventData
	*/
	protected function getEventData($id){
		
		$sql	=	<<<SQL
							SELECT
								*
							FROM
								trigger_event
							WHERE
								id = $id
SQL;
		$getEventData	=	DB::select($sql);
		
		$getEventDataArr	=	array();
		if(isset($getEventData[0]->id)){
			$getEventDataArr	=	$getEventData[0];
		}
		
		return $getEventDataArr;
	}
	
	/**
	* Update Trigger Event Info
	*/
	
	protected function updateTriggerEvent($data){
		
		 $event_name			=	rtrim($data['event_name']);
		 $action_name			=	rtrim($data['action_name']);
		 $is_enable				=	rtrim($data['is_enable']);
		 $start_time			=	rtrim($data['start_date_time']);
		 $end_time				=	rtrim($data['end_date_time']);
		 $event_id				=	rtrim($data['event_id']);
		 
		 if(!empty($start_time)){
			 $start_time	=	strtotime($start_time);
		 }
		 
		 if(!empty($end_time)){
			 $end_time	=	strtotime($end_time);
		 }
		 
		 $currentDate	=	time();
		 
		 $sql	=	<<<SQL
						UPDATE
							trigger_event
						SET
							`name` 			= '$event_name',
							`action_id` 	= '$action_name',
							`start_time` 	= '$start_time',
							`end_time` 		= '$end_time',
							`is_active` 	= '$is_enable',
							`updated` 		= '$currentDate'
						WHERE
							id = $event_id
SQL;
		DB::update($sql);
	}
	
	/**
	* DELETE TRIGGER EVENT
	*/
	protected function deleteActionEvent($id){
		
		$sql	=	<<<SQL
							DELETE 
								FROM
							trigger_event 
							WHERE
								id = $id
SQL;

		DB::delete($sql);
	}
	
	/**
	* GET EVENT ACTION DETAIL
	*/
	protected function getEventActionData($id){
		
		$sql	=	<<<SQL
						
						SELECT 
							ta.subject 			AS email_subject,
							ta.email_body 		AS email_body,
							te.id 				AS event_id,
							ta.type_id 			AS type_id,
							ta.promo_code_type 	AS promo_code_type,
							ta.promo_percent 	AS promo_percent,
							ta.promo_amount 	AS promo_amount
						FROM
							trigger_event AS te
						LEFT JOIN 
							trigger_action AS ta
						ON
							ta.id = te.action_id
						LEFT JOIN
							trigger_action_type AS tp
						ON
							tp.id = ta.type_id
						WHERE
							te.id = $id
			
SQL;
		$getEventActionData	=	DB::select($sql);
		
		
		$getEventActionDataArr	=	array();
		
		if(isset($getEventActionData[0]->email_body)){
			$getEventActionDataArr	=	$getEventActionData[0];
		}
		
		return $getEventActionDataArr;
	}
	
	//CHECK ACTION CAN DELETE
	protected function isActionAssociateWithEvent($id){
		
		$sql	=	<<<SQL
						SELECT
							count(id) AS totalCount
						FROM 
							trigger_event
						WHERE
							action_id = $id
SQL;
		
		
		$getEventActionData	=	DB::select($sql);
		
		
		$getEventActionDataArr	=	0;
		
		if(isset($getEventActionData[0]->totalCount)){
			$getEventActionDataArr	=	$getEventActionData[0]->totalCount;
		}
		
		if($getEventActionDataArr>0){
			return true;
		}
		
		return false;
		
	}
}