<?php
class SupplierEmail extends \Eloquent {

	protected function getSupplierEmail($get)
	{
		$order_by	= 'value';
		$sSortDir_0 = 'asc';
		$data = array();
		$column_names = array('e.ref_no', 't.title', 'tm.title', 'e.recipient');
		
		$iSortCol_0		= $get['iSortCol_0'];
        $sSortDir_0		= $get['sSortDir_0'];
        
		$sSearch_0		= $get['sSearch_0'];	#	REF
        $sSearch_1		= $get['sSearch_1'];	#	Team
		$sSearch_2		= $get['sSearch_2'];	#	Option
        $sSearch_3		= $get['sSearch_3'];	#	Recipient
		
		$email_config = DB::table('supplier_mail_order_tracking as e');
					
		if( ($iSortCol_0 != '') && (isset($column_names[$iSortCol_0])) )
        {
        	$order_by = $column_names[$iSortCol_0];
        }
        
		if ($sSearch_0 != ''){
			$email_config->where('e.ref_no', '=', $sSearch_0);
		}
		
		if ($sSearch_1 != ''){
			$email_config->where('t.title', 'like', '%'.$sSearch_1.'%');
		}
		
		if ($sSearch_2 != ''){
			$email_config->where('tm.title', 'like', '%'.$sSearch_2.'%');
		}
		
		if ($sSearch_3 != ''){
			$email_config->where('e.recipient', 'like', '%'.$sSearch_3.'%');
		}
			
		$email_config->select('e.ref_no', 't.title as team_title', 'tm.title as option_title', 'e.recipient')
					 ->leftJoin('team as t', 'e.team_id', '=', 't.id')
					 ->leftJoin('team_multi as tm', 'e.option_id', '=', 'tm.id')
					 ->orderBy($order_by, $sSortDir_0); 
		return Datatable::query($email_config)
				->showColumns(array('ref_no', 'team_title', 'option_title', 'recipient'))
				/*->addColumn('edit', function($model) {
						return '<a txt="'.$model->team_id.'" class="enable_disable_emails" id="'.$model->team_id.'" style="text-decoration: none;"><span  data-original-title="Enable/Disable" id="span_'.$model->team_id.'">'.$model->team_id.'</span></a>';	
				})*/
		->orderColumns(array('e.ref_no', 't.title', 'tm.title', 'e.recipient'))
		->make();
	}
	
}