<?php
class AuthorizeParser
{
	private $response;
	private $response_by_code;
	public $OK = false;
	public $caller_object = 'AIM';
	
	protected $_handleResponse;

	function _handleResponse(){
		return $this->_handleResponse;
	}
	
	function __construct($_handleRequest, $_handleResponse, $caller_object = AuthorizeNetAIM) {

		$this->_handleResponse->OK = FALSE;
		$this->_handleResponse = $_handleResponse;
		
		$this->_handleResponse->response_code =
			isset($_handleResponse->response_code) ? 
			$_handleResponse->response_code : -1;
			
		$this->_handleResponse->response_reason_code =
			isset($_handleResponse->response_reason_code) ? 
			$_handleResponse->response_reason_code : -1;

		$this->caller_object = $caller_object;
		
		$this->_handleResponse->response_reason_text  = 'Error connecting to Authorize.net, please check your credentials';
		$this->_handleResponse->response_reason_notes = 'Please make sure that Authorize.net end point is correct.';
		
        $a = func_get_args(); 
        $i = func_num_args();
        if(count($a) < 2){
        	
        	if(count($a) != 0){
	        	$f='__constructProfile';
				return call_user_func_array(array($this,$f),$a);
        	}else{
	        	echo 'Error in passing params';
	        	exit;
        	}
        }
        
        if( 
        	($this->caller_object instanceof AuthorizeNetCIM) 
        )
        {
        	debug('CIM Profile');

        	if( 
        		# Customer Profile
        		preg_match('/createCustomerProfileRequest/',$_handleRequest)	||
        		
        		# Customer Payment Profile
        		preg_match('/createCustomerPaymentProfileRequest/',$_handleRequest)
        	){
        	
	        	if($_handleResponse->isOk()){
					$this->_handleResponse->OK = true;
					# Set fixed resposne code and reason code if success to compatible with AIM attributes
					$this->_handleResponse->response_code 			= 1;
					$this->_handleResponse->response_reason_code 	= 1;
					$this->_setResponse();	
	        	}
				$this->_handleResponse->response_reason_text  = $this->_handleResponse->getMessageCode();
				$this->_handleResponse->response_reason_notes = $this->_handleResponse->getMessageText();

			}else{
	        	
	        	if($_handleResponse->isOk()){
	        		
					$this->_handleResponse->OK = true;
					# Set fixed resposne code and reason code if success to compatible with AIM attributes
					$this->_handleResponse->response_code 			= 1;
					$this->_handleResponse->response_reason_code 	= 1;
					$this->_setResponse();
					
					$this->_handleResponse->response_reason_text  = $this->_handleResponse->getMessageCode();
					$this->_handleResponse->response_reason_notes = $this->_handleResponse->getMessageText();
				
					# Map CIM response same as AIM
					$validationResponses = $this->_handleResponse->getTransactionResponse();
					$this->_handleResponse->responseArray = $validationResponses;
					//debug('$validationResponses=>');
					//debug($validationResponses);
					//exit;
					//$this->_handleResponse->response = $validationResponses;
					//debug('$validationResponses=>');
					//debug($this->_handleResponse->response);
					//exit;
					
				}
				
	        }
        }        
        if (method_exists($this,$f='__constructTransaction')) {
			return call_user_func_array(array($this,$f),$a);
        }
	        
	}
	
	/**/
	function __constructTransaction(){
		$this->_setResponse();
	}
	
	function __constructProfile(){

	}
	
	function _setResponse(){
		if(empty($this->_handleResponse->response_code) && empty($this->_handleResponse->response_reason_code)){
			/*
			$this->_handleResponse->response = array(
				'text' 	=> 'Error connecting to Authorize.net, please check your credentials',
				'notes' => 'Please make sure that Authorize.net end point is correct.'
			);
			*/
			$this->_handleResponse->OK = false;
		}else{
			$result = mysql_query("
				SELECT 
					* 
				FROM 
					authorize_parser
				WHERE
					response_code = {$this->_handleResponse->response_code}
				AND
					response_reason_code = {$this->_handleResponse->response_reason_code}
				LIMIT 1
			");
			$data = array();
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			$this->_handleResponse->response_reason_text  = $row['response_reason_text'];
			$this->_handleResponse->response_reason_notes = $row['response_reason_notes'];
			
			if($this->_handleResponse->response_code==1 && $this->_handleResponse->response_reason_code==1)
				$this->_handleResponse->OK = true;
				
			mysql_free_result($result);
		}
	}
	
	
	function getResponse(){
		return $this->_handleResponse->response;
	}
	function getResponseText(){
		return $this->_handleResponse->response['text'];
	}
	function getResponseNotes(){
		return $this->_handleResponse->response['notes'];
	}
	
	function getResponseByCode($response_code, $response_reason_code){
		return $this->_handleResponse->response_by_code[$response_code][$response_reason_code];
	}
	
	function getDecision(){
		$res = mysql_query("
			SELECT 
				us_decision 
			FROM 
				authorize_parser 
			WHERE 
				response_code={$response_code} 
			AND 
				response_reason_code={$response_reason_code}
		");
		$data = mysql_result($res);
		exit;
	}
	
	/*
	function authParseResponse(
		$response_code=false,
		$response_reason_code=false
	)
	{
		$res = mysql_query("SELECT us_decision FROM authorize_parser WHERE response_code={$response_code} AND response_reason_code={$response_reason_code}");
		$data = mysql_result($res);
		debug($data);
		exit;
	}
	*/
}
