<?PHP
//extension of ?????'s credit card class
//usage is:-
// <$fieldname>= credit_card:check(<$creditcard>, <$validfrom>, <$validto>);
//which returns a parameter array where:-
// $<fieldname>['Valid']	indicates if the card passes the luhn modulo 10 check
// $<fieldname>['CanAccept'] 	indicates if the card is acceptable to your installation
// $<fieldname>['Index'] 	indicates the card type, identified below - count satrts from 10, unless it is unknown
// $<fieldname>['Type']		returns a text string identifying the card eg 'VISA', 'Switch / Maestro' etc....
// $<fieldname>['VTo']		returns true if the "Valid To" date is invalid
// $<fieldname>['VFr']		returns true if the "Valid From" date (or "Issue No") is invalid
//
// Note the 'CanAccept' parameter contravenes the spirit of an open source class - it is very site specific
// most of my customers are unable to accept all cards, so the 'CanAccept' is a kludge workaround to flag
// cards they can or cannot accept. It was getting to hairy to try and develop a scheme which would idetify
// each card type they could or could not use. you may elect to ignore the 'CanAccept' parameter
//
// Copyright notice: there isn't one, you are free to do what you wish with this class, no limits "just do it.."
//
// No warranties are provided, no guarantees, no liabilities, are provided - it is provided "as is" in the
// hope that it may prove usefull to the PHP community, or as a basis for other developments
//
// You are strongly advised to thoroughly test the class before using in a live site.
// author M.W.Heald matthew.heald@virgin.net
// parts of this class are based on a previous author's class contributed to phpclasses.org, sadly I do not
// have that authors name or contact details - but thanks mate - much appreciated.
//
class credit_card
{ 
    
    private function checkSum($ccnum) {
		$checksum = 0;
		for ($i=(2-(strlen($ccnum) % 2)); $i<=strlen($ccnum); $i+=2)
		{
			$checksum += (int)($ccnum{$i-1});
		}
	    for ($i=(strlen($ccnum)% 2) + 1; $i<strlen($ccnum); $i+=2) 
		{
	  		$digit = (int)($ccnum{$i-1}) * 2;
	  		if ($digit < 10) { 
	  			$checksum += $digit; 
	  		}
	  		else { 
	  			$checksum += ($digit-9); 
	  		}
    	}
		if (($checksum % 10) == 0) 
			return true; 
		else 
			return "Invalid creditcard number. Please check creditcard number";
	}

        public function isValidCardNumber($cardnumber) {
		$creditcard = array(  "visa"=>"/^4\d{3}-?\d{4}-?\d{4}-?\d{4}$/",
						"mastercard"=>"/^5[1-5]\d{2}-?\d{4}-?\d{4}-?\d{4}$/",
						"discover"=>"/^6011-?\d{4}-?\d{4}-?\d{4}$/",
						"amex"=>"/^3[4,7]\d{13}$/",
						"diners"=>"/^3[0,6,8]\d{12}$/",
						"bankcard"=>"/^5610-?\d{4}-?\d{4}-?\d{4}$/",
						"jcb"=>"/^[3088|3096|3112|3158|3337|3528]\d{12}$/",
						"enroute"=>"/^[2014|2149]\d{11}$/",
						"switch"=>"/^[4903|4911|4936|5641|6333|6759|6334|6767]\d{12}$/");
		$match=false;
		foreach($creditcard as $type=>$pattern) {
			if(preg_match($pattern,$cardnumber) === 1) {
				$match=true;
				$this->cardType = $type;
				break;
			}
		}
		if(!$match) {
			return "Invalid";			
		}
		else {
			return $this->checkSum($cardnumber);
		}	
	}
        
        private function checkExpDate() {
		$expTs = mktime(0, 0, 0, $this->data['month'] + 1, 1, $this->data['year']);
		$curTs = time();
		$maxTs = $curTs + (10 * 365 * 24 * 60 * 60);
		if ($expTs > $curTs && $expTs < $maxTs) {
			return true;
		} else {
			throw new Exception("Invalid Expiry Month/Year");			
		}
	}
        
        private function validateCVV() {
		$count = ($this->cardType === 'amex') ? 4 : 3;
		if(preg_match('/^[0-9]{'.$count.'}$/', $this->data['cvv'])) {
			return true;
		} 
		else { 
		   throw new Exception("Invalid format of CVV number");		   
		} 
	}
        
}
?>