<?php
require_once 'class.logging.php';
require_once 'skeletons/iPaymentProcessorProfile.php';
require_once 'PaymentProcessor.php';
require_once 'functions/functions_payment_processors.php';
/**
 * @package 	iPaymentProcessor
 * @subpackage 	PaymentProcessor
 * @author 	Nouman
 * @since	March20,2014
 * @version 0.1
 * @desc
 * 	Payment Processor Interface
 * 
 * Default Payment Process is to be choosen based on the Platform we are going to use Payment Processor
 * 
 * In our case we use Authorize.net CardPresent
 * 
 * */
CLASS PaymentProcessorProfile EXTENDS PaymentProcessor implements iPaymentProcessorProfile {
	
	/*
	 * New Methods
	 * */	
	protected $profile_id = false;
	protected $payment_profile_id = false;
	
	function setProfileId($profile_id = false){
		$this->profile_id = $profile_id;
	}
	function getProfileId(){
		return $this->profile_id;
	}
	function setPaymentProfileId($payment_profile_id = false){
		$this->payment_profile_id = $payment_profile_id;
	}
	function getPaymentProfileId(){
		return $this->payment_profile_id;
	}

	
	function createCustomerProfile(
		$options = array(
                    'email'=>'nouman+payment_processors@nextgeni.com',
                    'merchantCustomerId'=>null,
                    'description' => 'Description of customer'
		)
	)
	{
		
		$log = new Logging();
		$log->log(__METHOD__);
		$this->last_payment_action = 'CIM_CustomerProfile';
		
		if($this->test_mode)
			return $this->_process();
		
		extract($options);
		$customerProfile = new AuthorizeNetCustomer;
		$customerProfile->description = "";
		$customerProfile->merchantCustomerId =  $merchantCustomerId . '_'. time().rand(1,100);
		$customerProfile->email = $email;
		$response = $this->_process($this->connection()->createCustomerProfile($customerProfile));
	    $customerProfileId = $this->response->getCustomerProfileId();
		$this->setProfileId($customerProfileId);
		return $response;
		
	}
	
	/**
	 * @author 	NoumanArshad
	 * @date	Sep22,2014
	 * 
	 * @desc
	 * 	Customer payment profile
	 * */
	function createPaymentProfile($credit_card=false, $expiry=false, $customerProfileId = false){
		
		$log = new Logging();
		$log->log(__METHOD__);
		$this->last_payment_action = 'CIM_CustomerPaymentProfile';
		
		if($this->test_mode)
			return $this->_process();
		
		( $credit_card		? $this->setCreditCard($credit_card) : null);
		( $expiry			? $this->setExpiry($expiry) : null);
		( $customerProfileId? $this->setProfileId($customerProfileId) : $this->getProfileId());
		
		$v = $this->getCreditCard();
		if(empty($v)){
			$this->_validationError('Error in CreditCard');
		}
		
		$v = $this->getExpiry();
		if(empty($v)){
			$this->_validationError('Error in Expiry');
		}
		
		$v = $this->getProfileId();
		if(empty($v)){
			$this->_validationError('Error in Customer Profile Id');
		}
		
		if(!$this->isValidationError()){
			
			// Add payment profile.
		    $paymentProfile = new AuthorizeNetPaymentProfile;
		    $paymentProfile->customerType = "individual";
		    $paymentProfile->payment->creditCard->cardNumber 		= $this->getCreditCard();
		    $paymentProfile->payment->creditCard->expirationDate 	= $this->getExpiry();
		    
			$response = $this->_process($this->connection()->createCustomerPaymentProfile($customerProfileId, $paymentProfile));
		    $paymentProfileId = $this->response->getPaymentProfileId();
			$this->setPaymentProfileId($paymentProfileId);
			return $response;
			
			
		}
		
		return $this->result;
			
	}
	
	function authorizeAndCapture($amount=null, $customerProfileId=false, $customerPaymentProfileId=false){
		
		$log = new Logging();
		$log->log(__METHOD__);
		$this->last_payment_action = 'CIM_authorizeAndCapture';
		
		if($this->test_mode)
			return $this->_process();

		( $amount 			 ? $this->setAmount($amount) : null);
		( $customerProfileId ? $this->setProfileId($customerProfileId) : $this->getProfileId());
		( $customerPaymentProfileId	? $this->setPaymentProfileId($customerPaymentProfileId) : $this->getPaymentProfileId());
		
		$v = $this->getAmount();
		if(empty($v)){
			$this->_validationError('Error in Amount');
		}
		
		if(empty($this->profile_id)){
			$this->_validationError('Error in Customer Profile Id');
		}
		
		if(empty($this->payment_profile_id )){
			$this->_validationError('Error in Customer Payment Profile Id');
		}
		
		if(!$this->isValidationError()){
			
			
		    $transaction = new AuthorizeNetTransaction;
		    $transaction->amount = $amount;
		    $transaction->customerProfileId = $this->getProfileId();
		    $transaction->customerPaymentProfileId = $this->getPaymentProfileId();
		    //$transaction->customerShippingAddressId = $customerAddressId;
		    
		    $lineItem              = new AuthorizeNetLineItem;
		    $lineItem->itemId      = "4";
		    $lineItem->name        = "Cookies";
		    $lineItem->description = "Chocolate Chip";
		    $lineItem->quantity    = "4";
		    $lineItem->unitPrice   = "1.00";
		    $lineItem->taxable     = "true";
		    
		    /*
		    $lineItem2             = new AuthorizeNetLineItem;
		    $lineItem2->itemId     = "4";
		    $lineItem2->name       = "Cookies";
		    $lineItem2->description= "Peanut Butter";
		    $lineItem2->quantity   = "4";
		    $lineItem2->unitPrice  = "1.00";
		    $lineItem2->taxable    = "true";
		    
		    $transaction->lineItems[] = $lineItem2;
		    */
		    $transaction->lineItems[] = $lineItem;
		    
		    if(!empty($this->lineItems)){
		    	return 1;
		    }
		    
			$response = $this->_process($this->connection()->createCustomerProfileTransaction("AuthCapture", $transaction, 'x_duplicate_window=1'));
		    $transactionResponse = $this->response->getTransactionResponse();
		    $transactionId = $transactionResponse->transaction_id;
			$this->setTransactionId($transactionId);
			return $response;
		    
			
		}
		
		return $this->result;

	}
	
	function addItem(){
	    $lineItem              = new AuthorizeNetLineItem;
	    $lineItem->itemId      = "4";
	    $lineItem->name        = "Cookies";
	    $lineItem->description = "Chocolate Chip";
	    $lineItem->quantity    = "4";
	    $lineItem->unitPrice   = "1.00";
	    $lineItem->taxable     = "true";
		
	    $this->lineItems[] = $lineItem;
	    
	}
	
	function authorize($amount=null){}
	function capture($auth_code = false, $amount = false){}
	function void($amount=null){}
	function refund($trans_id = false, $amount = false){}
	
	
	
	
}
