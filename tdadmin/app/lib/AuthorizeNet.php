<?php
/**
 * The AuthorizeNet PHP SDK. Include this file in your project.
 *
 * @package AuthorizeNet
 */
require dirname(__FILE__) . '/authorize_lib/shared/AuthorizeNetRequest.php';
require dirname(__FILE__) . '/authorize_lib/shared/AuthorizeNetTypes.php';
require dirname(__FILE__) . '/authorize_lib/shared/AuthorizeNetXMLResponse.php';
require dirname(__FILE__) . '/authorize_lib/shared/AuthorizeNetResponse.php';
require dirname(__FILE__) . '/authorize_lib/AuthorizeNetAIM.php';
require dirname(__FILE__) . '/authorize_lib/AuthorizeNetARB.php';
require dirname(__FILE__) . '/authorize_lib/AuthorizeNetCIM.php';
require dirname(__FILE__) . '/authorize_lib/AuthorizeNetSIM.php';
require dirname(__FILE__) . '/authorize_lib/AuthorizeNetDPM.php';
require dirname(__FILE__) . '/authorize_lib/AuthorizeNetTD.php';
require dirname(__FILE__) . '/authorize_lib/AuthorizeNetCP.php';

if (class_exists("SoapClient")) {
    require dirname(__FILE__) . '/authorize_lib/AuthorizeNetSOAP.php';
}
/**
 * Exception class for AuthorizeNet PHP SDK.
 *
 * @package AuthorizeNet
 */
class AuthorizeNetException extends Exception
{
}