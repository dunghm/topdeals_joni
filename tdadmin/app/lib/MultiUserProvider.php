<?php //app/lib/MultiUserProvider.php

use Illuminate\Auth\UserProviderInterface,
    Illuminate\Auth\UserInterface,
    Illuminate\Auth\GenericUser;

class MultiUserProvider implements UserProviderInterface {

	protected $providers;

	protected function __construct() {

		// This should be moved to the config later...
		// This is a list of providers that can be used, including
		// their user model, hasher class, and hasher options...
		$this->providers = array(
			'joomla' => array(
				'model' => 'JoomlaUser',
				'hasher' => 'JoomlaHasher',
				),
			'another' => array(
				'model' => 'AnotherUser',
				'hasher' => 'AnotherHasher',
				'options' => array(
					'username' => 'empolyee_number',
					'salt' => 'salt',
					)
				),
			);
	}
	/**
	 * Retrieve a user by their unique identifier.
	 *
	 * @param  mixed  $identifier
	 * @return \Illuminate\Auth\UserInterface|null
	 */
	public function retrieveById($identifier)
	{
		// Returns the current provider from the session.
		// Should throw an error if there is none...
		$provider = Session::get('user.provider');

		$user = $this->createModel($this->providers[$provider]['model'])->newQuery()->find($identifier);

		if ($user){
			$user->provider = $provider;
		}

		return $user;
	}

	/**
	 * Retrieve a user by the given credentials.
	 *
	 * @param  array  $credentials
	 * @return \Illuminate\Auth\UserInterface|null
	 */
	public function retrieveByCredentials(array $credentials)
	{
		// First we will add each credential element to the query as a where clause.
		// Then we can execute the query and, if we found a user, return it in a
		// Eloquent User "model" that will be utilized by the Guard instances.

		// Retrieve the provider from the $credentials array.
		// Should throw an error if there is none...
		$provider = $credentials['provider'];

		$query = $this->createModel($this->providers[$provider]['model'])->newQuery();

		foreach ($credentials as $key => $value)
		{
			if ( ! str_contains($key, 'password') && ! str_contains($key, 'provider'))
				$query->where($key, $value);
		}

		$user = $query->first();

		if ($user){
			Session::put('user.provider', $provider);
			$user->provider = $provider;
		}

		return $user;
	}

	/**
	 * Validate a user against the given credentials.
	 *
	 * @param  \Illuminate\Auth\UserInterface  $user
	 * @param  array  $credentials
	 * @return bool
	 */
	public function validateCredentials(UserInterface $user, array $credentials)
	{
		$plain = $credentials['password'];

		// Retrieve the provider from the $credentials array.
		// Should throw an error if there is none...
		$provider = $credentials['provider'];

		$options = array();

		if (isset($this->providers[$provider]['options'])){
			foreach ($this->providers[$provider]['options'] as $key => $value) {
				$options[$key] = $user->$value;
			}
		}

		return $this->createModel($this->providers[$provider]['hasher'])
			->check($plain, $user->getAuthPassword(), $options);
	}

	/**
	 * Create a new instance of a class.
	 *
	 * @param string $name Name of the class
	 * @return Class
	 */
	public function createModel($name)
	{
		$class = '\\'.ltrim($name, '\\');

		return new $class;
	}

}
?>