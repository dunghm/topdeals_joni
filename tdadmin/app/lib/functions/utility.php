<?php
function _getLast4Digits($string=null){
	if(null!==$string)
		$string	= substr($string, -4);	# Extract last 4 characters
	return $string;
}

function _maskString($str, $pattern='X', $start=2, $end=4, $str_pad=STR_PAD_RIGHT){
	
	$length = strlen($str);
	
	if($length < 10)
		$end = 2;
	$numberOfCharacters = $start + $end;
	$len = $length - $numberOfCharacters;
	
	$string = $str;
	$string = substr($string, 0, $start); 		# Extract initial 2 characters
	$string	= str_pad($string, $len, $pattern, $str_pad);
	$string	.= substr($str, (-1 * $end));	# Extract last 4 characters
	return $string;
}

function strNVPToJson($strNVP){
	
	//$strNVP = "x_cpversion=1.0&x_delim_char=%2C&x_encap_char=%7C&x_market_type=2&x_response_format=1&x_test_request=TRUE&x_device_type=4&x_amount=1000.0&x_track2=4111111111111111%3D1803101000020000831&x_duplicate_window=1&x_type=AUTH_CAPTURE&x_login=8wS2uGn3q&x_tran_key=6B2cyb3P3vf35Xvr";
	
	if(!empty($strNVP)){
		
		$requestArrayIndex = explode("&", $strNVP);
		$requestArray = array();
		foreach($requestArrayIndex as $index => $value){
			$items = explode("=", $value);
			$requestArray[$items[0]] = $items[1];
		}
		if(!empty($requestArray))
			foreach($requestArray as $index => $arrItem){
				if(in_array($index, array('x_login', 'x_tran_key', 'x_track1', 'x_track2')))
					$requestArray[$index] = _maskString($requestArray[$index], '_');
			}
			
		if(is_array($requestArray)){
			return json_encode($requestArray);
		}
		return $requestArray;
	}
}
