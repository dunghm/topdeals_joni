<?php

/*
 * create table "log_payment_processors" 
 * */
CLASS LogPaymentProcessors{

	var $table; # table name

	var $id;
	var $type = 'AUTHORIZE';
	var $request;
	var $response;
	var $created;
	
	function __construct(){
		$this->created 	= date('Y-m-d H:i:s');
		$this->table = 'log_payment_processors';
	}
        
        function getCreated_test(){return "asfasf";}

	function getCreated(){return $this->created;}
	
	function setType($type='AUTHORIZE'){
		if(!empty($type) || in_array($type, array('AUTHORIZE', 'CYBERSOURCE', 'PAYPAL')))
			$this->type=$type;
	}
	function getType(){return $this->type;}
	
	function setRequest($request){$this->request=$request;}
	function getRequest(){return $this->request;}
	
	function setResponse($resposne){$this->resposne=$resposne;}
	function getResponse(){return $this->resposne;}
	
	
	/**
	 * We have to use PDO connection for rest of the DB operations which are outside CI
	 * PDO is much safer than mysql
	 * FIXME: use PDO connection
	 * */
	function save($toSaveThirdParty=true){
		//$this->request 	= mysql_real_escape_string($this->request);
		//$this->response	= mysql_real_escape_string($this->response);
		$request 	= mysql_real_escape_string($this->getRequest());
		$response 	= mysql_real_escape_string($this->getResponse());
		mysql_query("INSERT INTO {$this->table} VALUES (null, '{$this->getType()}', '{$request}', '{$response}', '{$this->getCreated()}')");
		
		if($toSaveThirdParty){
			$this->saveThirdPartyLogs();
		}
	}
	
	function _formatJson($string){
		return strNVPToJson($string);
	}
	
	function saveThirdPartyLogs(){
		//$request 	= mysql_real_escape_string($this->getRequest());
		//$response 	= mysql_real_escape_string($this->getResponse());
		
		$service_provider_id = 3;
		$type = $this->getType();
		if($type=='CYBERSOURCE')
			$service_provider_id = 4;
			
		$direction = 2;
		$ip_address = $_SERVER['REMOTE_ADDR'];
		
		$request 	= $this->_formatJson($this->getRequest());
		$response 	= json_encode($this->getResponse());
		
		$query = "
			INSERT INTO `third_party_logs` VALUES 
			(
				null
				, $service_provider_id
				, $direction
				, '$ip_address'
				, '{$type}'
				, '{$request}'
				, '{$response}'
				, '{$this->getCreated()}'
				, '{$this->getCreated()}'
			)
		";
		//echo $query;
		mysql_query($query);
	}
	
}

class Logging{
	
	var $ROOT;
	var $file;
	var $saveLogs=true;
	var $lineNumber;
	var $prefix;
	var $logFormat 	= 'Ymd H:i:s';
	var $newLine	= "\n\r";
	
	function __construct($file='log'){
		$this->ROOT = CONF_LOG . '/';
		$this->file = $file;
	}
	
	function setLogFile($file='log'){
		$this->file = $file;
	}
	
	function getDate(){
		return date("Ymd");
	}
	
	function getFileName(){
		return $this->ROOT. $this->file ."." .date("Ymd"). ".txt";
	}
	
	function getPrefix($lineNumber){
		$this->lineNumber = $lineNumber;
		return $this->prefix = date($this->logFormat). "%% ". $this->lineNumber.  ': ';
	}
	
	function INFO($str){
		
		$trace = debug_backtrace();
		$lineNumber = $trace[0]['line'];
		$data = $this->getPrefix($lineNumber). $str. "\n";
		if (true == $this->saveLogs) {
			error_log($data, 3, $this->getFileName());
		}
		return true;
				
	}
	
	function log($str) {
		
		$trace = debug_backtrace();
		$lineNumber = $trace[0]['line'];
		$data = $this->getPrefix($lineNumber). $str. "\n";
		if (true == $this->saveLogs) {
			error_log($data, 3, $this->getFileName());
		}
		return true;
		
	}
	
	function start(){
		$text = "{$this->file} - START at ". date('Y-m-d H:i:s A');
		$this->INFO("{$this->newLine}############### {$text} ###############");
	}
	
	function end(){
		$text = "{$this->file} - END at ". date('Y-m-d H:i:s A');
		$this->INFO("{$this->newLine}############### {$text} ###############");
	}
	
	function newLine($newLine="\n\r"){
		$newLine = "";
		$this->INFO($newLine);
	}
	
}

class LoggingCron extends Logging{
	
	function __construct(){
		parent::__construct('CRON');
		$this->start();
	}
	
	
}
