<?php
	Route::group(array('before' => 'auth'), function(){
	
	# Globalconfig
	Route::get('globalconfig','GlobalconfigController@index');
	Route::get('cmsimage','GlobalconfigController@imageupload');
	Route::post('/upload/image','GlobalconfigController@postUpload');
	# ACL
	Route::resource('acl','AclController');
	Route::resource('manage-acl/{user_id}/','AclController@manage_acl');
	Route::resource('edit_acl','AclController@edit_acl');
	Route::get('remove-admin/{id}', 'AclController@set_to_customer');
	Route::post('update-access', 'AclController@update');

	# TODO: Chumper - only for testing DataTable
	Route::get('api/chumper', array(
		'as'=>'api.chumper', 
		'uses'=>'ChumperController@get_dashboard_Datatable'
	));
	Route::resource('chumper','ChumperController@index');
	
	
	# Dashboard
	/************
	  * Admin
	  ************/
	 # Dashboard
	 // Route::get('api/dashboard', array(
	  // 'as'=>'api.dashboard', 
	  // 'uses'=>'DashboardController@get_dashboard_Datatable'
	 // ));
	 // Route::resource('admin/','DashboardController');
	 #Route::resource('admin/dashboard','DashboardController@index');
	 
	
	Route::get('admin/api/dashboard', array(
		'as'=>'api.dashboard', 
		'uses'=>'DashboardController@getDatatable'
	));
	Route::resource('admin/dashboard','DashboardController@topViewProduct');
	
	Route::get('admin/dashboard','DashboardController@index');
	Route::get('admin/best_seller_category/{id}','DashboardController@sellerByCategory');
	Route::post('admin/reg_users','DashboardController@reg_users');
	Route::post('admin/sales','DashboardController@sales');
	Route::post('admin/daily_margin','DashboardController@dailyMargin');
	Route::post('admin/sold_item','DashboardController@soldItem');
	Route::post('admin/total_revenue','DashboardController@totalRevenue');
	
	 
	 /************
	  * Partner
	  ************/
	 # Dashboard
	 Route::get('api/dashboard', array(
	  'as'=>'api.dashboard', 
	  'uses'=>'PartnerDashboardController@get_dashboard_Datatable'
	 ));
	Route::resource('partner/','PartnerDashboardController');
	#Route::resource('partner/dashboard','PartnerDashboardController@index');
	Route::get('partner/dashboard','PartnerDashboardController@index');
	
	# Deals
	Route::get('partner/api/deals', array(
		'as'=>'api.deals', 
		'uses'=>'DealsController@getDatatable'
	));
	Route::resource('partner/deals','DealsController');
	#getteambycategory
	Route::get('admin/getteambycategory/{id}','DealsController@getteambycategory');
	#multiteambyteamid
	Route::get('admin/multiteambyteamid/{id}','DealsController@multiteambyteamid');
	
	
	# Coupon
	Route::get('partner/api/coupons', array(
		'as'=>'api.coupons', 
		'uses'=>'CouponController@getDatatable'
	));
	Route::resource('partner/coupons','CouponController');
	
	/**
	* Partner
	*/
	Route::resource('partner/edit-update-profile','UsersController@updateuser');
	Route::resource('partner/edit-profile','UsersController@editProfile');
	Route::resource('partner/edit-password','UsersController@editPassword');
	Route::resource('partner/edit-update-password','UsersController@updatePassword');
	Route::resource('partner/user-setting','UsersController@userSetting');
	Route::post('partner/user-update-setting','UsersController@updateSetting');
	/**
	* Admin
	*/
	Route::resource('admin/edit-update-profile','UsersController@updateuser');
	Route::resource('admin/edit-profile','UsersController@editProfile');
	Route::resource('admin/edit-password','UsersController@editPassword');
	Route::resource('admin/edit-update-password','UsersController@updatePassword');
	Route::resource('admin/user-setting','UsersController@userSetting');
	Route::post('admin/user-update-setting','UsersController@updateSetting');

	# Email configuration
	Route::get('admin/api/emailConfiguration', array(
		'as'=>'api.emailConfiguration', 
		'uses'=>'EmailConfigurationController@getDatatable'
	));
	Route::resource('admin/emailconfiguration','EmailConfigurationController');
	
	#SupplierEmail
	Route::get('admin/api/supplierEmail', array(
		'as'=>'api.supplierEmail', 
		'uses'=>'SupplierEmailController@getDatatable'
	));
	Route::resource('admin/supplieremail','SupplierEmailController');
	
	#Warehouse
	Route::get('admin/api/warehouseSection', array(
		'as'=>'api.warehouseSection', 
		'uses'=>'WarehouseSectionController@getDatatable'
	));
	
	Route::resource('admin/warehousesection/data','WarehouseSectionController@index');
	Route::resource('admin/warehousesection','WarehouseSectionController');
	
	
	#Warehouse Create
	Route::get('admin/warehousesectioncreate','WarehouseSectionController@create');
	Route::post('admin/warehousesectionsave','WarehouseSectionController@saveandupdate');
	#Route::get('warehousesectionadd/{id}','WarehouseSectionController@create');
	
	#Warehouse Update
	Route::get('admin/warehousesectionupdate/{id}','WarehouseSectionController@update');
	Route::post('admin/warehousesectionsave/{id}','WarehouseSectionController@saveandupdate');
	
	#Warehouse Delete
	Route::get('admin/warehousesectiondelete/{id}','WarehouseSectionController@delete');
	
	#Warehouse Move form
	Route::resource('admin/warehousesectionmoveform','WarehouseSectionController@moveform');
	#Warehouse Move section
	Route::resource('admin/warehousesectionmove','WarehouseSectionController@move');
	
	
	#stock assign
	Route::resource('admin/warehousesectionassignstock','WarehouseSectionController@assignstock');
	
	# get and save team
	Route::resource('admin/getandsaveteambysection','WarehouseSectionController@getandsaveteambysection');
	#delete team
	Route::resource('admin/deleteteam','WarehouseSectionController@deleteteam');
	
	#Assign stock data
	Route::resource('admin/getassignstocklist','WarehouseSectionController@assignstocklist');
	Route::resource('admin/getassignstockdata','WarehouseSectionController@assignstockdetail');
	
	#Coupon validator
	Route::resource('couponvalidatorform','CouponController@couponValidatorForm');
	Route::resource('partner/coupon/couponvalidator','CouponController@couponValidator');
	
	#QRCODE
	Route::get('partner/qrcode', 'CouponController@qrCode');
	
	#Newsletter
	Route::get('admin/api/newsletter', array(
		'as'=>'api.newsletter', 
		'uses'=>'NewsletterController@getDatatable'
	));
	Route::resource('admin/newsletter','NewsletterController');
	#Newsletter Create
	Route::get('admin/newsletter/create','NewsletterController@create');
	Route::post('admin/newsletter/save','NewsletterController@saveandupdate');
	
	#Newsletter Edit
	Route::get('admin/newsletter_edit/{id}','NewsletterController@update');
	
	#Newsletter Get Deal By Category
	Route::get('admin/getdealbycategory','NewsletterController@getAndAddDealByCategory');
	
	#DELETE Newsletter
	Route::get('admin/newsletter_delete/{id}','NewsletterController@deleteNewsletter');
	
	#EXPORT Newsletter
	Route::get('admin/newsletter_export/{id}','NewsletterController@export');
	
	#PREVIEW NEWSLETTER
	Route::get('admin/newsletter_preview','NewsletterController@previewNewsLetter');
	
	#PREVIEW NEWSLETTER TEST EMAIL
	Route::post('admin/newsletter_sendEmail','NewsletterController@sendNewsletterEmail');
	
	
	#ORDER EDIT
	 Route::get('admin/api/orderlist', array(
		'as'=>'api.orderlist', 
		'uses'=>'OrderController@getDatatable'
	)); 
	
	Route::resource('admin/orderlist','OrderController@index');
	
	
	#order refund form
	Route::resource('admin/refundform','OrderController@refundForm');
	
	#full Order refund
	Route::resource('admin/fullorderrefundform','OrderController@fullOrderRefundForm');
	#full order refund
	Route::get('admin/fullorderrefund','OrderController@fullOrderRefund');
	
	#order refund
	Route::get('admin/refund','OrderController@refund');
	
	#order shipping method Edit
	Route::get('admin/delivery_option_edit/{id}','OrderController@shippingOptionUpdate');
	
	#order shipping method submit
	Route::post('admin/deliveryOptionEditSubmit','OrderController@shippingOptionSubmit');
	
	
	#Update Delivery Address
	Route::get('admin/update_delivery_address/{id}','OrderController@updateShippingAddress');
	
	#oUpdate Delivery Address  submit
	Route::post('admin/deliveryAddressSubmit','OrderController@deliveryAddressSubmit');
	
	
	#order Deal Option Edit
	Route::get('admin/deal_option_edit/{id}','OrderController@dealOptionUpdate');
	
	#order Deal Option submit
	Route::post('admin/dealOptionSubmit','OrderController@dealOptionSubmit');
	
	
	/**
	* TRIGGER ROUTE START
	*/
	
	#TRIGGER SETTING
	# Trigger Event List
	Route::get('admin/api/trigger', array(
		'as'=>'api.trigger', 
		'uses'=>'TriggerController@getDatatable'
	));
	Route::resource('admin/trigger','TriggerController');
	
	# Trigger Action List
	Route::get('admin/api/triggerAction', array(
		'as'=>'api.triggerAction', 
		'uses'=>'TriggerController@triggerActionDatatable'
	));
	Route::resource('admin/triggerAction','TriggerController@triggerAction');
	
	//ADD ACTION
	Route::get('admin/addNewAction','TriggerController@addNewAction');
	
	//ADD ACTION SUBMIT
	Route::post('admin/addNewActionSubmit','TriggerController@addNewActionSubmit');
	
	//UPDATE ACTION
	Route::get('admin/updateNewAction/{id}','TriggerController@updateNewAction');
	
	//UPDATE ACTION SUBMIT
	Route::post('admin/updateActionSubmit','TriggerController@updateActionSubmit');
	
	//ACTION VIEW
	Route::get('admin/actionView/{id}','TriggerController@actionView');
	
	//DELETE TRIGGER ACTION
	Route::get('admin/deleteTriggerAction/{id}','TriggerController@deleteTriggerAction');
	
	//ADD EVENT ON TRIGGER
	Route::get('admin/addevent','TriggerController@addEvent');
	
	//ADD EVENT ON TRIGGER FORM SUBMIT
	Route::post('admin/addTriggerSubmit','TriggerController@addTriggerSubmit');
	
	//ADD EVENT ON TRIGGER
	Route::get('admin/updateEvent/{id}','TriggerController@updateEvent');
	
	//ADD EVENT ON TRIGGER FORM SUBMIT
	Route::post('admin/updateEventSubmit','TriggerController@updateEventSubmit');
	
	//DELETE TRIGGER EVENT
	Route::get('admin/deleteTriggerEvent/{id}','TriggerController@deleteTriggerEvent');
	
	//TRIGGER EVENT VIEW
	Route::get('admin/eventView/{id}','TriggerController@eventView');
	
	
	//TRIGGER TEMPLATE PATTERN
	Route::get('admin/templatePattern','TriggerController@templatePattern');

	//SAMPLE EMAIL 
	Route::get('admin/sendSampleEmail/{id}','TriggerController@sendSampleEmail');
	Route::post('admin/triggerEmailSend','TriggerController@triggerEmailSend');
	
	/**
	* TRIGGER ROUTE END
	*/
	
	/**
	* DASHBOARD CHART
	*/
	Route::get('admin/get_revenue_chart','DashboardController@get_revenue_chart');
	Route::get('admin/get_users_chart','DashboardController@get_users_chart');
	Route::get('admin/get_sales_chart','DashboardController@get_sales_chart');
	Route::get('admin/get_margin_chart','DashboardController@get_margin_chart');
	
	
	# Supplier Email List
	/*Route::get('admin/api/supplieremail', array(
		'as'=>'api.supplieremail', 
		'uses'=>'SupplierEmailController@getDatatable'
	));
	Route::resource('admin/supplieremail','SupplierEmailController@index');*/
	
	
}); // End Auth Route

/*
* This Route Handling the Login  Page
*/
Route::get('/', array('uses' => 'AdminController@explicitlogin'));
Route::get('login', array('uses' => 'AdminController@explicitlogin'));
Route::get('admin', array('uses' => 'AdminController@explicitlogin'));


Route::get('admin/login', array('uses' => 'AdminController@showLogin'));
Route::post('admin/login', array('uses' => 'AdminController@dologin'));
Route::get('admin/logout', array('uses' => 'AdminController@doLogout'));

Route::get('partner/login', array('uses' => 'AdminController@showLogin'));
Route::post('partner/login', array('uses' => 'AdminController@dologin'));
Route::get('partner/logout', array('uses' => 'AdminController@doLogout'));


/* End Login Route */

Route::any("/request", [
 "as" => "user/request",
 "uses" => "UsersController@request"
]);
 
Route::any("/reset/{token}", [
 "as" => "user/reset",
 "uses" => "UsersController@reset"
]);

Route::any("/request", [
  "as"   => "user/request",
  "uses" => "UsersController@request"
]);
 
Route::any("/reset/{token}", [
  "as"   => "user/reset",
  "uses" => "UsersController@reset"
]);


# Table data migration
// Route::resource('migraterooms', 'MigratetblController@rooms');
Route::resource('migrate_event_counries', 'MigratetblController@event_counries');
Route::resource('migrate_hotel_counries', 'MigratetblController@hotel_counries');

App::missing(function($exception) {
	return Response::view('errors.404');
});

// Display all SQL executed in Eloquent
/*Event::listen('illuminate.query', function($query)
{
    var_debug($query);
});*/