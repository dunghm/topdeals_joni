<?php
##########################################################
# @author:	Muhammad Sajid
# @desc:	Common functions, available for every request
##########################################################
function last_query(){
	$queries = DB::getQueryLog();
	$last_query = end($queries);
	var_debug('$last_query=>');
	var_debug($last_query);
	exit;
}

/*
 * @name:	var_debug
 * @author: Muhammad Sajid
 */
function var_debug($arr, $return=false) {

	if ($return == false) {
		echo '<pre>';
	    print_r($arr, $return);
	    echo '</pre>';
	} else {
		$var = '<pre>';
	    $var.=print_r($arr, $return);
	    $var.= '</pre>';
	    return $var;
	}
}

	function GetRemoteIp($default='127.0.0.1')
    {
		$ip = $default;
        
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		
        return $ip;
    }
	
	
	//GET CURRENT ROL
	function getCurrentUserRole(){
		
		//IF SESSION MENTAIN THEN REDIRECTED TO ADMIN/PARTNER DASHBOARD
		$userType = "partner";
		if(Session::has('user_type')){
			$userTypeSes	=	Session::get('user_type');
			if(!empty($userTypeSes)){
				$userType = $userTypeSes;
			}
		}
		
		return $userType;
	}
	
	function do_admin_logging(){
		return 1;
	}
	
	
	 function generateOption($a=array(), $v=null, $all=null)
    {
        $option = null;
        if ( $all ){
            $selected = ($v) ? null : 'selected';
            $option .= "<option value='' $selected>".strip_tags($all)."</option>";
        }

        $v = explode(',', $v);
        settype($v, 'array');
		if(!empty($a)){
			foreach( $a AS $key=>$value )
			{ 
				$key = strval($value->id);
				$value = strval($value->name); 
				
				$selected = in_array($key, $v) ? 'selected' : null;
				$option .= "<option value='{$key}' {$selected}>".strip_tags($value)."</option>";
			}
		}
        return $option;
    }
	
	//GENERATE TEAM IMAGE
	function team_image($image=null, $index=false) {
		if (!$image) return null;
		if ($index) {
			$path = WWW_ROOT . '/static/' . $image;
			$image = preg_replace('#(\d+)\.(\w+)$#', "\\1_index.\\2", $image); 
			$dest = WWW_ROOT . '/static/' . $image;
			if (!file_exists($dest) && file_exists($path) ) {
				//Image::Convert($path, $dest, 200, 120, Image::MODE_SCALE);
				//Image::Convert($path, $dest, 348, 141, Image::MODE_SCALE);
						Image::Convert($path, $dest, 456, 300, Image::MODE_SCALE);
			}
		}
			return TOPDEAL_URL_LINK . '/static/' .$image;
	}

	function moneyit($k, $frontend = false) {
		if($frontend == true){
			if(is_decimal( $k )){
				return rtrim(rtrim(sprintf('%.2f',$k)), '.');
			}
			else{
				return floor($k).'.-';
			}
		}
		else{
			return rtrim(rtrim(sprintf('%.2f',$k), '0'), '.');
		}
	}
	function is_decimal( $val ){
		return is_numeric( $val ) && floor( $val ) != $val;
	}
	
	
	 function GetTeamUrl(&$team, $primary = false, $absolute=true)
	{
		if ( $absolute ){
			$url = str_replace("tdadmin","", URL::to('/'));
			$url = preg_replace('{/$}', '', $url);
		}
		else{
			$url = "";
		}
			
		if ( strlen(trim($team->seo_title)) > 0 )
		{
			$category = Team::getCategoryById($team->group_id);
			if ( $primary )
				$url .= "/deal_du_jour/";
			else
				$url .= "/deals/";
			
			$url .= strtolower($category[0]->name)."/{$team->seo_title}";
		}
		else
		{
			$url .= "/team.php?id={$team->id}";
			
			if ( $primary )
				$url .= "&primary=1";
		}
			
		return $url;
	}
	
	//SEND EMAIL
	
	/**
	* email_manager send mail function
	**/
	function email_manager_sendmail($to, $subject, $body){
		
		
		
		$headers = array(
				"Mime-Version: 1.0",
				"Content-Type: text/html; charset=charset=UTF-8",
				//"Content-Transfer-Encoding: base64",
				//"X-Mailer: ZTMailer/40b0e624bba09a0b598dd24da9466d49/1.0",
				"From: TopDeal.ch <commande@topdeal.ch>",
				"Reply-To: TopDeal.ch <commande@topdeal.ch>",
				);
				
		// if ($bcc) { 
			// $bcc = join(', ', $bcc);
			// $headers[] = "Bcc: {$bcc}";
		// }
		
		$headers = join("\r\n", $headers);
		//echo nl2br($body);	
		//$body = str_replace('\n', '<br/>', $body);
		// $body= wordwrap($body);
		// $body= str_replace("\n", "\r\n", $body);

		return mail($to,  $subject, $body ,$headers );
	}

	//DELIVERY STATUS ARRAY
	function deliveryStatusArray(){
		$delivery_status = array(
				1 => 'En cours de traitement',
				ShipStat_InPrep => 'En cours de pr&#233;paration',
				2 => 'Disponible',
				3 => 'Envoy&#233;',
				ShipStat_PickedUp => 'article retir&#233;'
		);
		
		return $delivery_status;
	}
	
	//OPTION DELIVERY ARRAY
	function optionDelivery(){
		
		$option_delivery = array(
							'express' => 'express',
							'coupon' => 'coupon',
							'pickup' => 'self delivery',
							'express_pickup' => 'express or self delivery'
						);
						
		return $option_delivery;
	}
	
	
##############################################function order refund ###################################################	
#update deal options quantity and stcok
function dealOptionQuantityUpdate($optionId=0,$refundQuantity=0,$orderItemDelivery='',$orderItemDeliveryStatus='')
{
	
	$teamMultiUpdated = true;
	$teamMulti = Team::getMultiTeamById($optionId);
	$now_number =0;
	if(!empty($teamMulti))
	{
		$teamMultiData = $teamMulti[0];
		
		if($teamMultiData->now_number)
		{
			$now_number = $teamMultiData->now_number;
		}
	}
		
	
	$fieldsWithOptionValue = '';
	#update now_number
	if($now_number>=$refundQuantity)
	{
		$fieldsWithOptionValue.= " now_number = now_number-'$refundQuantity' ";
	}
	
	#Deduct Stock                      
	if( $orderItemDelivery != 'coupon' && $orderItemDeliveryStatus != ShipStat_Invalid && $orderItemDeliveryStatus != ShipStat_Waiting  )                           
	{
	   if(!empty($fieldsWithOptionValue))
	   {
		 $fieldsWithOptionValue.= " , ";
	   }
	   
	   $fieldsWithOptionValue.= " stock_count = stock_count + $refundQuantity ";
	} 
	
	#update team multi quantity
	if($fieldsWithOptionValue)
	{
		$teamMultiUpdated 		= Order::updateQuantity('team_multi',$optionId,$fieldsWithOptionValue);
	}
	return $teamMultiUpdated;

}


#update deal quantity and stcok
function dealQuantityUpdate($teamId=0,$refundQuantity=0,$orderItemDelivery='',$orderItemDeliveryStatus='')
{
	$teamUpdated =true;
	$teamState =false;
	$now_number  =0;
	$team = Team::getTeamById($teamId);
	if(!empty($team))
	{
		$teamData = $team[0];
		
		if($teamData->state)
		{
			$teamState = $teamData->state;
		}
		
		if($teamData->now_number)
		{
			$now_number = $teamData->now_number;
		}
		
		if($teamState != 'failure')
		{
			$fieldsWithOptionValue = '';
			#update now_number
			if($now_number>=$refundQuantity)
			{
				$fieldsWithOptionValue.= " now_number = now_number-'$refundQuantity' ";
			}
			
			
			#Deduct Stock                      
			if( $orderItemDelivery != 'coupon' && $orderItemDeliveryStatus != ShipStat_Invalid && $orderItemDeliveryStatus != ShipStat_Waiting  )                           
			{
			   if(!empty($fieldsWithOptionValue))
			   {
				 $fieldsWithOptionValue.= " , ";
			   }
			   
			   $fieldsWithOptionValue.= " stock_count = stock_count + $refundQuantity ";
			} 
			
			if($fieldsWithOptionValue)
			{
				#update team multi quantity
				$teamUpdated 		= Order::updateQuantity('team',$teamId,$fieldsWithOptionValue);
			}
		}
	}
	
	return $teamUpdated;
}


#### update order item table#####
function orderItemUpdate($refundOrderItemId=0,$quantity=0,$refundQuantity=0,$totalRefund=0,$orderQuantity=0,$orderCard=0,$totalCashBackAmount=0)
{
	$orderId =$orderOrigin=$price=$fare=0;
	#get order id by order item id
	$orderItems 		= OrderItems::getOrderItemsById($refundOrderItemId);
			
	if(!empty($orderItems))
	{
		
		$orderItem = $orderItems[0];
		
		#orderId
		if($orderItem->order_id)
		{
			$orderId = $orderItem->order_id;
		}
		
		#price
		if($orderItem->price)
		{
			$price = $orderItem->price;
		}
		
		
		#get order detail
		$orders 		= Order::getOrderById($orderId);
		#orders
		
		if(!empty($orders))
		{
			$order = $orders[0];
			
			#fare
			if($order->fare)
			{
				$fare = $order->fare;
			}
			
			#orderOrigin
			if($order->origin)
			{
				$orderOrigin = $order->origin;
			}
		}
	}	
	
	
	
	$remaining =  $quantity -$refundQuantity;
	if($remaining==0)
	{
		#delete if items quantity total refund
		$orderItems = OrderItems::deleteOrderItem($refundOrderItemId);
	}
	else
	{
			#add promocode amout in total refund for order item value updatation.
			$promoCardAmount = getPromoCardAmount($orderCard,$orderQuantity,$refundQuantity,$orderOrigin,$fare,$price);
			if($promoCardAmount>0)
			{
				$totalRefund = $totalRefund+$promoCardAmount;
			}
			
			#update order item table
			$fieldsWithOrderItemValue = " quantity =quantity-$refundQuantity, total =total-$totalRefund, cashback_amount=cashback_amount-$totalCashBackAmount ";
			$orderItems = Order::updateQuantity('order_item',$refundOrderItemId,$fieldsWithOrderItemValue);
	}
	return true;
}

#####update order quantity ######

function updateOrderQuantity($orderId=0,$totalRefund=0,$refundQuantity=0,$orderfare=0,$orderItemPrice=0,$callFrom='')
{
	#count order items by order id 
	$fieldsWithValue = '';
	
	if($callFrom=="fullOrderRefund")
	{
		$fieldsWithValue.= " state = 'refund', ";
	}
	else
	{
		$totalcount = orderItemCountByOrderId($orderId);
		#if order item count is zero then mark refund
		if($totalcount ==0)
		{
			$fieldsWithValue.= " state = 'refund', ";
		}
	}
	
	#####Get Max fare#######
	$maxFare = 0;
	$getMaxFareArray = Order::getMaxFareByOrderId($orderId);
	if(!empty($getMaxFareArray))
	{
		$getMaxFare =$getMaxFareArray[0];
		
		if(!empty($getMaxFare->fare))
		{
			$maxFare 		=	$getMaxFare->fare;
		}	
	}
	
	#check order money field is greater than order credit card
	$orderMoney =$orderCredit=$orderCard=$orderQuantity=$card=$orderOrigin=$fare=0;
	$orders 		= Order::getOrderById($orderId);
	#orders	
	if(!empty($orders))
	{
		$order = $orders[0];
		
		#orderMoney
		if($order->money)
		{
			$orderMoney = $order->money;
		}
		
		if($order->credit)
		{
			$orderCredit = $order->credit;
		}
		
		
		if($order->card)
		{
			$orderCard = $order->card;
		}
		
		#orderQuantity
		if($order->quantity)
		{
			$orderQuantity = $order->quantity;
		}
		
		#orderOrigin
		if($order->origin)
		{
			$orderOrigin = $order->origin;
		}
		
		#fare
		if($order->fare)
		{
			$fare = $order->fare;
		}
		
	}
	
	###
	$credit = $orderCredit;
	#$card 	= $orderCard;
	$money = $orderMoney-$totalRefund;
	
	if($money<0)
	{
		$credit = $money+$orderCredit;
		$money = 0;
		
		/* if($credit<0)
		{
			$card = $credit+$orderCard;
			$credit =0;
			
			if($card<0)
			{
				$card = 0;
			}
		} */ 
		
	}
	$promoCardAmount = getPromoCardAmount($orderCard,$orderQuantity,$refundQuantity,$orderOrigin,$fare,$orderItemPrice);
	if($orderCard>$promoCardAmount)
	{
		$card = $orderCard-$promoCardAmount;
	}
	
	
	#check order money
	
	$fieldsWithValue.= " money =$money, credit = $credit, card=$card, ";

	#######Order table update #########
	$fieldsWithValue.= " origin =origin-$totalRefund, quantity =quantity-$refundQuantity ,fare = $maxFare ";
	$orders 		= Order::updateQuantity('order',$orderId,$fieldsWithValue);
	return true;
} 

function orderItemCountByOrderId($orderId=0)
{
	$totalcount =0;
	#######count order items#########
	$orderItemsCount = OrderItems:: getOrderItemCountByOrderId($orderId);
	if(is_array($orderItemsCount) && count($orderItemsCount) >0)
	{
		$orderItemsCount 	=	$orderItemsCount[0];
		
		if(!empty($orderItemsCount->Totalcount))
		{
			$totalcount 		=	$orderItemsCount->Totalcount;
		}	
	}
	
	return $totalcount;
}


function insertOrderRefundEntries($orderId=0,$orderItemId=0,$refundQuantity=0,$orderfare=0,$totalRefund=0,$totalCashBackAmount=0)
{
	#order items
	$orderItems 		= OrderItems::getOrderItemsById($orderItemId);
	
	if(!empty($orderItems))
	{
		
		$orderItem = $orderItems[0];
		
		$totalRefundWithCashback = $totalRefund - $totalCashBackAmount;	
		#insert into order refuded table
		 $tableName = "refunded_order_item";
		 $params = array(
							'order_id' 						=> $orderId, //
							'order_item_id' 				=> $orderItemId, //
							'team_id' 						=> $orderItem->team_id,
							'option_id' 					=> $orderItem->option_id,
							'delivery' 						=> $orderItem->delivery,
							'quantity' 						=> $refundQuantity, //
							'price' 						=> $orderItem->price,
							'fare' 							=> $orderfare, //
							'total' 						=> $totalRefundWithCashback, //
							'cashback_amount'				=> $totalCashBackAmount,
							'remark' 						=>$orderItem->remark,
							'mode' 							=>$orderItem->mode,
							'first_name' 					=>$orderItem->first_name,
							'last_name' 					=>$orderItem->last_name,
							'email' 						=>$orderItem->email,
							'gender' 						=>$orderItem->gender,
							'mobile' 						=>$orderItem->mobile,
							'address' 						=>$orderItem->address,
							'address2' 						=>$orderItem->address2,
							'zipcode' 						=>$orderItem->zipcode,
							'region' 						=>$orderItem->region,
							'country' 						=>$orderItem->country,
							'delivery_time' 				=>$orderItem->delivery_time,
							'delivery_status' 				=>$orderItem->delivery_status,
							'shipment_identcode' 			=>$orderItem->shipment_identcode,
							'shipment_status' 				=>$orderItem->shipment_status,
							'shipment_status_updated_on' 	=>$orderItem->shipment_status_updated_on,
							'shipment_post_service' 		=>$orderItem->shipment_post_service,
							'deleted_by' 					=>Auth::user()->id,
							'deleted_on' 					=>time()
					);

		$Inserted = Order::insertRecord($tableName,$params);
	}
	return true;
	
}


function getFareDifference($orderId=0,$orderItemId=0,$orderFare=0)
{
	##########for multiple fare#########
	#Get max quantity except this orderitem
	$maxFareExceptThisOrderItem = 0;
	$fareDifference = 0;
	$getMaxFareExceptThisOrderItemArray = Order::getMaxFareByOrderId($orderId,$orderItemId);
	if(!empty($getMaxFareExceptThisOrderItemArray))
	{
		$getMaxFareExceptThisOrderItem =$getMaxFareExceptThisOrderItemArray[0];
		
		if(!empty($getMaxFareExceptThisOrderItem->fare))
		{
			$maxFareExceptThisOrderItem 		=	$getMaxFareExceptThisOrderItem->fare;
			
			###check order fare is greater than maxFareExceptThisOrderItem
			if($orderFare>$maxFareExceptThisOrderItem)
			{
				$fareDifference = $orderFare - $maxFareExceptThisOrderItem;
			}
		}	
	}
	
	
	
	return $fareDifference;
}


function checkOrderHaveMoreExpressItems($orderId=0,$orderItemId=0)
{
	$fare_count =0;
	$OrderItemsByOrderId 		= OrderItems:: getOrderItemsByOrderId($orderId);
	
	if(!empty($OrderItemsByOrderId))
	{
		if(count($OrderItemsByOrderId)>1)
		{
			
			foreach($OrderItemsByOrderId as $oi)
			{
				
				if($oi->delivery == 'express')
				{
					if($oi->id != $orderItemId)
					{
						$fare_count++;
					}
				} 
				
			}
		}
	}
	
	return $fare_count;
}

function getPromoCardAmount($orderCard=0,$orderQuantity=0,$refundQuantity=0,$orderOrigin=0,$orderFare=0,$orderItemPrice=0)
{
	$promoCardAmount =0;
	if($orderCard>0)
	{
		$totalPrice = ($orderCard+$orderOrigin)-$orderFare;
		
		
		$promoPercent = ($orderCard /$totalPrice) *100;
		$promoCardAmount = ($orderItemPrice * $promoPercent)/100;
		//$promoCardAmount = $orderCard /$orderQuantity;
		if($refundQuantity>0)
		{
			$promoCardAmount = $promoCardAmount*$refundQuantity;
		}
	}
	
	return $promoCardAmount;
}

#stock count update by team
function stockUpdateByTeamId($teamId=0)
{
	
	#get team detail
	$teamStockCount = 0;
	$team = Team::getTeamById($teamId);
	if(!empty($team))
	{
		$teamData = $team[0];
		
		if($teamData->stock_count)
		{
			$teamStockCount = $teamData->stock_count;
		}
	}
	
	#check team stock
	if($teamStockCount>0)
	{
		#get order items by order id
		$orderItemsByTeams  = OrderItems::getOrderItemByTeamId($teamId);	
		foreach($orderItemsByTeams as $orderItem)
		{
			$orderItemId				= $orderItem->id;
			$orderItemQuantity			= $orderItem->quantity;
			$orderItemDeliveryStatus 	= $orderItem->delivery_status;
			$orderItemDelivery		 	= $orderItem->delivery;
			
			if($teamStockCount>=$orderItemQuantity)
			{
				$remainingStock = $teamStockCount - $orderItemQuantity;
				if($remainingStock>=0)
				{
					#update team stock with remaining stock                     
					$teamFields= " stock_count = $remainingStock ";
					 
					#update team multi quantity
					$teamUpdated 		= Order::updateQuantity('team',$teamId,$teamFields);
					
					#update order item status with 5 
					$newDeliveryStatus = ShipStat_InPrep;
					if($orderItemDelivery=="pickup")
					{
						$newDeliveryStatus = ShipStat_Available;
					}
					
					$fieldsWithOrderItemValue = " delivery_status = $newDeliveryStatus ";
					$orderItems = Order::updateQuantity('order_item',$orderItemId,$fieldsWithOrderItemValue);
				}
				else
				{
					break;
				}
				
				
			}
			else
			{
				continue;
			}
		}
	}
	return true;
}

#stock count update by team
function stockUpdateByTeamOptionId($teamOptionId=0)
{
	#Option detail	
	$teamOptionStockCount = 0;
	$teamMulti = Team::getMultiTeamInfoById($teamOptionId);	
	if(!empty($teamMulti))
	{
		$teamMultiData = $teamMulti[0];
		if($teamMultiData->stock_count)
		{
			$teamOptionStockCount = $teamMultiData->stock_count;
		}
	}
	
	#check team stock
	if($teamOptionStockCount>0)
	{
		#get order items by order id
		$orderItemsByTeamOptions  = OrderItems::getOrderItemByTeamOptionId($teamOptionId);	
		foreach($orderItemsByTeamOptions as $orderItem)
		{
			$orderItemId				= $orderItem->id;
			$orderItemQuantity			= $orderItem->quantity;
			$orderItemDeliveryStatus 	= $orderItem->delivery_status;
			$orderItemDelivery		 	= $orderItem->delivery;
			
			if($teamOptionStockCount>=$orderItemQuantity)
			{
				$remainingStock = $teamOptionStockCount - $orderItemQuantity;
				
				if($remainingStock>=0)
				{
					#update team stock with remaining stock                     
					$teamOptionFields= " stock_count = $remainingStock ";
					 
					#update team multi quantity
					$teamOptionUpdated 		= Order::updateQuantity('team_multi',$teamOptionId,$teamOptionFields);
					
					
					#update order item status with 5 
					$newDeliveryStatus = ShipStat_InPrep;
					if($orderItemDelivery=="pickup")
					{
						$newDeliveryStatus = ShipStat_Available;
					}
					
					$fieldsWithOrderItemValue = " delivery_status = $newDeliveryStatus ";
					$orderItems = Order::updateQuantity('order_item',$orderItemId,$fieldsWithOrderItemValue);
				}
				else
				{
					break;
				}
				
				
			}
			else
			{
				continue;
			}
		}
	}
	return true;
	
	
}

function stockStatusUpdate($teamId=0,$teamOptionId=0)
{
	if(is_numeric($teamOptionId) && $teamOptionId>0)
	{
		stockUpdateByTeamOptionId($teamOptionId);
	}
	else
	{
		stockUpdateByTeamId($teamId);
	}
	return true;
	
	
}

function getcashBackAmount($cashBackAmount=0,$orderQuantity=0,$refundQuantity=0)
{
	$totalCashBackAmount = 0;
	if($cashBackAmount>0)
	{
		#get single quantity cashback amount from order item
		$totalCashBackAmount = $cashBackAmount /$orderQuantity;
		
		#for multi quantity
		if($refundQuantity>0)
		{
			$totalCashBackAmount = $totalCashBackAmount*$refundQuantity;
		}
	}
	return $totalCashBackAmount;
}

##############################################//function order refund #################################################	

  //ADDITIONAL PAYMENT METHOD
 function additionalPaymentMethod(){
  
  $method_additional_costArr = array(
            'user_balance_cash' => 'User Balance and Cash',
            'cash' => 'Cash',
            'wire' => 'Wire',
            'maestro' => 'Maestro',
           );
           
  return $method_additional_costArr;
 }
 
##########################################order refund email #############################################################
function emailConfiguration($eventKey='')
{
	$value = 0;
	#get value from email_config table
	
	$emailConfigArray = Configuration::getEmailConfigurationByEvent($eventKey);
	
	if(is_array($emailConfigArray) && count($emailConfigArray)>0)
	{
		$emailConfigArray = $emailConfigArray[0];
		if(isset($emailConfigArray->value))
		{
			$value = $emailConfigArray->value;
		}
	}
	
	#value check
	if($value<=0)
	{
		return true;
	}
	else
	{
		return false;
	}
	
}


function fullOrderRefundEmailTemplate($param=array(),$order=array(),$orderItems=array())
{
	#top deal url
	$topDealLink = TOPDEAL_URL_LINK;
	
	#get param 
	$orderId = $orderItemId =$refundQuantity=0;
	$refundPolicy ='';
	
	#get order detail
	$realname =$lastname=$payTime=$orderService='';
	$payId =$origin=$orderCredit=$orderCard=$totalAmount=$orderFare=0;
	
	#$orderItems 	= OrderItems:: getOrderItemsByOrderId($orderId);
	#$orders 		= Order::getOrderById($orderId);
	
	
	#refund policy
	if($param['refundPolicy'])
	{
		$refundPolicy = $param['refundPolicy'];
	}
	

	if(!empty($order))
	{
		#realname
		if($order['realname'])
		{
			$realname = $order['realname'];
		}
		
		#lastname
		if($order['lastname'])
		{
			$lastname = $order['lastname'];
		}
		
		#pay_id
		if($order['payId'])
		{
			$payId = $order['payId'];
		}
		
		#origin
		if($order['origin'])
		{
			$origin = moneyit($order['origin']);
		}
		
		#fare
		 if($order['orderFare'])
		{
			$orderFare = moneyit($order['orderFare']);
		}
		
		#credit
		if($order['orderCredit'])
		{
			$orderCredit = $order['orderCredit'];
		}
		
		#card
		if($order['orderCard'])
		{
			$orderCard = $order['orderCard'];
		}
		
		#service
		if($order['orderService'])
		{
			$orderService = $order['orderService'];
		}
		
		#pay_time
		
		if($order['payTime'])
		{
			$payTime = $order['payTime'];
		}
		
		$totalAmount = moneyit(($origin));
		/* if($origin>$orderCard)
		{
			$totalAmount = moneyit(($origin-$orderCard));
		} */
		
		$total =0;
			
	}
	
	
	$body = <<<BODY
			<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			</head>

			<body>
			<table border="0" cellpadding="0" cellspacing="0" style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 14px; line-height: 130%; width: 100%;">
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" style="width: 580px; padding: 0 10px;" align="center">
					<tr>
						<td align="center">
							<h1 style="margin: 0;"><a href="#"><img src="$topDealLink/static/v4/img/logo.jpg" width="100" height="50" alt="Topdeal" style="vertical-align: middle; border: 0;" /></a></h1>
						</td>
					</tr>
					<tr>
						<td align="center" style="background-color: #ded7cf; height: 80px; vertical-align: middle;">
							<h2 style="margin: 0;"><a href="#"><img src="$topDealLink/static/v4/img/annulation-de-commande.jpg" width="370" height="42" alt="Annulation de commande" style="vertical-align: middle; border: 0;" /></a></h2>
						</td>
					</tr>
					<tr>
						<td style="padding: 15px 40px;">
							<h4 style="margin: 0 0 5px; font-weight: normal; font-size: 14px;">Cher/Ch�re $realname $lastname, </h4>
							Ceci est un accus� de r�ception de votre demande d�annulation de commande suivante: 
						</td>
					</tr>
					
					<tr>
						<td style="padding: 0 40px;">
							<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" align="center">
								<tr>
									<td style="padding: 25px 10px; font-size: 12px; background-color: #f4f2f0;">
										N&deg;  commande:  $payId<br />
										Date de paiment: $payTime
									</td>
								</tr>
								<tr>
									<td style="font-size: 12px; padding: 0 10px; background-color: #f4f2f0;">
										<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" align="center">
											<tr>
												<th style="text-align: left; border-bottom: #7f7f7f solid 1px;">Article</th>
												<th style="text-align: left; width: 130px; padding: 0 10px; border-bottom: #7f7f7f solid 1px;">Livraison</th>
												<th style="text-align: right; width: 80px;  border-bottom: #7f7f7f solid 1px;"> Total (CHF)</th>
											</tr>
											
BODY;
										$sumCashBack = 0;
										foreach($orderItems as $orderItem)
										{
											$orderItemQuantity=$teamId =$optionId=$orderItemTotal=0;
											$teamTitle = $optionSummary = '';
											
											#quantity
											if($orderItem['orderItemQuantity'])
											{
												$orderItemQuantity =$orderItem['orderItemQuantity'];
											}
											
											#total
											if($orderItem['orderItemTotal'])
											{
												$orderItemTotal =moneyit($orderItem['orderItemTotal']);
											}
											
											#TeamId
											if($orderItem['teamId'])
											{
												$teamId =$orderItem['teamId'];
											}
											
											#optionId
											if($orderItem['optionId'])
											{
												$optionId =$orderItem['optionId'];
											}
											#team title
											if($orderItem['teamTitle'])
											{
												$teamTitle =$orderItem['teamTitle'];
											}
											
											#option Summary
											if($orderItem['optionSummary'])
											{
												$optionSummary =$orderItem['optionSummary'];
											}
											
											#cash back AMount
											if($orderItem['cashBack'])
											{
												$cashbackAmount =$orderItem['cashBack'];
												$sumCashBack+= $cashbackAmount;
											}
											#$sumCashBack+= $cashback_amount;
											
											$total += $orderItemTotal;
											
											$body.= <<<BODY
											<tr>
												<td style="text-align: left; padding: 10px 0; border-bottom: #cccccc solid 1px; vertical-align: top;">$orderItemQuantity x $teamTitle
BODY;
												
												if($optionId)
												{
$body.= <<<BODY
												<br /> $optionSummary 
BODY;
												}
$body.= <<<BODY
												<br /></td>
												<td style="text-align: left; padding: 10px; border-bottom: #cccccc solid 1px; vertical-align: top;">													
													<p style="margin: 0;">Retrait chez: <br />
													TopDeal<br />
													Rue du Centre 136<br />
													1025 St-Sulpice
													</p>
												</td>
												<td style="text-align: right; padding: 10px 0; border-bottom: #cccccc solid 1px; vertical-align: top;">$orderItemTotal</td>
											</tr>
BODY;
										}
										
$body.= <<<BODY
									<tr>
												<td style="padding-bottom: 15px">&nbsp;</td>
												<td colspan="2" style="text-align: right; vertical-align: top; padding: 0 0 15px 10px;">
													<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" align="right">
														<tr>												
															<td style="text-align: right; vertical-align: top; padding: 10px 0 0;">
																Total : 
															</td>
															<td style="text-align: right; width: 80px; vertical-align: top; padding: 10px 0 0;">$total</td>
														</tr>
BODY;
														if($orderFare>0)
														{
$body.= <<<BODY
														<tr>												
															<td style="text-align: right; vertical-align: top; border-bottom: #cccccc solid 1px; padding: 0 0 10px;">
																Livraison: 
															</td>
															<td style="text-align: right; vertical-align: top; border-bottom: #cccccc solid 1px; padding: 0 0 10px;">$orderFare</td>
														</tr>
BODY;
														}
														
														/*  if($orderCredit>0)
														{
$body.= <<<BODY
														<tr>												
															<td style="text-align: right; vertical-align: top; padding: 10px 0 0;">
																D�duction de cr�dits: 
															</td>
															<td style="text-align: right; vertical-align: top; padding: 10px 0 0;">- $orderCredit </td>
														</tr>												
BODY;
														} */ 
														if($orderCard>0)
														{
$body.= <<<BODY
														<tr>												
															<td style="text-align: right; vertical-align: top; border-bottom: #7f7f7f solid 1px; padding: 0 0 10px;">
																D�duction de code promo: 
															</td>
															<td style="text-align: right; vertical-align: top; border-bottom: #7f7f7f solid 1px; padding: 0 0 10px;">-$orderCard</td>
														</tr>
BODY;
														}
														if($sumCashBack>0)
														{
															if($totalAmount>$sumCashBack)
															{
																$totalAmount = moneyit($totalAmount- $sumCashBack);
															}
$body.= <<<BODY
														<tr>												
															<td style="text-align: right; vertical-align: top; border-bottom: #7f7f7f solid 1px; padding: 0 0 10px;">
																Credit Back: 
															</td>
															<td style="text-align: right; vertical-align: top; border-bottom: #7f7f7f solid 1px; padding: 0 0 10px;">-$sumCashBack</td>
														</tr>
BODY;
														
														}
														
														
$body.= <<<BODY
														<tr>												
															<th style="text-align: right; vertical-align: top; padding: 10px 0;">
																Total � payer : 
															</th>
															<th style="text-align: right; vertical-align: top; padding: 10px 0;">$totalAmount</th>
														</tr>

													 </table>
												</td>
											</tr>								
										</table>
									</td>
								</tr>												
							</table>
						</td>
					</tr>
					<tr>
						<td style="padding: 25px 40px 0;">
							<p style="margin: 0; font-weight: 700;">
BODY;
	
							if($refundPolicy == 'credit')
							{	
$body.= <<<BODY
							Le montant de CHF $totalAmount a �t� cr�dit� sur votre compte Topdeal
BODY;
							}
							else
							{							
$body.= <<<BODY
							Le montant de CHF $totalAmount a �t� cr�dit� sur carte de cr�dit
BODY;
							}

$body.= <<<BODY
							</p>
							<p>Pour toute question relative � votre commande, n�hesitez pas � prendre contact avec le support TopDeal.ch en mentionnant le num�ro de votre commande � l�adresse suivante: <a style="color: #000000;" href="mailto: support@topdeal.ch">support@topdeal.ch</a>.</p>
						</td>
					</tr>	
					<tr>
						<td style="padding: 0 40px 15px;">
							<p style="margin: 0;">En esp�rant que nous aurons prochainement le plaisir de vous revoir.<br />
							Votre �quipe Topdeal.ch
							</p>
						</td>
					</tr>		
				</table>
			</td>
		</tr>
	</table>
</body>
</html>	
BODY;

	return $body;
}


function fullOrderRefundEmail($to='',$subject='',$body='')
{
	#if value is zero then function email not sent 
	if(emailConfiguration('order_refund'))
	{
		return true;
	}
	
	#$subject = "Annulation de votre commande";
	#$body = fullOrderRefundEmailTemplate($param,$refundedOrders,$refundedOrderItems);
	
	email_manager_sendmail($to, $subject, $body);
}



function singleOrderRefundEmail($to='',$subject='',$body='')
{
	#if value is zero then function email not sent 
	if(emailConfiguration('order_refund'))
	{
		return true;
	}
	
	#$subject = "Order Item Refunded"; #"Passer commande R�f�rence rembours�";
	#$body = singleOrderRefundEmailTemplate($param,$refundedOrders,$refundedOrderItems);
	email_manager_sendmail($to, $subject, $body);
}

function singleOrderRefundEmailTemplate($param=array(),$order=array(),$orderItem=array())
{
	#top deal url
	$topDealLink = TOPDEAL_URL_LINK;
	#get param 
	$orderId = $orderItemId =$refundQuantity=0;
	$refundPolicy ='';
	
	#get order detail
	$realname =$lastname=$payTime=$orderService='';
	$payId =$origin=$orderCredit=$orderCard=$totalAmount=$orderFare=$orderQuantity=0;
	
	$orderItemQuantity=$teamId =$optionId=$orderItemTotal=$totalCashBackAmount=$fare=$price=0;
	$teamTitle = $optionSummary = '';
	
	#refund policy
	if($param['refundPolicy'])
	{
		$refundPolicy = $param['refundPolicy'];
	}
	

	if(!empty($order))
	{
		#realname
		if($order['realname'])
		{
			$realname = $order['realname'];
		}
		
		#lastname
		if($order['lastname'])
		{
			$lastname = $order['lastname'];
		}
		
		#pay_id
		if($order['payId'])
		{
			$payId = $order['payId'];
		}
		
		#origin
		if($order['origin'])
		{
			$origin = moneyit($order['origin']);
		}
		
		#credit
		if($order['orderCredit'])
		{
			$orderCredit = $order['orderCredit'];
		}
		
		#card
		if($order['orderCard'])
		{
			$orderCard = $order['orderCard'];
		}
		
		#card
		if($order['orderQuantity'])
		{
			$orderQuantity = $order['orderQuantity'];
		}
		
		
		#service
		if($order['orderService'])
		{
			$orderService = $order['orderService'];
		}
		
		#pay_time
		
		if($order['payTime'])
		{
			$payTime = $order['payTime'];
		}
		
		#orderFare
		if($order['orderFare'])
		{
			$fare = $order['orderFare'];
		}
		
	}
	
	$body = <<<BODY
			<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			</head>

			<body>
			<table border="0" cellpadding="0" cellspacing="0" style="font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif; font-size: 14px; line-height: 130%; width: 100%;">
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" style="width: 580px; padding: 0 10px;" align="center">
					<tr>
						<td align="center">
							<h1 style="margin: 0;"><a href="#"><img src="$topDealLink/static/v4/img/logo.jpg" width="100" height="50" alt="Topdeal" style="vertical-align: middle; border: 0;" /></a></h1>
						</td>
					</tr>
					<tr>
						<td align="center" style="background-color: #ded7cf; height: 80px; vertical-align: middle;">
							<h2 style="margin: 0;"><a href="#"><img src="$topDealLink/static/v4/img/annulation-de-commande.jpg" width="370" height="42" alt="Annulation de commande" style="vertical-align: middle; border: 0;" /></a></h2>
						</td>
					</tr>
					<tr>
						<td style="padding: 15px 40px;">
							<h4 style="margin: 0 0 5px; font-weight: normal; font-size: 14px;">Cher/Ch�re $realname $lastname, </h4>
							Ceci est un accus� de r�ception de votre demande d�annulation de commande suivante: 
						</td>
					</tr>
					
					<tr>
						<td style="padding: 0 40px;">
							<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" align="center">
								<tr>
									<td style="padding: 25px 10px; font-size: 12px; background-color: #f4f2f0;">
										N&deg;  commande:  $payId<br />
										Date de paiment: $payTime
									</td>
								</tr>
								<tr>
									<td style="font-size: 12px; padding: 0 10px; background-color: #f4f2f0;">
										<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" align="center">
											<tr>
												<th style="text-align: left; border-bottom: #7f7f7f solid 1px;">Article</th>
												<th style="text-align: left; width: 130px; padding: 0 10px; border-bottom: #7f7f7f solid 1px;">Livraison</th>
												<th style="text-align: right; width: 80px;  border-bottom: #7f7f7f solid 1px;"> Total (CHF)</th>
											</tr>
											
BODY;

											#quantity
											if($orderItem['orderItemQuantity'])
											{
												$orderItemQuantity =$orderItem['orderItemQuantity'];
											}
											
											#total
											if($orderItem['orderItemTotal'])
											{
												$orderItemTotal =moneyit($orderItem['orderItemTotal']);
											}
											
											#TeamId
											if($orderItem['teamId'])
											{
												$teamId =$orderItem['teamId'];
											}
											
											#optionId
											if($orderItem['optionId'])
											{
												$optionId =$orderItem['optionId'];
											}
											#team title
											if($orderItem['teamTitle'])
											{
												$teamTitle =$orderItem['teamTitle'];
											}
											
											#option Summary
											if($orderItem['optionSummary'])
											{
												$optionSummary =$orderItem['optionSummary'];
											}
											
											#option Summary
											if($orderItem['orderfare'])
											{
												$orderFare =$orderItem['orderfare'];
											}
											
											#total cash back amount
											if($orderItem['cashBack'])
											{
												$totalCashBackAmount =$orderItem['cashBack'];
											}
											
											#price
											if($orderItem['price'])
											{
												$price =$orderItem['price'];
											}
											
											$promoCode = getPromoCardAmount($orderCard,$orderQuantity,$orderItemQuantity,$origin,$fare,$price);
											
											$total = $orderItemTotal+$promoCode;
											$totalAmount = $orderItemTotal + $orderFare;
											$totalAmount = $totalAmount - $totalCashBackAmount;
											
											$body.= <<<BODY
											<tr>
												<td style="text-align: left; padding: 10px 0; border-bottom: #cccccc solid 1px; vertical-align: top;">$orderItemQuantity x $teamTitle
BODY;
												
												if($optionId)
												{
$body.= <<<BODY
												<br /> $optionSummary 
BODY;
												}
$body.= <<<BODY
												<br /></td>
												<td style="text-align: left; padding: 10px; border-bottom: #cccccc solid 1px; vertical-align: top;">													
													<p style="margin: 0;">Retrait chez: <br />
													TopDeal<br />
													Rue du Centre 136<br />
													1025 St-Sulpice
													</p>
												</td>
												<td style="text-align: right; padding: 10px 0; border-bottom: #cccccc solid 1px; vertical-align: top;">$total</td>
											</tr>
BODY;
										

								
															
$body.= <<<BODY
									<tr>
												<td style="padding-bottom: 15px">&nbsp;</td>
												<td colspan="2" style="text-align: right; vertical-align: top; padding: 0 0 15px 10px;">
													<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" align="right">
														<tr>												
															<td style="text-align: right; vertical-align: top; padding: 10px 0 0;">
																Total : 
															</td>
															<td style="text-align: right; width: 80px; vertical-align: top; padding: 10px 0 0;">$total</td>
														</tr>
BODY;
														if($orderFare>0)
														{
$body.= <<<BODY
														<tr>												
															<td style="text-align: right; vertical-align: top; border-bottom: #cccccc solid 1px; padding: 0 0 10px;">
																Livraison: 
															</td>
															<td style="text-align: right; vertical-align: top; border-bottom: #cccccc solid 1px; padding: 0 0 10px;">$orderFare</td>
														</tr>
BODY;
														}
														
														if($promoCode>0)
														{
$body.= <<<BODY
														<tr>												
															<td style="text-align: right; vertical-align: top; border-bottom: #7f7f7f solid 1px; padding: 0 0 10px;">
																D�duction de code promo: 
															</td>
															<td style="text-align: right; vertical-align: top; border-bottom: #7f7f7f solid 1px; padding: 0 0 10px;">-$promoCode</td>
														</tr>
BODY;
														}
														if($totalCashBackAmount>0)
														{
$body.= <<<BODY
														<tr>												
															<td style="text-align: right; vertical-align: top; border-bottom: #7f7f7f solid 1px; padding: 0 0 10px;">
																Credit Back: 
														</td>
														<td style="text-align: right; vertical-align: top; border-bottom: #7f7f7f solid 1px; padding: 0 0 10px;">-$totalCashBackAmount</td>
														</tr>
BODY;
														}
																											
$body.= <<<BODY
														<tr>												
															<th style="text-align: right; vertical-align: top; padding: 10px 0;">
																Total � payer : 
															</th>
															<th style="text-align: right; vertical-align: top; padding: 10px 0;">$totalAmount</th>
														</tr>

													 </table>
												</td>
											</tr>								
										</table>
									</td>
								</tr>												
							</table>
						</td>
					</tr>
					<tr>
						<td style="padding: 25px 40px 0;">
							<p style="margin: 0; font-weight: 700;">
BODY;

							if($refundPolicy == 'credit')
							{	
$body.= <<<BODY
							Le montant de CHF $totalAmount a �t� cr�dit� sur votre compte Topdeal
BODY;
							}
							else
							{							
$body.= <<<BODY
							Le montant de CHF $totalAmount a �t� cr�dit� sur carte de cr�dit
BODY;
							}

$body.= <<<BODY
							</p>
							<p>Pour toute question relative � votre commande, n�hesitez pas � prendre contact avec le support TopDeal.ch en mentionnant le num�ro de votre commande � l�adresse suivante: <a style="color: #000000;" href="mailto: support@topdeal.ch">support@topdeal.ch</a>.</p>
						</td>
					</tr>	
					<tr>
						<td style="padding: 0 40px 15px;">
							<p style="margin: 0;">En esp�rant que nous aurons prochainement le plaisir de vous revoir.<br />
							Votre �quipe Topdeal.ch
							</p>
						</td>
					</tr>		
				</table>
			</td>
		</tr>
	</table>
</body>
</html>	
BODY;

return $body;

}



##########################################order refund email #############################################################


/***
* TRIGGER PATTER
*/

function templatePatternContent(){
	
	$patternArray	=	array(
								'#USERNAME#' 	 => 'Customer Name',
								'#FULL_NAME#' 	 => 'Customer Full Name',
								'#EMAIL#' 		 => 'Customer Email',
								'#PROMO_CODE#' 	 => 'Promo Code',
							);
	
	return $patternArray;
}

###########################################ware house move section ##########################################################
function validateWareHouseSectionItems($warehouseSectionId=0,$warehouseSectionItemIds=0)
{
	
	$teamId=$optionId=$loopCounter=0;
	$outPut = array();
	$warehouseSectionItemIdsArray= array();
	$message = '';
	$sectionAlreadyExistMessage="Sections is already assigned for below Deal(s) and Option(s) Ids<br/>";
	
	if(!empty($warehouseSectionItemIds))
	{
		#explode comma seprated values on warehouseSectionItemIds
		$explodedItemIdArray = explode(',',$warehouseSectionItemIds);
		
		if(is_array($explodedItemIdArray) && count($explodedItemIdArray)>0)
		{
			foreach($explodedItemIdArray as $warehouseSectionItemId)
			{
				#first get team and option id from warehouse_section_items table 
				$warehouseSectionItems = WarehouseSection::getWarehouseSectionItemById($warehouseSectionItemId);
				
				if(is_array($warehouseSectionItems) && count($warehouseSectionItems)>0)
				{
					$warehouseSectionItem = $warehouseSectionItems[0];
					
					$teamId 	= $warehouseSectionItem->team_id;
					$optionId 	= $warehouseSectionItem->option_id;
					
					#then match team and option with warehouseSectionId on warehouse_section_items table.
					
					#check section is already exist or not
							
					$checkTeamSection = WarehouseSection::checkTeamSection($warehouseSectionId,$teamId,$optionId);
					if(is_array($checkTeamSection) && count($checkTeamSection) >0)
					{
						$checkTeamSection 	=	$checkTeamSection[0];
						
						if(!empty($checkTeamSection->Totalcount))
						{
							$loopCounter++;
							$warehouseSectionItemIdsArray[] = $warehouseSectionItemId;
							$sectionAlreadyExistMessage.="Deal ID =".$teamId.", Option ID=".$optionId."<br/>";
						}
					}
					
				}
				
			}
			
			$outPut['ids'] = $warehouseSectionItemIdsArray;
		
			if($loopCounter>0)
			{
				$message = $sectionAlreadyExistMessage;
				$outPut['message'] = $message;
			}
		
		}
	}	
	
	return $outPut;
}


function arrayDifference($array1=array(),$array2=array())
{
	 $result = 0;
	$diffArray = array_diff($array1,$array2);
	if(is_array($diffArray) && count($diffArray)>0)
	{
		$result = implode(',',$diffArray);
	}
	return $result;
}


###########################################//ware house move section ##########################################################
/*
* Add wildcard to word
*/
function wildcarded($words) 
{
	$words = singleQuoteReplaceWithDoubleQuote($words);
	$arr = preg_split("/[\s,]+/", $words);
	$arr = array_filter($arr);

	$q=false;
	$str="";
	foreach ($arr as $word) { 
	if (strpos($word,"\"")!==False) $q = !$q;
	$str.= $word . ($q ? " " : "* ");
	}
	return $str;
}

/*
* Single quote replace double quote
*/
function singleQuoteReplaceWithDoubleQuote($string='') 
{
	$string = str_replace("'", "''", $string);
	return $string;
}

function getDataFroGraph($monthlySales=array(),$monthArray=array())
{
	#make user array by month
		$monthlySalesArrayByMonth = array();
		if(is_array($monthlySales) && count($monthlySales)>0)
		{
			foreach($monthlySales as $key=>$row)
			{
				$monthlySalesArrayByMonth[$row->month_num]= $row->total;
			}
		}	
		
		#merge array of moth and register user
		$mergedArray = array();
		foreach($monthArray as $monthkey=>$monthvalue)
		{
			 if(!empty($monthlySalesArrayByMonth[$monthkey]))
			 {
				$mergedArray[] = $monthlySalesArrayByMonth[$monthkey];
			 }
			 else
			 {
				  $mergedArray[] = 0;
			 }
			
		}	
		
		$chartData = array();
		$labelKey	=	1; 
		foreach($mergedArray as $key=>$row)
		{
			$chartData[$key][]=$labelKey;
			$chartData[$key][]=$row;
			
			$labelKey++;
		}
		
	return $chartData;	
		
}

function getMonthArray()
{
	$monthArray		 = array( 1=>'Jan',2=>'Feb',3=>'Mar',4=>'Apr',5=>'May',6=>'Jun',7=>'Jul',8=>'Aug',9=>'Sep',10=>'Oct',11=>'Nov',12=>'Dec'); 
	return $monthArray;
}

?>