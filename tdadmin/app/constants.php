<?php
require_once(dirname(dirname(dirname(__FILE__))). '/include/configure/system.php');
$siteUrl = '';
if(!empty($value))
{
	
	#Fixed new admin url issue
	$siteUrl = $value['wwwprefix']; 
	
	# Comment out if we need to add / at the end of base url $siteUrl
	# Currently it will look like 			http://abc.com 
	# and after comment out it will become 	http://abc.com/
	# Also, we can use this code to remove "/" from the end of URL if some how we start getting it from configuration file
	# -----------------------
	# DO NOT COMMENT OUT IT
	# -----------------------
// 	if('/' != substr($siteUrl, -1))
// 		$siteUrl .= '/';
	
}


define('EMAIL_CONF', '');
define('TT_EMAIL', '');
define('VER_CSS', '?ver=24062015_01');
define('VER_JS', '?ver=24062015_01');
define('COMMISSION_LOGS', 'commission-logs/');
define('AUTHORIZE', FALSE);
define('CP', 'cp');
define('TT_SPECIAL', 'nouman+specialevents@nextgeni.com');
define('EMAIL_JEANNE', 'nouman+Jeanne@nextgeni.com');
define('EMAIL_DEMETRA', 'nouman+demetra@nextgeni.com');

define('BASE_PATH', base_path());
define('PORTLET', BASE_PATH.'/assets/portlet/');

//// Image Upload Path ///
define('UPLOAD_IMAGE', 'uploads/');
define('UPLOAD_THUMB', 'uploads/thumb/');
define('amenity', 'assets/amenity/');

//// Image Thumb Sizes //////
define('THUMB', 'thumb_');
define('SMALL', 'small_');
define('MEDIUM', 'medium_');
define('LARGE', 'large_');

define('THUMB_WIDHT', '70');
define('THUMB_HEIGHT', '70');

define('SMALL_WIDHT', '200');
define('SMALL_HEIGHT', '200');

define('MEDIUM_WIDHT', '340');
define('MEDIUM_HEIGHT', '230');

define('LARGE_WIDHT', '800');
define('LARGE_HEIGHT', '800');

define('SECURE_MD5_STR', '@4!@#$%@');
define('TOPDEAL_URL_LINK', $siteUrl);
define('CURRENCY_SYMBOL_CODE', '&#165');

define('EXPIRED','Expired');
define('VALID','Valid');


define('ShipStat_Invalid', 		'0');
define('ShipStat_Waiting', 		'1');
define('ShipStat_Available', 	'2');
define('ShipStat_Delivered', 	'3');
define('ShipStat_PickedUp', 	'4');
define('ShipStat_InPrep', 		'5');


#manage Url
define('TOPDEAL_MANAGE_URL', 		$siteUrl.'/manage/');

define('DIR_ROOT', str_replace('\\','/',dirname(__FILE__)));
define('WWW_ROOT', rtrim(dirname(DIR_ROOT),'/'));

//ORDER LISTING
define('RECORD_PERPAGE', 	20);
define('CURRENCY', 		'CHF');

define('DEFAULT_EXPRESS_SHIPPING', 		9);

define('PAGGINATION_LENGTH', 100);