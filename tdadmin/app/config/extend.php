<?php
 class Admin_ArticleResource extends \BaseController {
/*
|--------------------------------------------------------------------------
| Extend File Configuration
|--------------------------------------------------------------------------
|
*/
 
return array(
 
    /**
     * Static resource files alias configuration
     */
    'webAssets' => array(
        // ...
        'jsAliases'  => array(  // Script alias configuration
            // ...
            'dropzone' => 'https://raw.githubusercontent.com/enyo/dropzone/master/downloads/dropzone.js',
            // ...
        ),
    ),
 
);
 }