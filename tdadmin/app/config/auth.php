<?php
$segment = Request::segment(1);
/*$request_uri = explode('/', $_SERVER['REQUEST_URI']);
$request_uri = $request_uri[3];*/

if ($segment == 'admin')
{
	return array(
		'driver' => 'eloquent',
		'model' => 'User',
		'table' => 'user',
		'reminder' => array(
			'email' => 'emails.auth.reminder',
			'table' => 'password_reminders',
			'expire' => 60,
		),
	);
} elseif ($segment == 'partner') {

	return array(
		'driver' => 'eloquent',
		'model' => 'Partner',
		'table' => 'partner',
		'reminder' => array(
			'email' => 'emails.auth.reminder',
			'table' => 'password_reminders',
			'expire' => 60,
		),
	);
}/* else {
	return array(
		'driver' => 'eloquent',
		'model' => 'Partner',
		'table' => 'partner',
		'reminder' => array(
			'email' => 'emails.auth.reminder',
			'table' => 'password_reminders',
			'expire' => 60,
		),
	);
}*/