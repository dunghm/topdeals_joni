<?php
/**
*@author:Deals
*@desc: *	function index loading dashboard/index and counting customers from the customers table.
*		*	function get_dashboard_Datatable() it is showing customers list with data table
**/
class DealsController extends BaseController {
	
	/*
	 * __construct
	 */
	public function __construct() {
		
	}
	
	/**
	*@author:MAK
	*@desc: it will load index page and including customers
	**/
	public function index()
	{

		$data	=	array();
		$this->layout->title	= trans('localization.DealTitle');
		$this->layout->content	= View::make('deals/index',compact('data'));
	}
	
	public function getDatatable()
	{
		$local = Config::get('app.locale');
		//GET PARTNER ID
		$loginPartnerID	=	Auth::user()->id;
		
		if(!empty($loginPartnerID)&&$loginPartnerID>0){
			
			switch($local){
				case'en':
					$title = "t.title as title";
				break;
				
				case'fr':
					$title = "t.title_fr as title";
				break;
				
				default:
					$title = "t.title as title";
				break;
			}
			
			$order_by = 't.id';
			$sSortDir_0 = 'desc';
			
			//CREATE QUERY FOR GET DATA
			$deals = DB::table('team as t')
						->leftJoin('coupon as cp', 'cp.team_id', '=', 't.id')
						->leftJoin('category as cat', 'cat.id', '=', 't.city_id');
						// ->leftJoin('category as cat', function($join)
                         // {
                             // $join->on('cat.id', '=', 't.city_id');
                             // $join->on('cp.consume','=',DB::raw("'Y'"));
                         // });
			$deals->where("t.partner_id", "=", $loginPartnerID);			
			// $deals->where("cp.consume", "=", 'Y');
			#$deals->whereRaw("t.partner_id = $loginPartnerID");
			#$deals->whereRaw("cp.consume = 'Y' ");
			
			$deals->select($title, 
							"cat.name", 
							"t.begin_time", 
							"t.end_time",
							"t.team_price", 
							"t.market_price",
							//DB::Raw('count(cp.id) AS validated'),
							DB::Raw("(select count(*) from coupon where team_id = t.id AND consume= 'Y') AS validated"),
							DB::Raw("(select sum(oi.quantity) from order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE oi.team_id = t.id AND o.state = 'pay') AS total"),
							"t.id"
							)
					->groupBy('t.id')
					->orderBy($order_by, $sSortDir_0);
			
			// var_debug($deals->toSql());exit();
					
			return Datatable::query($deals)
					->showColumns(array("title", "name", "begin_time","total", "validated", "market_price"))
					->addColumn('title', function($model) {
						
						$topDealLink	=	TOPDEAL_URL_LINK;
						$id	=	$model->id;
						
						return "<a href='$topDealLink/team.php?id=$id' target='_blank'>".$model->title."</a>";
					})
					->addColumn('begin_time', function($model) {
						
						$begin_time	=	date('Y-m-d',$model->begin_time);
						$end_time	=	date('Y-m-d',$model->end_time);
						
						return $begin_time."<br/>".$end_time;
					})
					->addColumn('market_price', function($model) {
						
						$team_price		=	 number_format($model->team_price, 2);
						$market_price	=	number_format($model->market_price, 2);
						
						return CURRENCY_SYMBOL_CODE." ".$team_price."<br/>".CURRENCY_SYMBOL_CODE." ".$market_price;
					})
					
					->searchColumns(array("title", "name", "begin_time","total","validated", "market_price"))
					->orderColumns(array("title", "name", "begin_time", "total", "validated", "market_price"))
					->make();
		}
	}
	
	
	/*
	 * getdeal by category id
	 */

	public function getteambycategory($categoryId=0)
	{
		 $jsData = array();
		 $teamDropDownTpl = '';
		 if (is_numeric($categoryId) && $categoryId > 0) 
		 {
			$teamList =  Team::getTeamByCategoryId($categoryId);
			$teamDropDownTpl = "<option value='0'>- ".trans('warehouse_lang.select')." -</option>";
			
			if(is_array($teamList) && count($teamList)>0)
			{
				$id = 0;
				$title = '';
				foreach ($teamList as $row)
				{
					#id
					if($row->id)
					{
						$id = $row->id;
					}
					
					#title
					if($row->title)
					{
						$title = $row->title;
					}
					
					 $teamDropDownTpl .="<option value=". $id. ">" .$id.' - '.$title . "</option>";
				} 
			}
		 }
		 
		 $jsData['teamDropDownTpl'] = $teamDropDownTpl;

         $jsData = json_encode($jsData);

         echo $jsData;
         exit;
		 
	}

	/*
     * getMultiTeamByTeamId
     */	 
	 
	 function multiteambyteamid($teamId=0)
	 {
		$jsData = array();
		$multiteamDropDownTpl = '';
		
		if (is_numeric($teamId) && $teamId > 0) 
		{
			$multiTeamList =  Team::getMultiTeamByTeamId($teamId);
			$multiteamDropDownTpl = "<option value='0'>- ".trans('warehouse_lang.select')." -</option>";
			
			if(is_array($multiTeamList) && count($multiTeamList)>0)
			{
				$id = 0;
				$title = '';
				foreach ($multiTeamList as $row)
				{
					#id
					if($row->id)
					{
						$id = $row->id;
					}
					
					#title
					if($row->title)
					{
						$title = $row->title;
					}
					
					 $multiteamDropDownTpl .="<option value=". $id. ">" .$id.' - '.$title ."</option>";
				} 
			}
		}
		
		$jsData['multiteamDropDownTpl'] = $multiteamDropDownTpl;

         $jsData = json_encode($jsData);

         echo $jsData;
         exit;
	 }
}