<?php
class SupplierEmailController extends BaseController {
	
	/*
	 * __construct
	 */
	public function __construct() {		
	}
	
	/**
	*@author:Sajid
	*
	**/
	public function index()
	{
		$data = '';
		$this->layout->title	= 'Supplier Email';
		$this->layout->content 	= View::make('supplieremail/index', compact('data'));
	}
	
	/*
	* Ajax Request for server side data
	*/	
	public function getDatatable()
	{
		# call function
		return SupplierEmail::getSupplierEmail(Input::get());
	}
}