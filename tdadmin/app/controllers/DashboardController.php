<?php
/**
*@author:Dashboard
*@desc: *	function index loading dashboard/index and counting customers from the customers table.
*		*	function get_dashboard_Datatable() it is showing customers list with data table
**/
class DashboardController extends BaseController {
	
	/*
	 * __construct
	 */
	public function __construct() {
		
	}
	
	/**
	*@author:Arshad
	*
	**/
	public function index()
	{
		$data = array();
		
		//CURRENT DATE
		$daytime = strtotime(date('Y-m-d'));
		
		/*
		*	GET TOTAL NUMBER OF TODAY SALE
		*/
		
		$todaySale			=	Dashboard::todaySale($daytime);#TODAY PAID ORDER
		$data['todaySale']	=	Dashboard::totalAmountOrderPlaceToday($daytime);
		
		//TODAY TOTAL ORDER 
		$totalPlaceOrder		=	Dashboard::totalOrderPlaceToday($daytime);
		
		//TODAY PAID ORDERS 
		$todayPaidOrders		=	Dashboard::todayPaidOrder($daytime);
		
		//TODAY PAID ORDERS 
		$todayUnPaidOrder		=	Dashboard::todayUnPaidOrder($daytime);
		
		//TODAY SALE PRECENTAGE
		$todaySalePercentage	=	"";
		if($todaySale>0){ 
			
			$calculatePercentage	=	Dashboard::calculatePercentage($todaySale, $totalPlaceOrder);
			$todaySalePercentage	=	$calculatePercentage."%"; 
		}else{
			$todaySalePercentage	= "0%";
		} 
		$data['todaySalePercentage']	=	$todaySalePercentage;
		
		
		/*
		*	DAILY MARGIN
		*/
		
		$dailyMargin			=	Dashboard::dailyMargin($daytime);
		$data['dailyMargin']	=	$dailyMargin;
		//TODAY SALE PRECENTAGE
		//Daily margin in % = Same as above but in %.  Margin / Total Amount Received in Paid Orders 
		$dailyMarginPercentage	=	"";
		if($todaySale>0){ 
			
			$dailyMarginTotal		=	Dashboard::dailyMarginTotal($daytime);
			$calculatePercentage	=	Dashboard::calculatePercentage($dailyMargin, $dailyMarginTotal);
			$dailyMarginPercentage	=	$calculatePercentage."%"; 
		}else{
			$dailyMarginPercentage	= "0%";
		} 
		$data['dailyMarginPercentage']		=	$dailyMarginPercentage;
		
		//TOTAL TODAY ORDER
		$data['totalPlaceOrder']			=	$totalPlaceOrder;
		
		//TODAY CREATED USER
		$data['todayNewUser']				=	Dashboard::todayNewUser($daytime);
		
		//TOP VIEW PRODUCT
		$data['topViewProduct']				=	Dashboard::topViewProduct();
		
		//TOP Purchase PRODUCT
		$data['topPurchasedProduct']		=	Dashboard::topPurchasedProduct($daytime);
		
		//Top Purchased category
		$data['topPurchasedCategory']		=	Dashboard::topPurchasedCategory($daytime);
		
		//Top Buyer
		$data['topBuyer']					=	Dashboard::topBuyer();
		
		/* Top Seller By Category*/
		$getCategoryList					=	Dashboard::getCategoryList();
		$data['getCategoryList']			=	$getCategoryList;
		
		//GET Top Seller by category
		// $selectedCategoyrId		=	0;
		// if(!empty($getCategoryList)&&isset($getCategoryList[0]->id)){
			
			$selectedCategoyrId				=	'all';
			$data['bestSellerByCategory']	=	Dashboard::bestSellerByCategory($selectedCategoyrId);
		// }
		$data['selectedCategoyrId']	=	$selectedCategoyrId;
		
		
		//COLOR ARRAY
		$data['colorArr']		=	array('success','info','important','warning','success');
		
		//CREATE DATA TO RENDER IN CHART
		//GET REVENUE BY WEEK
		$getRevenueByRange		=	Dashboard::getRevenueByRange('12 MONTH','month');
		
		$chartData	=	array();
		$labelArray	=	array();
		if(!empty($getRevenueByRange)){
			$labelKey	=	1;
			foreach($getRevenueByRange as $key=>$getRevenueByRangeRow){
				$labelArray[$labelKey]	   =  date("M,Y",$getRevenueByRangeRow->create_time);
				$chartData[$key][] = $labelKey;
				$chartData[$key][] = $getRevenueByRangeRow->dailyMargin;
				
				$labelKey++;
			}
		}
		
		$total_order		=	Order::count();
		$order_paid			=	Order::ordersPaidCount();
		$order_to_be_paid	=	Order::ordersToBePaidCount();
		$total_team			=	Team::count();
		$active_teams		=	Team::activeTeams();
		$expired_teams		=   Team::expiredTeams();
		$total_reg_users	=	User::regUsers();
		$offlineTopUp 		= 	Dashboard::offlineTopUp();
		$todayTopUp 		= 	Dashboard::todayTopUp();
		$totalTopUp 		= 	Dashboard::totalTopUp();
		
		$orderPaidPercentage	=	"";
		if($order_paid)
		{
			$calculatePercentage	=	Dashboard::calculatePercentage($order_paid, $total_order);
			$orderPaidPercentage	=	$calculatePercentage."%"; 
		}
		else
		{
				$orderPaidPercentage	= "0%";
		}
		
		
		$orderToBePaidPercentage	=	"";
		if($order_to_be_paid)
		{
			$calculatePercentage	=	Dashboard::calculatePercentage($order_to_be_paid, $total_order);
			$orderToBePaidPercentage	=	$calculatePercentage."%"; 
		}
		else
		{
				$orderToBePaidPercentage	= "0%";
		}
		
		
		$activeTeamsPercentage	=	"";
		if($active_teams>0)
		{
			$calculatePercentage	=	Dashboard::calculatePercentage($active_teams, $total_team);
			$activeTeamsPercentage	=	$calculatePercentage."%"; 
		}
		else
		{
				$activeTeamsPercentage	= "0%";
		}
		
		
		$expiredTeamsPercentage	=	"";
		if($expired_teams>0)
		{
			$calculatePercentage	=	Dashboard::calculatePercentage($expired_teams, $total_team);
			$expiredTeamsPercentage	=	$calculatePercentage."%"; 
		}
		else
		{
				$expiredTeamsPercentage	= "0%";
		}
		
		#default year
		$startYear = date('Y')-6;
		$mimmumUserRegisterYear = User::mimmumUserRegisterYear();
		if(is_array($mimmumUserRegisterYear) && count($mimmumUserRegisterYear))
		{
			$mimmumUserRegisterYear = $mimmumUserRegisterYear[0];
			$startYear = $mimmumUserRegisterYear->startYear;
			
		}
		
		$start = date('Y-m-d', strtotime(date('Y-m')." -1 month"));
		$end = date('Y-m-d');
		
		############################################################################
		# get total users
		$total_user = User::count();
		
		# get reg. users by date
		$reg_users	= User::reg_users($start, $end);
		
		# get percentage_user 
		$percentage_user = ($reg_users/$total_user)*100;
		$percentage_user = number_format($percentage_user, 2, '.', '') . '%';
		############################################################################
		
		
		############################################################################
		# get total Orders
		$total_sales = Order::count();
		
		# get slaes by date
		$this_sales	= Order::this_order($start, $end);
		
		# get percentage_user 
		$percentage_sales = ($this_sales/$total_sales)*100;
		$percentage_sales = number_format($percentage_sales, 2, '.', '') . '%';
		############################################################################
		
		
		############################################################################
		# get total dailyMarginDR
		$total_daily_margin = Order::count();
		
		# get margine by date
		$dailyMarginDR	= Order::dailyMarginDR($start, $end);
		
		# get percentage_user 
		$percentage_dailyMarginDR = ($dailyMarginDR/$total_daily_margin)*100;
		$percentage_dailyMarginDR = number_format($percentage_dailyMarginDR, 2, '.', '') . '%';
		############################################################################
		
		
		############################################################################
		# get total soldItemDR
		$total_sold_item = Order::count();
		
		# get margine by date
		$soldItemDR	= Order::soldItemDR($start, $end);
		
		# get percentage_user 
		$percentage_soldItemDR = ($soldItemDR/$total_sold_item)*100;
		$percentage_soldItemDR = number_format($percentage_soldItemDR, 2, '.', '') . '%';
		############################################################################
		
		
		############################################################################		
		# get total Revenue by date
		$totalRevenue	= Order::totalRevenue($start, $end);
		############################################################################
		
		$data['total_top_up']				= $totalTopUp;
		$data['today_top_up']				= $todayTopUp;
		$data['today_paid_order']			= $todayPaidOrders;
		$data['today_unpaid_order']			= $todayUnPaidOrder;
		$data['percentage_si']				= $percentage_soldItemDR;
		$data['total_si']					= $soldItemDR;
		$data['total_rev']					= $totalRevenue;
		$data['percentage_dm']				= $percentage_dailyMarginDR;
		$data['total_dm']					= $dailyMarginDR;
		$data['percentage_sales']			= $percentage_sales;
		$data['total_sales']				= $this_sales;
		$data['percentage_user']			= $percentage_user; 
		$data['reg_users']					= $reg_users;
		$data['chartData']					= $chartData;
		$data['labelArray']					= $labelArray;
		$data['total_order'] 				= $total_order;
		$data['order_paid'] 				= $order_paid;
		$data['orderToBePaidPercentage'] 	= $orderToBePaidPercentage;
		
		$data['order_to_be_paid'] 			= $order_to_be_paid;
		$data['orderToBePaidPercentage'] 	= $orderToBePaidPercentage;
		$data['total_team'] 				= $total_team;
		$data['active_teams'] 				= $active_teams;
		$data['activeTeamsPercentage']		= $activeTeamsPercentage;
		$data['expired_teams'] 				= $expired_teams;
		$data['expiredTeamsPercentage']		= $expiredTeamsPercentage;
		$data['total_reg_users']			= $total_reg_users;
		$data['offlineTopUp']				= $offlineTopUp;
		$data['startYear']					= $startYear;
		
		$this->layout->title	= trans('localization.dashboard');
		$this->layout->content	= View::make('dashboard/index',compact('data'));
	}

	/*
	 * @name:	totalRevenue
	 * @author:	M.Sajid
	 */
	public function totalRevenue(){
		
		$date = Input::get('date');
		$explode = explode(' - ', $date);
		
		$start = date( 'Y-m-d', strtotime($explode[0]) );
		$end = date( 'Y-m-d', strtotime($explode[1]) );
		
		# get total Revenue by date
		$totalRevenue	= Order::totalRevenue($start, $end);
		
		return json_encode( number_format($totalRevenue,2).'|'.'');
	}
	
	/*
	 * @name:	soldItem
	 * @author:	M.Sajid
	 */
	public function soldItem(){
		
		# get total Orders
		$total_order = Order::count();
		
		$date = Input::get('date');
		$explode = explode(' - ', $date);
		
		$start = date( 'Y-m-d', strtotime($explode[0]) );
		$end = date( 'Y-m-d', strtotime($explode[1]) );
		
		$soldItemDR	= Order::soldItemDR($start, $end);
		
		# get percentage_user 
		$percentage_soldItemDR = ($soldItemDR/$total_order)*100;
		$percentage_soldItemDR = number_format($percentage_soldItemDR, 2, '.', '') . '%';

		return json_encode( number_format($soldItemDR,2).'|'.$percentage_soldItemDR);
	}
	
	/*
	 * @name:	dailyMargin
	 * @author:	M.Sajid
	 */
	public function dailyMargin(){
		
		# get total Orders
		$total_order = Order::count();
		
		$date = Input::get('date');
		$explode = explode(' - ', $date);
		
		$start = date( 'Y-m-d', strtotime($explode[0]) );
		$end = date( 'Y-m-d', strtotime($explode[1]) );
		
		$dailyMarginDR	= Order::dailyMarginDR($start, $end);
		
		# get percentage_user 
		$percentage_dailyMarginDR = ($dailyMarginDR/$total_order)*100;
		$percentage_dailyMarginDR = number_format($percentage_dailyMarginDR, 2, '.', '') . '%';

		return json_encode( number_format($dailyMarginDR,2).'|'.$percentage_dailyMarginDR );
	}
	
	/*
	 * @name:	sales
	 * @author:	M.Sajid
	 */
	public function sales(){
		
		# get total Orders
		$total_order = Order::count();
		
		$date = Input::get('date');
		$explode = explode(' - ', $date);
		
		$start = date( 'Y-m-d', strtotime($explode[0]) );
		$end = date( 'Y-m-d', strtotime($explode[1]) );
		
		$this_order	= Order::this_order($start, $end);
		
		# get percentage 
		$percentage_order = ($this_order/$total_order)*100;
		$percentage_order = number_format($percentage_order, 2, '.', '') . '%';

		return json_encode( number_format($this_order,2).'|'.$percentage_order);
	}
	
	/*
	 * @name:	reg_users
	 * @author:	M.Sajid
	 */
	public function reg_users(){
		
		# get total users
		$total_user = User::count();
		
		$date = Input::get('date');
		$explode = explode(' - ', $date);
		
		$start = date( 'Y-m-d', strtotime($explode[0]) );
		$end = date( 'Y-m-d', strtotime($explode[1]) );
		
		$reg_users	= User::reg_users($start, $end);

		# get percentage_user 
		$percentage_user = ($reg_users/$total_user)*100;
		$percentage_user = number_format($percentage_user, 2, '.', '') . '%';

		return json_encode( $reg_users.'|'.$percentage_user);
	}
	
	//GET TOP SELLER BY CATEGORY 
	public function sellerByCategory($categoryId = 'all'){
		
		$bestSellerByCategory	=	array();
		$bestSellerByCategory	= Dashboard::bestSellerByCategory($categoryId);
		$data['bestSellerByCategory']	=	$bestSellerByCategory;
		
		//COLOR ARRAY
		$data['colorArr']		=	array('success','info','important','warning','success');
		
		// $this->layout->title	= trans('localization.dashboard');
		$view	= View::make('dashboard/sellerByCategory',compact('data'));
		return $view;
	}
	
	/**
	* DASHBOARD REVENUE CHART
	*/
	
	public function get_revenue_chart(){
		
		$requestParameter	=	Input::get();
		
		if(isset($requestParameter['data'])){
			
			//IF REQUEST FOR MONTHLY RECORD
			if($requestParameter['data'] == 'monthly'){
				
				//GET REVENUE BY WEEK
				$getRevenueByRange		=	Dashboard::getRevenueByRange('12 MONTH','month');
				
				$chartData	=	array();
				$labelArray	=	array();
				if(!empty($getRevenueByRange)){
					$labelKey	=	1;
					foreach($getRevenueByRange as $key=>$getRevenueByRangeRow){
						$labelArray[$labelKey]	   =   $getRevenueByRangeRow->order_day;
						$chartData[$key][] = $labelKey;
						$chartData[$key][] = $getRevenueByRangeRow->dailyMargin;
						
						$labelKey++;
					}
				}
				$data['chartData']	=	$chartData;
				$data['labelArray']	=	$labelArray;
				
			}
			
			//IF REQUEST FOR WEEKLY RECORD
			if($requestParameter['data'] == 'weekly'){
				
				//GET LIST OF DATES OF LAST WEEK
				$previous_week 	= strtotime("-1 week +1 day");

				$start_week 	= date("Y-m-d");
				$end_week 		= date("Y-m-d",$previous_week);

				$weekDateList	=	$this->get_last_week_dates($end_week,$start_week);
			
				
				//GET REVENUE BY WEEK
				$getRevenueByRange		=	Dashboard::getRevenueByRange('1 WEEK','');
				
				$chartData	=	array();
				$labelArray	=	array();
				$dbDateArr	=	array();
				if(!empty($getRevenueByRange)){
					$labelKey	=	1;
					foreach($getRevenueByRange as $key=>$getRevenueByRangeRow){
						
						$labelArray[$labelKey]	   =  $getRevenueByRangeRow->order_day;
						$chartData[strtotime($getRevenueByRangeRow->order_formattedDate)][] 	= $labelKey;
						$chartData[strtotime($getRevenueByRangeRow->order_formattedDate)][] 	= $getRevenueByRangeRow->dailyMargin;
						$dbDateArr[]		=	$getRevenueByRangeRow->order_formattedDate;
						$labelKey++;
					}
				}
				
				//GET NON_EXSIST DATE IN DB
				$key=$labelKey=0;
				$dateArrayDiff	=	array_diff($weekDateList,$dbDateArr);
				if(!empty($dateArrayDiff)){
					//SET RECORD 0 FOR NON-EXSIST RECORD
					
					foreach($dateArrayDiff as $dateArrayDiffRow){
						$key++;
						
						$labelArray[$labelKey]	   =  date("M d,Y",strtotime($dateArrayDiffRow));
						$chartData[strtotime($dateArrayDiffRow)][] = $labelKey;
						$chartData[strtotime($dateArrayDiffRow)][] = 0;
						
						$labelKey++;
					}
				}
				//SORT DATA w.r.t to DATE
				krsort($chartData);
				$chartDataSortedArr	=	array();
				$labelSortedArray	=	array();
				if(!empty($chartData)){
					$labelKey		=	1;
					$key			=	0;
					foreach($chartData as $dateTime=>$chartDataRow){
						
						$labelSortedArray[$labelKey]	   =  date("M d,Y",$dateTime);
						$chartDataSortedArr[$key][]	=	$labelKey;
						$chartDataSortedArr[$key][]	=	$chartDataRow[1];
						
						$labelKey++;
						$key++;
					}
				}
				// echo "<pre>";
				// print_r($labelSortedArray);
				// print_r($chartDataSortedArr);
				// exit();
				$data['chartData']	=	$chartDataSortedArr;
				$data['labelArray']	=	$labelSortedArray;
				
			}
			
			
			//IF REQUEST FOR YEARLY RECORD
			if($requestParameter['data'] == 'yearly'){
				
				//GET REVENUE BY WEEK
				$getRevenueByRange		=	Dashboard::getRevenueByRange('yearly','yearly');
				
				$chartData	=	array();
				$labelArray	=	array();
				if(!empty($getRevenueByRange)){
					$labelKey	=	1;
					foreach($getRevenueByRange as $key=>$getRevenueByRangeRow){
						
						if($getRevenueByRangeRow->create_time<=0){
							continue;
						}
						
						$labelArray[$labelKey]	   = $getRevenueByRangeRow->order_day;
						$chartData[$key][] = $labelKey;
						$chartData[$key][] = $getRevenueByRangeRow->dailyMargin;
						
						$labelKey++;
					}
				}
				$data['chartData']	=	$chartData;
				$data['labelArray']	=	$labelArray;
				
			}
			
			$view	= View::make('dashboard/ajax_chart',compact('data'));
			echo $view;exit();
		}
		
	}
	
	#get users chart
	public function get_users_chart()
	{
		$data = array();
		$requestParameter	=	Input::get();
		
		$year =0;
		if(!empty($requestParameter['year']))
		{
			$year = $requestParameter['year'];
		}
		
		
		$monthArray		 = getMonthArray(); 
		$registerUsers 	 = User::registeredUsersMonthly($year);
		
		#data for graph
		$userChartData = getDataFroGraph($registerUsers,$monthArray);
		
		//User chart
		$data['userChartData']				= $userChartData;
		$data['monthArray']					= $monthArray;
		$view	= View::make('dashboard/users_chart',compact('data'));
		echo $view;exit();
	}
	
	#get sales chart
	
	public function get_sales_chart()
	{
		$data = array();
		$requestParameter	=	Input::get();
		
		$fromYear=$toYear=0;
		if(!empty($requestParameter['fromYear']))
		{
			$fromYear = $requestParameter['fromYear'];
		}
				
		$toYear = 0;
		if(!empty($requestParameter['toYear']))
		{
			$toYear = $requestParameter['toYear'];
		}
		
		$monthArray		 = getMonthArray(); 
		
		$fromMonthlySales 	 = Dashboard::totalNumberMonthlySales($fromYear);
		$toMonthlySales 	 = Dashboard::totalNumberMonthlySales($toYear);
		
		#from array 
		$fromChartData = getDataFroGraph($fromMonthlySales,$monthArray);
		#toarray
		$toChartData = getDataFroGraph($toMonthlySales,$monthArray);
		
		
		
		
		//User chart
		$data['fromChartData']				= $fromChartData;
		$data['toChartData']				= $toChartData;
		$data['monthArray']					= $monthArray;
		$data['fromYear']					= $fromYear;
		$data['toYear']						= $toYear;
		$view	= View::make('dashboard/sales_chart',compact('data'));
		echo $view;exit();
	}
	
	#get margin chart
	public function get_margin_chart()
	{
		$data = array();
		$requestParameter	=	Input::get();
		
		$fromYear=$toYear=0;
		if(!empty($requestParameter['fromYear']))
		{
			$fromYear = $requestParameter['fromYear'];
		}
				
		$toYear = 0;
		if(!empty($requestParameter['toYear']))
		{
			$toYear = $requestParameter['toYear'];
		}
		
		$monthArray		 = getMonthArray();  
		
		$fromMonthlySales 	 = Dashboard::totalNumberMonthlyMargin($fromYear);
		$toMonthlySales 	 = Dashboard::totalNumberMonthlyMargin($toYear);
		
		#from array 
		$fromChartData = getDataFroGraph($fromMonthlySales,$monthArray);
		#toarray
		$toChartData = getDataFroGraph($toMonthlySales,$monthArray);
		
		//User chart
		$data['fromChartData']				= $fromChartData;
		$data['toChartData']				= $toChartData;
		$data['monthArray']					= $monthArray;
		$data['fromYear']					= $fromYear;
		$data['toYear']						= $toYear;
		$view	= View::make('dashboard/margin_chart',compact('data'));
		echo $view;exit();
	}
	
	//GET LIST OF DATE BETWEEN TWO DATES
	function get_last_week_dates($date_from,$date_to)
	 {
			
		$dateList	=	array();
		// Specify the start date. This date can be any English textual format  
		$date_from = strtotime($date_from); // Convert date to a UNIX timestamp  
		  
		// Specify the end date. This date can be any English textual format  
		$date_to = strtotime($date_to); // Convert date to a UNIX timestamp  
		  
		// Loop from the start date to end date and output all dates inbetween  
		for ($i=$date_from; $i<=$date_to; $i+=86400) {  
			$dateList[]	=	 date("Y-m-d", $i);  
		} 
		
		// krsort($dateList);
		return $dateList;
	}
}