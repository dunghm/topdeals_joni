<?php
class WarehouseSectionController extends BaseController 
{

	/*
	 * __construct
	 */
	public function __construct() 
	{
		
	}
	
	public function index() 
	{
		$data = '';
		$this->layout->title 	= trans('warehouse_lang.title');		
		$this->layout->content 	= View::make('warehouse/index', compact('data'));
	}
	
	/*
	* Ajax Request for server side data
	*/	
	public function getDatatable()
	{
		# call function
		return WarehouseSection::getSections(Input::get());
		
	}
	
	/*
	 * create from for warehouse section
	 */
	public function create()
	{
		$data = '';
		$this->layout->title 	= trans('warehouse_lang.createsection');	
		$warehouseList = Warehouse::getAll();
		$this->layout->content 	= View::make('warehouse/create', compact('warehouseList'));
	}
	
	/*
	 * update from for warehouse section
	 */
	function update($id=0)
	{
		$warehouseList = array();
		$warehouseSection = array();
		
		$this->layout->title 	= trans('warehouse_lang.title');	
		$warehouseList 			= Warehouse::getAll();
		$warehouseSection 		= WarehouseSection::getwarehouseSectionById($id);
		
		if(is_array($warehouseSection) && count($warehouseSection)>0)
		{
			$warehouseSection = $warehouseSection[0];
		}
		
		$this->layout->content 	= View::make('warehouse/update', compact('warehouseList','warehouseSection'));
	}
	
	/*
	 * save from for warehouse section
	 */
	protected function saveandupdate($id=0)
	{ 
		$name 				= Input::get('name');
		$warehouseId 		= Input::get('warehouse');
		$description 		= Input::get('description');
	
		$successMessage 	= '';
		$errorMessage		= '';
		
		
		#upadet record
		if(is_numeric($id) && $id>0)
		{
			$fieldsWithValue = " warehouse_id ='$warehouseId', name ='$name', description = '$description' ";
			$updated = WarehouseSection::updateRecord($id,$fieldsWithValue);
			if($updated)
			{
				$successMessage = "Warehouse Section Updated successfully";
			}
			else
			{
				$errorMessage = "Warehouse Section not Updated successfully, please try again!";
			}
		}
		else
		{
			#insert record
			$tableName			= 'warehouse_section';
			$fields 			= 'warehouse_id, name,description';
			$warehouseSection 	= array($warehouseId,$name,$description);
		
			$Inserted = WarehouseSection::saveRecord($tableName,$fields,$warehouseSection);
			
			if($Inserted)
			{
				$successMessage = "Warehouse Section Added successfully";
			}
			else
			{
				$errorMessage = "Warehouse Section not added successfully, please try again!";
			}
		}
		
		
		
		if($successMessage)
		{
			Session::flash('successMessage', $successMessage);
		}
		else
		{
			Session::flash('errorMessage', $errorMessage);
		}
		
		
		if(is_numeric($id) && $id>0)
		{
			return Redirect::to('admin/warehousesectionassignstock/'.$id);
		}
		else
		{
			return Redirect::to('admin/warehousesection');
		}
	}
	
	/*
	 *delete record
	 */
	
	protected function delete($id=0)
	{
		$deleted = WarehouseSection::deleteRecord($id);
		
		if($deleted)
		{
			Session::flash('successMessage', "Warehouse Section deleted successfully");
		}
		else
		{
			Session::flash('errorMessage', "Warehouse Section not deleted successfully, please try again!");
		}
		
		return Redirect::to('admin/warehousesection');
	}
	
	
	/*
	 * Assignstock
	 */ 
	function assignstock($sectionId=0)
	{
		$data = array();
		$this->layout->title 	= trans('warehouse_lang.assignstock');
		$warehouseSection 		= WarehouseSection::getAllWarehouseSection();
		$categoryByZone			= Category::getCategoryByZone();

		$this->layout->content 	= View::make('warehouse/assignstock', compact('sectionId','warehouseSection','categoryByZone'));
	}
	
	
	function getandsaveteambysection()
	{
		
		#$warehouseTeamCategory 				= Input::get('warehouseTeamCategory');
		#$warehouseTeamId 					= Input::get('warehouseTeamId');
		#$warehouseOptionId 					= Input::get('warehouseOptionId');
		$warehouseSectionId 				= Input::get('warehouseSectionId');
		$action 							= Input::get('action');
		
		if($action=="save")
		{
			#
			$assignStockCheckBox 				= Input::get('assignStockCheckBox');
			
			$jsError = '';
			if(!$warehouseSectionId)
			{
				$jsError .= '<div class="alert alert-danger alert-dismissible fade in" role="alert">Section is required field!</div>';
			}
			
			if (!$jsError) 
			{
				$warehouseTeamId=$loopCounter=0;
				
				$sectionAlreadyExistMessage="Sections is already assigned for below Deal(s) and Option(s) Ids<br/>";
				if(is_array($assignStockCheckBox) && count($assignStockCheckBox)>0)
				{
					foreach($assignStockCheckBox as $assignStock)
					{
						
						$warehouseOptionId=0;
						$insertRecord = true;
						
						$exploded = explode('-',$assignStock);
						$warehouseTeamId		=	reset($exploded);
						
						if(!empty($exploded[1]))
						{
							$warehouseOptionId 		=   end($exploded);
						}
						
						
						#check section is already exist or not
						
						$checkTeamSection = WarehouseSection::checkTeamSection($warehouseSectionId,$warehouseTeamId,$warehouseOptionId);
						if(is_array($checkTeamSection) && count($checkTeamSection) >0)
						{
							$checkTeamSection 	=	$checkTeamSection[0];
							
							if(!empty($checkTeamSection->Totalcount))
							{
								$insertRecord       = false;
							}
						}
						
						//this will give array of deal id and option id var_debug($sectionAlreadyExistArray);
						
						if($insertRecord)
						{
							#insert record
							$tableName			= 'warehouse_section_items';
							$fields 			= 'warehouse_section_id, team_id,option_id';
							$teamSectionArray 	= array($warehouseSectionId,$warehouseTeamId,$warehouseOptionId);
						
							$Inserted = WarehouseSection::saveRecord($tableName,$fields,$teamSectionArray);
						}
						else
						{
							$loopCounter++;
							$sectionAlreadyExistMessage.="Deal ID =".$warehouseTeamId.", Option ID=".$warehouseOptionId."<br/>";
						}
						
						
						
					}
					
					#error loop counter
					if($loopCounter>0)
					{
						$jsError.= '<div class="alert alert-danger alert-dismissible fade in" role="alert">'.$sectionAlreadyExistMessage.'</div>';
					}
					
				}
				
			} 
			
			if ($jsError)
			{
				echo $jsError;
            }
			
		}
		
		#get team section
		if (is_numeric($warehouseSectionId) && $warehouseSectionId > 0) 
		{
			
			$teamsection = WarehouseSection::getTeamSection($warehouseSectionId);
		
			echo View::make('warehouse/teamsgrid', compact('teamsection'));
		}
		exit;
		
		
	}
	
	

	
	
	function deleteteam($warehouseItems=0)
	{
		
		$action 							= Input::get('action');
		
		if($action=="deletemultiple")
		{
			$warehouseItemsArray 	= Input::get('warehouse_items');
			
			$explodedItem = explode(',', $warehouseItemsArray);
            
			foreach($explodedItem as $item)
			{
				WarehouseSection::deleteItem($item);
            }
		}
		else
		{
			WarehouseSection::deleteItem($warehouseItems);
		}
		exit;
		
	}
	
	#move section form
	function moveForm()
	{
		$this->layout->title 			= trans('warehouse_lang.assignstock');
		
		$warehouseSectionId 			= Input::get('warehouseSectionId');
		$warehouseSectionItemId 		= Input::get('warehouseSectionItemId');
		
		$warehouseSection 				= WarehouseSection::getAllWarehouseSection($warehouseSectionId);
		
		echo View::make('warehouse/move', compact('warehouseSection','warehouseSectionItemId','warehouseSectionId'));
        exit;
	}
	
	
	function move()
	{
		$this->layout->title 			= trans('warehouse_lang.assignstock');
		
		$oldwarehouseSectionId 			= Input::get('oldwarehouseSectionId');
		$warehouseSectionId 			= Input::get('warehouseSectionId');
		$warehouseSectionItemIds 		= Input::get('warehouseSectionItemIds');
		
		$jsData = array();
		$jsData['error'] = '';
		$successMessage = $errorMessage =$redirectUrl=$existMessage='';
		
		$jsError = '';
		$updateSection = false;
		
		if(!$warehouseSectionId)
		{
			$jsError.= "Target Section is required field!";
		}
		
		if(!$jsError)
		{
			
			$validateWareHouseSectionItems = validateWareHouseSectionItems($warehouseSectionId,$warehouseSectionItemIds);
			if(is_array($validateWareHouseSectionItems) && count($validateWareHouseSectionItems)>0)
			{
					if(!empty($validateWareHouseSectionItems['ids']))
					{
						$existSectionItemArray	 = $validateWareHouseSectionItems['ids'];
						$postSectionItemIdArray  = explode(',',$warehouseSectionItemIds);
						$warehouseSectionItemIds = arrayDifference($postSectionItemIdArray,$existSectionItemArray);
					}
					
					if(!empty($validateWareHouseSectionItems['message']))
					{
						$existMessage 				= $validateWareHouseSectionItems['message'];
					}
					
			}
			
			
			
			$redirectUrl =URL::to('admin/warehousesectionassignstock/'.$oldwarehouseSectionId); 
			$updateSection = WarehouseSection::updatesection($warehouseSectionId,$warehouseSectionItemIds);
			
			if($updateSection)
			{
				$successMessage = "Section Moved successfully";
				
			}
			else
			{
				$errorMessage = "Section not Moved successfully";
			}
			
			
			
			$jsData['redirect'] 		= $redirectUrl;
			$jsData['successMessage'] 	= $successMessage;
			$jsData['errorMessage'] 	= $errorMessage;
			$jsData['existMessage'] 	= $existMessage;
		}
		
		$jsData['error'] = $jsError;

		$jsData = json_encode($jsData);

		echo $jsData;
		exit;
		
	}
	
	
	 public function assignstocklist()
	 {
		$this->layout->title 			= trans('warehouse_lang.assignstock');
		
		$filterStock 	=  Input::get('filterStock');
		$categoryId 	=  Input::get('categoryId');
        $search 		=  Input::get('search');
		
		#$this->layout->content 	= View::make('warehouse/assignstocklist');
        echo View::make('warehouse/assignstocklist',compact('categoryId','search','filterStock'));exit;
	 }
	
	 /*
	  * assignstockdetail
	  */
	  
	  public function assignstockdetail()
	  {
		$arrQueryParams = array();
        #############################Filter#################################
        $filterBySearch 	=  Input::get('filterBySearch');
        $filterByCategory 	=  Input::get('filterByCategory');
		$filterStock 		=  Input::get('filterStock');

        
		if (!empty($filterStock)) 
		{
            $arrQueryParams['filterStock'] = $filterStock;
        }
		
		if (!empty($filterBySearch)) {
            $arrQueryParams['filterBySearch'] = $filterBySearch;
        }

        if (!empty($filterByCategory)) {
            $arrQueryParams['filterByCategory'] = $filterByCategory;
        }
		
		
        #############################SET LIMIT###############################
        $iDisplayStart  = Input::get('iDisplayStart');
        $iDisplayLength = Input::get('iDisplayLength');

        if (is_numeric($iDisplayStart)) {
            $arrQueryParams['setFirstResult'] = $iDisplayStart;
        }

        if (is_numeric($iDisplayLength)) {
            $arrQueryParams['setMaxResults'] = $iDisplayLength;
        } 
        #############################//SET LIMIT###############################
		##############################ORDERING################################

        /*
         * Ordering
         */

         $aColumns = array('dealId','category', 'dealId', 'dealTitle', 'optionId', 'optionTitle', 'stock','section'); 

        $iSortCol_0 = Input::get('iSortCol_0');

        if (isset($iSortCol_0)) {
            $sOrder = "";
            $iSortingCols = Input::get('iSortingCols');

            for ($i = 0; $i < intval($iSortingCols); $i++) {

                if (Input::get('bSortable_' . intval(Input::get('iSortCol_' . $i))) == "true") {
                    $sOrder .= $aColumns[intval(Input::get('iSortCol_' . $i))] . "
				 	" . Input::get('sSortDir_' . $i) . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            $arrQueryParams['sOrder'] = $sOrder;
        }

        ##############################//ORDERING################################
		$iTotal = 0;
		$assignStockList 		= WarehouseSection::getAssignStockInfo($arrQueryParams);
		$iTotal 				= WarehouseSection::getAssignStockCount($arrQueryParams);
		################################SET OUTPUT FOR JSON#########################
        $output = array(
            "sEcho" => intval(Input::get('sEcho')),
            "iTotalRecords" => $iTotal,
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array()
        );
        $row = array();
        $counter = 0;
		
		 if (is_array($assignStockList) && count($assignStockList) > 0) 
		 {
			 foreach($assignStockList as $data)
			 {
				$category =$dealTitle= $optionTitle=$optionId=$section='';
				$dealId =$stock=0;
				
				#category
				if($data->category)
				{
					$category = $data->category;
				}
				
				#Deal id
				if($data->dealId)
				{
					$dealId = $data->dealId;
				}
				
				#deal title
				if($data->dealTitle)
				{
					$dealTitle = $data->dealTitle;
				}
				
				#optionId
				if($data->optionId)
				{
					$optionId = $data->optionId;
				}
				
				#optionTitle
				if($data->optionTitle)
				{
					$optionTitle = $data->optionTitle;
				}
				
				#stock
				if($data->stock)
				{
					$stock = $data->stock;
				}
				
				#section 
				if($data->section)
				{
					$section = $data->section;
				}
				
				$assignId = $dealId."-".$optionId;
$checkBox = <<<HTML
				<input type="checkbox" name="assignStockCheckBox" class="assignStockCheckBox" id="assignStock-$assignId" value="$assignId" onclick="enabledAssignButton();" />
HTML;
				
				
				$row[$counter][] = $checkBox;
				$row[$counter][] = $category;
                $row[$counter][] = $dealId;
                $row[$counter][] = $dealTitle;
                $row[$counter][] = $optionId;
                $row[$counter][] = $optionTitle;
                $row[$counter][] = $stock;
				$row[$counter][] = $section;
                $output['aaData'][] = $row[$counter];

                $counter++;
			 }
		 }
		 
		 echo json_encode($output);
         exit;
	  }

	
	
}	