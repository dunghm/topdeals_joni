<?php
/**
*@page: Login
*desc:	* 	function __construct() is checking the Cross Site Forgery Request and Authentication.
*		* 	function showLogin() rendering login page
*		* 	function dologin(), passing rules for email&password(required), validate, if login fail it will return to login page with error / 
*			if success it will check user role if user level = 1 it will redirect to dashboard and if its role = 3 it will redirect to my-*		*			account page.
*		*	function doLogout() it is using for logout ( Auth::logout() ) is laravel function for session destroy
***/
class AdminController extends BaseController {
	
	/*
	* Public Function __construct to check the Login Security and CSRF
	*/	
	public function __construct() {	
		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->beforeFilter('auth', array('only'=>array('login')));
	} 
	
	public function explicitlogin() {
		
		//IF SESSION MENTAIN THEN REDIRECTED TO ADMIN/PARTNER DASHBOARD
		if(Session::has('user_type')){
			$userType	=	Session::get('user_type');
			if(!empty($userType)){
				return Redirect::to("$userType/dashboard");
			}
		}
		
		$segment = Request::segment(1);
		$segmentRedirect	=	"admin";
		if($segment=="partner")
		{
			$segmentRedirect	=	"partner";
		}
		
		Session::flush();
		return Redirect::to($segmentRedirect.'/login');
	}
	
	/*
	* Public Function To Show Login Form
	*/	
	public function showLogin() {
		
		$this->layout->title = "Login";
		if (!isset(Auth::user()->id)) {
			$this->layout->content = View::make('login/login');
		} else {
			return Redirect::to('admin/dashboard');
		}
	}
	
	/*
	* Check If User Is Exist, If Yes then check access level 
	* (1 = admin, other than 1 will be storeside  customer)  
	* Successfully Login
	* Other wise will show login errors
	*/
	public function dologin() {
		
		/*$request_uri = explode('/', $_SERVER['REQUEST_URI']);
		$request_uri = $request_uri[3];*/
		$segment = Request::segment(1);
		
		$rules = array(
			'username' 	=> 'required',
			'password'	=> 'required'
		);
		// var_debug(Input::get());exit;
		/*
		 * Sample validation added, please use this sample
		 * If there is any validation fail, it will automatic redirect to the last page, don't need to worry
		 * Further code processing will stop, and user will be redirected to last action where coming from
		 * */
		if(!$this->validation(compact('rules'))) return $this->goBack();
		
		# Your code logic goes here, continue
		
			
			//GET USER INFO BY NAME TO LOGIN USER WITH OUR OLD PASSWORD
			if ($segment == 'admin'){
				$user_entity = User::where('username', '=', Input::get('username'))->first();
			} else {
				$user_entity = Partner::where('username', '=', Input::get('username'))->first();
			}

			if(isset($user_entity)) {
				if($user_entity->password == md5(Input::get('password').SECURE_MD5_STR)) { // If their password is still MD5
					// $user_entity->password_laravel = Hash::make(Input::get('password')); // Convert to new format
					// $user_entity->save();
					if(isset($user_entity->id)){
						
						// set the remember me cookie if the user check the box
						$remember = (Input::has('remember')) ? 1 : '';
						$user_entityID	=	$user_entity->id;
						$enable			=	$user_entity->enable;
						if($enable == 'Y'){
							if(Auth::loginUsingId($user_entityID,$remember)){
									Session::put('user_type', $segment);
									$adminRole =  getCurrentUserRole();
									
									if($adminRole=="admin")
									{
										return Redirect::to($segment.'/dashboard/');
									}
									else
									{
										return Redirect::to($segment.'/deals/');
									} 
									
									
							}else{
								return $this->sendError('Unable to Login');
							}
						}else{
							return $this->sendError('Your account is disabled please contact to administrator.');
						}
						
					}else{
						return $this->sendError('Your username/password combination was incorrect');
					}
					// ;
				}else{
					return $this->sendError('Your password was incorrect');
				}
			}else{
				return $this->sendError('Invalid Username');
			}
			
	} /* End dologin() */
	
	
	/*
	* Public Function Logout
	*/
	
	public function doLogout() {
		
		$segment = Request::segment(1);	
				
		Session::put('perpage', '');
		Session::put('newtab', '' );
		Session::put('show_warning', '');
		Session::put('user_type', '');
		Auth::logout(); // log the user out of our application
		#return Redirect::to('login'); // redirect the user to the login screen
		return Redirect::to($segment.'/login')->with('message', 'You are successfully logged out!');
	} // End doLogout()
}  // End AdminController