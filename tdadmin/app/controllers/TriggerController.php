<?php
require_once(dirname(dirname(dirname(dirname(__FILE__)))) . '/trigger/email_parser.php');

class TriggerController extends BaseController {
	
	/*
	 * __construct
	 */
	public function __construct() {
		
	}
	
	/**
	*@author:VaqasUddin
	*@desc: it will load index page and including customers
	**/
	public function index()
	{
		$data['customers'] = array();
		$this->layout->title	= trans('trigger_lang.trigger');
		$this->layout->content	= View::make('trigger/index',compact('data'));
	}
	
	/*
	* Ajax Request for server side data
	*/	
	public function getDatatable()
	{
		# call function
		return Trigger::getSections(Input::get());
		
	}
	
	/**
	* triggerAction
	*/
	public function triggerAction()
	{
		$data['customers'] = array();
		$this->layout->title	= trans('trigger_lang.trigger');
		$this->layout->content	= View::make('trigger/triggerAction',compact('data'));
	}
	/*
	* Ajax Request for server side data
	*/	
	public function triggerActionDatatable()
	{
		# call function
		return Trigger::triggerActionDatatable(Input::get());
		
	}
	
	/**
	* ADD NEW ACTION
	*/
	public function addNewAction(){
		
		$data = array();
		$this->layout->title	= trans('trigger_lang.add_action');
		
		//Get Action Type
		$data['getActionType']	=	Trigger::getActionType();
		
		$this->layout->content	= View::make('trigger/addNewAction',compact('data'));
	}
	
	/**
	* ADD NEW ACTION SUBMIT
	*/
	public function addNewActionSubmit(){
		 
		 $data = Input::get();
	
		 $errMessage	=	"";
		 if(!empty($data)){
			
			//VALIDATION
			$errMessage	=	Trigger::validateActionForm($data);
			
			
			
			if($errMessage==""){
				//SAVE TRIGGER ACTION
				Trigger::saveTriggerAction($data);
			}
			 
		 }
		 
		if($errMessage!=""){
			return Redirect::to('admin/addNewAction')->with('errmessage', "$errMessage");
		}else{
			return Redirect::to('admin/triggerAction')->with('message', "Action Added Successfully!");
		}
	}
	
	
	/**
	* updateNewAction
	**/
	
	public function updateNewAction($id = 0){
		
		if(is_numeric($id) && $id>0){
			$data = array();
			$this->layout->title	= trans('trigger_lang.update_action');
			
			//Get Action Type
			$data['getActionType']	=	Trigger::getActionType();
			
			//GET ACTION DATA
			$data['getActionData']	=	Trigger::getActionData($id);
			
			$this->layout->content	= View::make('trigger/updateAction',compact('data'));
		}else{
				return Redirect::to('admin/triggerAction')->with('errmessage', "Invalid Action");
				exit();
		}
	}
	
	
	/**
	* UPDATE ACTION SUBMIT
	*/
	public function updateActionSubmit(){
		 
		 $data = Input::get();
		 $action_id	=	$data['action_id'];
		 
		 $errMessage	=	"";
		 if(!empty($data)){
			
			//VALIDATION
			$errMessage	=	Trigger::validateActionForm($data);
			
			
			
			if($errMessage==""){
				//SAVE TRIGGER ACTION
				Trigger::updateTriggerAction($data);
			}
			 
		 }
		if($errMessage!=""){
			return Redirect::to("admin/updateNewAction/$action_id")->with('errmessage', "$errMessage");
		}else{
			return Redirect::to('admin/triggerAction')->with('message', "Action Updated Successfully!");
		}
	}
	
	/**
	* actionView
	*/
	
	public function actionView($id){
		
		if(is_numeric($id) && $id>0){
			
			$data = array();
			$this->layout->title	= trans('trigger_lang.view_action');
			
			//Get Action Type
			$data['getActionType']	=	Trigger::getActionType();
			
			//GET ACTION DATA
			$data['getActionData']	=	Trigger::getActionData($id);
			
			echo View::make('trigger/viewAction',compact('data')); exit();
		}else{
				echo"Invalid Action";
				exit();
		}
	}
	
	/**
	* DELETE TRIGGER ACTION
	*/
	public function deleteTriggerAction($id){
		
		if(is_numeric($id) && $id>0){
			
			//CHECK ACTION CAN DELETE IF ITS NOT ASSOCIATE WITH ANY OTHER EVENT
			$isActionAssociateWithEvent	=	Trigger::isActionAssociateWithEvent($id);
			
			if(!$isActionAssociateWithEvent){
				//GET ACTION DATA
				Trigger::deleteActionTrigger($id);
				
				return Redirect::to('admin/triggerAction')->with('message', "Trigger Action Deleted Successfully!");
			}else{
				return Redirect::to('admin/triggerAction')->with('errmessage', "Trigger action cannot delete because events are bind with this action");
			}
		}else{
			return Redirect::to('admin/triggerAction')->with('errmessage', "Invalid Action!");
		}
		exit();
	}
	
	/**
	* ADD EVENT
	*/
	public function addEvent(){
			
		$data = array();
		$this->layout->title	= trans('trigger_lang.add_event');
		
		//Get getActionList
		$data['getActionList']	=	Trigger::getActionList();
		
		
		$this->layout->content	= View::make('trigger/addEvent',compact('data'));
	}
	
	/**
	* addTriggerSubmit
	*/
	public function addTriggerSubmit(){
		
		$data = Input::get();
		
		$errMessage	=	"";
		 if(!empty($data)){
			
			//VALIDATION
			$errMessage	=	Trigger::validateTriggerForm($data);
			
			
			
			if($errMessage==""){
				//SAVE TRIGGER ACTION
				Trigger::addTriggerEvent($data);
			}
			 
		 }
		 
		if($errMessage!=""){
			return Redirect::to("admin/addevent")->with('errmessage', "$errMessage");
		}else{
			return Redirect::to('admin/trigger')->with('message', "Event Added Successfully!");
		}
	}
	
	/**
	* updateEvent
	*/
	
	protected function updateEvent($id){
		
		if(is_numeric($id) && $id>0){
			$data = array();
			$this->layout->title	= trans('trigger_lang.update_event');
			
			//Get Action Type
			$data['getActionList']	=	Trigger::getActionList();
			
			//GET ACTION DATA
			$data['getEventData']	=	Trigger::getEventData($id);

			$this->layout->content	= View::make('trigger/updateEvent',compact('data'));
		}else{
				return Redirect::to('admin/trigger')->with('errmessage', "Invalid Action");
				exit();
		}
	}
	
	/**
	* UPDATE EVENT SUBMIT
	*/
	public function updateEventSubmit(){
		 
		 $data = Input::get();
		 $event_id	=	$data['event_id'];
		 
		 $errMessage	=	"";
		 if(!empty($data)){
			
			//VALIDATION
			$errMessage	=	Trigger::validateTriggerForm($data);
			
			
			
			if($errMessage==""){
				//SAVE TRIGGER ACTION
				Trigger::updateTriggerEvent($data);
			}
			 
		 }
		if($errMessage!=""){
			return Redirect::to("admin/updateEvent/$event_id")->with('errmessage', "$errMessage");
		}else{
			return Redirect::to('admin/trigger')->with('message', "Event Updated Successfully!");
		}
	}
	
	/**
	* DELETE TRIGGER EVENT
	*/
	
	protected function deleteTriggerEvent($id){
		
		if(is_numeric($id) && $id>0){
			
			//DELETE TRIGGER EVENY
			Trigger::deleteActionEvent($id);
			
			return Redirect::to('admin/trigger')->with('message', "Trigger Event Deleted Successfully!");
		}else{
			return Redirect::to('admin/trigger')->with('errmessage', "Invalid Action!");
		}
		exit();
	}
	
	/**
	* TRIGGER EVENT VIEW
	*/
	protected function eventView($id){
		
		if(is_numeric($id) && $id>0){
			
			$data = array();
			$this->layout->title	= trans('trigger_lang.view_event');
			
			//Get Action Type
			$data['getActionList']	=	Trigger::getActionList();
			
			//GET ACTION DATA
			$data['getEventData']	=	Trigger::getEventData($id);
			
			echo View::make('trigger/viewEvent',compact('data')); 
			exit();
		}else{
				echo"Invalid Action";
				exit();
		}
	}
	
	/**
	* EMAIL TEMPLAT HINT
	*/
	
	protected function templatePattern(){
		$data = array();
		
		$this->layout->title		=	'Template Pattern';
		
		//GET ACTION DATA
		$data['templatePattern']	=	templatePatternContent();
		
		echo View::make('trigger/templatePattern',compact('data')); 
		exit();
	}
	
	/**
	* SEND SAMPLE EMAIL
	*/
	
	protected function sendSampleEmail($id){
		
		if(is_numeric($id) && $id>0){
			$data = array();
			
			$this->layout->title		=	'Sample Email';
			
			//GET ACTION DATA
			$data['getEventActionData']	=	Trigger::getEventActionData($id);
			
			echo View::make('trigger/sendSampleEmail',compact('data')); 
		
		}else{
			echo"Invalid Action";
		}
		exit();
	}
	
	/**
	* SEND SAMPLE EMAIL FORM SUBMIT
	*/
	
	protected function triggerEmailSend(){
		 
		 $data = Input::get();
		 
		 if(!empty($data)){
			$event_id		=	$data['event_id'];
			$email_sent_to	=	$data['email_sent_to'];
			
			
			$err_Message	=	"";
			//VALDIATE INPUT
			if($email_sent_to==""){
				$err_Message .= "Please Enter Email Address";
			}
			if($event_id==""){
				$err_Message .= "Inavlid Event";
			}
			//IF VALID REQUEST
			if($err_Message==""){
			
			
				$getEventActionData = Trigger::getEventActionData($event_id);
				
				if(isset($getEventActionData->email_body)){
					
					$html = $getEventActionData->email_body;
					
					// DEFAULT SUBJECT
					$subject	= "Sample Test Email";
					
					if(isset($getEventActionData->email_subject) && !empty($getEventActionData->email_subject)){

						$subject		=	$getEventActionData->email_subject;
					}
					
					//FORMAT EMAIL BODY
					$customerDataRow		=	array(
													'username' 		=>	'Test User',
													'email' 		=>	'testEmail@abc.com',
													'first_name' 	=>	'Test',
													'last_name' 	=>	'User'
												);
												
					$getActiveTriggerRow	=	array();
					
					$promo_code				=	'Test_promo_code';
					
					$html	= emailBodyFormat($html,$customerDataRow,$getActiveTriggerRow,$promo_code);

					// SEND EMAIL
					if(email_manager_sendmail($email_sent_to, $subject, $html))
					{
						return Redirect::to('admin/trigger')->with('message',  "Sample Email Send Successfully");
					}
					else
					{
						return Redirect::to('admin/trigger')->with('errmessage', "Not able to send email, please try later!");
					}
				}else{
					return Redirect::to('admin/trigger')->with('errmessage', "Invalid Action!");
				}
			}else{
				return Redirect::to('admin/trigger')->with('errmessage', "Invalid Action!");
			}
		}
		
		exit();
	}
}