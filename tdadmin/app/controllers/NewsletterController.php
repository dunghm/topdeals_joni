<?php
class NewsletterController extends BaseController 
{

	/*
	 * __construct
	 */
	public function __construct() 
	{
		
	}
	
	public function index() 
	{
		$data = '';
		$this->layout->title 	= trans('localization.newletter_title');		
		$this->layout->content 	= View::make('newsletter/index', compact('data'));
	}
	
	/*
	* Ajax Request for server side data
	*/	
	public function getDatatable()
	{
		# call function
		return Newsletter::getSections(Input::get());
		
	}
	
	/*
	 * create newsletter
	 */
	public function create()
	{
		$data = '';
		$this->layout->title 	= trans('localization.newsletter_create');	
		$data['getAllCategory'] = Newsletter::getAllCategory();
		$this->layout->content 	= View::make('newsletter/create', compact('data'));
	}
	
	/*
	* Get Deal by Category
	*/
	public function getAndAddDealByCategory(){

		if(isset($_GET['action'])){
            $action = $_GET['action'];
            if($action == "calldeals")
			{
                    $id = Input::get('id');
					$getAllDealByCategory	=	 Newsletter::getAllDealByCategory($id);
					$generateOption			=	generateOption($getAllDealByCategory);
					echo $generateOption;
                   exit;
            }
            else if($action == 'add-deal'){
                
				$id = Input::get('id');
               
                $team = Team::getTeamById($id);
				// echo "<pre>";print_r($team);exit();
                // echo $this->load->view( "newsletter/newsletter_node", $data, true);
				echo View::make('newsletter/newsletter_node', compact('team'));
                //echo json_encode($team);
                exit;
                
            }
        }
		exit;
	}
	
	//UPDATE NEWSLETTER
	function update($id = 0){
		
		if(is_numeric($id) && $id>0){
			
			$data 					= array();
			
			$data['id'] 			 = $id;
			$data['newsletter']		 = Newsletter::getNewsletterInfo($id);
			$data['getAllCategory']  = Newsletter::getAllCategory();
			
			$this->layout->title 	 = trans('localization.newsletter_update');	
			$this->layout->content 	 = View::make('newsletter/edit', compact('data'));
		}else{
				return Redirect::to('admin/newsletter')->with('errmessage', "Invalid Action");
				exit();
		}
	}
	
	/**
	* SAVE AND UPDATE NEWSLETTER
	*/
	function saveandupdate(){

		$postedData	=	$_POST;
		
		$errMessage		=	"";
		$successMessage	=	"";
		
		if(!empty($postedData)){
			
			$title 				= Input::get('title');
			$deal_category 		= Input::get('deal_category');
			$newsletter_type 	= Input::get('newsletterType');
			$deals 				= Input::get('deals');
			
			//VALIDATE INPUT
			if($title==""){
				$errMessage	.=	"Newsletter Title is required. <br/>";
			}
			
			if($deal_category==""){
				$errMessage	.=	"Newsletter Category is required. <br/>";
			}
			
			if(empty($deals)){
				$errMessage	.=	"Please Select Deal for Newsletter. <br/>";
			}
			
			
			
			//IF FIELD IS VALIDATED NO ERROR FOUND
			if($errMessage==""){
				
				if(isset($postedData['newsLetterId'])){
					//UPDATE NEWSLETTER INFO
					$id = Input::get('newsLetterId');
				   
					Newsletter::updateNewsletter($title, $deal_category, $id,$newsletter_type);
					Newsletter::updateNewsletterDeal($deals,$id);
					// $this->newsletter_model->add_deals($deals, $id);
					$successMessage	=	"Newsletter Updated successfully.";
				}else{
					//INSERT NEW RECORD
					 $id = Newsletter::insertNewsletter($title, $deal_category,$newsletter_type);
					Newsletter::add_deals($deals, $id);
					$successMessage	=	"Newsletter Added successfully.";
				}
				
			}
			
		}else{
			$errMessage	.=	"Please fill all required field";
		}
		
		//SET REDIRECTION
		if($errMessage!=""){
			return Redirect::to('admin/newsletter/create')->with('errmessage', $errMessage);
		}else{
			return Redirect::to('admin/newsletter')->with('message', $successMessage);
		}
		exit();
	}
	
	
	//PREVIEW NEWSLETTER
	function previewNewsLetter(){
		 // require_once(dirname(dirname(dirname(dirname(__FILE__)))) . '/app.php');
        // echo "<pre>";print_r($_GET);exit();
        if(isset($_GET['id'])){
            $id = Input::get('id');
            $data['newsletter'] = Newsletter::getNewsletterInfo($id);
        }
        else{
            $newsletter = array(
									'newsletter_name'=> Input::get('title'), 
									'team_category'  => Input::get('deal_category')
								);
            
            $deals = array();
            $teams = array();
            foreach (Input::get('deals') as $d){
                $query_team = Team::getTeamById($d);
				
				if(isset($query_team[0])){
					$teams[$d] = $query_team[0];
				}
                $deals[] = array('deal_id'=>$d);
                
            }
             $data['newsletter'] = array('newsletter'=>$newsletter, 'deals'=>$deals, 'teams'=> $teams);
        }
        // echo $this->load->view( "newsletter/newsletter_preview", $data, true);
        echo View::make('newsletter/newsletter_preview', compact('data'));
        exit;
	}
	
	//DLEETE NEWSLETTER
	function deleteNewsletter($id=0){
		
		if(is_numeric($id) && $id>0){
			
			$deleted = Newsletter::deleteRecord($id);
		
			if($deleted)
			{
				Session::flash('successMessage', "Newsletter deleted successfully");
			}
			else
			{
				Session::flash('errorMessage', "Newsletter not deleted successfully, please try again!");
			}
		
		}
		return Redirect::to('admin/newsletter');
	}
	
	//Export Newsletter
	 function export($id = 0){

		if(is_numeric($id) && $id>0){
			
			$data['newsletter'] = Newsletter::getNewsletterInfo($id);
			 
			$data['export'] = 1;
			$html = View::make('newsletter/newsletter_preview', compact('data'));
			$file = tempnam("tmp", "zip");
            $zip = new ZipArchive();
            $zip->open($file, ZipArchive::OVERWRITE);
            $zip->addFromString('index.html', $html);
            
             $zip->addEmptyDir('images/');
            $teams = $data['newsletter']['teams'];
            foreach($teams as $team){
               
                if($team->image != ''){
                    $image_arr = explode('/', $team->image);
                    $zip->addFile(dirname(dirname(dirname(dirname(__FILE__)))).'/static/'. $team->image , 'images/'.$image_arr[count($image_arr)-1]);
                    //$team['image'] = $image_arr[count($image_arr)-1];
                }
            }
            $static_imgs = array(
                'twitter.jpg',
                'facebook.jpg',
                'mail.jpg',
                'line.jpg'
            );
            
            foreach($static_imgs as $img){
                 $zip->addFile(dirname(dirname(dirname(__FILE__))).'/assets/images/'. $img , 'images/'.$img);
            }
            $newsletter_node = $data['newsletter']['newsletter'];
            $logo_img = 'top.png';
            if($newsletter_node['team_category'] == 40){
				$logo_img = 'topkids.png';
			}else if($newsletter_node['team_category'] == 20){
				$logo_img = 'topbaby.png';
			}
			else if($newsletter_node['team_category'] == 19){
				$logo_img = 'topzen.png';
			}
			else if($newsletter_node['team_category'] == 8){
				$logo_img = 'topfun.png';
			}
			else if($newsletter_node['team_category'] == 11){
				$logo_img = 'topmaison.png';
			}else if($newsletter_node['team_category'] == 9){
				$logo_img = 'topgourmet.png';
			}
			
		    $zip->addFile(dirname(dirname(dirname(dirname(__FILE__)))).'/static/images/'. $logo_img , 'images/'.$logo_img);  
                                
            
            $zip->close();
			header('Content-Type: application/zip');
			header('Content-Length: ' . filesize($file));
			header('Content-Disposition: attachment; filename="newsletter-'.$id.'.zip"');
			readfile($file);
			unlink($file); 
            
		}else{
			return Redirect::to('admin/newsletter')->with('errmessage', "Invalid Action");
		}
		exit();
    }
	
	//SEND TEST EMAIL FROM PREVIEW NEWSLETTER
	function sendNewsletterEmail(){
		
		if(!empty($_POST)){
			if (Input::has('send_email')){
			
				$email_sent_to	=	Input::get('email_sent_to');
				$newsletter_id	=	Input::get('newsletter_id');
				
				$err_Message	=	"";
				//VALDIATE INPUT
				if($email_sent_to==""){
					$err_Message .= "Please Enter Email Address";
				}
				if($newsletter_id==""){
					$err_Message .= "Inavlid Newsletter";
				}
				
				//IF VALID REQUEST
				if($err_Message==""){
				
				
					$data['newsletter'] = Newsletter::getNewsletterInfo($newsletter_id);
					
					$html = View::make('newsletter/newsletter_preview', compact('data'));
					
					//DEFAULT SUBJECT
					$subject	= "Newsletter Test - $newsletter_id";
					
					if(isset($data['newsletter']['newsletter']['newsletter_name'])){

						$newsletterName	=	$data['newsletter']['newsletter']['newsletter_name'];
						$subject		=	"Newsletter Test - $newsletterName";
					}
					
					//SEND EMAIL
					if(email_manager_sendmail($email_sent_to, $subject, $html))
					{
						Session::flash('successMessage', "Sample Email Send Successfully");
					}
					else
					{
						Session::flash('errorMessage', "Not able to send email, please try later!");
					}
				}else{
					Session::flash('errorMessage', $err_Message);
				}
			}
		}
		return Redirect::to('admin/newsletter');
		exit();
	}
}	