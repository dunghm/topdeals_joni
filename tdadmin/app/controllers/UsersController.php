<?php

class UsersController extends \BaseController {

	/**
	 * Display a listing of users
	 *update user_setting set perpage 
	 * @return Response
	 */
	public function index()
	{
		$users = User::all();
        $this->layout->title 	= 'Users';	
		$this->layout->content 	= View::make('users/index', compact('users'));
                
	}

	/**
	 * Show the form for creating a new user
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('users.create');
	}

	/**
	 * Store a newly created user in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), User::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		User::create($data);

		return Redirect::route('users.index');
	}

	/**
	 * Display the specified user.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::findOrFail($id);

		return View::make('users.show', compact('user'));
	}

	/**
	 * Show the form for editing the specified user.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::find($id);

		return View::make('users.edit', compact('user'));
	}
        
        /**
	 * Show the form for editing the specified user.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function editProfile() {
        $id = Auth::user()->id;
		$user = Partner::find($id);
        $this->layout->title 	= trans('localization.partnerProfileTitle');	
		$this->layout->content 	= View::make('users/editProfile', compact('user'));
	}
        
		
	//Pasowrd View Show
	public function editPassword(){
		$id = Auth::user()->id;
		$user = Partner::find($id);
        $this->layout->title 	= trans('localization.partnerProfileTitle');	
		$this->layout->content 	= View::make('users/editPassword', compact('user'));		
	}
	
        /**
	 * Show the form for editing the specified user.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function userSetting()
	{
            $user_id    = Auth::user()->id;
            $setting_data               = DB::select( DB::raw("SELECT * FROM partner where id = ".$user_id));
            $this->layout->title 	= 'User Setting';	
            $this->layout->content 	= View::make('users/userSetting', compact('user_setting'));
            
	}
        /**
	 * Update the specified user in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateSetting()
	{
		$user_id    = Auth::user()->id;
			$new_tab 	= isset($_POST['new_tab']) ? $_POST['new_tab'] : 0;
			$warnings 	= isset($_POST['warnings']) ? $_POST['warnings'] : 0;
			$ccard_days	= isset($_POST['cc_days']) ? $_POST['cc_days'] : 30;
                
			DB::table('user_setting')
				->where('user_id',$user_id)
				->update(array(
					'perpage'		=>	Input::get('result_page'),
					'newtab'		=>	$new_tab,
					'show_warning'	=>	$warnings,
					'cc_days'		=>  $ccard_days
				));
			$userid 		=  Auth::user()->id;
			$usersettings 	= DB::select("SELECT * FROM user_setting where user_id = $userid");
			Session::put('perpage', $usersettings[0]->perpage );
			Session::put('newtab', $usersettings[0]->newtab );
			Session::put('show_warning', $usersettings[0]->show_warning );
			Session::put('cc_days', $usersettings[0]->cc_days );
			$perpage = Session::get('perpage');
			$newtab = Session::get('newtab');
			$show_warning = Session::get('show_warning');
			$cc_days = Session::get('cc_days');
			return Redirect::to('user-setting/')->with('message', 'Setting has been updated');
	}
	
	/**
	 * Update the specified user in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function updateuser() {
		
		$user = Auth::user();
		
		$id 					= Input::get('id');
		/*
		$title 					= Input::get('title');
		$homepage 				= Input::get('homepage');
		$address 				= Input::get('address');
		$phone 					= Input::get('phone');
		$mobile 				= Input::get('mobile');
		$location 				= Input::get('location');
		$business_address 		= Input::get('business_address');
		$other 					= Input::get('other');
		$bank_name 				= Input::get('bank_name');
		$bank_user 				= Input::get('bank_user');
		$bank_no 				= Input::get('bank_no');
		*/
		
		//IF SESSION MENTAIN THEN REDIRECTED TO ADMIN/PARTNER DASHBOARD
		$userType = getCurrentUserRole();
		
		if (!empty($id)){
			Partner::updatePartner(Input::get());
		} else {
			return Redirect::to($userType.'/edit-profile/')
						->with('message', 'Update successfully');
		}
		return Redirect::to($userType.'/edit-profile/');
		
	}		
	public function updatePassword() {
		$user = Auth::user();	
		$id 				= Input::get('id');
		$partnerData 				= Partner::find($id);
		
		//IF SESSION MENTAIN THEN REDIRECTED TO ADMIN/PARTNER DASHBOARD
		$userType = getCurrentUserRole();
		
		
		if(!empty($partnerData)){
			// $userDbPassword		=	$password['password'];
			// var_dump($partnerData);exit();
			$oldPassword 		= Input::get('password');
			$newPassword		= Input::get('newpassword');
			$confirmPassword	= Input::get('repeatnewpassword');
			
			if ( (!empty($newPassword) && !empty($confirmPassword)) && ($newPassword == $confirmPassword) ){
			
				if ( md5($oldPassword.SECURE_MD5_STR) == $partnerData->password ) {
						
						$partnerData->password = md5($newPassword.SECURE_MD5_STR);
						$partnerData->save();
						return Redirect::to($userType.'/edit-password/')
							->with('message', 'Password Change Successfully');
				}
				else {
					return Redirect::to($userType.'/edit-password/')
						->with('message', 'Old Password Is not match');
				}
			} else {
				return Redirect::to($userType.'/edit-password/')
							->with('message', 'Confirm Password not Match');
			}
		}
		return Redirect::to($userType.'/edit-password/');
	}

	/**
	 * Remove the specified user from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	
	public function destroy($id)
	{
		User::destroy($id);

		return Redirect::route('users.index');
	}

	public function request() {
	  if ($this->isPostRequest()) {
		$response = $this->getPasswordRemindResponse();

		if ($this->isInvalidUser($response)) {
		  return Redirect::back()
			->withInput()
			->with("error", Lang::get($response));
		}
		return Redirect::back()->with("status", Lang::get($response));
	  }
	  $this->layout->content = View::make('login/request');
	}

	public function reset($token) { 
		if ($this->isPostRequest()) { 
			$credentials = Input::only( "email", "password", "password_confirmation" ) + compact("token"); 
			$response = $this->resetPassword($credentials); 
			if ($response === Password::PASSWORD_RESET) { 
				#return Redirect::route("login.showLogin");
				
				#return "password changed";
				return Redirect::to('login')->with('message','Password Change Successfully');
			} 
			return Redirect::back() ->withInput() ->with("error", Lang::get($response)); 
		} 
		$this->layout->title = "Reset Password";
		$this->layout->content 	= View::make("login/reset", compact("token")); 
		
	}

	protected function resetPassword($credentials) { 
		return Password::reset($credentials, function($user, $pass) { 
			$user->password = Hash::make($pass); $user->save(); 
		}); 
	}
	
	protected function getPasswordRemindResponse() {
            
		if(Input::get("email") != '')
		{
			$user_data = DB::select( DB::raw("SELECT title, remember_token from customers where email = :email "), array('email'=>Input::get("email")));
                
				if( count($user_data) > 0 ) {
					Password::remind(Input::only('email'), function($message)
					{
						$message->subject('Do You Forget Password');
						$message->from(TT_EMAIL, 'Topdeal Biz Partner');
						
					});
				}
				else {
					return "email not found";
				}
		
            }
	}
  
	protected function isInvalidUser($response)	{
  		return $response === Password::INVALID_USER;
	}
	
	protected function isPostRequest()	{
    	return Input::server("REQUEST_METHOD") == "POST";
  	}
}