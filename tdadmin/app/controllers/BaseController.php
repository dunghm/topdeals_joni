<?php

class BaseController extends Controller {

	protected $layout = 'layout.master';
	
	protected $rules			= array();
	protected $validator		= array();
	protected $error_messages 	= array();
	
	private $class_name, $model_name, $action;
	private $except = array('password', 'password_confirm', 'csrf');
	
	private $validation_messages = array(
	// 		'required' => '<p class="help-block">The :attribute field is required.</p>',
			'required' => 'The :attribute field is required.',
			'same'    => 'The :attribute and :other must match.',
			'size'    => 'The :attribute must be exactly :size.',
			'between' => 'The :attribute must be between :min - :max.',
			'in'      => 'The :attribute must be one of the following types: :values',
	);
	
	
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	function __construct()
	{
		// do_admin_logging();
		
		$currentRouteAction = Route::currentRouteAction();
		
		if(!empty($currentRouteAction) && (false !== stripos($currentRouteAction, '@'))){
			$currentRouteAction_parts = explode('@', $currentRouteAction);
			$this->action = array_pop($currentRouteAction_parts);
		}
		
		$class_name = get_class($this);
		$this->class_name = $class_name;
		
		if(!empty($class_name)){
				
			# Convert Camel case name into Underscore names
			# e.g, UsersController will become -> users_controller
			$class_u = Inflector::underscore($class_name);
				
			$model_name = $class_name;
				
			# Check if there is any underscore found
			if(stripos($class_u, '_')){
		
				# Break names into array
				$model_parts = explode('_', $class_u);
			
				# Check if last name is "controller", then remove it
				$last_part_name = $model_parts[count($model_parts)-1];
				if($last_part_name == 'controller')
					unset($model_parts[count($model_parts)-1]);
	
				# Pick first name wh
				$model_name = implode('_',$model_parts);
				
			}
				
			$model_name = Inflector::camelize(Inflector::singularize($model_name));
			$this->model_name = $model_name;
		
		}
		
	}
	 
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	
	/*
	 * @author: NoumanArshad
	 * @date	Feb04,2015
	 * 
	 * @desc
	 * 
	 * 	Validation functions
	 * */
	
	protected function getModelName(){
		return $this->model_name;
	}
	
	function setRules($rules){
		$this->rules = $rules;
	}
	
	function getRules($model=null){
	
		$rules = array();
	
		if(null == $model)
			$model = $this->model_name;
	
		if(!empty($model) && class_exists($model)){
			if(!empty($this->action) && isset($model::$rules[$this->action])){
				$rules = $model::$rules[$this->action];
			}else{
				$rules = $model::$rules;
			}
		}
	
		return $rules;
	}
	
	function addValidation($rule){
	
	}
	
	/*
	 * References
	 * 	http://stackoverflow.com/questions/21291395/laravel-4-redirect-back-to-the-same-page-where-the-request-comes-from
	 * 	https://scotch.io/tutorials/laravel-form-validation
	 *
	 * $options['model'] :
	 * 	- if not provided then Model name will be of controller being using for POST request
	 * 		it will use to pick rules if exists in the model
	 *
	 * $options['rules'] :
	 * 	- if passed, will be use
	 *  - if not passed, will automatic check and pick if any rule exists with the same name of action in Model
	 *
	 *
	 * */
	public function validation(
		$options = array(
			/*
			 * $input_array
			 * rules		= Rule array (if passed will be picked up, otherwise auto-pick from default model)
			 * model		= Name of the model pick validation rules from
			 * XXX - controller 	= Name of the controller
			 * XXX - action		= Action to be redirect
			 * XXX - from			= Name of the form, from where the form is submitted (as we will have to send user back to the same form with
			 * 					validation error messages)
			 * XXX - go 	= redirect to [Redirect::to(go)]
			 * */
		)
	){
	
		# Import all params of $options array
		extract($options);

		# if model name is not passed then pick default one (of controller)
		if(!isset($model))
			$model = $this->model_name;

		if(!isset($input_array))
			$input_array = Input::all();

		# If customer rules are not passed then pick default one
		if(!isset($rules)){
				
			# Rules may not be define in any model, so there is no validation :)
			$rules = array();
				
			# Fetch the rules defiend in the Model
			if(class_exists($model)){
				if(!empty($this->action) && isset($model::$rules[$this->action])){
					$rules = $model::$rules[$this->action];
				}else{
					$rules = $model::$rules;
				}
			}
		}

		$validator = Validator::make( $input_array, $rules);
	
		if(!empty($rules)){
				
			// check if the validator failed -----------------------
			if ($validator->fails()) {
					
				Redirect::back()
							->withMessage(sprintf('%s validation error', $this->model_name))
							->withErrors($validator)
							->withInput(Input::except($this->except));
				return false;
			}

		}

		return true;
	
	}
	
	function sendError($message, $location=null){
		if($location==null)
			return Redirect::back()
				->withMessage(sprintf('%s', $message))
				->withInput(Input::except($this->except));
		else
			return Redirect::to($location)->with('message', $message);
		
	}
	
	function goBack(){
		return Redirect::back();
	}

	
	
	
	
}