<?php
/**
*@author:Dashboard
*@desc: *	function index loading dashboard/index and counting customers from the customers table.
*		*	function get_dashboard_Datatable() it is showing customers list with data table
**/
class PartnerDashboardController extends BaseController {
	
	/*
	 * __construct
	 */
	public function __construct() {
	}
	
	/**
	*@author:VaqasUddin
	*@desc: it will load index page and including customers
	**/
	public function index()
	{
		$data['partner'] = array();
		$this->layout->title	= "Partner Dashboard";
		$this->layout->content	= View::make('partner/dashboard/index',compact('partner'));
	}
}