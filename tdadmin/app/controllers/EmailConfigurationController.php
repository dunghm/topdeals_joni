<?php
class EmailConfigurationController extends BaseController {

	/*
	 * __construct
	 */
	public function __construct() {
		
	}
	
	public function index() 
	{
		$data = '';
		$this->layout->title 	= 'Email Configuration';		
		$this->layout->content 	= View::make('email_configuration/index', compact('data'));
	}
	/*
	* Ajax Request for server side data
	*/	
	public function getDatatable()
	{

		# call function
		return Configuration::getEmailConfiguration(Input::get());
		
	}

	/*
	 * enable disabled status
	 */
	 public function enable_disable() 
	 {
		$email_id = Input::get('email_id');
		return Configuration::updateStatusById($email_id);
	 }
}