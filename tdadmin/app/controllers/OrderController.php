<?php
class OrderController extends BaseController 
{

	/*
	 * __construct
	 */
	public function __construct() 
	{

	}
	
	public function index() 
	{
		$data = '';
		 
		//CUSTOM PAGINATION FOR ORDER LISTING
		$perPage    =    RECORD_PERPAGE;
		$offset     =    "";
            
		if(isset($_GET['page'])){
			
			$currentPage    =    $_GET['page'];
			$offsetNum		=	0;
			if($currentPage>=1){
				$currentPage    =    $currentPage - 1;                    
				$offsetNum	  =    $perPage * $currentPage;
			}

			$offset    =    "OFFSET ".$offsetNum;
		}else{
			//CLEAR SEARCH RESULT FOR DEFAULT PAGE LOAD
			Session::forget('sr_order_num');
			Session::forget('sr_user');
			Session::forget('sr_deal_num');
			Session::forget('sr_delivery_type');
			Session::forget('sr_delivery_status');
		}
		
		// MAINTAIN SESSION FOR CHUMPER COLLECTION
		Session::put('order_perPage', $perPage);
		Session::put('order_offset',  $offset);
		
		
		 
		//SEARCH FIELD
		$data['deliveryStatusArray']	=	deliveryStatusArray();
		$data['optionDelivery']			=	optionDelivery();
		
		//SEARCH FORM SUBMIT MAINTAIN SESSION FOR CHUMPER COLLECTION
		if(isset($_POST['search'])){
			
			$sr_order_num		=	rtrim($_POST['order_num']);
			$sr_user			=	rtrim($_POST['user']);
			$sr_deal_num		=	rtrim($_POST['deal_num']);
			$sr_delivery_type	=	rtrim($_POST['delivery_type']);
			$sr_delivery_status	=	rtrim($_POST['delivery_status']);
			
			if(!empty($sr_order_num)){
				Session::put('sr_order_num', 		$sr_order_num);
			}
			
			if(!empty($sr_user)){
				Session::put('sr_user', 			$sr_user);
			}
			
			if(!empty($sr_deal_num)){
				Session::put('sr_deal_num', 		$sr_deal_num);
			}
			
			if(!empty($sr_delivery_type)){
				Session::put('sr_delivery_type', 	$sr_delivery_type);
			}
			
			if(!empty($sr_delivery_status)){
				Session::put('sr_delivery_status', 	$sr_delivery_status);
			}
		}
		
		
		//RESET SEARCH
		if(isset($_POST['reset'])){
			//CLEAR SEARCH RESULT FOR DEFAULT PAGE LOAD
			Session::forget('sr_order_num');
			Session::forget('sr_user');
			Session::forget('sr_deal_num');
			Session::forget('sr_delivery_type');
			Session::forget('sr_delivery_status');
		}
			
		
		$data['pagination']	=	Order::orderCustomPagination($perPage,$offset);
		
		$this->layout->title 	= trans('localization.paid_order');		
		$this->layout->content 	= View::make('order/index', compact('data')); 
	}
	
	/*
	* Ajax Request for server side data
	*/	
	 public function getDatatable()
	{
		# call function
		return Order::getSections(Input::get());
		
	} 
	
	
	
	#getOrder fare by checking if order items have more express or not
	function getOrderFare($orderId=0,$orderItemId=0,$fare=0,$postedQuantity=0,$callFrom='')
	{
		$addFareDifferce = false;
		####get order items by order id#####
			
		$orderFare = $fare;
		$fare_count = $orderItemQuantity =$fareDifference= 0;
		
		#get data of current order item
		$currentOrderItemArray = OrderItems::getOrderItemsById($orderItemId);
			
		if(!empty($currentOrderItemArray))
		{
			
			$orderItem = $currentOrderItemArray[0];
			
			#quantity
			if($orderItem->quantity)
			{
				$orderItemQuantity = $orderItem->quantity;
			}	
		}	
		
		#check order have more express items
		$fare_count = checkOrderHaveMoreExpressItems($orderId,$orderItemId);
		
		#if record exist except this order item id then fare is zero.
		if($fare_count > 0)
		{
			$fare = 0;
			
			#for full order refund its true
			$addFareDifferce = true;
			
			#for single order refund
			if($callFrom=="orderRefund")
			{
				$addFareDifferce = false;
				#fare should only be add once oredr item quantity is equal to post quantity for single order  
				if($orderItemQuantity==$postedQuantity)
				{
					$addFareDifferce = true;
				}
			}
		}
		
		#if record not exist except this order item id then check order quantiy is greater than posted quantity or not if yes then fare is zero.
		if($fare_count==0 && $callFrom=="orderRefund")
		{
			//check order quantity is greater than posted quantity 
			// 4 >3
			if($orderItemQuantity>$postedQuantity)
			{
				$fare = 0;
			}
		}
		
		#add fare difference if other deal have fare with less than of this order item fare
		if($addFareDifferce)
		{
			$fareDifference = getFareDifference($orderId,$orderItemId,$orderFare);
		}
		
		$fare = $fare+$fareDifference;
		
		return $fare;
	}
	
	public function fullOrderRefundForm()
	{
		$this->layout->title 	= trans('order_refund.title');
		
		$orderId 	 		= Input::get('orderId');
		
		$orderCard = 0;
		#get Order detail
		$orders 		= Order::getOrderById($orderId);
		if(!empty($orders))
		{
			$order = $orders[0];
			
			#card
			if($order->card)
			{
				$orderCard = $order->card;
			}
		}		
		$orderItemDetails   = OrderItems::getOrderItemDetailByOrderId($orderId);
		#refun options
		$refundOption = array(
								'credit' => 'Refund to the account balance',
								'online' => 'Refunded by other means',
							  );
		
		echo View::make('order/full_order_refund', compact('orderId','orderItemDetails','refundOption','orderCard'));
        exit;
	}
	
	
	function fullOrderRefund()
	{
		$refundedOrders =$refundedOrderItems= array();
		$jsData = array();
		$jsData['success'] = false;
		$jsData['error'] = '';
		$jsData['redirect'] = '';
		$jsError = '';
		$orderItemUpdate = true;
		
		$orderId 	 		= Input::get('orderId');
		$reasonRefund 	 	= Input::get('reasonRefund');
		$refundPolicy 		= Input::get('refundPolicy');
		
		
		if (!$refundPolicy) 
		{
            $jsError.= 'Refund policy is required!<br/>';
        }
		
		if($jsError)
		{
			$jsData['error'] = $jsError;
		}
		else
		{
			$totalRefund =$orderOrigin=$fare=$userId=$payId=$orderCredit=$orderCard=$orderQuantity=0;
			$orderState =$email=$realname=$lastname=$orderService=$payTime='';
			$callFrom='fullOrderRefund';
			
			
			#get Order detail
			$orders 		= Order::getOrderById($orderId);
			if(!empty($orders))
			{
				$order = $orders[0];
				
				#state
				if($order->state)
				{
					$orderState = $order->state;
				}
				
				#origin
				if($order->origin)
				{
					$orderOrigin = $order->origin;
				}
				
				#fare
				if($order->fare)
				{
					$fare = $order->fare;
				}
				
				#userId
				if($order->user_id)
				{
					$userId = $order->user_id;
				}
####################################################Email param##############################################
		
				#realname
				if($order->realname)
				{
					$realname = $order->realname;
				}
				
				#lastname
				if($order->lastname)
				{
					$lastname = $order->lastname;
				}
				
				#pay_id
				if($order->pay_id)
				{
					$payId = $order->pay_id;
				}
				
				
				#credit
				if($order->credit)
				{
					$orderCredit = $order->credit;
				}
				
				#card
				if($order->card)
				{
					$orderCard = $order->card;
				}
				
				#service
				if($order->service)
				{
					$orderService = $order->service;
				}
				
				#pay_time
				
				if($order->pay_time)
				{
					$payTime = date('j.n.Y H\hi',$order->pay_time);
				}
				
				#order quantity
				if($order->quantity)
				{
					$orderQuantity =$order->quantity;
				}
	
		$refundedOrders = array
								(
									'realname'		=> $realname,
									'lastname'		=> $lastname,
									'payId'			=> $payId,
									'origin'    	=> $orderOrigin,
									'orderFare' 	=> $fare,
									'orderCredit' 	=> $orderCredit,
									'orderCard'		=> $orderCard,
									'orderService'  => $orderService,
									'payTime'		=> $payTime	
								);
##################################################/Email param################################################				
				
				
				
				$userDetails = User::getUserByUserId($userId);
				if(!empty($userDetails))
				{
					$userDetail = $userDetails[0];
					
					if($userDetail->email)
					{
						$email = $userDetail->email;
					}
					
					
				}
				
			}	
			
			
			#get order items by order id
			$orderItems  = OrderItems::getOrderItemsByOrderId($orderId);
			
			$refundedOrderItem = array();
			$loopCounter =0;
			if(is_array($orderItems) && count($orderItems) >0)
			{
				foreach($orderItems as $orderItem)
				{
					
					$orderItemId 				= $orderItem->id;
					$refundQuantity				= $orderItem->quantity;
					$teamId						= $orderItem->team_id;
					$optionId 					= $orderItem->option_id;
					$orderItemDelivery 			= $orderItem->delivery;
					$orderItemDeliveryStatus 	= $orderItem->delivery_status;
					$orderItemTotal 			= $orderItem->total;
					$orderItemfare 			    = $orderItem->fare;
					$cashbackAmount 			= $orderItem->cashback_amount;
					$price 						= $orderItem->price;
					
					
					#order item  minus order fare
					if($orderItemTotal>$orderItemfare && $orderItemfare>0)
					{
						$orderItemTotal = $orderItemTotal-$orderItemfare;
					}
					
					##########Email param#########
					#team detail
					$teamTitle  = '';
					$team = Team::getTeamById($teamId);
					if(!empty($team))
					{
						$teamData = $team[0];
						if($teamData->title)
						{
							$teamTitle = $teamData->title;
						}
					}

					$optionSummary = '';
					#Option detail	
					$teamMulti = Team::getMultiTeamInfoById($optionId);	
					if(!empty($teamMulti))
					{
						$teamMultiData = $teamMulti[0];
						if($teamMultiData->summary)
						{
							$optionSummary = $teamMultiData->summary;
						}
					}
					
					
					
					$refundedOrderItem['orderItemQuantity'] = $refundQuantity;	
					$refundedOrderItem['orderItemTotal']    = $orderItemTotal;	
					$refundedOrderItem['teamId']    		= $teamId;	
					$refundedOrderItem['teamTitle']    	    = $teamTitle;
					$refundedOrderItem['optionId']    	    = $optionId;						
					$refundedOrderItem['optionSummary']     = $optionSummary;
					$refundedOrderItem['cashBack']			= $cashbackAmount;	
					
					##########Email param#########
					
					
					
					#check order state
					if ( $orderState =='pay') 
					{
						 $totalRefund = $this->getTotalRefundAmount($orderId,$orderItemId,$reasonRefund,$refundPolicy,$refundQuantity,$callFrom);
					}
					 
					
					 $refund = false;
					 #deal option update
					 if(is_numeric($optionId) && $optionId>0)
					 {
						#update deal option Quantity or Stock (function call from helper)
						$teamMultiUpdated = dealOptionQuantityUpdate($optionId,$refundQuantity,$orderItemDelivery,$orderItemDeliveryStatus);
						if($teamMultiUpdated)
						{
							$refund = true;
						}
					 } 
					 else
					 {
						#update deal Quantity or Stock (function call from helper)
						$teamUpdated = dealQuantityUpdate($teamId,$refundQuantity,$orderItemDelivery,$orderItemDeliveryStatus);
						if($teamUpdated)
						{
							$refund = true;
						}
						
					 }
					 
					 #delete order items 
					 if($refund)
					 {
						
						#cash back amout
						$totalCashBackAmount = getcashBackAmount($cashbackAmount,$refundQuantity,$refundQuantity);
						$orderfare = $this->getOrderFare($orderId,$orderItemId,$fare,$refundQuantity,$callFrom);
						
						# insert order refund entries	
						insertOrderRefundEntries($orderId,$orderItemId,$refundQuantity,$orderfare,$totalRefund,$totalCashBackAmount);
						
						#delete Coupon for refund items
						$couponParam=array('orderItemId'=>$orderItemId,'callFrom'=>$callFrom);
						Coupon::deleteRefundItemsCoupon($couponParam);
						
						#delete if items quantity total refund
						$orderItemUpdate = OrderItems::deleteOrderItem($orderItemId);
						#update order 
						if($orderOrigin>=$totalRefund)
						{
							#update order quantity amd status.
							$orders = updateOrderQuantity($orderId,$totalRefund,$refundQuantity,$orderfare,$price,$callFrom);	
						}
						
					 }
					 $refundedOrderItems[$loopCounter]     = $refundedOrderItem;		
					 $loopCounter++;
					 
					 #stock status update
					 stockStatusUpdate($teamId,$optionId);
				}
			
			}
			
			$successMessage = $errorMessage ='';
			if($orderItemUpdate)
			{
				
				$orderRefundEmailParam = array
											  (
												'refundPolicy'=>$refundPolicy,
												'to'=>$email
											   );
				
				
				$subject = "Annulation de votre commande";
				#get body from template
				$emailBody = View::make('email_template/order/fullOrderRefund', compact('orderRefundEmailParam','refundedOrders','refundedOrderItems'));
				
				#send email on order refund
				fullOrderRefundEmail($email,$subject,$emailBody);
				#send email on order refund
				#fullOrderRefundEmail($orderRefundEmailParam,$refundedOrders,$refundedOrderItems);	
				
				$successMessage = "Order refund successfully";
			}
			else
			{
				$errorMessage = "order not refund successfully";
			}
			
			$jsData['success'] 			= true;
			$jsData['successMessage'] 	= $successMessage;
			$jsData['errorMessage'] 	= $errorMessage;
			
		}
		
		$jsData = json_encode($jsData);

		echo $jsData;
		exit;
		
	}
	
	#order refund Form
	public function refundForm()
	{
		$this->layout->title 	= trans('order_refund.title');
		
		$orderId 	 = Input::get('orderId');
		$orderItemId = Input::get('orderItemId');
		
		$teamId =$quantity=$fare=$orderfare=$price=$totalAmount=$optionId=$orderCard=$orderQuantity=$cashbackAmount=$orderOrigin=0;
		$title =$optionTitle='';
		
		
		$orders 		= Order::getOrderById($orderId);
		#orders
		
		if(!empty($orders))
		{
			$order = $orders[0];
			
			#fare
			if($order->fare)
			{
				$fare = $order->fare;
			}
			#orderCard
			if($order->card)
			{
				$orderCard = $order->card;
			}
			#orderQuantity
			if($order->quantity)
			{
				$orderQuantity = $order->quantity;
			}
			
			#Order origin
			if($order->origin)
			{
				$orderOrigin = $order->origin;
			}
		}	
		
		#get orderItems by id
		$orderItems 		= OrderItems::getOrderItemsById($orderItemId);
		if(!empty($orderItems))
		{
			
			$orderItem = $orderItems[0];
			
			#team id
			if($orderItem->team_id)
			{
				$teamId = $orderItem->team_id;
			}
			
			#option id
			if($orderItem->option_id)
			{
				$optionId = $orderItem->option_id;
			}
			
			#quantity
			if($orderItem->quantity)
			{
				$quantity = $orderItem->quantity;
			}
			
			if($orderItem->price)
			{
				$price = $orderItem->price;
			}
			
			#cashbackAmount
			if($orderItem->cashback_amount)
			{
				$cashbackAmount = $orderItem->cashback_amount;
			}
		}

		#get order fare
		$postedQuantity=0; // not required from form
		$callFrom='orderRefund';
		$orderfare = $this->getOrderFare($orderId,$orderItemId,$fare,$quantity,$callFrom);
			
		$totalAmount = ($price*$quantity)+$orderfare;
		$totalPaid = $totalAmount;
		
		#promocode
		$promoCode = getPromoCardAmount($orderCard,$orderQuantity,0,$orderOrigin,$fare,$price);
		if($promoCode)
		{
			$totalPaid = $totalPaid-($promoCode*$quantity);
		}
		#cash back
		$totalCashBackAmount = getcashBackAmount($cashbackAmount,$quantity);
		if($totalCashBackAmount)
		{
			$totalPaid = $totalPaid-($totalCashBackAmount*$quantity);
		}
		
		
		#get team detail
		$teamData	=	Team::getTeamById($teamId);	
		if(!empty($teamData))
		{
			$team = $teamData[0];
			
			if($team->title)
			{
				$title = $team->title;
			}
			
		}
		
		#get option detail
		$teamOptionData	=	Team::getMultiTeamById($optionId);	
		
		if(!empty($teamOptionData))
		{
			$teamOption = $teamOptionData[0];
			
			if($teamOption->title)
			{
				$optionTitle = $teamOption->title;
			}
			
		}
		
		#refun options
		$refundOption = array(
								'credit' => 'Refund to the account balance',
								'online' => 'Refunded by other means',
							  );
		
		echo View::make('order/refund', compact('refundOption','orderId','orderItemId','title','totalAmount','quantity','orderfare','price','optionTitle','promoCode','totalPaid','totalCashBackAmount'));
        exit;
	}
	
	#order refund
	public function refund()
	{
		$jsData = array();
		$jsData['success'] = false;
		$jsData['error'] = '';
		$jsData['redirect'] = '';
		$jsError = '';
		$refundedOrders =$refundedOrderItems= array();
		
		$orderId 	 		= Input::get('orderId');
		$refundOrderItemId 	= Input::get('refundOrderItemId');
		$reasonRefund 	 	= Input::get('reasonRefund');
		$refundPolicy 		= Input::get('refundPolicy');
		$refundQuantity 	= Input::get('refundQuantity');
		
		
		if (!$refundQuantity || !is_numeric($refundQuantity) || $refundQuantity < 0) 
		{
            $jsError.= 'Quantity is required! <br/>';
        }
		
		if (!$refundPolicy) 
		{
            $jsError.= 'Refund policy is required!<br/>';
        }
		
		
		if($jsError)
		{
			$jsData['error'] = $jsError;
		}
		else
		{
	
			#get order by id
			$orderState =$orderItemDelivery =$orderItemDeliveryStatus=$email=$realname=$lastname=$orderService=$payTime=$teamTitle =$optionSummary ='';
			$optionId= 'NULL';
			$callFrom='orderRefund';
			$orderOrigin =$quantity=$teamId=$fare=$orderfare=$totalRefund=$userId=$payId=$orderCredit=$orderCard=$orderQuantity=$cashbackAmount=$price=0;
			$orders 		= Order::getOrderById($orderId);
			if(!empty($orders))
			{
				$order = $orders[0];
				
				#state
				if($order->state)
				{
					$orderState = $order->state;
				}
				
				#origin
				if($order->origin)
				{
					$orderOrigin = $order->origin;
				}
				
				#fare
				if($order->fare)
				{
					$fare = $order->fare;
				}
				
####################################################Email param##############################################
		
				#realname
				if($order->realname)
				{
					$realname = $order->realname;
				}
				
				#lastname
				if($order->lastname)
				{
					$lastname = $order->lastname;
				}
				
				#pay_id
				if($order->pay_id)
				{
					$payId = $order->pay_id;
				}
				
				
				#credit
				if($order->credit)
				{
					$orderCredit = $order->credit;
				}
				
				#card
				if($order->card)
				{
					$orderCard = $order->card;
				}
				
				#service
				if($order->service)
				{
					$orderService = $order->service;
				}
				
				#pay_time
				
				if($order->pay_time)
				{
					$payTime = date('j.n.Y H\hi',$order->pay_time);
				}
				
				#order quantity
				if($order->quantity)
				{
					$orderQuantity =$order->quantity;
				}
				 
				
				$refundedOrders = array
								(
									'realname'		=> $realname,
									'lastname'		=> $lastname,
									'payId'			=> $payId,
									'origin'    	=> $orderOrigin,
									'orderFare' 	=> $fare,
									'orderCredit' 	=> $orderCredit,
									'orderCard'		=> $orderCard,
									'orderService'  => $orderService,
									'payTime'		=> $payTime,
									'orderQuantity'	=> $orderQuantity
								);
				
###############################################/Email param############################################						
				#userId
				if($order->user_id)
				{
					$userId = $order->user_id;
				}
				
				$userDetails = User::getUserByUserId($userId);
				if(!empty($userDetails))
				{
					$userDetail = $userDetails[0];
					
					if($userDetail->email)
					{
						$email = $userDetail->email;
					}
					
					
				}	
				
			}
			
			#order items
			$orderItems 		= OrderItems::getOrderItemsById($refundOrderItemId);
			
			if(!empty($orderItems))
			{
				
				$orderItem = $orderItems[0];
				
				#quantity
				if($orderItem->quantity)
				{
					$quantity = $orderItem->quantity;
				}
				
				#option Id
				if($orderItem->option_id)
				{
					$optionId = $orderItem->option_id;
				}
				
				#team id
				if($orderItem->team_id)
				{
					$teamId = $orderItem->team_id;
				}
				
				#delivery
				if($orderItem->delivery)
				{
					$orderItemDelivery = $orderItem->delivery;
				}
				
				#delivery_status
				if($orderItem->delivery_status)
				{
					$orderItemDeliveryStatus = $orderItem->delivery_status;
				}
				
				#cashBack
				if($orderItem->cashback_amount)
				{
					$cashbackAmount = $orderItem->cashback_amount;
				}
				
				#price
				if($orderItem->price)
				{
					$price = $orderItem->price;
				}
				
					$team = Team::getTeamById($teamId);
					if(!empty($team))
					{
						$teamData = $team[0];
						if($teamData->title)
						{
							$teamTitle = $teamData->title;
						}
					}

					#Option detail	
					$teamMulti = Team::getMultiTeamInfoById($optionId);	
					if(!empty($teamMulti))
					{
						$teamMultiData = $teamMulti[0];
						if($teamMultiData->summary)
						{
							$optionSummary = $teamMultiData->summary;
						}
					}
			}
			
################################################calculation start#################################################			
			
			#check order state is pay and calculate refund amount
			if ( $orderState =='pay') 
			{
				$totalRefund = $this->getTotalRefundAmount($orderId,$refundOrderItemId,$reasonRefund,$refundPolicy,$refundQuantity,$callFrom);
			}
			
			
			//if its an option then deduct option 
			
			$refunded = false;
			if(is_numeric($optionId) && $optionId>0)
			{
				#update deal option Quantity or Stock (function call from helper)
				$teamMultiUpdated = dealOptionQuantityUpdate($optionId,$refundQuantity,$orderItemDelivery,$orderItemDeliveryStatus);
				if($teamMultiUpdated)
				{
					$refunded = true;
				}
			}
			else
			{
				#update deal Quantity or Stock (function call from helper)
				$teamUpdated = dealQuantityUpdate($teamId,$refundQuantity,$orderItemDelivery,$orderItemDeliveryStatus);
				if($teamUpdated)
				{
					$refunded = true;
				}
			}
			#delete order item record
			$orderItemUpdate = '';
			if($refunded)
			{
				
				#cash back amout
				$totalCashBackAmount = getcashBackAmount($cashbackAmount,$quantity,$refundQuantity);
				#get order fare
				$orderfare = $this->getOrderFare($orderId,$refundOrderItemId,$fare,$refundQuantity,$callFrom);
				
				insertOrderRefundEntries($orderId,$refundOrderItemId,$refundQuantity,$orderfare,$totalRefund,$totalCashBackAmount);
				
				#delete Coupon for refund items
				$couponParam=array('orderItemId'=>$refundOrderItemId,'quantity'=>$refundQuantity,'teamId'=>$teamId,'optionId'=>$optionId,'callFrom'=>$callFrom);
				Coupon::deleteRefundItemsCoupon($couponParam);
				
				#update order item 
				$orderItemUpdate = orderItemUpdate($refundOrderItemId,$quantity,$refundQuantity,$totalRefund,$orderQuantity,$orderCard,$totalCashBackAmount);
				
				#update order 
				if($orderOrigin>=$totalRefund)
				{
					#update order quantity and also check order item count.
					$orders = updateOrderQuantity($orderId,$totalRefund,$refundQuantity,$orderfare,$price,$callFrom);	
				}
			
			}
			
			$successMessage = $errorMessage ='';
			if($orderItemUpdate)
			{
				#stock status update
				stockStatusUpdate($teamId,$optionId);
				#order item  minus order fare
				if($totalRefund>$orderfare && $orderfare>0)
				{
					$totalRefund = $totalRefund-$orderfare;
				}
				
				
				$refundedOrderItems['orderItemQuantity']	= $refundQuantity;
				$refundedOrderItems['orderItemTotal']		= $totalRefund;
				$refundedOrderItems['teamId']				= $teamId;
				$refundedOrderItems['optionId']				= $optionId;
				$refundedOrderItems['teamTitle']			= $teamTitle;
				$refundedOrderItems['optionSummary']		= $optionSummary;
				$refundedOrderItems['orderfare']			= $orderfare;
				$refundedOrderItems['cashBack']				= $totalCashBackAmount;
				$refundedOrderItems['price']				= $price;
				
				
				$orderRefundEmailParam = array
											  (
												'refundPolicy'=>$refundPolicy,
												'to'=>$email
											   );
				
				$subject = "Order Item Refunded";
				#get body from template
				$emailBody = View::make('email_template/order/singleOrderRefund', compact('orderRefundEmailParam','refundedOrders','refundedOrderItems'));
				
				#send email on order refund
				singleOrderRefundEmail($email,$subject,$emailBody);
				#singleOrderRefundEmail($orderRefundEmailParam,$refundedOrders,$refundedOrderItems);//,$refundedOrderItems
				
				$successMessage = "Order refund successfully";
			}
			else
			{
				$errorMessage = "order not refund successfully";
			}
			
			$jsData['success'] 			= true;
			//$jsData['redirect'] 		= "orderlist";
			$jsData['successMessage'] 	= $successMessage;
			$jsData['errorMessage'] 	= $errorMessage;
		}
		
		
		$jsData = json_encode($jsData);

		echo $jsData;
		exit;
	}
	
	
	#get total refund  amount
	public function getTotalRefundAmount($orderId=0,$refundOrderItemId=0,$reasonRefund='',$refundPolicy='',$refundQuantity=0,$callFrom='')
	{
		$price=$quantity=$fare=$totalRefund=$userId=$orderfare=$orderQuantity=$orderCard=$promoCode=$cashbackAmount=$orderOrigin=0;
		
		#get order by id
		$orders 		= Order::getOrderById($orderId);
		#orders
		
		if(!empty($orders))
		{
			$order = $orders[0];
			
			#fare
			if($order->fare)
			{
				$fare = $order->fare;
			}
			
			#userId
			if($order->user_id)
			{
				$userId = $order->user_id;
			}
			
			#orderQuantity
			if($order->quantity)
			{
				$orderQuantity = $order->quantity;
			}
			
			#order card
			if($order->card)
			{
				$orderCard = $order->card;
			}
			
			#order origin
			if($order->origin)
			{
				$orderOrigin = $order->origin;
			}
			
		}
		
		#get orderItems by id
		$orderItems 		= OrderItems::getOrderItemsById($refundOrderItemId);
		
		if(!empty($orderItems))
		{
			
			$orderItem = $orderItems[0];
			
			#price
			if($orderItem->price)
			{
				$price = $orderItem->price;
			}
			
			#quantity
			if($orderItem->quantity)
			{
				$quantity = $orderItem->quantity;
			}
			
			if($orderItem->cashback_amount)
			{
				$cashbackAmount = $orderItem->cashback_amount;
			}
			
			#get order fare
			$orderfare = $this->getOrderFare($orderId,$refundOrderItemId,$fare,$refundQuantity,$callFrom);
			
		}
		
		#order card discount
		$totalDiscount=0;
		/* $orderCards		= Order::getOrderCardByOrderId($orderId);
		if(!empty($orderCards))
		{
			foreach($orderCards as $oc)
			{
               $totalDiscount += $oc->card_value;
            }
		} */
		
		$promoCode = getPromoCardAmount($orderCard,$orderQuantity,$refundQuantity,$orderOrigin,$fare,$price);
		$totalCashBackAmount = getcashBackAmount($cashbackAmount,$quantity,$refundQuantity);
		
		$userMoney = 0;
		$usercredit = 0;
	
		$totalRefund = (($price*$refundQuantity)+ $orderfare)-$promoCode;
		$totalRefundWithCashback = $totalRefund - $totalCashBackAmount;
		
		if($totalRefund>0)
		{
			
			if($refundPolicy=="credit")
			{
				#update user credit
				User::updateUserCredit($totalRefundWithCashback,$userId);
			}
			
			#save records
			$adminId			=	Auth::user()->id;
			$createTime			=	time();
			
			$tableName			= 'flow';
			
			$params = array('user_id' => $userId, 
							  'admin_id' => $adminId,
							  'money' => $totalRefundWithCashback,
							  'direction' => "income",
							  'action' => "refund",
							  'detail' => $reasonRefund,
							  'detail_id' => $refundOrderItemId,
							  'create_time' => $createTime
							);
			
			$Inserted = Order::insertRecord($tableName,$params);

         return $totalRefund;
		}
		else
		{
			return 0;
		}
		
	}
	
	

	/**
	* Update Shipping Method
	* @Author : Arshad
	*/
	public function shippingOptionUpdate($id = 0){
		
		if(is_numeric($id) && $id>0){
			
			$data 					= array();
			
			$data['id'] 			 		 = $id;
			$getOrderItemDetail			=	Order::getOrderItemDetail($id);
			
			if(!empty($getOrderItemDetail)){
				
				
				
				$team_id						=	$getOrderItemDetail->team_id;
				$se_option_id					=	$getOrderItemDetail->option_id;
				$data['team']					=	Team::getTeamById($team_id);
				$data['team_multi']				=	Team::getMultiTeamInfoByTeamId($team_id);
				$data['team_option_info']		=	Team::getMultiTeamInfoById($se_option_id);
				$data['getOrderItemDetail']		= $getOrderItemDetail;
				$data['optionDelivery']			=	optionDelivery();
				
				// echo "<pre>";print_r($data['team_multi']	);exit();
				// $this->layout->title 	 = trans('localization.newsletter_update');	
				echo  View::make('order/shipping_option_update', compact('data'));
				exit();
			}
		}
		
		return Redirect::to('admin/orderlist')->with('errmessage', "Invalid Action");
		exit();
		
	}
	
	/**
	* Update Shipping Method SUBMIT FORM ACTION
	* @Author Arshad
	*/
	
	function shippingOptionSubmit(){
		
		$submittedPost	=	Input::get();
		
		$errMessage		=	"Invalid Action";
		$successMessage	=	"";
		if(!empty($submittedPost) && isset($submittedPost['update'])){
			
			$team_option = "";
			if(isset($submittedPost['team_option'])){
				$team_option		=	$submittedPost['team_option'];
			}
			$quantity			=	$submittedPost['quantity'];
			
			$delivery_method = "";
			if(isset($submittedPost['delivery_method'])){
				$delivery_method	=	$submittedPost['delivery_method'];
			}
			$order_item_id			=	$submittedPost['order_item_id'];
			$order_id				=	$submittedPost['order_id'];
			
			
			//UPDATE ORDE ITEM SHIPPING METHOD
			if(!empty($delivery_method)&&!empty($order_item_id)){
				
				//GET ORDER ITEM DETAIL
				$getOrderItemDetail			=	Order::getOrderItemDetail($order_item_id);
				
				$order_id					=	$getOrderItemDetail->order_id;
				$orderItemDeliveryStatus	=	$getOrderItemDetail->delivery_status;
				
				//CHECK SUBMITTED ORDER ITEM HAS PREVIOUS METHOD EXPRESS
				$orderItemMethod	=	Order::getShippingMethodOfOrderItem($order_item_id);
					
				//IF ORDER IS MOVING FROM PIKCUP TO EXPRESS
				if($delivery_method == 'express'){
					
					//SHIP METHOD CHANGE FROM PICKUP TO EXPRESS ITS USE ON ORDER STICK STATUS SECTION
					if($getOrderItemDetail->delivery_status == ShipStat_PickedUp){
						$orderItemDeliveryStatus = ShipStat_Delivered;
					}else if($getOrderItemDetail->delivery_status == ShipStat_Available){
						$orderItemDeliveryStatus = ShipStat_InPrep;
					}
					
					//check is submitted item are not express shipping method already
					if($orderItemMethod != 'express'){
					
						//SELECTED SUBMITED DEAL FROM ORDER ITEM
						$team_id	=	$getOrderItemDetail->team_id;
						$teamInfo	=	Team::getTeamById($team_id);
						
						$currentItemFare	=	0;
						if(isset($teamInfo[0]->fare)){
							$currentItemFare	=	$teamInfo[0]->fare;
						}
						
						if($currentItemFare<=0){
							$currentItemFare = DEFAULT_EXPRESS_SHIPPING;
						}
						
						//GET MAX ORDER ITEMS FARE FOR OTHER EXPRESS ITEM
						$expressItemsFare	=	Order::getExpressItemsFare($order_id);
						
						//GET MAX ORDER FARE FROM SELECTED ITEM AND FROM PREVIOUS EXPRESS ITEM
						$orderFareAmount	=	max(array($currentItemFare,$expressItemsFare));
						
						/*
						* CHECK CUSTOMER HAS SUFFICENT BALANCE FOR EXPRESS SHIIPING
						*/
						//SET REMAINING AMOUNT TO OF ORDER TOTAL CHECK WHICH IS DEDUCT BY USER
						$customerHasToPay	=	$orderFareAmount - $expressItemsFare;
						
						// $isCustomerHasEnoughBalance	=	Order::checkCustomerHasSufficientBalance($order_id,$customerHasToPay);
						
						//IF CUTOMER HAS ENOUGH BALANCE THEN CONTINUE ORDER AMOUNT UPDATE ELSE SHOW ERROR MESSAGE
						// if($isCustomerHasEnoughBalance){
							
							//DELETING COUPON
							Coupon::deleteCouponByOrderItemId($order_item_id);
							
							//UPDATE method_additional_cost for Express
							$method_additional_cost	=	"user_balance_cash";
							if(isset($submittedPost['method_additional_cost'])){
								$method_additional_cost	=	$submittedPost['method_additional_cost'];
							}
							
							//UPDATE ORDER ITEM FARE FOR EXPRESS	
							Order::orderAmountTransferFromPickupToExpress($order_id,$getOrderItemDetail,$orderFareAmount,$currentItemFare,$customerHasToPay,$method_additional_cost);
						// }else{
							// return Redirect::to('admin/orderlist?page=1')->with('errmessage', "User not have sufficient balance.");
							// exit();
						// }
					}
				}
				
				
				//IF ORDER IS MOVING FROM EXPRESS TO PICKUP
				if($delivery_method == 'pickup'){
					//GET COUNT for 'Express' METHOD 
					$orderItemShippingMethod	=	Order::getShippingMethodOfOrder($order_id);
					
					
					//SHIP METHOD CHANGE FROM EXPRESS TO PICKUP ITS USE ON ORDER STICK STATUS SECTION
					if($getOrderItemDetail->delivery_status == ShipStat_Delivered){
						$orderItemDeliveryStatus = ShipStat_PickedUp;
					}else if($getOrderItemDetail->delivery_status == ShipStat_InPrep){
						$orderItemDeliveryStatus = ShipStat_Available;
					}
					
					if(!empty($orderItemShippingMethod)){
						
						
						//ALL ORDER ITEM HAS NOT EXPRESS SHIPPING METHOD THEN CONITNUE CHECK FOR PICKUP LOGIC
						// if($orderItemShippingMethod == 1 && $orderItemMethod == 'express'){
						if( $orderItemMethod == 'express'){	
							//UPDATE ORDER ITEM FARE FOR PICKUP	
							Order::orderAmountTransferFromExpressToPickup($order_id,$getOrderItemDetail);
						}
					}
				}
				
				
				//UPDATE SHIPPING METHOD
				Order::UpdateShippingMethod($order_item_id,$delivery_method,$orderItemDeliveryStatus);
				$successMessage	=	"Order Information Update Successfully";
			}
		}
		
		//GET REDIRECTION URL FOR ORDER LISTING
		$redirectionUrl	=	Order::getOrderListingRedirectionUrl();
		
		if(!empty($successMessage)){
			return Redirect::to($redirectionUrl)->with('message', $successMessage);
		}else{
			return Redirect::to($redirectionUrl)->with('errmessage', $errMessage);
		}
		exit();
	}
	
	
	/**
	* UPDATE Shipping Address
	* @Author Arshad
	*/
	function updateShippingAddress($id = 0){
		
		if(is_numeric($id) && $id>0){
			
			$data 					= array();
			
			$data['id'] 			 	= $id;
			$getOrderItemDetail			= Order::getOrderItemDetail($id);

			$getOrderById		=	array();
			if(isset($getOrderItemDetail->order_id)){
				$getOrderById		=	Order::getOrderById($getOrderItemDetail->order_id);
			}
			
			$data						= 	array();
			$data['getOrderItemDetail']	=	$getOrderItemDetail;
			$data['getOrderById']		=	$getOrderById[0];
			

			
			echo  View::make('order/update_shipping_address', compact('data'));
			exit();
		}
		
		
		return Redirect::to('admin/orderlist')->with('errmessage', "Invalid Action");
		exit();
	}
	
	/**
	* Update deliveryAddressSubmit
	* @Author Arshad
	*/
	function deliveryAddressSubmit(){
		
		$submittedPost	=	Input::get();
		
		//GET REDIRECTION URL FOR ORDER LISTING
		$redirectionUrl	=	Order::getOrderListingRedirectionUrl();
		
		if(!empty($submittedPost)){
			
			$first_name		=	$submittedPost['first_name'];
			$last_name		=	$submittedPost['last_name'];
			$mobile			=	$submittedPost['mobile'];
			$address		=	$submittedPost['address'];
			$address2		=	$submittedPost['address2'];
			$zipcode		=	$submittedPost['zipcode'];
			$region			=	$submittedPost['region'];
			$order_item_id	=	$submittedPost['order_item_id'];
			
			$errMessage		=	"";
			$successMessage	=	"";
			
			//VALIDATE INPUT
			if(!empty($first_name)&&!empty($last_name)&&!empty($mobile)&&!empty($address)&&!empty($zipcode)&&!empty($region)){
				
					
				//UPDATE SHIPPING ADDRESS
				Order::updateShippingAddress($submittedPost);
				
				$successMessage = "Shipping Address Updated Successfully";
				return Redirect::to($redirectionUrl)->with('message', $successMessage);
				exit();
					
				
			}else{
				$errMessage	=	"Please Enter Required Fields.";
			}
			
		}
		
		return Redirect::to($redirectionUrl)->with('errmessage', $errMessage);
		exit();
	}
	
	
	/**
	* Update DEAL ITEM
	* @Author : Arshad
	*/
	public function dealOptionUpdate($id = 0){
		
		if(is_numeric($id) && $id>0){
			
			$data 					= array();
			
			$data['id'] 			 		 = $id;
			$getOrderItemDetail			=	Order::getOrderItemDetail($id);
			
			if(!empty($getOrderItemDetail)){
				
				
				
				$team_id						=	$getOrderItemDetail->team_id;
				$order_id						=	$getOrderItemDetail->order_id;
				$se_option_id					=	$getOrderItemDetail->option_id;
				$team							=	Team::getTeamById($team_id);
				$data['team']					=	$team;
				$data['team_multi']				=	Team::getMultiTeamInfoByTeamId($team_id);
				$data['team_option_info']		=	Team::getMultiTeamInfoById($se_option_id);
				$data['getOrderItemDetail']		= 	$getOrderItemDetail;
				$data['optionDelivery']			=	optionDelivery();
				
				
				//CHECK DEAL STOCK & LIMIT PER USER
				//LOGIC COPY FROM BASKET.PHP
				
				
				$teamArray	 = json_decode(json_encode($team),true);
				$team		 = $teamArray[0];
				
				$upper_limit = $team['max_number'];
				
				$parentDealCloseOrOutOfStock	=	false;
				$parentDealCloseOrOutOfStockMsg	=	"";
				$dealLimitPerUserErr			=	false;
				//GET ORDER INFO
				$orderInfo	=	Order::getOrderById($order_id);
				$user_id	=	$orderInfo[0]->user_id;

				$data['parentDealCloseOrOutOfStock']	=	$parentDealCloseOrOutOfStock;
				$data['parentDealCloseOrOutOfStockMsg']	=	$parentDealCloseOrOutOfStockMsg;
				$data['dealLimitPerUserErr']			=	$dealLimitPerUserErr;
				
				$data['cashbackErrorMessage']	=	"";
				// echo "<pre>";print_r($data['team_multi']	);exit();
				// $this->layout->title 	 = trans('localization.newsletter_update');	
				echo  View::make('order/deal_option_update', compact('data'));
				exit();
			}
		}
		
		return Redirect::to('admin/orderlist')->with('errmessage', "Invalid Action");
		exit();
		
	}
	
	/**
	* Update DEAL ITEM SUBMIT FORM ACTION
	* @Author Arshad
	*/
	
	function dealOptionSubmit(){
		
		$submittedPost	=	Input::get();
		
		$errMessage		=	"";
		$successMessage	=	"";
		if(!empty($submittedPost) && isset($submittedPost['update'])){
			
			$team_option			=	$submittedPost['team_option'];
			$submitted_team_option	=	$team_option;
			$submitted_quantity		=	$submittedPost['quantity'];
			$delivery_method		=	$submittedPost['delivery_method'];			
			$order_item_id				=	$submittedPost['order_item_id'];
			$order_id					=	$submittedPost['order_id'];
			
			
			//UPDATE ORDE ITEM SHIPPING METHOD
			if(!empty($delivery_method)&&!empty($order_item_id)){
				
				//GET ORDER ITEM DETAIL
				$getOrderItemDetail			=	Order::getOrderItemDetail($order_item_id);
				//ORDER DEAL
				$getOrderByIdInfo			=	Order::getOrderById($order_id);
				
				$order_id					=	$getOrderItemDetail->order_id;
				$orderItemDeliveryStatus	=	$getOrderItemDetail->delivery_status;
				$old_orderItemDeliveryStatus	=	$getOrderItemDetail->delivery_status;
				$oi_delivery				=	$getOrderItemDetail->delivery;
				$team_id					=	$getOrderItemDetail->team_id;
				$option_id					=	$getOrderItemDetail->option_id;
				$oi_option_id				=	$option_id;
				$order_item_quantity		=	$getOrderItemDetail->quantity;
				  
				 /**
				 * VALIDATION START
				 */
				 if($submitted_quantity == 0){
					 
					$errMessage = 'Please refund Order item';
				 }
				 if($submitted_quantity > $order_item_quantity){
					 
					$errMessage =  'Order Quantity cant be increased, Please refund and place new order';
				 }
				 
				 
				 //GET TEAM FARE BY TEAM ID
				 $teamInfo	=	Team::getTeamById($team_id);
				 $teamFare	=	0;
				 $teamStock	=	0;
				 if(isset($teamInfo[0]->fare)){
					 $teamFare 	= $teamInfo[0]->fare;
					 
				 }
				 //CHECK IF SUBMMITED DEAL FOR OPTION OR SIMPLE IF SIMPLE DEAL THEN STOCK SET FROM TEAM TABLE ELSE FROM TEAM MULTI TABLE
				 $dealOptionChange		=	false;
				 $getMultiTeamInfoById	=	array();
				 if($submitted_team_option > 0){
					
					$getMultiTeamInfoById	=	Team::getMultiTeamInfoById($submitted_team_option);
					
					if(!empty($getMultiTeamInfoById)){
						if(isset($getMultiTeamInfoById[0]->id)){
							
							$teamStock 			= $getMultiTeamInfoById[0]->stock_count;
							$optionMaxNumber 	= $getMultiTeamInfoById[0]->max_number;
							$teamTitle 			= $getMultiTeamInfoById[0]->title;
							$to_per_number 		= $getMultiTeamInfoById[0]->per_number;
							$user_id			= $getOrderByIdInfo[0]->user_id;
							//CHECK SUBMITTED DEAL OPTION ID IS UPDATE TO THER ONE OR NOT IF UPDATED THEN CHECK STOCK AND AVAIABLE QUANTITY TO PROCESS FURTHER
							if($submitted_team_option != $oi_option_id){
								 
								 // DEAL OPTION CHANGE FALG ENABLE
								 $dealOptionChange	=	true;
								
								//CHECK STOCK IS AVAILABLE FOR DESIRE TEAM OPTION
								$optionErrorMessage	=	"";
								// if($teamStock <= 0){
									// $optionErrorMessage .=  "$teamTitle is Not Available in Stock.";
								// }

								if($optionMaxNumber > 0 && $optionMaxNumber < $submitted_quantity){
									$optionErrorMessage .=  "$teamTitle is Not Available in Our Store Stock.(Max Number)";
								}

								if($to_per_number > 0){
									 // There is a Limit per User
									$basket_quantity = Order::baskteGetQuantity($team_id, $submitted_team_option,$user_id);     
									$purchased_count = 0;
									// Currently selected quantity in the basket is lower than the Limit
									// Check if User has previously purchased it
					
									// $purchased_count = $count_result['now_count'];
									$purchased_count	=	Order::userPrevioslyPurhchasedDeal($user_id,$team_id,"  AND order_item.option_id = '$submitted_team_option'");
									//$quantity += $now_count;
						

									$leftnum = ($to_per_number - $basket_quantity - $purchased_count);									
									if ($leftnum <= 0) {
										$optionErrorMessage	.= "This offer is limited in quantity OR item is already purchased by User";
									}  

									if ($leftnum < $submitted_quantity) {
										$optionErrorMessage	.= "This offer is limited in quantity ";
									} 									
								}
								$errMessage	=	$optionErrorMessage;
							}
						}
					}
					 
				 }else{
					 if(isset($teamInfo[0]->stock_count)){
						$teamStock = $teamInfo[0]->stock_count;
					 }
				 }
			   /**
				 * VALIDATION END
				 */
				 

				/**
				*
				* CALCULATION START
				*/

				
				//IF VALIDATED
				if($errMessage == ""){
					
					
					/*****************************************************************
					* 					CALCULATE CASHBACK AMOUNT START
					****************************************************************/
							
					$newItemOption_id			=	$submitted_team_option;
					$new_itemQuantity			=	$submitted_quantity;
							
					//GET OLD ITEM CASHBACK AMOUNT
					$oldItemCashBack	=	$getOrderItemDetail->cashback_amount;
						
					//GET NEW CASH BACK AMOUNT
					$newItemCashBack	=	Team::teamCashbackAmount($team_id, $newItemOption_id, $new_itemQuantity);
					
					$isCashBackAmountChange	=	false;
					$currentCashBackAmount	=	$newItemCashBack;
					if($newItemCashBack > $oldItemCashBack){
						$currentCashBackAmount	=	$newItemCashBack - $oldItemCashBack;
						$isCashBackAmountChange	=	true;
					}elseif($newItemCashBack < $oldItemCashBack){
						$currentCashBackAmount	=	$oldItemCashBack - $newItemCashBack;
						$isCashBackAmountChange	=	true;
					}elseif($newItemCashBack == 0 && $oldItemCashBack > 0){
						$isCashBackAmountChange	=	true;
					}
					
					//IF CASHBACK AMOUNT DIFFERENR
					if($isCashBackAmountChange){
							
							
							
							//AMOUNT TO PAID CUTOMER
							$currentUserCredit	=	Order::getUserCreditAmount($user_id);
							//DEDUCT CASH BACK AMOUNT FROM CUSTOMER CREDIT
							$cashBackAmountRefund	=	false;
							if($oldItemCashBack>0){
								if($currentUserCredit >= $oldItemCashBack){
									
									$userCreditAfterDeductUserCredit	=	$currentUserCredit - $oldItemCashBack;
									$cashBackAmountRefund	=	true;
									User::simpleUpdateUserCredit($userCreditAfterDeductUserCredit,$user_id);
									//LOG CREDIT FLOW
									$admin_id	=	Auth::user()->id;
									
									
									User::logUserCreditFlow($user_id,$admin_id,$oldItemCashBack,'expense','cash_back','CashBack Amount Deduct From Customer Account because order option change',$order_id, time());
								}else{
									//$newitemTotal	=	$newitemTotal + $newItemCashBack;
									return Redirect::to(Order::getOrderListingRedirectionUrl())->with('errmessage', "Cashback Amount is used & customer doesn't have suffienect amount on his account");
								}
							}
							//ADD CASHBACK AMOUNT IN CUSTOMER ACCOUNT
							if($newItemCashBack > 0){
								$cashBackAmountRefund	=	true;
								User::updateUserCredit($newItemCashBack,$user_id);
								//LOG CREDIT FLOW
								$admin_id	=	Auth::user()->id;
								User::logUserCreditFlow($user_id,$admin_id,$newItemCashBack,'income','cash_back','CashBack Amount Income inCustomer Account because order option change',$order_id, time());
							}
							// echo $userCreditAfterDeductUserCredit."<br/>".$newItemCashBack."<br/>".$oldItemCashBack."<br/>".$currentUserCredit;exit();
							
							//SET CASHBACK AMOUN FOR EACH ITEM
							$orderItemCashBackSql	=	<<<SQL
													UPDATE
															`order_item`
													SET
														`cashback_amount`	=	'$newItemCashBack'
													WHERE
														`id`		=	'$order_item_id'
SQL;
							DB::update($orderItemCashBackSql);
						//
						
					}
							
					/***************************************************************
					* 				CALCULATE CASHBACK AMOUNT END
					****************************************************************/
					
					if($dealOptionChange){
						
						if(isset($getMultiTeamInfoById[0]->id)){
							
							$itemId						=	$getOrderItemDetail->id;
							$itemFare					=	$teamFare;
							$old_itemPrice				=	$getOrderItemDetail->price;
							$old_itemQuantity			=	$getOrderItemDetail->quantity;
							// $method_additional_cost		=	$getOrderItemDetail->shipment_additional_cost;
							$method_additional_cost		=	$submittedPost['method_additional_cost'];
							
							$newItemOption_id			=	$submitted_team_option;
							$new_team_price				=	$getMultiTeamInfoById[0]->team_price;
							$new_itemQuantity			=	$submitted_quantity;
							
							//CALCULATE NEW ITEM OPTION PRICE
							$newitemTotal				=	$new_itemQuantity * $new_team_price;
							$newitemTotalWithFare		=	$newitemTotal + $itemFare;
							
							//CALCULATE OLD ITEM OPTION PRICE
							$olditemTotal				=	$old_itemQuantity * $old_itemPrice;
							$olditemTotalWithFare		=	$olditemTotal + $itemFare;
							
							
							

							
							
							//CALCULATE DIFF AMOUNT TO CHECK EITHER WE NEED TO PAY AMOUNT TO USER OR WE HAVE TO DEDUCT IT FROM USER
							
							$amountDeductFromUser = 0;
							$amountPayToUser = 0;
							
							if($newitemTotal > $olditemTotal){
								$amountDeductFromUser = $newitemTotal - $olditemTotal;
							}elseif($olditemTotal > $newitemTotal){
								$amountPayToUser = $olditemTotal - $newitemTotal;
							}
							
							//UPDATE ORDER ITEM AMOUNT
							Order::updateOrderItemAmount($itemId,$itemFare,$newitemTotalWithFare,$method_additional_cost, ", quantity = '$new_itemQuantity',price='$new_team_price', option_id='$newItemOption_id' ");
							
							
							//UPDATE ORDER AMOUNT
							$user_id	=	$getOrderByIdInfo[0]->user_id;
							$orderFare	=	$getOrderByIdInfo[0]->fare;
							$money		=	$getOrderByIdInfo[0]->money;
							$origin		=	$getOrderByIdInfo[0]->origin;
							$origin		=	$getOrderByIdInfo[0]->origin;
							$credit		=	$getOrderByIdInfo[0]->credit;
							$card		=	$getOrderByIdInfo[0]->card;
							
							//GET TOTAL ITEM UPADTE QUANTITY FOR ORDER
							$totalUpdatedQuantity	=	Order::getUpdatedTotalQuantity($order_id);
							
							//GET MAX ORDER ITEMS FARE FOR OTHER EXPRESS ITEM
							$expressItemsFare	=	Order::getExpressItemsFare($order_id);
							//UPDATE ORIGIN PRICE 
							
							/*
							* IF NEW ORDER DEAL AMOUNT IS GREATER THAN OLD DEAL OPTION
							* ADD GRAND TOTAL ON ORDER TABLE AND SUBTRACT FROM USER CREDIT
							*/
							
							if($amountDeductFromUser > 0){
								
								/* DEDUCT AMOUNT EITHER FROM CREDIT,CARD OTHER RESOURCE AND PUSH IT TO CUSTOMER CREDIT*/
								//CREATE ARRAY OF ORIGIN,CREDIT,CARD TO GET MAX VALUE AND MINUS FARE FROM IT TO AVOID NEGATIVE VALUE
								$maxAmountArr	=	array('credit'=>$credit,'money'=>$money);
								$maxAmount 		=  max($maxAmountArr);
								//GET NAME OF VARIABLE WHICH HAS MAX VALUE
								$maxKey = array_search($maxAmount, $maxAmountArr); 
								// echo $maxKey."<br/>". $$maxKey;
								//MINUS FARE BY VARIABLE VARIABLE
								$$maxKey	=  $$maxKey + $amountDeductFromUser;
								// echo "<br/>".$$maxKey;exit();
								//UPDATE TOTAL AMOUNT WHEN PROMO CODE VALUE IS LOWER
								if($maxKey != 'card'){
									$origin		=	$amountDeductFromUser + $origin;
								}
								
								/* UPDATE USER CREDIT */
								//AMOUNT TO PAID CUTOMER
								$currentUserCredit	=	Order::getUserCreditAmount($user_id);
			
			
								//CHECK PAYMENT METHOD FOR CHARGING
								if($method_additional_cost == 'user_balance_cash'){
								
									//UPDATE USER CREDIT AMOUNT
									if($amountDeductFromUser>0 && $currentUserCredit > 0 ){
										
										//UPDATE USER CREDIT
										$updateUserCredit	=	0;
										
										if($amountDeductFromUser >= $currentUserCredit){
											
											$updateOrderCredit = $amountDeductFromUser - $currentUserCredit;
											$credit			+= $currentUserCredit;
											$$maxKey	=  $$maxKey - $currentUserCredit;
											if($maxKey == 'card'){
												$origin		=	$currentUserCredit + $origin;
											}
											
											$updateUserCredit	=	0;
										}else{
											$updateUserCredit = $currentUserCredit - $amountDeductFromUser;
											
											$credit			+= $amountDeductFromUser;
											$$maxKey	=  $$maxKey - $amountDeductFromUser;
											if($maxKey == 'card'){
												$origin		=	$amountDeductFromUser + $origin;
											}
										}
										
										User::simpleUpdateUserCredit($updateUserCredit,$user_id);
										//LOG CREDIT FLOW
										$admin_id	=	Auth::user()->id;
										User::logUserCreditFlow($user_id,$admin_id,$currentUserCredit,'expense','deduct','Fare Amount Deducted due to change in deal option type',$order_id, time());
									}
								}else{
									$$maxKey	=  $$maxKey - $amountDeductFromUser;
									$money		=  $money + $amountDeductFromUser;
								}									

								
							}
							
							/*
							* IF NEW ORDER DEAL AMOUNT IS LESS THAN OLD DEAL OPTION
							* SUBTRACT FROM GRAND TOTAL ON ORDER TABLE AND ADD TO USER CREDIT
							*/
							if($amountPayToUser > 0){
								

								/* DEDUCT AMOUNT EITHER FROM CREDIT,CARD OTHER RESOURCE AND PUSH IT TO CUSTOMER CREDIT*/
								//CREATE ARRAY OF ORIGIN,CREDIT,CARD TO GET MAX VALUE AND MINUS FARE FROM IT TO AVOID NEGATIVE VALUE
								$maxAmountArr	=	array('credit'=>$credit,'card'=>$card,'money'=>$money);
								$maxAmount 		=  max($maxAmountArr);
								//GET NAME OF VARIABLE WHICH HAS MAX VALUE
								$maxKey = array_search($maxAmount, $maxAmountArr); 
								
								//MINUS FARE BY VARIABLE VARIABLE
								if($$maxKey >= $amountPayToUser){
									$$maxKey	=  $$maxKey - $amountPayToUser;
								}else{
									$$maxKey	=  $amountPayToUser	-	$$maxKey;
								}
			
								//ORDER TOTAL AMOUNT UPDATE
								if($maxKey != "card"){
									if($amountPayToUser >= $origin){
										$origin		=	$amountPayToUser - $origin;
									}else{
										$origin		=	$origin - $amountPayToUser;
									}
								}
								
								//CHECK PAYMENT METHOD FOR CHARGING
								User::updateUserCredit($amountPayToUser,$user_id);
								//LOG CREDIT FLOW
								$admin_id	=	Auth::user()->id;
								User::logUserCreditFlow($user_id,$admin_id,$amountPayToUser,'expense','refund','Fare Amount Refund due to change in deal option',$order_id, time());
								
								
							}
							
							//UPDATE AMOUNT FOR ORDER
							Order::updateOrderAmount($order_id,$expressItemsFare,$money,$origin,$credit,$card, ", quantity = '$totalUpdatedQuantity'");
							
							
							
							/* UPDATE SHIPPING STATUS ON BEHALF OF SHIPPING METHOD WHEN DELIVERY STATUS IS WAITING */
							if($optionMaxNumber > 0 && $optionMaxNumber >= $submitted_quantity){
									if($oi_delivery == 'express'){
										$orderItemDeliveryStatus	=	ShipStat_InPrep;
									}elseif($oi_delivery == 'pickup'){
										$orderItemDeliveryStatus	=	ShipStat_Available;
									}
							}
								
							if($teamStock < $submitted_quantity ){
								$orderItemDeliveryStatus = ShipStat_Waiting;
							}
							
							Order::UpdateShippingMethod($order_item_id,$oi_delivery,$orderItemDeliveryStatus);
							
							/* UPDATE STOCK	*/
							// ADD STOCK ON OLD DEAL OPTION
							// ADD STOCK ONLY WHEN OLD DEAL IS NOT IN WAITING STATUS 
							if($old_orderItemDeliveryStatus != ShipStat_Waiting){
								Team::updateTeamStock($oi_option_id,$old_itemQuantity,'team_multi');
							}
							
							//SUBTRACT STOCK FROM NEW DEAL
							if($teamStock >= $submitted_quantity ){
								Team::subtractTeamStock($newItemOption_id,$new_itemQuantity,'team_multi');
							}
							
							stockUpdateByTeamOptionId($oi_option_id);
							stockUpdateByTeamOptionId($newItemOption_id);
							
							$successMessage	=	"Order Information Update Successfully";
						}
					}else{
						
						//ADD QUANTITY ON STOCK
						$addQuantityStock	=	$order_item_quantity - $submitted_quantity;
				
						//IF SIMPLE QUANTITY UPDATE FOR DEAL AND DEAL OPTION BELOW CALCULATION DONE
						
						//CALCULATION PROCESS IF QUANTITY DIFFER OTHER WISE NONE B/C USER CAN SUBMIT FORM WITHOUT CHANGES
						if($addQuantityStock > 0){
							
							$itemId						=	$getOrderItemDetail->id;
							$itemFare					=	$teamFare;
							$oldItemTotal				=	$getOrderItemDetail->total;
							$itemPrice					=	$getOrderItemDetail->price;
							$olditemQuantity			=	$getOrderItemDetail->quantity;
							$method_additional_cost		=	$getOrderItemDetail->shipment_additional_cost;
							$itemQuantity				=	$submitted_quantity;

							$newitemTotal	=	$itemQuantity * $itemPrice;
							$itemTotal		=	$newitemTotal + $itemFare;
							
							//UPDATE ORDER ITEM AMOUNT
							Order::updateOrderItemAmount($itemId,$itemFare,$itemTotal,$method_additional_cost, ", quantity = '$itemQuantity'");
							
							//UPDATE ORDER AMOUNT
							$user_id	=	$getOrderByIdInfo[0]->user_id;
							$orderFare	=	$getOrderByIdInfo[0]->fare;
							$money		=	$getOrderByIdInfo[0]->money;
							$origin		=	$getOrderByIdInfo[0]->origin;
							$origin		=	$getOrderByIdInfo[0]->origin;
							$credit		=	$getOrderByIdInfo[0]->credit;
							$card		=	$getOrderByIdInfo[0]->card;
							
							//GET TOTAL ITEM UPADTE QUANTITY FOR ORDER
							$totalUpdatedQuantity	=	Order::getUpdatedTotalQuantity($order_id);
							
							//UPDATE ORIGIN PRICE 
							//DEDUCT PREVIOUS FARE AMOUNT
							#addQuantityStock Quantity which need to add on stock after deduction from cuurent quantity of order
							$itemAmountToDeduct	=	$itemPrice * $addQuantityStock;
							
							
							/* DEDUCT AMOUNT EITHER FROM CREDIT,CARD OTHER RESOURCE AND PUSH IT TO CUSTOMER CREDIT*/
							//CREATE ARRAY OF ORIGIN,CREDIT,CARD TO GET MAX VALUE AND MINUS FARE FROM IT TO AVOID NEGATIVE VALUE
							$maxAmountArr	=	array('credit'=>$credit,'card'=>$card,'money'=>$money);
							$maxAmount 		=  max($maxAmountArr);
							//GET NAME OF VARIABLE WHICH HAS MAX VALUE
							$maxKey = array_search($maxAmount, $maxAmountArr); 
							
							//MAX KEY 
							$oldMaxKey	=	$maxKey;
							
							//MINUS FARE BY VARIABLE VARIABLE
							if($$maxKey >= $itemAmountToDeduct){
								$$maxKey	=  $$maxKey - $itemAmountToDeduct;
							}else{
								if($maxKey != "card"){
									$$maxKey	=  $itemAmountToDeduct	-	$$maxKey;
								}else{
									
									$amountDeducFromOrigin	=  $itemAmountToDeduct	-	$$maxKey;
									//SET PROMO CARD VALUE ZERO
									$$maxKey	=	0;
									//NOW DEDUCT AMOUNT ANOTHER COLOUMN
									$maxAmountArr	=	array('credit'=>$credit,'money'=>$money);
									$maxAmount 		=  max($maxAmountArr);
									//GET NAME OF VARIABLE WHICH HAS MAX VALUE
									$maxKey = array_search($maxAmount, $maxAmountArr); 
									
									if($$maxKey >= $amountDeducFromOrigin){
										$$maxKey	=  $$maxKey - $amountDeducFromOrigin;
									}else{
										$$maxKey	=  $amountDeducFromOrigin- $$maxKey;
									}
									
									$origin		=	$origin - $amountDeducFromOrigin;
								}
							}
							
							//ORDER TOTAL AMOUNT UPDATE
							if($oldMaxKey != "card"){
								if($itemAmountToDeduct >= $origin){
									$origin		=	$itemAmountToDeduct - $origin;
								}else{
									$origin		=	$origin - $itemAmountToDeduct;
								}
							}
							
							//GET MAX ORDER ITEMS FARE FOR OTHER EXPRESS ITEM
							$expressItemsFare	=	Order::getExpressItemsFare($order_id);
								
							//UPDATE AMOUNT FOR ORDER
							Order::updateOrderAmount($order_id,$expressItemsFare,$money,$origin,$credit,$card, ", quantity = '$totalUpdatedQuantity'");
							
							
							/* UPDATE SHIPPING STATUS ON BEHALF OF SHIPPING METHOD WHEN DELIVERY STATUS IS WAITING */
							if($orderItemDeliveryStatus == ShipStat_Waiting){
								
								//CHECK AVAIABLE STOCK OF DEAL OR DEAL OPTION TP SET IT AVAIABLE
								if($teamStock >= $submitted_quantity){
									if($oi_delivery == 'express'){
										$orderItemDeliveryStatus	=	ShipStat_InPrep;
									}elseif($oi_delivery == 'pickup'){
										$orderItemDeliveryStatus	=	ShipStat_Available;
									}
								}
								
							}
							Order::UpdateShippingMethod($order_item_id,$oi_delivery,$orderItemDeliveryStatus);
							
							//UPDATE STOCK			
							
							
							if($team_option>0){
								Team::updateTeamStock($team_option,$addQuantityStock,'team_multi');
								stockUpdateByTeamOptionId($team_option);
							}else{
								Team::updateTeamStock($team_id,$addQuantityStock,'team');
								stockUpdateByTeamId($team_id);
							}
							
							/* UPDATE USER CREDIT */
							
							//AMOUNT TO PAID CUTOMER
							$amountTopaidCustomer	=	$itemAmountToDeduct;
							
							User::updateUserCredit($amountTopaidCustomer,$user_id);
							//LOG CREDIT FLOW
							$admin_id	=	Auth::user()->id;
							User::logUserCreditFlow($user_id,$admin_id,$amountTopaidCustomer,'expense','refund','Fare Amount Refund due to change in delivery type',$order_id, time());
							
							$successMessage	=	"Order Information Update Successfully";
						}else{
							$successMessage	=	"Order Information Update Successfully";
						}
					
					}
				}
			}
		}
		
		//GET REDIRECTION URL FOR ORDER LISTING
		$redirectionUrl	=	Order::getOrderListingRedirectionUrl();
		
		if(!empty($successMessage)){
			return Redirect::to($redirectionUrl)->with('message', $successMessage);
		}else{
			return Redirect::to($redirectionUrl)->with('errmessage', $errMessage);
		}
		exit();
	}

}	