﻿<?php
/**
*@author:Deals
*@desc: *	function index loading dashboard/index and counting customers from the customers table.
*		*	function get_dashboard_Datatable() it is showing customers list with data table
**/
class CouponController extends BaseController {
	
	/*
	 * __construct
	 */
	public function __construct() {
		
	}
	
	/**
	*@author:MAK
	*@desc: it will load index page and including customers
	**/
	public function index()
	{

		$data	=	array();
		$this->layout->title	= trans('localization.CouponList');
		$this->layout->content	= View::make('coupon/index',compact('data'));
	}
	
	public function getDatatable()
	{ 
		$local = Config::get('app.locale');
		//GET PARTNER ID
		$loginPartnerID	=	Auth::user()->id;
		
		if(!empty($loginPartnerID)&&$loginPartnerID>0){
			
			
			
			switch($local){
				case'en':
					$title = "t.title as title";
				break;
				
				case'fr':
					$title = "t.title_fr as title";
				break;
				
				default:
					$title = "t.title as title";
				break;
			}
		
			//RECORD BY STATE
			$daytime = strtotime(date('Y-m-d'));
			$couponNumSer			=	Input::get('sSearch_0');
			$dealNameSer			=	Input::get('sSearch_1');
			$buyerName				=	Input::get('sSearch_2');
			$dueTimeSearch			=	Input::get('sSearch_4');
			$stateType				= 	Input::get('sSearch_5');
			
			$order_by = 'cp.team_id';
			$sSortDir_0 = 'desc';
			
			//CREATE QUERY FOR GET DATA
			$deals = DB::table('coupon as cp')
						->leftJoin('team as t', 'cp.team_id', '=', 't.id')
						->leftJoin('user as u', 'u.id', '=', 'cp.user_id');
						
			$deals->where("cp.partner_id", "=", "$loginPartnerID");
			
			$stateCondition = "";
			
			if ($stateType) {
				
				switch(strtoupper($stateType)) {
					case 'Y':
						$deals->where("cp.consume", "=", 'Y');
						break;
					case 'N': 
						$deals->where("cp.consume", "=", 'N');
						$deals->where("cp.expire_time", ">=", "$daytime");
						// $stateCondition =  "cp.consume = 'N' and cp.expire_time >= $daytime"; 
						break;
					case 'E': 
						// $stateCondition =  "cp.consume = 'N' and cp.expire_time< $daytime"; 
						$deals->where("cp.consume", "=", 'N');
						$deals->where("cp.expire_time", "<", "$daytime");
						break;
				}
			}
			
			//	SEARCH

			if ($couponNumSer) {
				
				$deals->where('cp.id', 'like', '%'.$couponNumSer.'%');
			}
			
			if ($dealNameSer) {
				
				switch($local){
					case'en':
						$deals->where('t.title', 'like', '%'.$dealNameSer.'%');
					break;
					
					case'fr':
						$deals->where('t.title_fr', 'like', '%'.$dealNameSer.'%');
					break;
					
					default:
						$deals->where('t.title', 'like', '%'.$dealNameSer.'%');
					break;
				}
				
				
				
			}
			
			if ($buyerName) {
				
				$deals->where('u.first_name', 'like', '%'.$buyerName.'%')->orwhere('u.last_name', 'like', '%'.$buyerName.'%');
			}
			
			//DUE TIME SEARCH
			if($dueTimeSearch!=""){
				$dueTimeSearch	=	strtotime($dueTimeSearch."00:00:00");
				$deals->where("cp.expire_time", "<=", "$dueTimeSearch");
			}
			//IF STATE CONDITION NOT EMPTY
			// if(!empty($stateCondition)){
				// $deals->whereRaw($stateCondition);
			// }
			
			$deals->select($title, 
							"cp.id as cou_id", 
							"t.id", 
							"u.first_name",
							"u.last_name", 
							"cp.secret",
							"cp.expire_time",
							"cp.consume_time",
							"cp.consume"
							);
			$deals->orderBy($order_by, $sSortDir_0);
			// echo $dueTimeSearch."<br/>";
			// var_debug($deals->toSql());exit();
					
			return Datatable::query($deals)
					->showColumns(array("cou_id", "title", "first_name", "secret", "expire_time","consume_time"))
					->addColumn('title', function($model) {
						
						$topDealLink	=	TOPDEAL_URL_LINK;
						$id	=	$model->id;
						
						return "<a href='$topDealLink/team.php?id=$id' target='_blank'>".$model->title."</a>";
					})
					->addColumn('first_name', function($model) {
						
						$first_name		=	 $model->first_name;
						$last_name	=	 $model->last_name;
						
						return $first_name." ".$last_name;
					})
					->addColumn('secret', function($model) {
						return "******";
					})
					->addColumn('expire_time', function($model) {
						return date('d-m-Y', $model->expire_time);
					})
					->addColumn('consume_time', function($model) {
						
						$stateStr	=	"";
						if ($model->consume == 'Y'){
							$stateStr	=	"used <br/>".date('d-m-Y H:i', $model->consume_time);
						}elseif ($model->expire_time < time() && $model->consume = 'N'){
							$stateStr	=	EXPIRED;
						}else{
							$stateStr	=	VALID;
						}
						
						return $stateStr;
					})
					
					// ->searchColumns(array("cp.id", "title", "first_name", "secret"))
					 // ->searchColumns(array("cp.id", "title", "secret"))
					->orderColumns(array("cp.id", "title", "first_name", "secret", "cp.expire_time","consume_time"))
					->make();
		}
	}
	
	/**
	* @author Jawwad
	* 
	* Open Auto Open Coupon validation dialog
	* Coupon validator form, display everywhere defined in header
	* 
	* Coupon
	* 	Step 1:
	*/
	public function couponValidatorForm()
	{
		$data = '';
		$this->layout->title 	= 'Coupon Validator';		
		$this->layout->content 	= View::make('coupon/coupon_validator', compact('data'));
	}
	
	/**
	 * Coupon
	 *	Step 2:
	 *
	 *	Coupon Validation form submission
	 *	Validate coupon when validate and return JSON response
	 * */
	public function couponValidator()
	{
		$id = Input::get('id');
		$secret = Input::get('secret');
		
		$coupon 		= Coupon::getCouponById($id);
		$v	=	array();
		$oi	= array();
		$valid = false;
		if(is_array($coupon) && count($coupon)>0)
		{
			$coupon = $coupon[0];
			$couponOrderId	= $coupon->order_id;
			
			$orderItem		= Coupon::getOrderItemByCouponOrderId($couponOrderId);
			
			if(is_array($orderItem) && count($orderItem)>0)
			{
				$oi = $orderItem[0];
			}
			
			
			if (!$coupon) {
				$v[] = "N° de bon invalide";
				$v[] = ' La validation a échoué!';
			}
			else if ($coupon->secret!=$secret) {
				$v[] = 'Le PIN est incorrect';
				$v[] = ' La validation a échoué!';
			} else if ( $coupon->expire_time < strtotime(date('Y-m-d')) && $oi->delivery != 'pickup') {
				$v[] = "#{$id}&nbsp;expiré";
				$v[] = 'Expiration' . date('Y-m-d', $coupon->expire_time);
				$v[] = 'La validation a échoué!';
			} else if ( $coupon->consume == 'Y' ) {		
				$v[] = "#{$id}&nbsp;Bon déjà validé";
				$v[] = 'Validé le:' . date('Y-m-d H:i:s', $coupon->consume_time);
				$v[] = 'La validation a échoué!';
			}
			else if (  $oi->delivery == 'pickup'  && $oi->delivery_status != ShipStat_Available ) {		
				$v[] = 'Article pas encore disponiblee';
			}
			else {
				// if($callback) {
					// $v[] = "<script>".$callback."();</script>";
				// }
				if ( Coupon::Consume($coupon)){
					
							// $coupons = Table::Fetch('coupon', array($coupon['order_id']), 'order_id');
							$cp_order_id	=	$coupon->order_id;
							$teamId			=   $coupon->team_id;
							
							$coupons = DB::table('coupon')->where('order_id', $cp_order_id)->get();
							
							$status_check = TRUE;
							if(!empty($coupons)){
								foreach($coupons as $c ){
									if ( $c->consume == 'N' ){
										$status_check = FALSE;
									}
								}
							}
							
							
							
							
							
							if($status_check == TRUE){
								// $order_item = Table::FetchForce('order_item', $coupon['order_id']);
								if ( $oi->delivery == 'pickup' )
								{
									
									$warehouseData = Coupon::getWarehouseSectionByTeam($teamId);
									$warehousesectiondetail = "	<div class='warehouse-detail' >Warehouse Section <span class='glyphicon glyphicon-chevron-down'></span></div>
									<div class='warehouse-descp' style='display:block;'>
									<table width='100%'><tr><td>Name</td><td>Description</td></tr>
									
									";
									if(is_array($warehouseData) && count($warehouseData) >0)
									{
											
											
											foreach($warehouseData as $row)
											{
												$name 			= $row->name;
												$description 	= $row->description;
												
												$warehousesectiondetail.= "<tr><td>".$name."</td><td>".$description."</td></tr>";
											}
											
											
									}
									else
									{
										$warehousesectiondetail.= "<tr><td>No Record Found</td></tr>";
									}
									
									$warehousesectiondetail.= "</table></div>
										<script>
											jQuery(document).ready(function(){
												jQuery('.warehouse-detail').on('click',function(){
													jQuery('.warehouse-descp').toggle();
												});
											});
										</script>
										";
										
										$v[] = $warehousesectiondetail;
									
									
									// ZOrder::UpdateShippingState(ZOrder::ShipStat_PickedUp, $coupon['order_id']);
									Coupon::UpdateShippingState(ShipStat_PickedUp,$cp_order_id);
								}
							} 
						
						}
				//credit to user'money'
				//$tip = ($coupon['credit']>0) ? " Rebate:{$coupon['credit']}$" : '';
				$v[] = 'Bon valable';
				$v[] = 'Bon validé le:' . date('Y-m-d H:i:s', time());
				
				$valid = true;
			}
			
		}
		else
		{
			$v[] = 'Invalid coupon';
		}
		
		$v = implode('<br/>', $v);
		$v = json_encode($v); 
		
		$class ="alert-danger";
		if($valid)
		{
			$class ="alert-success";
		}		
		echo $v."--".$class;
		exit;
	}
	
	
	/**
	* For QRCODE 
	* 
	* When QR Code scane, we get a URL, which fires this function
	* 
	* Sample
	* 	http://example.com/tdadmin/partner/qrcode/?id=COUPON_ID&sec=PIN_CODE
	*/
	public function qrCode(){

			$cid = Input::get('id');
			$sec = Input::get('sec');

			// $coupon = Table::FetchForce('coupon', $cid);
			$coupon = DB::table('coupon')->where('id', $cid)->get();
			
			if ( !$coupon && !isset($coupon['0']))
			{
				return Redirect::to('partner/dashboard');
			}
			
			//GET COUPON DELIVERY TYPE ON WHICH BASIS REDIRECTION SET FOR OLD OR NEW Partner Backend Redirection 
			//for pickup new backend & Non-pickup coupon to be served from Old Partner backend
			$couponDeliveryType = Coupon::getCouponDeliveryType($cid);
			
			if($couponDeliveryType != "pickup"){
				
				$redirectURL = TOPDEAL_URL_LINK."/biz/qrcode.php?id={$cid}&sec={$sec}";
				return Redirect::to($redirectURL);
			}
			
			
			if($coupon['0']){
				
				$coupon	=	$coupon['0'];

				$cp_partner_id	=	$coupon->partner_id;
				
				if(isset(Auth::user()->id)){
					// $partner = Table::Fetch('partner', $coupon['partner_id']);
					// $partner_id = abs(intval($_SESSION['partner_id']));
					//$partner_id = abs(intval($_SESSION['partner_id']));
					// $login_partner = Table::Fetch('partner', $partner_id);

					//var_dump($login_partner); var_dump($coupon); exit;
					$login_partner	=	Auth::user()->id;
					if ($cp_partner_id == $login_partner )
					{
						Session::put('consume_id', $cid);
						Session::put('consume_secret', $sec);
						return Redirect::to('partner/dashboard');
					}
					else 
					{
						//$team = Table::Fetch('team', $coupon['team_id']);
					   // redirect(ZTeam::GetTeamUrl($team,false,true));
					}

					return Redirect::to('partner/dashboard');
				}
			}else{
				return Redirect::to('partner/dashboard');
			}
	}
}