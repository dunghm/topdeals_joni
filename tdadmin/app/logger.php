<?php
/*
 * @ref:	http://www.laravel-tricks.com/tricks/loggingsaving-all-db-queries-in-log-file
 * @ref:	http://www.codediesel.com/php/debuggin-laravel-with-monolog-and-firephp/
 */
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

function logger($msg, $level='INFO', $file_name='log-')
{
	$monolog = new Logger('log');
	
	switch ($level){
		case 'ALERT':
			$monolog->pushHandler(new StreamHandler(storage_path('logs/'.$file_name.date('Y-m-d').'.txt')), Logger::ALERT);
			$monolog->alert($msg);
		break;
		
		case 'CRITICAL':
			$monolog->pushHandler(new StreamHandler(storage_path('logs/'.$file_name.date('Y-m-d').'.txt')), Logger::CRITICAL);
			$monolog->critical($msg);
		break;
		
		case 'EMERGENCY':
			$monolog->pushHandler(new StreamHandler(storage_path('logs/'.$file_name.date('Y-m-d').'.txt')), Logger::EMERGENCY);
			$monolog->emergency($msg);
		break;
		
		case 'INFO':
			$monolog->pushHandler(new StreamHandler(storage_path('logs/'.$file_name.date('Y-m-d').'.log')), Logger::INFO);
			$monolog->info($msg);
		break;
		
		case 'NOTICE':
			$monolog->pushHandler(new StreamHandler(storage_path('logs/'.$file_name.date('Y-m-d').'.txt')), Logger::NOTICE);
			$monolog->notice($msg);
		break;
		
		case 'DEBUG':
			$monolog->pushHandler(new StreamHandler(storage_path('logs/'.$file_name.date('Y-m-d').'.txt')), Logger::DEBUG);
			$monolog->debug($msg);
		break;
		
		case 'ERROR':
			$monolog->pushHandler(new StreamHandler(storage_path('logs/'.$file_name.date('Y-m-d').'.txt')), Logger::ERROR);
			$monolog->error($msg);
		break;
		
		case 'WARNING':
			$monolog->pushHandler(new StreamHandler(storage_path('logs/'.$file_name.date('Y-m-d').'.txt')), Logger::WARNING);
			$monolog->warning($msg);
		break;
	}
}