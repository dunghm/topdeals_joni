<?php
require_once CONF_LIB. 'skeletons/iPaymentProcessor.php';
require_once CONF_LIB. 'skeletons/PaymentProcessorGateways.php';
require_once CONF_LIB. 'skeletons/PaymentProcessorTransactionTypes.php';

require_once CONF_LIB. 'authorize/anet_php_sdk/AuthorizeNet.php';

/**
Response for Duplicate Transactions
The Card Present API allows you to specify the window of time after a transaction is 
submitted during which the payment gateway checks for a duplicate transaction. To use 
this functionality, you must pass the Duplicate Window (x_duplicate_window) field with a 
value between 0 to 28800 seconds (maximum of 8 hours). 
If the transaction request does not include the Duplicate Window field, and the payment 
gateway detects a duplicate transaction within the system default window of 2 minutes, 
the gateway response will contain the response code of 3 (processing error) with a reason 
code of 11 (duplicate transaction) and no additional details. 
If the transaction request does include the Duplicate Window field and value, and the 
payment gateway detects a duplicate transaction within the window of time specified, the 
gateway response for the duplicate transaction will also include information about the 
original transaction (as outlined below).
If the original transaction was declined, and a value was passed in the Duplicate Window 
field, the payment gateway response for the duplicate transaction will include the following 
information for the original transaction:
	The AVS Code result
	The Card Code result
	The Transaction ID
	The MD5 Hash
	The User Reference
 * */
CLASS PaymentProcessorDuplicate{}

/**
 * @package 	iPaymentProcessor
 * @subpackage 	PaymentProcessor
 * @author 	Nouman
 * @since	March20,2014
 * @version 0.1
 * @desc
 * 	Payment Processor Interface
 * 
 * Default Payment Process is to be choosen based on the Platform we are going to use Payment Processor
 * 
 * In our case we use Authorize.net CardPresent
 * 
 * */
CLASS PaymentProcessor implements iPaymentProcessor {
	
	protected $test_mode = false;
	
	//private $PaymentProcessor;
	private $api_login_key;
	private $api_transaction_key;
	
	private $amount;
	private $manual_process=FALSE; # SwipeCard by default
	private $device_type=4;
	
	/*
	 * --- Card Present ----
	 * */
	private $track1data;
	private $track2data;
	
	/*
	 * --- Credit Card ----
	 * */
	private $credit_card;
	
	private $expiry; 		# Expiry yy/mm
	private $expiry_year;	# yy
	private $expiry_month;	# mm
	
	
	private $processor;
	
	# Last called payment action 
	public $last_payment_action = false; # AUTH_CAPTURE, AUTH, CAPTURE, VOID, REFUND
	public $request;
	public $response;
	public $result; # Abstract resposne
	
	/*
	 * AUTHORIZE_CP 	(Card Present) 		(Offline - Store)
	 * AUTHORIZE_CNP	(Card NOT Present) 	(Online  - E-Commerce)
	 * AUTHORIZE_AIM	(Authorize One time payment)
	 * AUTHORIZE_CIM	(Authorize Customer Profile)
	 * CYBERSOURCE
	 * PAYPAL
	 * I_PAY
	 * SAGE_PAY
	 * */
	private $gateway_list = array(
		PaymentProcessorGatewaysList::AUTHORIZE		=> PaymentProcessorGatewaysList::AUTHORIZE,
		PaymentProcessorGatewaysList::AUTHORIZE_CIM		=> PaymentProcessorGatewaysList::AUTHORIZE_CIM,
		PaymentProcessorGatewaysList::CYBERSOURCE	=> PaymentProcessorGatewaysList::CYBERSOURCE,
		PaymentProcessorGatewaysList::PAYPAL		=> PaymentProcessorGatewaysList::PAYPAL,
		PaymentProcessorGatewaysList::I_PAY			=> PaymentProcessorGatewaysList::I_PAY,
		PaymentProcessorGatewaysList::SAGE_PAY		=> PaymentProcessorGatewaysList::SAGE_PAY,
	); 
	
	private $transaction_type 	= PaymentProcessorTransactionTypes::CARD_PRESENT;
	private $default_gateway 	= PaymentProcessorGatewaysList::AUTHORIZE;
	//private $default_gateway 	= PaymentProcessorGatewaysList::AUTHORIZE_CIM;
	private $transaction_mode	= 'AUTHORIZE_CP';
	
	private $isOk 		= false;
	private $isError 	= false;
	private $error_message 	= 'Unknown Error';
	
	private $is_store_order			= true;
	private $is_validation_error 	= false;
	
	/**
	 * @method
	 * AUTH
	 * AUTH_CAPTURE
	 * REFUND
	 * VOID
	 * */
	private $_METHOD_AUTH 			= false;
	private $_METHOD_AUTH_CAPTURE	= false;
	private $_METHOD_REFUND = false;
	private $_METHOD_VOID 	= false;
	
	/**
	 * Payment Processor's Transaction ID
	 * */
	private $trans_id;
	public $customerProfileId;
	
	//const CS_LIVE_URL 			= 'https://ics2ws.ic3.com/commerce/1.x/transactionProcessor/CyberSourceTransaction_1.86.wsdl';
	//const CS_SANDBOX_URL		= 'https://ics2wstest.ic3.com/commerce/1.x/transactionProcessor/CyberSourceTransaction_1.86.wsdl';
	
	//const AUTH_LIVE_URL 		= 'https://secure.authorize.net/gateway/transact.dll';
	//const AUTH_SANDBOX_URL 		= 'https://test.authorize.net/gateway/transact.dll';
	//const AUTH_SANDBOX_URL_CP 	= 'https://cardpresent.authorize.net/gateway/transact.dll';
	
	
	/*
	 * ##############################
	 * LIST OF PRIVATE FUNCTIONS
	 * ##############################
	 * */
	function _secureString($str=null){
		$string = $str;
		$string = substr($string, 0, 2); 		# Extract initial 2 characters
		$string	= str_pad($string, 6, "X", STR_PAD_RIGHT);
		$string	.= substr($str, -4);	# Extract last 4 characters
		return $string;
	}
	
	function __construct($api_login_key=null, $api_transaction_key=null, $default_gateway=null, $sandbox=false){
	//function __construct($default_gateway=false){
		$log = new Logging();
		$log->log("#####################################################################");
		$log->log("         CONSTRUCT PAYMENT PROCESSOR - {$this->default_gateway}");
		$log->log("#####################################################################");
		//$log->log("API_KEY:{$api_login_key}|API_TRANSACTION_KEY:{$api_transaction_key}");
		$log->log("API_KEY:". $this->_secureString($api_login_key)."|API_TRANSACTION_KEY:". $this->_secureString($api_transaction_key));
		$this->setApiLoginKey($api_login_key);
		$this->setApiTransactionKey($api_transaction_key);
		//($api_login_key 		? $this->api_login_key 			= $api_login_key : 			null);
		//($api_transaction_key 	? $this->api_transaction_key 	= $api_transaction_key : 	null);
		
		($default_gateway ? 
			(in_array($default_gateway, $this->gateway_list)) 
				? $this->default_gateway = $default_gateway 
				: $this->default_gateway
			: null
		);
		$log->log("DefaultGateway:{$this->default_gateway}");
		
		# Default Payment Gateway is Authorize.net with CardPresent
		if($this->default_gateway == PaymentProcessorGatewaysList::AUTHORIZE && $this->transaction_type == PaymentProcessorTransactionTypes::CARD_PRESENT){
			$this->processor = new AuthorizeNetCP($api_login_key, $api_transaction_key);
			
			/*
			 * NOTE:	
			 * 
			 * *** 	DONOT CHANGE THESE LINE OF CODE UNTIL AND UNLESS WE KNOW WHAT WE ARE GOING TO
			 * 		DO. SETTING VALUE FALSE WILL ENABLE LIVE PAYMENT PROCESSING IF TEST MODE IS OFF
			 * 		ON AUTHORIZE.NET WEB SITE 
			 * 
			 * When we use LIVE account, value of x_test_request will TURN TEST MODE ON/OFF
			 * 
			 * SET x_test_request = TRUE will enable TEST MODE ON even if TEST MODE is off on LIVE
			 * SET x_test_request = FALSE will let Authorize.net use what is set on LIVE
			 * */
			$this->processor->setField('test_request', 'TRUE');
			$log->log("Setting default AUTHORIZE_MODE:TRUE");
			
			$TEST_REQUEST = IS_TEST_PAYMENT();
			if(false !== $TEST_REQUEST)
				$this->processor->setField('test_request', 'FALSE');
			
			$AUTHORIZE_MODE = get_AUTHORIZE_MODE();
			$log->log("AUTHORIZE_MODE VALUE:{$AUTHORIZE_MODE}");
			if(!empty($AUTHORIZE_MODE) && $AUTHORIZE_MODE == 'PRODUCTION'){
				$this->processor->setField('test_request', 'FALSE');
				$log->log("Setting preference AUTHORIZE_MODE:FALSE");
			}

            /*
			//$this->processor = new AuthorizeNetCP(CP_API_LOGIN_ID, CP_TRANSACTION_KEY);
			$this->processor->setSandbox(true);
				
			if(APP_MODE == APPMODE_LIVE)
				$this->processor->setSandbox(false);

			# if SANDBOX is set in the file, override settings
			if(defined('AUTHORIZENET_SANDBOX'))
				$this->processor->setSandbox(AUTHORIZENET_SANDBOX);
			//$this->setEndPoint();
			*/
			
			$log->log("TYPE: CARD PRESENT");
			$this->processor->setEndPointUrl(get_AUTHORIZENET_CARDPRESENT_ENDPOINT());
			$log->log("ENDPOINT: ". get_AUTHORIZENET_CARDPRESENT_ENDPOINT());
			$this->processor->setField('device_type', $this->getDeviceType());
			
		//}else{
			//$this->processor = new AuthorizeNetAIM;
		}else{
			$log->log("TYPE: AuthorizeNetCIM");
			$this->processor = new AuthorizeNetCIM($api_login_key, $api_transaction_key);
		}
		
	}
	
	function connection(){
		
		$processor = $this->processor;
		if(empty($this->processor)){
			$processor = new PaymentProcessor();
			$this->processor = $processor;
		}
		return $processor;
	}
	
	/*
	function setEndPoint($endPointUrl=null){
		
		if(null==$endPointUrl){
			$endPointUrl = get_AUTHORIZENET_CARDPRESENT_ENDPOINT();
			$this->processor->setEndPointUrl($endPointUrl);
		}
		$this->endPointUrl = $endPointUrl;
	}
	
	function getEndPoint(){
		return $this->endPointUrl;
	}
	*/
	
	function switchGateway($gateway=PaymentProcessorGatewaysList::AUTHORIZE){

		//if($gateway=='C')
		$this->defualt_gateway = PaymentProcessorGatewaysList::CYBERSOURCE;
		$this->processor = new AuthorizeNetCP(CP_API_LOGIN_ID, CP_TRANSACTION_KEY);
			
	}
	
	function switchToTestMode($on=true){
        $log = new Logging();
        $log->log('########### TRAINING MODE ###########');
		$this->test_mode = $on;
	}
	
	function setApiLoginKey($api_login_key){ 
		
		($api_login_key ? $this->api_login_key = $api_login_key: null);
		
		if(empty($this->api_login_key) || null == $this->api_login_key)
			$this->_validationError('API_LOGIN_KEY cannot be empty. Merchant credentials not found.');
		
		if($this->isError())
			return ($this->result['auth_message']);
	}
	
	function setApiTransactionKey($api_transaction_key){
		
		
		($api_transaction_key ? $this->api_transaction_key = $api_transaction_key: null);
		
		if(empty($this->api_transaction_key) || null == $this->api_transaction_key)
			$this->_validationError('API_TRANSACTION_KEY cannot be empty. Merchant credentials not found.');
		
		if($this->isError())
			return ($this->result['auth_message']);
		
	}

	function isOk(){return $this->isOk;}
	function isError(){return $this->isError;}
	
	function setTransactionId($transaction_id=false){
		
		($transaction_id ? $this->trans_id = $transaction_id: null);
		
		if(empty($this->trans_id) || null == $this->trans_id)
			$this->_validationError('Transaction ID cannot be empty.');
		
		if($this->isError())
			return ($this->result['auth_message']);
			
		if(method_exists($this->processor, 'setField'))
			$this->processor->setField('ref_trans_id', $this->trans_id);
        
	}
	
	function getTransactionId(){
		return $this->trans_id;
	}
	
	/**
	 * http://www.acmetech.com/documentation/javascript/parse_magnetic_track_javascript.html
	  //-- Example: Track 1 & 2 Data
	  //-- %B1234123412341234^CardUser/John^030510100000019301000000877000000?;1234123412341234=0305101193010877?
	  //-- Key off of the presence of "^" and "="
	
	  //-- Example: Track 1 Data Only
	  //-- B1234123412341234^CardUser/John^030510100000019301000000877000000?
	  //-- Key off of the presence of "^" but not "="
	
	  //-- Example: Track 2 Data Only
	  //-- 1234123412341234=0305101193010877?
	  //-- Key off of the presence of "=" but not "^"
	 * 
	 * @param (string) $track1data Set Credit Card Track1 data
	 * 
	 * */
	function setTrack($track_string){

		$log = new Logging();
		$log->log("Setting track data");
		
		/*
		$track_1_2	= '%B4111111111111111^CARDUSER/JOHN^1803101000000000020000831000000?;4111111111111111=1803101000020000831?';
		$track_1	= '%B4111111111111111^CARDUSER/JOHN^1803101000000000020000831000000?';
		$track_2	= ';4111111111111111=1803101000020000831?';
		*/
		
		//echo '<hr/>';
		//debug($track_string);
		//debug(explode('?;', $track_string));
		//$s1 = substr($track_string, 0, stripos($track_string, '?;')+1);
		//$s2 = substr($track_string, stripos($track_string, '?;')+1);
		//debug($s1);
		//debug($s2);
		//exit;
		
		$is_track1_2	= false;
		$is_track1		= false;
		$is_track2		= false;
		
		# Track 1_2
		if (preg_match('/^%.*\?;.*\?$/', $track_string))
			$is_track1_2 = true;
			
		# Track 1
		if (preg_match('/^%.*\?$/', $track_string))
			$is_track1 = true;
			
		# Track 2
		if (preg_match('/^;.*\?$/', $track_string))
			$is_track2 = true;
		
		if($is_track1_2){
			$log->log("Found Track1&2 data");
			$track_data1 = substr($track_string, 0, stripos($track_string, '?;')+1);
			$track_data2 = substr($track_string, stripos($track_string, '?;')+1);
			$this->setTrack1($track_data1);
			$this->setTrack2($track_data2);
		}elseif($is_track1){
			$log->log("Found Track1 data");
			$this->setTrack1($track_string);
		}elseif($is_track2){
			$log->log("Found Track2 data");
			$this->setTrack2($track_string);
		}else{
			$this->_validationError('Error in Track data, please swipe card again.');
			$log->log("TrackData: {$track_string}");
		}
			
	}
	
	/**
	 * @param (string) $track1 Set Credit Card Track1 data
	 * */
	function setTrack1($track1data){
		$this->track1data = $track1data;
		$this->processor->setTrack1Data($track1data);
        //$this->processor->setTrack1Data('%B4111111111111111^CARDUSER/JOHN^1803101000000000020000831000000?');
	}
	
	/**
	 * @param (string) $track2 Set Credit Card Track1 data
	 * */
	function setTrack2($track2data){
		$this->track2data = $track2data;
        $this->processor->setTrack2Data($track2data);
	}
	
	/**
	 * @param (string) $credit_card Credit Card number
	 * */
	function setCreditCard($credit_card=false, $full=true){
		$log = new Logging();
		//$this->credit_card = $credit_card;
        //$this->processor->setField('card_num', $credit_card);
		($credit_card ? $this->credit_card = $credit_card: null);
		$log->log("Set CreditCard: ". $this->_secureString($this->credit_card));
		
		$this->credit_card = $this->credit_card;
		if(false === $full)
			$this->credit_card = _getLast4Digits($this->credit_card);
			
		if(method_exists($this->processor, 'setField'))
			$this->processor->setField('card_num', $this->credit_card);
	}
	
	function getCreditCard($credit_card=false, $full=true){
		return $this->credit_card;
	}
	
	/**
	 * @param (float) $amount Amount to charge/refund 
	 * */
	function setAmount($amount=false){
		
		$log = new Logging();
		$log->log("Amount Validation");
		
		($amount ? $this->amount = $amount: null);
		$log->log("Amount:{$this->amount}");
		
		if(empty($this->amount) || null == $this->amount)
			$this->_validationError('Amount cannot be empty');
		
		//if(!(is_float($this->amount) || is_double($this->amount) || is_string($this->amount)))
			//$this->_validationError('Amount value must be float OR doulbe OR string');
			
		if($this->isError())
			return ($this->result['auth_message']);
			
		$log->log("Set Amount:$amount");
		
		if(method_exists($this->processor, 'setField'))
			$this->processor->setField('amount', $this->amount);

	}
	
	function getAmount(){
		return $this->amount;
	}
	
	function getExpiry(){
		return $this->expiry;
	}
	
	/**
	 * @param (string) $expiry(mmyy) expiry MonthYear of Credit Card
	 * */
	function setExpiry($expiry=false){
		$log = new Logging();
		$log->log("Set Expiry: $expiry");
		//$this->expiry = $expiry;
		//$this->processor->setField('exp_date', $expiry);
		($expiry ? $this->expiry = $expiry: null);
		
		if(method_exists($this->processor, 'setField'))
			$this->processor->setField('exp_date', $this->expiry);
	}

	/**
	 * @param (string) $ expiry year of Credit Card
	 * */
	function setExpiryYear($year){
		$this->expiry_year = $year;
	}
	
	/**
	 * @param (string) $month expiry month of Credit Card 
	 * */
	function setExpiryMonth($month){
		$this->expiry_month = $month;
	}
	
	/**
     * Device Types (x_device_type)
     * 1 = Unknown
     * 2 = Unattended Terminal
     * 3 = Self Service Terminal
     * 4 = Electronic Cash Register
     * 5 = Personal Computer- Based Terminal
     * 6 = AirPay
     * 7 = Wireless POS
     * 8 = Website
     * 9 = Dial Terminal
     * 10 = Virtual Terminal
	 * 
	 * @param (string) $device_type Device used in Terminal 
	 * */
	function setDevice($device_type){
		$this->device = $device_type;
		if(method_exists($this->processor, 'setField'))
			$this->processor->setField('device_type', $device_type);
	}
	
	/**
	 * @param void
	 * return DeviceType of Termnial 
	 * */
	function getDeviceType(){
		return $this->device_type;
	}
	
	/**
	 * @param void
	 * Change Swipe to Manual Credit Card entry mode 
	 * */
	function switchToManualMode(){
		return $this->manual_process = true;
	}
	
	/**
	 * @param void
	 * Change Swipe to Manual Credit Card entry mode 
	 * */
	function isManualProcessing(){
		return $this->manual_process;
	}
	
	function isValidationError(){
		return $this->is_validation_error;
	}
	
	function _validationError($msg=null, $type='system'){
		$log = new Logging();
		$log->log("There is a validation error in the system");
		$this->is_validation_error = true;
		$this->_error($msg, $type);
	}
	
	function _error($msg = 'One of the field has error', $type='system'){
		
		$log = new Logging();
		$log->log("Error:{$msg}");
		$this->isOk		= FALSE;
		$this->isError 	= TRUE;
		$this->error_message = $msg;
		
		$this->result['OK'] 			= $this->isOk == true ? 1 : 0;
		$this->result['type']			= $type;#'system';
		$this->result['error'] 			= 'true';
		$this->result['decision'] 		= $this->isOk == true ? 'ACCEPT' : 'REJECT';
		$this->result['reason_code']	= '-1';
		$this->result['auth_message']	= $msg;

		if($this->isError() && $type=='system')
			return ($this->result['auth_message']);
		
	}
	
	function _validate(){

		if(empty($this->amount)){
			$this->_validationError('Amount not set');
		}
		if($this->isManualProcessing()){
			
			if(empty($this->credit_card)){
				$this->_validationError('Credit card issue');
			}
			if(empty($this->expiry)){
				$this->_validationError('Expiry date issue');
			}
			
		}else{
			if(empty($this->track1data) && empty($this->track2data) && empty($this->credit_card)){
				$this->_validationError('Track1, Track2 and Credit Card data are missing. One of the data is required');
			}
			if(empty($this->expiry) && !empty($this->credit_card)){
				$this->_validationError('Credit Card expiry cannot be empty');
			}
			
		}
		
	}

	function _process($response = ''){
		
        $log = new Logging();
        $log->log('We are in function : '. __METHOD__);
        
		if($this->test_mode){

			$log->log("TRAINING=YES");
			$log->log("Preparing fake data");
			
	        $this->isOk = true;
	        $this->isError = false;
			$this->result = array(
				'OK' 				=> 1,
				'decision' 			=> 'ACCEPT',
				'type' 				=> $this->default_gateway,
				'error' 			=> 'false',
				'amount' 			=> $this->amount,
				'transaction_id'	=> '000123000',
	        	'auth_code' 		=> '000123',
				'auth_message'		=> 'Test Mode Tranasction completed successfully',
				'response_code'		=> 1,
				'reason_code'		=> 1,
			);
			
		}else{
		
			$log->log("TRAINING=NO");
			$log->log("Logging data in Database");
			
			$logProcessor = new LogPaymentProcessors();
			$logProcessor->setType($this->default_gateway);
			$logProcessor->setRequest($this->processor->request);
			$logProcessor->setResponse($this->processor->response);
			//$logProcessor->type		= $this->default_gateway;
			//$logProcessor->request 	= serialize($this->processor->request);
			//$logProcessor->response = serialize($this->processor->response);
			$logProcessor->save();
			
			if($response != '')
				$this->response = $response;
			else
				$log->log("PaymentProcessor response no found");
	        
			$log->log("Checking PaymentProcessor");
			$log->log("PaymentGateway:". $this->default_gateway);
			
			if($this->default_gateway == PaymentProcessorGatewaysList::AUTHORIZE_CIM){
				
		        $this->isOk = ($this->response->OK == 1 ? $this->isOk = true : $this->isError = false);
				$this->result = array(
					'OK' 				=> $this->isOk == true ? 1 : 0,
					'decision' 			=> $this->isOk == true ? 'ACCEPT' : 'REJECT',
					'type' 				=> PaymentProcessorGatewaysList::AUTHORIZE_CIM,
					'error' 			=> 'false',
					'amount' 			=> isset($this->response->responseArray->amount) ? $this->response->responseArray->amount : 0,
					'transaction_id'	=> isset($this->response->responseArray->transaction_id) ? $this->response->responseArray->transaction_id : 0,
		        	'auth_code' 		=> isset($this->response->responseArray->authorization_code) ? $this->response->responseArray->authorization_code : 0,
					'auth_message'		=> isset($this->response->responseArray->response_reason_text) ? $this->response->responseArray->response_reason_text : 0,
					'response_code'		=> isset($this->response->responseArray->response_code) ? $this->response->responseArray->response_code : 0,
					'reason_code'		=> isset($this->response->responseArray->response_reason_code) ? $this->response->responseArray->response_reason_code : 0,
				);
				
			}else{
			
		        $this->isOk = ($this->response->approved == 1 ? $this->isOk = true : $this->isError = false);
				$this->result = array(
					'OK' 				=> $this->isOk == true ? 1 : 0,
					'decision' 			=> $this->isOk == true ? 'ACCEPT' : 'REJECT',
					'type' 				=> PaymentProcessorGatewaysList::AUTHORIZE,
					'error' 			=> 'false',
					'amount' 			=> $this->response->amount,
					'transaction_id'	=> $this->response->transaction_id,
		        	'auth_code' 		=> $this->response->authorization_code,
					'auth_message'		=> $this->response->response_reason_text,
					'response_code'		=> $this->response->response_code,
					'reason_code'		=> $this->response->response_reason_code,
				);
				
			}
			//debug('$this->result');
			//debug($this->result);
			//exit;
			
		}
		$log->log("this->result => ". var_export($this->result, true));

		if($this->isOk)
			$log->log("Payment Processed successfully");
		else
			$log->log("Error in Payment Processing");
		
		return $this->result;

	}
	
	function getResult(){
		return $this->result;
	}
	
	/*
	 * ###########################
	 * Payment Processing Methods
	 * ###########################
	 * */
	
	function authorize($amount=null){}
	
	function capture($auth_code = false, $amount = false){}
	
	function authorizeAndCapture($amount=false, $credit_card=false, $expiry=false){
		
		$log = new Logging();
		$log->log(__METHOD__);
		$this->last_payment_action = 'AUTH_CAPTURE';
		
		if($this->test_mode)
			return $this->_process();
			
		/**
		 * FIXME: Remove following instructions after testing
		 * 
		 * Change 
		 * 	($amount ? $this->amount = $amount : null);
		 * TO
		 * 	($amount ? $this->setAmount($amount): null);
		 * 
		 * to handle single place validations
		 * */
		//( $amount		? $this->amount = $amount : null);
		( $amount		? $this->setAmount($amount) : null);
		( $credit_card	? $this->setCreditCard($credit_card) : null);
		( $expiry		? $this->setExpiry($expiry) : null);

		# Validate all fields have valid data
		$this->_validate();
		
		if(!$this->isValidationError()){
			$duplicate_window = get_AUTHORIZENET_DUPLICATE_WINDOW();
			if(!empty($duplicate_window)){
				if(method_exists($this->processor, 'setField'))
					$this->processor->setField('duplicate_window', $duplicate_window);
			}
			$log->log("duplicate_window:$duplicate_window");
			return $this->_process($this->processor->authorizeAndCapture());
			//$this->assertTrue($response->isAuthorizeNet(CP_API_LOGIN_ID));
			//return $this->_process($this->processor->authorizeAndCapture($this->amount, $this->credit_card, $this->expiry));
		//}else{
			//$this->_validationError('INTERNAL ERROR: Validation issue');
		}
		
		return $this->result;
		//return false;
		
	}
	
	/**
	 * Return Authorize.NET order
     * 
     * In CyberSource case, we don't have void case as we are using CustomerProfile for online E-Commerce system
     * 
     * Return Cases:
     * 	Store Shopping 	-> Return on store 	-> 	(Authorize.NET)
     * 	Store Shopping 	-> Return online	-> 	(Authorize.NET)
     * 	Online Shopping -> Return online	-> 	(CyberSource)
     * 	Online Shopping -> Return on store 	-> 	(CyberSource)
     * 
     * In one Business day (VOID) otherwise refund
     * @param (float) 	$amount 		Amount to refund
     * @param (string) 	$trans_id 		Authorize.NTE auth_trans_id
     * @param (string) 	$credit_card 	Customer's CC last 4 digits
     * @param (string) 	$expiry 		Customer's CC expiry
	 * */
	function refundPaymentByAuthorize($amount = false, $trans_id = false, $credit_card = false, $expiry = false){
		
		$log = new Logging();
		$log->log(__METHOD__);
		
		$this->last_payment_action = 'REFUND_AUTHORIZE_CP';
		$this->_METHOD_REFUND = TRUE;
		
		if($this->test_mode)
			return $this->_process();
			
		
		( $amount		? $this->setAmount($amount) 		: null);
		( $trans_id		? $this->setTransactionId($trans_id): null);
		( $credit_card	? $this->setCreditCard($credit_card, false): null);
		//( $expiry		? $this->setExpiry($expiry) 		: null);
		
		if(empty($this->amount))
			$this->_validationError('Amount cannot be null');

		if(empty($this->trans_id))
			$this->_validationError('Authorize Transaction ID cannot be null');

		if(empty($this->credit_card))
			$this->_validationError('Credit Card cannot be null');

		if(strlen($this->credit_card) < 4)
			$this->_validationError('Credit Card minimum length is 4 digits long');
			
		//if(empty($this->expiry))
			//$this->_validationError('Expiry cannot be null');
		
		//$log->log("Validation passed");
		
		if(!$this->isValidationError()){
			if(method_exists($this->processor, 'setField'))
				$this->processor->setField('duplicate_window', get_AUTHORIZENET_DUPLICATE_WINDOW());
			return $this->_process($this->processor->credit($this->trans_id, $this->amount, $this->credit_card));
		}		
		return $this->result;
		
		
	}
	
	/**
	 * Return Authorize.NET order
     * 
     * In CyberSource case, we don't have void case as we are using CustomerProfile for online E-Commerce system
     * 
     * Return Cases:
     * 	Store Shopping 	-> Return on store 	-> 	(Authorize.NET)
     * 	Store Shopping 	-> Return online	-> 	(Authorize.NET)
     * 	Online Shopping -> Return online	-> 	(CyberSource)
     * 	Online Shopping -> Return on store 	-> 	(CyberSource)
     * 
     * In one Business day (VOID) otherwise refund
     * @param (float) 	$amount 	Amount to refund
     * @param (string) 	$trans_id 	Authorize.NTE auth_trans_id
     * @param (boolean) $void		Action to take (TRUE=VOID/FALSE=REFUND)
     * 	if $void is TRUE, we try to VOID transaction in Authorize.NET and if it fails, we try to
     * 	refund payment
	 * */
	function voidPaymentByAuthorize($trans_id = false){

		$void = true;
		$this->last_payment_action = 'VOID_AUTHORIZE';
		
		( $trans_id		? $this->setTransactionId($trans_id): null);
		( $void 		? $this->_METHOD_VOID 	= $void		: null);
		
		if(!$void)
			( $trans_id	? $this->setTransactionId($trans_id): null);
		
		if(empty($this->trans_id))
			$this->_validationError('Authorize Transaction ID cannot be null');
		
			# For VOID
			//$this->processor->setField('ref_trans_id', $this->trans_id);
			$this->response = $this->processor->void($this->trans_id);
			
			if(isset($this->response->approved) && !$this->response->approved){
				
				/*
				Concord EFS � This transaction is not allowed. The Concord EFS processing 
				platform does not support voiding credit transactions. Please debit the credit card
				instead of voiding the credit.
				 * */
				if($this->response->response_code ==3 && ($this->response->response_reason_code==175)){
					$this->_validationError('Transaction cannot void, you need to refund amount.');
				//}else{
					//$this->_validationError('Unable to VOID transaction, please try again');
				}
				
			}
			
		if(!$this->isValidationError()){
			return $this->_process($this->response);
		}		
		return $this->result;
		
	}

	
	/**
     * Do a VOID transaction.
     * 
     * VOID only perform in Authorize.NET AIM 
     * In CyberSource case, we don't have void case as we are using CustomerProfile for online E-Commerce system
	 * */
	function void($trans_id=false){
		
		exit;
		//echo $trans_id;exit;
		# If Authorize.NET 
		# https://issues.openbravo.com/view.php?id=10023
		# http://community.developer.authorize.net/t5/Integration-and-Testing/Card-Present-Integration-Void-and-Refund-Issue/td-p/10770
		if(method_exists($this->processor, 'setField'))
			$this->processor->setField('ref_trans_id', $trans_id);
		$this->response = $this->processor->void($trans_id);
		return $this->_process();
        
	}

	/*function refund($transaction_id, $amount, $credit_card, $expiry){
		
		exit;
		if(method_exists($this->processor, 'setField'))
			$this->processor->setField('duplicate_window', 1);
		//$this->setExpiry('0423');
		$r = $this->processor->credit(
			//'2213942321', 
			'2213804135', 
			'0.01',
			//'4699'
			'1111'
			);
		
		/*
			$this->setExpiry('0415');
			$r = $this->processor->credit(
				'2209880541', 
				'1',
				'4111111111111111'
				);
			//return $r;
		 
		debug($r);				

	}*/
	
	function _testRefund(){
		
		exit;
        $sale = new AuthorizeNetCP(CP_API_LOGIN_ID, CP_TRANSACTION_KEY);
        $sale->setField('exp_date', '0415');
        $response = $sale->credit('2209880541', '1', '4111111111111111');
        debug($response);
        debug($sale);
        exit;
        
		
		exit;
		//$this->setExpiry('0318');
		$this->setExpiry('0415');
		//$this->setCreditCard('4111111111111111');
		//$this->setCreditCard('1881');
		//$this->setCreditCard('XXXX1111');
		//if(method_exists($this->processor, 'setField'))
		//$this->processor->setField('card_num', 'XXXX1111');
		$this->setCreditCard('1111');
        $this->processor->setField('card_num', '1111');
        
		$r = $this->processor->credit(
			//'IYBXKW', 
			'2209880541', 
			'1',
			'1111'
			//'XXXX1111'
			//'4111111111111111'
			//'40121881'
			);
		debug($r);		
	}

	function _sample(){
		
		/**
		 * Return Authorize.NET order
	     * 
	     * In CyberSource case, we don't have void case as we are using CustomerProfile for online E-Commerce system
	     * 
	     * Return Cases:
	     * 	Store Shopping 	-> Return on store 	-> 	(Authorize.NET)
	     * 	Store Shopping 	-> Return online	-> 	(Authorize.NET)
	     * 	Online Shopping -> Return online	-> 	(CyberSource)
	     * 	Online Shopping -> Return on store 	-> 	(CyberSource)
	     * 
	     * In one Business day (VOID) otherwise refund
	     * @param (float) 	$amount 	Amount to refund
	     * @param (string) 	$trans_id 	Authorize.NTE auth_trans_id
	     * @param (boolean) $void		Action to take (TRUE=VOID/FALSE=REFUND)
	     * 	if $void is TRUE, we try to VOID transaction in Authorize.NET and if it fails, we try to
	     * 	refund payment
		 * */
		function returnPaymentByAuthorize($amount = false, $trans_id = false, $void=false){
	
			$this->last_payment_action = 'REFUND_AUTHORIZE';
			
			//($amount 	? $this->amount 		= $amount 	: null);
			//($trans_id 	? $this->trans_id 		= $trans_id : null);
			( $amount		? $this->setAmount($amount) 		: null);
			( $trans_id		? $this->setTransactionId($trans_id): null);
			( $void 		? $this->_METHOD_VOID 	= $void		: null);
			
			if(!$void)
				( $trans_id	? $this->setTransactionId($trans_id): null);
			
			if(empty($this->amount))
				$this->_validationError('Amount cannot be null');
	
			if(empty($this->trans_id))
				$this->_validationError('Authorize Transaction ID cannot be null');
			
			# If Payment is to VOID
			if($this->_METHOD_VOID){
				
				# For VOID
				//$this->processor->setField('ref_trans_id', $this->trans_id);
				$this->response = $this->processor->void($this->trans_id);
				
				if(isset($this->response->approved) && !$this->response->approved){
					
					/*
					Concord EFS � This transaction is not allowed. The Concord EFS processing 
					platform does not support voiding credit transactions. Please debit the credit card
					instead of voiding the credit.
					 * */
					if($this->response->response_code ==3 && ($this->response->response_reason_code==175)){
					
						# Do return amount as VOID can't perform cause of payment settlement
						$this->response = $this->processor->credit($this->trans_id, $this->amount);
					
					}else{
						$this->_validationError('Unable to VOID transaction, please try again');
					}
					
				}
			}
			# For Refund
			else 
			{
				$this->response = $this->processor->credit($this->trans_id, $this->amount, '4111111111111111');
			}
				
			return $this->_process();
			
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	/*
	 * New Methods
	 * */	
	
	function createProfile(
		$options = array(
					'email'=>'nouman+payment_processors@nextgeni.com',
					'merchantCustomerId'=>null,
					'description' => 'Description of customer'
		)
	)
	{
		
		$log = new Logging();
		$log->log(__METHOD__);
		$this->last_payment_action = 'PaymentProfile';
		
		/*
		if(empty($optoins)){
			$options = array(
						'email'=>null,
						'merchantCustomerId'=>null,
						'description'=>null
			);
		}
		*/

		extract($options);
		$customerProfile = new AuthorizeNetCustomer;
		$customerProfile->description = "";
		$customerProfile->merchantCustomerId =  $merchantCustomerId . '_'. time().rand(1,100);
		$customerProfile->email = $email;
		return $this->_process($this->processor->createCustomerProfile($customerProfile));
		exit;
		//return $this->_process($this->processor->authorizeAndCapture());
		
		
		if($this->test_mode)
			return $this->_process();
			
		/**
		 * FIXME: Remove following instructions after testing
		 * 
		 * Change 
		 * 	($amount ? $this->amount = $amount : null);
		 * TO
		 * 	($amount ? $this->setAmount($amount): null);
		 * 
		 * to handle single place validations
		 * */
		//( $amount		? $this->amount = $amount : null);
		( $amount		? $this->setAmount($amount) : null);
		( $credit_card	? $this->setCreditCard($credit_card) : null);
		( $expiry		? $this->setExpiry($expiry) : null);

		# Validate all fields have valid data
		$this->_validate();
		
		if(!$this->isValidationError()){
			$duplicate_window = get_AUTHORIZENET_DUPLICATE_WINDOW();
			if(!empty($duplicate_window)){
				$this->processor->setField('duplicate_window', $duplicate_window);
			}
			$log->log("duplicate_window:$duplicate_window");
			return $this->_process($this->processor->authorizeAndCapture());
			//$this->assertTrue($response->isAuthorizeNet(CP_API_LOGIN_ID));
			//return $this->_process($this->processor->authorizeAndCapture($this->amount, $this->credit_card, $this->expiry));
		//}else{
			//$this->_validationError('INTERNAL ERROR: Validation issue');
		}
		
		return $this->result;
		
	}
	
	/**
	 * @author 	NoumanArshad
	 * @date	Sep19,2014
	 * 
	 * @desc
	 * 	Charge payment by customer profile id
	 * 	currently support Authorize->CIM
	 * TODO:
	 * 	Add Cybersource and other payment processors
	 * */
	function authorizeAndCaptureByProfile($amount=null, $payment_profile_id){

		//$request = new AuthorizeNetCIM;
		
		if($this->default_gateway == PaymentProcessorGatewaysList::AUTHORIZE_CIM){
			$transaction = new AuthorizeNetTransaction;
		    $transaction->amount = "9.79";
		    $transaction->customerProfileId = $customerProfileId;
		    $transaction->customerPaymentProfileId = $paymentProfileId;
		    $transaction->customerShippingAddressId = $customerAddressId;
		    
			$response = $this->processor->createCustomerProfileTransaction("AuthCapture", $transaction);
		//$response = $request->createCustomerProfileTransaction("AuthCapture", $transaction);
		}elseif($this->default_gateway == PaymentProcessorGatewaysList::CYBERSOURCE){
			/* Cybersource payment */
		}
		
	}
	
	
	
	
}
