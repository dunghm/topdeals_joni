<?php
require_once CONF_LIB. 'cybersource.php';

class CS_Wrapper extends CyberSource{
	
	public $method = 'AUTH_ONLY';
	public $methods = array(
		'AUTH_ONLY' 			=> 'AUTH_ONLY',
		'AUTH_CAPTURE' 			=> 'AUTH_CAPTURE',
		'PRIOR_AUTH_CAPTURE' 	=> 'PRIOR_AUTH_CAPTURE',
//		'AUTH_ONLY' => 'AUTH_ONLY',
	);
	public $connection = null;
	public $request;
	public $response;
	public $responseAuthorize;
	public $amount = null;
	public $statuses = array(
		'void' 			=> 'void'
		,'approved'		=> 'approved'
		,'declined' 	=> 'declined'
		,'refund' 		=> 'refund'
		,'error' 		=> 'error'
	);
	public $reply;
	public $avsStatus	= false;
	public $avsFields	= array('avs'=>null, 'cvn'=>null, 'rc'=>null);
	public $list_currencies = array();
	public $_ErrorCurrencyNotSet 	= 'Currency is blank';
	public $_ErrorCurrencyNotFound 	= 'Currency is not in allowed list';
	
	/**
	 * Call Cybersource constructure to initialize settings
	 * */
	function __construct($CS_MERCHANT_ID, $CS_TRANSACTION_ID, $CS_WSDL){
		
		//parent::__construct( CS_MERCHANT_ID, CS_TRANSACTION_ID, CS_WSDL);
		parent::__construct( $CS_MERCHANT_ID, $CS_TRANSACTION_ID, $CS_WSDL);
				
	}
	
	function isValidCurrency($currency_name){

		GLOBAL $CS_AllowedCurrencies;
		$log = new Logging();
		#$log->INFO("CS: Verifying Currency [{$currency_name}] is in allowed list...");
		
		if(!isset($CS_AllowedCurrencies) || empty($CS_AllowedCurrencies) || empty($currency_name)){
			
			if(!isset($CS_AllowedCurrencies))
				$log->INFO("CS: System error in allowed currencies");
			
			if(empty($CS_AllowedCurrencies))
				$log->INFO("CS: List of allowed currencies not found");
			
			if(empty($currency_name))
				$log->INFO("CS: Currency name not passed");
				
				
			$this->error($this->_ErrorCurrencyNotSet, 'currency');
			return false;
		}
		
		$this->list_currencies = $CS_AllowedCurrencies;
		
		if(in_array($currency_name, $this->list_currencies)){
			#$log->INFO("CS: Currency verified");
			return true;
		}

		$log->INFO("CS: Currency [$currency_name] is not supported");
		$this->error($this->_ErrorCurrencyNotFound, 'currency');
		
	}
	
	function load($amount = null){
		$action = null;
		
/*		switch($this->method){
			
			case "AUTH_ONLY":
				$action = $this->cs->authorize($amount);
				break;

			case "CAPTURE_ONLY":
				$action = $this->cs->capture($amount);
				break;
			
			case "AUTH_CAPTURE":
				$action = $this->cs->authorize($amount);
				break;
			
			case "PROFILE_CREATE":
				$action = $this->cs->authorize($amount);
				break;
			
			case "PROFILE_UPDATE":
				$action = $this->cs->authorize($amount);
				break;
				
			default:
				$action = $this->cs->authorize($amount);
				break;
				
				
		}
*/		
		$action = $this->authorize($amount);
		$this->request 	= $this->cs->request;
		$this->response = $this->cs->response;
		
		return $action;
	}
	
	function setAmount($amount){
		if(empty($amount) || $amount <= 0)
			return 'Error in amount';

		$this->amount = $amount;
	}
	
	/**
	 * @param string $number 			CreditCard number
	 * @param string $expiration_month 	Month of Credit Card expire 
	 * @param string $expiration_year 	Year of Credit Card expire
	 * @param string $cvn_code			
	 * @param string $card_type type of card (Visa, Master, AmericanExpres, etc) 
	 *   
	 * */
	function setCard( $number, $expiration_month, $expiration_year, $cvn_code = null, $card_type = null ){
		//return $this->cs->card($number, $expiration_month, $expiration_year, $cvn_code, $card_type);
		return $this->card($number, $expiration_month, $expiration_year, $cvn_code, $card_type);
	}
	
	/**
	 * @param string $firstName		Billing First Name	(R)
	 * @param string $lastName		Billing	Last Name 	(R)
	 * @param string $street1		Billing	Street		(R)
	 * @param string $city			Billing	City		(R)
	 * @param string $state			Billing	State		(R)
	 * @param string $postalCode	Billing	Postal Code	(R)
	 * @param string $country		Billing	Country		(R)
	 * @param string $email			Email				(R)
	 *   
	 * */
	function setBillTo($firstName, $lastName, $street1, $city, $state, $postalCode, $country, $email){
		$info = compact('firstName', 'lastName', 'street1', 'city', 'state', 'postalCode', 'country', 'email');
		//return $this->cs->bill_to($info);
		return $this->bill_to($info);
	}

	function __authorize($amount = null){
		
		if(!empty($this->amount)){
			$amount = $this->amount;
			$this->amount = null; 
		}
		
		$response = $this->cs->authorize($amount);
//		$this->request 	= $this->cs->request;
//		$this->response = $this->cs->response;
		return $response;
		
	}
	
	function getResponse($response=null){
		
		if(!empty($response))
			$this->modifyResponse($response);
			
		return $this->responseAuthorize;
		
	}

	function modifyResponse($response = null){
		if(null == $response)
			$response = $this->response;
		
		$this->responseAuthorize = $response;
	}
	function modifyResponse_Auth($response = null){
		$this->_modifyResponse($response);
	}
	function modifyResponse_Capture($response = null){
		$this->_modifyResponse($response);
	}
	function modifyResponse_AuthAndCapture($response = null){
		$this->_modifyResponse($response);
	}
	function modifyResponse_UserProfileCharge($response = null){
		$this->_modifyResponse($response);
	}
	function modifyResponse_UserProfileCreate($response = null){
		$this->_modifyResponse($response);
	}
	function modifyResponse_UserProfileUpdate($response = null){
		$this->_modifyResponse($response);
	}
	function modifyResponse_UserProfileAuth($response = null){
		$this->_modifyResponse($response);
	}
	function modifyResponse_UserProfileCapture($response = null){
		$this->_modifyResponse($response);
	}
	function modifyResponse_UserProfileAuthAndCapture($response = null){
		$this->_modifyResponse($response);
	}
	function modifyResponse_UserProfileRefund($response = null){
		$this->_modifyResponse($response);
	}
	function _modifyResponse($response=null){

		if(null == $response)
			$response = $this->response;
		
		$referenceCode 		= '';
		if(isset($response->merchantReferenceCode))
			$referenceCode 		= $response->merchantReferenceCode;
		$token 				= $response->requestToken;
		
		unset($response->merchantReferenceCode);
		unset($response->requestToken);
		
		$this->response = $response;
		
		
		$this->responseAuthorize = compact(
			'referenceCode', 
			'token'
		);
		
		if(isset($response->ccAuthReply)){
			$this->responseAuthorize['avsCode'] 		= $response->ccAuthReply->avsCode;
			$this->responseAuthorize['avsCodeRaw'] 		= $response->ccAuthReply->avsCodeRaw;
			if(isset($response->ccAuthReply->cvCode))
				$this->responseAuthorize['cvCode'] 			= $response->ccAuthReply->cvCode;
			if(isset($response->ccAuthReply->cvCodeRaw))
				$this->responseAuthorize['cvCodeRaw'] 		= $response->ccAuthReply->cvCodeRaw;
			$this->responseAuthorize['amount_auth'] 	= $response->ccAuthReply->amount;
			$this->responseAuthorize['auth_code'] 		= $response->ccAuthReply->authorizationCode;
			$this->responseAuthorize['reconciliationID']= $response->ccAuthReply->reconciliationID;
		}

		if(isset($response->ccCaptureReply)){
			$this->responseAuthorize['amount_capture'] 	= $response->ccCaptureReply->amount;
			$this->responseAuthorize['transactionId'] 	= $response->ccCaptureReply->reconciliationID;
			$this->responseAuthorize['reconciliationID'] 	= $response->ccCaptureReply->reconciliationID;
		}
		 
		if(isset($response->paySubscriptionCreateReply->subscriptionID)){
			$this->responseAuthorize['subscriptionID'] 	= $response->paySubscriptionCreateReply->subscriptionID;
		}
		
		if(isset($response->paySubscriptionUpdateReply->subscriptionID)){
			$this->responseAuthorize['subscriptionID'] 	= $response->paySubscriptionUpdateReply->subscriptionID;
		}
		
		if(isset($this->responseAuthorize->avsCode))
			$this->avsFields['avs'] = $this->responseAuthorize->avsCode;
		
		if(isset($this->responseAuthorize->cvCode))
			$this->avsFields['cvn'] = $this->responseAuthorize->cvCode;
		
	}
	
	/**
	 * ##################################################################################
	 * 
	 * 									Public Functions
	 * 
	 * ##################################################################################
	 * */
	
	/**
	 * @param string $amount Amount to authorize
	 * @desc Authorize amount to capture later
	 * 
	 * return response ojbect return from CyberSource
	 * */
	function Authorize($amount=null, $merge=true){
		
		$this->method = $this->methods['AUTH_ONLY'];
		
		($this->request);
		$response = parent::authorize($amount);
		
		$this->modifyResponse_Auth($response);
		
		if($merge)
			return array_merge((array) $response, (array) $this->getResponse());
		else
			return $response;
			
	}
	
	/**
	 * @param $requestToken Reference token of transactions against which amount to be capture
	 * @param $amount Amount to be capture
	 *  
	 * return response ojbect return from CyberSource
	 * */
	function Capture($requestToken, $amount, $merge=true){
		
		$this->method = $this->methods['PRIOR_AUTH_CAPTURE'];
		try {
			$response = parent::capture( $requestToken, $amount );
		}
		catch ( CyberSource_Declined_Exception $e ) {
			echo 'Transaction declined';
		}
		$this->modifyResponse_Capture($response);
		
		if($merge)
			return array_merge((array) $response, (array) $this->getResponse());
		else
			return $response;
		
	}
	
	/**
	 * Not tested 
	 * Auth and Capture amount
	 * 
	 * @param string $amount Amount to Authorize and Capture
	 * @deprecated
	 * */
	function AuthAndCatpture($amount=null){
		
		$this->method = $this->methods['AUTH_CAPTURE'];
		
		$auth_response = $this->Authorize($amount, false);
		
		if ( !isset( $auth_response->requestToken ) ) {
			die('Authorization seems to have failed!');
		}
		
		# if amount is not set, get amount from response ccAuthReply
		if(null == $amount )
			$amount = $auth_response->ccAuthReply->amount;

		return $this->Capture($auth_response->requestToken, $amount);
		
	}
	
	/**
	 * Create a new payment subscription, either by performing a $0 authorization check on the credit card or using a 
	 * pre-created request token from an authorization request that's already been performed.
	 * 
	 * @param string $payment_profile_id is merchantReferenceCode it should be unique cross reference numbe of system
	 */
	function UserProfileCreate($referenceCode, $currency){
		
		# $payment_profile_id is merchantReferenceCode
		if(empty($referenceCode))
			$this->error('Missing merchantReferenceCode field.');
		
		if(empty($currency))
			$this->error('Currency is not defined.');
			
		if(false === $this->isValidCurrency($currency))			
			$this->error($this->_ErrorCurrencyNotFound);

		parent::reference_code($referenceCode);
		$response = parent::create_profile($currency);
//		$response = parent::create_subscription();
		
		$this->{"modifyResponse_".__FUNCTION__}($response);
		$response = array_merge((array) $response, (array) $this->getResponse());
		$this->logResponse(__FUNCTION__, $response);
		
		return $response;
		
	}
		
	// Written By Zia for
	function createProfile($infoArray, $currency)
	{
		$retval=array();
		$this->setCard($infoArray["cardnumber"], date("m",strtotime($infoArray["expirationdate"])), date("Y",strtotime($infoArray["expirationdate"])), $infoArray["cardcode"], $infoArray["ccard"]);
		$this->setBillTo( 
			$infoArray['fname'], 
			$infoArray['lname'], 
			$infoArray['address'], 
			$infoArray['city'], 
			$infoArray['state'], 
			$infoArray['zip'], 
			$infoArray['country'],
			$infoArray['email']
		);
		$payment_profile_id=$infoArray['contactlist_id']."-".substr($infoArray['cardnumber'],strlen($infoArray["cardnumber"])-4,4);
		try{
			$subscriptions=$this->UserProfileCreate($payment_profile_id, $currency);
		}
		catch(Exception $e){
			echo $e->getCode() . ': ' . $e->getMessage() . '<br />';
		}
		//print_r($subscriptions);
		if($subscriptions["auth_status"]=="1")
		{
			$retval["sub_id"]=$this->response->paySubscriptionCreateReply->subscriptionID;
			$retval["ref_code"]=$payment_profile_id;
			return $retval;
		}
		else
		{
			return $subscriptions;
		}
	}
	
	
	/**
	 * Updatesubscription
	 * 
	 * @param string $subscription_id Subscription ID
	 * @param string $payment_profile_id Merchant Reference ID
	 */
	function UserProfileUpdate($subscription_id, $referenceCode){
		
		if(empty($subscription_id))
			$this->error ('Subscription ID is required.');
			
		if(empty($referenceCode))
			$this->error ('MerchantReferenceCode is required.');
			
		parent::reference_code($referenceCode);
		$response = parent::update_subscription($subscription_id);
		
		$this->{"modifyResponse_".__FUNCTION__}($response);
		$response = array_merge((array) $response, (array) $this->getResponse());
		$this->logResponse(__FUNCTION__, $response);
				
		return $response;	
		
	}
	
	/**
	 * Charge the given Subscription ID a certain amount.
	 * 
	 * @param string $subscription_id The CyberSource Subscription ID to charge.
	 * @param float $amount The dollar amount to charge.
	 * @return stdClass The raw response object from the SOAP endpoint
	 */
	function UserProfileCharge($subscription_id, $payment_profile_id, $amount){
		
		parent::reference_code($payment_profile_id);
		
		$response = parent::charge_subscription($subscription_id, $amount);
		$this->modifyResponse_UserProfileCharge($response);
		
		return array_merge((array) $response, (array) $this->getResponse());
		
	}

	function UserProfileAuth($subscription_id=null, $referenceCode=null, $amount=null, $currency=null){
		
		if(empty($subscription_id))
			$this->error ('Subscription ID is required.');
			
		if(empty($referenceCode))
			$this->error ('MerchantReferenceCode is required.');
			
		if(empty($amount))
			$this->error ('Amount is required.');
		
		if(empty($currency))
			$this->error('Currency is not defined.');

		if(false === $this->isValidCurrency($currency))			
			$this->error($this->_ErrorCurrencyNotFound);
			
		if($this->isError()){
			return $this->reply;
		}
		parent::reference_code($referenceCode);
		$response = parent::auth_subscription($subscription_id, $amount, $currency);

		$this->{"modifyResponse_".__FUNCTION__}($response);
		$response = array_merge((array) $response, (array) $this->getResponse());
		$this->logResponse(__FUNCTION__, $response);
				
		return $response;
			
	}
	
	function UserProfileCapture($subscription_id, $referenceCode, $auth_token, $auth_request_id, $amount, $currency='USD'){
		
		if(empty($subscription_id))
			$this->error ('Subscription ID is required.');
			
		if(empty($referenceCode))
			$this->error ('MerchantReferenceCode is required.');
			
		if(empty($auth_token))
			$this->error ('Auth Token is required.');
			
		if(empty($auth_request_id))
			$this->error ('Auth Request ID is required.');
			
		if(empty($amount))
			$this->error ('Amount is required.');

		# Removed on Sep 20 by NOuman
		# Not required on Funds capture
		/*
		if(empty($currency))
			$this->error('Currency is not defined.');
			
		if(false === $this->isValidCurrency($currency))
			$this->error($this->_ErrorCurrencyNotFound);
		*/
		if($this->isError()){
			return $this->reply;
		}
			
		parent::reference_code($referenceCode);
		$response = parent::capture_subscription($subscription_id, $referenceCode, $auth_token, $auth_request_id, $amount);
		
		$this->{"modifyResponse_".__FUNCTION__}($response);
		$response = array_merge((array) $response, (array) $this->getResponse());
		$this->logResponse(__FUNCTION__, $response);
				
		return $response;	
		
	}
	
	function UserProfileAuthAndCapture($subscription_id, $referenceCode, $amount, $currency='USD'){
		
		if(empty($subscription_id))
			$this->error ('Subscription ID is required.');
			
		if(empty($referenceCode))
			$this->error ('MerchantReferenceCode is required.');
			
		if(empty($amount))
			$this->error ('Amount is required.');
		
		# Removed on Sep 20 by NOuman
		# Not required on Funds capture
		/*
		if(empty($currency))
			$this->error('Currency is not defined.');
		
		if(false === $this->isValidCurrency($currency))			
			$this->error($this->_ErrorCurrencyNotFound);
		*/

		if($this->isError()){
			return $this->reply;
		}
		
		parent::reference_code($referenceCode);
		$response = parent::authandcapture_subscription($subscription_id, $amount);
		
		$this->{"modifyResponse_".__FUNCTION__}($response);
		$response = array_merge((array) $response, (array) $this->getResponse());
		$this->logResponse(__FUNCTION__, $response);
		
		return $response;	
		
	}

	function UserProfileRefund($subscription_id, $referenceCode, $captureRequestID, $captureRequestToken, $reconciliationID, $amount, $currency='USD'){
		
		if(empty($subscription_id))
			$this->error ('Subscription ID is required.');
			
		if(empty($referenceCode))
			$this->error ('MerchantReferenceCode is required.');
			
		if(empty($captureRequestID))
			$this->error ('Capture Request ID is required.');
			
		if(empty($captureRequestToken))
			$this->error ('CaptureRequestToken Request ID is required.');
			
			
		if(empty($reconciliationID))
			$this->error ('ReconciliationID Request ID is required.');
			
			
		if(empty($amount))
			$this->error ('Amount is required.');
			
		# Removed on Sep 20 by NOuman
		# Not required on Funds capture
		/*
		if(empty($currency))
			$this->error('Currency is not defined.');
			
		if(false === $this->isValidCurrency($currency))			
			$this->error($this->_ErrorCurrencyNotFound);
		*/
			
		if($this->isError()){
			return $this->reply;
		}
		
		parent::reference_code($referenceCode);
		$response = parent::refund_subscription($subscription_id, $captureRequestID, $captureRequestToken, $reconciliationID, $amount);
		$this->{"modifyResponse_".__FUNCTION__}($response);
		$response = array_merge((array) $response, (array) $this->getResponse());
		$this->logResponse(__FUNCTION__, $response);
				
		return $response;	
		
	}
	
	function error($msg = 'One of the field has error', $type='system'){
		
		$this->reply['type']		= $type;#'system';
		$this->reply['error'] 		= 'true';
		$this->reply['reasonCode']	= '033';
		$this->reply['decision'] 	= 'REJECT';
		$this->reply['auth_message']= $msg;
		$this->reply['message'] 	= $msg;
		$this->reply['status'] 		= 0;
		
	}
	
	function isError(){

		if($this->reply['error'] == 'true')
			return true;
			
		return false;
		
	}	
	
	function isOk(){
		return parent::isOk();
	}

	function logResponse($fn=null, $response){
		
//		('LINE:'. __LINE__);
		$log = array();
		extract($response);

		if(isset($purchaseTotals))
			$purchaseTotals = (array) ($purchaseTotals);
			
		if(isset($ccAuthReply))
			$ccAuthReply	= (array) ($ccAuthReply);
			
		if(isset($ccCaptureReply))
			$ccCaptureReply = (array) ($ccCaptureReply);

		if(isset($paySubscriptionCreateReply))
			$paySubscriptionCreateReply = (array) ($paySubscriptionCreateReply);
		
		if(isset($paySubscriptionUpdateReply))
			$paySubscriptionUpdateReply = (array) ($paySubscriptionUpdateReply);
		
			
		$log = compact(
			'merchantReferenceCode', 
			
			'transactionId', 
			'avsCode',
			'avsCodeRaw',
		
			'requestID',
			'requestToken',
			'decision',
			'reasonCode',
		
			'amount',
			'status',
			'resultCode',
		
			'purchaseTotal',
			'ccAuthReply',
			'ccCaptureReply',
			'paySubscriptionCreateReply',
			'paySubscriptionUpdateReply',
		
			'function_name',
			'created'
		
		); 

		if(isset($ccAuthReply))
			$log['ccAuthReply'] 				= serialize( (array) ($ccAuthReply));
			
		if(isset($ccCaptureReply))
			$log['ccCaptureReply'] 				= serialize( (array) ($ccCaptureReply));
			
		if(isset($paySubscriptionCreateReply))
			$log['paySubscriptionCreateReply'] 	= serialize( (array) ($paySubscriptionCreateReply));
			
		if(isset($paySubscriptionUpdateReply))
			$log['paySubscriptionUpdateReply'] 	= serialize( (array) ($paySubscriptionUpdateReply));
		
		$log['function_name'] = $fn;
		$log['created']	= date('Y-m-d H:i:s');
		
		
		//$cslog=new CS_Log();
		//$cslog->assignFromArray($log);
		//$cslog->create();
		
		//$log = new Logging();
		//$log->INFO("Request: ".serialize($this->cs->request));
		//$log->INFO("Response: ".serialize($this->response));		
		#FIXME: Save $response in database
		
	}
	
	function setAvsCountry($country){
		if($county!='us')
			$this->avs_country = 'intl_decision';
		else
			$this->avs_country = 'us_decision';
	}
}
