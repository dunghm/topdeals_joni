<?php
require_once 'class.logging.php';
require_once 'skeletons/iPaymentProcessorProfile.php';
require_once 'PaymentProcessor.php';
require_once 'functions/functions_payment_processors.php';
/**
 * @package 	iPaymentProcessor
 * @subpackage 	PaymentProcessor
 * @author 	Nouman
 * @since	March20,2014
 * @version 0.1
 * @desc
 * 	Payment Processor Interface
 * 
 * Default Payment Process is to be choosen based on the Platform we are going to use Payment Processor
 * 
 * In our case we use Authorize.net CardPresent
 * 
 * */
CLASS PaymentProcessorProfile EXTENDS PaymentProcessor implements iPaymentProcessorProfile {
	
	/*
	 * New Methods
	 * */	
	protected $profile_id = false;
        protected $customerProfile = false;
	protected $payment_profile_id = false;
        protected $amount = false;
	protected $CCNumber = false;
        protected $cc_expiry = false;
        
	function setProfileId($profile_id = false){
		$this->profile_id = $profile_id;
	}
	function getProfileId(){
		return $this->profile_id;
	}
	function setPaymentProfileId($payment_profile_id = false){
		$this->payment_profile_id = $payment_profile_id;
	}
	function getPaymentProfileId(){
		return $this->payment_profile_id;
	}
        function getAmount(){
		return $this->amount;
	}
	function setAmount($amount = false){
		$this->amount = $amount;
	}
        function getCCNumber(){
		return $this->CCNumber;
	}
	function setCCNumber($CCNumber = false){
		$this->CCNumber = $CCNumber;
	}
        function getExpiry(){
		return $this->cc_expiry;
	}
	function setExpiry($cc_expiry = false){
		$this->cc_expiry = $cc_expiry;
	}

        function setEmail($email, $required=true){
            
            $this->email = false;
            
                $this->email = $email;
                
            if($required)
                $this->_validationError('Error in CreditCard');
            
        }
        
	function createCustomerProfile($email = FALSE, $merchantCustomerId = FALSE)
	{
		
		$log = new Logging();
		$log->log(__METHOD__);
		$this->last_payment_action = 'CIM_CustomerProfile';
		
		if($this->test_mode)
			return $this->_process();
                
		( $email                ? $email : null);
		( $merchantCustomerId   ? $merchantCustomerId : null);
                
		$customerProfile = new AuthorizeNetCustomer;
		$customerProfile->description = "";
		$customerProfile->merchantCustomerId =  $merchantCustomerId . '_'. time().rand(1,100);
		$customerProfile->email = $email;
                
		$response = $this->_process($this->connection()->createCustomerProfile($customerProfile));
                $customerProfileId = $this->response->getCustomerProfileId();
		$this->setProfileId($customerProfileId);
		return $response;
		
	}
	
	/**
	 * @author 	NoumanArshad
	 * @date	Sep22,2014
	 * 
	 * @desc
	 * 	Customer payment profile
	 * */
	function createPaymentProfile($credit_card = false,$expiry = false,$customerProfileId = false){
		
		$log = new Logging();
		$log->log(__METHOD__);
		$this->last_payment_action = 'CIM_CustomerPaymentProfile';
		
		if($this->test_mode)
			return $this->_process();
		
                ( $credit_card		? $this->setCCNumber($credit_card) : $this->getCCNumber());
		( $expiry		? $this->setExpiry($expiry) : $this->getExpiry());
		( $customerProfileId    ? $this->setProfileId($customerProfileId) : $this->getProfileId());

		if($this->getCCNumber() == ''){
			$this->_validationError('Error in CreditCard');
		}
		
		if($this->getExpiry() == ''){
			$this->_validationError('Error in Expiry');
		}
		
		if($this->getProfileId() == ''){
			$this->_validationError('Error in Customer Profile Id');
		}
		
		if(!$this->isValidationError()){
			
			// Add payment profile.
		    $paymentProfile = new AuthorizeNetPaymentProfile;
		    $paymentProfile->customerType = "individual";
		    $paymentProfile->payment->creditCard->cardNumber 		= $this->getCCNumber();
		    $paymentProfile->payment->creditCard->expirationDate 	= $this->getExpiry();
                    
                    $response = $this->_process($this->connection()->createCustomerPaymentProfile($this->getProfileId(), $paymentProfile));
                    
		    $paymentProfileId = $this->response->getPaymentProfileId();
                    $this->setPaymentProfileId($paymentProfileId);
                    return $response;
		}
		
		return $this->result;
			
	}
	
	function authorizeAndCapture($amount=null, $customerProfileId=false, $customerPaymentProfileId=false)
                {
		
		$log = new Logging();
		$log->log(__METHOD__);
		$this->last_payment_action = 'CIM_authorizeAndCapture';
		
		if($this->test_mode)
			return $this->_process();

		( $customerProfileId            ? $this->setProfileId($customerProfileId) : $this->getProfileId());
		( $customerPaymentProfileId	? $this->setPaymentProfileId($customerPaymentProfileId) : $this->getPaymentProfileId());
                ( $amount                       ? $this->setAmount($amount) : $this->getAmount());
                
		
		if($this->getAmount() == ''){
			$this->_validationError('Error in Amount');
		}
	
		if($this->getProfileId() == ''){
			$this->_validationError('Error in Customer Profile Id');
		}
		
		if($this->getPaymentProfileId() == ''){
			$this->_validationError('Error in Customer Payment Profile Id');
		}
		
		if(!$this->isValidationError()){
			
		    $transaction = new AuthorizeNetTransaction;
		    $transaction->amount = $this->getAmount();
		    $transaction->customerProfileId = $this->getProfileId();
		    $transaction->customerPaymentProfileId = $this->getPaymentProfileId();
		    //$transaction->customerShippingAddressId = $customerAddressId;
                    
                    /*$lineItem              = new AuthorizeNetLineItem;
		    $lineItem->itemId      = "4";
		    $lineItem->name        = "Cookies";
		    $lineItem->description = "Chocolate Chip";
		    $lineItem->quantity    = "4";
		    $lineItem->unitPrice   = "1.00";
		    $lineItem->taxable     = "true";
		    $transaction->lineItems[] = $lineItem;*/
                    
		    $transaction->lineItems[] = $this->getItem();
		    //var_debug($transaction->lineItems);
		    if(empty($transaction->lineItems)){

                        $this->_validationError('Error in Line Items');
		    }
		    
                    $response = $this->_process($this->connection()->createCustomerProfileTransaction("AuthCapture", $transaction, 'x_duplicate_window=1'));
                        
		    $transactionResponse = $this->response->getTransactionResponse();
                    //var_debug($transactionResponse.'asf');
		    $transactionId = $transactionResponse->transaction_id;
			$this->setTransactionId($transactionId);
			return $response;
		    
			
		}
		
		return $this->result;

	}
        
        function authorize($amount=null, $customerProfileId=false, $customerPaymentProfileId=false){
		
		$log = new Logging();
		$log->log(__METHOD__);
		$this->last_payment_action = 'CIM_authorizeAndCapture';
		
		if($this->test_mode)
			return $this->_process();

		( $customerProfileId            ? $this->setProfileId($customerProfileId) : $this->getProfileId());
		( $customerPaymentProfileId	? $this->setPaymentProfileId($customerPaymentProfileId) : $this->getPaymentProfileId());
                ( $amount                       ? $this->setAmount($amount) : $this->getAmount());
                
		
		if($this->getAmount() == ''){
			$this->_validationError('Error in Amount');
		}
	
		if($this->getProfileId() == ''){
			$this->_validationError('Error in Customer Profile Id');
		}
		
		if($this->getPaymentProfileId() == ''){
			$this->_validationError('Error in Customer Payment Profile Id');
		}
		
		if(!$this->isValidationError()){
			
		    $transaction = new AuthorizeNetTransaction;
		    $transaction->amount = $this->getAmount();
		    $transaction->customerProfileId = $this->getProfileId();
		    $transaction->customerPaymentProfileId = $this->getPaymentProfileId();
		    //$transaction->customerShippingAddressId = $customerAddressId;
                    
                    /*$lineItem              = new AuthorizeNetLineItem;
		    $lineItem->itemId      = "4";
		    $lineItem->name        = "Cookies";
		    $lineItem->description = "Chocolate Chip";
		    $lineItem->quantity    = "4";
		    $lineItem->unitPrice   = "1.00";
		    $lineItem->taxable     = "true";
		    $transaction->lineItems[] = $lineItem;*/
                    
		    $transaction->lineItems[] = $this->getItem();
		    //var_debug($transaction->lineItems);
		    if(empty($transaction->lineItems)){

                        $this->_validationError('Error in Line Items');
		    }
		    
                    $response = $this->_process($this->connection()->createCustomerProfileTransaction("AuthOnly", $transaction, 'x_duplicate_window=1'));
                        
		    $transactionResponse = $this->response->getTransactionResponse();
                    //var_debug($transactionResponse.'asf');
		    $transactionId = $transactionResponse->transaction_id;
			$this->setTransactionId($transactionId);
			return $response;
		    
			
		}
		
		return $this->result;

	}
	
        function capture($auth_code = false, $amount = false){
		
		$log = new Logging();
		$log->log(__METHOD__);
		$this->last_payment_action = 'CIM_authorizeAndCapture';
		
		if($this->test_mode)
			return $this->_process();

		( $customerProfileId ? $this->setProfileId($customerProfileId) : $this->getProfileId());
		( $customerPaymentProfileId	? $this->setPaymentProfileId($customerPaymentProfileId) : $this->getPaymentProfileId());
		
		$v = $this->getAmount();
		if(empty($v)){
			$this->_validationError('Error in Amount');
		}
		
		if(empty($this->profile_id)){
			$this->_validationError('Error in Customer Profile Id');
		}
		
		if(empty($this->payment_profile_id )){
			$this->_validationError('Error in Customer Payment Profile Id');
		}
		
		if(!$this->isValidationError()){
			
			
		    $transaction = new AuthorizeNetTransaction;
		    $transaction->amount = $this->getAmount();
		    $transaction->customerProfileId = $this->getProfileId();
		    $transaction->customerPaymentProfileId = $this->getPaymentProfileId();
		    //$transaction->customerShippingAddressId = $customerAddressId;
                    
		    $transaction->lineItems[] = $this->getItem();;
		    
		    if(!empty($this->lineItems)){
		    	return 1;
		    }
		    
			$response = $this->_process($this->connection()->createCustomerProfileTransaction("AuthOnly", $transaction, 'x_duplicate_window=1'));
		    $transactionResponse = $this->response->getTransactionResponse();
		    $transactionId = $transactionResponse->transaction_id;
			$this->setTransactionId($transactionId);
			return $response;
		    
			
		}
		
		return $this->result;

	}
	
	
	function void($transactionId=null){
            
            $transaction = new AuthorizeNetTransaction;
            $transaction->transId = $transactionId;
            $response = $request->createCustomerProfileTransaction("Void", $transaction);
            $this->assertTrue($response->isOk());
            $transactionResponse = $response->getTransactionResponse();
            $this->assertTrue($transactionResponse->approved);
            
        }
	function refund($trans_id = false, $amount = false){}
	
	function addItem($itemId,$name,$description,$quantity,$unitPrice,$taxable){
	    $lineItem              = new AuthorizeNetLineItem;
	    $lineItem->itemId      = $itemId;
	    $lineItem->name        = $name;
	    $lineItem->description = $description;
	    $lineItem->quantity    = $quantity;
	    $lineItem->unitPrice   = $unitPrice;
	    $lineItem->taxable     = $taxable;
		
	    $this->lineItems = $lineItem;
	    
	}
        
        function getItem()
        {
            return $this->lineItems;
        }
        
        function addAddress($firstName,$lastName,$company,$address,$city,$state,$zip,$country,$phoneNumber,$faxNumber){
            // Add shipping address.
            $address = new AuthorizeNetAddress;
            $address->firstName = "john";
            $address->lastName = "Doe";
            $address->company = "John Doe Company";
            $address->address = "1 Main Street";
            $address->city = "Boston";
            $address->state = "MA";
            $address->zip = "02412";
            $address->country = "USA";
            $address->phoneNumber = "555-555-5555";
            $address->faxNumber = "555-555-5556";
            $this->customerProfile[] = $address;
	}
        
        function getAddress()
        {
            return $this->customerProfile;
        }
	
	
}
