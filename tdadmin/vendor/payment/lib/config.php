<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

if (!function_exists('curl_init')) {
	$console_msg = 'AuthorizeNetSDK needs the CURL PHP extension.';
	if(defined('IS_CRON'))
		echo($console_msg);
	else
		throw new Exception($console_msg);
}

if (!function_exists('simplexml_load_file')) {
	$console_msg = 'The AuthorizeNet SDK requires the SimpleXML PHP extension.';
	if(defined('IS_CRON'))
		die($console_msg);
	else
		throw new Exception($console_msg);
}


# Common CI config settings 
GLOBAL $my_config, $database_config, $global_mysql_db;

$is_admin 		= false;
$admin_suffix	= '';
$http = 'http';
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')
	$http .= 's';
$http .= "://";

if($_SERVER['PHP_SELF'] === '/admin/index.php') {$is_admin = true;$admin_suffix = 'admin/';}

define('CONF_HTTP', 		$http);
define('CONF_IS_ADMIN', 	$is_admin);
define('CONF_ADMIN_SUFFIX', $admin_suffix);


$this_path = __FILE__;
$this_path = dirname($this_path). '/';
define('ROOT', dirname($this_path). "/");
define('PATH_APPLICATION', ROOT); # Usefull when application path is different than ROOT
define('CONF_LIB', PATH_APPLICATION. "lib/");
define('CONF_LOG', PATH_APPLICATION. "logs");
define('CONF_FUNCTIONS', CONF_LIB. "functions/");
define('CONF_LOCATIONS', CONF_LIB. "locations/");
//if(file_exists(CONF_LIB. "database.php"))
	//require_once CONF_LIB. "database.php";

if(file_exists(CONF_LIB. "config.payment.php"))
	require_once CONF_LIB. "config.payment.php";
	
if(file_exists(CONF_FUNCTIONS. "init.php"))
	require_once CONF_FUNCTIONS. "init.php";
/*
if(!isset($global_mysql_db)){
	$console_msg = 'Database connection not found';
	if(defined('IS_CRON'))
		die($console_msg);
	else
		throw new Exception($console_msg);
}*/

