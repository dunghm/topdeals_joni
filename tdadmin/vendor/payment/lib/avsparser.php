<?php
class AvsParser
{
	
    function avsParseResponse($country,$avsCode,$cvsCode,$rcCode) {
    	
    	$responseData  = array();
    	$typeArray	   = array();
    	
   		 /**
		 * Get Response Type & Store it in array
		 * */
		
		$resposneTypeQuery = DB::select( DB::raw('SELECT * FROM avs_response_type'));
		
    	foreach($resposneTypeQuery as $resposneTypedata){
    		
    		$typeArray[$resposneTypedata->type_id] = $resposneTypedata->type_name;
    	}
    	
    	if(!empty($typeArray)){
    		
    		//Get Reponse For CVS
	    	$typeId = array_search('cvs', $typeArray);
	    	$responseData['cvs'] = $this->avsResponseByType($country,$typeId,$cvsCode);
    		
	    	//Get Reponse For AVS
	    	$typeId = array_search('avs', $typeArray);
	    	$responseData['avs'] = $this->avsResponseByType($country,$typeId,$avsCode);
	    	
	    	//Get Reponse For RC
	    	$typeId = array_search('rc', $typeArray);
	    	$responseData['rc'] = $this->avsResponseByType($country,$typeId,$rcCode);
	    	
	    	//Error Array 
	    	$reponseResultErr  = 'General Failure Occur!';
	    							
	    	//Reponse Array To Store response result
	    	$responseResultArr = array();
	    	
	    	if(!empty($responseData)){
	    		
		    	foreach($responseData as $key=>$responseDataRow){
		    		//response data of any type is emtpy
		    		if(empty($responseDataRow)){
		    			return $reponseResultErr;
		    		}else{
		    			//if decision & error message of any type is emtpy
		    			if($responseDataRow[0]=='' || $responseDataRow[1]==''){
			    			return $reponseResultErr;
		    			}else{
		    				
//		    				$responseResultArr["$key"] = $responseDataRow;
		    				$responseResultArr['response_status'][] = $responseDataRow[0];
		    				$responseResultArr['response_msg'][] 	= $responseDataRow[1];
		    			}
		    			
		    		}
		    	}
		    	
		    	//Response Data not empty
		    	if(!empty($responseResultArr)){
		    		
		    		//if Rejected Status 0 is found in status array
		    		if(in_array(0,$responseResultArr['response_status'])){
		    			
		    			foreach($responseResultArr['response_status'] as $statsuKey => $responseStatusRow){
		    				//Reponse Status Message Overwrite by lastone by checking status value
		    				if($responseStatusRow==0){
		    					 $responseStatusMessage = $responseResultArr['response_msg'][$statsuKey];
		    				}
		    			}
		    			return $responseStatusMessage;
		    			
		    		}else{
		    			//if all three codes are accepted
		    			return true;	
		    		}
		    		
		    	}else{
		    		return $reponseResultErr;
		    	}
		    	
	    	}else{
	    		return $reponseResultErr;
	    	}
	    	
	    	
    	}
    	
//    	$this->debug($responseData);
    }
    
    /**
     * Get AVS Response w.r.t type
     * */
    
    function avsResponseByType($country, $type, $code){
	
    	$usCountry 		 = array('US','USA','United State');
    	$responseMessage = array();
    	
    	if($country!=null ){
    		
    		$selectedDecision = 'intl_decision';
    		//if country is US then we select us_decision
    		if(in_array($country,$usCountry)){
    			$selectedDecision = 'us_decision';
    		}
    		
	   		$resposneQuery = mysql_fetch_array(mysql_query("SELECT $selectedDecision AS decision,IFNULL(error_msg,'No Response') AS errorMsg FROM avs_parser where type='$type' AND code='$code'"));
	   		
	   		$responseMessage[] = $resposneQuery["decision"];
	   		$responseMessage[] = $resposneQuery["errorMsg"];
    	}
    	
    	return $responseMessage;
    }
    
}

?>
