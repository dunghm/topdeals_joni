<?php
class UnitTestClassPayment{

	function _test(){
		
		$request = "x_cpversion=1.0&x_delim_char=%2C&x_encap_char=%7C&x_market_type=2&x_response_format=1&x_test_request=TRUE&x_device_type=4&x_amount=1000.0&x_track2=4111111111111111%3D1803101000020000831&x_duplicate_window=1&x_type=AUTH_CAPTURE&x_login=8wS2uGn3q&x_tran_key=6B2cyb3P3vf35Xvr";
		
		$requestArrayIndex = explode("&", $request );
		foreach($requestArrayIndex as $index => $value){
			$items = explode("=", $value);
			$requestArray[$items[0]] = $items[1];
		}
		debug($requestArray);
		//exit;
		
		foreach($requestArray as $index => $arrItem){
			if(in_array($index, array('x_login', 'x_tran_key', 'x_track1', 'x_track2')))
				$requestArray[$index] = _maskString($requestArray[$index], '_');
			//unset($arrItem['x_login']);
			//unset($arrItem['x_tran_key']);
			//unset($arrItem['x_track1']);
			//unset($arrItem['x_track2']);
		}
		debug(strNVPToJson($request));
		//debug($requestArray );
		exit;
		
	}
	
	
	public function profile()
	{
		$formSubmit = TRUE;
		$manual		= FALSE;
			
		echo 'Please be patient, Payment Processing ...';
		
		$amount 	= '10000';
		//$cc_number 	= '4111111111111111';
		$credit_card 	= '4012888888881881';
		$expiry 	= '04/15';
		$expiry 	= '0318';
		$expiry 	= '2015-10';
		
		/*
		 * Customer Profile IDs
		 * */
		$customerProfileId = '29158284';
		$customerProfileId = '29158289';
		$customerProfileId = '29158295';
		$customerProfileId = '29158423';
		$customerProfileId = '29158424';
		
		
		$customerPaymentProfileId = '26474035';
				
		//$paymentProcessor = new PaymentProcessor();
		//$paymentProcessor = new PaymentProcessor(get_CNP_API_LOGIN_ID(), get_CNP_TRANSACTION_KEY(), PaymentProcessorGatewaysList::AUTHORIZE_CIM);
		//$response = $paymentProcessor->createCustomerProfile();
		$paymentProcessor = new PaymentProcessorProfile(get_CNP_API_LOGIN_ID(), get_CNP_TRANSACTION_KEY(), PaymentProcessorGatewaysList::AUTHORIZE_CIM);
		
		# Enable to create Customer Profile
		//$response = $paymentProcessor->createCustomerProfile();
		$paymentProcessor->setProfileId($customerProfileId);
		$customerProfileId = $paymentProcessor->getProfileId();
		//debug('$customerProfileId=>'.$customerProfileId);
		
		# Enable to create payment profile
		//$paymentProcessor->createPaymentProfile($credit_card, $expiry, $customerProfileId);
		$paymentProcessor->setPaymentProfileId($customerPaymentProfileId);
		$customerPaymentProfileId = $paymentProcessor->getPaymentProfileId();
		//debug('$customerPaymentProfileId=>'.$customerPaymentProfileId);
		$trans_id = $paymentProcessor->authorizeAndCapture($amount);
		//debug($paymentProcessor);exit;
		//debug('$trans_id=>');
		//debug($trans_id);
		
		debug($paymentProcessor->result);
		debug('Payment Processor Response');
		debug($paymentProcessor->response);
		
		exit;
			
	}
	
	
	public function index()
	{
		//$this->_test();
		$formSubmit = TRUE;
		$manual		= FALSE;
			
		echo 'Please be patient, Payment Processing ...';
		
		$amount 	= '1000.0';
		$cc_number 	= '4012888888881881';
		$expiry 	= '0318';
		
		//$track_string = '%B4111111111111111^CARDUSER/JOHN^1803101000000000020000831000000?;4111111111111111=1803101000020000831?';
		//$track_string = '%B41111111111111^CARDUSER/JOHN^1803101000000000020000831000000?;4111111111111111=1803101000020000831?';
		//$track_string = '%B4012888888881881^CARDUSER/JOHN^1803101000000000020000831000000?;4111111111111111=1803101000020000831?';
		//$track_string = 'B401288888888188^CARDUSER/JOHN^1803101000000000020000831000000?;4111111111111111=1803101000020000831';
		//$track_string = '%B4012888888881881^CARDUSER/JOHN^1803101000000000020000831000000?';
		$track_string = ';4111111111111111=1803101000020000831?';
		
		//$paymentProcessor = new PaymentProcessor();
		$paymentProcessor = new PaymentProcessor(get_CP_API_LOGIN_ID(), get_CP_TRANSACTION_KEY());
		$paymentProcessor->setAmount($amount);
		$paymentProcessor->setTrack($track_string);
		$response = $paymentProcessor->authorizeAndCapture();
		debug($paymentProcessor->result);
		debug('Payment Processor Response');
		debug($paymentProcessor->response);
		exit;
			
			
	}
	
	public function manual()
	{
		$formSubmit = TRUE;
		$manual		= FALSE;
			
		echo 'Please be patient, Payment Processing ...';
		
		$amount 	= '1000.0';
		//$cc_number 	= '4111111111111111';
		$credit_card 	= '4012888888881881';
		//$expiry 	= '04/15';
		$expiry 	= '0318';
		
		//$paymentProcessor = new PaymentProcessor();
		$paymentProcessor = new PaymentProcessor(get_CP_API_LOGIN_ID(), get_CP_TRANSACTION_KEY());
		$paymentProcessor->setAmount($amount);
		
		# if manual enter Credit Card number
		$paymentProcessor->setCreditCard($credit_card);
		$paymentProcessor->setExpiry($expiry);
		
		//$paymentProcessor->switchToManualMode();
		//$response = $paymentProcessor->authorizeAndCapture($amount, $cc_number, $expiry);
		//$paymentProcessor->authorizeAndCapture();
		$response = $paymentProcessor->authorizeAndCapture();
		
		debug($paymentProcessor->result);
		debug('Payment Processor Response');
		debug($paymentProcessor->response);
		exit;
			
	}
	
	public function test_manual()
	{
		$formSubmit = TRUE;
		$manual		= FALSE;
			
		echo 'Please be patient, Payment Processing ...';
		
		$amount 	= '1000.0';
		//$cc_number 	= '4111111111111111';
		$credit_card 	= '4012888888881881';
		//$expiry 	= '04/15';
		$expiry 	= '0318';
		
		$paymentProcessor = new PaymentProcessor();
		$paymentProcessor->switchToTestMode();
		$paymentProcessor->setAmount($amount);
		
		# if manual enter Credit Card number
		$paymentProcessor->setCreditCard($credit_card);
		$paymentProcessor->setExpiry($expiry);
		
		//$paymentProcessor->switchToManualMode();
		//$response = $paymentProcessor->authorizeAndCapture($amount, $cc_number, $expiry);
		//$paymentProcessor->authorizeAndCapture();
		$response = $paymentProcessor->authorizeAndCapture();
		
		debug($paymentProcessor->result);
		debug('Payment Processor Response');
		debug($paymentProcessor->response);
		exit;
			
	}
	
	public function voiditem(){
		$trans_id 	= '2213059808';
		$trans_id 	= '2213557863';
		$paymentProcessor = new PaymentProcessor(get_CP_API_LOGIN_ID(), get_CP_TRANSACTION_KEY());
		$response = $paymentProcessor->voidPaymentByAuthorize($trans_id);
		debug($paymentProcessor->result);
			
		debug('Payment Processor Response');
		debug($paymentProcessor->response);
		
		debug('$response=>');
		debug($response);
		exit;
	}
	
	public function returnAmount(){
		
		$void = TRUE;
		$is_store_order = TRUE;
		$amount = '0.01';
		
		$paymentProcessor = new PaymentProcessor();
		//$paymentProcessor->setAmount($amount);
		$response = $paymentProcessor->returnPaymentByAuthorize($amount, $trans_id, $void);
		$response = $paymentProcessor->returnPaymentByCybersource($subscription_id, $referenceCode, $captureRequestID, $captureRequestToken, $reconciliationID, $amount);
		exit;
	}
	
	public function refund(){
		
		$amount 	= '0.01';
		//$trans_id = '2213804135';
		//$cc_number 	= '4012888888881881';
		$trans_id = '2213608250';
		$cc_number 	= '4585460001442970';
		$trans_id 	= '';
		$cc_number 	= '';
		$trans_id 	= '2213942321';
		$cc_number 	= '5327020225004699';
		
		# 100001416 - MasterCard - Mobile
		$trans_id 	= '2213942517';
		$cc_number 	= '5327020225004699';
		$cc_number 	= '4699';
		
		# 100001416 - Visa - Mobile
		$trans_id 	= '2213788574';
		$cc_number 	= '4585460001442970';
		$cc_number 	= '2970';

		# 100001416 - Visa - Mobile
		$trans_id 	= '2213745450';
		$cc_number 	= '4111111111111111';
		$cc_number 	= '1111';
		
		
		
		
		
		$paymentProcessor = new PaymentProcessor('37qGerh4PPU', '6H3T8849PZ6xgqAp');
		$response = $paymentProcessor->refundPaymentByAuthorize($amount, $trans_id, $cc_number);
		debug($response);
		debug('Payment Processor Response');
		debug($paymentProcessor->response);
		//$response = $paymentProcessor->refund(0, 0,0,0);
		exit;
	}
	
	public function void(){
		$trans_id = $_GET['trans_id'];
		$paymentProcessor = new PaymentProcessor();
		$response = $paymentProcessor->void($trans_id);
		debug($response);
		exit;
	}
	
}

