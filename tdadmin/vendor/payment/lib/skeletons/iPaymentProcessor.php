<?php
/**
 * @package iPaymentProcessor
 * @author 	Nouman
 * @since	March19,2014
 * @version 0.1
 * @desc
 * 	Payment Gateway Interface
 * */
INTERFACE iPaymentProcessor{

	/**
	 * @desc
	 * Authorization
	 * 	Authorize.net 
	 * 	CyberSource
	 * 	Paypal
	 * */
	function authorize($amount = false);
	
	/**
	 * @desc
	 * Capture
	 * 	Authorize.net 
	 * 	CyberSource
	 * 	Paypal
	 * */
	function capture($auth_code = false, $amount = false);
	
	/**
	 * @desc
	 * Authorize + Capture
	 * 	Authorize.net 
	 * 	CyberSource
	 * 	Paypal
	 * */
	function authorizeAndCapture($amount=null);

	/**
	 * @desc
	 * Refund
	 * 	Authorize.net 
	 * 	CyberSource
	 * 	Paypal
	 * */
	//function returnPayment($trans_id = false, $amount = false);
	function refundPaymentByAuthorize($amount = false, $trans_id = false, $credit_card = false, $expiry = false);
	
	//function returnPaymentByAuthorize($trans_id = false, $amount = false, $void=false);
	//function returnPaymentByCybersource($subscription_id, $referenceCode, $captureRequestID, $captureRequestToken, $reconciliationID, $amount);
	
	/**
	 * @desc
	 * Refund
	 * 	Authorize.net 
	 * 	CyberSource
	 * 	Paypal
	 * */
	//function refund($trans_id = false, $amount = false);

	/**
	 * @desc
	 * Void
	 * 	Authorize.net 
	 * 	CyberSource
	 * 	Paypal
	 * */
	function void($trans_id = false);
	
}
