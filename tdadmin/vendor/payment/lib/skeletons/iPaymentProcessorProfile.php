<?php
/**
 * @package iPaymentProcessorProfile
 * @author 	Nouman
 * @since	Sep19,2014
 * @version 0.1
 * @desc
 * 	Payment Gateway Profile Interface
 * */
INTERFACE iPaymentProcessorProfile{

	/**
	 * @desc
	 * Authorization
	 * 	Authorize.net 
	 * 	CyberSource
	 * 	Paypal
	 * */
	function createCustomerProfile($options=array());

	function setPaymentProfileId($payment_profile_id = false);
	
	/**
	 * @desc
	 * Authorization
	 * 	Authorize.net 
	 * 	CyberSource
	 * 	Paypal
	 * */
	function authorize($amount = false);
	
	/**
	 * @desc
	 * Capture
	 * 	Authorize.net 
	 * 	CyberSource
	 * 	Paypal
	 * */
	function capture($auth_code = false, $amount = false);
	
	/**
	 * @desc
	 * Authorize + Capture
	 * 	Authorize.net 
	 * 	CyberSource
	 * 	Paypal
	 * */
	function authorizeAndCapture($amount=null);

	/**
	 * @desc
	 * Refund
	 * 	Authorize.net 
	 * 	CyberSource
	 * 	Paypal
	 * */
	function refund($trans_id = false, $amount = false);

	/**
	 * @desc
	 * Void
	 * 	Authorize.net 
	 * 	CyberSource
	 * 	Paypal
	 * */
	function void($trans_id = false);
	
}
