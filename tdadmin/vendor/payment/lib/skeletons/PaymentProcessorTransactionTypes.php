<?php
/**
 * @package 	PaymentProcessor
 * @subpackage 	PaymentTransactionTypes
 * @author 	Nouman
 * @since	March20,2014
 * @version 0.1
 * @desc
 * 	Payment Transaction Types
 * <br/>
 * <br/>CARD PRESENT: When customer shopping in store etc with Credit Card present
 * <br/>CARD NOT PRESENT: In online E-Commerce system where customer submit Credit Card number
 * 
 * */
CLASS PaymentProcessorTransactionTypes{

	/*CARD PRESENT: When customer shopping in store etc with Credit Card present */
	const CARD_PRESENT		= 'CARD_PRESENT';
	
	/*CARD NOT PRESENT: In online E-Commerce system where customer submit Credit Card number */
	const CARD_NOT_PRESENT	= 'CARD_NOT_PRESENT';
	
}
