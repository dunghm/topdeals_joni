<?php
/**
 * @package 	iPaymentProcessor
 * @subpackage 	PaymentProcessor
 * @author 	Nouman
 * @since	March20,2014
 * @version 0.1
 * @desc
 * 	Payment Processor Interface
 * */
CLASS PaymentProcessorGatewaysList{

	const AUTHORIZE 	= 'AUTHORIZE';
	const AUTHORIZE_CIM = 'AUTHORIZE_CIM';
	const CYBERSOURCE	= 'CYBERSOURCE';
	const PAYPAL 		= 'PAYPAL';
	const I_PAY 		= 'I_PAY';
	const SAGE_PAY 		= 'SAGE_PAY';
	
}
