<?php
/**
 * Debug
 *
 * Lets you determine whether an array index is set and whether it has a value.
 * If the element is empty it returns FALSE (or whatever you specify as the default value.)
 *
 * @access	public
 * @param	string|array|object
 * @param	integer
 * @return	mixed
 */
if ( ! function_exists('debug'))
{
	function debug($data, $return = FALSE)
	{
		# This function use for debugging
		$can_debug = TRUE;
		
		# Global debugging off
		if(defined('DEBUG') && DEBUG === 0)
			$can_debug = FALSE;

		$string = "";
		if($return){
			return var_export($data, $return);
		}elseif($can_debug === TRUE){
			echo '<pre>';
			print_r($data);
			echo '</pre>';
		}
		
	}
}

// ------------------------------------------------------------------------

