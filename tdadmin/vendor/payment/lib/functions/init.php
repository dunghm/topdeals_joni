<?php
$CONF_FUNCTIONS = '';
if(defined('CONF_FUNCTIONS')){
	$CONF_FUNCTIONS = CONF_FUNCTIONS;
}

if(file_exists($CONF_FUNCTIONS. "utility.php"))
	require_once $CONF_FUNCTIONS. "utility.php";

if(file_exists($CONF_FUNCTIONS. "common_functions.php"))
	require_once $CONF_FUNCTIONS. "common_functions.php";
	
if(file_exists($CONF_FUNCTIONS. "functions_payment_processors.php"))
	require_once $CONF_FUNCTIONS. "functions_payment_processors.php";
else
	die('Error to load required file <b>functions_payment_processors.php</b>');