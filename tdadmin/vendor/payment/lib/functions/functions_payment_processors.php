<?php
function getPreferences($field=null){
	
	$field = str_replace("get_", "", $field);
	if(defined($field))
		return constant($field);
	else 
		die('Please define Constant ['. strtoupper($field) .']');
		
	return false;
	
}

function IS_TEST_PAYMENT(){return getPreferences(__FUNCTION__);}
function get_AUTHORIZENET_ENDPOINT(){return getPreferences(__FUNCTION__);}
function get_AUTHORIZENET_CARDPRESENT_ENDPOINT(){return getPreferences(__FUNCTION__);}
function get_AUTHORIZENET_DUPLICATE_WINDOW(){return getPreferences(__FUNCTION__);}
function get_AUTHORIZE_MODE(){return getPreferences(__FUNCTION__);}

function get_CP_API_LOGIN_ID(){return getPreferences(__FUNCTION__);}
function get_CP_TRANSACTION_KEY(){return getPreferences(__FUNCTION__);}

function get_CNP_API_LOGIN_ID(){return getPreferences(__FUNCTION__);}
function get_CNP_TRANSACTION_KEY(){return getPreferences(__FUNCTION__);}

function get_CS_MERCHANT_ID(){return getPreferences(__FUNCTION__);}
function get_CS_TRANSACTION_ID(){return getPreferences(__FUNCTION__);}
function get_CS_WSDL(){return getPreferences(__FUNCTION__);}


