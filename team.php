<?php
require_once(dirname(__FILE__) . '/app.php');

if ( isset($_GET['title']))
{
    $urltitle = $_GET['title'];
    $team = DB::LimitQuery('team', array(
	'condition' => array ('seo_title' => $urltitle),
	'order' => 'ORDER BY id DESC',
        'one' => true
        ));
    
    $team = Table::Fetch('team',$urltitle, 'seo_title');
    if ( !$team && strstr($urltitle, '_'))
    {
         $team = Table::Fetch('team', str_replace("_","-", $urltitle), 'seo_title');
         if ($team)
         {
             header("HTTP/1.1 301 Moved Permanently"); 
             header("Location: ". ZTeam::GetTeamUrl($team));
             exit;
         }
    }
    if ( !$team )
    {
       redirect( WEB_ROOT . '/team/index.php');
    }
    $id = $team['id'];
}
else
{
   
    $id = abs(intval($_GET['id']));
    
    if ( !$id )
    {
		$isSunday = isSunday();	
		$team = current_team($city['id'], 'normal', 'home_pg',$isSunday);
		$teamArray = $team;

    }
    else if (!$team = Table::FetchForce('team', $id) ) 
    {
        redirect( WEB_ROOT . '/team/index.php');
    }    
}

/*  deal view history */
$deal_view_history = $_SESSION['deal_view_history'];

if($login_user_id){
	$h = array(
				'user_id' => $login_user_id,
				'deal_id' => $team['id'],
				);
	$q = DB::Insert('deal_view_history', $h);

	$hist_sql = "SELECT DISTINCT deal_id FROM deal_view_history WHERE user_id = '{$login_user_id}' ORDER BY datetime DESC LIMIT 3";

	$hist_res = DB::GetQueryResult($hist_sql, false);
	$deal_ids = Utility::GetColumn($hist_res, 'deal_id');
	
	$deal_sql = "SELECT * FROM team WHERE id IN (". implode(",", $deal_ids) .")" ;
	$deal_view_history = DB::GetQueryResult($deal_sql, false);
}

	if(!$deal_view_history){
		$deal_view_history = array();
	}
	if(in_array($team, $deal_view_history)){
		array_splice($deal_view_history, array_search($team, $deal_view_history), 1);
	}
	array_push($deal_view_history, $team);
	if(sizeof($deal_view_history)>2){
		$deal_popped = array_shift($deal_view_history);
	}


$_SESSION['deal_view_history'] = $deal_view_history;
/*  deal view history */

/* refer */
if ($_rid = abs(intval($_GET['r']))) { 
	if($_rid) cookieset('_rid', abs(intval($_GET['r'])));
	redirect( WEB_ROOT . ZTeam::GetTeamUrl($team));
}
$teamcity = Table::Fetch('category', $team['city_id']);
$city = $teamcity ? $teamcity : $city;
$city = $city ? $city : array('id'=>0, 'name'=>'All');

if ( $lang == "en" )
    $pagetitle = $team['title'];
else
    $pagetitle = $team['title_fr'];

$discount_price = $team['market_price'] - $team['team_price'];
$discount_rate = team_discount($team);

$left = array();
$now = time();

if($team['end_time']<$team['begin_time']){$team['end_time']=$team['begin_time'];}

$diff_time = $left_time = $team['end_time']-$now;
if ( $team['team_type'] == 'seconds' && $team['begin_time'] >= $now ) {
	$diff_time = $left_time = $team['begin_time']-$now;
}

$left_day = floor($diff_time/86400);
$left_time = $left_time % 86400;
$left_hour = floor($left_time/3600);
$left_time = $left_time % 3600;
$left_minute = floor($left_time/60);
$left_time = $left_time % 60;

/* progress bar size */
$bar_size = ceil(110*($team['now_number']/$team['max_number'])); // it was 118
$bar_offset = 0;//ceil(118*($team['now_number']/$team['max_number']));
$partner = Table::Fetch('partner', $team['partner_id']);
$team['state'] = team_state($team);

/* your order */
if ($login_user_id && 0==$team['close_time']) {
	$order = DB::LimitQuery('order', array(
				'condition' => array(
					'team_id' => $id,
					'user_id' => $login_user_id,
					'state' => 'unpay',
					),
				'one' => true,
				));
}
/* end order */

/*kxx team_type */
if ($team['team_type'] == 'seconds') {
	die(include template('team_view_seconds'));
}
if ($team['team_type'] == 'goods') {
	die(include template('team_view_goods'));
}
/*xxk*/

/*seo*/
$seo_title = $team['seo_title'];
$seo_keyword = $team['seo_keyword'];
$seo_description = $team['seo_description'];
/*end*/

/*right sidebar deals*/
$condition2 = array(
	'system' => 'Y',
	"end_time > {$now}",
	"sort_order > 0 ",
);
	/* filter start */
	$team_typ = strval($_GET['team_type']);
	if ($team_typ) { $condition2['team_type'] = $team_typ; }
	/* filter end */
$deals = DB::LimitQuery('team', array(
	'condition' => $condition2,
	'order' => 'ORDER BY sort_order DESC',
));
/*end right sidebar deals*/

if($team['type']!=''){			// if multi option enabled?

/* multi buy options */
$multi_con = array(
		'team_id' => $team['id'],
		'parent_option_id' => 0,
		);
$multi_op = DB::LimitQuery('team_multi', array(
	'condition' => $multi_con,
	'order' => 'ORDER BY multi_order ASC',
));



// filter sold out items
  foreach($multi_op as $key=>$mul){
    //echo '<br>'.$mul['id'].' - '.$mul['title'].' - '.$mul['max_number'].' - '.$mul['now_number'];
    $mul['soldout'] = 0;		
      if ($mul['max_number'] > 0 )
    {
     $stock_left = $mul['max_number'] - $mul['now_number'];
      if($stock_left>0){
        $multi[]=$mul;
      }else{
      	$mul['soldout'] = 1;
      	$multi[]=$mul; 
      }
    }
    else
    $multi[]=$mul;
    
  }
 
 /// print_r_r($multi);
if( count($multi_op) > 0 && empty($multi)){
    $team['state'] = 'soldout';
}

$my_cond = true;
$parent=$multi;
$multi_option_type = $multi[0]['type'];

while($my_cond){
       // print_r_r($parent);
	$sub_options = extract_sub_options($parent);
	
	foreach($sub_options as $sub){
            
		//echo '<br>'.$sub['id'].' - '.$sub['title'].' - '.$sub['parent_option_id'].' - '.$sub['type'];		
           $sub['soldout'] = 0;
           
	   if ($sub['max_number'] > 0 ){
	  	$stock_left = $sub['max_number'] - $sub['now_number'];
		  
		  if($stock_left > 0) {
		  	$subs[]=$sub;
		  } else {	
		  	$sub['soldout'] = 1;
			$subs[]=$sub;
		  }
          
	  }
          else{
              $subs[]=$sub;
          }
	}
	
	$parent=$my_cond=$sub_options;
}

$sub_option_type = $sub['type'];

}	// end multi option enabled check
//echo 'asdf';
//print_r_r($subs);
$category = Table::Fetch('category', $team['group_id']);
$primary = false;

$category_count = DB::GetQueryResult("select category.id as group_id, count(title) sum from category left join team on category.id = group_id  and end_time >  {$now} AND begin_time <= {$now} 
where zone = 'group'

GROUP BY category.id" , false);

    $category_count = Utility::AssColumn($category_count, "group_id");


$group_id = $team['group_id'];

$recommended_deals = ZTeam::GetRecommended(2, $team);
$desc_images = DB::LimitQuery('team_slideshow_images', array(
			'condition' => array( 'team_id' => $id,
                                                'image_desc' => 'desc',    
                                ),
                        'order' => 'ORDER BY image_order'    
			));
   
$gallery_images = DB::LimitQuery('team_slideshow_images', array(
			'condition' => array( 'team_id' => $id,
                                                'image_desc' => 'gallery',    
                                ),
                         'order' => 'ORDER BY image_order'           
			));

$brand = Table::Fetch('brands', $team['brand_id']);

/**
* GET NEXT AND PREVIOUS TEAM
*/
//LOGIC IS GET FROM TEAM LISTING PAGE
$daytime = strtotime(date('Y-m-d'));
$type = '';

$condition = array(
	'system' => 'Y'
);

$condition[] = "group_id = {$group_id}";  
$condition[] = "end_time > {$now}";  
$condition[] = "begin_time <= {$now}";  


if (!option_yes('displayfailure')) {   // if Failed deals are not allowed to display then cut those
	$condition['OR'] = array(
		"now_number >= min_number",
		"end_time > '{$daytime}'",
	);
}
//REMOVE FEATURE TEAM
$sql_feature = "SELECT * from team "
        . "WHERE";
	if( $group_id ){
		$sql_feature.= " group_id = {$group_id} AND ";
	}
	$sql_feature.= " end_time > {$now} AND begin_time <= {$now} AND featured = 1 ORDER BY begin_time DESC";
   
$feature_team = DB::GetQueryResult($sql_feature , true);
if($feature_team){
	$condition[] = "id != {$feature_team['id']}";
}



//BUILD CONDITION TO GET NEXT RECORD RESULT
$condition[] = "id > {$id}";  
$nextCondition = DB::BuildCondition( $condition );

//BUILD CONDITION TO GET NEXT RECORD RESULT
//UNSET ID condition to get Previous Link DATA
unset($condition[4]);
$condition[] = "id < {$id}";  
$previousCondition = DB::BuildCondition( $condition );

// $getData	= DB::Count('team', $condition);
$nextIdSql	=	<<<SQL
					select min(id) AS nextid from team where $nextCondition
SQL;

$getNextData	=	DB::GetQueryResult($nextIdSql);
$next_team_id	=	"";
if(!empty($getNextData)&&isset($getNextData['nextid'])){
	$next_team_id	=	$getNextData['nextid'];
}
// $getData	= DB::Count('team', $condition);
$prevIdSql	=	<<<SQL
					select max(id) AS previd from team where $previousCondition
SQL;

$getPrevData		=	DB::GetQueryResult($prevIdSql);
$previous_team_id	=	"";
if(!empty($getPrevData)&&isset($getPrevData['previd'])){
	$previous_team_id	=	$getPrevData['previd'];
}


   // echo count($recommended_deals);
if ( $home )
{
    $exp_soon_cond = array (
	"end_time > {$now}",
        "id != {$team['id']}",
        "begin_time <= {$now}","sort_order > 0");
    //$exp_soon_order = "ORDER BY (end_time - {$now}) ASC, sort_order DESC";
    $exp_soon_order =  'ORDER BY sort_order DESC';
    
    $expiring_soon = DB::LimitQuery("team", array(
        "condition" => $exp_soon_cond,
        "order" => $exp_soon_order,
        'size' => 4,
    ));
    $video_sql = "SELECT * FROM videos where video_starttime < {$now} AND video_endtime > {$now}";    
    $home_page_video = DB::GetQueryResult($video_sql);

    if(!$home_page_video){
        $home_page_video = DB::GetQueryResult("SELECT * FROM videos where `default`=1");
    }
    
    $one_month_before = strtotime('-1 month');
    $best_selleer_sql = "SELECT t.*, COUNT(oi.quantity) AS sold_times  FROM `team` t INNER JOIN order_item oi ON oi.team_id = t.id 
                        INNER JOIN `order` o ON o.id = oi.order_id
                        WHERE t.system = 'Y' AND o.create_time > $one_month_before AND ".
                        "t.end_time > {$now} AND begin_time <= {$now} ".  
                        "GROUP BY t.id ORDER BY sold_times DESC LIMIT 0, 4";
    $best_sellers = DB::GetQueryResult($best_selleer_sql , false);
	
	$category_deals = array();
    $all_categories = ZCategory::GetCategories();
    foreach($all_categories as $index => $deal_category)
    {
        $category_deal = ZTeam::GetLatestTeamByCategroy($deal_category['id'], $team['id']);
        $category_deals[$deal_category["name"]] = $category_deal;
    }
	$cat_deal = "";
    
	include template_ex('content_home');
}
else
{
    //$primary = false;
    if ( isset($_GET['primary']) ){
        $primary = true;
    }
    if(isset($_GET['mode'])){
        $mode = $_GET['mode'];
        if($mode == 'options'){
            include template_ex('content_deal_options');
        }else{
            include template_ex('content_deal_detail');
        }
    }else{
        include template_ex('content_deal_detail');
    }
    
}
function extract_sub_options($parent){
        
/* sub options */
	$parent_option_id = Utility::GetColumn($parent, 'id');
	$sub_options_sql = "SELECT * FROM team_multi WHERE parent_option_id IN (". implode(",", $parent_option_id) .") order by multi_order ASC";
	//echo '<br>'.$sub_options_sql;
	$sub_options = DB::GetQueryResult($sub_options_sql, false);
	return $sub_options;
}
