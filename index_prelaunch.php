<?php

//Header("Location: http://www.topdeal.ch/");

	require_once(dirname(__FILE__) . '/app.php');
	if ( $login_user ) {
		redirect(WEB_ROOT . '/account/refer.php');
	} 

$target = mktime(6, 35, 0, 11, 22, 2011) ;

$today = time ()  -  60*60;

$diff =($target-$today);
?>
<!DOCTYPE html 
    PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
           "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>	
	<title>TopDeal.ch - Les meilleures offres - deals de Suisse Romande</title>
  <link href="static/css/stylesheet.css" rel="stylesheet" type="text/css" />
  <link href="static/css/prelaunch.css" rel="stylesheet" type="text/css" />
  <link rel="shortcut icon" href="/static/icon/favicon.ico" />
  
  <script type="text/javascript" src="static/js/jquery.min.js"></script>
  <script type="text/javascript" src="static/js/jquery.center.js"></script>
  <script src="static/js/cufon-yui.js" type="text/javascript"></script>
  <script src="static/js/Akkurat_400.font.js" type="text/javascript"></script>
  <script src="static/js/Akkurat_Light_400.font.js" type="text/javascript"></script>
  <script src="static/js/Akkurat_Bold_400.font.js" type="text/javascript"></script>
  <script type="text/javascript">
        
        remain = <?php echo $diff; ?>;
        remain += ((new Date()).getTimezoneOffset() + 60) * 60;
        function convert(){
          if(remain <= 0) {
            clearTimeout(timer);	
            return;
          };
          var days = parseInt(remain/(3600*24));
          var hours = parseInt((remain - days*24*3600)/3600);
          var minutes = parseInt((remain - days*24*3600 - hours * 3600)/60);
          var seconds = remain - days*24*3600 - hours * 3600 - minutes * 60;
          render(days, hours, minutes, seconds);
          remain--;
          timer = setTimeout(convert, 1000);
        }
        function render(days, hours, minutes, seconds){
						var html = '<ul>';
						html += '<li><div>Jours</div><div class="round_bg">' + days + '</div></li>';
						html += 	'<li><div>Heures</div><div class="round_bg">' + hours + '</div></li>';
						html += 	'<li><div>Minutes</div><div class="round_bg">' + minutes + '</div></li>';
						html += 	'<li><div>Secondes</div><div class="round_bg">' + seconds + '</div></li></ul>';
						$('#clock').html(html);
				}
        convert();
  </script>
</head>
<body>
	<div class="top_wrraper"></div>
	<div id="header" class="header clear_fix">			
		<div class="right fr">
			<form id="login-user-form" action="account/login.php" method="post">
				<div class="inputarea">
					<p>Email</p>
					<div class="left_input">
						<div class="right_input">
							<div class="center_input">
								<input name="email" type="text" value=""/>
							</div>
						</div>
					</div>
					<p>Mot de passe</p>
					<div class="left_input">
						<div class="right_input">
							<div class="center_input">
								<input name="password" type="password" value=""/>
							</div>
						</div>
					</div>
				</div>
				<div class="action">
					<a href="javascript:document.getElementById('login-user-form').submit();">Login</a>
					<a href="account/signup.php">S'enregistrer</a>
				</div>
			</form>			
		</div>
		<div class="left">
			<div class="banner clear_fix">
      		<div id="clock" class="fr"></div>
         	<div class="logo fl"><a href="/index.php" class="link"><img src="/static/img/logo.png"></a></div>              
			</div>
			<div class="navigation">
				&nbsp;			
			</div>
		</div>
	</div>
	<div id="main" class="main clear_fix">
		<div class="sidebar fl leftside">     
    	<h3><a href="http://www.facebook.com/TopDeal.ch">CONCOURS IPHONE 4S</a></h3>
      <a href="http://www.facebook.com/TopDeal.ch"><img src="/static/img/iphone4s.png"/></a>
      <p>Gagnez un iPhone 4s via notre page facebook</p>
      <a href="http://www.facebook.com/TopDeal.ch"><img src="/static/img/participer.png"/></a>
		</div>
    
		<div class="sidebar fl" id="mainbody">
    	<h2>OUVERTURE DU SITE LE 22 NOVEMBRE</h2>
      <p align="justify">Envie de détente avec un voyage découverte ou un abonnement dans un centre de bien-être? Envie de sensations fortes avec un saut en parachute ou un vol en hélicoptère? Ou tout simplement faire des cadeaux à vos enfants et votre famille avec les fêtes qui se profilent à l'horizon?</p>
	    <div class="m1">  
      <p id="m1">Enregistrez-vous dès à présent et bénéficiez de crédits à utiliser sur TopDeal.ch dès l'ouverture du site!</p>
			</div>
			<div id="news_letter" class="clear_fix">
      	
        <div id="message_box">				
						<form id="message" action="/account/signup.php" method="post">
						<input class="email" type="text" value="E-mail" name="email" onclick="this.value=''">
						<input class="email" type="text" value="Mot de passe" name="password" onclick="this.value='';this.type='password';" onfocus="this.value='';this.type='password';">
						<input class="email" type="text" value="Confirmation du mot de passe" name="password2" onclick="this.value='';this.type='password';" onfocus="this.value='';this.type='password';">
						<input class="submit" type="submit" value="">
						</form>

				</div><!--eof_message_box-->
        <div id="message_box1">					
          <ul>
          	<li>Enregistrez-vous maintenant</li>
            <li>Accédez à votre lien personnel</li>
            <li>Partagez votre lien avec vos amis</li>
            <li>Gagnez des crédits TopDeal</li>
          </ul>
      	</div>
              
					
					
        </div>
        <div id="subscribe">	
						<div id="subscribe_header">
<p id="s_txt">Pour être informé des offres TopDeal, saisissez votre adresse e-mail</p></div>
						<form id="subscribe" action="/subscribe.php" method="post">
						<input class="email1" type="text" value="E-mail" name="email" onclick="this.value=''">
						<input type="hidden" name="city_id" value="{$city['id']}">
						<input class="submit1" type="submit" value="">
						</form>
				</div><!--eof_subscribe-->							
		</div>
    <div class="sidebar right fl">
	<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/fr_FR/all.js#xfbml=1&appId=184414568270630";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
	<div class="fb-like-box" data-href="http://www.facebook.com/topdeal.ch" data-width="160" data-height="287" data-show-faces="true" data-stream="false" data-header="false"></div>
    </div>
    <div class="sidebar right fl" style="margin-top: 10px; text-align:center;">
    	<h3>RETROUVEZ NOUS SUR</h3>
      <div class="clear_fix">
      	<a href="http://www.facebook.com/TopDeal.ch" class="social facebook">&nbsp;</a>
        <a href="http://www.linkedin.com/company/topdeal-s-a-" class="social linkedin">&nbsp;</a>
        <a href="http://twitter.com/#!/topdealch" class="social twitter">&nbsp;</a>
        <a href="http://topdealch.tumblr.com/" class="social tumblr">&nbsp;</a>
      </div>
    </div>
	</div>
    <div id="footer_wrapper">
		<div id="footer" class="footer">
        	<div class="column_first">
            	<a href="index.php"><img src="/static/img/logosmall.png" alt="top deal"  /></a>
                <div class="copyright">
                    <p>&copy; 2011 TopDeal.ch <br/>copyright reserved <br/>
                    by TopDeal S.A.</p>
                </div>
            </div>
            <div class="column linespace">
            	<h3>TOP DEAL</h3>
                <ul>
                    <li><a href="about/contact.php">Nous contacter</a></li>
                    <li><a href="about/terms.php">Conditions générales</a></li>
                    <li><a href="about/privacy.php">Confidentialité</a></li>                    
                </ul>
            </div>   
        </div>
    </div>
<script type="text/javascript">
$(document).ready(function(){
	Cufon.replace('.action a',{ fontFamily: 'Akkurat' });
		
	
});
</script>
<script type="text/javascript"> Cufon.now(); </script>
 <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', "{$INI['other']['google_analytic_tarcking_id']}"]); //UA-25716430-1
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</body>
</html>
