<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$lang = "fr";                   // Default English
$LC = 'fr';

if ( isset($_GET['tr']) )
{
	if ( $_GET['tr'] == "fr" )
	{
             // User requested French
            $LC = $_SESSION['tr'] = $lang = "fr";     
                
	}
	else
	{
            // User requested English
            $LC = $_SESSION['tr'] = $lang = "en";               
	}
}
else if ( isset($_SESSION['tr']) )
{
	if ( strcmp($_SESSION['tr'], "en") == 0  )
        {
            // User has previously Selected English
           $LC = $lang = "en";
        }
        else if ( strcmp($_SESSION['tr'], "fr") == 0  )
        {
           // User has previously Selected English
           $LC = $lang = "fr";            
        }
}
else
{
   // Niether requested nor found in Session, detect based on Browser
   /*$language = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
    if (substr($language, 0, 2) == 'en')
    {
        // User Browser is set on English
       $LC = $lang = "en";        
    }
    else  if (substr($language, 0, 2) == 'fr')
    {
        // User Browser is set on French
       $LC = $lang = "fr";        
    }*/

}
/*

if ( $lang == 'en' )
{
    define('LABEL_DAILY', 'Today\'s Hype');
    define('LABEL_WEEKLY', 'Weekly Hype');
    define('LABEL_RECENT', 'Recent Hypes');
    define('LABEL_TOUR', 'Tour');
    define('LABEL_DISCUSS', 'Discuss');
    define('LABEL_FAQ', 'FAQs');
    define('LABLE_ALL', 'All');
    
}
else if ( $lang == 'fr' )
{
    define('LABEL_DAILY', 'NoOpY du jour');
    define('LABEL_WEEKLY', 'NoOpY de la semaine');
    define('LABEL_RECENT', 'NoOpYs récents');
    define('LABEL_TOUR', 'Tour');
    define('LABEL_DISCUSS', 'Discuss');
    define('LABEL_FAQ', 'FAQs');
    define('LABLE_ALL', 'Toutes');
}
*/
?>
