<?php
/* import other */
import('configure');
import('current');
import('utility');
import('mailer');
import('sms');
import('upgrade');
import('uc');
import('cron');

$subtemplate = "";

function truncateTitle($title)
{
	return true;
	/*if(strlen($title) > 30)
     $title = substr($title,0,30) . "...";
	 return $title;*/
}

function template_ex($sub) {
    
    global $subtemplate;
    $subtemplate = $sub;
    
    if($sub == "content_home"){
        return template("master_home");
    }
    else{
        return template("master");    
    }
    
}

function template_sub($sub) {
	return __template($sub);
}

function template($tFile) {
	global $INI;
	if ( 0===strpos($tFile, 'manage') ) {
		return __template($tFile);
	}
	if ($INI['skin']['template']) {
		$templatedir = DIR_TEMPLATE. '/' . $INI['skin']['template'];
		$checkfile = $templatedir . '/html_header.html';
		if ( file_exists($checkfile) ) {
			return __template($INI['skin']['template'].'/'.$tFile);
		}
	}
	return __template($tFile);
}

function render($tFile, $vs=array()) {
    ob_start();
    foreach($GLOBALS AS $_k=>$_v) {
        ${$_k} = $_v;
    }
	foreach($vs AS $_k=>$_v) {
		${$_k} = $_v;
	}
	include template($tFile);
    return render_hook(ob_get_clean());
}

function render_hook($c) {
	global $INI;
	$c = preg_replace('#href="/#i', 'href="'.WEB_ROOT.'/', $c);
	$c = preg_replace('#src="/#i', 'src="'.WEB_ROOT.'/', $c);
	$c = preg_replace('#action="/#i', 'action="'.WEB_ROOT.'/', $c);

	/* theme */
	$page = strval($_SERVER['REQUEST_URI']);
	if($INI['skin']['theme'] && !preg_match('#/manage/#i',$page)) {
		$themedir = WWW_ROOT. '/static/theme/' . $INI['skin']['theme'];
		$checkfile = $themedir. '/css/index.css';
		if ( file_exists($checkfile) ) {
			$c = preg_replace('#/static/css/#', "/static/theme/{$INI['skin']['theme']}/css/", $c);
			$c = preg_replace('#/static/img/#', "/static/theme/{$INI['skin']['theme']}/img/", $c);
		}
	}
	if (strtolower(cookieget('locale','zh_cn'))=='zh_tw') {
		require_once(DIR_FUNCTION  . '/tradition.php');
		$c = str_replace(explode('|',$_charset_simple), explode('|',$_charset_tradition),$c);
	}
	/* encode id */
	$c = obscure_rep($c);
	return $c;
}

function output_hook($c) {
	global $INI;
	if ( 0==abs(intval($INI['system']['gzip'])))  die($c);
	$HTTP_ACCEPT_ENCODING = $_SERVER["HTTP_ACCEPT_ENCODING"]; 
	if( strpos($HTTP_ACCEPT_ENCODING, 'x-gzip') !== false ) 
		$encoding = 'x-gzip'; 
	else if( strpos($HTTP_ACCEPT_ENCODING,'gzip') !== false ) 
		$encoding = 'gzip'; 
	else $encoding == false;
	if (function_exists('gzencode')&&$encoding) {
		$c = gzencode($c);
		header("Content-Encoding: {$encoding}"); 
	}
	$length = strlen($c);
	header("Content-Length: {$length}");
	die($c);
}

$lang_properties = array();
function I($key) { 
    global $lang_properties, $LC;
    
    if (empty($lang_properties)) {
       
        $ini = 'i18n/' . $LC. '/properties.ini';
      
        $lang_properties = Config::Instance($ini);
    }
    return isset($lang_properties[$key]) ?
        $lang_properties[$key] : $key;
}

function json($data, $type='eval') {
    $type = strtolower($type);
    $allow = array('eval','alert','updater','dialog','mix', 'refresh');
    if (false==in_array($type, $allow))
        return false;
    Output::Json(array( 'data' => $data, 'type' => $type,));
}
if ( ! function_exists('redirect'))
{
function redirect($url=null, $notice=null, $error=null) {
	$url = $url ? obscure_rep($url) : $_SERVER['HTTP_REFERER'];
	$url = $url ? $url : '/';
	if ($notice) Session::Set('notice', $notice);
	if ($error) Session::Set('error', $error);
    header("Location: {$url}");
    exit;
}
}
function write_php_file($array, $filename=null){
	$v = "<?php\r\n\$INI = ";
	$v .= var_export($array, true);
	$v .=";\r\n?>";
	return file_put_contents($filename, $v);
}

function write_ini_file($array, $filename=null){   
	$ok = null;   
	if ($filename) {
		$s =  ";;;;;;;;;;;;;;;;;;\r\n";
		$s .= ";; SYS_INIFILE\r\n";
		$s .= ";;;;;;;;;;;;;;;;;;\r\n";
	}
	foreach($array as $k=>$v) {   
		if(is_array($v))   { 
			if($k != $ok) {   
				$s  .=  "\r\n[{$k}]\r\n";
				$ok = $k;   
			} 
			$s .= write_ini_file($v);
		}else   {   
			if(trim($v) != $v || strstr($v,"["))
				$v = "\"{$v}\"";   
			$s .=  "$k = \"{$v}\"\r\n";
		} 
	}

	if(!$filename) return $s;   
	return file_put_contents($filename, $s);
}   

function save_config($type='ini') {
	return configure_save();
	global $INI; $q = ZSystem::GetSaveINI($INI);
	if ( strtoupper($type) == 'INI' ) {
		if (!is_writeable(SYS_INIFILE)) return false;
		return write_ini_file($q, SYS_INIFILE);
	} 
	if ( strtoupper($type) == 'PHP' ) {
		if (!is_writeable(SYS_PHPFILE)) return false;
		return write_php_file($q, SYS_PHPFILE);
	} 
	return false;
}

function save_system($ini) {
	$system = Table::Fetch('system', 1);
	$ini = ZSystem::GetUnsetINI($ini);
	$value = Utility::ExtraEncode($ini);
	$table = new Table('system', array('value'=>$value));
	if ( $system ) $table->SetPK('id', 1);
	return $table->update(array( 'value'));
}

/* user relative */
function need_login($wap=false) {
	if ( isset($_SESSION['user_id']) ) {
		if (is_post()) {
			unset($_SESSION['loginpage']);
			unset($_SESSION['loginpagepost']);
		}
		return $_SESSION['user_id'];
	}
	if ( is_get() ) {
		Session::Set('loginpage', $_SERVER['REQUEST_URI']);
	} else {
		Session::Set('loginpage', $_SERVER['HTTP_REFERER']);
		Session::Set('loginpagepost', json_encode($_POST));
	}
	if (true===$wap) {
		return redirect(WEB_ROOT . '/wap/login.php');	
	}
	//return redirect(WEB_ROOT . '/account/signup.php');
        
        return redirect($INI['system']['secure_public_url'] . '/account/login.php?ref='.$_SERVER['REQUEST_URI']);
}
function need_post() {
	return is_post() ? true : redirect(WEB_ROOT . '/index.php');
}
function need_manager($super=false) {
	if ( ! is_manager() ) {
		redirect( WEB_ROOT . '/manage/login.php' );
	}
	if ( ! $super ) return true;
	if ( abs(intval($_SESSION['user_id'])) == 1 ) return true;
	return redirect( WEB_ROOT . '/manage/misc/index.php');
}
function need_partner($redirect = TRUE) {
        
        if(is_partner()){
            return true;
        }else{
            if(!ZLogin::GetPartner()){
                if($redirect){
                    redirect( WEB_ROOT . '/biz/login.php');
                }else{
                    return false;
                }
                    
            }
        }
}

function need_open($b=true) {
	if (true===$b) {
		return true;
	}
	if ($AJAX) json('Discuss is not yet Open', 'alert');
	Session::Set('error', 'Discuss is Disabled! Please Contact admin');
	redirect( WEB_ROOT . '/index.php');
}

function need_auth($b=true) {
	global $AJAX, $INI, $login_user;
	if (is_string($b)) {
		$auths = $INI['authorization'][$login_user['id']];
		$b = is_manager(true)||in_array($b, $auths);
	}
	if (true===$b) {
		return true;
	}
	if ($AJAX) json('Authority to operate', 'alert');
	die(include template('manage_misc_noright'));
}

function is_manager($super=false, $weak=false) {
	global $login_user;
	if ( $weak===false && 
			( !$_SESSION['admin_id'] 
			  || $_SESSION['admin_id'] != $login_user['id']) ) {
		return false;
	}
	if ( ! $super ) return ($login_user['manager'] == 'Y');
	return $login_user['id'] == 1;
}
function is_partner() {
	return ($_SESSION['partner_id']>0);
}

function is_newbie(){ return (cookieget('newbie')!='N'); }
function is_get() { return ! is_post(); }
function is_post() {
	return strtoupper($_SERVER['REQUEST_METHOD']) == 'POST';
}

function is_login() {
	return isset($_SESSION['user_id']);
}

function get_loginpage($default=null) {
	$loginpage = Session::Get('loginpage', true);
	if ($loginpage)  return $loginpage;
	if ($default) return $default;
	return WEB_ROOT . '/index.php';
}

function cookie_city($city) {
	global $hotcities;
	if($city) { 
		cookieset('city', $city['id']);
		return $city;
	} 
	$city_id = cookieget('city'); 
	$city = Table::Fetch('category', $city_id);
	if (!$city) $city = get_city();
	if (!$city) $city = array_shift($hotcities);
	if ($city) return cookie_city($city);
	return $city;
}

function ename_city($ename=null) {
	return DB::LimitQuery('category', array(
		'condition' => array(
			'zone' => 'city',
			'ename' => $ename,
		),
		'one' => true,
	));
}

function cookieset($k, $v, $expire=0) {
	$pre = substr(md5($_SERVER['HTTP_HOST']),0,4);
	$k = "{$pre}_{$k}";
	if ($expire==0) {
		$expire = time() + 365 * 86400;
	} else {
		$expire += time();
	}
	setCookie($k, $v, $expire, '/');
}

function cookieget($k, $default='') {
	$pre = substr(md5($_SERVER['HTTP_HOST']),0,4);
	$k = "{$pre}_{$k}";
	return isset($_COOKIE[$k]) ? strval($_COOKIE[$k]) : $default;
}

function is_decimal( $val ){
    return is_numeric( $val ) && floor( $val ) != $val;
}

function moneyit($k, $frontend = false) {
    if($frontend == true){
        if(is_decimal( $k )){
            return rtrim(rtrim(sprintf('%.2f',$k)), '.');
        }
        else{
            return floor($k).'.-';
        }
    }
    else{
        return rtrim(rtrim(sprintf('%.2f',$k), '0'), '.');
    }
}

function debug($v, $e=false) {
	global $login_user_id;
	if ($login_user_id==100000) {
		echo "<pre>";
		var_dump( $v);
		if($e) exit;
	}
}

function getparam($index=0, $default=0) {
	if (is_numeric($default)) {
		$v = abs(intval($_GET['param'][$index]));
	} else $v = strval($_GET['param'][$index]);
	return $v ? $v : $default;
}
function getpage() {
	$c = abs(intval($_GET['page']));
	return $c ? $c : 1;
}
function pagestring($count, $pagesize, $wap=false) {
	$p = new Pager($count, $pagesize, 'page');
	if ($wap) {
		return array($pagesize, $p->offset, $p->genWap());
	}
	return array($pagesize, $p->offset, $p->genBasic());
}

function uencode($u) {
	return base64_encode(urlEncode($u));
}
function udecode($u) {
	return urlDecode(base64_decode($u));
}
function share_facebook($team) {
	global $login_user_id;
	global $INI;
	if ($team)  {
		$query = array(
				'u' => $INI['system']['wwwprefix'] . "/team.php?id={$team['id']}&r={$login_user_id}",
				't' => $team['title'],
				);
	}
	else {
		$query = array(
				'u' => $INI['system']['wwwprefix'] . "/r.php?r={$login_user_id}",
				't' => $INI['system']['sitename'] . '(' .$INI['system']['wwwprefix']. ')',
				);
	}

	$query = http_build_query($query);
	return 'http://www.facebook.com/sharer.php?'.$query;
}

/* twitter @Harry */
function share_twitter($team) {
	global $login_user_id;
	global $INI;
	if ($team)  {
		$query = array(
				'status' => $INI['system']['wwwprefix'] . "/team.php?id={$team['id']}&r={$login_user_id}" . ' ' . $team['title'],
				);
	}
	else {
		$query = array(
				'status' => $INI['system']['wwwprefix'] . "/r.php?r={$login_user_id}" . ' ' . $INI['system']['sitename'] . '(' .$INI['system']['wwwprefix']. ')',
				);
	}

	$query = http_build_query($query);
	return 'http://twitter.com/?'.$query;
}

/* share link */
function share_renren($team) {
	global $login_user_id;
	global $INI;
	if ($team)  {
		$query = array(
				'link' => $INI['system']['wwwprefix'] . "/team.php?id={$team['id']}&r={$login_user_id}",
				'title' => $team['title'],
				);
	}
	else {
		$query = array(
				'link' => $INI['system']['wwwprefix'] . "/r.php?r={$login_user_id}",
				'title' => $INI['system']['sitename'] . '(' .$INI['system']['wwwprefix']. ')',
				);
	}

	$query = http_build_query($query);
	return 'http://share.renren.com/share/buttonshare.do?'.$query;
}

function share_kaixin($team) {
	global $login_user_id;
	global $INI;
	if ($team)  {
		$query = array(
				'rurl' => $INI['system']['wwwprefix'] . "/team.php?id={$team['id']}&r={$login_user_id}",
				'rtitle' => $team['title'],
				'rcontent' => strip_tags($team['summary']),
				);
	}
	else {
		$query = array(
				'rurl' => $INI['system']['wwwprefix'] . "/r.php?r={$login_user_id}",
				'rtitle' => $INI['system']['sitename'] . '(' .$INI['system']['wwwprefix']. ')',
				'rcontent' => $INI['system']['sitename'] . '(' .$INI['system']['wwwprefix']. ')',
				);
	}
	$query = http_build_query($query);
	return 'http://www.kaixin001.com/repaste/share.php?'.$query;
}

function share_douban($team) {
	global $login_user_id;
	global $INI;
	if ($team)  {
		$query = array(
				'url' => $INI['system']['wwwprefix'] . "/team.php?id={$team['id']}&r={$login_user_id}",
				'title' => $team['title'],
				);
	}
	else {
		$query = array(
				'url' => $INI['system']['wwwprefix'] . "/r.php?r={$login_user_id}",
				'title' => $INI['system']['sitename'] . '(' .$INI['system']['wwwprefix']. ')',
				);
	}
	$query = http_build_query($query);
	return 'http://www.douban.com/recommend/?'.$query;
}

function share_sina($team) {
	global $login_user_id;
	global $INI;
	if ($team)  {
		$query = array(
				'url' => $INI['system']['wwwprefix'] . "/team.php?id={$team['id']}&r={$login_user_id}",
				'title' => $team['title'],
				);
	}
	else {
		$query = array(
				'url' => $INI['system']['wwwprefix'] . "/r.php?r={$login_user_id}",
				'title' => $INI['system']['sitename'] . '(' .$INI['system']['wwwprefix']. ')',
				);
	}
	$query = http_build_query($query);
	return 'http://v.t.sina.com.cn/share/share.php?'.$query;
}

function share_mail($team) {
	global $login_user_id;
	global $INI;
	if (!$team) {
		$team = array(
				'title' => $INI['system']['sitename'] . '(' . $INI['system']['wwwprefix'] . ')',
				);
	}
	$pre[] = "Found a good site--{$INI['system']['sitename']}，Every day is a New deal!";
	if ( $team['id'] ) {
		$pre[] = "Customers today are:{$team['title']}";
		$pre[] = "I think you will be interested in:";
		$pre[] = $INI['system']['wwwprefix'] . "/team.php?id={$team['id']}&r={$login_user_id}";
		$pre = mb_convert_encoding(join("\n\n", $pre), 'GBK', 'UTF-8');
		$sub = "You are interested in：{$team['title']}";
	} else {
		$sub = $pre[] = $team['title'];
	}
	$sub = mb_convert_encoding($sub, 'GBK', 'UTF-8');
	$query = array( 'subject' => $sub, 'body' => $pre, );
	$query = http_build_query($query);
	return 'mailto:?'.$query;
}

function domainit($url) {
	if(strpos($url,'//')) { preg_match('#[//]([^/]+)#', $url, $m);
} else { preg_match('#[//]?([^/]+)#', $url, $m); }
return $m[1];
}

// that the recursive feature on mkdir() is broken with PHP 5.0.4 for
function RecursiveMkdir($path) {
	if (!file_exists($path)) {
		RecursiveMkdir(dirname($path));
		@mkdir($path, 0777);
	}
}

function upload_image($input, $image=null, $type='team', $scale=false) {
	$year = date('Y'); $day = date('md'); $n = time().rand(1000,9999).'.jpg';
	$z = $_FILES[$input];
	if ($z && strpos($z['type'], 'image')===0 && $z['error']==0) {
		if (!$image) { 
			RecursiveMkdir( IMG_ROOT . '/' . "{$type}/{$year}/{$day}" );
			$image = "{$type}/{$year}/{$day}/{$n}";
			$path = IMG_ROOT . '/' . $image;
		} else {
			RecursiveMkdir( dirname(IMG_ROOT .'/' .$image) );
			$path = IMG_ROOT . '/' .$image;
		}
		if ($type=='user') {
			Image::Convert($z['tmp_name'], $path, 48, 48, Image::MODE_CUT);
		} 
		else if($type=='team') {
			move_uploaded_file($z['tmp_name'], $path);
		}
                else if($type=='team_desc') {
			move_uploaded_file($z['tmp_name'], $path);
		}
		if($type=='team' && $scale) {
			$npath = preg_replace('#(\d+)\.(\w+)$#', "\\1_index.\\2", $path); 
			//Image::Convert($path, $npath, 348, 141, Image::MODE_CUT);
            //Image::Convert($path, $npath, 348, 141, Image::MODE_SCALE);
			$simple = new SimpleImage(); 
			$simple->load($path);			
			$simple->resize(460,307); 
			$simple->save($npath, IMAGETYPE_PNG);
		}
		if($type=='team_desc' && $scale) {
			$npath = preg_replace('#(\d+)\.(\w+)$#', "\\1_index.\\2", $path); 
			//Image::Convert($path, $npath, 348, 141, Image::MODE_CUT);
            //Image::Convert($path, $npath, 638, 273, Image::MODE_SCALE);
			
			$simple = new SimpleImage(); 
			$simple->load($path);			
			$simple->resize(638,273); 
			$simple->save($npath, IMAGETYPE_PNG);
		}
		return $image;
	} 
	return $image;
}
if ( ! function_exists('user_image'))
{
function user_image($image=null) {
	global $INI;
	if (!$image) { 
		return $INI['system']['imgprefix'] . '/static/images/avatar-bg.jpg';
	}
	return $INI['system']['imgprefix'] . '/static/' .$image;
}
}

function team_image($image=null, $index=false) {
	global $INI;
	if (!$image) return null;
	if ($index) {
		$path = WWW_ROOT . '/static/' . $image;
		$image = preg_replace('#(\d+)\.(\w+)$#', "\\1_index.\\2", $image); 
		$dest = WWW_ROOT . '/static/' . $image;
		if (!file_exists($dest) && file_exists($path) ) {
			//Image::Convert($path, $dest, 200, 120, Image::MODE_SCALE);
			//Image::Convert($path, $dest, 348, 141, Image::MODE_SCALE);
                    Image::Convert($path, $dest, 456, 300, Image::MODE_SCALE);
		}
	}
	return $INI['system']['imgprefix'] . '/static/' .$image;
}

function userreview($content) {
	$line = preg_split("/[\n\r]+/", $content, -1, PREG_SPLIT_NO_EMPTY);
	$r = '<ul>';
	foreach($line AS $one) {
		$c = explode('|', htmlspecialchars($one));
		$c[2] = $c[2] ? $c[2] : '/';
		$r .= "<li>{$c[0]}<span>－－<a href=\"{$c[2]}\" target=\"_blank\">{$c[1]}</a>";
		$r .= ($c[3] ? "（{$c[3]}）":'') . "</span></li>\n";
	}
	return $r.'</ul>';
}

function invite_state($invite) {
	if ('Y' == $invite['pay']) return 'Rebate paid';
	if ('C' == $invite['pay']) return 'Unapproved';
	if ('N' == $invite['pay'] && $invite['buy_time']) return 'Rebate to be paid';
	if (time()-$invite['create_time']>7*86400) return 'Expired';
	return 'Not bought';
}

function team_state(&$team) {
	if ( $team['now_number'] >= $team['min_number'] ) {
		if ($team['max_number']>0) {
			if ( $team['now_number']>=$team['max_number'] ){
				if ($team['close_time']==0) {
					$team['close_time'] = $team['end_time'];
				}
				return $team['state'] = 'soldout';
			}
		}
		if ( $team['end_time'] <= time() ) {
			$team['close_time'] = $team['end_time'];
		}
		return $team['state'] = 'success';
	} else {
		if ( $team['end_time'] <= time() ) {
			$team['close_time'] = $team['end_time'];
			return $team['state'] = 'failure';
		}
	}
	return $team['state'] = 'none';
}

function is_deal_success(&$team) {
    return in_array(team_state($team), array('success', 'soldout'));
}

function current_team($city_id=0, $type='normal', $home_pg='',$isSunday='') {
	
	$today = strtotime(date('Y-m-d H:i'));
	
	$cond = array(
			//'city_id' => array(0, abs(intval($city_id))),
			'team_type' => $type,
			"begin_time <= {$today}",
			"end_time > {$today}",
            "featured" => '1',
			"system" => 'Y'				
			);
	$order = 'ORDER BY sort_order DESC, begin_time DESC, id DESC';

	if ( $home_pg == 'home_pg' && !empty($isSunday) )
	{
		# Ref: https://notableapp.com/posts/1081897
		
		$last_six_days = strtotime('-6 days');
		/*$sql = "select * from team where team_type = '".$type."' 
				and begin_time < {$today}
				and featured = 1 and system = 'Y'
				$order
				LIMIT 0,6";*/
		$sql = "select * from team where team_type = '".$type."' 
				and begin_time < {$today}
				and featured = 1 and system = 'Y'
				ORDER BY id DESC
				LIMIT 0,6";
		$team = DB::GetQueryResult($sql, false);
	} else {
		/* normal team */
		$team = DB::LimitQuery('team', array(
					'condition' => $cond,
					'one' => true,
					'order' => $order,
					));
	}#echo'<pre>';print_r($sql);exit;
	
	if ($team) return $team;

	/* seconds team */
	$cond['team_type'] = 'seconds';
	unset($cond['begin_time']);	
	$order = 'ORDER BY sort_order DESC, begin_time ASC, id DESC';
        
	$team = DB::LimitQuery('team', array(
				'condition' => $cond,
				'one' => true,
				'order' => $order,
				));

	return $team;
}

function current_invitenwin() {
	$today = strtotime(date('Y-m-d H:i'));
	$cond = array(
			
			"start_time <= {$today}",
			"end_time > {$today}",
                        "status = 1",
			);
	$order = 'ORDER BY start_time DESC, id DESC';

	/* normal team */
	$invitenwin = DB::LimitQuery('invitecontest', array(
				'condition' => $cond,
				'one' => true,
				'order' => $order,
				));
	if ($invitenwin) 
            return $invitenwin;

	return false;
}

function state_explain($team, $error='false') {
	$state = team_state($team);
	$state = strtolower($state);
	switch($state) {
		case 'none': return 'Ongoing';
		case 'soldout': return 'Sold out';
		case 'failure': if($error) return 'Hype failed';
		case 'success': return 'Hype succeeded';
		default: return 'Ended';
	}
}

function get_zones($zone=null) {
	$zones = array(
			'city' => 'City',
			'group' => 'Hype Category',
			'public' => 'Forum Category',
			'grade' => 'User Grade',
			'express' => 'Express',
			'partner' => 'Biz Category',
			);
	if ( !$zone ) return $zones;
	if (!in_array($zone, array_keys($zones))) {
		$zone = 'city';
	}
	return array($zone, $zones[$zone]);
}

function down_xls($data, $keynames, $name='dataxls', $metadata=false) {
	$xls[] = "<html><meta http-equiv=content-type content=\"text/html; charset=UTF-8\"><body><table border='1'>";
        if ($metadata)
        {
            foreach ($metadata as $mdk=>$mdv)
            {
                $xls[]="<tr><td>".$mdk."</td><td colspan='".sizeof($keynames)."'>".$mdv."</td></tr>";
            }
        }
	$xls[] = "<tr><td>ID</td><td>" . implode("</td><td>", array_values($keynames)) . '</td></tr>';
	foreach($data As $o) {
		$line = array(++$index);
		foreach($keynames AS $k=>$v) {
			$line[] = $o[$k];
		}
                if ( isset($o['##bgcolor'])) 
                    $xls[] = '<tr bgcolor="'.$o['##bgcolor'].'">';
                else
                    $xls[] = '<tr>';
                $xls[] = '<td>'. implode("</td><td>", $line) . '</td></tr>';
	}
	$xls[] = '</table></body></html>';
	$xls = join("\r\n", $xls);
	header('Content-Disposition: attachment; filename="'.$name.'.xls"');
	die(mb_convert_encoding($xls,'UTF-8','UTF-8'));
}

function option_hotcategory($zone='city', $force=false, $all=false) {
	$cates = option_category($zone, $force, true);
	$r = array();
	foreach($cates AS $id=>$one) {
		if ('Y'==strtoupper($one['display'])) $r[$id] = $one;
	}
	return $all ? $r: Utility::OptionArray($r, 'id', 'name');
}

function option_category($zone='city', $force=false, $all=false) {
	$cache = $force ? 0 : 86400*30;
	$cates = DB::LimitQuery('category', array(
		'condition' => array( 'zone' => $zone, ),
		'order' => 'ORDER BY sort_order DESC, id DESC',
		'cache' => $cache,
	));
	$cates = Utility::AssColumn($cates, 'id');
	return $all ? $cates : Utility::OptionArray($cates, 'id', 'name');
}

function option_yes($n, $default=false) {
	global $INI;
	if (false==isset($INI['option'][$n])) return $default;
	$flag = trim(strval($INI['option'][$n]));
	return abs(intval($flag)) || strtoupper($flag) == 'Y';
}

function option_yesv($n, $default='N') {
	return option_yes($n, $default=='Y') ? 'Y' : 'N';
}

function magic_gpc($string) {
	if(SYS_MAGICGPC) {
		if(is_array($string)) {
			foreach($string as $key => $val) {
				$string[$key] = magic_gpc($val);
			}
		} else {
			$string = stripslashes($string);
		}
	}
	return $string;
}

function team_discount($team, $save=false) {
	if ($team['market_price']<0 || $team['team_price']<0 ) {
		return '?';
	}
	return moneyit((10*$team['team_price']/$team['market_price']));
}

function team_origin($team, $quantity=0) {
	$origin = $quantity * $team['team_price'];
	if ($team['delivery'] == 'express'
			&& ($team['farefree']==0 || $quantity < $team['farefree'])
		) {
			$origin += $team['fare'];
		}
	return $origin;
}

function error_handler($errno, $errstr, $errfile, $errline) {
	switch ($errno) {
		case E_PARSE:
		case E_ERROR:
			echo "<b>Fatal ERROR</b> [$errno] $errstr<br />\n";
			echo "Fatal error on line $errline in file $errfile";
			echo "PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
			exit(1);
			break;
		default: break;
	}
	return true;
}
/* for obscureid */
function obscure_rep($u) {
	if(!option_yes('encodeid')) return $u;
	if(preg_match('#/manage/#', $_SERVER['REQUEST_URI'])) return $u;
	return preg_replace_callback('#(\?|&)id=(\d+)(\b)#i', obscure_cb, $u);
}
function obscure_did() {
	$gid = strval($_GET['id']);
	if ($gid && strpos($gid, 'WR')===0) {
		$_GET['id'] = base64_decode(substr($gid,2))>>2;
	}
}
function obscure_cb($m) {
	$eid = obscure_eid($m[2]);
	return "{$m[1]}id={$eid}{$m[3]}";
}
function obscure_eid($id) {
	return 'WR'.base64_encode($id<<2);
}
obscure_did();
/* end */

/* for post trim */
function trimarray($o) {
	if (!is_array($o)) return trim($o);
	foreach($o AS $k=>$v) { $o[$k] = trimarray($v); }
	return $o;
}

function deal_box(&$one){
    return render('content/content_deal_box');
}

function print_r_r($var){
    echo '<pre>';
        print_r($var);
    echo '</pre>';
}
$_POST = trimarray($_POST);
/* end */
function curPageName() {
 return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);
}
set_error_handler('error_handler');


//orderCashBackItem
function orderCashBackItem($orderId){
	
	if(!empty($orderId)){
		
		$sql = "SELECT 
					id,
					team_id,
					option_id,
					quantity
				FROM 
					order_item 
				WHERE
				 order_id = $orderId";
				
        $sqlData = DB::GetQueryResult($sql , false);
		
		
		//IF ORDER INFO NOT EMPTY
		if(!empty($sqlData)){
			//DEFAULT CACH BACK AMOUNT
			$customerCashBackAmount	=	0;
			
			foreach($sqlData as $sqlDataRow){
				
				$order_item_id	=	$sqlDataRow['id'];
				$team_id		=	$sqlDataRow['team_id'];
				$option_id		=	$sqlDataRow['option_id'];
				$quantity		=	$sqlDataRow['quantity'];
				//if option id is not empty then check team_multi table else on team table

				if(!empty($team_id) && $team_id != NULL && $team_id !=""){
					
					$sqlOption	=	"SELECT is_cash_back_allowed,cb_amount,cb_start_time,cb_end_time FROM team WHERE id = $team_id";
				}
				
				if(!empty($option_id) && $option_id != NULL && $option_id !=""){
					
					//CHECK OPTION HASE cb_amount OR NOT
					// $sqlOption	=	"SELECT is_cash_back_allowed,cb_amount,cb_start_time,cb_end_time FROM team_multi WHERE id = $option_id";
					$sqlOptionQry	=	"SELECT is_cash_back_allowed,cb_amount,cb_start_time,cb_end_time FROM team_multi WHERE id = $option_id";
					$sqlOptionData 		= DB::GetQueryResult($sqlOptionQry , false);
					
					$is_cash_back_allowed	=	false;
					$cb_amount				=	0;

					if(!empty($sqlOptionData)&&isset($sqlOptionData[0]['is_cash_back_allowed'])){
						 $is_cash_back_allowed 	= $sqlOptionData[0]['is_cash_back_allowed'];
						 $cb_amount 			= $sqlOptionData[0]['cb_amount'];
					}
					//IF CHILD HAS CASH BACK AMOUNT THEN CHILD CB VALUE WILL BE USED 
					if($is_cash_back_allowed && $cb_amount>=0){
						 $sqlOption	=	"SELECT is_cash_back_allowed,cb_amount,cb_start_time,cb_end_time FROM team_multi WHERE id = $option_id";
					}
					
				}
				
				
				//GET DATA BY TEAM ID OR OPTION ID
				 $sqlTeamData = DB::GetQueryResult($sqlOption , false);
				 if(!empty($sqlTeamData)){
					 
					 
					 
					 foreach($sqlTeamData as $sqlTeamDataRow){
						 
						 $is_cash_back_allowed 	= $sqlTeamDataRow['is_cash_back_allowed'];
						 $cb_amount 			= $sqlTeamDataRow['cb_amount'];
						 $cb_start_time 		= $sqlTeamDataRow['cb_start_time'];
						 $cb_end_time 			= $sqlTeamDataRow['cb_end_time'];
						 
						 //SET CASHBACK AMOUNT FOR EACH ITEM
						 $itemCashbackAmount	=	0;
						 
						 //CHECK Cash BAck allowed
						 if($is_cash_back_allowed == 1 && $cb_amount > 0 && !empty($cb_start_time)){
							 
							 $cb_start_timeFormated	=	date("Y-m-d",$cb_start_time);
							 $currentDate			=	date("Y-m-d");
							 
							 
							 $cb_end_timeFormated	=	"";
							 //FORMAT END DATE
							 if(!empty($cb_end_time)){
								  $cb_end_timeFormated	=	date("Y-m-d",$cb_end_time);
							 }
							 
							 
							 //DATE EXPIRY VALIDATION
							 //CHECK IF END DATE ARE NOT SET THEN CHECK ONLY START DATE ELSE CHECK FOR START & END DATE RANGE
							 if($cb_end_timeFormated==""){
								 
								 if($currentDate >= $cb_start_timeFormated){
									 $customerCashBackAmount	+= $cb_amount * $quantity;
									 $itemCashbackAmount		=	$cb_amount * $quantity;
								 }
							 }else{
								 
								 //EXPIRY
								 if($currentDate >= $cb_start_timeFormated && $currentDate <= $cb_end_timeFormated){
									 $customerCashBackAmount	+= $cb_amount * $quantity;
									 $itemCashbackAmount		=	$cb_amount * $quantity;
								 }
							 }
							 
							 //SET CASHBACK AMOUN FOR EACH ITEM
							 $orderItemCashBackSql	=	<<<SQL
														UPDATE
																`order_item`
														SET
															`cashback_amount`	=	'$itemCashbackAmount'
														WHERE
															`id`		=	'$order_item_id'
SQL;
							DB::Query($orderItemCashBackSql);
							 // echo "Current Date->".$currentDate."<br/>Start->".$cb_start_timeFormated."<br/>End->".$cb_end_timeFormated."<br/>".$customerCashBackAmount;
						 }
						 
					 }
				 }
				 
				 
				 // echo "<pre>";print_r($sqlTeamData);
				
			}
			
			
			//IF CASH BACK AMOUNT IS GREATER THAN ZERO INSERT RECORD
			if($customerCashBackAmount > 0 ){
				if(isset($_SESSION['user_id'])){
					
					$loginUserId	= $_SESSION['user_id'];
					if ( $loginUserId <= 0 ) return 0;
					
					//update user money;
					$user = Table::Fetch('user', $loginUserId);
					Table::UpdateCache('user', $loginUserId, array(
						'money' => array( "money + {$customerCashBackAmount}" ),
					));
					
					$u = array(
						'user_id' => $loginUserId,
						'money' => $customerCashBackAmount,
						'direction' => 'income',
						'action' => 'cash_back',
						'detail_id' => $orderId,
						'create_time' => time(),
						);
				   DB::Insert('flow', $u);
				   

				}
			}
			
		}
		
	}
}
#check if deal history is exist or not
function checkDealPriceHistory($teamId=0,$optionId=0)
{
	 $optionCondition = " AND option_id is NULL";
	 if($optionId)
	 {
		$optionCondition = " AND option_id ='$optionId' " ;
	 
	 }
	 
	 
	 
	 $priceHistoryNotExist = false;
	 $totalcount =0;
	 $sql	=	"select count(id) as totalCount from team_price_history where team_id ='$teamId' $optionCondition ";
	 $result = DB::GetQueryResult($sql , false);
	 if(is_array($result) && count($result) >0)
	 {
		$result = $result[0];
		$totalcount = $result['totalcount'];
	 }
	 
	 #check price history not exist
	 if($totalcount==0)
	 {
		$priceHistoryNotExist = true;
	 }
	
	return $priceHistoryNotExist; 
}


function insertDealPriceHistory($eteam=array(),$team=array(),$login_user_id=0,$multiDeal =false)
{
	$team_price = $market_price = $partner_revenue = 0;  
	//first get data w.r.t to team id
	if(is_array($eteam) && count($eteam)>0)
	{
		$team_price 		= $eteam ['team_price'];
		$market_price 		= $eteam ['market_price'];
		$partner_revenue 	= $eteam ['partner_revenue'];
	}	
	
	
	//check if there is change in any fields
	$addHistoryDetail = false;
	if($team_price != $team['team_price'])
	{
		$addHistoryDetail = true;
	}
	else if($market_price!=$team['market_price'])
	{
		$addHistoryDetail = true;
	}
	else if($partner_revenue !=$team['partner_revenue'] )
	{
		$addHistoryDetail = true;
	}
	
	$optionId = null;
	$team_id  = $team['id'];
	if($multiDeal)
	{
		$optionId = $team['id'];
		$team_id  = $team['team_id'];
	}
	
	#check if deal history is exist or not
	$priceHistoryNotExist = checkDealPriceHistory($team_id,$optionId);
	if($priceHistoryNotExist)
	{
		$addHistoryDetail = true;
	}
	
	
	if($addHistoryDetail)
	{
		// Adding Stock History
		$team_price_history = array ();
		$team_price_history['team_id'] = $team_id;
		$team_price_history['user_id'] = $login_user_id;
		$team_price_history['option_id'] = $optionId;
		$team_price_history['team_price'] = $team['team_price'];
		$team_price_history['market_price'] = $team['market_price'];
		$team_price_history['partner_revenue'] = $team['partner_revenue'];
		$team_price_history['create_time'] = time();
		
		ZTeam::addDealPriceHistory($team_price_history);
	} 
	return true;
}




function getDateTimeByTimestamp($timestamp='')
{
	$dateTime = '';
	if($timestamp)
	{
		$dateTime =date('d-m-Y H:i:s',$timestamp);
	}
	return $dateTime;
}


//RESET BASKET AFTER ORDER SUCCESS
function resetBasketAfterOrderPaid(){
	
	if(isset($_SESSION['user_id'])){
		
		$loginUserId	= $_SESSION['user_id'];
		
		$basket = new Basket($loginUserId);
		$basket->ClearBasket();
	}
}

/*
* Add wildcard to word
*/
function wildcarded($words) 
{
	$words = singleQuoteReplaceWithDoubleQuote($words);
	$arr = preg_split("/[\s,]+/", $words);
	$arr = array_filter($arr);

	$q=false;
	$str="";
	foreach ($arr as $word) { 
	if (strpos($word,"\"")!==False) $q = !$q;
	$str.= $word . ($q ? " " : "* ");
	}
	return $str;
}

/*
* Single quote replace double quote
*/
function singleQuoteReplaceWithDoubleQuote($string='') 
{
	$string = str_replace("'", "''", $string);
	return $string;
}


#revenue tab query condition
function revenueQueryCondition($param=array())
{
	$queryCondition = "";
	if(!empty($param))
	{
		$condition 		= array();
		#deal search
		$title 			= '';
		$partnerTitle 	= '';
		$supplier_id 	= 0;
		
		if(!empty($_POST['title']))
		{
			$title = $_POST['title'];
			
			#call Partial search method and pass colmun names
			$columnNames = array(
								'id'				=> 't.id',
								'title'				=> 't.title',			
								);
			$condition[] =  PartialSearch::partialSearchQuery($columnNames,$title);
		}
		
		#partner
		if(!empty($_POST['partner']))
		{
			$partnerTitle = $_POST['partner'];
			
			#call Partial search method and pass colmun names
			$columnNames = array(
								'title'				=> 'p.title',			
								);
			$condition[] = PartialSearch::partialSearchQuery($columnNames,$partnerTitle);
			
		}
		
		# supplier
		if(!empty($_POST['supplier_id']))
		{
			$supplier_id = $_POST['supplier_id'];
			$condition[] = " t.supplier_id ='$supplier_id' ";
		}
		
		#query conditions
		if(is_array($condition) && count($condition)>0)
		{
			$queryCondition =  " WHERE " .implode(" AND ",$condition);;
		}
	}
	
	return $queryCondition;
}

function cashBackCredit($team=array())
{
	
	$credit = 0;
	$nowTime = time();
	$isCashBackAmountPositive = false;
	
	#check cashback amount is positive
	if(is_numeric($team['cb_amount']) && $team['cb_amount']>0)
	{
		$isCashBackAmountPositive= true;
	}
	
	#check cashback amount is positive and start time is less than now time and  end time is greater than now time
	if($isCashBackAmountPositive && $team['cb_start_time']<$nowTime && $team['cb_end_time']>$nowTime)
	{
		$credit = $team['cb_amount'];
	}

	return $credit;
}

function cashBackLeftTime($team=array())
{
	$nowTime = time();
	$timeLeft =$timeLeftInUnixStamp = false;
	if($team['cb_end_time']>$nowTime)
	{
		$timeLeftInUnixStamp= $team['cb_end_time']-$nowTime;
	}
	
	if($timeLeftInUnixStamp)
	{
		$hours      = floor($timeLeftInUnixStamp /3600);     
		$minutes    = intval(($timeLeftInUnixStamp/60) % 60);        
		$seconds    = intval($timeLeftInUnixStamp % 60); 
		
		$timeLeft = $hours."h : ".$minutes."min : ".$seconds."s";
	}
	
	return $timeLeft;
	
}


function cashBackText($cbAmount=0)
{
	$text ="CHF ".moneyit($cbAmount, true)." de credit back";
	echo $text;
}

function getDayFromDate()
{
	$now_day = date('l', time());
	return $now_day;
}

function isSunday()
{
	$isSunday = false;

	$today = getDayFromDate();
	if($today =="Sunday")
	{
		 $isSunday = true;
	}
	
	return $isSunday;
}

function isParentOption($optionId=0)
{
	$count = ZTeam::isParentOptionCount($optionId);
	
	if($count>0)
	{
		return true;
	}
	else
	{
		return false;
	}
}