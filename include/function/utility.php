<?php
define('VER','4.5');

function get_city($ip=null) {
	$cities = option_category('city', false, true);
	$ip = ($ip) ? $ip : Utility::GetRemoteIP();
	$url = "http://open.baidu.com/ipsearch/s?wd={$ip}&tn=baiduip";
	$res = mb_convert_encoding(Utility::HttpRequest($url), 'UTF-8', 'GBK');
	if ( preg_match('#From：<b>(.+)</b>#Ui', $res, $m) ) {
		foreach( $cities AS $one) {
			if ( FALSE !== strpos($m[1], $one['name']) ) {
				return $one;
			}
		}
	}
	return array();
}

function mail_zd($email) {
	global $option_mail;
	if ( ! Utility::ValidEmail($email) ) return false;
	preg_match('#@(.+)$#', $email, $m);
	$suffix = strtolower($m[1]);
	return $option_mail[$suffix];
}

function nanooption($string) {
	if ( preg_match_all('#{(.+)}#U', $string, $m) ){
		return $m[1];
	}
	return array();
}


global $current_deal_day;
$current_deal_day = array(
    'Monday' => "LE LUNDI, C'EST TOPMAISON", 
    'Tuesday' => "LE MARDI, C'EST TOPGOURMET",  
    'Wednesday' => "LE MERCREDI, C'EST TOPBABY",
    'Thursday' => "LE JEUDI, C'EST TOPZEN", 
    'Friday'=>"LE VENDREDI, C'EST TOPFUN", 
    'Saturday'=>"LE SAMEDI, C'EST TOPKIDS",
);

function current_deal_day(){
    global $current_deal_day;
    $now_day = date('l', time());
    if(isset($current_deal_day[$now_day])){
        return $current_deal_day[$now_day];
    }
    else{
        return false;
    }
}

global $striped_field;
$striped_field = array(
	'username',
	'realname',
	'name',
	'tilte',
	'email',
	'address',
	'mobile',
	'url',
	'logo',
	'contact',
);

function GetCountries(&$countries)
{
    $countries = array(
		'AF'=>'AFGHANISTAN',
		'AL'=>'ALBANIA',
		'DZ'=>'ALGERIA',
		'AS'=>'AMERICAN SAMOA',
		'AD'=>'ANDORRA',
		'AO'=>'ANGOLA',
		'AI'=>'ANGUILLA',
		'AQ'=>'ANTARCTICA',
		'AG'=>'ANTIGUA AND BARBUDA',
		'AR'=>'ARGENTINA',
		'AM'=>'ARMENIA',
		'AW'=>'ARUBA',
		'AU'=>'AUSTRALIA',
		'AT'=>'AUSTRIA',
		'AZ'=>'AZERBAIJAN',
		'BS'=>'BAHAMAS',
		'BH'=>'BAHRAIN',
		'BD'=>'BANGLADESH',
		'BB'=>'BARBADOS',
		'BY'=>'BELARUS',
		'BE'=>'BELGIUM',
		'BZ'=>'BELIZE',
		'BJ'=>'BENIN',
		'BM'=>'BERMUDA',
		'BT'=>'BHUTAN',
		'BO'=>'BOLIVIA',
		'BA'=>'BOSNIA AND HERZEGOVINA',
		'BW'=>'BOTSWANA',
		'BV'=>'BOUVET ISLAND',
		'BR'=>'BRAZIL',
		'IO'=>'BRITISH INDIAN OCEAN TERRITORY',
		'BN'=>'BRUNEI DARUSSALAM',
		'BG'=>'BULGARIA',
		'BF'=>'BURKINA FASO',
		'BI'=>'BURUNDI',
		'KH'=>'CAMBODIA',
		'CM'=>'CAMEROON',
		'CA'=>'CANADA',
		'CV'=>'CAPE VERDE',
		'KY'=>'CAYMAN ISLANDS',
		'CF'=>'CENTRAL AFRICAN REPUBLIC',
		'TD'=>'CHAD',
		'CL'=>'CHILE',
		'CN'=>'CHINA',
		'CX'=>'CHRISTMAS ISLAND',
		'CC'=>'COCOS (KEELING) ISLANDS',
		'CO'=>'COLOMBIA',
		'KM'=>'COMOROS',
		'CG'=>'CONGO',
		'CD'=>'CONGO, THE DEMOCRATIC REPUBLIC OF THE',
		'CK'=>'COOK ISLANDS',
		'CR'=>'COSTA RICA',
		'CI'=>'COTE D IVOIRE',
		'HR'=>'CROATIA',
		'CU'=>'CUBA',
		'CY'=>'CYPRUS',
		'CZ'=>'CZECH REPUBLIC',
		'DK'=>'DENMARK',
		'DJ'=>'DJIBOUTI',
		'DM'=>'DOMINICA',
		'DO'=>'DOMINICAN REPUBLIC',
		'TP'=>'EAST TIMOR',
		'EC'=>'ECUADOR',
		'EG'=>'EGYPT',
		'SV'=>'EL SALVADOR',
		'GQ'=>'EQUATORIAL GUINEA',
		'ER'=>'ERITREA',
		'EE'=>'ESTONIA',
		'ET'=>'ETHIOPIA',
		'FK'=>'FALKLAND ISLANDS (MALVINAS)',
		'FO'=>'FAROE ISLANDS',
		'FJ'=>'FIJI',
		'FI'=>'FINLAND',
		'FR'=>'FRANCE',
		'GF'=>'FRENCH GUIANA',
		'PF'=>'FRENCH POLYNESIA',
		'TF'=>'FRENCH SOUTHERN TERRITORIES',
		'GA'=>'GABON',
		'GM'=>'GAMBIA',
		'GE'=>'GEORGIA',
		'DE'=>'GERMANY',
		'GH'=>'GHANA',
		'GI'=>'GIBRALTAR',
		'GR'=>'GREECE',
		'GL'=>'GREENLAND',
		'GD'=>'GRENADA',
		'GP'=>'GUADELOUPE',
		'GU'=>'GUAM',
		'GT'=>'GUATEMALA',
		'GN'=>'GUINEA',
		'GW'=>'GUINEA-BISSAU',
		'GY'=>'GUYANA',
		'HT'=>'HAITI',
		'HM'=>'HEARD ISLAND AND MCDONALD ISLANDS',
		'VA'=>'HOLY SEE (VATICAN CITY STATE)',
		'HN'=>'HONDURAS',
		'HK'=>'HONG KONG',
		'HU'=>'HUNGARY',
		'IS'=>'ICELAND',
		'IN'=>'INDIA',
		'ID'=>'INDONESIA',
		'IR'=>'IRAN, ISLAMIC REPUBLIC OF',
		'IQ'=>'IRAQ',
		'IE'=>'IRELAND',
		'IL'=>'ISRAEL',
		'IT'=>'ITALY',
		'JM'=>'JAMAICA',
		'JP'=>'JAPAN',
		'JO'=>'JORDAN',
		'KZ'=>'KAZAKSTAN',
		'KE'=>'KENYA',
		'KI'=>'KIRIBATI',
		'KP'=>'KOREA DEMOCRATIC PEOPLES REPUBLIC OF',
		'KR'=>'KOREA REPUBLIC OF',
		'KW'=>'KUWAIT',
		'KG'=>'KYRGYZSTAN',
		'LA'=>'LAO PEOPLES DEMOCRATIC REPUBLIC',
		'LV'=>'LATVIA',
		'LB'=>'LEBANON',
		'LS'=>'LESOTHO',
		'LR'=>'LIBERIA',
		'LY'=>'LIBYAN ARAB JAMAHIRIYA',
		'LI'=>'LIECHTENSTEIN',
		'LT'=>'LITHUANIA',
		'LU'=>'LUXEMBOURG',
		'MO'=>'MACAU',
		'MK'=>'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF',
		'MG'=>'MADAGASCAR',
		'MW'=>'MALAWI',
		'MY'=>'MALAYSIA',
		'MV'=>'MALDIVES',
		'ML'=>'MALI',
		'MT'=>'MALTA',
		'MH'=>'MARSHALL ISLANDS',
		'MQ'=>'MARTINIQUE',
		'MR'=>'MAURITANIA',
		'MU'=>'MAURITIUS',
		'YT'=>'MAYOTTE',
		'MX'=>'MEXICO',
		'FM'=>'MICRONESIA, FEDERATED STATES OF',
		'MD'=>'MOLDOVA, REPUBLIC OF',
		'MC'=>'MONACO',
		'MN'=>'MONGOLIA',
		'MS'=>'MONTSERRAT',
		'MA'=>'MOROCCO',
		'MZ'=>'MOZAMBIQUE',
		'MM'=>'MYANMAR',
		'NA'=>'NAMIBIA',
		'NR'=>'NAURU',
		'NP'=>'NEPAL',
		'NL'=>'NETHERLANDS',
		'AN'=>'NETHERLANDS ANTILLES',
		'NC'=>'NEW CALEDONIA',
		'NZ'=>'NEW ZEALAND',
		'NI'=>'NICARAGUA',
		'NE'=>'NIGER',
		'NG'=>'NIGERIA',
		'NU'=>'NIUE',
		'NF'=>'NORFOLK ISLAND',
		'MP'=>'NORTHERN MARIANA ISLANDS',
		'NO'=>'NORWAY',
		'OM'=>'OMAN',
		'PK'=>'PAKISTAN',
		'PW'=>'PALAU',
		'PS'=>'PALESTINIAN TERRITORY, OCCUPIED',
		'PA'=>'PANAMA',
		'PG'=>'PAPUA NEW GUINEA',
		'PY'=>'PARAGUAY',
		'PE'=>'PERU',
		'PH'=>'PHILIPPINES',
		'PN'=>'PITCAIRN',
		'PL'=>'POLAND',
		'PT'=>'PORTUGAL',
		'PR'=>'PUERTO RICO',
		'QA'=>'QATAR',
		'RE'=>'REUNION',
		'RO'=>'ROMANIA',
		'RU'=>'RUSSIAN FEDERATION',
		'RW'=>'RWANDA',
		'SH'=>'SAINT HELENA',
		'KN'=>'SAINT KITTS AND NEVIS',
		'LC'=>'SAINT LUCIA',
		'PM'=>'SAINT PIERRE AND MIQUELON',
		'VC'=>'SAINT VINCENT AND THE GRENADINES',
		'WS'=>'SAMOA',
		'SM'=>'SAN MARINO',
		'ST'=>'SAO TOME AND PRINCIPE',
		'SA'=>'SAUDI ARABIA',
		'SN'=>'SENEGAL',
		'SC'=>'SEYCHELLES',
		'SL'=>'SIERRA LEONE',
		'SG'=>'SINGAPORE',
		'SK'=>'SLOVAKIA',
		'SI'=>'SLOVENIA',
		'SB'=>'SOLOMON ISLANDS',
		'SO'=>'SOMALIA',
		'ZA'=>'SOUTH AFRICA',
		'GS'=>'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS',
		'ES'=>'SPAIN',
		'LK'=>'SRI LANKA',
		'SD'=>'SUDAN',
		'SR'=>'SURINAME',
		'SJ'=>'SVALBARD AND JAN MAYEN',
		'SZ'=>'SWAZILAND',
		'SE'=>'SWEDEN',
		'CH'=>'SWITZERLAND',
		'SY'=>'SYRIAN ARAB REPUBLIC',
		'TW'=>'TAIWAN, PROVINCE OF CHINA',
		'TJ'=>'TAJIKISTAN',
		'TZ'=>'TANZANIA, UNITED REPUBLIC OF',
		'TH'=>'THAILAND',
		'TG'=>'TOGO',
		'TK'=>'TOKELAU',
		'TO'=>'TONGA',
		'TT'=>'TRINIDAD AND TOBAGO',
		'TN'=>'TUNISIA',
		'TR'=>'TURKEY',
		'TM'=>'TURKMENISTAN',
		'TC'=>'TURKS AND CAICOS ISLANDS',
		'TV'=>'TUVALU',
		'UG'=>'UGANDA',
		'UA'=>'UKRAINE',
		'AE'=>'UNITED ARAB EMIRATES',
		'GB'=>'UNITED KINGDOM',
		'US'=>'UNITED STATES',
		'UM'=>'UNITED STATES MINOR OUTLYING ISLANDS',
		'UY'=>'URUGUAY',
		'UZ'=>'UZBEKISTAN',
		'VU'=>'VANUATU',
		'VE'=>'VENEZUELA',
		'VN'=>'VIET NAM',
		'VG'=>'VIRGIN ISLANDS, BRITISH',
		'VI'=>'VIRGIN ISLANDS, U.S.',
		'WF'=>'WALLIS AND FUTUNA',
		'EH'=>'WESTERN SAHARA',
		'YE'=>'YEMEN',
		'YU'=>'YUGOSLAVIA',
		'ZM'=>'ZAMBIA',
		'ZW'=>'ZIMBABWE'
	);
}

function GetPopup(){
         $now = time();
        
        $popup_sql = "SELECT * FROM home_popup WHERE popup_enddate > {$now} AND status = 1";
	$popup_res = DB::GetQueryResult($popup_sql);
        return $popup_res;
}

global $option_gender;
$option_gender = array(
		'F' => 'female',
		'M' => 'male',
		);
global $option_pay;
$option_pay = array(
		'pay' => 'paid',
		'unpay' => 'to be paid',
		);
global $option_service;
$option_service = array(
		'alipay' => 'alipay',
		'tenpay' => 'tenpay',
		'chinabank' => 'ChinaBank',
		'cash' => 'pay in cach',
		'credit' => 'pay with balance',
		'other' => 'other',
		);
global $option_delivery;
$option_delivery = array(
		'express' => 'express',
		'coupon' => 'coupon',
		'pickup' => 'self delivery',
		'express_pickup' => 'express or self delivery'
		);
global $delivery_status;
$delivery_status = array(
        1 => 'En cours de traitement',
        ZOrder::ShipStat_InPrep => 'En cours de préparation',
        2 => 'Disponible',
        3 => 'Envoyé',
        ZOrder::ShipStat_PickedUp => 'article retiré'
);
global $option_flow;
$option_flow = array(
		'buy' => 'buy',
		'invite' => 'invite',
		'store' => 'topup',
		'withdraw' => 'withdraw',
		'coupon' => 'rebate',
		'refund' => 'refund',
		'register' => 'register',
		'charge' => 'topup',
		);
global $option_mail;
$option_mail = array(
		'gmail.com' => 'https://mail.google.com/',
		'163.com' => 'http://mail.hotmail.com/',
		'yahoo.com' => 'http://mail.yahoo.com/',
		);
global $option_cond;
$option_cond = array(
		'Y' => ' successful Hype decided by the number of buyers',
		'N' => ' successful Hype decided by the quantity of products purchased',
		);
global $option_open;
$option_open = array(
		'Y' => 'display',
		'N' => 'display closed',
		);
global $option_buyonce;
$option_buyonce = array(
		'Y' => 'buy once',
		'N' => 'repeated buying',
		);
global $option_teamtype;
$option_teamtype = array(
		'normal' => 'Daily',
		'seconds' => 'Seconds',
		'goods' => 'Goods',
                'weekly' => 'Weekly'
		);
global $option_yn;
$option_yn = array(
		'Y' => 'yes',
		'N' => 'no',
		);
global $option_alipayitbpay;
$option_alipayitbpay= array(
		'1h' => '1 hour',
		'2h' => '2 hours',
		'3h' => '3 hours',
		'1d' => '1 day',
		'3d' => '3 days',
		'7d' => '7 days',
		'15d' => '15 days',
		);

global $gid_category;
$gid_category = array(
    20 => 'kids'
);


global $coupon_status;
$coupon_status = array(
    'Disputed' => 1,
    'Undisputed' => 0,
    'Disputed_Unadjusted' => 2,
    'Disputed_Adjusted' => 3,
    'Normal_AdjustmentNeeded' => 4,
    'Normal_Adjusted' => 45
);
global $partner_statement_status;
$partner_statement_status = array(
    'New_UnPaid' => 1,
    'New_Paid' => 2,
    'UnPaid' => 3,
    'Paid' => 4
);
