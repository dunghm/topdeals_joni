<?php
function mail_custom($emails=array(), $subject, $message, $from = NULL) 
{
	global $INI;
	$encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';
	settype($emails, 'array');

	$options = array(
		'contentType' => 'text/html',
		'encoding' => $encoding,
	);
        if(is_null($from)){
            $from = $INI['mail']['from'];
        }
	$to = array_shift($emails);
	if ($INI['mail']['mail']=='mail') {
		Mailer::SendMail($from, $to, $subject, $message, $options, $emails);
	} else {
		Mailer::SmtpMail($from, $to, $subject, $message, $options, $emails);
	}
}

function mail_sign($user) 
{
	
	#if value is zero then function email not sent 
	if(emailConfiguration('confirm_email'))
	{
		return true;
	}
	global $INI;
	$encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';
	if ( empty($user) ) return true;
	$from = $INI['mail']['from'];
	$to = $user['email'];

	$vars = array( 'user' => $user,);
	$message = render('mail_sign_verify', $vars);
	//$subject = 'Thanks For Signing up at '.$INI['system']['sitename'].',Please Verify Email';
        
        $subject = 'Merci pour votre inscription sur TopDeal.ch, veuillez confirmer votre adresse e-mail';

	$options = array(
		'contentType' => 'text/html',
		'encoding' => $encoding,
	);
	if ($INI['mail']['mail']=='mail') {
		Mailer::SendMail($from, $to, $subject, $message, $options);
	} else {
		Mailer::SmtpMail($from, $to, $subject, $message, $options);
	}
}

function mail_sign_id($id) {
	$user = Table::Fetch('user', $id);
	mail_sign($user);
}

function mail_sign_email($email) {
	$user = Table::Fetch('user', $email, 'email');
	mail_sign($user);
}

function mail_repass($user) 
{
	#if value is zero then function email not sent 
	if(emailConfiguration('repass'))
	{
		return true;
	}
	global $INI;
	$encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';
	if ( empty($user) ) return true;
	$from = $INI['mail']['from'];
	$to = $user['email'];

	$vars = array( 'user' => $user,);
	$message = render('mail_repass', $vars);
	$subject = "Re-initialisation de votre mot de passe";//$INI['system']['sitename'] . 'Reset Password';

	$options = array(
		'contentType' => 'text/html',
		'encoding' => $encoding,
	);
	if ($INI['mail']['mail']=='mail') {
		Mailer::SendMail($from, $to, $subject, $message, $options);
	} else {
		Mailer::SmtpMail($from, $to, $subject, $message, $options);
	}
}

function mail_subscribe($city, $team, $partner, $subscribe) 
{
	#if value is zero then function email not sent 
	if(emailConfiguration('subscribe'))
	{
		return true;
	}
	global $INI;
	$encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';
	$week = array('Day','1','2','3','4','5','6');
	$today = date('Y Years n Months j Days Weeks') . $week[date('w')];
	$vars = array(
		'today' => $today,
		'team' => $team,
		'city' => $city,
		'subscribe' => $subscribe,
		'partner' => $partner,
		'help_email' => $INI['mail']['helpemail'],
		'help_mobile' => $INI['mail']['helpphone'],
		'notice_email' => $INI['mail']['reply'],
	);
	$message = render('mail_subscribe_team', $vars);
	$options = array(
		'contentType' => 'text/html',
		'encoding' => $encoding,
	);
	$from = $INI['mail']['from'];
	$to = $subscribe['email'];
	$subject = $INI['system']['sitename'] . "Customers today:{$team['title']}";

	if ($INI['mail']['mail']=='mail') {
		Mailer::SendMail($from, $to, $subject, $message, $options);
	} else {
		Mailer::SmtpMail($from, $to, $subject, $message, $options);
	}
}


function mail_invitation($emails, $content, $name) {
	
	#if value is zero then function email not sent 
	if(emailConfiguration('invitation'))
	{
		return true;
	}
	
	global $INI;
	if(empty($emails)) return;

	$emails = preg_split('/[\s,]+/', $emails, -1, PREG_SPLIT_NO_EMPTY);
	$subject = "Votre ami [{$name}] vous invite sur {$INI['system']['sitename']}";
	$vars = array( 
			'name' => $name,
			'content' => $content,
			);
	$message = render('mail_invite', $vars);

	$step = ceil(count($emails)/20);
	for($i=0; $i<$step; $i++) {
		$offset = $i * 20;
		$tos = array_slice($emails, $offset, 20);
		mail_custom($tos, $subject, $message);
	}
	return true;
}

function mail_invite_success($invite, $team, $inviter,$invitee)
{
    #if value is zero then function email not sent 
	if(emailConfiguration('invite_success'))
	{
		return true;
	}
	global $INI;
    $encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';

    $vars = array(
                    'inviter' => $inviter,
                     'invitee' => $invitee,
                    'invite' => $invite,
                    'team' => $team
			);
    
    $message = render('mail_invite_success', $vars);
    $options = array(
            'contentType' => 'text/html',
            'encoding' => $encoding,
    );
    $from = $INI['mail']['from'];
    $to = $inviter['email'];
    $subject = "Congratulations! Your friend '{$invitee['name']}' made a Purchase.";

    if ($INI['mail']['mail']=='mail') {
            Mailer::SendMail($from, $to, $subject, $message, $options);
    } else {
            Mailer::SmtpMail($from, $to, $subject, $message, $options);
    }
}

function mail_promo_code($order, $card)
{
    #if value is zero then function email not sent 
	if(emailConfiguration('promo_code'))
	{
		return true;
	}
	global $INI;
    $encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';

    $vars = array(
                    'order' => $order,
                     'card' => $card,
                   
			);
    
    $message = render('mail/mail_promo_code', $vars);
    $options = array(
            'contentType' => 'text/html',
            'encoding' => $encoding,
    );
    $from = $INI['mail']['from'];
    $to = $order['email'];
    $subject = "TopDeal.ch vous offre un code promo de CHF 15";

    if ($INI['mail']['mail']=='mail') {
            Mailer::SendMail($from, $to, $subject, $message, $options);
    } else {
            Mailer::SmtpMail($from, $to, $subject, $message, $options);
    }
}

function mail_pickup_available($order_item, $order)
{
    #if value is zero then function email not sent 
	if(emailConfiguration('pickup_available'))
	{
		return true;
	}
	global $INI;
    $encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';

    $team = Table::Fetch('team', $order_item['team_id']);
    $option = Table::Fetch('team_multi', $order_item['option_id']);
    $user = Table::Fetch('user', $order['user_id']);
    $vars = array(
        'user' => $user,
        'order_item' => $order_item,
        'order' => $order,
        'team' => $team,
        'option' => $option
        
    );
    
    $message = render('mail/mail_template_pickup_available', $vars);
     $from = $INI['mail']['from'];
    $to = $user['email'];
    $subject = "Votre article est disponible pour le retrait";
     $options = array(
            'contentType' => 'text/html',
            'encoding' => $encoding,
    );
     
    if ($INI['mail']['mail']=='mail') {
        Mailer::SendMail($from, $to, $subject, $message, $options);
    } else {
        Mailer::SmtpMail($from, $to, $subject, $message, $options);
    }
}


function mail_order_sent($order_item, $order)
{
    #if value is zero then function email not sent 
	if(emailConfiguration('order_sent'))
	{
		return true;
	}
	global $INI;
    $encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';

    $team = Table::Fetch('team', $order_item['team_id']);
    $option = Table::Fetch('team_multi', $order_item['option_id']);
    $user = Table::Fetch('user', $order['user_id']);
    $vars = array(
        'user' => $user,
        'order_item' => $order_item,
        'order' => $order,
        'team' => $team,
        'option' => $option
        
    );
    
    $message = render('mail/mail_template_order_sent_confirmation', $vars);
     $from = $INI['mail']['from'];
    $to = $user['email'];
    $subject = "Confirmation d envoi de votre article";
     $options = array(
            'contentType' => 'text/html',
            'encoding' => $encoding,
    );
     
    if ($INI['mail']['mail']=='mail') {
        Mailer::SendMail($from, $to, $subject, $message, $options);
    } else {
        Mailer::SmtpMail($from, $to, $subject, $message, $options);
    }
}


function mail_order_success_express($order_item, $order)
{
    #if value is zero then function email not sent 
	if(emailConfiguration('order_success_express'))
	{
		return true;
	}
	global $INI;
    $encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';

    $team = Table::Fetch('team', $order_item['team_id']);
    $option = Table::Fetch('team_multi', $order_item['option_id']);
    $user = Table::Fetch('user', $order['user_id']);
    $vars = array(
        'user' => $user,
        'order_item' => $order_item,
        'order' => $order,
        'team' => $team,
        'option' => $option
        
    );
    
    $message = render('mail/mail_template_order_successful_express', $vars);
     $from = $INI['mail']['from'];
    $to = $user['email'];
    $subject = "Confirmation de votre commande sur TopDeal.ch";
     $options = array(
            'contentType' => 'text/html',
            'encoding' => $encoding,
    );
     
    if ($INI['mail']['mail']=='mail') {
        Mailer::SendMail($from, $to, $subject, $message, $options);
    } else {
        Mailer::SmtpMail($from, $to, $subject, $message, $options);
    }
}


function mail_order_success_coupon($order_item, $order)
{
    #if value is zero then function email not sent 
	if(emailConfiguration('order_success_coupon'))
	{
		return true;
	}
	global $INI;
    $encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';

    $team = Table::Fetch('team', $order_item['team_id']);
    $option = Table::Fetch('team_multi', $order_item['option_id']);
    $user = Table::Fetch('user', $order['user_id']);
    $vars = array(
        'user' => $user,
        'order_item' => $order_item,
        'order' => $order,
        'team' => $team,
        'option' => $option
        
    );
    
    $message = render('mail/mail_template_order_successful_coupon_deal', $vars);
     $from = $INI['mail']['from'];
    $to = $user['email'];
    $subject = "Confirmation de votre commande sur TopDeal.ch";
     $options = array(
            'contentType' => 'text/html',
            'encoding' => $encoding,
    );
     
    if ($INI['mail']['mail']=='mail') {
        Mailer::SendMail($from, $to, $subject, $message, $options);
    } else {
        Mailer::SmtpMail($from, $to, $subject, $message, $options);
    }
}


function mail_order_success_pickup_waiting($order_item, $order)
{
    #if value is zero then function email not sent 
	if(emailConfiguration('order_success_pickup_waiting'))
	{
		return true;
	}
	
	global $INI;
    $encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';

    $team = Table::Fetch('team', $order_item['team_id']);
    $option = Table::Fetch('team_multi', $order_item['option_id']);
    $user = Table::Fetch('user', $order['user_id']);
    $vars = array(
        'user' => $user,
        'order_item' => $order_item,
        'order' => $order,
        'team' => $team,
        'option' => $option
        
    );
    
    
    $message = render('mail/mail_template_order_successful_pickup_waiting', $vars);
    $from = $INI['mail']['from'];
    $to = $user['email'];
    $subject = "Confirmation de votre commande sur TopDeal.ch";
     $options = array(
            'contentType' => 'text/html',
            'encoding' => $encoding,
    );
     
    if ($INI['mail']['mail']=='mail') {
        Mailer::SendMail($from, $to, $subject, $message, $options);
    } else {
        Mailer::SmtpMail($from, $to, $subject, $message, $options);
    }
}

function mail_order_success($user,$order,$team)
{
    #if value is zero then function email not sent 
	if(emailConfiguration('order_success'))
	{
		return true;
	}
	
	global $INI;
    $encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';

    $order_items = Table::Fetch('order_item', array($order['id']), 'order_id');
    
  /*  
    foreach($order_items as $item)
    {
        if ($item['delivery'] == 'coupon')
            mail_order_success_coupon($item, $order);
        else if ( $item['delivery'] == 'express' )
            mail_order_success_express($item, $order);
        else if ( $item['delivery'] == 'pickup' )
            mail_order_success_pickup_waiting($item, $order);
    }
    
    return;
    */
    $team_ids = Utility::GetColumn($order_items, 'team_id');
    $option_ids = Utility::GetColumn($order_items, 'option_id');
    
    $teams = Table::Fetch('team', $team_ids);
    $options = Table::Fetch('team_multi', $option_ids);
    
    $vars = array(
                    'user' => $user,
                    'team' => $team,
                    'order' => $order,
                    'order_items' => $order_items,
                    'teams' => $teams,
                    'options' => $options,
                );

  
    $message = render('mail/mail_template_confirmation', $vars);
    
    $options = array(
            'contentType' => 'text/html',
            'encoding' => $encoding,
    );
    $from = $INI['mail']['from'];
    $to = $user['email'];
    $subject = "Confirmation de votre commande sur TopDeal.ch";

    if ($INI['mail']['mail']=='mail') {
            Mailer::SendMail($from, $to, $subject, $message, $options);
    } else {
            Mailer::SmtpMail($from, $to, $subject, $message, $options);
    }
}

function mail_payment_success($user,$order,$team)
{
    #if value is zero then function email not sent 
	if(emailConfiguration('payment_success'))
	{
		return true;
	}
	
	global $INI;
    $encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';

    $vars = array(
                    'user' => $user,
                    'team' => $team,
                    'order' => $order
			);

    $message = render('mail_payment_success', $vars);
    $options = array(
            'contentType' => 'text/html',
            'encoding' => $encoding,
    );
    $from = $INI['mail']['from'];
    $to = $user['email'];
    $subject = "Your order payment has been received";

    if ($INI['mail']['mail']=='mail') {
            Mailer::SendMail($from, $to, $subject, $message, $options);
    } else {
            Mailer::SmtpMail($from, $to, $subject, $message, $options);
    }
}

function mail_order_wire_transfer($user,$order,$team)
{
    #if value is zero then function email not sent 
	if(emailConfiguration('order_wire_transfer'))
	{
		return true;
	}
	
	global $INI;
    $encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';

    $vars = array(
                    'user' => $user,
                    'team' => $team,
                    'order' => $order
			);

    $message = render('mail_order_wire_transfer', $vars);
    $options = array(
            'contentType' => 'text/html',
            'encoding' => $encoding,
    );
    $from = $INI['mail']['from'];
    $to = $user['email'];
    $subject = "Votre commande sur TopDeal.ch par virement bancaire";

    if ($INI['mail']['mail']=='mail') {
            Mailer::SendMail($from, $to, $subject, $message, $options);
    } else {
            Mailer::SmtpMail($from, $to, $subject, $message, $options);
    }
}


function mail_order_booked($user,$order,$team)
{
    #if value is zero then function email not sent 
	if(emailConfiguration('order_booked'))
	{
		return true;
	}
	
	global $INI;
    $encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';

    $vars = array(
                    'user' => $user,
                    'team' => $team,
                    'order' => $order
			);

    $message = render('mail_order_booked', $vars);
    $options = array(
            'contentType' => 'text/html',
            'encoding' => $encoding,
    );
    $from = $INI['mail']['from'];
    $to = $user['email'];
    $subject = "Your Order has been booked";

    if ($INI['mail']['mail']=='mail') {
            Mailer::SendMail($from, $to, $subject, $message, $options);
    } else {
            Mailer::SmtpMail($from, $to, $subject, $message, $options);
    }
}

function mail_order_cancel($user, $order, $team)
{
     #if value is zero then function email not sent 
	if(emailConfiguration('order_cancel'))
	{
		return true;
	}
	
	global $INI;
    $encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';

    $vars = array(
                    'user' => $user,
                    'team' => $team,
                    'order' => $order
			);

    $message = render('mail_order_cancel', $vars);
    $options = array(
            'contentType' => 'text/html',
            'encoding' => $encoding,
    );
    $from = $INI['mail']['from'];
    $to = $user['email'];
    $subject = "Your Order has been cancelled";

    if ($INI['mail']['mail']=='mail') {
            Mailer::SendMail($from, $to, $subject, $message, $options);
    } else {
            Mailer::SmtpMail($from, $to, $subject, $message, $options);
    }
}

function mail_success_topup($user,$money)
{
     #if value is zero then function email not sent 
	if(emailConfiguration('success_topup'))
	{
		return true;
	}
	
	global $INI;
    $encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';

    $vars = array(
                    'user' => $user,
                     'money' => $money,
			);

    $message = render('mail_topup_success', $vars);
    $options = array(
            'contentType' => 'text/html',
            'encoding' => $encoding,
    );
    $from = $INI['mail']['from'];
    $to = $user['email'];
    $subject = "Credits sur votre compte TopDeal.ch";

    if ($INI['mail']['mail']=='mail') {
            Mailer::SendMail($from, $to, $subject, $message, $options);
    } else {
            Mailer::SmtpMail($from, $to, $subject, $message, $options);
    }
}

function mail_deal_tipped($team)
{
	global $INI;
    $encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';
    $vars = array ( 'team' => $team );

    $options = array(
            'contentType' => 'text/html',
            'encoding' => $encoding,
    );
    $from = $INI['mail']['from'];
    
  $subject = "Le deal que vous avez commandé sur TopDeal.ch a été validé!"; //"Congratulations! Hype has been created";
    $message = render('mail_deal_tipped', $vars);
   
   
    $order_items = Table::Fetch('order_item', array($team['id']), 'team_id');
    $order_ids = Utility::GetColumn($order_items,'order_id');
    $orders = Table::Fetch('order',$order_ids);    
    
    $to[] = $INI['mail']['from'];


  $bcc[] = 'mail2navaid@gmail.com';//$INI['mail']['from'];
  
  foreach($orders as $one)
    {
        $user = Table::Fetch('user', $one['user_id']);
        if ( $user ){
          $bcc[] = $user['email'];          
        }
    }
    
    $unique_users = join(',',array_unique($to));
    if ($INI['mail']['mail']=='mail') {
            Mailer::SendMail($from, $unique_users, $subject, $message, $options, $bcc);
    } else {
        Mailer::SmtpMail($from, $unique_users, $subject, $message, $options, $bcc);
    }
}

function mail_giftcard_send($email, $cardid)
{
    #if value is zero then function email not sent 
	if(emailConfiguration('giftcard_send'))
	{
		return true;
	}
	
	global $INI;
    $encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';

   

      $vars = array(
                    'cardid' => $cardid,
                     
			);

    $message = render('mail_giftcard_send', $vars);
    $options = array(
            'contentType' => 'text/html',
            'encoding' => $encoding,
    );
    $from = $INI['mail']['from'];
    $to = $email;
    $subject = "Votre bon cadeau de 10.-";

    if ($INI['mail']['mail']=='mail') {
            Mailer::SendMail($from, $to, $subject, $message, $options);
    } else {
            Mailer::SmtpMail($from, $to, $subject, $message, $options);
    }
}


function mail_contest_invite($from, $to, $subject, $message)
{
	global $INI;
    $encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';

   
    $options = array(
            'contentType' => 'text/html',
            'encoding' => $encoding,
    );
	
    $to = is_array($to) ? $to : array($to);
	
    $unique_users = join(',', array_unique($to));
	
    if ($INI['mail']['mail']=='mail') {
            Mailer::SendMail($from, $unique_users, $subject, $message, $options, $to);
    } else {
        Mailer::SmtpMail($from, $unique_users, $subject, $message, $options, $to);
    }
}

function mail_welcome_user($user, $password) {
	#if value is zero then function email not sent 
	if(emailConfiguration('welcome_user'))
	{
		return true;
	}
	
	global $INI;
	$encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';
	if ( empty($user) ) return true;
	$from = $INI['mail']['from'];
	$to = $user['email'];

	$vars = array( 'user' => $user, 'password' => $password);
	$message = render('mail/mail_welcome_user', $vars);
	//$subject = 'Thanks For Signing up at '.$INI['system']['sitename'].',Please Verify Email';
        
    $subject = 'Bienvenue sur TopDeal.ch';

	$options = array(
		'contentType' => 'text/html',
		'encoding' => $encoding,
	);
	if ($INI['mail']['mail']=='mail') {
		Mailer::SendMail($from, $to, $subject, $message, $options);
	} else {
		Mailer::SmtpMail($from, $to, $subject, $message, $options);
	}
}

function mail_stock_threshold_alert($team, $option)
{
	
	#if value is zero then function email not sent 
	if(emailConfiguration('stock_threshold_alert'))
	{
		return true;
	}
	
	global $INI;
    $emails = array();
    $emails[] = $INI['mail']['from'];
    $emails[] = 'abdullahrahim87@gmail.com';
    $emails[] = 'pmacloud@gmail.com';
    $subject = 'Stock Threshold Alert';
    
    if($team){
        $message = 'Deal :'.$team['title']. ' stock has decreased to '.$team['stock_count'].'.';
        mail_custom($emails , $subject, $message); 
    }
    else if($option){
        $team = Table::Fetch('team', $option['team_id']);
        $message = 'Deal :'.$team['title']. ' Option:'.$option['title'].' stock has decreased to '.$option['stock_count'].'.';
        mail_custom($emails , $subject, $message); 
    }
}

function mail_stock_finished_alert($team, $option)
{
	
	#if value is zero then function email not sent 
	if(emailConfiguration('stock_finished_alert'))
	{
		return true;
	}
	
	global $INI;
    $emails = array();
    $emails[] = $INI['mail']['from'];
    $emails[] = 'abdullahrahim87@gmail.com';
    $emails[] = 'pmacloud@gmail.com';
    $subject = 'Stock Finished Alert';
    
    if($team){
        $message = ' Deal :'.$team['title']. ' stock is finished.';
        mail_custom($emails , $subject, $message); 
    }
    else if($option){
        $team = Table::Fetch('team', $option['team_id']);
        $message = ' Deal :'.$team['title']. ' Option:'.$option['title'].' stock is finished.';
        mail_custom($emails , $subject, $message); 
    }
}

function mail_order_refund($user,$order, $team,$rid='')
{
     #if value is zero then function email not sent 
	if(emailConfiguration('order_refund'))
	{
		return true;
	}
	
	global $INI;
    $encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';

    $order_items = Table::Fetch('order_item', array($order['id']), 'order_id');
    
  /*  
    foreach($order_items as $item)
    {
        if ($item['delivery'] == 'coupon')
            mail_order_success_coupon($item, $order);
        else if ( $item['delivery'] == 'express' )
            mail_order_success_express($item, $order);
        else if ( $item['delivery'] == 'pickup' )
            mail_order_success_pickup_waiting($item, $order);
    }
    
    return;
    */
    $team_ids = Utility::GetColumn($order_items, 'team_id');
    $option_ids = Utility::GetColumn($order_items, 'option_id');
    
    $teams = Table::Fetch('team', $team_ids);
    $options = Table::Fetch('team_multi', $option_ids);
    
    $vars = array(
                    'user' => $user,
                    'team' => $team,
                    'order' => $order,
                    'order_items' => $order_items,
                    'teams' => $teams,
                    'options' => $options,
					'rid'	=> $rid,	
                );

  
    $message = render('mail/mail_template_refund', $vars);
    
    $options = array(
            'contentType' => 'text/html',
            'encoding' => $encoding,
    );
    $from = $INI['mail']['from'];
    $to = $user['email'];
    //$subject = "Confirmation de votre commande sur TopDeal.ch";
$subject = "Annulation de votre commande";

    
    if ($INI['mail']['mail']=='mail') {
            Mailer::SendMail($from, $to, $subject, $message, $options);
    } else {
            Mailer::SmtpMail($from, $to, $subject, $message, $options);
    }
}


function emailConfiguration($eventKey='')
{
	$value = 0;
	#get value from email_config table
	$emailConfigArray = Table::Fetch('email_config', $eventKey, 'event_key');
	if(is_array($emailConfigArray) && count($emailConfigArray)>0)
	{
		if(isset($emailConfigArray['value']))
		{
			$value = $emailConfigArray['value'];
		}
	}
	#value check
	if($value<=0)
	{
		return true;
	}
	else
	{
		return false;
	}
	
}

function mail_waiting_order_pass()
{
	global $INI;
    $mailSent = false;
	#from
	$from = $INI['mail']['from'];
	#to
	$to = 'support@topdeal.ch';
	//$to = 'jawwadshamim3@gmail.com';
    #subject
	$subject = ' Waiting Orders ETA Expiring Tomorrow';
	#message
	$message = render('mail/mail_template_waiting_order_pass');
	#option
	$options = array(
            'contentType' => 'text/html',
            'encoding' => $encoding,
    );
	
	if ($INI['mail']['mail']=='mail') {
		$mailSent = Mailer::SendMail($from, $to, $subject, $message, $options); 
	} else {
		$mailSent =Mailer::SmtpMail($from, $to, $subject, $message, $options);
	}
	
	if($mailSent)
	{
		echo "email sent successfully";
	}
	else
	{
		echo "email not sent successfully please try again!";
	}
}

?>

