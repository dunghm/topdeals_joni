<?php
function current_frontend() {
	global $INI;
	$a = array(
			'/index.php' => 'Accueil',
                        '/deal_du_jour' => 'Deal du jour',
			'/deals' => 'Deals actuels',
                        "http://www.topdeal.ch/infos/concept" => "Concept",
			//'/deals/submit.php' => I('Propose_Deal'),
			//'/help/tour.php' => I('Tour'),
			//'/forum/index.php' => I('Discuss'),
			//'/help/faqs.php' => I('Faq'),
                       // '/invitenwin/index.php' => 'Concours'
			);
	//if(option_yes('navforum')) $a['/forum/index.php'] = I('Discuss');
	$r = $_SERVER['REQUEST_URI'];
	
        if(preg_match('#index.php\?t=w#',$r))
                $l = '/index.php?t=w';
        else if ( preg_match('#/deal_du_jour#', $r))
                $l = '/deal_du_jour';
        else if (preg_match('#/deals#',$r))
                $l = '/deals';
        else if (preg_match('#/help#',$r))
                $l = '/help/tour.php';
        else if (preg_match('#/subscribe#',$r))
                $l = '/subscribe.php';
        else if (preg_match('#/account#',$r))
                $l = '/help/tour.php';	
        else if (preg_match('#/forum#',$r))
                $l = '/forum/index.php';
        else
            $l = '/index.php';
	return current_link($l, $a);
}

function current_backend() {
	global $INI;
	$a = array(
			'/manage/misc/index.php' => 'Home',
			'/manage/team/index.php' => 'Deal',
			'/manage/userdeal/index.php' => 'User Deal',
			'/manage/order/index.php' => 'Order',
			'/manage/coupon/index.php' => 'Coupon',
			'/manage/contest/index.php' => 'Contest',
            '/manage/invitenwin/index.php' => 'Invite Contest',   
			'/manage/user/index.php' => 'User',
			'/manage/market/index.php' => 'Marketing',
			'/manage/category/index.php' => 'Category',
			'/manage/statement/index.php' => 'Statement',
                        '/manage/revenue/index.php' => 'Revenue',
			'/manage/credit/index.php' => 'Credit',
			'/manage/system/index.php' => 'Setting',
			);
	$r = $_SERVER['REQUEST_URI'];
	if (preg_match('#/manage/(\w+)/#',$r, $m)) {
		$l = "/manage/{$m[1]}/index.php";
	} else $l = '/manage/misc/index.php';
	return current_link($l, $a);
}

function current_biz() {
	global $INI;
	$a = array(
			'/biz/index.php' => 'Home',
			'/biz/settings.php' => 'Info partenaire',
			'/biz/coupon.php' => 'Liste des bons',
			);
	$r = $_SERVER['REQUEST_URI'];
	if (preg_match('#/biz/coupon#',$r)) $l = '/biz/coupon.php';
	elseif (preg_match('#/biz/settings#',$r)) $l = '/biz/settings.php';
	else $l = '/biz/index.php';
	return current_link($l, $a);
}

function current_forum($selector='index') {
	global $city;
	$a = array(
			'/forum/index.php' => I('All'),
			'/forum/city.php' => "{$city['name']} Forum",
			'/forum/public.php' => 'Public forum',
			);
	if (!$city) unset($a['/forum/city.php']);
	$l = "/forum/{$selector}.php";
	return current_link($l, $a, true);
}

function current_invite($selector='refer') {
	$a = array(
			'/account/refer.php' => I('All'),
			'/account/referpending.php' => I('To_Buy'),
			'/account/referdone.php' => I('Rebate_Paid'),
			);
	$l = "/account/{$selector}.php";
	return current_link($l, $a, true);
}

function current_partner($gid='0') {
	$a = array(
			'/partner/index.php' => I('All'),
			);
	foreach(option_category('partner') AS $id=>$name) {
		$a["/partner/index.php?gid={$id}"] = $name;
	}
	$l = "/partner/index.php?gid={$gid}";
	if (!$gid) $l = "/partner/index.php";
	return current_link($l, $a, true);
}

function current_city($cename, $citys) {
	$link = "/city.php?ename={$cename}";
	$links = array();
	foreach($citys AS $city) {
		$links["/city.php?ename={$city['ename']}"] = $city['name'];
	}
	return current_link($link, $links);
}

function current_coupon_sub($selector='index') {
	$selector = $selector ? $selector : 'index';
	$a = array(
		'/coupon/index.php' => I('Usable'),
		'/coupon/consume.php' => I('Used'),
		'/coupon/expire.php' => I('Expired'),
	);
	$l = "/coupon/{$selector}.php";
	return current_link($l, $a);
}

function current_account($selector='/account/settings.php') {
	global $INI;
	$a = array(
		'/coupon/index.php' => I('My').' '.$INI['system']['couponname'],
		'/order/index.php' => I('My_Orders'),		
		'/credit/index.php' => I('My_Balance'),
		'/account/settings.php' => I('My_Account'),
		//'/account/myask.php' => I('My_Q_A'),
	);
	if (option_yes('usercredit')) {
		$a['/credit/score.php'] = I('My_Points');
	}
        $a['/account/refer.php']  = I('My_Invitations');
        
	return current_link($selector, $a, true);
}

function current_about() {
	global $INI;
	$a = array(
		'/apropos' => 'A propos',
		'/contact' => 'Contact',
		'/jobs' => 'Jobs',
		'/presse' => 'Presse'
	);
	$r = $_SERVER['REQUEST_URI'];
	if(preg_match('#/apropos#',$r))
                $l = '/apropos';
        else if ( preg_match('#/contact#', $r))
                $l = '/contact';
        else if (preg_match('#/deals#',$r))
                $l = '/deals';
        else if (preg_match('#/jobs#',$r))
                $l = '/jobs';
        else if (preg_match('#/presse#',$r))
                $l = '/presse';
        else
            $l = '/index.php';
	return current_link($l, $a, true);
}

function current_help($selector='faqs') {
	global $INI;
	$a = array(
		'/help/tour.php' => 'Play with ' . $INI['system']['abbreviation'],
		'/help/faqs.php' => 'FAQ',
		'/help/wroupon.php' => 'What is ' . $INI['system']['abbreviation'],
	);
	$l = "/help/{$selector}.php";
	return current_link($l, $a, true);
}

function current_recent_index($selector='index',$groupid = 0) {
	$selector = $selector ? $selector : 'index';
        $groupSelector = $groupid ? "&gid={$groupid}" : "";
	$a = array(
		"/team/index.php?t=index{$groupSelector}" => I('Recent_All'),
		"/team/index.php?t=d{$groupSelector}" => I('Recent_Daily'),
		"/team/index.php?t=w{$groupSelector}" => I('Recent_Weekly'),
	);
	$l = "/team/index.php?t={$selector}{$groupSelector}";
	return current_link($l, $a);
}

function current_order_index($selector='index') {
	$selector = $selector ? $selector : 'index';
	$a = array(
		'/order/index.php?s=index' => I('All'),
		'/order/index.php?s=unpay' => I('ToBePaid'),
		'/order/index.php?s=pay' => I('Paid'),
	);
	$l = "/order/index.php?s={$selector}";
	return current_link($l, $a);
}

function current_credit_index($selector='index') {
	$selector = $selector ? $selector : 'index';
	$a = array(
		'/credit/score.php' => 'My Points',
		'/credit/goods.php' => 'Convert Points',
	);
	$l = "/credit/{$selector}.php";
	return current_link($l, $a);
}

function current_link($link, $links, $span=false) {
	$html = '';
	$span = $span ? '<span></span>' : '';
	$i = 0;
	foreach($links AS $l=>$n) {
		if (trim($l,'/')==trim($link,'/')) {
			$html .= "<li class=\"current\"><a class=\"item{$i}\" href=\"{$l}\">{$n}</a>{$span}</li>";
		}
		else $html .= "<li><a class=\"item{$i}\" href=\"{$l}\">{$n}</a>{$span}</li>";
		$i++;
	}
	return $html;
}

/* manage current */


function mcurrent_statement($selector=null) {
	$a = array(
		'/manage/statement/index.php' => 'All Statements',
		'/manage/statement/index.php?filter=unpaid' => 'UnPaid Statements',
		'/manage/statement/index.php?filter=paid' => 'Paid Statements',
                '/manage/statement/generate.php' => 'Generate Statement',
		
	);
        if ( $selector == "paid" )
            $l = "/manage/statement/index.php?filter=paid";
        else if ( $selector == "unpaid" )
            $l = "/manage/statement/index.php?filter=unpaid";
        else
            $l = "/manage/statement/{$selector}.php";
	return current_link($l,$a,true);
}

function mcurrent_contest($selector='index') {
	$a = array(
          //'/manage/contest/index.php' => 'Contest Home',
          //'/manage/contest/create.php' => 'Create Contest',
          '/manage/contest/list.php' => 'Contest Participants',
          '/manage/contest/winner.php' => 'Contest Winner',
	);
	$l = "/manage/contest/{$selector}.php";
	return current_link($l, $a, true);
}

function mcurrent_invitecontest($selector='index') {
	$a = array(
          //'/manage/contest/index.php' => 'Contest Home',
          //'/manage/contest/create.php' => 'Create Contest',
          '/manage/invitenwin/index.php?cmd=index' => 'Main',
            '/manage/invitenwin/index.php?cmd=create' => 'Create Contest',
            '/manage/invitenwin/index.php?cmd=edit' => 'Edit Contest',
            '/manage/invitenwin/index.php?cmd=subscription' => 'Subscribers',
            '/manage/invitenwin/index.php?cmd=referals' => 'Referals',
         // '/manage/contest/winner.php' => 'Contest Winner',
	);
	$l = "/manage/invitenwin/index.php?cmd={$selector}";
	return current_link($l, $a, true);
}

function mcurrent_misc($selector=null) {
	$a = array(
		'/manage/misc/index.php' => 'Home',
		'/manage/misc/ask.php' => 'Q & A',
		'/manage/misc/feedback.php' => 'Feedback',
		'/manage/misc/subscribe.php' => 'Email',
		'/manage/misc/smssubscribe.php' => 'SMS',
		'/manage/misc/invite.php' => 'Rebate',
		'/manage/misc/money.php' => 'Finance',
		'/manage/misc/link.php' => 'Friend link',
		'/manage/misc/backup.php' => 'Backup',
                '/manage/misc/popup.php' => 'Popup',
                '/manage/misc/promotions.php' => 'Promotions',
	);
	$l = "/manage/misc/{$selector}.php";
	return current_link($l,$a,true);
}

function mcurrent_misc_money($selector=null){
	$selector = $selector ? $selector : 'store';
	$a = array(
		'/manage/misc/money.php?s=store' => 'Offline topup',
		'/manage/misc/money.php?s=charge' => 'Online topup',
		'/manage/misc/money.php?s=withdraw' => 'Withdraw records',
		'/manage/misc/money.php?s=cash' => 'Pay in cach',
		'/manage/misc/money.php?s=refund' => 'Refund records',
	);
	$l = "/manage/misc/money.php?s={$selector}";
	return current_link($l, $a);
}

function mcurrent_misc_backup($selector=null){
	$selector = $selector ? $selector : 'backup';
	$a = array(
		'/manage/misc/backup.php' => 'Database backup',
		'/manage/misc/restore.php' => 'Database restore',
	);
	$l = "/manage/misc/{$selector}.php";
	return current_link($l, $a);
}

function mcurrent_misc_invite($selector=null){
	$selector = $selector ? $selector : 'index';
	$a = array(
		'/manage/misc/invite.php?s=index' => 'Pendings',
		'/manage/misc/invite.php?s=record' => 'Approved',
		'/manage/misc/invite.php?s=cancel' => 'Fouls',
	);
	$l = "/manage/misc/invite.php?s={$selector}";
	return current_link($l, $a);
}
function mcurrent_order($selector=null) {
	$a = array(
		'/manage/order/index.php' => 'Order of today\'s Hype',
		'/manage/order/pay.php' => 'Order paid',
		'/manage/order/credit.php' => 'Pay with balance',
		'/manage/order/unpay.php' => 'Order to be paid',
                '/manage/order/inventory.php' => 'Inventory',
                '/manage/order/refund.php' => 'Order Refund',
                '/manage/order/generate_barcode.php' => 'Generate Barcode',
                '/manage/order/stock_status.php' => 'Stock Status',
	);
	$l = "/manage/order/{$selector}.php";
	return current_link($l,$a,true);
}

function mcurrent_user($selector=null) {
	$a = array(
		'/manage/user/index.php' => 'User list',
		'/manage/user/manager.php' => 'Manager list',
		'/manage/partner/index.php' => 'Partner list',
		'/manage/partner/create.php' => 'New Partner',
                '/manage/supplier/list.php' => 'Supplier list',
		'/manage/supplier/create_supplier.php' => 'New Supplier',
	);
	$l = "/manage/user/{$selector}.php";
	return current_link($l,$a,true);
}
function mcurrent_team($selector=null) {
	$a = array(
		'/manage/team/index.php' => 'On Going Deals',
        /*'/manage/team/index.php?team_type=weekly' => 'Weekly Hype',*/
		'/manage/team/success.php' => 'Unpublished Deals',
		/*'/manage/team/failure.php' => 'Unsuccessful Hype',*/
		'/manage/team/edit.php' => 'Create Deal',
                '/manage/team/brands.php' => 'Brands',
		/*'/manage/team/userdeals.php' => 'User Submitted ',
		'/manage/team/dealofday.php' => 'Deal of the Day ',		*/
	);
        if ( $selector == "weekly" )
            $l = "/manage/team/index.php?team_type=weekly";
        else
            $l = "/manage/team/{$selector}.php";
	return current_link($l,$a,true);
}

function mcurrent_userdeal($selector=null) {
	$a = array(
		'/manage/userdeal/index.php' => 'Today\'s Hype',
        '/manage/userdeal/alldeal.php' => 'All Hype',
		'/manage/userdeal/dealofday.php' => 'Deal of the Day ',		
	);
         $l = "/manage/userdeal/{$selector}.php";
	return current_link($l,$a,true);
}

function current_deal($selector=null) {
	$a = array(
		'/deals/index.php' => 'Submitted Deals',
		'/deals/submit.php' => 'Submit New Deal',
	);
	$l = "/deals/{$selector}.php";	
	return current_link($l,$a,true);
}

function mcurrent_feedback($selector=null) {
	$a = array(
		'/manage/feedback/index.php' => 'Overview',
	);
	$l = "/manage/feedback/{$selector}.php";
	return current_link($l,$a,true);
}
function mcurrent_coupon($selector=null) {
	$a = array(
		'/manage/coupon/index.php' => 'Usable Coupons',
		'/manage/coupon/consume.php' => 'Used Coupons',
		'/manage/coupon/expire.php' => 'Expired Coupons',
		'/manage/coupon/card.php' => 'Promo Code',
		'/manage/coupon/cardcreate.php' => 'New Promo Code',
		'/manage/coupon/cardsend.php' => 'Send Promo Code',
        '/manage/coupon/advance.php' => 'Advance Coupons',
        '/manage/coupon/advancecreate.php' => 'New Advance Coupons',                
	);
	$l = "/manage/coupon/{$selector}.php";
	return current_link($l,$a,true);
}
function mcurrent_category($selector=null) {
	$zones = get_zones();
	$a = array();
	foreach( $zones AS $z=>$o ) {
		$a['/manage/category/index.php?zone='.$z] = $o;
	}
	$l = "/manage/category/index.php?zone={$selector}";
	return current_link($l,$a,true);
}
function mcurrent_partner($selector=null) {
	$a = array(
		'/manage/user/index.php' => 'User list',
		'/manage/user/manager.php' => 'Manager list',
		'/manage/partner/index.php' => 'Partner list',
		'/manage/partner/create.php' => 'New Partner',
                '/manage/supplier/list.php' => 'Supplier list',
		'/manage/supplier/create_supplier.php' => 'New Supplier',
	);
	$l = "/manage/partner/{$selector}.php";
	return current_link($l,$a,true);
}
function mcurrent_supplier($selector=null) {
	$a = array(
		'/manage/user/index.php' => 'User list',
		'/manage/user/manager.php' => 'Manager list',
		'/manage/partner/index.php' => 'Partner list',
		'/manage/partner/create.php' => 'New Partner',
                '/manage/supplier/list.php' => 'Supplier list',
		'/manage/supplier/create_supplier.php' => 'New Supplier',
	);
	$l = "/manage/supplier/{$selector}.php";
	return current_link($l,$a,true);
}

function mcurrent_revenue($selector=null) {
    $a = array(
		'/manage/revenue/index.php' => 'Revenue by Deal',
		'/manage/revenue/supplier.php' => 'Revenue by Supplier',
		
	);
	$l = "/manage/revenue/{$selector}.php";
	return current_link($l,$a,true);
}

function mcurrent_market($selector=null) {
	$a = array(
		'/manage/market/index.php' => 'Email marketing',
                '/manage/market/sms.php' => 'SMS group-sending',
          	'/manage/market/down.php' => 'Data download',
       		'/manage/etemplate/index.php' => 'Email Templates',
	);
	$l = "/manage/market/{$selector}.php";
	return current_link($l,$a,true);
}
function mcurrent_market_down($selector=null) {
	$a = array(
		'/manage/market/down.php' => 'Mobile number',
		'/manage/market/downemail.php' => 'Email address',
		'/manage/market/downorder.php' => 'Hype order',
		'/manage/market/downcoupon.php' => 'Coupon',
		'/manage/market/downuser.php' => 'User info',
	);
	$l = "/manage/market/{$selector}.php";
	return current_link($l,$a,true);
}

function mcurrent_system($selector=null) {
	$a = array(
		'/manage/system/index.php' => 'Basic',
		'/manage/system/option.php' => 'Option',
		'/manage/system/bulletin.php' => 'Bulletin',
		'/manage/system/pay.php' => 'Pay',
		'/manage/system/email.php' => 'Email',
		'/manage/system/sms.php' => 'SMS',
		'/manage/system/page.php' => 'Webpage',
		'/manage/system/cache.php' => 'Cache',
		'/manage/system/skin.php' => 'Skin',
		'/manage/system/template.php' => 'Template',
		'/manage/system/upgrade.php' => 'Upgrade',
		'/manage/system/deal.php' => 'Deal',
                '/manage/system/shipping.php'=> 'Shipping',
		'/manage/system/generatesitemap.php' => 'Sitemap',
                '/manage/system/videos.php' => 'Video Upload',

	);
	$l = "/manage/system/{$selector}.php";
	return current_link($l,$a,true);
}

function mcurrent_credit($selector=null) {
	$a = array(
		'/manage/credit/index.php' => 'Credit records',
		'/manage/credit/settings.php' => 'Credit rules',
		'/manage/credit/goods.php' => 'Convert credits',
	);
	$l = "/manage/credit/{$selector}.php";
	return current_link($l,$a,true);
}
