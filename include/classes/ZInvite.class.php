<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/invitenwin/InviteContest.php');

class ZInvite
{
	static public function CreateNewId($other_user_id) {
		$_rid = abs(intval(cookieget('_rid')));
		$other_user_id = abs(intval($other_user_id));
		if ($_rid==0 || $other_user_id==0) return;
		self::CreateFromId($_rid, $other_user_id);
	}
	
	static public function Create($ruser, $newuser) {
		if ($ruser['id'] == $newuser['id']) return;
		cookieset('_rid', null, -1);
		if ($newuser['newbie'] == 'N') return;
		$have = Table::Fetch('invite', $newuser['id'], 'other_user_id');
		cookieset('_rid', null, -1);
		if ( $have ) return false;
		$invite = array(
			'user_id' => $ruser['id'],
			'user_ip' => $ruser['ip'],
			'other_user_id' => $newuser['id'],
			'other_user_ip' => $newuser['ip'],
			'create_time' => time(),
			'pay' => 'N', //added by Nav
			'buy_time' => time(), //added by Nav
		);
		
		//return DB::Insert('invite', $invite);
		$insert_id = DB::Insert('invite', $invite);
                
                /***** Invite Contest Addition ******/
                $invitecontest = current_invitenwin();
                if ( $invitecontest && InviteContest::IsUserSubscribed($ruser['id'], $invitecontest['id']) )
                {
                    $contest_entry = array('user_id' => $newuser['id'],
                        'invitecontest_id' => $invitecontest['id'], 
                        'refer_by' => $ruser['id'],
                        'invite_id' => $insert_id);
                    DB::Insert('ic_referals', $contest_entry);
                }
                /****** End Invite Contest *********/

		$money=0;
	
		$condition = array( 'user_id' => $ruser['id'] );
		$count_invite = Table::Count('invite', $condition);		

          //if($pre_launch){
		if($count_invite>=5) { //5
		
			if($count_invite==5) { //5
				$money=10;
			}
			elseif($count_invite==10) { //10
				$money=10;
			}
			elseif($count_invite==20) { //20
				$money=20;
			}

			if($money>0) {
				//update flow entry;
				$u = array(
				'user_id' => $ruser['id'],
				'admin_id' => 0,//$login_user_id,
				'money' => $money,
				'direction' => 'income',
				'action' => 'invite',
				'detail_id' => $newuser['id'],
				'create_time' => time(),
				//'detail' => $count_invite.'-'. $ruser['id'],
				);
				DB::Insert('flow', $u);

				//update user money;
				$user = Table::Fetch('user', $ruser['id']);
				Table::UpdateCache('user', $ruser['id'], array(
							'money' => array( "money + {$money}" ),
							));
			}
		}
          //}	
		return $insert_id;
	}

	static public function CreateFromId($user_id, $other_user_id) {
		if (!$user_id || !$other_user_id) return;
		if ($user_id == $other_user_id) return;
		$ruser = Table::Fetch('user', $user_id);
		$newuser = Table::Fetch('user', $other_user_id);
		if ( $newuser['newbie'] == 'Y' ) {
			cookieset('_rid', null, -1);
			self::Create($ruser, $newuser);
		}
	}

	static public function CreateFromBuy($other_user_id) {
		$rid = abs(intval(cookieget('_rid')));
		return self::CreateFromId($rid, $other_user_id);
	}

	static public function CheckInvite($order) {
		if ( $order['state'] != 'pay' ) return;
		$user = Table::Fetch('user', $order['user_id']);
		//$team = Table::Fetch('team', $order['team_id']);
		if ( !$user || $user['newbie'] != 'Y' ) return;
		Table::UpdateCache('user', $order['user_id'], array(
			'newbie' => 'N',
		));

		global $INI;
		$invite = Table::Fetch('invite',$order['user_id'],'other_user_id');
		//$invitecredit = abs(intval($team['bonus']));
		$invitecredit = $INI['credit']['invite'];

		/* invitation not recorded or rebate given or cancelled */
		if (!$invite || $invite['credit']>0 || $invite['pay']!='N') {
			return;
		}
		if (time() - $invite['create_time'] > 7*86400) {
			return;
		}
		Table::UpdateCache('invite', $invite['id'], array(
			'credit' => $invitecredit,
			'team_id' => $order['id'],
			'buy_time' => time(),
		));
		return true;
	}
}
