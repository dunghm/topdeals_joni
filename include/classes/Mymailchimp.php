<?php
require_once dirname(dirname(__FILE__)).'/library/Mailchimp.php';
class Mymailchimp
{

	# Credentials
	# Jawwad's Account
// 	static private $apiKey = "64290e8ac32893e956d6a6ff25a3cfa8-us10";
// 	static private $listId = "62c884ff0b";
	# Nouman's Account
	static private $apiKey = "5da2829014852d7eab24c568d2d62756-us10";
	static private $listId = "b5ef47484b";
	
	static public function validateMember($email)
	{
		$apiKey = self::$apiKey;
		$listId = self::$listId;
		
		#validate Member
		$Mailchimp = new Mailchimp($apiKey);
		$memberInfo = $Mailchimp->call('lists/member-info', array(
																	'id' => $listId, // your mailchimp list id here
																	'emails' => array(
																	array(
																			'email' => $email) // Mr Potato's email here
																		  )
																	)
										); 
		return $memberInfo;								
	} 
	
	
	static public function Subscribe($email)
    {
		
		$apiKey = self::$apiKey;
		$listId = self::$listId;
		
		$Mailchimp = new Mailchimp($apiKey);

		$memberInfo = Mymailchimp::validateMember($email);
		
		if(isset($memberInfo['success_count']) &&  $memberInfo['success_count']== 0)
		{
			$Mailchimp_Lists = new Mailchimp_Lists( $Mailchimp );

			$subscriber = $Mailchimp_Lists->subscribe(
				$listId, 
				array( 'email' => $email ),
				null,
				'html',
				false # Disable double_optin
			);
			
			 if ( ! empty( $subscriber['leid'] ) ) 
			 {
			   return true;
			 }
			 else
			 {
				return false;
			 } 
		}
		#else
		#{
			#return false;
			//echo "your email address  ".$email." is already exist";
		#}	 						
										
										
	}
	
	
	static public function UnSubscribe($email)
	{
		$apiKey = self::$apiKey;
		$listId = self::$listId;
		
		$Mailchimp = new Mailchimp($apiKey);

		$memberInfo = Mymailchimp::validateMember($email); 
		
		if(!empty($memberInfo['success_count']))
		{
			$Mailchimp_Lists = new Mailchimp_Lists( $Mailchimp );
			$retval = $Mailchimp_Lists->unsubscribe(
				$listId,
				array('email' => $email),
				true, //delete_member
				true, //send_goodbye
				false //send_notify
			);

			if($retval['complete'])
			{
				 return true;
			}
			else
			{
				return false;
			} 
		 }
		 else
		 {
			return false;
			//echo "Sorry we could not find your email address  ".$my_email;
		 }
	}
}
