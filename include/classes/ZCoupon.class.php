<?php
class ZCoupon
{
	static public function Consume($coupon) {
		if ( !$coupon['consume']=='N' ) return false;
		$u = array(
			'ip' => Utility::GetRemoteIp(),
			'consume_time' => time(),
			'consume' => 'Y',
		);
		Table::UpdateCache('coupon', $coupon['id'], $u);
		ZFlow::CreateFromCoupon($coupon);
		return true;
	}

        static public function CheckOrderItem($order_item, $user_id)
        {
            $team_id = $order_item['team_id'];
            
            $team = Table::FetchForce('team', $order_item['team_id']);
            
            if ($order_item['delivery'] == 'pickup' || $order_item['delivery'] == 'express')
            {    
                $eta_add  =  isset($team['delivery_eta'])?$team['delivery_eta']:0;
                
                if( isset($eta_add) && ($eta_add > 0) && $order_item['delivery'] =='express' ){
                     $order = Table::FetchForce('order', $order_item['order_id']); 
                     $eta = $order['pay_time'] + ($eta_add*86400);
                     
                }
                else{
                     $eta = 0;
                }
                
                ZOrder::UpdateShippingState(ZOrder::ShipStat_Available, $order_item['id'], $eta, TRUE);         
            }   
            
            // --> Check if this Deal needs Coupon
            $coupon_array = array('coupon', 'pickup');            
            if (!in_array($order_item['delivery'], $coupon_array)) return;
            // <---
            
            if ( $team['now_number'] >= $team['min_number'] ) 
            {
                //init coupon create;
                $last = ($team['conduser']=='Y') ? 1 : $order_item['quantity'];
                $offset = max(5, $last);
                if ( $team['now_number'] - $team['min_number'] < $last) 
                {
                    // Fetch all previous orders for this Team
                    $sql = "SELECT order_item.*, user_id FROM order_item 
LEFT JOIN `order` ON `order`.id = order_item.order_id 
WHERE order_item.team_id = {$order_item['team_id']}
AND state = 'pay'";
                    $order_item_records = DB::GetQueryResult($sql, false);
                    foreach($order_item_records AS $oi_record) 
                    {
                        self::Create($oi_record, $oi_record['user_id']);
                    }
                }
                else{
                    self::Create($order_item, $user_id);
                }
            }
        }
        
	static public function CheckOrder($order) {
                if ( $order ['state'] != 'pay' ) return;

		$order_items = Table::Fetch('order_item', array($order['id']), 'order_id');
                foreach ($order_items as $item)
                {
                    self::CheckOrderItem($item, $order['user_id']);
                }
	}
        
        static public function AssignAdvance($order_item,$user_id){
            
            $countcon = array('order_id' => $order_item['id']);
            $count = Table::Count('coupon', $countcon);
            
            $required = $order_item['quantity'] - $count;
            
            if ( $required <= 0 )
                return;
            
            $ccon = array('order_id' => '0',
                'user_id' => '0',
                'team_id' => $order_item['team_id'] );
            if ( $order_item['option_id'])
                $ccon['option_id'] = $order_item['option_id'];
            
            $coupons = DB::LimitQuery('coupon', array ( 
                'condition' => $ccon,
                'size' => $required
                )
               ) ;
            
            

		
            foreach ( $coupons as $coupon )
            {
              $coupon['order_id'] = $order_item['id'];
              $coupon['user_id'] = $user_id;
              //$coupon['order_item_id'] = 
              Table::UpdateCache('coupon', $coupon['id'], $coupon);
                
              if ( Table::Count('coupon', $countcon) >= $order_item['quantity'] )
                      break;
                 
            }
        }
        
	static public function Create($order_item, $user_id) {
                
                self::AssignAdvance($order_item, $user_id);
                
		$team = Table::Fetch('team', $order_item['team_id']);
		$partner = Table::Fetch('partner', $team['partner_id']);
		$ccon = array('order_id' => $order_item['id']);
		$count = Table::Count('coupon', $ccon);
		// default 09/03/2015 00:00:00 add 86399 to make it 09/03/2015 23:59:59  date('m/d/Y H:i:s', $team['expire_time']+86399);
		$expireTime = 0;
		if(!empty($team['expire_time']))
		{
			$expireTime = $team['expire_time']+86399;
		}
		while($count<$order_item['quantity']) {
			$id = Utility::GenSecret(8, Utility::CHAR_NUM);
			$id = Utility::VerifyCode($id);
			$cv = Table::Fetch('coupon', $id);
			if ($cv) continue;
			$coupon = array(
					'id' => $id,
					'user_id' => $user_id,
					'partner_id' => $team['partner_id'],
					'order_id' => $order_item['id'],
					'credit' => $team['credit'],
					'team_id' => $order_item['team_id'],
					'secret' => Utility::GenSecret(6, Utility::CHAR_WORD),
					'expire_time' => $expireTime,
					'create_time' => time(),
                                        'option_id' => $order_item['option_id']
					);
			DB::Insert('coupon', $coupon);
			//sms_coupon($coupon);
			$count = Table::Count('coupon', $ccon);
		}
	}

        static public function GetCouponForPrint($id)
        {
            $coupon = Table::Fetch('coupon', $id);

            if (!$coupon) {
                return false;
            }

            $c['id'] = $id;
            $c['coupon'] = $coupon;
            $c['partner'] = Table::Fetch('partner', $coupon['partner_id']);
            $c['team'] = Table::Fetch('team', $coupon['team_id']);
            $c['user'] = Table::Fetch('user', $coupon['user_id']);
            
            $order_item = Table::Fetch('order_item', $coupon['order_id']);
            $c['order'] = Table::Fetch('order', $order_item['order_id'] );
            $c['order_item'] = $order_item;
            return $c;
        }
        
        static public function AdvanceCreate($advance) {
		$team = Table::Fetch('team', $advance['team_id']);
                $multi = Table::Fetch('team_multi', array($team['id']), 'team_id');
		//$partner = Table::Fetch('partner', $advance['partner_id']);
		//$ccon = array('order_id' => $order['id']);
		//$count = Table::Count('coupon', $ccon);
                $count = 0;
		while($count<$advance['quantity']) {
                        if ($multi)
                        {
                            // e.g 100 coupons, 10 options: 0/10 = 0, 5/10 = 0, 9/10 = 0, 10/10 = 1, 15/10 = 1, 50/10 = 5, 99/10 = 9
                            // e.g 20 coupons, 2 options: 0/2 = 0, 2/2 = 1, 15/2 = 7
                            $div = intval(ceil($advance['quantity']/count($multi)));
                            $multi_id = $multi[ intval($count / $div) ]['id'];
                        }else $multi_id = 0;
                        
			$id = Utility::GenSecret(8, Utility::CHAR_NUM);
			$id = Utility::VerifyCode($id);
			$cv = Table::Fetch('coupon', $id);
			if ($cv) continue;
			$coupon = array(
					'id' => $id,
					//'user_id' => $order['user_id'],
					'partner_id' => $team['partner_id'],
					//'order_id' => $order['id'],
					'credit' => $team['credit'],
					'team_id' => $team['id'],
					'secret' => Utility::GenSecret(6, Utility::CHAR_WORD),
					'expire_time' => $team['expire_time'],
					'create_time' => time(),
                                        'option_id' => $multi_id,
					);
			DB::Insert('coupon', $coupon);
			$count++;
		}
	}
        
        static public function UnConsume($coupon){
            if ( !$coupon['consume']=='Y' ) return false;
                    $u = array(
                            'consume' => 'N',
                    );
                    Table::UpdateCache('coupon', $coupon['id'], $u);
                    
                if ( $coupon['credit'] <= 0 ) return 0;

		//update user money;
		$user = Table::Fetch('user', $coupon['user_id']);
		Table::UpdateCache('user', $coupon['user_id'], array(
					'money' => array( "money - {$coupon['credit']}" ),
					));

		$u = array(
				'user_id' => $coupon['user_id'],
				'money' => $coupon['credit'],
				'direction' => 'expense',
				'action' => 'coupon',
				'detail_id' => $coupon['id'],
				'create_time' => time(),
				);
		return DB::Insert('flow', $u);
         }
}
