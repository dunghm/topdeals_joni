<?php
require_once dirname(__FILE__).'/PaymentDTA/DTA.php';

class DTAExport
{
    public static function GetDTAFile($sender, $transactions)
    {
        /**
        * Initialize new DTA file.
        * In this example the file contains credits.
        * This means that in an exchange the sender is the person who pays money
        * to the receiver.
        */

        $dta_file = new DTA(DTA_CREDIT);

        /**
        * Set file sender. This is also the default sender for transactions.
        */

        $dta_file->setAccountFileSender(
            array(
                "name"           => $sender['name'], //"Michael Mustermann",
                "bank_code"      => $sender['bank_code'], //1112222,
                "account_number" => $sender['account_number'], //654321
            )
        );

        /**
        * Add transaction.
        */
        foreach($transactions as $txn){
            $dta_file->addExchange(
                $txn['recipient'],
                $txn['amount'],                                      // Amount of money.
                $txn['info']
            );
        }
        
        /**
        * Output DTA-File.
        */

        echo $dta_file->getFileContent();

    }
}
?>
