<?php


class Basket
{
    var $user_id;
    var $items;
    var $expire_at;
    var $create_at;
    var $expire_period;
    var $delivery;
	
    public function __construct($user_id, $expiry = 900) {
        $this->user_id = $user_id;
        $this->items = array();
        $this->expire_period = $expiry;
        $this->create_at = time();
        $this->expire_at = $this->create_at +  $this->expire_period;
        
        $this->Load();
    }
    
    public function Load(){
        $condition = array( 'user_id' => $this->user_id);        
        $order = "ORDER BY create_time ASC";
        $this->items = DB::LimitQuery('basket', array(
                'condition' => $condition,
                'order' => $order
                    ));
        
        if ( count($this->items) > 0 )
        {
            $this->create_at = $this->items[0]['create_time'];
            $this->expire_at = $this->items[0]['expire_time'];
        }
        
        if ( $this->expire_at < time() )
        {
            $this->ClearBasket();
        }
        
    }
    
    public function ClearBasket()
    {
        $this->create_at = time();
        $this->expire_at = $this->create_at +  $this->expire_period;
        $this->items = array();
        $this->Save();
    }
    
    public function Save($action='')
    {        
		#count of basket item
		$basketItemsCount = $this->getBasketItemsCountByUserId();

		#count of total item
		$itemsAddedCount = count($this->items);	
		
		if($basketItemsCount!=$itemsAddedCount)
		{
			Table::Delete('basket',$this->user_id,'user_id');
			
			$insert_fields = array('create_time', 
                'expire_time', 'user_id','team_id','quantity','price','mode', 'option_id', 'delivery');
			#first time count 1
			foreach( $this->items as &$item)
			{
				
				$item['delivery'] = $this->delivery;
				
				if($action=='add')
				{
					#add 300 is 5 min 
					if($itemsAddedCount>1)
					{
						$item['expire_time'] = $this->expire_at +300;	
					}
				}
				
				$basket = new Table('basket', $item);
				
			  
				
				$basket->insert($insert_fields);
			}	
			
		}
		else
		{
			Table::Delete('basket',$this->user_id,'user_id');
			$insert_fields = array('create_time', 
					'expire_time', 'user_id','team_id','quantity','price','mode', 'option_id', 'delivery');
			foreach( $this->items as &$item)
			{
				$item['delivery'] = $this->delivery;
				
				$basket = new Table('basket', $item);
				
			   
				
				$basket->insert($insert_fields);
			}
		}
    }
    
    public function Update($team_id, $quantity, $price, $mode, $option = false)
    {
        foreach( $this->items as &$item )
        {
            if ( $item['team_id'] == $team_id && $item['mode'] == $mode && 
                    (!$option || ($option && $option > 0 && $item['option_id'] == $option) ))
            {
                $item['quantity'] = $quantity;
                return;
            }
        }
        
        // we are here, this means we could not find the Team in basket
        return $this->Add($team_id, $quantity, $price, $mode, $option);
    }
    
    public function Update_gift($team_id, $option = false, $mode='gift'){
        foreach( $this->items as &$item )
        {
            if ( $item['team_id'] == $team_id && 
                    (!$option || ($option && $option > 0 && $item['option_id'] == $option) ))
            {
                $item['mode'] = $mode;
                return;
            }
        }
    }
    
    public function Add($team_id, $quantity, $price, $mode, $option = false){
        foreach( $this->items as &$item )
        {
            if ( $item['team_id'] == $team_id && $item['mode'] == $mode && 
                (!$option || ($option && $option > 0 && $item['option_id'] == $option) ))
            {
                $item['quantity'] += $quantity;
                return;
            }
        }
        
        $new_item = array(
            'create_time' => $this->create_at,
            'expire_time' => $this->expire_at,
            'user_id' => $this->user_id,
            'team_id' => $team_id,
            'quantity' => $quantity,
            'price' => $price,
            'mode' => $mode,
            'option_id' => ($option?$option : 'NULL'),
			'delivery' => $this->delivery,
        );
      
        
        $this->items[] = $new_item;
    }
    
    public function Remove($team_id, $mode, $option = false)
    {
        foreach( $this->items as $key => $item )
        {
            if ( $item['team_id'] == $team_id && $item['mode'] == $mode && 
                 (!$option || ($option && $option > 0 && $item['option_id'] == $option) ) )
            {
                unset($this->items[$key]);
                return;
            }
        }
    }
    
    public function GetQuantity($team_id, $option = false)
    {
        $qty = 0;
        foreach( $this->items as $key => $item )
        {
            if ( $item['team_id'] == $team_id && 
                (!$option || ($option && $option > 0 && $item['option_id'] == $option)))
            {
               $qty += $item['quantity'];
            }
        }
        return $qty;
    }
    
    public function GetItems()
    {
        return $this->items;
    }
    
    public function TimeLeft()
    {
        return ($this->expire_at - time());
    }
    
	public function SetDelivery($delivery)
	{
		$this->delivery = $delivery;
	}
	
    public static function GetDealCountInBasket($team_id, $option_id = false)
    {
        $cut_off = time();
        $sql = "select sum(quantity) total from basket where team_id = {$team_id} AND expire_time > {$cut_off}";
        
        if ($option_id)
            $sql .= " AND option_id = {$option_id}";
            
        $result = DB::GetQueryResult($sql,true);
        
        return $result['total'];
    }
	
	
	public function getBasketItemsCountByUserId()
	{
		$itemsInDBCount = 0;
		#get basket table item by user id
		$condition = array( 'user_id' => $this->user_id);
		$order = "ORDER BY create_time ASC";
		$itemsInDB = DB::LimitQuery('basket', array(
                'condition' => $condition,
                'order' => $order
                    ));
					
		#count of basket item
		$itemsInDBCount = count($itemsInDB);
		
		return $itemsInDBCount;
	}
} 

?>
