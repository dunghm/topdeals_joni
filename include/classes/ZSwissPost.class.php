<?php

class ZSwissPost{
    
    static $SOAP_Client;
    
    static function GenerateLabels(&$order_items, &$orders){
        global $INI;
        
        $frankinglicense = $INI['swisspost']['frankinglicense'];
        
        $imgfile = dirname(dirname(dirname(__FILE__))).'/static/images/logo-label.gif';
        
        $logo_binary_data = fread(fopen($imgfile, "r"), filesize($imgfile));
        $ident_codes = array();
        
       // foreach( $order_items as $oi ){
           
            $item_results = self::GetItemLicense($order_items, $orders);
           
            $iterator_j =0;
            foreach($item_results as $item_result ){
                
            $item = $item_result['item'];
            $frankinglicense = $item_result['license'];
            $ident_codes = array();
            
            //for($i =1; $i <= $oi['quantity']; $i++){
            $generateLabelRequest = array(
                'Language' => 'en',
                'Envelope' => array(
                    'LabelDefinition' => array(
                            'LabelLayout' => 'A7',
                            'PrintAddresses' => 'RecipientAndCustomer',
                            'ImageFileType' => 'PDF',
                            'ImageResolution' => 200,
                                    'PrintPreview' => false
                    ),
                'FileInfos' => array(
                         'FrankingLicense' => $frankinglicense,
			 'PpFranking' => false,
                         'Customer' => array(
                            'Name1' => 'Topdeal',
                            // 'Name2' => 'Generalagentur',
                            'Street' => 'Rue du Centre',
                            // 'POBox' => 'Postfach 600',
                            'ZIP' => '1025',
                            'City' => 'St-Sulpice',
                            'Country' => 'CH',
                            'Logo' => $logo_binary_data,
                            'LogoFormat' => 'GIF',
                            'DomicilePostOffice' => '1025 St-Sulpice'
                        ),
			'CustomerSystem' => 'PHP Client System'
                	),
                'Data' => array(
                    'Provider' => array(
                        'Sending' => array(
	            			'Item' => 
                                            $item,
					
				)
			)
		)
            )
        );
    
        
        $response = null;
        
                $SOAP_wsdl_file_path = __DIR__.'/barcode_v2_2.wsdl';
                $SOAP_config = array(
                    'location' => $INI['swisspost']['endpoint_url'],
                    'login' => $INI['swisspost']['username'],
                    'password' => $INI['swisspost']['pwd'],
                    'trace'=> 1
                );
                
        try {
             $SOAP_Client = new SoapClient($SOAP_wsdl_file_path, $SOAP_config);
        }
        catch (SoapFault $fault) {
            echo('Error in SOAP Initialization: '. $fault -> __toString() .'<br/>');
            exit;
        }
        
        try{
                $response = $SOAP_Client->GenerateLabel($generateLabelRequest);
        }
        catch (SoapFault $fault) {
                echo('Error in GenerateLabel: '. $fault -> __toString() .'<br />');
                exit;
        }

        
        foreach (self::getElements($response->Envelope->Data->Provider->Sending->Item) as $item) {
                if ($item->Errors != null) {

                    $errorMessages = "";
                    $delimiter="";
                    foreach (self::getElements($item->Errors->Error) as $error) {
                            $errorMessages .= $delimiter.$error->Message;
                            $delimiter=",";
                    }
                    //echo '<p>ERROR for item with itemID = '.$item->ItemID.": ".$errorMessages.'.<br/></p>';
                    
                    //echo "REQUEST:\n" . $SOAP_Client->__getLastRequest() . "\n";
                    return array('order'=> $item->ItemID, 'message'=> $errorMessages);
                   exit;
                }
                else {
                     
                        $identCode = $item->IdentCode;
                        $labelBinaryData = $item->Label;
                        
                        // Save the binary image data to image file:
                        $filename = dirname(dirname(dirname(__FILE__))).'/generated_labels/label_'.$identCode.'.pdf';
                        file_put_contents($filename, $labelBinaryData);
                        
                        
                        //$iden_code_string = implode(',', $ident_codes);
                        
                       /* 
                        $itr = 0;
                        foreach($order_items as $oi){
                            
                          $order_item_i = Table::FetchForce('order_item', $oi['id']);
                            if($order_item_i['shipment_identcode'] != '' || $order_item_i['shipment_identcode'] != 0 )
                            { 
                                $identCode2 = $order_item_i['shipment_identcode'].','.$identCode;
                            }
                            else{
                                $identCode2 = $identCode;
                            }
                            Table::UpdateCache('order_item', $oi['id'], array("shipment_identcode" =>$identCode2, 'shipment_status'=> 1));
                            $itr++;
                        }     
                       */
                       $item_ids = $item->ItemID;
                       $item_arr = explode('-', $item_ids);
                       
                       foreach($item_arr as $item_id ){
                           
//                           $order_item_i = Table::FetchForce('order_item', $item_id);
//                            
//                           if($order_item_i['shipment_identcode'] != '' || $order_item_i['shipment_identcode'] != 0 )
//                            { 
//                                $identCode2 = $order_item_i['shipment_identcode'].','.$identCode;
//                            }
//                            else{
//                                $identCode2 = $identCode;
//                            }
                            $order_item_i = Table::FetchForce('order_item', $item_id);
                            
                            if($order_item_i['shipment_identcode'] != ''){
                                if(!strstr($order_item_i['shipment_identcode'], $identCode))
                                {
                                    $identCode = $order_item_i['shipment_identcode'].','.$identCode;
                                }
                                else{
                                    $identCode = $order_item_i['shipment_identcode'];
                                }
                            }
                            
                            Table::UpdateCache('order_item', $item_id, array("shipment_identcode" =>$identCode, 'shipment_status'=> 1));
                            
                       }
                         // Table::UpdateCache('order_item', $item->ItemID, array("shipment_identcode" =>$identCode, 'shipment_status'=> 1));
                        //$ident_codes[] = $identCode;
                        //
                        // Printout some label information (and warnings, if any):
                        //echo '<p>Label generated successfully for identCode='.$identCode.': <br/>';
                        if ($item->Warnings != null) {
                                $warningMessages = "";
                                foreach (self::getElements($item->Warnings->Warning) as $warning) {
                                        $warningMessages .= $warning->Message.",";
                                }
                        //echo 'with WARNINGS: '.$warningMessages.'.<br/>';
                        }
                        
                        //echo $filename.':<br/><img src="'.$filename.'"/><br/>';
                        //echo '</p>';
                        //return TRUE;
                }
        }
        //$iden_code_string = implode(',', $ident_codes);
        //Table::UpdateCache('order_item', $item->ItemID, array("shipment_identcode" =>$iden_code_string, 'shipment_status'=> 1));
        
          //  }
        $iterator_j++;
       }
       
       return TRUE;
    }
    
    static function GetItemLicense($order_items, $order){
        
            $density_arr_1 = array();
            $item_id_arr = array();
            
            foreach($order_items as $oi ){
                
                                Table::UpdateCache('order_item', $oi['id'], array("shipment_identcode" =>''));
                 
                for($i=0; $i < $oi['quantity']; $i++){
                    $item_id_arr[] = $oi['id'];
                    if( isset($oi['option_id'])){
                        $option = Table::Fetch('team_multi', $oi['option_id']);
                        if( $option['volume_length'] !='' && $option['volume_weight'] != ''){
                            $density_arr_1[] = array('length'=> $option['volume_length'] , 'width'=> $option['volume_breadth'], 'height'=> $option['volume_height'], 'weight'=> $option['volume_weight'] );
                        }
                        else{
                            $team = Table::Fetch('team', $oi['team_id']);
                            $density_arr_1[] = array('length'=> $team['volume_length'] , 'width'=> $team['volume_breadth'], 'height'=> $team['volume_height'], 'weight'=> $team['volume_weight'] );
                        }
                        //$density_arr_1[] = array('length'=> $option['volume_length'] , 'width'=> $option['volume_breadth'], 'height'=> $option['volume_height'], 'weight'=> $option['volume_weight'] );
                    }
                    else{
                        $team = Table::Fetch('team', $oi['team_id']);
                        $density_arr_1[] = array('length'=> $team['volume_length'] , 'width'=> $team['volume_breadth'], 'height'=> $team['volume_height'], 'weight'=> $team['volume_weight'] );
                    }
                }
            }
            $density_arr['density_arr'] = $density_arr_1;
            $density_arr['item_id'] = $item_id_arr;
            //print_r_r($team);
            //print_r($density_arr);
            //exit;
            $package_f_code = PackageSelect::select_package( $density_arr);
            
            
            
            if($package_f_code == FALSE){
                Session::Set('notice', 'Shipping Package cant be selected');
                redirect( WEB_ROOT . "/manage/order/generate_barcode.php");
            }
            
            
            $items = array();
            foreach($package_f_code as $f_code){
                
                $p_rr = explode('-', $f_code['package']);
                
                foreach($f_code['order_items'] as $o_items){
                    $shipment_fares = Table::Fetch('shipment_fares', $p_rr[0], 'fares_code');
                    Table::UpdateCache('order_item', $o_items, array("shipment_post_service" =>$shipment_fares['fares_title']));
                }
                 $przl = array();
                $przl[] = $p_rr[0];

                if($p_rr[0] == 'APOST'){
                    $przl[] = 'BLN';
                }
               
                $item = array( 
                        'ItemID' => implode('-',$f_code['order_items']),
                        'Recipient' => array(
                                //'PostIdent' => 'IdentCodeUser',
                                //'Title' => substr($order[$oi['id']]['realname'].$order[$oi['id']]['lastname'],0,30).'...',
                                'Vorname' => $oi['first_name'],
                                'Name1' => $oi['first_name'].' '.$oi['last_name'],
                                //'Name2' => $order[$oi['id']]['lastname'],
                                'Street' => $oi['address'],
                                //'HouseNo' => '21',
                                //'FloorNo' => '1',
                                //'MailboxNo' => '1111',
                               'ZIP' =>  $oi['zipcode'],
                               'City' =>  $oi['region'],
                                //'Country' => 'CH',
                                //'Phone' =>  $order[$oi['id']]['mobile'], // für ZAW3213
                                //'Mobile' => '0793381111',
                                'EMail' => $order[$oi['id']]['email']

                        ),
                        
                        'Attributes' => array(
                                'PRZL' => $przl,
                                //'Amount' => $oi['total'],
                                'ProClima' => false
                        )
                );
                if($p_rr[0] == 'APOST'){
                   $item['AdditionalINFOS'] = array(
                                     //Cash-On-Delivery amount in CHF for service 'BLN':
                                            'AdditionalData' => array(
                                            'Type' => 'NN_BETRAG',
                                                    'Value' => '1.0'
                            ));
                }
                $items[] = array('item' => $item, 'license' => $p_rr[1]);
            }
          
        return $items;
        
    }
    static function getElements($root) {
	if ($root==null) {
		return array();
	}
	if (is_array($root)) {
		return $root;
	}	
	else {
		// simply wrap a single value or object into an array		
		return array($root);
	}
    }
}

