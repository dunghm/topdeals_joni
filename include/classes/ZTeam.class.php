<?php

class ZTeam
{
	static public function DeleteTeam($id) {
		$orders = Table::Fetch('order', array($id), 'team_id');
		foreach( $orders AS $one ) {
			if ($one['state']=='pay') return false;
			if ($order['card_id']) {
				Table::UpdateCache('card', $order['card_id'], array(
					'team_id' => 0,
					'order_id' => 0,
					'consume' => 'N',
				));
			}
			Table::Delete('order', $one['id']);
		}
		return Table::Delete('team', $id);
	}

	/*Function For Tipping*/
	static public function Tipping($order)
	{
            $main_order = $order['id'];
            $order = Table::FetchForce('order', $order['id']);
            $order_items = Table::Fetch('order_item', array($order['id']),'order_id');
            
            foreach ( $order_items as $item )
            {
                $team = Table::FetchForce('team', $item['team_id']);
                $plus = $team['conduser']=='Y' ? 1 : $item['quantity'];
                //echo 'Plus = '.$plus.'<br>';
                $team['now_number'] += $plus;
                //echo 'team[now_number] = '.$team['now_number'].'<br>';
                /* close time */
                if ( $team['max_number']>0 && $team['now_number'] >= $team['max_number'] ) 
                {
                        $team['close_time'] = time();
                }

                $bDealTipped = false;
                /* reach time */
                if ( $team['now_number']>=$team['min_number'] && $team['reach_time'] == 0 ) 
                {
                        $team['reach_time'] = time();
                        // Deal Tipped: email every one
                        $bDealTipped = true;
                }




                Table::UpdateCache('team', $team['id'], array(
                        'close_time' => $team['close_time'],			
                        'reach_time' => $team['reach_time'],
                        'now_number' => array( "`now_number` + {$plus}", ),
                ));

                if (  $item['option_id'] && $item['option_id'] > 0 )
                {
                    Table::UpdateCache('team_multi', $item['option_id'], array(
                        'now_number' => array( "`now_number` + {$plus}")
                    ));
                }

                if ( $bDealTipped )
                {
                     //mail_deal_tipped($team);
                    //echo "<h2>Deal Tipped</h2><br>";
                    //$table = new Table('test');
                    //$table->response = 'Deal Tipped';
                    //$table->my = $order['id'];
                    //$table->insert( array('response', 'my') );			

                    //$order = Table::FetchForce('order', $order['id']);
                    //$dealss = DB::Query("Select * from `order` where team_id = '".$order['team_id']."'"); 
                    /*while($one = mysql_fetch_array($dealss))
                    {
                        $deals = DB::Query("Select * from pay where id = '".$one['pay_id']."' AND transaction_state = 'authorize'"); 
                        while($two = mysql_fetch_array($deals))
                        {
                            $abc = 0;
                            if($two['service'] == 'post')
                            {
                                $abc = 1;
                            }
                            elseif($two['service'] == 'paypal')
                            {
                                $abc = self::paypal_capture($one['id'],$one['team_id'],$one['transaction_id'],$one['price']);
                            }
                            if($abc)
                            {
                                Table::UpdateCache('pay', $two['id'], array('transaction_state' => 'sale',));
                                $tmp = DB::Query("Select id from `order` where pay_id = '".$two['id']."'");
                                if(mysql_num_rows($tmp) == 1)
                                {
                                    $idd = mysql_fetch_array($tmp);
                                    //echo "idd['id'] = ".$idd['id'].'<br>';
                                    if($idd['id'] != $main_order)
                                    {
                                        $ord = Table::FetchForce('order', $idd['id']);
                                        //print_r($ord);
                                        //echo "<br><br><br>";
                                        //* cash flow 
                                        ZFlow::CreateFromOrder($ord); // Deduct Money from User balance if paid through Balance
                                        ///* order : send coupon ? 
                                        ZCoupon::CheckOrder($ord);   // Create Coupons
                                        ///* order : invite buy 
                                        ZInvite::CheckInvite($ord);
                                        ///* credit 
                                        if ( $ord['state'] == 'pay' )
                                                ZCredit::Buy($ord['user_id'], $ord);
                                        $user = Table::Fetch('user', $ord['user_id']);
                                        $team = Table::FetchForce('team', $ord['team_id']);	
                                        //print_r($team);
                                        //echo "<br><br><br>";									
                                    }
                                }
                            }	
                        }	
                    }*/                   
                }
            }            
	}


        static public function paypal_capture($ord_id,$deal_id,$trid,$amt)
        {
                $environment = 'sandbox';
                $authorizationID = urlencode($trid);
                $amount = urlencode($amt);
                $currency_code = $INI['system']['currencyname'];
                $currency = urlencode($currency_code);						// or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
                $completeCodeType = urlencode('NotComplete');				// or 'NotComplete'
                $invoiceID = urlencode('');
                $note = urlencode('Deal Tipped');

                // Add request-specific fields to the request string.
                $nvpStr="&AUTHORIZATIONID=$authorizationID&AMT=$amount&COMPLETETYPE=$completeCodeType&CURRENCYCODE=$currency&NOTE=$note";	
                // Execute the API operation; see the PPHttpPost function above.
                $httpParsedResponseAr = self::PPHttpPost('DoCapture', $nvpStr);
                if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) 
                {
                        return 1;					
                } 
                else
                {
                        return 0;
                }
        }

        static public function PPHttpPost($methodName_, $nvpStr_) {
                global $environment;


                // Set up your API credentials, PayPal end point, and API version.
                /*$API_UserName = urlencode('farhan_1310623046_biz_api1.laeffect.com');
                $API_Password = urlencode('1310623083');
                $API_Signature = urlencode('A6BGSgxnAbLHP14IPOdYY0ZGAmsUAA6xDkKb0J82wOj-NWdApolNrQoo');
                $API_Endpoint = "https://api-3t.paypal.com/nvp";
                if("sandbox" === $environment || "beta-sandbox" === $environment) {
                        $API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
                }*/
                $API_UserName = urlencode('faheem_1316787777_biz_api1.gmail.com');
                $API_Password = urlencode('1316787814');
                $API_Signature = urlencode('AFcWxV21C7fd0v3bYYYRCpSSRl31AcaNocXpe7VEs83G8hgDQVtk0BY4');
                $API_Endpoint = "https://api-3t.sandbox.paypal.com/nvp";	
                $version = urlencode('51.0');

                // Set the curl parameters.
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
                curl_setopt($ch, CURLOPT_VERBOSE, 1);

                // Turn off the server and peer verification (TrustManager Concept).
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);

                // Set the API operation, version, and API signature in the request.
                $nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";

                // Set the request as a POST FIELD for curl.
                curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

                // Get response from the server.
                $httpResponse = curl_exec($ch);

                if(!$httpResponse) {
                        exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
                }

                // Extract the response details.
                $httpResponseAr = explode("&", $httpResponse);

                $httpParsedResponseAr = array();
                foreach ($httpResponseAr as $i => $value) {
                        $tmpAr = explode("=", $value);
                        if(sizeof($tmpAr) > 1) {
                                $httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
                        }
                }

                if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
                        exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
                }

                return $httpParsedResponseAr;
        }
        static public function BuyOne($order, $bOnlyPostBuying = false) {

				$log = new KLogger(dirname(dirname(dirname(__FILE__)))."/include/logs/", KLogger::DEBUG);
				$log->logInfo("ZTeam::BuyOne Method Hit For {$order['id']} , Current Status of Order is {$order['state']} & Service Use for Order is {$order['service']}");
				
				$log->logError("BuyOne Method Hit For {$order['id']} -> Check bOnlyPostBuying Variable ");
                if ( $bOnlyPostBuying == false ){
						$log->logError("BuyOne Method Hit For {$order['id']} -> Call Tipping Method");
                        self::Tipping($order);
				}
                /* cash flow */
				$log->logError("BuyOne Method Hit For {$order['id']} -> Call ZFlow::CreateFromOrder Method");
                ZFlow::CreateFromOrder($order); // Deduct Money from User balance if paid through Balance
                /* order : send coupon ? */
				$log->logError("BuyOne Method Hit For {$order['id']} -> Call ZCoupon::CheckOrder Method");
                ZCoupon::CheckOrder($order);   // Create Coupons
                /* order : invite buy */
				$log->logError("BuyOne Method Hit For {$order['id']} -> Call Invite::CheckInvite Method");
                ZInvite::CheckInvite($order);
                /* credit */
                if ( $order['state'] == 'pay' ){
					$log->logError("BuyOne Method Hit For {$order['id']} -> Call ZCredit::Buy Method");
                    ZCredit::Buy($order['user_id'], $order);
				}

                $user = Table::Fetch('user', $order['user_id']);
				
				//CHECK CASH BACK ORDER ITEM
				$log->logError("BuyOne Method Hit For {$order['id']} -> Call orderCashBackItem Method");
				orderCashBackItem($order['id']);

                //$order_items = Table::Fetch('order_item', $order['id'], 'order_id');
                //$team = Table::FetchForce('team', $order_items['team_id']);
				$log->logError("BuyOne Method Hit For {$order['id']} -> Send Success Order Email");
                mail_order_success($user,$order,false);
        }
        
        static public function GetTeamUrl(&$team, $primary = false, $absolute=false)
        {
            global $INI;
            if ( $absolute )
                $url = $INI['system']['wwwprefix'];
            else
                $url = "";
                
            if ( strlen(trim($team['seo_title'])) > 0 )
            {
                $category = Table::Fetch('category', $team['group_id']);
                if ( $primary )
                    $url .= "/deal_du_jour/";
                else
                    $url .= "/deals/";
                
                $url .= strtolower($category['name'])."/{$team['seo_title']}";
            }
            else
            {
                $url .= "/team.php?id={$team['id']}";
                
                if ( $primary )
                    $url .= "&primary=1";
            }
                
            return $url;
        }
        
        static public function GetMeta($team_id) {
            $metas = DB::LimitQuery('team_meta', array ( 'condition' => array( 'team_id' => $team_id ) ));
            $team_meta = array();
            foreach($metas as $meta)
            {
                $arrKey = strtolower($meta['key']).'_'.$meta['lang'];
                $team_meta[$arrKey] = $meta['value'];
            }
            
            return $team_meta;
        }


       static public function Search($str, $checkseo = true, $checktime = true, $supplier_id = 0, $system = 'Y',$tag_id =0 ) {                       
			//$query = "select * from team where (title like '%".$str."%' or title_fr like '%".$str."%') and system='Y'";
			$query = "SELECT * FROM team";
                        $where = false;     
                        
                        if($str != '')
						{
                            $where = true;
                            $query .= " WHERE ( ";
							
							#call Partial search method and pass colmun names
							$columnNames = array();
							
							$columnNames = array(
								'id'				=> 'id',
								'title'				=> 'title',
								'seo_description'	=> 'seo_description', 	// Emotional Text
								'summary'			=> 'summary', 			// Nous vous proposons
								'notice'			=> 'notice',			//vous aimerez
								'systemreview'		=> 'systemreview',		//technical information	
								'seo_keyword' 		=> 'seo_keyword'			
								);
							
							$query.= PartialSearch::partialSearchQuery($columnNames,$str);
							
							/*  $query .= " WHERE ( MATCH(`title`) AGAINST('{$str}' in boolean mode) ";
                             //$query .= " OR MATCH(`title_fr`) AGAINST ('{$str}' IN boolean mode) ";

                             if ( $checkseo )
                             {
                                     $query .= " OR MATCH(`seo_description`) AGAINST ('{$str}' IN boolean mode)";
                             } */

                             $query .= " ) ";
                        }     
                        if($supplier_id !=0){
                            
                            if($where)
                                $query .= " AND supplier_id = $supplier_id";
                            else
                                $query .= " WHERE supplier_id = $supplier_id";
                            
                            $where = true;
                        }
                        
                        if($where)
                            $query .= " AND system='$system'";
                        else
                            $query .= " WHERE system='$system'";
                        
                        
            if ( $checktime )
            {
                $daytime = strtotime(date('Y-m-d'));
                $query = " and end_time > ".$daytime." and begin_time <= ".$daytime;
            }
		
		//	$query .= " ORDER BY relevance DESC";

            //." and now_number >= min_number ";
           // echo $query . '<br>';
            $dealss = DB::GetQueryResult($query, false);
            return $dealss;
        }
        
        static public function SearchWithCategory($str , $old = FALSE, $count = FALSE) {
			$daytime = strtotime(date('Y-m-d'));
			$category_query = "select id from category where zone='group' and name like '%".$str."%'";
			
			$category_res = DB::GetQueryResult($category_query, false);
			$category_ids = Utility::GetColumn($category_res, 'id');
			
			if($category_ids){
				$category_clause = "or group_id in (". implode(",", $category_ids) .")";
			}
			
			$query = "select * from team where (";
			
			#call Partial search method and pass colmun names
			$columnNames = array(
								'title'				=> 'title',
								'seo_description'	=> 'seo_description', 	// Emotional Text
								'summary'			=> 'summary', 			// Nous vous proposons
								'notice'			=> 'notice',			//vous aimerez
								'systemreview'		=> 'systemreview',		//technical information	
								'seo_keyword' 		=> 'seo_keyword'			
								);
			
			$query.= PartialSearch::partialSearchQuery($columnNames,$str);	
						/* $query .= " MATCH(`title`) AGAINST('{$str}' in boolean mode) ";
						// Emotional Text
                        $query .= " OR MATCH(`seo_description`) AGAINST ('{$str}' IN boolean mode)";
                        // Nous vous proposons
                        $query .= " OR MATCH(`summary`) AGAINST ('{$str}' IN boolean mode)";
                        //vous aimerez
                        $query .= " OR MATCH(`notice`) AGAINST ('{$str}' IN boolean mode)";
                        //technical information
                        $query .= " OR MATCH(`systemreview`) AGAINST ('{$str}' IN boolean mode)";
                        $query .= " OR MATCH(`seo_keyword`) AGAINST ('{$str}' IN boolean mode)"; */
                        
                        $query .= " {$category_clause}) and system='Y'";
                        if($old == false){
                            $query .= " and end_time > ".$daytime." and begin_time <= ".$daytime;
                        }
                        else{
                            $query .= " and begin_time <= ".$daytime;
                        }	
			//." and now_number >= min_number ";
		        //echo $query . '<br>';
				
                        if($count == TRUE){
                            $sql_count = " 
                                SELECT COUNT(*) AS count FROM ( $query ) AS t1";
                            $result_count = DB::GetQueryResult($sql_count, false);

                            $count = $result_count[0]['count'];
                            return $count;
                        }
                        else{
                            $dealss = DB::GetQueryResult($query, false);
                            return $dealss;
                        }
        }
  
        
        static public function CheckInventoryAvailability(&$team, $order_id, &$option = false)
        {
            if ( !$team || !$order_id || $order_id <= 0 )
                return false;
            
            if ( $team['max_number'] > 0 || ($option && $option['max_number'] > 0) )
            {
                $deal_quantity_left = $team['max_number'] - $team['now_number'];
               
                $option_quantity_left = $option['max_number'] - $option['now_number'];
                
                
		if ( ( $team['max_number'] > 0 && $deal_quantity_left > 0) ||
			( $option && $option['max_number'] > 0 && $option_quantity_left > 0) )
                {
                    // Some quantity left. Lets check how many are in 
                    // Payment phase

                     $last_active_time = time() - 600;

                    // All orders including this one
                    $condition = array ( 
                        'team_id' => $team['id'],
                        'state' => 'unpay',
                        "create_time > {$last_active_time}",
                        "id <= {$order_id}"                    
                    );
                    
                              
                    if ( $team['conduser']=='Y' )
                    {
                        // each order counts 1
                        $unpaid_quantity = Table::Count('order', $condition);
                        //echo "debug: unpaid qty by # of Orders: {$unpaid_quantity}";
                    }
                    else
                    {
                        // each order's quantity counts
                        $unpaid_quantity = Table::Count('order', $condition, 'quantity');
                        //echo "debug: unpaid qty by order qty: {$unpaid_quantity}";
                    }


                    if ( $team['max_number'] >0 && 
		    	$unpaid_quantity > $deal_quantity_left )
                    {
                        // Oops!!! this Order has caused the violation of
                        // max allowed. 
                         //echo "debug: unpaid qty >  qty left";
                        $error = true;
                        return false;
                    }
                    
                    if ( $option && $option['max_number'] > 0 )
                    {
                        $condition['option_id'] = $option['id'];
                    
                        if ( $team['conduser']=='Y' )
                        {
                            // each order counts 1
                            $unpaid_quantity = Table::Count('order', $condition);
                            //echo "debug: unpaid option qty by # of Orders: {$unpaid_quantity}";
                        }
                        else
                        {
                            // each order's quantity counts
                            $unpaid_quantity = Table::Count('order', $condition, 'quantity');
                            //echo "debug: unpaid qty by ordre qty: {$unpaid_quantity}";
                        }

                        if ( $unpaid_quantity > $option_quantity_left )
                        {
                            // Oops!!! this Order has caused the violation of
                            // max allowed. 
                            //echo "debug: unpaid qty > option qty left";
                            $error = true;
                            return false;
                        }
                    }
                    
                }
                else
                {
                    //echo "debug: no qty left";
                    // no quantity left, this order can't be allowed
                    $error = true;
                    return false;
                }
        }
        return true;
    }
    
    public static function DeliveryTypeDescription($delivery)
    {
        switch ($delivery)
        {
            case    'coupon':
                return 'Bons à imprimer';
            case    'pickup':
                return 'Retrait de la marchandise au bureau TopDeal';
            case    'express':
                return  'Envoi à une adresse de livraison';
            case    'express_pickup':
                return  'Retrait de la marchandise au bureau TopDeal ou envoi à une adresse de livraison';
        }
        
        return "Unknown";
    }
    
    public static function GetRecommended($count, $team, $user_id = false)
    {
        if ( $team )
            $category_id = $team['group_id'];
        else if ( $user_id )
        {
            $last_view_team = DB::LimitQuery('deal_view_history', array(
                'condition'=>array('user_id'=>$user_id),
                'order'=> 'order by id desc',
                'one'=>true,));
            if ($last_view_team)
                $category_id = $last_view_team['category_id'];
        }
        else
            return array();
        $now = time(0);
        $condition = array("end_time > {$now}",
                            "begin_time <= {$now}",
                            "close_time" => 0,);
       if ( $team )
            $condition[] = "id <> {$team['id']}";
        
        if ( $category_id )
            $condition['group_id'] = $category_id;
        $order = 'order by sort_order DESC';
        
        $teams = DB::LimitQuery('team', array('condition'=>$condition,
            'order'=>$order,
            'size'=>$count));
        
        
        return $teams;
    }
    
    public static function GetUserBuyLimit(&$team, $option, $user_id)
    {
        $can_purchase = 0;
        
        if ($option && $option['per_number'] >0) {          
           $sql = "SELECT ifnull(SUM(order_item.quantity),0) AS now_count FROM order_item left join `order` on order_id = `order`.id where user_id = {$user_id} AND state = 'pay' AND order_item.option_id = {$option['id']}";
           $count_result = DB::GetQueryResult($sql, true);
           $now_count = $count_result['now_count'];
           $can_purchase = $option['per_number'] - $now_count;
           if($can_purchase == 0){
               $can_purchase = -1;
           }
        }
        else if ($team && $team['per_number']>0) {          
           $sql = "SELECT ifnull(SUM(order_item.quantity),0) AS now_count FROM order_item left join `order` on order_id = `order`.id where user_id = {$user_id} AND state = 'pay' AND order_item.team_id = {$team['id']}";
           $count_result = DB::GetQueryResult($sql, true);
           $now_count = $count_result['now_count'];
           $can_purchase = $team['per_number'] - $now_count;
           if($can_purchase == 0){
               $can_purchase = -1;
           }
        }
        
        return $can_purchase;
    }
    
    public static function GetStockLeft(&$team, $option)
    {
        $stock_left = -1;
        if($option){
            
            $upper_limit = ( $option['max_number'] > 0 ) ? $option['max_number'] : $team['max_number'];
        }
        else{
            $upper_limit = $team['max_number'];
        }
        
        if ( $upper_limit > 0 )
        {
            $option_id = ($option && $option['max_number'] > 0) ? $option['id'] : false; 
            $in_basket = Basket::GetDealCountInBasket($team['id'], $option_id);
            $in_payment = ZOrder::GetDealCountInPaymentProcessing($team['id'], $option_id);
            
            
            if($option){
                
                $stock_left = $upper_limit - ($option['now_number'] + $in_basket + $in_payment);
               // echo 'stock left ='.$stock_left.'<br>';
            }
            else{
                $stock_left = $team['max_number'] - ($team['now_number'] + $in_basket + $in_payment);
            }
            
        }
        //echo $stock_left;
        
        return $stock_left;
    }
    public static function GetUserBasketMaxQuantity(&$team, $option, $user_id, $basket_quantity){
        
            $max_quantity = ZTeam::GetStockLeft($team, $option);
            $per_number = ZTeam::GetUserBuyLimit($team, $option, $user_id);
           
           
           if($max_quantity >= 0){
                $max_quantity = $max_quantity + $basket_quantity;
           }
            
           if ($per_number < 0){
                   $max_quantity = -1;
           }
           else{
		   if($per_number > 0){
                        if($max_quantity == -1){
                             $max_quantity = $per_number;
                        }else{
                            if ($max_quantity >  $per_number){
                                $max_quantity = $per_number;
                            }
                        }   
                   }
           } 
           return $max_quantity;
    }
	
	static public function GetLatestTeamByCategroy($category_id, $exclude)
    {
		$now = time(0);
        $conditions = array(
            "group_id" => $category_id,
            "end_time > {$now}",
            "begin_time <= {$now}",
 //           "sort_order > 0",
        );
      
        if ( isset($exclude) ) 
            $conditions[] = "id != {$exclude}";
            
        $order_cond =  'ORDER BY begin_time DESC';
        
        $latest_team = DB::LimitQuery('team', array(
			'condition' => $conditions,
                        'order' => $order_cond,
                        'size' => 1,       
			));
        
		if ( sizeof($latest_team) > 0 )
			return $latest_team[0];
			
        return $latest_team;
    }
	
	
	static public function addDealPriceHistory($team_price_history=array())
	{
		$teamPriceHistoryID = DB::Insert('team_price_history', $team_price_history);
		return $teamPriceHistoryID;
	}
	
	static public function isParentOptionCount($optionId=0)
	{
		$count = 0;
		$sql_count = "SELECT COUNT(id) as count FROM `team_multi` WHERE parent_option_id ='$optionId' "; 
		$result_count = DB::GetQueryResult($sql_count, false);
		if(is_array($result_count) && count($result_count) >0)
		{
			$count = $result_count[0]['count'];
			
		}
		return $count;
	}
}
?>
