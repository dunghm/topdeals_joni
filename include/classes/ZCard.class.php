<?php
class ZCard
{
	const ERR_NOCARD = -1;
	const ERR_TEAM = -2;
	const ERR_CREDIT = -3;
	const ERR_EXPIRE = -4;
	const ERR_USED = -5;
	const ERR_ORDER = -6;
        const ERR_MIN_AMT = -7;
        const ERR_MAX_USE = -8;
        const ERR_LIM_OVER = -9;

	static public function Explain($errno) {
		switch($errno) {
			case self::ERR_NOCARD : return "Ce code promo n'existe pas";
			case self::ERR_TEAM : return "Ce code promo n'est pas valable pour ce deal";
			case self::ERR_CREDIT : return "Le montant de ce code promo est supérieur au montant maximum autorisé pour les promotions concernant ce deal";
			case self::ERR_EXPIRE : return 'La validité de ce code promo a expiré';
			case self::ERR_USED : return 'Code promo déjà utilisé';
			case self::ERR_ORDER: return 'Un seul code promo par deal uniquement';
			case self::ERR_MIN_AMT: return "Le montant requis pour l'utilisation de ce code promo n'a pas été atteint";
			case self::ERR_MAX_USE: return "Le nombre d'utilisation maximum pour ce code promo a déjà été atteint";
			case self::ERR_LIM_OVER: return 'Vous avez déjà utilisé ce code promo';
			case self::ERR_OTHER: return "Ce code promo ne peut pas être utilisé avec d'autres promotions";
		}
		return 'unknown error';
	}
        
	static public function CheckLimits($card, $order) 
	{
            if ( $card['limit_per_user'] > 0 )
            {
                $sql = "SELECT user_id, COUNT(*) uses FROM order_card LEFT JOIN `order` ON order_id = `order`.id
WHERE state = 'pay' AND user_id = {$order['user_id']} AND order_card.card_id = {$card['id']}
GROUP BY user_id, order_card.card_id";
                $result = DB::GetQueryResult($sql);
                if ( $result['uses'] >= $card['limit_per_user'] )
                    return false;
            }
            
            if ( $card['limit_per_deal'] > 0 )
            {
                $sql = "SELECT order_item.team_id, COUNT(order_item.team_id) uses FROM order_card LEFT JOIN `order` ON order_card.order_id = `order`.id
LEFT JOIN order_item ON order_item.order_id = `order`.id
WHERE state = 'pay' AND order_card.card_id = {$card['id']}
GROUP BY order_item.team_id, order_card.card_id";
                $result = DB::GetQueryResult($sql, false);
                $items = Table::Fetch('order_item', array($order['id']), 'order_id');
                foreach($result as $one)
                {
                    foreach($items as $item)
                        if ( $one['team_id'] == $item['team_id'] &&
                                $one['uses'] >= $card['limit_per_deal'])
                            return false;
                }
            }
            
            if ( $card['limit_per_user_deal'] > 0 )
            {
                $sql = "SELECT user_id, order_item.team_id,COUNT(order_item.team_id) uses, order_card.card_id FROM order_card LEFT JOIN `order` ON order_card.order_id = `order`.id
LEFT JOIN order_item ON order_item.order_id = `order`.id
WHERE state = 'pay'  AND order_card.card_id = {$card['id']} AND user_id = {$order['user_id']} 
GROUP BY user_id ,order_item.team_id, order_card.card_id";
                $result = DB::GetQueryResult($sql, false);
                $items = Table::Fetch('order_item', array($order['id']), 'order_id');
                foreach($result as $one)
                {
                    foreach($items as $item)
                        if ( $one['team_id'] == $item['team_id'] &&
                                $one['uses'] >= $card['limit_per_user_deal'])
                            return false;
                }
                
            }
            return true;
        }
        
        static public function GetMultiUseCardUsageCount($card_id)
        {
            $sql = "SELECT COUNT(*) total FROM order_card LEFT JOIN `order` ON 
order_id = `order`.id WHERE state = 'pay' AND order_card.card_id = '{$card_id}'";
            $result = DB::GetQueryResult($sql);
            return $result['total'];
        }
        
	static public function UseCard($order, $card_id) 
	{
		//if ($order['card_id']) return self::ERR_ORDER;
		$card = Table::Fetch('card', $card_id);
                if ( !$card ){
                    $card = Table::Fetch('card',$card_id, 'promo_code');
                }
                
		if (!$card) return self::ERR_NOCARD;
                $existing = Table::Fetch('order_card',array($order['id']),'order_id');
                foreach($existing as $one){
                    if ( $one['card_id'] == $card['id'] )
                        return self::ERR_ORDER;     // Already used in Order
                    if ( $one['allow_other'] == 'N' || $card['allow_other'] == 'N' )
                        return self::ERR_OTHER;     // Either this card or an existing card in Order doesn't allow Others
                }
                    
		if ($card['max_use'] == 1 && $card['consume'] == 'Y') return self::ERR_USED;
                if ( $card['max_use'] > 1 && self::GetMultiUseCardUsageCount($card_id) >= $card['max_use']) return self::ERR_MAX_USE;
                if ( $card['max_use'] != 1 && !self::CheckLimits($card,$order))
                        return self::ERR_LIM_OVER;
                
		$today = strtotime(date('Y-m-d'));
		if ($card['begin_time'] > $today 
			|| $card['end_time'] < $today ) return self::ERR_EXPIRE;
		
                if ($card['min_order'] > $order['origin'])
                    return self::ERR_MIN_AMT;
                
                
                $teams = ZOrder::GetTeams($order);
                foreach($teams as $team)
                {
                    if ( $card['partner_id'] > 0 
                            && $card['partner_id'] != $team['partner_id'] ) {
                            //any one Deal in the Order doesn't belong to this Partner
                            return self::ERR_TEAM;
                    }
                    if ( $team['card'] > 0 && $team['card'] < $card['credit'] ) 
                        return self::ERR_CREDIT;
                }
                
                //$card_value = $card['value_type'] == 1 ? $card['credit'] : (round(($order['origin']) * $card['discount']/100.0));
                $card_value = $card['value_type'] == 1 ? $card['credit'] : (round((self::GetOrderAmount($order['id'])) * $card['discount']/100.0));
		$finalcard = ($card_value > $order['origin']) 
			? $order['origin'] : $card_value;
	
                // Update order amount
		Table::UpdateCache('order', $order['id'], array(
			//'card_id' => $card_id,
			'card' => array( "card + {$finalcard}"),
			'origin' => array( "origin - {$finalcard}" ),
		));
                      
                // Insert Card record in to DB
                $order_card = array ('order_id' => $order['id'],
                    'card_id' => $card['id'],
                    'card_value' => $finalcard,
                    'ip' => Utility::GetRemoteIp(),
                    );
                
                DB::Insert('order_card', $order_card);
                
                // Consume the Card
		Table::UpdateCache('card', $card['id'], array(
			'consume' => 'Y',
			//'team_id' => $teams[0]['id'],
			'order_id' => $order['id'],
			'ip' => Utility::GetRemoteIp(),
                        'current_use' => array("current_use + 1"),
		));
		return true;
	}

	static public function CardCreate($query) 
	{
		$need = $query['quantity'];
		while(true) {
			$id = Utility::GenSecret(16, Utility::CHAR_NUM);
			$card = array(
					'id' => $id,
					'code' => $query['code'],
					'partner_id' => $query['partner_id'],
					'credit' => $query['money'],
					'consume' => 'N',
					'begin_time' => $query['begin_time'],
					'end_time' => $query['end_time'],
                                        'min_order' => $query['min_order'],
                                        'max_use' => $query['max_use'],
                                        'limit_per_user' => $query['limit_per_user'],
                                        'value_type' => $query['value_type'],
                                        'discount' => $query['discount'],
                                        'limit_per_deal' => $query['limit_per_deal'],
                                        'limit_per_user_deal' => $query['limit_per_user_deal'],
                                        'allow_other' => $query['allow_other'],
                                        'promo_code' => $query['promo_code'],
                            
					);
			$need -= (DB::Insert('card', $card)) ? 1 : 0;
			if ( $need <= 0 ) return true;
		}

		return true;
	}
        
        static private function GetOrderAmount($order_id){
            $order_items = ZOrder::GetItems($order_id);
            $amount = 0;
            if($order_items){
                foreach($order_items as $oi){
                    $amount+=$oi['price'] * $oi['quantity'];
                }
            }
            return $amount;
        } 
}
