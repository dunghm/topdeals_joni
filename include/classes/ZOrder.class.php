<?php
class ZOrder {
    
        const ShipStat_Invalid = 0;
        const ShipStat_Waiting = 1;
        const ShipStat_Available = 2;
        const ShipStat_Delivered = 3;
        const ShipStat_PickedUp = 4;
        const ShipStat_InPrep = 5;
        
        static public  $abc = 4;
        
	static public function OnlineIt($order_id, $pay_id, $money, $currency='CNY', $service='paypal', $bank='paypal', 
                $transaction_state='sale', $transaction_id = ''){ 
				
		$log = new KLogger(dirname(dirname(dirname(__FILE__)))."/include/logs/", KLogger::DEBUG);
		$log->logInfo("Currently On Zorder::OnlineIt Method For Updating Order status Order # {$order_id} & Pay Id : {$pay_id}");
		
		list($_, $_, $quantity, $_) = explode('-', $pay_id);
		
		$log->logError("Order # {$order_id} -> Check Order id, Pay Id & Money");
		
		if (!$order_id || !$pay_id || $money <= 0 ) return false;
		
		$log->logError("Order # {$order_id} -> Check Pass For Order id, Pay Id & Money");
		$log->logError("Order # {$order_id} -> Fetch Order Data");
		
		$order = Table::Fetch('order', $order_id);
		
		$log->logError("Order # {$order_id} -> Check Order Status is unpay (Order Status{$order['state']})");
		
		if ( $order['state'] == 'unpay' ) {
			
			$log->logError("Order # {$order_id} -> Set Order Status Pay");
			
			Table::UpdateCache('order', $order_id, array(
				'pay_id' => $pay_id,
				'money' => $money,
				'state' => 'pay',
				'service' => $service,
				//'quantity' => $quantity,
				'pay_time' => time(),
			));
			$order = Table::FetchForce('order', $order_id);
			
			$log->logError("Order # {$order_id} -> Check Order Status is pay (Order Status{$order['state']})");
			
			if ( $order['state'] == 'pay' ) {
				
				$log->logError("Order # {$order_id} -> Order Status is Pay Now Save Pay Table Data");
				
				$table = new Table('pay');
				$table->id = $pay_id;
				$table->order_id = $order_id;
				$table->money = $money;
				$table->currency = $currency;
				$table->bank = $bank;
				$table->service = $service;
				$table->create_time = time();
				$table->transaction_state = $transaction_state;
				$table->transaction_id = $transaction_id;
				$table->insert( array('id', 'order_id', 'money', 'currency', 'service', 'create_time', 'bank', 'transaction_state','transaction_id') );
				
				//TeamBuy Operation
				$log->logError("Order # {$order_id} -> Calling By One Method");
				
				ZTeam::BuyOne($order);
			}
		}
		/*elseif( $order['state'] == 'pay' ) {
			Table::UpdateCache('order', $order_id, array(
				'transact_id' => $transaction_id,
			));
			$payi = Table::Fetch('pay', $pay_id);
			if ( $payi['order_id'] == $order_id ) {
				Table::UpdateCache('pay', $pay_id, array(
					'transaction_id' => $transaction_id,
					'transaction_state' => 'sale',
				));				
				//TeamBuy Operation
				//ZTeam::BuyOne($order);
			}
		}		
		return true;*/
	}
	
	static public function CaptureIt($order_id, $pay_id, $money, $currency='CNY', $service='paypal', $bank='paypal', 
                $transaction_state='sale', $transaction_id = ''){ 
		list($_, $_, $quantity, $_) = explode('-', $pay_id);
		if (!$order_id || !$pay_id || $money <= 0 ) return false;
		$order = Table::Fetch('order', $order_id);
		if ( $order['state'] == 'pay' ) {
			Table::UpdateCache('order', $order_id, array(
				'transact_id' => $transaction_id,
			));
			$payi = Table::Fetch('pay', $pay_id);
			if ( $payi['order_id'] == $order_id ) {
				Table::UpdateCache('pay', $pay_id, array(
					'transaction_id' => $transaction_id,
					'transaction_state' => 'sale',
				));				
				//TeamBuy Operation
				//ZTeam::BuyOne($order);
			}
		}
		return true;
	}	
		
static public function OnlinePaypal($order_id,$pay_id,$amount,$quantity, $currency,$txn_id, $service='paypal', $bank='paypal'){ 
		$stte = 'Authorize';
		if (!$order_id || !$pay_id || $amount <= 0 ) return false;
		$order = Table::Fetch('order', $order_id);
		if ( $order['state'] == 'unpay' ) {
			Table::UpdateCache('order', $order_id, array(
				'pay_id' => $pay_id,
				'money' => $amount,
				'state' =>'authorize' ,
				'service' => $service,
				'quantity' => $quantity,
				'pay_time' => time(),
				'transact_id' =>$txn_id,
			));

			$order = Table::FetchForce('order', $order_id);
			if ( $order['state'] == 'authorize' ) {
				$table = new Table('pay');
				$table->id = $pay_id;
				$table->order_id = $order_id;
				$table->money = $amount;
				$table->currency = $currency;
				$table->bank = $bank;
				$table->service = $service;
				$table->create_time = time();
				$table->transact_id = $txn_id;
				$table->insert( array('id', 'order_id', 'money', 'currency', 'service', 'create_time', 'bank','transact_id') );
				
				//TeamBuy Operation
				ZTeam::BuyOne($order);
			}
		}
		return true;
	}
	static public function CashIt($order, $mode="cash") {
		global $login_user_id;
                
                if ( $order[ 'state' ] === "pay" )
                    return 0;

                $bOnlyPostBuyProcess = ($order['service'] == 'cashondelivery');
			
		$modeUpdate = "cash";
		if($mode == "wire"){
			$modeUpdate	=	"wire";
		}elseif($mode == "maestro"){
			$modeUpdate	= "maestro";
		}	
		//update order
		Table::UpdateCache('order', $order['id'], array(
			'state' => 'pay',
			'service' => ($modeUpdate),
			'admin_id' => $login_user_id,
			'money' => $order['origin'] - $order['credit'],
			'pay_time' => time(),
		));

		$order = Table::FetchForce('order', $order['id']);
                ZTeam::BuyOne($order, $bOnlyPostBuyProcess);
	}

	static public function CreateFromCharge($money, $user_id, $time,$service) {
		if (!$money || !$user_id || !$time || !$service) return 0;
		$pay_id = "charge-{$user_id}-{$time}";
		$oarray = array(
			'user_id' => $user_id,
			'pay_id' => $pay_id,
			'service' => $service,
			'state' => 'pay',
			'money' => $money,
			'origin' => $money,
			'create_time' => $time,
		);
		return DB::Insert('order', $oarray);
	}


        static public function Cancel($order) {
            global $login_user_id;

            if ( $order[ 'state' ] === "pay" )
                return 0;

            //update order
            Table::UpdateCache('order', $order['id'], array(
                    'state' => 'cancel',
                    'admin_id' => $login_user_id
            ));

            /*
            if ( $order['credit'] > 0 )
            {
                //Pay the Credit/Balance back to User if any
            }
             * */

            /* team -- */
            $team = Table::Fetch('team', $order['team_id']);
            team_state($team);
            if ( $team['state'] != 'failure' ) {
                    $minus = $team['conduser'] == 'Y' ? 1 : $order['quantity'];
                    Table::UpdateCache('team', $team['id'], array(
                                            'now_number' => array( "now_number - {$minus}", ),
                    ));
            }
            /* card refund */
            if ( $order['card_id'] ) {
                    Table::UpdateCache('card', $order['card_id'], array(
                            'consume' => 'N',
                            'team_id' => 0,
                            'order_id' => 0,
                    ));
            }
            /* coupons */
            if ( in_array($team['delivery'], array('coupon', 'pickup') )) {
                    $coupons = Table::Fetch('coupon', array($order['id']), 'order_id');
                    foreach($coupons AS $one) Table::Delete('coupon', $one['id']);
            }

            /* order update */
            Table::UpdateCache('order', $id, array(
                                    'card' => 0,
                                    'card_id' => '',
                                    'express_id' => 0,
                                    'express_no' => '',
                                    ));       
            

        }
        
        static public function GetTeams($order)
        {
            $items = Table::Fetch('order_item', array($order['id']), 'order_id');
            $team_ids = Utility::GetColumn($items,'team_id');
            $teams = Table::Fetch('team', $team_ids);
            return $teams;
        }
        
        static public function GetItems($order_id)
        {
            return Table::Fetch('order_item', array($order_id), 'order_id');
        }
        
        public static function GetDealCountInPaymentProcessing($team_id, $option_id = false)
        {
            $cut_off = time() - 600;
            $sql = "select sum(order_item.quantity) total from 
                order_item left join `order` on order_item.order_id = `order`.id 
                where state != 'pay' AND `state` != 'temporary'
                AND order_item.team_id = {$team_id} AND create_time > {$cut_off}";

            if ($option_id)
                $sql .= " AND order_item.option_id = {$option_id}";

            $result = DB::GetQueryResult($sql,true);

            return $result['total'];
        }
        
        public static function UpdateShippingState($state, $oi_id, $eta = 0, $new_order = FALSE)
        {
            if ( $state != self::ShipStat_Waiting && 
                    $state != self::ShipStat_Available &&
                    $state != self::ShipStat_Delivered &&
                    $state != self::ShipStat_PickedUp &&
                    $state != self::ShipStat_InPrep
                )
               return json_encode(array('status'=> 'failed', 'msg' => 'Invalid Status Update'));
            
            $order_item = Table::Fetch('order_item', $oi_id);
            if ($order_item['delivery'] != 'pickup' && $order_item['delivery'] != 'express')
                return  json_encode(array('status'=> 'failed', 'msg' => 'Invalid Delivery Type'));
            
            $order = Table::Fetch('order', $order_item['order_id']);
            
            $log = new KLogger("/tmp", KLogger::DEBUG);
            $log->logInfo("Updating Delivery status {$state} of Order Item [{$order_item['id']}] with Quantity [{$order_item['quantity']}] & status {$order_item['delivery_status']}");

            if ( $order['state'] != 'pay' ){
                $log->logError("Sayeen paeesa to deo!");
                return json_encode(array('status'=> 'failed', 'msg' => 'Can not set Shipment status for an unpaid Order'));
            }
            
            if ( $eta == 0 )
                $eta = time();
            
            $bPickupAvailable = false;
            $bItemSent = false;
            $bDoChange = false;
            $bAssignStock = false;
            switch($order_item['delivery_status'])
            {
                case self::ShipStat_Invalid:
                    if ( $state == self::ShipStat_Waiting)
                    {
                        $bDoChange = true;
                    }
                              
                case self::ShipStat_Waiting:
                    $bItemSent = ($state == self::ShipStat_Delivered && $order_item['delivery'] == 'express');
                    $bPickupAvailable = ($state == self::ShipStat_Available && $order_item['delivery'] == 'pickup');
                    if ( $state == self::ShipStat_Available || $state == self::ShipStat_Delivered || $state == self::ShipStat_PickedUp || $state == self::ShipStat_InPrep )
                    {
                        $bDoChange = true;
                        $bAssignStock = true;
                    }
                    else if ( $state == self::ShipStat_Waiting)
                        $bDoChange = true;
                    
                   break;
                case self::ShipStat_Delivered:
                    break;
                case self::ShipStat_InPrep:
                    $bItemSent = ($state == self::ShipStat_Delivered && $order_item['delivery'] == 'express');
                    if ( $state == self::ShipStat_Delivered || $state == self::ShipStat_PickedUp)
                    {
                        
                        $bDoChange = true;
                        $bAssignStock = false;
                    }
                    break;
                case self::ShipStat_Available:
                   
                    if ( $state == self::ShipStat_Delivered || $state == self::ShipStat_PickedUp)
                    {
                       $bDoChange = true;
                    }
                    break;
                    
                default:
                    if ( $state == self::ShipStat_Waiting)
                    {
                       $bDoChange = true;
                    }
                    
            }
            
            if ( $bDoChange ){
                $log->logInfo("Baba update to karna hai!");
                if ( $bAssignStock )
                {
                    $log->logInfo("Baba stock to release karo");
                    if ( self::AssignStock($order_item) == false  )
                    {
                        
                        $log->logError("Saeen stock to nahi hai apne pass");
                        // Failed to assign stock
                        return json_encode(array('status'=> 'failed', 'msg' => 'Insufficient stock'));
                    }
                }
                
                if( $state == self::ShipStat_Available  && $order_item['delivery'] == 'express' ){
                        $state = self::ShipStat_InPrep;
                }
                $order_item['delivery_status'] = $state;
                $order_item['delivery_time'] = $eta;
                Table::UpdateCache('order_item', $order_item['id'], $order_item);
                
                if ( $bPickupAvailable ){
                    if($new_order === FALSE){
                        mail_pickup_available($order_item, $order);
                    }
                }
                else if ($bItemSent)
                    mail_order_sent($order_item, $order);
                return TRUE;
            }
            
            $log->logError("ye to kuch bhi nahi hua");
            return json_encode(array('status'=> 'failed', 'msg' => 'Status Update Failed'));
        }
        
        public static function stock_update( $order_item )
        {
            
             if($order_item['option_id'] != NULL){
                            $option_data = Table::Fetch('team_multi', $order_item['option_id']);
                            if( $option_data['stock_count'] >= $order_item['quantity'] ) {
                                $new_stock = intval($option_data['stock_count']) - $order_item['quantity'];
                                
                                DB::Update('team_multi', $order_item['option_id'], array('stock_count' => array('stock_count-'.$order_item['quantity'])) );
                                
                                if($new_stock ==  0 )
                                {
                                    $team_of_option = Table::Fetch('team', $option_data['team_id']);
                                    $subject = 'Stock End Alert';
                                    $message = 'Deal : '.$team_of_option['title']. ' Option :'.$option_data['title'].', Stock has Ended';
                                    global $INI;
                                    $emails =array();
                                    $emails[] = $INI['mail']['from'];
                                    $emails[] = 'abdullahrahim87@gmail.com';
                                    mail_custom($emails, $subject, $message);
                                }
                                if($option_data['stock_count'] > $option_data['alert_threshold'] &&  $new_stock <=  $option_data['alert_threshold'] ){
                                       
                                    $team_of_option = Table::Fetch('team', $option_data['team_id']);
                                    $subject = 'Stock threshold Alert';
                                    $message = 'Deal : '.$team_of_option['title']. ' Option :'.$option_data['title'].', Stock Count is decreased to'. $new_stock;
                                    global $INI;
                                    $emails =array();
                                    $emails[] = $INI['mail']['from'];
                                    $emails[] = 'abdullahrahim87@gmail.com';
                                    mail_custom($emails, $subject, $message);
                                }
                            
                                return TRUE;
                            }
                            else{
                                return FALSE;
                            }
             }
             else{
                            $team_data = Table::Fetch('team', $order_item['team_id']);
                            if( $team_data['stock_count'] >= $order_item['quantity'] ){
                                $new_stock = intval($team_data['stock_count']) - $order_item['quantity'];
                                
                                DB::Update('team', $order_item['team_id'], array('stock_count' => array('stock_count-'.$order_item['quantity'])) );
                                
                                if($new_stock ==  0 )
                                {
                                    $subject = 'Stock End Alert';
                                    $message = 'Deal : '.$team_data['title'].', Stock Count is decreased to'. $new_stock;
                                    global $INI;
                                    $emails =array();
                                    $emails[] = $INI['mail']['from'];
                                    $emails[] = 'abdullahrahim87@gmail.com';
                                    mail_custom($emails, $subject, $message);
                                }
                                
                                
                                if($option_data['stock_count'] > $option_data['alert_threshold'] &&  $new_stock <=  $option_data['alert_threshold'] ){
                                    
                                    $subject = 'Stock threshold Alert';
                                    $message = 'Deal : '.$team_data['title'].', Stock Count is decreased to'. $new_stock;
                                    global $INI;
                                    $emails =array();
                                    $emails[] = $INI['mail']['from'];
                                    $emails[] = 'abdullahrahim87@gmail.com';
                                    mail_custom($emails, $subject, $message);
                                }
                                return TRUE;
                            }
                            else{
                                return FALSE;
                            }
              }
        }
        
        public static function AssignStock( $order_item )
        {
            global $log;
            $log->logInfo("Assigning Stock to Order Item [{$order_item['id']}] with Quantity [{$order_item['quantity']}]");
            // Sanity Check: Is this order belongs to Delivery or Pickup
            if ( $order_item['delivery'] !== 'express' && $order_item['delivery'] !== 'pickup' ){
                $log->logError("Invalid Delivery Type ". $order_item['delivery']);
                return false;
            }
            
            // Does this Order Item has Stock assignment already done?
            if ( $order_item['delivery_status'] != ZOrder::ShipStat_Invalid && 
                 $order_item['delivery_status'] != ZOrder::ShipStat_Waiting ){
                $log->logError("Invalid Delivery Status :". $order_item['delivery_status']);
                return false;
            }
            
            $team = Table::FetchForce('team', $order_item['team_id']);
            
            $table = $order_item['option_id'] ? "team_multi" : "team";
            $id = $order_item['option_id'] ? $order_item['option_id'] : $order_item['team_id'];
            $sql = "UPDATE {$table} SET stock_count = stock_count - {$order_item['quantity']} WHERE id = {$id} AND stock_count >= {$order_item['quantity']}";
            $result = DB::Query($sql);
            
            if ( ($result && mysql_affected_rows() == 0)|| !$result)
            {                  
                Table::UpdateCache('order_item', $order_item['id'], 
                        array('delivery_status' => ZOrder::ShipStat_Waiting, 
                            'delivery_time' => ($team['delivery_eta']*24*3600 + time())));
                return false;
            }
            
            Table::UpdateCache('order_item', $order_item['id'], array('delivery_status' => ZOrder::ShipStat_Available));
            $log->logDebug("Assigned the stock permenantly...");
            
            $team = Table::FetchForce('team', $order_item['team_id']);
            
            if ( $order_item['option_id'] )
                $option = Table::FetchForce('team_multi', $order_item['option_id']);
            
            $new_stock = $option ? $option['stock_count'] : $team['stock_count'];
            $previous_stock = $new_stock + $order_item['quantity'];
            
            
            $threshold = $option ? $option['alert_threshold'] : $team['alert_threshold'];
                    
            
            // Are we below the threshold?
            if ( $new_stock < $threshold && $previous_stock >= $threshold )
            {
                $log->logDebug("Threshold se kam hogaii baboo");
                mail_stock_threshold_alert($team, $option);
            }
            if ( $new_stock == 0 )
            {
                $log->logDebug("zero hogaii hai");
                mail_stock_finished_alert($team, $option);
            }
            
            
            return true;
            
        }
}
?>
