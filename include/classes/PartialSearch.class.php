<?php

class PartialSearch
{

	#partial search par colum names
	static public function partialSearchQuery($columParam= array(),$find='', $conditionOperator =' OR ')
	{
		$queryCondition = '';
		$loopCounter = 0;
		#get column name 
		if(is_array($columParam) && count($columParam)>0)
		{
			
			foreach ($columParam as $key =>$columnName)
			{
				#add condition Operator
				if($loopCounter >0)
				{
					$queryCondition.= $conditionOperator;
				}
				
				#make qnuery condition
				if($key=='id')
				{
					$queryCondition.=" $columnName = '$find' ";
				}
				else
				{
					$dot   = '.';
					$alias = '';
					$fieldName = '';
					$pos = strpos($columnName, $dot);
					if($pos)
					{
						$explodedbyDot = explode(".", $columnName);
						#alias name
						if(!empty($explodedbyDot[0]))
						{
							$alias = $explodedbyDot[0];
						}	
						
						#column name
						if(!empty($explodedbyDot[1]))
						{
							$fieldName = $explodedbyDot[1];
						} 
						
						$columnName = "$alias.`$fieldName`";
					}
					else
					{
						$columnName = "`$columnName`";
					}
					
					$str = wildcarded($find);
					$queryCondition.= " MATCH($columnName) AGAINST('{$str}' in boolean mode) ";
				}
				
				#loop counter
				$loopCounter++;	
			}	
		}
		
		return $queryCondition;
		
	}
}