<?php
    require_once dirname(__FILE__).'/campaign-monitor/csrest_subscribers.php';


    class CampaignMonitor
    {
      	static private $_apikey = "819eb6f046f4323f8b0c1f0d8b6db6fd";	//test

      	static private $_apisecret = "1d370dbd48e4178819880f06725bef2f";	
      
      //static private $_apikey = "ff185f623a1b12eca68fa5373e82a705"; 	//prod

        static public function Subscribe($email, $name="")
        {
            $wrap = new CS_REST_Subscribers("ff185f623a1b12eca68fa5373e82a705", "1d370dbd48e4178819880f06725bef2f");
            $result = $wrap->add(array(
                'EmailAddress' => $email,
                'Name' => $name,
                'CustomFields' => array(
                    array(
                        'Key' => 'Type',
                        'Value' => 'TopDeal Subscriber via API'
                    ),
                    array(
                        'Key' => 'Environment',
                        'Value' => 'Dev'
                    ),
                ),
                'Resubscribe' => true
            ));

            //echo "Result of POST /api/v3/subscribers/{list id}.{format}\n<br />";
            if($result->was_successful()) {
                return true;
            } else {
                return false;
            }
        }

        static public function UnSubscribe($email)
        {
            $wrap = new CS_REST_Subscribers("ff185f623a1b12eca68fa5373e82a705", "1d370dbd48e4178819880f06725bef2f");
            $result = $wrap->unsubscribe($email);

            //echo "Result of GET /api/v3/subscribers/{list id}/unsubscribe.{format}\n<br />";
            if($result->was_successful()) {
                return true;
            } else {
               return false;
            }
        }
    }

?>
