<?php

class Export{
    
    static function GetColNames($table_name){
        
        $sql = "SHOW COLUMNS FROM $table_name";
        $result  = DB::GetQueryResult($sql, false);
        $col_names = array();
        foreach($result as $r ){
            $col_names[] = $r['field'];
        }
        
        return $col_names;
        
    }
    
    
    
}
