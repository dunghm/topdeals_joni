<?php
    require_once dirname(__FILE__).'/QueryPath/QueryPath.php';
    
    class DataTrans
    {
        var $serviceAJAXURL;
        var $serviceXMLSettle;
        var $serviceXMLAuthorize;
        
        var $merchantID;
        
        var $serviceVersion = "1.0";
        
        function DataTrans($options, $mode = "test")
        {
            if ($mode == "live" )
            {
                $this->serviceAJAXURL = "https://payment.datatrans.biz/upp/ajax/call";
                $this->serviceXMLSettle = "https://payment.datatrans.biz/upp/jsp/XML_processor.jsp";
                $this->serviceXMLAuthorize = "https://payment.datatrans.biz/upp/jsp/XML_authorize.jsp";
            }
            else if ( $mode == "test" )
            {
                $this->serviceAJAXURL = "https://pilot.datatrans.biz/upp/ajax/call";
                $this->serviceXMLSettle = "https://pilot.datatrans.biz/upp/jsp/XML_processor.jsp";
                $this->serviceXMLAuthorize = "https://pilot.datatrans.biz/upp/jsp/XML_authorize.jsp";
            }
            
            $this->merchantID = $options["merchantID"];
        }
        
        function MakeXmlRequest($url, $data = null ) 
        {
           $ch = curl_init();
            if ( $data ) {
                    //$formdata = urldecode(http_build_query($data));
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            }
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml",
                "Content-length: ".strlen($data))); 

            curl_setopt($ch, CURLOPT_CAINFO,  dirname(__FILE__).'/cacert.pem');

            $result = curl_exec($ch);

            return $result;
        }
        
        function GetCCAlias($cardno)
        {
            $serviceParams = array ( "merchantId" => $this->merchantID,
                    "cardNumber" => $cardno,
                    "request" => "alias",
                    "version" => $this->serviceVersion,
           );
    
            foreach($serviceParams as $key => $value)
                $getparams[] = $key."=".urlencode($value);

            $qryString = implode($getparams, "&");
            
            $result = $this->MakeXmlRequest( $this->serviceAJAXURL."?". $qryString  );
    
            $output = json_decode($result);
            
            if ( $output && $output->status == "success" )
            {
                return $output->aliasCC;                
            }
          
            return false;
        }

        function MakeSettlement($transid, $params )
        {
            $xml = '<?xml version="1.0" encoding="UTF-8" ?>
             <paymentService version="1">
               <body merchantId="'.$this->merchantID.'">
                 <transaction refno="'.$params["refno"].'">
                   <request>
                     <amount>'.$params["amount"].'</amount>
                     <currency>'.$params["currency"].'</currency>
                     <uppTransactionId>'.$transid.'</uppTransactionId>
                   </request>
                 </transaction>
               </body>
             </paymentService>';
         
              $result = $this->MakeXmlRequest($this->serviceXMLSettle, $xml  );
              
               if ( $result )
                {
                    $qp = qp($result)->find('body');
                    if ( $qp->attr("status") == "accepted" )
                    {
                        return qp($result)->find("uppTransactionId")->text();
                    }
                    else /*if ( $qp->attr("status") == "error" )*/
                    {
                        //die("Error: ".$qp->find('errorMessage')->text());
                        return false;
                    }                    
                }
                

              return false;
        }

        function MakeAuthorization($params)
        {
            $aliasCC = $this->GetCCAlias($params["cardno"]);
            if ( $aliasCC )
            {
                $xml = '<?xml version="1.0" encoding="UTF-8" ?>
                 <authorizationService version="1">
                   <body merchantId="'.$this->merchantID.'">
                     <transaction refno="'.$params["refno"].'">
                       <request>
                         <amount>'.$params["amount"].'</amount>
                         <currency>'.$params["currency"].'</currency>
                         <aliasCC>'.$aliasCC.'</aliasCC>
                         <expm>'.$params["expm"].'</expm>
                         <expy>'.$params["expy"].'</expy>
                       </request>
                     </transaction>
                   </body>
                 </authorizationService>';
        
                $result = $this->MakeXmlRequest($this->serviceXMLAuthorize, $xml  );
                
                if ( $result )
                {
                    $qp = qp($result)->find('body');
                    if ( $qp->attr("status") == "accepted" )
                    {
                        return qp($result)->find("uppTransactionId")->text();
                    }
                    else /*if ( $qp->attr("status") == "error" )*/
                    {
                       // die("Error: ".$qp->find('errorMessage')->text());
                        return false;
                    }                    
                }               
            }
           
            
            return false;
        }
        
        function MakePayment($params)
        {
            $transid = $this->MakeAuthorization($params);
            
            if ( $transid )
            {
                return $this->MakeSettlement($transid, $params);
            }
            
            return false;
        }
    
    }
    
 ?>