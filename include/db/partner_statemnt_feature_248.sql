alter table  `coupon` add column `status` int(11) DEFAULT '0' NULL COMMENT '0 - Fresh coupon, 1 - Included in a Partner Statement, 2 - Omitted in a Partner Statement' after `ms_time`, add column `comment` text NULL after `status`;


alter table `partner_statement` add column `paid_time` int NULL after `create_time`;