/*Table structure for table `trigger_action` */

CREATE TABLE `trigger_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `subject` varchar(250) DEFAULT NULL,
  `email_body` text,
  `type_id` int(11) DEFAULT NULL,
  `promo_code_type` varchar(250) DEFAULT NULL,
  `promo_percent` int(11) DEFAULT NULL,
  `promo_amount` int(11) DEFAULT NULL,
  `minimum_order` int(11) DEFAULT '0',
  `created` int(11) DEFAULT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `trigger_action` */

insert  into `trigger_action`(`id`,`name`,`subject`,`email_body`,`type_id`,`promo_code_type`,`promo_percent`,`promo_amount`,`minimum_order`,`created`,`updated`) values (1,'Send e-mail with promo code 10% valid 48h','Enjoy 10% Discount','<p>Dear #USERNAME#,</p>\r\n<p>We offer you 10% Discount which is valid till 48 hours after this email recieved, please use this Promo Code (#PROMO_CODE#) on your cart.</p>\r\n<p>&nbsp;</p>\r\n<p>Thank You</p>',2,'percentage',10,0,0,1435194847,1435289349);
insert  into `trigger_action`(`id`,`name`,`subject`,`email_body`,`type_id`,`promo_code_type`,`promo_percent`,`promo_amount`,`minimum_order`,`created`,`updated`) values (2,'Send e-mail to inform him that shipping will offered in his next order','Topdeal Offered You Free Shipping for next order','<p>Dear #USERNAME#,</p>\r\n<p>Topdeal Offered You Free Shipping for next order</p>\r\n<p>&nbsp;</p>\r\n<p>Thank You</p>',1,'face_value',0,50,0,1435195335,1435289337);
insert  into `trigger_action`(`id`,`name`,`subject`,`email_body`,`type_id`,`promo_code_type`,`promo_percent`,`promo_amount`,`minimum_order`,`created`,`updated`) values (3,'E-mail to be sent giving him a promo code if his next purchase reach the CHF 100 amount','Enjoy  5 % Discount on Checkout','<p>Dear #USERNAME#,</p>\r\n<p>We offer you CHF 50 amount Discount if you purchase more than CHF 100.</p>\r\n<p>please use this Promo Code (#PROMO_CODE#) on your cart.</p>\r\n<p>Thank You</p>',2,'percentage',5,0,100,1435284064,1435289646);

/*Table structure for table `trigger_action_type` */

CREATE TABLE `trigger_action_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `trigger_action_type` */

insert  into `trigger_action_type`(`id`,`name`,`created`) values (1,'Simple Email','2015-06-23 06:39:58');
insert  into `trigger_action_type`(`id`,`name`,`created`) values (2,'Promo Code','2015-06-23 06:40:04');

/*Table structure for table `trigger_event` */

CREATE TABLE `trigger_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `start_time` int(11) DEFAULT NULL,
  `end_time` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT '0',
  `created` int(11) DEFAULT NULL,
  `updated` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `trigger_event` */

insert  into `trigger_event`(`id`,`name`,`action_id`,`start_time`,`end_time`,`is_active`,`created`,`updated`) values (1,'Re-activation of inactive customers since 2 years',1,0,0,1,1435195411,1435282608);
insert  into `trigger_event`(`id`,`name`,`action_id`,`start_time`,`end_time`,`is_active`,`created`,`updated`) values (2,'New inactive customer',3,0,0,1,1435195468,1435289745);
insert  into `trigger_event`(`id`,`name`,`action_id`,`start_time`,`end_time`,`is_active`,`created`,`updated`) values (3,'Re-activation of inactive customers since 1 year',3,0,0,0,1435289765,1435295941);

/*Table structure for table `trigger_event_logs` */

CREATE TABLE `trigger_event_logs` (
  `int` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `promo_code` varchar(255) DEFAULT NULL,
  `created` int(11) NOT NULL,
  PRIMARY KEY (`int`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trigger_event_logs` */

