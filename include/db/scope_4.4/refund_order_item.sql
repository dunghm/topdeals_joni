/*
SQLyog Enterprise - MySQL GUI v7.13 
MySQL - 5.1.36-community-log : Database - topdeals
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `refunded_order_item` */

DROP TABLE IF EXISTS `refunded_order_item`;

CREATE TABLE `refunded_order_item` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL,
  `order_item_id` int(11) NOT NULL,
  `team_id` int(10) unsigned NOT NULL DEFAULT '0',
  `option_id` int(11) DEFAULT NULL,
  `delivery` varchar(32) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT '1',
  `price` double(10,2) NOT NULL DEFAULT '0.00',
  `fare` double(10,2) NOT NULL DEFAULT '0.00',
  `total` double(10,2) NOT NULL DEFAULT '0.00',
  `remark` text CHARACTER SET latin1 COLLATE latin1_german1_ci,
  `mode` varchar(32) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT 'buy',
  `first_name` varchar(32) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `last_name` varchar(32) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `email` varchar(128) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `gender` varchar(5) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `mobile` varchar(128) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `address` varchar(128) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `address2` varchar(128) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `zipcode` char(6) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `region` varchar(128) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `country` varchar(5) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `delivery_time` int(11) NOT NULL DEFAULT '0',
  `delivery_status` int(11) NOT NULL DEFAULT '0',
  `shipment_identcode` varchar(255) CHARACTER SET latin1 COLLATE latin1_german1_ci DEFAULT NULL,
  `shipment_status` int(11) DEFAULT '0',
  `shipment_status_updated_on` int(11) DEFAULT NULL,
  `shipment_post_service` varchar(255) CHARACTER SET latin1 COLLATE latin1_german1_ci NOT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `deleted_on` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `team_id` (`team_id`),
  KEY `option_id` (`option_id`)
) ENGINE=MyISAM AUTO_INCREMENT=79566 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
