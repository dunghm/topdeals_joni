/* CASHBACK AMOUNT FIELD FOR ORDER ITEM*/
alter table `order_item` add column `cashback_amount` int DEFAULT '0' NULL after `shipment_additional_cost`;

/* TRIGGER ACTION PROMO CODE ATTRIBUTE COLOUMN ADDED */
alter table `trigger_action` add column `promo_code_attribute` text NULL after `minimum_order`;
/* CASHBACK AMOUNT FIELD FOR REFUND ORDER ITEM*/
alter table `refunded_order_item` add column `cashback_amount` int DEFAULT '0' NULL after `fare`;