alter table `team` add column `is_cash_back_allowed` int DEFAULT '0' NULL after `partner_revenue`, add column `cb_amount` decimal NULL after `is_cash_back_allowed`, add column `cb_start_time` int(11) NULL after `cb_amount`, add column `cb_end_time` int(11) NULL after `cb_start_time`;


alter table `team_multi` add column `is_cash_back_allowed` int DEFAULT '0' NULL after `multi_order`, add column `cb_amount` decimal NULL after `is_cash_back_allowed`, add column `cb_start_time` int(11) NULL after `cb_amount`, add column `cb_end_time` int(11) NULL after `cb_start_time`;
