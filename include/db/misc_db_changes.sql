/*Laravel Dual Login*/

alter table `user` add column `remember_token` varchar(255) NULL after `address2`;
alter table `partner` add column `remember_token` varchar(255) NULL after `email`;


/*ps_id need to set default for generate coupon listing*/
alter table  `coupon` change `ps_id` `ps_id` int(11) default '0' NULL ;
/* set all other null ps_id to 0 */
UPDATE coupon SET ps_id = 0 WHERE ps_id IS NULL;



/*Newsletter Changes*/
 alter table `newsletters` add column `newsletter_type` varchar(255) NULL after `newsletter_name`;

/* SUPPLIER EMAIL ORDER TRACKING  */
CREATE TABLE `supplier_mail_order_tracking` (              
                                `id` int(11) NOT NULL AUTO_INCREMENT,                    
                                `user_id` int(11) DEFAULT NULL,                          
                                `team_id` int(11) DEFAULT NULL,                          
                                `option_id` int(11) DEFAULT NULL,                        
                                `total_sent_mail` int(11) DEFAULT '1',                   
                                `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,  
                                PRIMARY KEY (`id`)                                       
                              ) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1; 


/* MAIL SUPPLIER TRACKING IS CHANGE WHEN DEAL STOCK UPDATE*/
alter table `supplier_mail_order_tracking` add column `is_stock_update` int DEFAULT '0' NULL after `total_sent_mail`;
