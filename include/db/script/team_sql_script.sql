/*
* SQL Script For Transfer Team Data From One Data To Another
*/

/* TEAM DATA COPY FROM DB1 To DB2 */
INSERT INTO `topdealch3_4_3`.`team` 
(
user_id,
       title,
       title_fr,
       sku,
       summary,
       summary_fr,
       city_id,
       group_id,
       partner_id,
       SYSTEM,
       team_price,
       reward,
       market_price,
       product,
       condbuy,
       per_number,
       min_number,
       max_number,
       now_number,
       pre_number,
       image,
       image1,
       image2,
       flv,
       mobile,
       credit,
       card,
       fare,
       farefree,
       bonus,
       address,
       detail,
       detail_fr,
       systemreview,
       userreview,
       notice,
       express,
       delivery,
       `state`,
       conduser,
       buyonce,
       team_type,
       sort_order,
       expire_time,
       expire_time_partner,
       begin_time,
       end_time,
       reach_time,
       close_time,
       seo_title,
       seo_keyword,
       seo_description,
       `type`,
       partner_revenue,
       ps_start_time,
       delivery_eta,
       stock_count,
       alert_threshold,
       supplier_id,
       volume_length,
       volume_breadth,
       volume_height,
       volume_weight,
       featured,
       last_updated,
       brand_id,
       ps,
       patner_revenue,
       is_cash_back_allowed,
       cb_amount,
       cb_start_time,
       cb_end_time
)
SELECT user_id,
       title,
       title_fr,
       sku,
       summary,
       summary_fr,
       city_id,
       group_id,
       partner_id,
       SYSTEM,
       team_price,
       reward,
       market_price,
       product,
       condbuy,
       per_number,
       min_number,
       max_number,
       now_number,
       pre_number,
       image,
       image1,
       image2,
       flv,
       mobile,
       credit,
       card,
       fare,
       farefree,
       bonus,
       address,
       detail,
       detail_fr,
       systemreview,
       userreview,
       notice,
       express,
       delivery,
       `state`,
       conduser,
       buyonce,
       team_type,
       sort_order,
       expire_time,
       expire_time_partner,
       begin_time,
       end_time,
       reach_time,
       close_time,
       seo_title,
       seo_keyword,
       seo_description,
       `type`,
       partner_revenue,
       ps_start_time,
       delivery_eta,
       stock_count,
       alert_threshold,
       supplier_id,
       volume_length,
       volume_breadth,
       volume_height,
       volume_weight,
       featured,
       last_updated,
       brand_id,
       ps,
       patner_revenue,
       is_cash_back_allowed,
       cb_amount,
       cb_start_time,
       cb_end_time
FROM `topdealch3_4_2`.`team`
WHERE id IN (1023, 1025, 1026, 1027, 1029, 1030);



/* TEAM MULTI DATA COPY FROM DB1 To DB2 */
INSERT INTO `topdealch3_4_3`.`team_multi` 
(
 team_id,
       title,
       title_fr,
       sku,
       summary,
       summary_fr,
       team_price,
       market_price,
       image,
       per_number,
       min_number,
       max_number,
       now_number,
       pre_number,
       parent_option_id,
       `type`,
       partner_revenue,
       stock_count,
       alert_threshold,
       volume_length,
       volume_breadth,
       volume_height,
       volume_weight,
       multi_order,
       is_cash_back_allowed,
       cb_amount,
       cb_start_time,
       cb_end_time  
)
SELECT team_id,
       title,
       title_fr,
       sku,
       summary,
       summary_fr,
       team_price,
       market_price,
       image,
       per_number,
       min_number,
       max_number,
       now_number,
       pre_number,
       parent_option_id,
       `type`,
       partner_revenue,
       stock_count,
       alert_threshold,
       volume_length,
       volume_breadth,
       volume_height,
       volume_weight,
       multi_order,
       is_cash_back_allowed,
       cb_amount,
       cb_start_time,
       cb_end_time      
FROM `topdealch3_4_2`.`team_multi`
WHERE team_id IN (1023, 1025, 1026, 1027, 1029, 1030);


/* TEAM PRICE HISTORY DATA COPY FROM DB1 To DB2 */
INSERT INTO `topdealch3_4_3`.`team_price_history` 
(
       team_id,
       option_id,
       team_price,
       market_price,
       partner_revenue,
       user_id,
       create_time    
)
SELECT 
       team_id,
       option_id,
       team_price,
       market_price,
       partner_revenue,
       user_id,
       create_time       
FROM `topdealch3_4_2`.`team_price_history`
WHERE team_id IN (1023, 1025, 1026, 1027, 1029, 1030);


/* TEAM SLIDESHOW IMAGE  DATA COPY FROM DB1 To DB2 */
INSERT INTO `topdealch3_4_3`.`team_slideshow_images` 
(
       team_id,
       image_desc,
       image_loc,
       image_order  
)
SELECT 
       team_id,
       image_desc,
       image_loc,
       image_order
       
FROM `topdealch3_4_2`.`team_slideshow_images`
WHERE team_id IN (1023, 1025, 1026, 1027, 1029, 1030);

