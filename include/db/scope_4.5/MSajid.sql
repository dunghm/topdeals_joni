#############
# 31 July 2015
#############
alter table `supplier_mail_order_tracking` add column `recipient` text NULL after `created`, add column `subject` varchar(255) NULL after `recipient`, add column `content` longtext NULL after `subject`;
alter table `supplier_mail_order_tracking` add column `ref_no` varchar(24) NULL after `id`;