<?php
$value = array (
  'location' => 'sitemap145.xml',
  'static_pages' => 
  array (
    0 => 'apropos',
    1 => 'contact',
    2 => 'jobs',
    3 => 'presse',
    4 => 'infos/faqs',
    5 => 'infos/concept',
    6 => 'infos/conditions_utilisation',
    7 => 'infos/confidentialite',
    8 => 'partenaires',
  ),
  'static_pages_priority' => '0.6',
  'active_deals' => 'No',
  'deal_priority' => '0.9',
  'deal_category_priority' => '0.7',
  'inactive_deal_priority' => '0.3',
);
?>