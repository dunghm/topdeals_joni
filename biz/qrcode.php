<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$cid = strval($_GET['id']);
$sec = strval($_GET['sec']);

$coupon = Table::FetchForce('coupon', $cid);

if ( !$coupon )
{
    redirect(WEB_ROOT);
}


//GET COUPON DELIVERY TYPE ON WHICH BASIS REDIRECTION SET FOR OLD OR NEW Partner Backend Redirection 
//for pickup new backend & Non-pickup coupon to be served from Old Partner backend
$couponDeliveryTypeSql = "SELECT 
										oi.delivery  AS delivery
									FROM 
										coupon AS c
									INNER JOIN
										order_item AS oi
									ON 
										oi.id = c.order_id
									WHERE 
										c.id = $cid";
$couponDeliveryTypeData = DB::GetQueryResult($couponDeliveryTypeSql, false);

$deliveryType	=	"";
if(!empty($couponDeliveryTypeData) && isset($couponDeliveryTypeData[0])){
	$deliveryType	=	$couponDeliveryTypeData[0]['delivery'];
}

if($deliveryType == "pickup"){
	
	$redirectURL = WEB_ROOT."/tdadmin/partner/qrcode?id={$cid}&sec={$sec}";
	return redirect($redirectURL);
}


$partner = Table::Fetch('partner', $coupon['partner_id']);
$partner_id = abs(intval($_SESSION['partner_id']));
//$partner_id = abs(intval($_SESSION['partner_id']));
$login_partner = Table::Fetch('partner', $partner_id);

//var_dump($login_partner); var_dump($coupon); exit;

if ($login_partner['id'] == $coupon['partner_id'] )
{
    Session::Set('consume_id', $cid);
    Session::Set('consume_secret', $sec);
    redirect(WEB_ROOT. '/biz/index.php');
}
else 
{
	//$team = Table::Fetch('team', $coupon['team_id']);
   // redirect(ZTeam::GetTeamUrl($team,false,true));
}

redirect(WEB_ROOT. '/biz/index.php');

?>
