<?php

require_once(dirname(dirname(__FILE__)) . '/app.php');

if (!is_partner() && !is_manager())
    need_partner();

$id = abs(intval($_GET['id']));
$statement = Table::Fetch('partner_statement', $id);

if ( !$statement || (is_partner() && $statement['partner_id'] != $_SESSION['partner_id']) )
{
	//ALSO CHECK IF LOGIN USER IS NOT MANAGER
	if(!is_manager()){
		Session::Set('error', 'Invalid Statement');
		redirect( WEB_ROOT . '/biz/statement.php');
	}
}

$action = $_GET['action'];

$partner = Table::Fetch('partner', $statement['partner_id'] );
$team = Table::Fetch('team', $statement['team_id']);
//$coupons = Table::Fetch('coupon', array($statement['id']), "ps_id");
$sql = "SELECT IFNULL(oi.option_id,0) option_id, COUNT(*) total FROM coupon c, order_item oi WHERE c.order_id = oi.id AND c.status NOT IN (2) AND ps_id = {$id} GROUP BY oi.option_id";
//Get Ommitted Coupon If Any
$ommitedCouponSql = "SELECT `id`,`comment` FROM coupon  WHERE   `status` IN (2) AND ps_id = {$id} ";



$coupons = DB::GetQueryResult($sql, false);
$couponsArr = DB::GetQueryResult($sql, false);
//GET OMMITTED COUPON DB
$ommitedCouponData = DB::GetQueryResult($ommitedCouponSql, false);

$multi = Table::Fetch('team_multi', array($statement['team_id']), 'team_id');

$grand_sum = 0;
foreach($coupons as $couponKey=>&$one)
{
    if ( $one['option_id'] )
        $revenue = Table::Fetch('team_multi', $one['option_id']);
    else
        $revenue = $team;
    
    $one['partner_revenue'] 					= $revenue['partner_revenue'];
	$couponsArr[$couponKey]['partner_revenue']	=	$revenue['partner_revenue'];
    $grand_sum += $one['total'] * $one['partner_revenue'];
}
$city  = Table::Fetch('category', $partner['city_id']);

if (is_partner()) {
	
	$new_status	= $statement['status'];	
    if ($statement['status'] == 1)
        $new_status = 3;
    else if ( $statement['status'] == 2 )
        $new_status = 4;

	
    Table::UpdateCache('partner_statement', $id, array("status" => $new_status));
}

if ( $ps['final'] == 1 )
    $template = 'biz_statement_view_final';
else if ( count($multi) > 0 )
    $template = 'biz_statement_view_multi';
else
    $template = 'biz_statement_view';

$vars = array(
    "grand_sum" => $grand_sum,
    "coupons" => $coupons,
	"couponsArr" => $couponsArr,
    "team" => $team,
    "partner" => $partner
);

if ( $action == "print" )
{
    $html = render($template, $vars);
    PDFExport::Export($html);
}
else
    include template($template);



?>
