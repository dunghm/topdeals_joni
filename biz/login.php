<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');
if(need_partner(FALSE)){
    redirect( WEB_ROOT . '/biz/index.php');
}
if ( $_POST ) {
    
    
	$login_partner = ZPartner::GetLogin($_POST['username'], $_POST['password']);
	if ( !$login_partner ) {
		Session::Set('error', 'username and password dont match！');
		redirect( WEB_ROOT . '/biz/login.php');
	} else {
		Session::Set('partner_id', $login_partner['id']);
               if(isset($_POST['remember_me'])){
                ZLogin::Partner_Remember($login_partner);
               }
                redirect( WEB_ROOT . '/biz/index.php');
	}
}

include template('biz_login');
