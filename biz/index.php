<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

need_partner();
$partner_id = abs(intval($_SESSION['partner_id']));
$login_partner = Table::Fetch('partner', $partner_id);
 $log = new KLogger(dirname(dirname(__FILE__))."/include/compiled/", KLogger::DEBUG);
            
$condition = array(
	'partner_id' => $partner_id,
);
$count = Table::Count('team', $condition);
list($pagesize, $offset, $pagestring) = pagestring($count, 10);
/*
$teams = DB::LimitQuery('team', array(
	'condition' => $condition,
	'order' => 'ORDER BY id DESC',
	'size' => $pagesize,
	'offset' => $offset,
));
*/
$sql = "select t.*, (select count(*) from coupon where team_id = t.id AND consume= 'Y') AS validated, 
(select sum(oi.quantity) from order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE oi.team_id = t.id AND o.state = 'pay') AS total
from team t where partner_id = {$partner_id} ORDER by t.id DESC LIMIT {$pagesize} OFFSET {$offset} ";
/*
$sql = "select team.*, count(coupon.id) AS validated, sum(oi.quantity) AS total  from team 
    INNER join coupon on team.id = coupon.team_id 
AND coupon.consume = 'Y'
INNER JOIN order_item oi ON oi.team_id = team.id  
where team.partner_id = {$partner_id}
group by team.id ORDER by team.id DESC LIMIT {$pagesize} OFFSET {$offset} ";
*/
$log->logInfo($sql);

$teams = DB::GetQueryResult($sql, false);

$city_ids = Utility::GetColumn($teams, 'city_id');
$cities = Table::Fetch('category', $city_ids);


include template('biz_index');
