<?php
require_once(dirname(__FILE__) . '/app.php');

$id = abs(intval($_GET['r']));
if ($id) { 
	if ($login_user_id) {
		ZInvite::CreateFromId($id, $login_user_id);
	} else {
		$longtime = 86400 * 3; //3 days
		cookieset('_rid', $id, $longtime);
	}
}
if ( !strstr($_SERVER['HTTP_USER_AGENT'],'facebookexternalhit') )
{
 	redirect( WEB_ROOT  . '/index.php');
	exit;
}

echo "<meta property='og:url' content='{$INI['system']['wwwprefix']}/r.php?r={$id}' />";
echo "<meta property='og:title' content='TopDeal.ch: Vos meilleures offres en Suisse Romande' />";
echo "<meta property='og:description' content=\"Profite chaque jour d&quot;une offre produit ou service en Suisse Romande au meilleur prix!\" />";
echo "<meta property='og:image' content='{$INI['system']['wwwprefix']}/static/img/new_logo.jpg'/>";

?>
<html>
	<head></head>
	<body></body>
</htm>