<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');


//print_r($_REQUEST) . '<br>';


$json_request = (json_decode($_REQUEST['data']) != NULL) ? true : false;

if(!$json_request)
{
  $response['error'] = 'Invalid Request';
  echo json_encode($response);
  die;
}


header('Content-type: application/json');
//$data = $_REQUEST['data'];
//echo $data;
$buy_data = json_decode ($_REQUEST['data'], true);
//echo($buy_data);
/*
  foreach($buy_data as $key=>$val){         
    echo '<br>-'.$key.'='.$val;
  }
*/

//die;
 

$id = abs(intval($buy_data['team_id']));
$op = abs(intval($buy_data['team_option']));
$user_id = abs(intval($buy_data['user_id']));

$team = Table::Fetch('team', $id);

$option = Table::Fetch('team_multi', $op);

	if($option && count($option)>0){
		$team['team_price'] = $option['team_price'];
		$team['title'] = $option['title'];
		$team['title_fr'] = $option['title_fr'];
		$team['image'] = $option['image'];
		$team['option_id'] = $op;
	}

	
if ( !$team || $team['begin_time']>time() ) {
		if($lang=="en")
			$response['error'] = 'Buy item does not exist';
		elseif($lang =='fr')
			$response['error'] = 'Le produit n\'existe pas';	

	echo json_encode($response);
	die;
}


//whether buy
$ex_con = array(
		'user_id' => $user_id,
		'team_id' => $team['id'],
		'state' => 'unpay',
		);
$order = DB::LimitQuery('order', array(
	'condition' => $ex_con,
	'one' => true,
));




//post buy
//if ( $_POST ) {

	// print_r($_REQUEST);
	// die;


	$table = new Table('order', $buy_data);
	$table->quantity = abs(intval($table->quantity));

	if ( $table->quantity == 0 ) {
		if($lang=="en")
			$response['error'] =  'quantity should not be less than 1';
		elseif($lang =='fr')
			$response['error'] = 'la quantit&eacute; choisie ne peut &acirc;tre en dessous de 1';			

		echo json_encode($response);
		die;			
	} 
	elseif ( $team['per_number']>0 && $table->quantity > $team['per_number'] ) {
		
		if($lang=="en")
			$response['error'] =  'your purchase has gone beyond the limit';
		elseif($lang =='fr')
			$response['error'] =  'vous avez d&eacute;pass&eacute; le nombre d\'achat ';			

		echo json_encode($response);
		die;			
	}


	if ($order && $order['state']=='unpay') {
		$table->SetPk('id', $order['id']);
	}

/* temp comment
	$upd_user = "Update user SET realname = '".$table->realname."', mobile='".$table->mobile."', address='".$table->address."', zipcode='".$table->zipcode."', region='".$table->region."', country='".$table->country."' WHERE id = '".$user_id."'";
	DB::Query($upd_user);
*/	
	$table->user_id = $user_id;
	$table->state = 'unpay';
	$table->team_id = $team['id'];
	$table->city_id = $team['city_id'];
	$table->express = ($team['delivery']=='express') ? 'Y' : 'N';
	$table->fare = $table->express=='Y' ? $team['fare'] : 0;
	$table->price = $team['team_price'];
	$table->credit = 0;
    $table->service = "paypal";

	//multi buy option override
	if($op){
		$table->option_id = $op;
	}

	//Gift
        $table->remark = "gift";
        $table->lastname = $buy_data['recname'];
	
	if (!$eorder){
		$table->SetPk('id', null);
		$table->create_time = time();
		$table->origin = team_origin($team, $table->quantity);
	}

	$insert = array(
			'user_id', 'team_id', 'city_id', 'state', 
			'fare', 'express', 'origin', 'price', 'gender',
			'address', 'zipcode', 'realname', 
			'quantity', 'create_time', 'remark', 'condbuy',
            'country', 'mobile', 'service','region','option_id'
		);
	if ($flag = $table->update($insert)) {
		$order_id = abs(intval($table->id));
	}



		// generate unique pay id
		$randid = rand(1000,9999);
		$pay_id = "go-{$order_id}-{$table->quantity}-{$randid}";
		Table::UpdateCache('order', $order_id, array(
					'pay_id' => $pay_id,
					));


	$response['order_id'] = $order_id;
	echo json_encode($response);
	die;			
	
//}
