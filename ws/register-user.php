<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

header('Content-type: application/json');

$json_request = (json_decode($_REQUEST['data']) != NULL) ? true : false;

if(!$json_request)
{
 echo 'Invalid Request';
 die;
}


$data = json_decode ($_REQUEST['data'], true);
//echo($data);


	$u = array();
			
	$u['username'] = strval($data['email']);
	$u['password'] = strval($data['password']);
	$u['email'] = strval($data['email']);

	if ( ! Utility::ValidEmail($u['email'], true) ) {
		$response['error'] =  'Email address is invalid';
	    echo json_encode($response);
	 	die;
          
	}
	if ($data['password2']==$data['password'] && $data['password']) {
		if ( option_yes('emailverify') ) { 
			$u['enable'] = 'Y'; 
		}
		if ( $user_id = ZUser::Create($u) ) {
			if ( option_yes('emailverify') ) {
				mail_sign_id($user_id);
			} else {
				ZLogin::Login($user_id);
				ZCredit::Register($user_id);
				$login_user = ZUser::GetLogin($data['email'], $data['password']);	
			}
                $response['user_id'] =  $user_id;
                echo json_encode($response);
		die;
			
		 } else {
			$au = Table::Fetch('user', $data['email'], 'email');
			if ( $au ) {
				$response['error'] =  'registration failed,Email id is already in use.';
				echo json_encode($response);
				die;
				} else {
				$response['error'] =  'registration failed,username is already in use.';
				echo json_encode($response);
				die;
			}
          }
    }		  
	 else {
			$response['error'] =  'registration failed,incorrect password settings';
			echo json_encode($response);
			die;	
	}						
