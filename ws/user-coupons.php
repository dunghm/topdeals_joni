<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');


header('Content-type: application/json');

$json_request = (json_decode($_REQUEST['data']) != NULL) ? true : false;

if(!$json_request)
{
 echo 'Invalid Request';
 die;
}


$data = json_decode ($_REQUEST['data'], true);

$user_id = $data['user_id'];
$type = $data['type'];
$daytime = strtotime(date('Y-m-d'));

//echo $user_id;

    $condition = array(
    'user_id' => $user_id,
    'consume' => 'N',
    "expire_time >= {$daytime}",
    );


if($type=='consume'){
	$condition = array(
	'user_id' => $user_id,
	'consume' => 'Y',
	);
}
else if($type=='expire'){
	$condition = array(
	'user_id' => $user_id,
	'consume' => 'N',
	"expire_time < {$daytime}",
	);
}


//$count = Table::Count('coupon', $condition);
//list($pagesize, $offset, $pagestring) = pagestring($count, 10);
//echo $count;
$coupons = DB::LimitQuery('coupon', array(
  	'condition' => $condition,
	'order' => 'ORDER BY team_id DESC, create_time DESC',
  //	'size' => $pagesize,
  //	'offset' => $offset,
));



$team_ids = Utility::GetColumn($coupons, 'team_id');
$teams = Table::Fetch('team', $team_ids);

/*
  foreach($coupons as $key=>$val){         
    $user_coupons[$key] = $val ;
  }
*/

  foreach($coupons as $one){         
    $user_coupons2['id'] = $one['id'];
    $user_coupons2['order_id'] = $one['order_id'];
    $user_coupons2['type'] = $one['type'];
    $user_coupons2['consume'] = $one['consume'];
    $user_coupons2['expire_time'] = $one['expire_time'];
    $user_coupons2['title'] = $teams[$one['team_id']]['title'];
    $user_coupons2['title_fr'] = $teams[$one['team_id']]['title_fr'];    
    $user_coupons2['print_url'] = '/coupon/print.php?id='.$one['id'].'&user_id='.$user_id; 
    
    $all_coupons[] = $user_coupons2;
  }

//$user_coupons['print_url'] = '/coupon/print.php?id=';


/*
  foreach($user_coupons as $c){         
    $c['print_url'] = '/coupon/print.php?id='.$c['id'];
  }
*/

header('Content-type: application/json');
echo json_encode($all_coupons);

?>