<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');


header('Content-type: application/json');

$json_request = (json_decode($_REQUEST['data']) != NULL) ? true : false;

if(!$json_request)
{
 echo 'Invalid Request';
 die;
}


$data = json_decode ($_REQUEST['data'], true);

$user_id = $data['user_id'];

$condition = array( 'user_id' => $user_id );


$flows = DB::LimitQuery('flow', array(
			'condition'=>$condition,
			'order' => 'ORDER BY id DESC',
			));

$detail_ids = Utility::GetColumn($flows, 'detail_id');
$teams = Table::Fetch('team', $detail_ids);
$user = Table::Fetch('user', $user_id);
//$users = Table::Fetch('user', $detail_ids);
//$coupons = Table::Fetch('coupon', $detail_ids);

$total_credit = $user['money'];

  foreach($flows as $one){         
    $credits['id'] = $one['id'];  
    $credits['detail'] = strip_tags(ZFlow::Explain($one));  //$teams[$one['detail_id']]['title']; 
    $credits['direction'] = $one['direction'];  
    $credits['money'] = moneyit($one['money']);  
    $credits['action'] = $one['action'];  
    $credits['create_time'] = $one['create_time'];      
    $credits['total_credit'] = $total_credit;
    
    $all_credits[] = $credits;
  }


header('Content-type: application/json');
echo json_encode($all_credits);

?>