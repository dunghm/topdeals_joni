<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');


header('Content-type: application/json');

$json_request = (json_decode($_REQUEST['data']) != NULL) ? true : false;

if(!$json_request)
{
 echo 'Invalid Request';
 die;
}


$data = json_decode ($_REQUEST['data'], true);

$user_id = $data['user_id'];

$mysqldate = date( 'Y-m-d' );
$user = Table::Fetch('user', $user_id);


if(!$user){
      $response['error'] =  'User does not exist';
      echo json_encode($response);
      die;  
}

$email = $user['email'];

$subs = array(
		'user_email' => $email,
		'date' => $mysqldate,
	);
	

$count = Table::Count('daily_contest', $subs);	

if($count>0) {
      $response['error'] =  'User already enrolled for the day';
      echo json_encode($response);
      die;  
}
	$result = DB::Insert('daily_contest', $subs);
	$response['success'] =  I(DailyContestSignup);
	echo json_encode($response);
	die;  

?>