<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');


header('Content-type: application/json');

$json_request = (json_decode($_REQUEST['data']) != NULL) ? true : false;

if(!$json_request)
{
 echo 'Invalid Request';
 die;
}

$data = json_decode ($_REQUEST['data'], true);

$user_id = $data['user_id'];

$user = Table::Fetch('user', $user_id);

if(!user) {
  $response['error'] = 'Invalid User';
  echo json_encode($response);
  die;
}

	$update = array(
		'email' => $data['email'],
		'username' => $data['username'],
		'realname' => $data['realname'], 
		'zipcode' => $data['zipcode'],
		'address' => $data['address'],
		'mobile' => $data['mobile'],   	
		'gender' => $data['gender'],
          	//'region' => $data['region'], 
          	//'city_id' => abs(intval($data['city_id'])),
		'region' => $data['city_id'], 
		'city_id' => $data['region'],
		'country' => $data['country'],		
	);

	//$avatar = upload_image('upload_image',$user['avatar'],'user');
	//$update['avatar'] = $avatar;

	if ( $data['subscribe'] ) {                 
		ZSubscribe::Create($data['email'], abs(intval($user['city_id'])), $data['frequency']);
	} else {
	    ZSubscribe::Unsubscribe($user);
    }

	if ( $data['password'] && $data['password'] == $data['password2'] ) 
	{
		$update['password'] = $data['password'];
	}
	
	//delete existing user interests
	$condition = array( 'user_id' => $user['id'],);	
	DB::Delete('user_interests', $condition);
	
	//insert newly selected user interests
	foreach($data['category'] as $c)
	{
		$ui = array(
		'user_id' => $user['id'],
		'interest_id' => $c,
		);
		DB::Insert('user_interests', $ui);
	}
	
	if ( ZUser::Modify($user['id'], $update) ) {
		$response['success'] =  'account resetting succeeded';

	} else {
		$response['error'] =  'account resetting failed';
	}

  echo json_encode($response);
  die;

?>