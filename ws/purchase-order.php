<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');


header('Content-type: application/json');

$json_request = (json_decode($_REQUEST['data']) != NULL) ? true : false;

if(!$json_request)
{
 echo 'Invalid Request';
 die;
}

$data = json_decode ($_REQUEST['data'], true);

$order_id = $data['order_id'];

$order = Table::Fetch('order', $order_id);

if(!$order) {
  $response['error'] = 'Invalid Order Request';
  echo json_encode($response);
  die;
}

if ( $order['state'] == 'pay' )
{
	$response['success'] =  'Order Purchase Succeeded';
	echo json_encode($response);
  	die;
}


/*
card type - possible set of values: Visa, MasterCard, Amex, Diners, JCB, AirPlus
card number - 16 digit number
card ccv - 3 digit number
card expiry year: YYYY (number)
card expiry month: MM (number 01-12)
order amount - numeric value picked from order total
order id - numeric value generated from system
*/


    $cardno = $data['card_number'];
    $expm = $data['card_expiry_month'];
    $expy = substr($data['card_expiry_year'], -2); 
    $bankaccount  = "";
    $cvv = $data['card_ccv'];  
    $amount = 1*$order['origin']; //$order['price']; 
    

    $currency = "CHF";
    $refno = "Pay-500-3201-".$order_id;
    $bankrouting = "";
    $hiddenMode = "yes";
    
    $merchantID = "1100002495";
    
    
    $params = array(
        "cardno" => $cardno,
        "refno" => $refno,          // Order Payment ID
        "amount" => $amount,
        "currency" => $currency,
        "expm" => $expm,
        "expy" => $expy 
   );
    
    $options = array ("merchantID" => $merchantID);
    $datatrans = new DataTrans($options);
    //$transid = $datatrans->MakeAuthorization($params);            // Do this for Authorization (before deal tipping )
    //$transid = $datatrans->MakeSettlement($transid, $params);     // Do this for capturing an Authorized Transaction
    $transid = $datatrans->MakePayment($params);    // Do this for Complete Payment

    if ( !$transid ) {
        $response['error'] =  'Order Purchase Failed';
    }
    else {
	$response['success'] =  'Order Purchase Succeeded';   
        $pay_id = $order['pay_id'];
	$service = $bank = "post";
	$transaction_state = "sale";
      
        ZOrder::OnlineIt($order_id, $pay_id, $amount, $currency, $service, $bank, $transaction_state, $transid); 
    }

  echo json_encode($response);
  die;

?>