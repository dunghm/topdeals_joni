<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

header('Content-type: application/json');

print_r($_REQUEST);

$json_request = (json_decode($_REQUEST) != NULL) ? true : false;

if(!$json_request)
{
 echo 'Invalid Request';
 die;
}


$data = json_decode ($_REQUEST, true);

	$login_user = ZUser::GetLogin($data['email'], $data['password']);
	if ( !$login_user ) {
		$response['error'] =  'Login failed';
	    echo json_encode($response);
	 	die;
	} else {
		ZCredit::Login($login_user['id']);
		$response['user_id'] =  $login_user['id'];
		echo json_encode($response);
		die;
	}
?>