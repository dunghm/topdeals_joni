<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');


$daytime = strtotime(date('Y-m-d'));
$type = '';
$selector = strval($_GET['t']);
if ( $selector == 'w' )
    $type = 'weekly';
else if ( $selector == 'd' )
    $type = 'normal';

$now = time();
$condition = array(
	'system' => 'Y',
	"end_time > {$now}",
);

if (!option_yes('displayfailure')) {
	$condition['OR'] = array(
		"now_number >= min_number",
		"end_time > '{$daytime}'",
	);
}





$group_id = abs(intval($_GET['gid']));
if ($group_id) $condition['group_id'] = $group_id;


$count = Table::Count('team', $condition);
list($pagesize, $offset, $pagestring) = pagestring($count, 10);
$teams = DB::LimitQuery('team', array(
  'condition' => $condition,
	'order' => 'ORDER BY begin_time DESC, sort_order DESC, id DESC',
	'size' => $pagesize,
	'offset' => $offset,
));

foreach($teams AS $id=>$one){
	team_state($one);
	if (!$one['close_time']) $one['picclass'] = 'isopen';
	if ($one['state']=='soldout') $one['picclass'] = 'soldout';
	$teams[$id] = $one;


  
  foreach($one as $key=>$val){         
    //echo '<br>-'.$key.'='.$val;
    $deal[$key] = $val;
  }	


  $team_meta = ZTeam::GetMeta($one['id']); 

  foreach($team_meta as $key=>$val){         
    //echo '<br>-'.$key.'='.$val;
    $deal[$key] = $val ;
  } 
  
  /* multi buy options */
  $multi_con = array(
                  'team_id' => $one['id'],
                  );
  $multi = DB::LimitQuery('team_multi', array(
          'condition' => $multi_con,
  ));
  
    foreach($multi as $key=>$val){         
      //echo '<br>-'.$key.'='.$val;
      $options[$key] = $val ;
    }
  $deal['options']=$options; 
  
  
  $deals[$id] = $deal;  
  //echo '<br>- deal end -<br>';
  
  unset($options);
  unset($deal);

  
}

header('Content-type: application/json');
echo json_encode($deals);

?>