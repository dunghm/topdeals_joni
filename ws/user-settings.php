<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');


header('Content-type: application/json');

$json_request = (json_decode($_REQUEST['data']) != NULL) ? true : false;

if(!$json_request)
{
 echo 'Invalid Request';
 die;
}

$data = json_decode ($_REQUEST['data'], true);

$user_id = $data['user_id'];

$user = Table::Fetch('user', $user_id);

$condition = array();
$condition['email'] = $user['email'];	
$subscriber = DB::LimitQuery('subscribe', array('condition' => $condition,));
$subscriber=$subscriber[0];


$condition = array( 'user_id' => $user['id'],);	
$user_interests = DB::LimitQuery('user_interests', array('condition' => $condition,));

$interest_id =  Utility::GetColumn($user_interests, 'interest_id');

$condition = array( 'id' => $interest_id,);	
$interests = DB::LimitQuery('category', array('condition' => $condition,));
//$interests = Table::Fetch('category', $interest_id);


$user_setting['realname']= $user['realname'];
$user_setting['gender']= $user['gender'];
$user_setting['username']= $user['username'];
$user_setting['email']= $user['email'];
$user_setting['avatar']= $user['avatar'];
$user_setting['mobile']= $user['mobile'];
$user_setting['address']= $user['address'];
$user_setting['zipcode']= $user['zipcode'];
//$user_setting['region']= $user['region'];
$user_setting['region']= $user['city_id'];
$user_setting['country']= $user['country'];
//$user_setting['city_id']= $user['city_id'];
$user_setting['city_id']= $user['region'];
$user_setting['frequency']= $subscriber['frequency'];
$user_setting['category']= $interests;


header('Content-type: application/json');
echo json_encode($user_setting);

?>