<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');


header('Content-type: application/json');

$json_request = (json_decode($_REQUEST['data']) != NULL) ? true : false;

if(!$json_request)
{
 echo 'Invalid Request';
 die;
}


$data = json_decode ($_REQUEST['data'], true);

$user_id = $data['user_id'];

$condition = array( 'user_id' => $user_id, 'team_id > 0', );


$orders = DB::LimitQuery('order', array(
	'condition' => $condition,
	'order' => 'ORDER BY team_id DESC, id ASC',
));

$team_ids = Utility::GetColumn($orders, 'team_id');
$teams = Table::Fetch('team', $team_ids);
foreach($teams AS $tid=>$one){
	team_state($one);
	$teams[$tid] = $one;
}

foreach($orders as $one){
  //$user_orders[$key] = $val;
  $user_order['order_id'] = $one['id'];
  $user_order['title'] = $teams[$one['team_id']]['title'];
  $user_order['quantity'] = $one['quantity'];
  $user_order['amount'] = moneyit($one['origin']);
  
  if ($one['state']=='pay')
    $state = I(Paid);
  elseif ($teams[$one['team_id']]['close_time']>0)
    $state = I(Expired);
  else
    $state = I(ToBePaid);  
  
  $user_order['state'] = $state;
  
  $user_orders[] = $user_order;
  
}

header('Content-type: application/json');
echo json_encode($user_orders);

?>