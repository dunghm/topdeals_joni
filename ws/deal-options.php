<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

$id = abs(intval($_GET['id']));
$team = Table::FetchForce('team', $id);


/* multi buy options */
$multi_con = array(
		'team_id' => $id,
		);
$multi = DB::LimitQuery('team_multi', array(
	'condition' => $multi_con,
));

  
  foreach($multi as $key=>$val){         
    //echo '<br>-'.$key.'='.$val;
    $options[$key] = $val ;
  }

header('Content-type: application/json');
echo json_encode($options);

?>
