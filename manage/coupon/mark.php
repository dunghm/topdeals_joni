<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();

if(is_post()){
    $coupon_id = $_POST['coupon_id'];
    $coupon = Table::Fetch('coupon', $coupon_id);
    $order_item_id = $coupon['order_id'];
    $update_order_item = array();
     $update_order_item['first_name'] = $_POST["gift_first_name"];
     $update_order_item['last_name'] = $_POST["gift_last_name"]; 
     
     if($_POST['coupon_action'] == "buy"){
       $update_order_item['mode'] = "gift"; 
     }
     else{
         $update_order_item['mode'] = "buy"; 
     }
     Table::UpdateCache('order_item', $order_item_id, $update_order_item);
     Session::Set('notice', "Coupon Marked Gift Successfully.");
     redirect($_SERVER["HTTP_REFERER"]);
    //redirect(WEB_ROOT . '/manage/coupon/consume.php');
}


$id = intval($_GET['id']);

$coupon =  Table::Fetch('coupon', $id);
$action = strval($_GET['action']);

if($action == 'usable'){
    ZCoupon::UnConsume($coupon);
     Session::Set('notice', "Coupon Marked Usable Successfully.");
    redirect(WEB_ROOT . '/manage/coupon/consume.php');
}
else if($action == 'used'){
	
	# biz wali logic start
	$coupon = Table::FetchForce('coupon', $id);
	$partner = Table::Fetch('partner', $coupon['partner_id']);
	$team = Table::Fetch('team', $coupon['team_id']);
    $oi =  Table::FetchForce('order_item', $coupon['order_id']);
        
	if ( ZCoupon::Consume($coupon))
	{
		$coupons = Table::Fetch('coupon', array($coupon['order_id']), 'order_id');
		$status_check = TRUE;
		foreach($coupons as $c ){
			if ( $c['consume']=='N' ){
				$status_check = FALSE;
			}
		}
                    
        if($status_check == TRUE){
        	$order_item = Table::FetchForce('order_item', $coupon['order_id']);
            if ( $order_item['delivery'] == 'pickup' ){
            	ZOrder::UpdateShippingState(ZOrder::ShipStat_PickedUp, $coupon['order_id']);
			}
		}
	}
	# biz wali logic end
	
    /*if ( ZCoupon::Consume($coupon))
    {
        $coupons = Table::Fetch('coupon', array($coupon['order_id']), 'order_id');
        $status_check = TRUE;
        foreach($coupons as $c ){
            if ( $c['consume']=='N' ){
                $status_check = FALSE;
            }
        }

        if($status_check == TRUE){
            $order_item = Table::FetchForce('order_item', $coupon['order_id']);
            if ( $order_item['delivery'] == 'pickup' )
                ZOrder::UpdateShippingState(ZOrder::ShipStat_PickedUp, $coupon['order_id']);
        } 

    }*/
    Session::Set('notice', "Coupon Marked Used Successfully.");
    redirect(WEB_ROOT . '/manage/coupon/index.php');
}

redirect();
