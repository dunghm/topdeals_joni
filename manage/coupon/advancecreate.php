<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('market');

if (is_post()){
	$coupon = $_POST;

	$coupon['quantity'] = abs(intval($coupon['quantity']));
	$coupon['team_id'] = abs(intval($coupon['team_id']));
//	$coupon['partner_id'] = abs(intval($coupon['partner_id']));
//	$coupon['begin_time'] = strtotime($coupon['begin_time']);
//	$coupon['end_time'] = strtotime($coupon['end_time']);

	$error = array();
	
        // Step 1: Validate Team
        $team = Table::FetchForce('team', $coupon['team_id'] );
        if ( ! $team )
        {
            $error[] = "Invalid Deal! Please select Specifiy a Valid Deal";
        }
        // Step 2: Count Available / Issued Coupons
        // Step 3: (Requested Quantity + Generated) < Team Quantity
        
	if ( $coupon['quantity'] < 1 || $coupon['quantity'] > 1000 ) {
		$error[] = "the quantity of coupon produced once should be between 1-1000 piece";
	}

	if ( !$error && ZCoupon::AdvanceCreate($coupon) ) {
		Session::Set('notice', "{$coupon['quantity']} pieces of coupon produced successfully");
		redirect(WEB_ROOT . '/manage/coupon/advancecreate.php');
	}
	$error = join("<br />", $error);
	Session::Set('error', $error);
}

if ( ! $coupon )
{

    $coupon = array(
            //'begin_time' => time(),
            //'end_time' => strtotime('+3 months'),
            'quantity' => 10,
            //'money' => 10,
            //'code' => date('Ymd').'_WR',
    );
}
//$count = Table::Count('topic', $condition);
//list($pagesize, $offset, $pagestring) = pagestring($count, 20);
$daytime = strtotime(date('Y-m-d'));
$condition = array(               
        'expire_time > ' . $daytime,
);
$teams = DB::LimitQuery('team', array(
                        'condition' => $condition,
                       // 'size' => $pagesize,
                        //'offset' => $offset,
                       // 'order' => 'ORDER BY head DESC, last_time DESC',
                        ));
// $teams = Table::Fetch('team');


include template('manage_coupon_advancecreate');
