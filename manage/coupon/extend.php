<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('market');

if (is_post()){
    
        $newexpiry = strval($_POST['new_expiry']);
        $iNewExpiry = strtotime($newexpiry);
       
        if ( $iNewExpiry < time() ) {		
                Session::Set('error', "Invalid new expiry");
                redirect(WEB_ROOT . '/manage/coupon/index.php');
	}
        
        $coupon_id = strval($_POST['coupon_id']);
        $coupon = Table::Fetch('coupon', $coupon_id);
        if ( $coupon )
        {
            $coupon['expire_time'] = $iNewExpiry;
            DB::Update('coupon', $coupon_id, array('expire_time' => $iNewExpiry) );
            Session::Set('notice', "Coupon id: {$coupon_id} extended till {$newexpiry}");
            redirect(WEB_ROOT . '/manage/coupon/index.php');
        }
        $team_id = strval($_POST['team_id']);
        $team = Table::Fetch('team', $team_id);
        if ( !$team )
        {
            Session::Set('error', "Invalid Team ID");
            redirect(WEB_ROOT . '/manage/coupon/index.php');
        }
       
        
        $sql = "UPDATE coupon set expire_time = {$iNewExpiry} WHERE team_id = {$team_id}";
        DB::Query($sql);
	
        Session::Set('notice', "Coupons for Deal id: {$team_id} extended till {$newexpiry}");
        redirect(WEB_ROOT . '/manage/coupon/index.php');
}

redirect(WEB_ROOT . '/manage/coupon/index.php');
?>
