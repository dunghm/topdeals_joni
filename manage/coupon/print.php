<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();

$coupons = Array();
 $pagetitle = 'Print Coupon';

if(is_post()){
    
    if($_POST['marked_disputed'] == 1){
        $remarks= $_POST['remarks_disputed'];
    foreach($_POST['cpnid'] as $id)
    {
       Table::UpdateCache('coupon', $id,  array('remarks' => $remarks, 'status' => 1));
    }
    
    
    Session::Set('notice', 'Marked Disputed successfully');
    redirect(WEB_ROOT . '/manage/coupon/index.php');
    }
     if($_POST['marked_undisputed'] == 1){
        
        
    foreach($_POST['cpnid'] as $id)
    {
       Table::UpdateCache('coupon', $id,  array('status' => 0));
    }
    
    
    Session::Set('notice', 'Marked Disputed successfully');
    redirect(WEB_ROOT . '/manage/coupon/index.php');
    }
}
 
if ( isset($_GET['cpnid']))
{
    $id = strval($_GET['cpnid']);
    $c = ZCoupon::GetCouponForPrint($id);

    if (!$c) {
            Session::Set('error', "{$INI['system']['couponname']} doesnt exist.");
            redirect(WEB_ROOT . '/manage/coupon/index.php');
    }

    $order = $c['order'];
  $order_item = $c['order_item'];
  /*
  if($order['option_id']){
    $multi = Table::Fetch('team_multi', $order['option_id']);
   // $c['team']['title'] = $multi['title'];
   // $c['team']['title_fr'] = $multi['title_fr'];
    //$c['team']['market_price'] = $multi['market_price'];
    //$c['team']['summary'] = $multi['summary'];
     $c['team']['title'] = $multi['title'];
    $c['team']['title_fr'] = $multi['title_fr'];
    $c['team']['image'] = $multi['image'];  
    $c['team']['market_price'] = $multi['market_price'];
    $c['team']['summary'] = $multi['summary'];
  }
  */
  
    $coupons[] = $c;
    
    if ($order_item['mode']=='gift') { 
	$pagetitle = 'Print Gift Coupon';
	die(include template('manage_coupon_print_gift'));
    }

    die(include template('manage_coupon_print'));
}
elseif ( isset($_POST['cpnid']))
{
    foreach($_POST['cpnid'] as $id)
    {
        $c = ZCoupon::GetCouponForPrint($id);
      if ( $c ){
        
        $order = $c['order_item'];
        if($order['option_id']){
          $multi = Table::Fetch('team_multi', $order['option_id']);
       //   $c['team']['title'] = $multi['title'];
        //  $c['team']['title_fr'] = $multi['title_fr'];
         // $c['team']['market_price'] = $multi['market_price'];
         //  $c['team']['summary'] = $multi['summary'];
	 $c['team']['title'] = $multi['title'];
    $c['team']['title_fr'] = $multi['title_fr'];
    $c['team']['image'] = $multi['image'];  
    $c['team']['market_price'] = $multi['market_price'];
    $c['team']['summary'] = $multi['summary'];
        }              
        
            $coupons[] = $c;
      }
    }
    
}
if ( sizeof($coupons) > 0 )
    die(include template('manage_coupon_print'));

redirect(WEB_ROOT . '/manage/coupon/index.php');

?>
