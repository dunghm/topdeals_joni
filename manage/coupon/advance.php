<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();

$usage = array( 'Y' => 'used', 'N' => 'usable' );
$condition = array();

$condition['user_id'] = '0';
$condition['order_id'] = '0';

/* filter */
if (strval($_GET['pid'])!=='') {
	$pid = abs(intval($_GET['pid']));
	$condition['partner_id'] = $pid;
}
/*
if (strval($_GET['code'])) {
	$code = strval($_GET['code']);
	$condition[] = "code LIKE '%".mysql_real_escape_string($code)."%'";
}
if (strval($_GET['state'])) {
	$state = strval($_GET['state']);
	$condition['consume'] = $state;
}
*/
if (strval($_GET['tid'])!=='') {
	$tid = abs(intval($_GET['tid']));
	$state = 'Y'; $pid = ''; $code = '';
	$condition = array();
	$condition['team_id'] = $tid;
}
/* end */

$count = Table::Count('coupon', $condition);
list($pagesize, $offset, $pagestring) = pagestring($count, 50);
if ( strval($_GET['download'])) { $offset = 0; $pagesize = 100000; }

$coupons = DB::LimitQuery('coupon', array(
	'condition' => $condition,
	'size' => $pagesize,
	'offset' => $offset,
/*	'order' => 'ORDER BY begin_time DESC, end_time DESC', */
));

$partner_ids = Utility::GetColumn($coupons, 'partner_id');
$partners = Table::Fetch('partner', $partner_ids);
$teams = Table::Fetch('team', Utility::GetColumn($coupons, 'team_id'));
$multis = Table::Fetch('team_multi', Utility::GetColumn($coupons,'option_id'));

if ( strval($_GET['download'])) {
	$name = "coupon_{$state}_".date('Ymd');
	$kn = array(
		'id' => 'No.',
		'secret' => 'Secret',
		'team' => 'Deal',
                'option'  => 'Option'
	);
       
	foreach($coupons AS $cid => $one) {
		$one['id'] = '#'.$one['id'];
		$one['team'] = $teams[$one['team_id']]['title'];
                if($one['option_id'])
                $one['option'] = $multis[$one['option_id']]['title'];
                else
                $one['option'] = '';
		$coupons[$cid] = $one;
	}
	down_xls($coupons, $kn, $name);
}

include template('manage_coupon_advance');
