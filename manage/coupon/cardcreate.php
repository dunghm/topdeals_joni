<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('market');

if (is_post()){
	$card = $_POST;

	$card['quantity'] = abs(intval($card['quantity']));
	$card['money'] = abs(intval($card['money']));
	$card['partner_id'] = abs(intval($card['partner_id']));
	$card['begin_time'] = strtotime($card['begin_time']);
	$card['end_time'] = strtotime($card['end_time']);
        $card['min_order'] = abs(intval($card['min_order']));
        $card['max_use'] = abs(intval($card['max_use']));
        $card['value_type'] = abs(intval($card['value_type']));
        
        $card['limit_per_deal'] = abs(intval($card['limit_per_deal']));
        $card['limit_per_user'] = abs(intval($card['limit_per_user']));
        $card['limit_per_user_deal'] = abs(intval($card['limit_per_user_deal']));
        $card['discount'] = floatval($card['discount']);
        $card['promo_code'] = trim($card['promo_code']);
        
        if ( !isset($card['allow_other']))
            $card['allow_other'] = 'N';
        
	$error = array();
	if ( $card['value_type'] == 1 && $card['money'] < 1 ) {
		$error[] = "the coupon value is no less than 1";
	}
        
        if ( $card['value_type'] == 2 && $card['discount'] < 0 ) {
		$error[] = "the discount value should be greator than 0";
	}
        
        if ( $card['max_use'] < 0 ) {
		$error[] = "Max use value must not be less than 0";
	}
        
        if ($card['max_use'] != 1 && $card['promo_code'] == "" ){
            $error[] = "Promo Code must be non empty string for multi use card";
        }
        
        if ( $card['promo_code'] != "" ){
            $prev = Table::Fetch('card', $card['promo_code'], 'promo_code');
            if ( $prev )
                $error[] = "Promo code must be unique";
        }
        
        if ( $card['max_use'] != 1 && $card['quantity'] > 1 ) {
		//$error[] = "Multi Use coupon quantity must be 1";
            $card['quantity']  = 1;
	}
        
	if ( $card['max_use'] == 1 && ($card['quantity'] < 1 || $card['quantity'] > 1000) ) {
		$error[] = "the quantity of coupon produced once should be between 1-1000 piece";
	}
        
        if ( $card['limit_per_user'] > 0 )
        {
            // Limit per Person
            if ( $card['limit_per_user_deal'] > 0 && $card['limit_per_user'] < $card['limit_per_user_deal'] ){
                $error[] = "Per User limit is greator than Per User Per Deal";
            }
        }
	$today = strtotime(date('Y-m-d'));
	if ( $card['begin_time'] < $today ) {
		$error[] = "beginning time is no earlier than today";
	}
	elseif ( $card['end_time'] < $card['begin_time'] ) {
		$error[] = "end time is no earlier than beginning time";
	}
	if ( !$error && ZCard::CardCreate($card) ) {
		Session::Set('notice', "{$card['quantity']}pieces of coupon produced successfully");
		redirect(WEB_ROOT . '/manage/coupon/cardcreate.php');
	}
	$error = join("<br />", $error);
	Session::Set('error', $error);
}
else {
	$card = array(
		'begin_time' => time(),
		'end_time' => strtotime('+3 months'),
		'quantity' => 10,
		'money' => 10,
		'code' => date('Ymd').'_WR',
                'max_use' => 1,
                'allow_other' => 'Y',
                'discount' => '0',
	);
}

include template('manage/manage_coupon_cardcreate');
