<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('market');


if (is_post()){
	$card = $_POST;
       

	$card['team_id'] = abs(intval($card['team_id']));
	//$card['code'] = abs(intval($card['money']));
	
	$error = array();
	 // Step 1: Validate Team
        $team = Table::FetchForce('team', $card['team_id'] );
        if ( ! $team )
        {
            $error[] = "Invalid Deal! Please select Specifiy a Valid Deal";
        }
        else
        {
	
            team_state($team);

            if ( $team['close_time'] == 0 )
            {
                $error[] = "Deal is still running!";
            }

            if ( $team['state'] == 'failure' )
            {
                $error[] = "Deal is failed!";
            }
        }
        
        $cardcondition = array( 'code' => $card['code'],
                             'consume' => 'N');
        $cardcount = Table::Count('card', $cardcondition );
        
        if ( $cardcount == 0 )
        {
             $error[] = "No cards, invalid Card Code!"; 
        }
        
        $ordercondition = array ( 'team_id' => $card['team_id'],
                             'state' => 'pay' );
        
        $gift_recipient_count = Table::Count('order', $ordercondition, 'quantity');
        
        if ( $gift_recipient_count == 0 )
        {
              $error[] = "No orders yet!";
        }
        
        if ( $cardcount <  $gift_recipient_count )
        {
            $error[] = "Available gift cards are less than Gift Recipients!";
        }
        
       
        
        if ( !$error )
        {
            $sql = "SELECT email, sum(quantity) as quantity FROM `order` Left Join `user` on `user`.id = `order`.user_id where team_id = {$team['id']}  AND state = 'pay'  group by user_id";
            $order_mails = DB::GetQueryResult($sql, false);
	
            
            $cards = DB::LimitQuery('card', array ( 'condition' => $cardcondition ));
            $cardids = Utility::Getcolumn($cards, 'id');


$mtime = microtime();
$mtime = explode(" ",$mtime);
$mtime = $mtime[1] + $mtime[0];
$starttime = $mtime;

            $index = 0;
            foreach( $order_mails as $email )
            {
                $qty = abs(intval($email['quantity']));
                for($i = 0 ; $i < $qty; $i++ )
                {
                    $counter = 0;
                    while ( $counter++ != 1000 )
                        mail_giftcard_send($email['email'], $cardids[$index]);
                    $index++;
                }                
            }
            
$mtime = microtime();
$mtime = explode(" ",$mtime);
$mtime = $mtime[1] + $mtime[0];
$endtime = $mtime;
$totaltime = ($endtime - $starttime);

            Session::Set('notice', "{$index} gift cards dispatched successfully. Took {$totaltime} seconds.");
            redirect(WEB_ROOT . '/manage/coupon/cardsend.php');
        }
        
        $error = join("<br />", $error);
	Session::Set('error', $error);	
}

include template('manage_coupon_cardsend');

?>
