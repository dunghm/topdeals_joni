<?php

// Manage / Statement / Index
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
$selector = 'supplier';

#post fields of filters
$title = '';
if(!empty($_POST['title']))
{
	$title = $_POST['title'];
}

#partner
$partnerTitle = '';
$partnerJoin = '';
if(!empty($_POST['partner']))
{
	$partnerJoin = 'LEFT JOIN partner p ON p.id = t.partner_id ';	
	$partnerTitle = $_POST['partner'];	
}

#supplier_id
$supplier_id = 0;
if(!empty($_POST['supplier_id']))
{
	$supplier_id = $_POST['supplier_id'];
}

#get query condition 
$queryCondition = revenueQueryCondition($_POST);

$sql = "SELECT t.id, t.`title`,t.`partner_id`,(team_price - partner_revenue) as margin, 
        (SELECT SUM(oi.`total`) FROM order_item oi
        
WHERE oi.`team_id` = t.`id` ) AS `total_revenue`, 
       (SELECT SUM(oi.`quantity`) FROM order_item oi  LEFT JOIN `order` AS o ON o.id = oi.order_id  WHERE oi.`team_id` =t.`id` AND o.state = 'pay') AS `total_sold`,
(SELECT COUNT(*) FROM coupon c WHERE c.`team_id` = t.`id`) AS `coupon_generated`, 
(SELECT COUNT(*) FROM coupon c WHERE c.`team_id` = t.`id` AND c.`consume` = 'Y') AS `coupon_validated`, 
(SELECT COUNT(*) FROM coupon c WHERE c.`team_id` = t.`id` AND c.`ps_id` <> 0 AND c.`consume` = 'Y') AS `coupon_invoiced`,
(SELECT COUNT(*) FROM coupon c WHERE c.`team_id` = t.`id` AND c.`ps_id` = 0 AND c.`consume` = 'Y' ) AS `coupon_waiting_invoiced`,
s.title AS `supplier_name`
 FROM team t 
INNER JOIN supplier s ON s.id = t.supplier_id 
$partnerJoin
$queryCondition
GROUP BY t.supplier_id ";
/*
$sql = " ";
  */  
//echo $sql;
$sql_count = " 
    SELECT COUNT(*) AS count FROM ( $sql ) AS t1";


$result_count = DB::GetQueryResult($sql_count, false);

$count = $result_count[0]['count'];


list($pagesize, $offset, $pagestring) = pagestring($count, 20);

$sql .= " ORDER BY t.id DESC LIMIT $offset, $pagesize";

$revenue = DB::GetQueryResult($sql, false);

$revenueTemp	=	$revenue;
if(is_array($revenue) && count($revenue)>0)
{
	foreach($revenue as $key=>$data)
	{
		$teamId = $data['id'];
		
		#get Total Revenue Generated for team
		$margin_sql = "Select IFNULL((t.team_price - t.partner_revenue),0) * oi.`quantity` as margin1 from team AS t 
					LEFT JOIN team_multi AS tm ON t.id = tm.team_id 
					LEFT JOIN `order_item` oi 
					ON
					oi.team_id = t.id 
					LEFT JOIN `order` o 
					ON  o.id= oi.order_id 
					where t.id='$teamId' AND o.state = 'pay' AND oi.option_id IS  NULL
					GROUP BY oi.team_id

		";
		//echo $margin_sql."<br/>";
		$margin_result1 = DB::GetQueryResult($margin_sql, false);
		
		$margin1Total	=	0;
		if(is_array($margin_result1) && count($margin_result1)>0)
		{
			foreach($margin_result1 as $row1)
			{
				$margin1Total +=$row1['margin1'];
			}
		}
		#get Total Revenue Generated for team_multi
		$margin_sql2 = "SELECT  (tm.team_price - tm.partner_revenue) as TDmargin,  oi.`quantity`,IFNULL((tm.team_price - tm.partner_revenue),0)*oi.`quantity` as totalResult FROM 
							order_item AS oi
							LEFT JOIN 
							team_multi tm
							ON
							tm.id = oi.option_id
							WHERE
							 oi.team_id = '$teamId'
							AND
								oi.option_id is not null
							AND
								oi.order_id in (select id from `order` o where o.state = 'pay') 
							group by oi.id
							";
		//echo $margin_sql."<br/>";
		$margin_result2 = DB::GetQueryResult($margin_sql2, false);

		$margin2Total = 0;
		if(is_array($margin_result2) && count($margin_result2)>0)
		{
			foreach($margin_result2 as $row)
			{ 
				$margin2Total +=$row['totalresult'];
			}
		}

		$totalMargin = $margin1Total+$margin2Total;
		$revenueTemp[$key]['totalMargin']	=	$totalMargin;


	}

}

$revenue	=	$revenueTemp;

#supplier 
$suppliers = DB::LimitQuery('supplier', array(
			'order' => 'ORDER BY title ASC',
			));
$suppliers = Utility::OptionArray($suppliers, 'id', 'title');

include template('manage_revenue_supplier');