<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager(true);

if(is_post()){
    
    $fares = array();
    $fares['fares_title'] = $_POST['fares_title'];
    $fares['fares_type'] = $_POST['fares_type']; 
    $fares['fares_weight'] = $_POST['fares_weight'];
    $fares['fares_price'] = $_POST['fares_price'];
    $fares['fares_length'] = $_POST['fares_length'];
    $fares['fares_width'] = $_POST['fares_width'];
    $fares['fares_height'] = $_POST['fares_height'];
    
    if(isset($_POST['id'])){
        $id = $_POST['id'];
        Table::UpdateCache('shipment_fares', $id, $fares);
        Session::Set('notice', 'Fares Updated Successfully');
    }
    else{
        DB::Insert('shipment_fares', $fares);
        Session::Set('notice', 'Fares Inserted Successfully');
    }
    redirect( WEB_ROOT . "/manage/system/shipping.php");
}

if(isset($_GET['id'])){
    $id = intval($_GET['id']);
    $shipment_fares = Table::Fetch('shipment_fares', $id);
}


include template('manage_system_shipping_edit');