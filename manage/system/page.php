<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_manager('admin');

$pages = array(
	'help_tour' => 'Tour ' . $INI['system']['abbreviation'],
	'help_faqs' => 'Faqs',
	'help_wroupon' => 'DailyHypes ' . $INI['system']['abbreviation'],
	'help_api' => 'API',
	'about_contact' => 'Contact',
	'about_us' => 'About' . $INI['system']['abbreviation'],
	'about_job' => 'Job',
	'about_terms' => 'Terms',
	'about_privacy' => 'Privacy',
	'about_press' => 'Press', 
        'help_enterprise' => 'Enterprise'
);

$id = strval($_GET['id']);
if ( $id && !in_array($id, array_keys($pages))) { 
	redirect( WEB_ROOT . "/manage/system/page.php");
}
$n = Table::Fetch('page', $id);

if ( $_POST ) {
	$table = new Table('page', $_POST);
	$table->SetStrip('value');
	$table->SetStrip('value_fr');
	if ( $n ) {
		$table->SetPk('id', $id);
		$table->update( array('id', 'value', 'value_fr') );
	} else {
		$table->insert( array('id', 'value', 'value_fr') );
	}
	Session::Set('notice', "Page：{$pages[$id]}Edited Successfully");
	redirect( WEB_ROOT . "/manage/system/page.php?id={$id}");
}

$value = $n['value'];
$value_fr = $n['value_fr'];
include template('manage_system_page');
