<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager(true);

if(isset($_GET['action'])){
    
    $action = $_GET['action'];
    $id = $_GET['id'];
    
    if($action == 'activate'){
        
        DB::Query('update videos set default=0');
        Table::UpdateCache('videos', $id, array('default' => 1));
        Session::Set('notice', 'Video Made Default Successfully');
    }
    else if($action == 'remove'){
        Table::Delete('videos', $id);
        Session::Set('notice', 'Video Deleted Successfully');
    }
    
}


$videos = DB::LimitQuery('videos');


include template('manage_system_videos');