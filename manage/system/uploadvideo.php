<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager(true);

ini_set('display_errors', 1);

if(is_post()){
    $video_name = $_POST['video_title'];
         $video_starttime = $_POST['video_starttime'];
         $video_endtime = $_POST['video_endtime'];
         $video_frequency = $_POST['frequency'];
         $video_duration = $_POST['duration'];
          
         
         if(isset($_POST['id'])){
             $id = $_POST['id'];
             
             $video = array(
                'video_name' => $video_name,
                //'video_location' => $_FILES['video_file']['name'], 
                'video_starttime' => strtotime($video_starttime),
                'video_endtime' => strtotime($video_endtime),
                 'duration' => $video_duration,
                'frequency' => $video_frequency,
            );
             if(isset($_POST['default_video'])){
                    $update_sql = "UPDATE videos SET `default` = 0";
                    DB::Query($update_sql);
                    $video['default'] = 1;
                }
                else{
                    $video['default'] = 0;
                }
           DB::Update('videos', $id,  $video);  
              Session::Set('notice', 'Video Successfully Updated!');
              redirect( WEB_ROOT . "/manage/system/videos.php");
         }
     if (isset($_FILES['video_file']) && $_FILES["video_file"]["error"] == 0) {
         
         
         
         
         $path = IMG_ROOT . '/' .$_FILES['video_file']['name'];
         move_uploaded_file($_FILES['video_file']['tmp_name'], $path);
         
         $video = array(
             'video_name' => $video_name,
             'video_location' => $_FILES['video_file']['name'], 
             'video_starttime' => strtotime($video_starttime),
             'video_endtime' => strtotime($video_endtime),
             'duration' => $video_duration,
             'frequency' => $video_frequency,
         );
         
         if(isset($_POST['default_video'])){
             $update_sql = "UPDATE videos SET `default` = 0";
             DB::Query($update_sql);
             $video['default'] = 1;
         }
         
         DB::Insert('videos', $video);
         
         
         Session::Set('notice', 'Video Successfully Uploaded!');
     
		redirect( WEB_ROOT . "/manage/system/videos.php");
     }
}