<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager(true);
$system = Table::Fetch('system', 1);

if ($_POST) {
	$team = $_POST;
	$insert = array(
		'type','name','value',
		);
		
	$team['type'] = 'deal';
	$team['name'] = 'reward';
	$team['value'] = $team['reward'] ;
	
	$result = DB::Query("Insert into preference (type, name, value) VALUES ('deal', 'reward',' ".$team['reward']. "')");
	
	Session::Set('notice', 'New Reward Added Successfully');
	redirect( WEB_ROOT . '/manage/system/deal.php');	
}

include template('manage_system_deal');
