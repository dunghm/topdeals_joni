<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager(true);
$shipment_fares = DB::LimitQuery('shipment_fares');

//print_r($shipment_fares);

if(isset($_GET['action']) && strval($_GET['action']) == 'remove'){
    $id = $_GET['id'];
    Table::Delete('shipment_fares', $id);
    Session::Set('notice', 'Shiping fares deleted');
    redirect( WEB_ROOT . '/manage/system/shipping.php');
}

include template('manage_system_shipping');