<?php

/*
 * Generate Sitemap 
 * 
 *  */
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager(true);

$system = Table::Fetch('system', 1);

if($_POST){
    
    $location = $_POST['location'];
    $static_pages = $_POST['static_pages'];
    $static_pages_priority = $_POST['static_pages_priority'];
    $deal_category_priority = $_POST['deal_category_priority'];
    $deal_priority = $_POST['deal_priority'];
    
    $active_deals = $_POST['active_deals'];
    $inactive_deal_priority = $_POST['inactive_deal_priority'];
    $st_pages = explode(',', html_entity_decode(trim($static_pages)));
    
    /*
    $xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n";
    $xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    $xml .= '</urlset>';
    $file = fopen(WWW_ROOT.'/'.$location,"w");
    fwrite($file, $xml);
    fclose($file);
    */
    
    
    $arr = array();
    $arr['sitemap']['location'] = $location;
    $arr['sitemap']['static_pages'] = array_map('trim', array_map('strip_tags', $st_pages));
    $arr['sitemap']['static_pages_priority'] =  $static_pages_priority;
    $arr['sitemap']['active_deals'] =  $active_deals;
    
    $arr['sitemap']['deal_priority']  = $deal_priority;
    $arr['sitemap']['deal_category_priority'] = $deal_category_priority;
    $arr['sitemap']['inactive_deal_priority'] = $inactive_deal_priority;
    $INI = Config::MergeINI($INI, $arr);
    $INI = ZSystem::GetUnsetINI($INI);
    
    save_config();
    $value = Utility::ExtraEncode($INI);
    $table = new Table('system', array('value'=>$value));
    if ( $system ) $table->SetPK('id', 1);
    $flag = $table->update(array( 'value'));
    
    Session::Set('notice', 'Sitemap information updated!');
}

include template('manage_system_generatesitemap');