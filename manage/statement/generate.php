<?php
// Manage / Statement / Generate
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();

if ( is_post() )
{
	
	//CHECK COUPON IS SLEECTED OR NOT
	$selectedCoupon		=	array();
	$omittedCoupon		=	array();
	$omittedCouponNo	=	array();
	
	if(isset($_POST['coupon'])){
			
		$couponList	=	$_POST['coupon'];
		
		if(!empty($couponList)){
			foreach($couponList as $couponNumber=>$couponListRow){
				//check coupoun is select
				if(isset($couponListRow["'id'"])){
					$selectedCoupon[]	=	"'".$couponListRow["'id'"]."'";
				}else{
					$omittedCoupon[$couponNumber]	=	$couponListRow["'comment'"];
					$omittedCouponNo[]				=	"'".$couponNumber."'";
				}
			}
		}
	}
	
	
	
	
    $team_id 	= $_POST['team_id'];
	$revenue	=	$_POST['revenue'];
	// echo "<pre>";
	// echo "SELECT COUPON ".print_r($selectedCoupon);
	// echo "OMITTED COUPON  ".print_r($omittedCoupon);
	// echo $team_id;
	// exit();
    $team = Table::Fetch('team', $team_id);
    if ( !$team )
    {
        Session::Set('error', "Deal not found.");
        
         redirect($INI['system']["wwwprefix"]. "/manage/statement/generate.php");
    }
    
    // echo "Fetching Partner <br/>";
     $partner = Table::Fetch('partner', $team['partner_id']);
     if ( !$partner ){
         Session::Set('error', "Partner not found.");
        
         redirect($INI['system']["wwwprefix"]. "/manage/statement/generate.php");
     }
         
     
     // count coupons available for statement
     $count = Table::Count('coupon',
             array('team_id' => $team['id'],
                 'consume' => 'Y',
                 'ps_id' => '0'));
     
     //echo "Coupons that are available to be included in the statement ".$count."<br/>";
     if ( $count == 0  ) 
     {
         Session::Set('error', "No coupons available.");        
        redirect($INI['system']["wwwprefix"]. "/manage/statement/generate.php");
     }
     
     //echo "Updating Deal with Last PS Time <br/>"
     $now = strtotime(date('Y-m-d H:i'));
    Table::UpdateCache('team', $team['id'], array( 'ps_last_time' => $now));
    
    
     //echo "Creating Partner Statement record <br/>";
     // new coupons validated
     $ps = array();
     $ps['team_id'] = $team['id'];
     $ps['create_time'] = time();
     $ps['partner_id'] 	= $team['partner_id'];
     $ps['status'] 		= 1;
     $ps['final'] 		= 0;
	 $ps['revenue']		=	$revenue;
	 
     // if deal is ended/soldout and all coupons have validated
     // or coupons have expired
     if ( (($team['end_time'] > 0 && $team['end_time'] < $now) ||
           ($team['close_time'] > 0 && $team['close_time'] < $now)) )
     {
         // Deal ended or sold out
        if ($team['expire_time'] > 0 && $team['expire_time'] < $now )
        {
            // coupons expired
            $ps['final'] = 1;
        }
        else
        {
            // coupons left or not?
             // count coupons available for statement
            $unused_count = Table::Count('coupon',
                    array('team_id' => $team['id'],
                        'consume' => 'N'
                        ));
            
            if ( $unused_count == 0 )
                $ps['final'] = 1;
        }
     }
	 
	 
	 //CHANGE REVENUE IF SOME COUPON IS OMMITIED
	 if(!empty($omittedCouponNo)){
		 
		 $omittedCouponNoStr	=	implode(",",$omittedCouponNo);
		 
		 $couponRevenueSql	=	<<<SQL
							select sum(Final) revenue from (
							select c.ps_id, c.team_id, team.partner_revenue, ifnull(oi.option_id,0) option_id, 
							tm.partner_revenue option_revenue, 
							CASE WHEN oi.option_id IS NULL THEN team.partner_revenue ELSE tm.partner_revenue END Final
							from coupon c left join 
							order_item oi ON oi.id= c.order_id left join
							team_multi tm on
							oi.option_id = tm.id
							left join team on 
							c.team_id = team.id
							where (team.partner_revenue > 0 or  tm.partner_revenue > 0)
							and
							c.id NOT IN ($omittedCouponNoStr)
							and
							team.id = $team_id
							and team.partner_id !=0 and team.ps_start_time > 0 and c.ps_id = 0 and c.consume='Y') good
							group by team_id, ps_id
SQL;

		 $couponRevenueData 	= DB::GetQueryResult($couponRevenueSql, true);	
		 
		 if(isset($couponRevenueData['revenue'])){
			$ps['revenue']			= $couponRevenueData['revenue'];
		 }
	 }
     
     // Generate Statement 
     $insert = array('team_id', 'partner_id', 'create_time', 'status', 'final', 'revenue');
 
     $table = new Table("partner_statement", $ps);
     $id = $table->insert($insert);
     $ps['id'] = $id;
     
    // echo "UPdating coupon with Ps ID ".$ps['id']."<br/>";
     // Find Coupons that will go into Statement
     $sql = "UPDATE coupon SET ps_id = {$ps['id']} WHERE team_id = {$team['id']} AND ps_id = 0 AND consume='Y'";
     DB::Query($sql);
    
	//UPDATE STATUS FOR SELECTED COUPON
	if(!empty($selectedCoupon)){
		
		$selectedCouponArr	=	implode(",",$selectedCoupon);
		
		$updateSelectedSql	=	<<<SQL
		
								UPDATE
									coupon
								SET
									`status` = 1
								WHERE 
									id IN ($selectedCouponArr)
								
SQL;
		DB::Query($updateSelectedSql);
	}
	
	//UPDATE STATUS FOR UNSELECTED COUPON
	if(!empty($omittedCoupon)){
		
		foreach($omittedCoupon as $couponNum=>$coupounComment){
			
		
			$omittedCouponSQL	=	<<<SQL
			
									UPDATE
										coupon
									SET
										`status`  = 2,
										`comment` = '$coupounComment'
									WHERE 
										id = $couponNum
								
SQL;
			DB::Query($omittedCouponSQL);
		}
	}
	
	
     //mail_partner_statement_ready($partner, $team);
     Session::Set('notice', "Statement Generated");
     redirect($INI['system']["wwwprefix"]. "/manage/statement/index.php");
}


// count coupons available for statement
$condition = array(  'consume' => 'Y',
            'ps_id' => '0');

// Fetch Coupons that have been consumed but are not part of any statement
$coupons = DB::LimitQuery('coupon',
        array('condition'=> $condition));

$team_ids = Utility::GetColumn($coupons, 'team_id');

$now = strtotime(date('Y-m-d H:i'));
$t_condition = array(
    'partner_id != 0',
    "ps_start_time > 0 ",
    "id" => $team_ids,
);  


if(isset($_GET['title'])){
	$title = strval($_GET['title']);
	if ($title) { 
		//$t_con[] = "title like '%".mysql_escape_string($title)."%'";
		
		#call Partial search method and pass colmun names
		$columnNames = array('title'=>'title','id'=>'id');
		$t_con[] = PartialSearch::partialSearchQuery($columnNames,$title);	

		
		$teams = DB::LimitQuery('team', array(
					'condition' => $t_con,
					));
		$team_ids = Utility::GetColumn($teams, 'id');
		if ( $team_ids ) {
			$t_condition['id'] = $team_ids;
		} else { $title = null; }
	}
}
if(isset($_GET['ptitle'])){
	$ptitle = strval($_GET['ptitle']);
	if ($ptitle) { 
		$t_con[] = "title like '%".mysql_escape_string($ptitle)."%'";
		$partners = DB::LimitQuery('partner', array(
					'condition' => $t_con,
					));
		$partner_ids = Utility::GetColumn($partners, 'id');
		if ( $partner_ids ) {
			$t_condition['partner_id'] = $partner_ids;
		} else { $ptitle = null; }
	}
}
    
$teams = DB::LimitQuery('team', 
        array('condition' => $t_condition));
$partner_ids = Utility::GetColumn($teams, 'partner_id');
$partners = Table::Fetch('partner', $partner_ids);

$sql = "select ps_id, team_id, sum(Final) revenue from (
select c.ps_id, c.team_id, team.partner_revenue, ifnull(oi.option_id,0) option_id, 
tm.partner_revenue option_revenue, 
CASE WHEN oi.option_id IS NULL THEN team.partner_revenue ELSE tm.partner_revenue END Final
from coupon c left join 
order_item oi ON oi.id= c.order_id left join
team_multi tm on
oi.option_id = tm.id
left join team on 
c.team_id = team.id
where (team.partner_revenue > 0 or  tm.partner_revenue > 0)
and team.partner_id !=0 and team.ps_start_time > 0 and c.ps_id = 0 and c.consume='Y') good
group by team_id, ps_id";

$result = DB::GetQueryResult($sql, false);
//var_dump($result); exit;
$revenues = Utility::AssColumn($result, 'team_id');

include template('manage_statement_generate');
?>

