<?php
// Manage / Statement / Index
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();

$selector = 'index';

$condition = array();

$title = strval($_GET['title']);
if ($title) { 
	
	#call Partial search method and pass colmun names
	$columnNames = array('title'=>'title','id'=>'id');
	$t_con[] = PartialSearch::partialSearchQuery($columnNames,$title);	

	//$t_con[] = "title like '%".mysql_escape_string($title)."%'";
	$teams = DB::LimitQuery('team', array(
				'condition' => $t_con,
				));
	$team_ids = Utility::GetColumn($teams, 'id');
	if ( $team_ids ) {
		$condition['team_id'] = $team_ids;
	} else { $title = null; }
}

$ptitle = strval($_GET['ptitle']);
if ($ptitle) { 
	$t_con[] = "title like '%".mysql_escape_string($ptitle)."%'";
	$partners = DB::LimitQuery('partner', array(
				'condition' => $t_con,
				));
	$partner_ids = Utility::GetColumn($partners, 'id');
	if ( $partner_ids ) {
		$condition['partner_id'] = $partner_ids;
	} else { $ptitle = null; }
}

$filter = strval($_GET['filter']);
$tabFilter		=	"";
//$tabOpenForPaid TO Check Tab Open for All Or Paid Statement to show paid date coloumn
$tabOpenForPaid	=	"all";
if ( $filter )
{
    if ($filter == "paid" ){
        $condition['status'] = array(2,4);
        $selector = "paid";
		$tabFilter = "<input type='hidden' name='filter' value='paid'";
		$tabOpenForPaid	=	"paid";
    }
    else if ($filter == "unpaid" ){
        $condition['status'] = array(1,3);
        $selector = "unpaid";
		$tabFilter = "<input type='hidden' name='filter' value='unpaid'";
		$tabOpenForPaid	=	"unpaid";
    }
}

$count = Table::Count('partner_statement', $condition);
list($pagesize, $offset, $pagestring) = pagestring($count, 20);

$statements = DB::LimitQuery('partner_statement', array(
	'condition' => $condition,
	'order' => 'ORDER BY id DESC',
	'size' => $pagesize,
	'offset' => $offset,
));

$team_ids = Utility::GetColumn($statements, 'team_id');
$teams = Table::Fetch('team', $team_ids);

$partner_ids = Utility::GetColumn($statements, 'partner_id');
$partners = Table::Fetch('partner', $partner_ids);

include template('manage_statement_index');
?>