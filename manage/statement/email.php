<?php

// Manage / Statement / Index
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();

	$id = abs(intval($_GET['id']));
	
	if(isset($_GET['id'])){
		
		$statement_id	=	$_GET['id'];
		$statement 		= Table::Fetch('partner_statement', $id);

		$partner 	= Table::Fetch("partner", $statement['partner_id']);
		$team 		= Table::Fetch("team", $statement['team_id']);
		$html 		= get_statement_html($statement);

		//DEFAULT FORM FIELD
		$partnerName	=	$partner['name'];
		$partnerEmail	=	$partner['email'];
		$EmailSubject	=	"Topdeal Account Statement";
		$emailBody      =	"Dear Partner<br/>Please review the account statement for deal '{$team['title']}' at:<br/>";
		
		$fileName 		=	'statement-'.rand().'.pdf';
		$pdfFilePath	= 	dirname(dirname(dirname(__FILE__))) . '/tmp_pdfs/'.$fileName;
		$downloadPdf	=	$INI['system']['wwwprefix'].'/tmp_pdfs/'.$fileName;
		PDFExport::Export($html, $pdfFilePath, 'F');
		
		

		include template('manage_statement_email');
	}
	
	
	if ( is_post() ){
		
			// echo"<pre>";print_r($_POST);exit();
			
			$partner_email		=	trim($_POST['partner_email']);
			$partner_subject	=	trim($_POST['partner_subject']);
			$partner_message	=	trim($_POST['partner_message']);
			$statement_id		=	trim($_POST['statement_id']);
			$fileName			=	trim($_POST['fileName']);
			$pdfFilePath		=	trim($_POST['pdfFilePath']);
			$downloadPdf		=	trim($_POST['downloadPdf']);
			$partner_emailArr	=	explode(";",$partner_email);
				
				
			if(!empty($partner_email) && !empty($pdfFilePath) && !empty($partner_emailArr)){
				
				$mail             = new PHPMailer(); // defaults to using php "mail()"
				
				
				// $mail->isSMTP();                                      // Set mailer to use SMTP
				// $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
				// $mail->SMTPAuth = true;                               // Enable SMTP authentication
				// $mail->Username = 'localhostemailsend1@gmail.com';                 // SMTP username
				// $mail->Password = '483G7XWTJLujV6C';                           // SMTP password
				// $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
				// $mail->Port = 25;                                    // TCP port to connect to

				
				$body		=	$partner_message;
				if(empty($partner_message)){
					$body             = "Dear Partner<br/>Please review the account statement for deal '{$team['title']}' at:<br/>";
				}

				//$mail->AddReplyTo("name@yourdomain.com","First Last");
				global $INI;
				$from = $INI['mail']['from'];
				$from	=	trim($from);
				if(empty($from)){
					$from = 'commande@topdeal.ch';
				}
				
				$mail->SetFrom($from, '');

				//$mail->AddReplyTo("name@yourdomain.com","First Last");
				foreach($partner_emailArr as $partner_emailRow){
					$address = $partner_emailRow;
					$mail->AddAddress($address, "");
				}
				//$address = "abdullahrahim87@gmail.com";
				// $mail->AddAddress($partner['email'], $partner['title']);
				$subject	=	$partner_subject;
				if(empty($partner_subject)){
					$subject	=	"Topdeal Account Statement";
				}
				$mail->Subject    = $subject;

				//$mail->AltBody    = ""; // optional, comment out and test

				$mail->MsgHTML($body);

				if(!empty($pdfFilePath)){
					$mail->AddAttachment($pdfFilePath);      // attachment
				}
				//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment

				if(!$mail->Send()) {
				  $mailError	= "Mailer Error: " . $mail->ErrorInfo;
				   Session::set("notice", $mailError);
				} else {
				   $mailError	= "Email Send Successfully";
				   Session::set("notice", $mailError);
				}
				
			}else{
				Session::set("error", "Required Field Missing");
			}
			
			
			redirect("/manage/statement/index.php");
			exit;
	}
	
	function get_statement_html($statement){
		

		$partner = Table::Fetch('partner', $statement['partner_id'] );
		$team = Table::Fetch('team', $statement['team_id']);
		$id	=	 $statement['id'];
		//$coupons = Table::Fetch('coupon', array($statement['id']), "ps_id");
		$sql = "SELECT IFNULL(oi.option_id,0) option_id, COUNT(*) total FROM coupon c, order_item oi WHERE c.order_id = oi.id AND c.status NOT IN (2) AND ps_id = {$id} GROUP BY oi.option_id";
		//Get Ommitted Coupon If Any
		$ommitedCouponSql = "SELECT `id`,`comment` FROM coupon  WHERE   `status` IN (2) AND ps_id = {$id} ";
		
		
		$coupons = DB::GetQueryResult($sql, false);
		$couponsArr = DB::GetQueryResult($sql, false);
		//GET OMMITTED COUPON DB
		$ommitedCouponData = DB::GetQueryResult($ommitedCouponSql, false);
		$multi = Table::Fetch('team_multi', array($statement['team_id']), 'team_id');

		$grand_sum = 0;
		foreach($coupons as &$one)
		{
			if ( $one['option_id'] )
				$revenue = Table::Fetch('team_multi', $one['option_id']);
			else
				$revenue = $team;

			$one['partner_revenue'] = $revenue['partner_revenue'];
			$grand_sum += $one['total'] * $one['partner_revenue'];
		}
		$couponsArr = $coupons;
		$city  = Table::Fetch('category', $partner['city_id']);

		if (is_partner()) {
			if ($statement['status'] == 1)
				$new_status = 3;
			else if ( $statement['status'] == 2 )
				$new_status = 4;

			Table::UpdateCache('partner_statement', $id, array("status" => $new_status));
		}

		if ( $ps['final'] == 1 )
			$template = 'biz_statement_view_final';
		else if ( count($multi) > 0 )
			$template = 'biz_statement_view_multi';
		else
			$template = 'biz_statement_view';

		$vars = array(
			"grand_sum" => $grand_sum,
			"coupons" => $coupons,
			"couponsArr"=>$couponsArr,
			"ommitedCouponData"=>$ommitedCouponData,
			"team" => $team,
			"partner" => $partner
		);
		
		$html = render($template, $vars);
		//PDFExport::Export($html);
		return $html;
	}


?>
