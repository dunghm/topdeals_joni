<?php

// Manage / Statement / Index
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();

$id = abs(intval($_GET['id']));
$statement = Table::Fetch('partner_statement', $id);

$action = strval($_GET['action']);

switch($action)
{
    case "view":
        ViewStatement($statement);
        break;
    case "print":
        PrintStatement($statement);
        break;
    case "email":
        EmailStatement($statement);
        break;
    case "dtafile":
        GenDTAFile($statement);
        break;
    case "markpaid":
        MarkPaidStatement($statement);
        break;
    case "markunpaid":
        MarkUnPaidStatement($statement);
        break;
    default:
        ViewStatement($statement);
        break;
}

function ViewStatement($statement)
{
    global $INI;
    redirect($INI['system']["wwwprefix"]. "/biz/statement_view.php?id={$statement['id']}");
}

function PrintStatement($statement)
{
    global $INI;
    redirect($INI['system']["wwwprefix"]. "/biz/statement_view.php?id={$statement['id']}&action=print");
}

function EmailStatement($statement)
{
    /*
    global $INI;
    $partner = Table::Fetch("partner", $statement['partner_id']);
    $team = Table::Fetch("team", $statement['team_id']);
    
    $subject = "Topdeal Account Statement";
    $message = "Dear Partner<br/>Please review the account statement for deal '{$team['title']}' at:<br/>";
    $message .= "<a href='".$INI['system']['wwwprefix']."/biz/statement_view.php?id=".$statement['id']."'>{$INI['system']['wwwprefix']}/biz/statement_view.php?id=</a>";
    mail_custom($partner['email'], $subject, $message);
     * 
     */
    $partner = Table::Fetch("partner", $statement['partner_id']);
    $team = Table::Fetch("team", $statement['team_id']);
    $html = get_statement_html($statement);
	
    $name = 'statement-'.rand().'.pdf';
     PDFExport::Export($html, dirname(dirname(dirname(__FILE__))) . '/tmp_pdfs/'.$name, 'F');
     
    $mail             = new PHPMailer(); // defaults to using php "mail()"

    $body             = "Dear Partner<br/>Please review the account statement for deal '{$team['title']}' at:<br/>";
   

    //$mail->AddReplyTo("name@yourdomain.com","First Last");
    global $INI;
    $from = $INI['mail']['from'];
    $mail->SetFrom($from, '');

    //$mail->AddReplyTo("name@yourdomain.com","First Last");

    $address = "arshad@nextgeni.com";
    $mail->AddAddress($address, "Arshad");
    
    //$address = "abdullahrahim87@gmail.com";
    // $mail->AddAddress($partner['email'], $partner['title']);
    
    $mail->Subject    = "Topdeal Account Statement";

    //$mail->AltBody    = ""; // optional, comment out and test

    $mail->MsgHTML($body);

    $mail->AddAttachment(dirname(dirname(dirname(__FILE__))) . '/tmp_pdfs/'.$name);      // attachment
    //$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment

    if(!$mail->Send()) {
      echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
       redirect($_SERVER["HTTP_REFERER"]);
    }
    
    exit;
    
}

function get_statement_html($statement){
    

    $partner = Table::Fetch('partner', $statement['partner_id'] );
    $team = Table::Fetch('team', $statement['team_id']);
	$id	=	 $statement['id'];
    //$coupons = Table::Fetch('coupon', array($statement['id']), "ps_id");
    $sql = "SELECT IFNULL(oi.option_id,0) option_id, COUNT(*) total FROM coupon c, order_item oi WHERE c.order_id = oi.id AND c.status NOT IN (2) AND ps_id = {$id} GROUP BY oi.option_id";
	
    $coupons = DB::GetQueryResult($sql, false);
    $multi = Table::Fetch('team_multi', array($statement['team_id']), 'team_id');

    $grand_sum = 0;
    foreach($coupons as &$one)
    {
        if ( $one['option_id'] )
            $revenue = Table::Fetch('team_multi', $one['option_id']);
        else
            $revenue = $team;

        $one['partner_revenue'] = $revenue['partner_revenue'];
        $grand_sum += $one['total'] * $one['partner_revenue'];
    }
    $city  = Table::Fetch('category', $partner['city_id']);

    if (is_partner()) {
        if ($statement['status'] == 1)
            $new_status = 3;
        else if ( $statement['status'] == 2 )
            $new_status = 4;

        Table::UpdateCache('partner_statement', $id, array("status" => $new_status));
    }

    if ( $ps['final'] == 1 )
        $template = 'biz_statement_view_final';
    else if ( count($multi) > 0 )
        $template = 'biz_statement_view_multi';
    else
        $template = 'biz_statement_view';

    $vars = array(
        "grand_sum" => $grand_sum,
        "coupons" => $coupons,
        "team" => $team,
        "partner" => $partner
    );
    
    $html = render($template, $vars);
    //PDFExport::Export($html);
    return $html;
}


function GenDTAFile($statement)
{
    $sender = array(
        "name" => "Topdeal SA",
        "bank_code" => "123456",
        "account_number" => "55555"
    );
    
    $partner = Table::Fetch('partner', $statement['partner_id']);
    $transactions = array(
    );
    
    $transactions[] = array( "recipient" => array(
                "name"           => $partner['bank_user'],    // Name of account owner.
                "bank_code"      => $partner['bank_name'],           // Bank code.
                "account_number" => $partner['bank_no']           // Account number.
            ),
            "amount" => $statement['revenue'],                                      // Amount of money.
            "info" => array(                                      // Description of the transaction ("Verwendungszweck").
                "Statement no. {$statement['id']}",
                "Information" )
            );
    
    header('Content-Disposition: attachment; filename="statement_'.$statement['id'].'.dta"');
    DTAExport::GetDTAFile($sender, $transactions);
    
}
/*
 * 1 = New - Unpaid
 * 2 = New - Paid
 * 3 = Unpaid
 * 4 = Paid
 */
function MarkPaidStatement($statement)
{
    if ($statement['status'] == 1)
        $new_status = 2;
    else if ( $statement['status'] == 3 )
        $new_status = 4;
    else
    {
        Session::set("error", "Already Paid");
        redirect("/manage/statement/index.php");
    }
	$currentDateTime	=	strtotime(date('c'));
    Table::UpdateCache('partner_statement', $statement['id'], array("status" => $new_status,"paid_time"=>$currentDateTime));
    Session::set("notice", "Updated");
    redirect("/manage/statement/index.php");
}

function MarkUnPaidStatement($statement)
{
    if ($statement['status'] == 2)
        $new_status = 1;
    else if ( $statement['status'] == 4 )
        $new_status = 3;
    else
    {
        Session::set("error", "Already Unpaid");
        redirect("/manage/statement/index.php");
    }

    Table::UpdateCache('partner_statement', $statement['id'], array("status" => $new_status));
    Session::set("notice", "Updated");
    redirect("/manage/statement/index.php");
}

?>
