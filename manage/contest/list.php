<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('admin');

$date = date($_GET['date']);

$condition = array(
  'date' => $date,
);

$count = Table::Count('daily_contest', $condition);
list($pagesize, $offset, $pagestring) = pagestring($count, 20);

$users = DB::LimitQuery('daily_contest', array(
        'condition' => $condition,
	'order' => 'ORDER BY id',
	'size' => $pagesize,
	'offset' => $offset,
));



include template('manage_contest_list');
