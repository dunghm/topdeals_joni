<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('admin');

$date = date($_GET['date']);

$condition = array(
  'date' => $date,
);

$winner = DB::LimitQuery('daily_contest_results', array(
        'condition' => $condition,
	'order' => 'ORDER BY id',		
  ));


$random_winner = Table::Fetch('daily_contest', $winner[0]['winner']);

//echo $winner[0]['winner'] . '>' . $random_winner['user_email'];

if(!$random_winner) {

  $count = Table::Count('daily_contest', $condition);

  $users = DB::LimitQuery('daily_contest', array(
          'condition' => $condition,
          'order' => 'ORDER BY id',
  ));
  
  $r = rand(0,$count-1);
  
  $random_winner = $users[$r];
  

  $rw = array(
  	'date' => $random_winner['date'],
  	'winner' => $random_winner['id'],
  );
  DB::Insert('daily_contest_results', $rw);

  
} 
  
//echo $r . '<br>';

//echo 'Date:' . $date . '<br>Winner:' .  $random_winner['user_email'];

include template('manage_contest_winner');
