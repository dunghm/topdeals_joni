<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');
require_once(dirname(dirname(dirname(__FILE__))) . '/include/function/current.php');

need_manager();
need_auth('contest');

$id = abs(intval($_GET['id']));
$team = $eteam = Table::Fetch('contest', $id);

if ( is_get() && empty($team) ) {
	$team = array();
	$team['id'] = 0;
	$team['user_id'] = $login_user_id;
	$team['begin_time'] = strtotime('+1 days');
	$team['end_time'] = strtotime('+2 days'); 
	$team['expire_time'] = strtotime('+3 months +1 days');
	$team['min_number'] = 10;
	$team['per_number'] = 1;
	$team['market_price'] = 1;
	$team['team_price'] = 1;
	$team['delivery'] = 'coupon';
	$team['address'] = $profile['address'];
	$team['mobile'] = $profile['mobile'];
	$team['fare'] = 5;
	$team['farefree'] = 0;
	$team['bonus'] = abs(intval($INI['system']['invitecredit']));
	$team['conduser'] = $INI['system']['conduser'] ? 'Y' : 'N';
	$team['buyonce'] = 'Y';
}
else if ( is_post() ) {
	$team = $_POST;

	$title = $_POST['title'];
	$description = $_POST['description'];
	$deals = $_POST['deals'];
	$city_id = $_POST['city_id'];
	$start_time = strtotime($_POST['start_time']);
	$end_time = strtotime($_POST['end_time']);
	$result = DB::Query("Insert into contest (title, description, deals, start_time, end_time, city_id) VALUES ('$title', '$description', '$deals', '$start_time', '$end_time', '$city_id')");
	Session::Set('notice', 'New Contest Success');
	redirect( WEB_ROOT . "/manage/contest/create.php");
	} 
else 
{
	Session::Set('error', 'Edit project failure');
	redirect(null);
}



$groups = DB::LimitQuery('category', array(
			'condition' => array( 'zone' => 'group', ),
			));
$groups = Utility::OptionArray($groups, 'id', 'name');

$partners = DB::LimitQuery('partner', array(
			'order' => 'ORDER BY id DESC',
			));
$partners = Utility::OptionArray($partners, 'id', 'title');

$now = time();
$condition = array(
	'legitimate' => 1,
);

$count = Table::Count('userdeals', $condition);
list($pagesize, $offset, $pagestring) = pagestring($count, 20);

$deals = DB::LimitQuery('userdeals', array(
	'condition' => $condition,
	'order' => 'ORDER BY id DESC',
	'size' => $pagesize,
	'offset' => $offset,
));

$selector = $team['id'] ? 'edit' : 'create';
include template('manage_contest_create');
