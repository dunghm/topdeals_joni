<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');
require_once(dirname(dirname(dirname(__FILE__))) . '/include/function/current.php');

need_manager();
need_auth('team');
$now = time();
$condition = array(
	
);

$count = Table::Count('contest', $condition);
list($pagesize, $offset, $pagestring) = pagestring($count, 20);

$teams = DB::LimitQuery('contest', array(
	'condition' => $condition,
	'order' => 'ORDER BY id DESC',
	'size' => $pagesize,
	'offset' => $offset,
));
$cities = Table::Fetch('category', Utility::GetColumn($teams, 'city_id'));
//$groups = Table::Fetch('category', Utility::GetColumn($teams, 'group_id'));

    $selector = 'index';
include template('manage_contest_index');
