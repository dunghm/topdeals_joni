<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');
require_once(dirname(dirname(dirname(__FILE__))) . '/include/function/current.php');

need_manager();
need_auth('contest');
$contest_now = time();
$id = abs(intval($_GET['id']));
$city_id = abs(intval($_GET['city_id']));

$team = $eteam = Table::Fetch('contest', $id);


if ( is_get() && empty($team) ) {
	$team = array();
	/*$team['id'] = 0;
	$team['user_id'] = $login_user_id;
	$team['begin_time'] = strtotime('+1 days');
	$team['end_time'] = strtotime('+2 days'); 
	$team['expire_time'] = strtotime('+3 months +1 days');
	$team['min_number'] = 10;
	$team['per_number'] = 1;
	$team['market_price'] = 1;
	$team['team_price'] = 1;
	$team['delivery'] = 'coupon';
	$team['address'] = $profile['address'];
	$team['mobile'] = $profile['mobile'];
	$team['fare'] = 5;
	$team['farefree'] = 0;
	$team['bonus'] = abs(intval($INI['system']['invitecredit']));
	$team['conduser'] = $INI['system']['conduser'] ? 'Y' : 'N';
	$team['buyonce'] = 'Y';*/
	
}
else if ( is_post() ) {
	$team = $_POST;
	$insert = array('title','description','city_id','deals','start_time','end_time');
	
	/*$title = $_POST['title'];
	$description = $_POST['description'];
	$deals = $_POST['deals'];
	$city_id = $_POST['city_id'];
	$start_time = strtotime($_POST['start_time']);
	$end_time = strtotime($_POST['end_time']);*/
	$team['title'] = $_POST['title'];
	$team['description'] = $_POST['description'];
	$team['deals'] = $_POST['deals'];
	$team['city_id'] = $_POST['cityid'];
	$team['start_time'] = strtotime($_POST['start_time']);
	$team['end_time'] = strtotime($_POST['end_time']);
	
	//$result = DB::Query("Insert into contest (title, description, deals, start_time, end_time, city_id) VALUES ('$title', '$description', '$deals', '$start_time', '$end_time', '$city_id')");
	
	$insert = array_unique($insert);
	$table = new Table('contest', $team);
	
	if ( $team['id'] && $team['id'] == $id ) {
		$table->SetPk('id', $id);
		$table->update($insert);
		Session::Set('notice', 'Information Successfully Modified!');
		redirect( WEB_ROOT . "/manage/contest/index.php");
	} 
	else {
	
	Session::Set('error', 'Information Successfully Modified!');
		redirect( WEB_ROOT . "/manage/contest/index.php");
	}
		
	/*Session::Set('notice', 'New Contest Success');
	redirect( WEB_ROOT . "/manage/contest/create.php");*/
	} 


/*
$groups = DB::LimitQuery('category', array(
			'condition' => array( 'zone' => 'group', ),
			));
$groups = Utility::OptionArray($groups, 'id', 'name');

$partners = DB::LimitQuery('partner', array(
			'order' => 'ORDER BY id DESC',
			));
$partners = Utility::OptionArray($partners, 'id', 'title');

$now = time();

*/
$count = Table::Count('userdeals', $condition);
list($pagesize, $offset, $pagestring) = pagestring($count, 20);

$condition = array(
	'legitimate' => 1,
	'city_id' => $city_id,
);
$deals = DB::LimitQuery('userdeals', array(
	'condition' => $condition,
	'order' => 'ORDER BY id DESC',
	'size' => $pagesize,
	'offset' => $offset,
));


include template('manage_contest_edit');
