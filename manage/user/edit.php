<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('admin');

$id = abs(intval($_GET['id']));
$user = Table::Fetch('user', $id);

if ( $_POST && $id==$_POST['id'] ) {
    
	
        $post_arr = $_POST;
        
        if(isset($_POST['bd_month']) && isset($_POST['bd_day']) && isset($_POST['bd_year'])){   
            unset($post_arr['bd_month']);
            unset($post_arr['bd_day']);
            unset($post_arr['bd_year']);
            
            $birth_day = mktime(0,0,0, $_POST['bd_month'], $_POST['bd_day'],$_POST['bd_year']);
            $post_arr['birth_day'] = $birth_day;
         }
        
        
        $table = new Table('user', $post_arr);
	$up_array = array(
			'username', 'realname', 
			'mobile', 'zipcode', 'address','address2','email',
			'secret', 'qq', 'region', 'birth_day', 'first_name', 'last_name','gender'
			);
       $email = $_POST['email'];
       
      
	// unique email per user
         $sql_user_check = "SELECT * FROM `user` where `id` <> $id AND email = '$email'";
	$eu = DB::GetQueryResult($sql_user_check, false);
        
		if ($eu ) {
                        
			Session::Set('notice', 'Email address already exists, can not be modified');
			redirect( WEB_ROOT . "/manage/user/edit.php?id={$id}");
		}
                
	


	if ( $login_user_id == 1 && $id > 1 ) { $up_array[] = 'manager'; }
	if ( $id == 1 && $login_user_id > 1 ) {
		Session::Set('notice', 'You have no right to modify the information super administrator');
		redirect( WEB_ROOT . "/manage/user/index.php");
	}
	$table->manager = (strtoupper($table->manager)=='Y') ? 'Y' : 'N';
	if ($table->password ) {
		$table->password = ZUser::GenPassword($table->password);
		$up_array[] = 'password';
	}
        
	$flag = $table->update( $up_array );
	if ( $flag ) {
		#echo'<pre>';print_r($_POST);echo'</pre>';exit;
		# logged-in user can not disable him self
		if ( ($post_arr['id'] == $login_user_id) && !isset($post_arr['enable']) )
		{
			Session::Set('notice', 'Your information update successfully but you are not allow to disable your account.');
			redirect( WEB_ROOT . "/manage/user/edit.php?id={$id}");
			
		} else if ( ($post_arr['id'] == $login_user_id) && isset($post_arr['enable']) ){
			
			Session::Set('notice', 'Modify user information successfully');
			redirect( WEB_ROOT . "/manage/user/edit.php?id={$id}");
			
		} else if ($post_arr['id'] != $login_user_id) {
			
			# update status
			$enable = 'N';
			if(isset($post_arr['enable']) && $post_arr['enable']=='Y'){
				$enable = 'Y';
			}
			$user_id = $post_arr['id'];
			DB::Query("UPDATE user SET enable = '".$enable."' WHERE id = $user_id");
			Session::Set('notice', 'Modify user information successfully');
			redirect( WEB_ROOT . "/manage/user/edit.php?id={$id}");
		}		
	}
	Session::Set('error', 'Failed to modify user information');
	$user = $_POST;
}

include template('manage_user_edit');
