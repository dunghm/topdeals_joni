<?php
function current_manageteam($selector='edit', $id=0) {
	$selector = $selector ? $selector : 'edit';
	$a = array(
		"/manage/team/edit.php?id={$id}" => 'Basic information',
		"/manage/team/editzz.php?id={$id}" => 'Miscellaneous Information',
		"/manage/team/editseo.php?id={$id}" => 'SEO Information',
	        "/manage/team/edit_multi.php?id={$id}" => 'Multi Buy Options',
                 "/manage/team/editmeta.php?id={$id}" => 'Meta Info',
	);
	$l = "/manage/team/{$selector}.php?id={$id}";
	return current_link($l, $a);
}
