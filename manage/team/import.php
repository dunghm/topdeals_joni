<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');
require_once(dirname(__FILE__) . '/current.php');

//Authentication
need_manager();
need_auth('team');
$log = new KLogger(dirname(dirname(dirname(__FILE__)))."/include/compiled/", KLogger::DEBUG);

if(is_post()){
        if($_FILES["import_file"]["name"]) {
               
                $log->logInfo("Entered the script");

                $filename = $_FILES["import_file"]["name"];
                $source = $_FILES["import_file"]["tmp_name"];
                $type = $_FILES["import_file"]["type"];
                 $log->logInfo("Filename =".$filename);
                $name = explode(".", $filename);
                
                $accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
                foreach($accepted_types as $mime_type) {
                        if($mime_type == $type) {
                                $okay = true;
                                break;
                        } 
                }

                $continue = strtolower($name[1]) == 'zip' ? true : false;
                if(!$continue) {
                        Session::Set('error', 'Edit project failure');
                        redirect( WEB_ROOT . "/manage/team/index.php");
                }
                
                $target_path = dirname(dirname(dirname(__FILE__))).'/generated_labels/'.$filename;  // change this to the correct site path
                if(move_uploaded_file($source, $target_path )) {
                    $log->logInfo("File uploaded ");
                    $log->logInfo("File Target Locaton: ".$target_path);
                    $final_dir = dirname(dirname(dirname(__FILE__))).'/generated_labels/team-'.rand();
                    //rmdir_recursive($targetdir);
                    mkdir($targetdir, 0777); 
                    $zip = new ZipArchive();
                    $x = $zip->open($target_path);  // open the zip file to extract
                    if ($x === true) {
                        $zip->extractTo($final_dir); // place in the directory with same name  
                        $log->logInfo("Zip extracted to the location: ".$final_dir);
                        $zip->close();
                        unlink($target_path);
                       
                    }else{
                        $log->logInfo("Zip File cannot be extracted");
                    }
                    
                    // NOw play here
                    $json_team = file_get_contents($final_dir.'/team.json');
                    
                    $log->logInfo("Json Downloaded: ".$json_team);
                    $package = json_decode($json_team, true);
                    $team = $package['team'];
                    unset($team['id']);
                    $team['image'] = upload_image_import($final_dir.'/'.$team['image'], 'team', true);
                    //print_r_r($team);
                    $team_id = DB::Insert('team', $team); 
                     $log->logInfo("Team Inserted with team_id: ".$team_id);
                     
                    $multi_ids = array(); 
                    
                    if(isset($package['multi'])){
                        $multi = $package['multi'];
                        foreach($multi as $m){
                            $m['image']= upload_image_import($final_dir.'/multi/'.$m['image'], 'team', true);
                            $prev_id = $m['id'];
                            unset($m['id']);
                            $m['team_id'] = $team_id;
                            $new_id = DB::Insert('team_multi', $m); 
                            $multi_ids[$prev_id] = $new_id;
                        }
                    }
                    
                    $multi_new = DB::LimitQuery('team_multi', array(
			'condition' => array( 'team_id' => $team_id),
                        ));
                    
                    foreach($multi_new as $m_new){
                        if($m_new['parent_option_id'] != 0){
                                $update_arr = array();
                             $update_arr['parent_option_id'] = $multi_ids[$m_new['parent_option_id']];
                             DB::Update('team_multi', $m_new['id'], $update_arr);
                        }
                    }
                    
                    $log->logInfo("Team Option Inserted");
                    if(isset($package['desc'])){
                        $desc = $package['desc'];
                        foreach($desc as $dsc){
                            $dsc['image_loc']= upload_image_import($final_dir.'/desc/'.$dsc['image_loc'], 'team_desc', true);
                            unset($dsc['id']);
                            $dsc['team_id'] = $team_id;
                            DB::Insert('team_slideshow_images', $dsc); 
                        }
                    }
                    $log->logInfo("Team Slideshow Inserted");
                    
                    if(isset($package['gallery'])){
                        $gallery = $package['gallery'];
                        foreach($gallery as $gl){
                            $gl['image_loc']= upload_image_import($final_dir.'/gallery/'.$gl['image_loc'], 'team_desc', true);
                            unset($gl['id']);
                            $gl['team_id'] = $team_id;
                            DB::Insert('team_slideshow_images', $gl); 
                        }
                    }
                    $log->logInfo("Team Gallery Images");
                    unlink($final_dir);
                    Session::Set('notice', 'Deal Imported Successfully.'. '<a href="/manage/team/edit.php?id='.$team_id.'">Have a Look!!</a>');
                    redirect( WEB_ROOT . "/manage/team/index.php");
                }
        }
}


function upload_image_import($img, $type='team', $scale) {
            
            $year = date('Y'); 
            $day = date('md'); 
            $n = time().rand(1000,9999).'.jpg';
	    RecursiveMkdir( IMG_ROOT . '/' . "{$type}/{$year}/{$day}" );
            $image = "{$type}/{$year}/{$day}/{$n}";
            $path = IMG_ROOT . '/' . $image;
            copy($img, $path);
            
            if($type=='team' && $scale) {
			$npath = preg_replace('#(\d+)\.(\w+)$#', "\\1_index.\\2", $path); 
                        //Image::Convert($path, $npath, 348, 141, Image::MODE_CUT);
                        //Image::Convert($path, $npath, 348, 141, Image::MODE_SCALE);
			$simple = new SimpleImage(); 
			$simple->load($path);			
			$simple->resize(460,307); 
			$simple->save($npath, IMAGETYPE_PNG);
            }
            if($type=='team_desc' && $scale) {
			$npath = preg_replace('#(\d+)\.(\w+)$#', "\\1_index.\\2", $path); 
			//Image::Convert($path, $npath, 348, 141, Image::MODE_CUT);
            //Image::Convert($path, $npath, 638, 273, Image::MODE_SCALE);
			
			$simple = new SimpleImage(); 
			$simple->load($path);			
			$simple->resize(638,273); 
			$simple->save($npath, IMAGETYPE_PNG);
            }
            return $image;
}
