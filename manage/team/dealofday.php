<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('userdeals');

$now = time();
$nowf = date("d-m-Y");
//echo $nowf;

$condition = array(
	'system' => 'Y',
	'legitimate' => '1',
	'submit_time'  => $nowf,
	"end_time > {$now}",
);

/* filter start */
$team_type = strval($_GET['team_type']);
if ($team_type) { $condition['team_type'] = $team_type; }
/* filter end */

$count = Table::Count('userdeals', $condition);
list($pagesize, $offset, $pagestring) = pagestring($count, 20);

$teams = DB::LimitQuery('userdeals', array(
	'condition' => $condition,
	'order' => 'ORDER BY id DESC',
	'size' => $pagesize,
	'offset' => $offset,
));
$cities = Table::Fetch('category', Utility::GetColumn($teams, 'city_id'));
$groups = Table::Fetch('category', Utility::GetColumn($teams, 'group_id'));

if ( $team_type == "weekly" )
    $selector = 'weekly';
else
    $selector = 'dealofday';

include template('manage_team_dealofday');
