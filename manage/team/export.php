<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');
require_once(dirname(__FILE__) . '/current.php');

//Authentication
need_manager();
need_auth('team');


$id = abs(intval($_GET['id']));
// Fetching Team
$team = Table::Fetch('team', $id);
$multi = DB::LimitQuery('team_multi', array(
			'condition' => array( 'team_id' => $id),
                        'order' => 'ORDER BY id ASC',
                        ));
//$multi = Table::Fetch('team_multi', $id, 'team_id');
// Getting Description and Gallery Images
$export_array = array();

if(!empty($team)){
    
    $desc_images = DB::LimitQuery('team_slideshow_images', array(
			'condition' => array( 'team_id' => $id,
                                                'image_desc' => 'desc',    
                                ),
                         'order' => 'ORDER BY image_order'      
			));
   
    $gallery_images = DB::LimitQuery('team_slideshow_images', array(
			'condition' => array( 'team_id' => $id,
                                                'image_desc' => 'gallery',    
                                ),
                        'order' => 'ORDER BY image_order'
			));
    //print_r_r($gallery_images);

}

// Creating ZIP FILE
$file = tempnam("tmp", "zip");
$zip = new ZipArchive();
$zip->open($file, ZipArchive::OVERWRITE);

$images = array();

if($team['image'] != ''){
    $image_arr = explode('/', $team['image']);
    $zip->addFile(dirname(dirname(dirname(__FILE__))).'/static/'. $team['image'] , $image_arr[count($image_arr)-1]);
    $team['image'] = $image_arr[count($image_arr)-1];
}
$export_array['team'] = $team;

if( count($multi) > 0 ){
    $zip->addEmptyDir('multi/');
    foreach($multi as $in => $m){
//        echo $m['image'].'<br>';
        if($m['image']!= ''){
            $image_arr = explode('/', $m['image']);
            $zip->addFile(dirname(dirname(dirname(__FILE__))).'/static/'. $m['image'] , 'multi/'.$image_arr[count($image_arr)-1]);
            $multi[$in]['image'] = $image_arr[count($image_arr)-1];
        }
   }
   $export_array['multi'] = $multi;
}

if( count($desc_images) > 0 ){
    $zip->addEmptyDir('desc/');
    foreach($desc_images as $i => $dsc){
        $image_desc_arr = explode('/', $dsc['image_loc']);   
        $zip->addFile(dirname(dirname(dirname(__FILE__))).'/static/'. $dsc['image_loc'],'desc/'.$image_desc_arr[count($image_desc_arr)-1]);
        $desc_images[$i]['image_loc'] = $image_desc_arr[count($image_desc_arr)-1];
    }
     $export_array['desc'] = $desc_images;  
}
if( count($gallery_images) > 0 ){
    $zip->addEmptyDir('gallery/');
    foreach($gallery_images as $j => $gl_img){
        $gl_img_arr = explode('/', $gl_img['image_loc']);   
        $zip->addFile(dirname(dirname(dirname(__FILE__))).'/static/'. $gl_img['image_loc'], 'gallery/'.$gl_img_arr[count($gl_img_arr)-1]);
        $gallery_images[$j]['image_loc'] = $gl_img_arr[count($gl_img_arr)-1];
    }
     $export_array['gallery'] = $gallery_images;  
}
    
 
$json = json_encode($export_array);
// Stuff with content
$zip->addFromString('team.json', $json);
//

// Close and send to users
$zip->close();
header('Content-Type: application/zip');
header('Content-Length: ' . filesize($file));
header('Content-Disposition: attachment; filename="team-'.$id.'.zip"');
readfile($file);
unlink($file); 
//dirname(dirname(dirname(__FILE__))).'/generated_labels/