<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');
require_once(dirname(__FILE__) . '/current.php');

need_manager();
need_auth('team');

$id = abs(intval($_REQUEST['id']));
$team_id = $_REQUEST['team_id'];
$multi = Table::Fetch('team_multi', $id);
$team = Table::Fetch('team', $team_id);

$old_qty = 0;
if ( $multi )
{
    // edit
    $old_qty = $multi['max_number'];
    $old_img = $multi['image'];
}

if ( !$team )
{
    Session::Set('error', 'Invalid Deal.');
    redirect( WEB_ROOT . "/manage/team/index.php");
}



if ( is_post() ) {


	//print_r($_POST); 
	//print_r("<br>------------------<br>");
	
	if($id && $team_id)
	{
		#insert deal Price history
		$multiDeal = true;
		insertDealPriceHistory($multi,$_POST,$login_user_id,$multiDeal);
	}
	
	$multi = $_POST;
        $new_qty_offset = $multi['max_number'] - $old_qty;
        
        $condition = array ('team_id' => $team_id );
        $total_options_qty = Table::Count('team_multi', $condition, 'max_number');
        $total_options_qty = $total_options_qty + $new_qty_offset;
        
        if ( $total_options_qty > $team['max_number']){
             Session::Set('error', 'Maximum Quantity value has caused total Multi Options quantity exceed Deal Limit');
             include template('manage_team_multi_edit');
             exit;
        }
        if($team['delivery']!= 'coupon'){
            /*
             * Stock count and threshold should be greater than zero
             */
            if($multi['stock_count'] < 0){
                Session::Set('error', 'Stock count should be greater than zero');
                die (include template('manage_team_multi_edit'));	
            }
            if($multi['alert_threshold'] < 0){
                Session::Set('error', 'Alert Threshold should be greater than zero');
                die (include template('manage_team_multi_edit'));	
            }
        }
	$multi['image'] = upload_image('upload_image', $old_img,'team',true);

	//CASH BACK FEATURE ADD
	if(isset($multi['is_cash_back_allowed'])&&isset($multi['cb_amount'])&&isset($multi['cb_start_time'])&&isset($multi['cb_end_time'])){
		
		if(!empty($multi['cb_start_time'])){
			$cb_start_time		=	strtotime($multi['cb_start_time']);
		}
		
		if(!empty($multi['cb_start_time'])){
			$cb_end_time		=	strtotime($multi['cb_end_time']);
		}
			
		if(!empty($multi['cb_end_time']) && !empty($multi['cb_start_time'])){
			if($multi['cb_end_time'] <= $multi['cb_start_time'] ){
				
				$multi['cb_amount'] 		= "";
				$cb_start_time  		= "";
				$cb_end_time 			= "";
			}
		}
		
		$multi['is_cash_back_allowed']	=	1;
	}else{
		$multi['cb_amount'] 				= "";
		$cb_start_time 					= "";
		$cb_end_time 					= "";
		$multi['is_cash_back_allowed']	= 0;
	}
	
	//CHECK SKU UNIQUENESS
	if(isset($multi['sku'])&&!empty($multi['sku'])){

		$submittedtTeamId	=	$multi['team_id'];
		$submitteddSku		=	$multi['sku'];
		$dealId				=	$multi['id'];	
		
		$skuMultiSQL  	= "select id from team_multi where sku = '$submitteddSku' AND id != '$dealId'";
		$resultSkuMultiSQL  = DB::GetQueryResult($skuMultiSQL);
		
		//CHECK SKU FOR TEAM ALSO
		$skuSQL  	= "select id from team where sku = '$submitteddSku'";
		$resultSku  = DB::GetQueryResult($skuSQL);
		
		if(!empty($resultSku) || !empty($resultSkuMultiSQL)){
			if(isset($resultSku['id']) || isset($resultSkuMultiSQL['id'])){
				
			   Session::Set('error', 'SKU already exsist.');
			   redirect( WEB_ROOT . "/manage/team/edit_multi.php?id=".$team_id);
			}
		}
	}
	
	$multi['cb_amount'] 				= $multi['cb_amount'];
	$multi['cb_start_time'] 			= $cb_start_time;
	$multi['cb_end_time'] 			= $cb_end_time;
	$multi['is_cash_back_allowed'] 	= $multi['is_cash_back_allowed'];
	$multi['partner_revenue']		=	$multi['partner_revenue'];
	
	//insert the record
	$insert = array(
		'team_id', 'title', 'title_fr', 'market_price', 'team_price', 'summary', 'summary_fr', 'image',
            'per_number', 'pre_number', 'min_number', 'max_number', 'now_number', 'type', 'parent_option_id','stock_count', 'alert_threshold',
            'volume_length', 'volume_breadth', 'volume_height', 'volume_weight' ,'is_cash_back_allowed','cb_amount','cb_start_time','cb_end_time','partner_revenue','sku'
		);
				
	$insert = array_unique($insert);
	$table = new Table('team_multi', $multi);

  
  	if($multi[id]==$id){
            $field = strtoupper($team['conduser'])=='Y' ? null : 'quantity';
		$now_number = Table::Count('order', array(
					'team_id' => $multi['team_id'],
					'state' => 'pay',
                                        'option_id' => $multi['id']
					), $field);
               
                $sql  = "select sum(order_item.quantity) now_number from order_item left join `order` on `order`.id=order_id
                    where state = 'pay' and order_item.option_id = {$id}";
                $result = DB::GetQueryResult($sql);
                $now_number = $result['now_number'];
		$table->now_number = ($now_number + $multi['pre_number']);
                
                
                
          $table->SetPk('id', $id);
          unset($insert['stock_count']);
          $flag = $table->update($insert);
		  
		  //Adding team Price history	
		$team_price_history = array ();
		$team_price_history['team_id'] = $team_id;
		$team_price_history['user_id'] = $login_user_id;
		$team_price_history['option_id'] = $flag;
		$team_price_history['team_price'] = $multi['team_price'];
		$team_price_history['market_price'] = $multi['market_price'];
		$team_price_history['partner_revenue'] = $multi['partner_revenue'];
		$team_price_history['create_time'] = time();
		
		ZTeam::addDealPriceHistory($team_price_history);
          
	} else {  
            $table->now_number = $multi['pre_number'];
	   $flag = $table->insert($insert);
           // Adding Stock History
                $stock_history = array ();
                $stock_history['option_id'] = $flag;
                $stock_history['quantity'] = $_POST['stock_count'];
                $stock_history['user_id'] = $login_user_id;
                $stock_history['create_time'] = time();
                DB::Insert('stock_history', $stock_history);
           
        }

	if ($flag) {
		Session::Set('notice', 'Multi Buy Option Saved');
		//redirect( WEB_ROOT . "/manage/team/index.php");
	}
	else {
		Session::Set('error', 'Edit project failure');
		//redirect(null);
	}
	
	
	if($multi['add']) {
          	//$team['id'] = $multi['team_id'];
          	unset($multi);
		die (include template('manage_team_multi_edit'));
	}
	if($multi['sub']) {
          	//$team['id'] = $multi['team_id'];
			$parent_option_id=$multi['id'];
          	unset($multi);
			$multi['type']='flat';
	          $multi['parent_option_id'] = $parent_option_id;
		die (include template('manage_team_multi_edit'));
	}
	else {
		
		### id empty mean deal is added then get last insert id.
		/* if(empty($id))
		{
			$id = $flag;
		} */
		#$urlPath = "/manage/team/multi.php?id=".$id."&team_id=".$team_id;
		$urlPath  = "/manage/team/edit_multi.php?id=".$team_id;
		redirect( WEB_ROOT . $urlPath);
		
	} 	

}
include template('manage_team_multi_edit');
