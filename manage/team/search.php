<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('team');

$term = $_REQUEST['rechercher'];

$supplier_id = $_REQUEST['supplier_id'];

if(empty($supplier_id))
    $supplier_id = 0;

$system = strval($_GET['system']);
if(empty($system)){
     $system = 0;
}

$teams = ZTeam::Search($term, false, false, $supplier_id, $system);
$cities = Table::Fetch('category', Utility::GetColumn($teams, 'city_id'));
$groups = Table::Fetch('category', Utility::GetColumn($teams, 'group_id'));

$suppliers = DB::LimitQuery('supplier', array(
			'order' => 'ORDER BY title ASC',
			));
$suppliers = Utility::OptionArray($suppliers, 'id', 'title');
$selector = 'search';
$hidden_team_options = array('Y'=> 'No','N'=>'Yes');
include template('manage_team_index');

?>
