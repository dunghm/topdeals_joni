<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');
require_once(dirname(__FILE__) . '/current.php');

//Authentication
need_manager();
need_auth('team');

$id = abs(intval($_GET['id']));
$brand = Table::Fetch('brands', $id);

$json = json_encode($brand);

header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename=brand-'.$id.'.json');
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . sizeof($json));
echo $json;