<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');
require_once(dirname(__FILE__) . '/current.php');

$team_id = intval($_GET['team_id']);
$action = $_GET['action'];

if($action == 'delete'){
    $id = intval($_GET['id']);
    DB::Delete('team_slideshow_images', array('id'=>$id));
    exit;
}

if($action == 'order_change'){
    $order = $_GET['order'];
    $team_id = $_GET['team_id'];
    $order_arr = explode(',', $order);
    
    foreach($order_arr as $id => $val){
        $val_ar = explode('gl_', $val);
        echo $val_ar[1]. '-'. $id;
        DB::Update('team_slideshow_images', $val_ar[1], array('image_order'=> $id));
    }
    exit;
}


if($action == 'desc'){
    $name = 'desc_image';
    $sql = "SELECT max(image_order) AS order FROM team_slideshow_images WHERE image_desc='desc_image' AND team_id = $team_id";
    $max_order = DB::GetQueryResult($sql, true);
    $max_order_count = $max_order['order'];
    $image_order = $max_order_count+1;
}else{
    $name = 'slideshow_image';
     $sql = "SELECT max(image_order) AS order FROM team_slideshow_images WHERE image_desc='slideshow_image' AND team_id = $team_id";
    $max_order = DB::GetQueryResult($sql, true);
    $max_order_count = $max_order['order'];
    $image_order = $max_order_count+1;
}
$image = upload_image($name, null,'team_desc',true);
$action = $_GET['action'];
$team_slideshow_images = array(
    'team_id' => $team_id,
    'image_loc' => $image,
    'image_desc' => $action,
    'image_order' => $image_order
);

$id = DB::Insert('team_slideshow_images', $team_slideshow_images);

$json_arr = array(
    'image_loc' => $image,
    'id' => $id
);
echo json_encode($json_arr);