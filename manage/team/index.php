<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('team');

$now = time();
ini_set("auto_detect_line_endings", "1");
function convert( $str ) {
    return iconv( "Windows-1252", "UTF-8", $str );
}
if(isset($_GET['eta']) && $_GET['eta']==1 ){
	Session::Set('notice', 'ETA updated successfully.');
}

if(is_post()){

   // print_r_r($_FILES);
     if (isset($_FILES['import_csv_file']) && $_FILES["import_csv_file"]["error"] == 0) {
	 
            $file = $_FILES['import_csv_file']['tmp_name'];
            $handle = fopen($file, "r");
            $pos_array = array();
            $i = 0;
            $team_columns = Export::GetColNames('team');
            
             while ($data = fgetcsv($handle, 10000)) {
			 
                 if ($i == 0) {
                    
                    foreach ($data as $d) {
                        if(!in_array($d, $team_columns)){
                             Session::Set('error', 'The column '.$d.' does not exists');
                redirect(WEB_ROOT . "/manage/team/index.php");
                exit;
                        }
                        else{
                        $pos_array[]= $d;    
                        }
                        
                        
                    }
                }else{
                    $j = 0;
                    $ins_upd_arr = array();
                    foreach ($data as $d) {
                        $ins_upd_arr[$pos_array[$j]] = $d;
                        $j++;
                    }
                    if($ins_upd_arr['id']){
						//$ins_upd_arr = array_map( "convert", $ins_upd_arr );
						
                        DB::Update('team', $ins_upd_arr['id'], $ins_upd_arr);
                    }
                    else{
						$ins_upd_arr = array_map( "convert", $ins_upd_arr );
                        DB::Insert('team', $ins_upd_arr);
                    }
                }
                $i++;
             }
             Session::Set('notice', 'CSV uploaded successfully');
                redirect(WEB_ROOT . "/manage/team/index.php");
     }
   
}
if(isset($_GET['export_csv']) && intval($_GET['export_csv']) ==1)
{
   //$col_names = Export::GetColNames('team');
   $col_names = array('id','title','seo_description', 'seo_keyword','seo_title'); 
   $condition = array(
	'system' => 'Y',
	"end_time > {$now}",
    );
        
        
    $teams = DB::LimitQuery('team', array(
	'condition' => $condition,
        'order' => 'ORDER BY id DESC',
    ));
   
    $filename = 'deals_'.date('Ymd').'.csv';
    header('Content-type: application/csv');
    header('Content-Disposition: attachment; filename='.$filename);
    $fp= fopen('php://output', 'w');
    fputcsv($fp, $col_names);
    
    foreach($teams as $t){
            $row_arr = array();
            foreach ($col_names as $cn) {
                $row_arr[] = $t["$cn"];
            }
            fputcsv($fp, $row_arr);
    }

    fclose($fp);
    exit;
   
}


$condition = array(
	'system' => 'Y',
	"end_time > {$now}",
);

/* filter start */
$team_type = strval($_GET['team_type']);
if ($team_type) { $condition['team_type'] = $team_type; }

$supplier_id = strval($_GET['supplier_id']);
if($supplier_id){
    $condition['supplier_id'] = $supplier_id;
}

$system = strval($_GET['system']);
if($system){
    $condition['system'] = $system;
}
/* filter end */

$count = Table::Count('team', $condition);
list($pagesize, $offset, $pagestring) = pagestring($count, 10000);

$teams = DB::LimitQuery('team', array(
	'condition' => $condition,
	'order' => 'ORDER BY id DESC',
	'size' => $pagesize,
	'offset' => $offset,
));
$cities = Table::Fetch('category', Utility::GetColumn($teams, 'city_id'));
$groups = Table::Fetch('category', Utility::GetColumn($teams, 'group_id'));

if ( $team_type == "weekly" )
    $selector = 'weekly';
else
    $selector = 'index';

$suppliers = DB::LimitQuery('supplier', array(
			'order' => 'ORDER BY title ASC',
			));
$suppliers = Utility::OptionArray($suppliers, 'id', 'title');
$hidden_team_options = array('Y'=> 'No','N'=>'Yes');
include template('manage_team_index');
