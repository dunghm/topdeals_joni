<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');
require_once(dirname(__FILE__) . '/current.php');

//Authentication
need_manager();
need_auth('team');

if(is_post()){
     if($_FILES["import_file"]["name"]) {
          $source = $_FILES["import_file"]["tmp_name"];
          $content = file_get_contents($source);
          $brand = json_decode($content, true);
          unset($brand['id']);
          $id = DB::Insert('brands', $brand);
          Session::Set('notice', 'Brand imported Successfully.'.'<a href="/manage/team/edit_brands.php?id='.$id.'">Have a Look!!</a>');
         redirect( WEB_ROOT . "/manage/team/brands.php");
     }
}