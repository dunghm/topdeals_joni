<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');
require_once(dirname(__FILE__) . '/current.php');
$team_id = $_POST['team_id'];

$team_record = Table::Fetch('team', $team_id);

if ($team_record) {
    if (isset($_FILES['import_file']) && $_FILES["import_file"]["error"] == 0) {
        $file = $_FILES['import_file']['tmp_name'];
        $handle = fopen($file, "r");
        $insert = array(
                    'team_id', 
                    'title', 
                    'title_fr', 
                    'summary', 
                    'summary_fr', 
                    'market_price',
                    'team_price', 
                    'image', 
                    'per_number',
                    'min_number', 
                    'max_number', 
                    'now_number', 
                    'pre_number', 
                    'parent_option_id', 
                    'type',
        );
        $pos_array = array();
        $i = 0;

        while ($data = fgetcsv($handle, 1000, ",", "'")) {

            if ($data[0]) {
                if ($i == 0) {
                    $j = 0;
                    foreach ($data as $d) {

                        if (in_array($d, $insert)) {
                            $pos_array[$d] = $j;
                        }
                        $j++;
                    }
                } else {

                    $multi = array();
                    
                    $multi['team_id'] = $team_id;
                    $multi['title'] = isset($data[$pos_array['title']]) ? $data[$pos_array['title']] : '';
                    $multi['title_fr'] = isset($data[$pos_array['title_fr']]) ? $data[$pos_array['title_fr']] : '';
                    
                    $multi['summary'] = isset($data[$pos_array['summary']]) ? $data[$pos_array['summary']] : '';
                    $multi['summary_fr'] = isset($data[$pos_array['summary_fr']]) ? $data[$pos_array['summary_fr']] : '';
                    $multi['team_price'] = isset($data[$pos_array['team_price']]) ? $data[$pos_array['team_price']] : '';
                    
                    $multi['market_price'] = isset($data[$pos_array['market_price']]) ? $data[$pos_array['market_price']] : '';
                    $multi['image'] = isset($data[$pos_array['image']]) ? $data[$pos_array['image']] : '';
                    $multi['per_number'] = isset($data[$pos_array['per_number']]) ? $data[$pos_array['per_number']] : '';
                    
                    $multi['min_number'] = isset($data[$pos_array['min_number']]) ? $data[$pos_array['min_number']] : '';
                    $multi['max_number'] = isset($data[$pos_array['max_number']]) ? $data[$pos_array['max_number']] : '';
                    $multi['now_number'] = isset($data[$pos_array['now_number']]) ? $data[$pos_array['now_number']] : '';
                    
                    $multi['pre_number'] = isset($data[$pos_array['pre_number']]) ? $data[$pos_array['pre_number']] : '';
                    $multi['parent_option_id'] = isset($data[$pos_array['parent_option_id']]) ? $data[$pos_array['parent_option_id']] : '';
                    $multi['type'] = isset($data[$pos_array['type']]) ? $data[$pos_array['type']] : '';


                    $insert = array_unique($insert);
                    $table = new Table('team_multi', $multi);
                    $flag = $table->insert($insert);
                }
                $i++;
            }
        }

        Session::Set('notice', 'CSV uploaded successfully');
        redirect(WEB_ROOT . "/manage/team/edit_multi.php?id=" . $team_id);
    } else {

        Session::Set('error', 'Please upload CSV file');
        redirect(WEB_ROOT . "/manage/team/edit_multi.php?id=" . $team_id);
    }
}

