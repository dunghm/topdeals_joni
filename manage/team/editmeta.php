<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');
require_once(dirname(__FILE__) . '/current.php');

need_manager();
need_auth('team');

$id = abs(intval($_GET['id']));
$team = $eteam = Table::Fetch('team', $id);

$team_meta = ZTeam::GetMeta($id);

        
        
if ( is_get() && empty($team) ) {
	redirect( WEB_ROOT . '/manage/team/edit.php' );
}
else if ( is_post() ) {
        Table::Delete('team_meta', $_POST['id'], 'team_id');
        
	$team_meta = array ();
        $team_meta['team_id'] = $_POST['id'];
        $team_meta['lang'] = 'fr';
        
        $team_meta['key'] = 'HIGHLIGHTS';
        $team_meta['value'] = $_POST['highlights_fr'];
        
        DB::Insert('team_meta', $team_meta);
        
        $team_meta['key'] = 'TERMS';
        $team_meta['value'] = $_POST['terms_fr'];
        
        DB::Insert('team_meta', $team_meta);
        
        $team_meta['key'] = 'DETAILS';
        $team_meta['value'] = $_POST['details_fr'];
        
        DB::Insert('team_meta', $team_meta);
        
        $team_meta['lang'] = 'en';
          
        $team_meta['key'] = 'HIGHLIGHTS';
        $team_meta['value'] = $_POST['highlights_en'];
        
        DB::Insert('team_meta', $team_meta);
      
        $team_meta['key'] = 'TERMS';
        $team_meta['value'] = $_POST['terms_en'];
        
        DB::Insert('team_meta', $team_meta);
        
        $team_meta['key'] = 'DETAILS';
        $team_meta['value'] = $_POST['details_en'];
        
        DB::Insert('team_meta', $team_meta);
        
	
	
        Session::Set('notice', 'Deal meta information successfully updated');
	redirect( WEB_ROOT . "/manage/team/editmeta.php?id={$id}");
	
}

$selector = 'meta';
include template('manage_team_editmeta');
