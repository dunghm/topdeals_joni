<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');
require_once(dirname(__FILE__) . '/current.php');

need_manager();
need_auth('team');

$id = abs(intval($_GET['id']));
$team = $eteam = Table::Fetch('team', $id);


if ( is_get() && empty($team) ) {
	$team = array();
	$team['id'] = 0;
	$team['user_id'] = $login_user_id;
	$team['begin_time'] = strtotime('+1 days');
	$team['end_time'] = strtotime('+2 days'); 
	$team['expire_time'] = strtotime('+3 months +1 days');
	
	$team['expire_time_partner'] =   strtotime('+1 months', $team['expire_time']);
	$team['min_number'] = 10;
	$team['per_number'] = 0;
	$team['market_price'] = 1;
	$team['max_number'] = 50;
	$team['team_price'] = 1;
	$team['delivery'] = 'coupon';
	$team['address'] = $profile['address'];
	$team['mobile'] = $profile['mobile'];
	$team['fare'] = 9;
	$team['farefree'] = 0;
	$team['bonus'] = abs(intval($INI['system']['invitecredit']));
	$team['conduser'] = $INI['system']['conduser'] ? 'Y' : 'N';
	$team['buyonce'] = 'Y';
	$team['delivery_eta'] = 10;
        
}
else if ( is_post() ) {
	$team = $_POST;
    
	//on edit 
	if($id)
	{
		#insert deal Price history
		$multiDeal = false;
		insertDealPriceHistory($eteam,$_POST,$login_user_id,$multiDeal);  
	}
	$insert = array(
		'title', 'title_fr', 'market_price', 'team_price', 'end_time', 
		'begin_time', 'expire_time', 'expire_time_partner', 'min_number', 'max_number', 
		'summary', 'summary_fr', 'notice', 'per_number', 'product', 
		'image', 'image1', 'image2', 'flv', 'now_number', 'systemreview',
		'detail', 'detail_fr', 'userreview', 'card', 
		'conduser', 'buyonce', 'bonus', 'sort_order',
		'delivery', 'mobile', 'address', 'fare', 
		'express', 'credit', 'farefree', 'pre_number',
		'user_id', 'city_id', 'group_id', 'partner_id','supplier_id',
		'team_type', 'sort_order', 'farefree', 'state',
		'condbuy','type', 'delivery_eta', 'stock_count', 'alert_threshold', 'system',
                'volume_length', 'volume_breadth', 'volume_height', 'volume_weight', 'featured', 'brand_id', 'seo_description', 'is_cash_back_allowed','cb_amount','cb_start_time','cb_end_time',
                'partner_revenue','ps_start_time','sku'
		);
        
        $team['systemreview'] = implode('<br />',$_POST['technical_details']);
        if($team['systemreview'] == '<br />'){
            $team['systemreview'] = '';
        }
        $vous_aimerez = array();
        foreach($_POST['vous_aimerez'] as $va){
            if( trim($va) != ''){
                $vous_aimerez[] = $va;
            }
        }
                
        $team['notice'] = implode('<br />',$vous_aimerez);
        if($team['notice'] == '<br />'){
            $team['notice'] = '';
        }
        //echo $team['notice'];
        //exit;
        $team['user_id'] = $login_user_id;
	$team['state'] = 'none';
	$team['begin_time'] = strtotime($team['begin_time']);
	$team['city_id'] = abs(intval($team['city_id']));
	$team['partner_id'] = abs(intval($team['partner_id']));
	$team['sort_order'] = abs(intval($team['sort_order']));
	$team['fare'] = abs(intval($team['fare']));
	$team['farefree'] = abs(intval($team['farefree']));
	$team['pre_number'] = abs(intval($team['pre_number']));
	$team['end_time'] = strtotime($team['end_time']);
	$team['expire_time'] = strtotime($team['expire_time']);
        $team['expire_time_partner'] = strtotime($team['expire_time_partner']);
	$team['image'] = upload_image('upload_image',$eteam['image'],'team',true);
	$team['image1'] = upload_image('upload_image1',$eteam['image1'],'team');
	$team['image2'] = upload_image('upload_image2',$eteam['image2'],'team');
        $team['last_updated'] = time();
        if($team['delivery']!= 'coupon'){
            /*
         * Stock count and threshold should be greater than zero
         */
            if($team['stock_count'] < 0){
                Session::Set('error', 'Stock count should be greater than zero');
                die (include template('manage_team_edit'));	
            }
            if($team['alert_threshold'] < 0){
                Session::Set('error', 'Alert Threshold should be greater than zero');
                die (include template('manage_team_edit'));	
            }
        }
	//team_type == goods
	if($team['team_type'] == 'goods'){ 
		$team['min_number'] = 1; 
		$team['conduser'] = 'N';
	}
	else
	{
		#conduser get from system.php file
		$team['conduser'] =$INI['system']['conduser'] ? 'Y' : 'N';
	}
	
	//CASH BACK FEATURE ADD
	if(isset($team['is_cash_back_allowed'])&&isset($team['cb_amount'])&&isset($team['cb_start_time'])&&isset($team['cb_end_time'])){
		
		if(!empty($team['cb_start_time'])){
			$cb_start_time		=	strtotime($team['cb_start_time']);
		}
		
		if(!empty($team['cb_start_time'])){
			$cb_end_time		=	strtotime($team['cb_end_time']);
		}
			
		if(!empty($team['cb_end_time']) && !empty($team['cb_start_time'])){
			if($team['cb_end_time'] <= $team['cb_start_time'] ){
				
				$team['cb_amount'] 		= "";
				$cb_start_time  		= "";
				$cb_end_time 			= "";
			}
		}
		
		$team['is_cash_back_allowed']	=	1;
	}else{
		$team['cb_amount'] 				= "";
		$cb_start_time 					= "";
		$cb_end_time 					= "";
		$team['is_cash_back_allowed']	= 0;
	}
	
	//CHECK SKU UNIQUENESS
	if(isset($team['sku'])&&!empty($team['sku'])){

		$submitteddSku	=	$team['sku'];
		$dealId			=	$team['id'];	
		
		$skuSQL  	= "select id from team where sku = '$submitteddSku' AND id != $dealId";
		$resultSku  = DB::GetQueryResult($skuSQL);
		
		//MULTI TEAM SKU CHECK
		$skuMultiSQL  	= "select id from team_multi where sku = '$submitteddSku'";
		$resultSkuMultiSQL  = DB::GetQueryResult($skuMultiSQL);
		
		if(!empty($resultSku) || !empty($resultSkuMultiSQL)){
			if(isset($resultSku['id']) || isset($resultSkuMultiSQL['id'])){
				
			   Session::Set('error', 'SKU already exsist.');
			   redirect( WEB_ROOT . "/manage/team/index.php");
			}
		}
	}
	
	
	$team['cb_amount'] 				= $team['cb_amount'];
	$team['cb_start_time'] 			= $cb_start_time;
	$team['cb_end_time'] 			= $cb_end_time;
	$team['is_cash_back_allowed'] 	= $team['is_cash_back_allowed'];
	$team['ps_start_time'] 			= time();
	
	# Fixed title_fr issue
	$team['title_fr'] = $team['title'];
	
	if ( !$id ) {
		$team['now_number'] = $team['pre_number'];
	} else {
		$field = strtoupper($table->conduser)=='Y' ? null : 'quantity';
		/*$now_number = Table::Count('order', array(
		//			'team_id' => $id,
		//			'state' => 'pay',
		//			), $field);*/
                $now_number = Table::Count('order',
					"`team_id` = {$id} AND `state` != 'unpay' AND `state` != 'temporary' AND (`service` = 'cashondelivery' OR `state` = 'pay')"
					, $field);
                                        
                $sql  = "select sum(order_item.quantity) now_number from order_item left join `order` on `order`.id=order_id
                    where state = 'pay' and order_item.team_id = {$id}";
                    $result = DB::GetQueryResult($sql);
                    $now_number = $result['now_number'];
		$team['now_number'] = ($now_number + $team['pre_number']);

		/* Increased the total number of state is not sold out */
		if ( $team['max_number'] > $team['now_number'] ) {
			$team['close_time'] = 0;
			$insert[] = 'close_time';
		}
	}
        
	$insert = array_unique($insert);
	
        $table = new Table('team', $team);
	$table->SetStrip('summary', 'detail', 'notice');
	
	if ( $team['id'] && $team['id'] == $id ) {
		$table->SetPk('id', $id);
                unset($insert['stock_count']);
                
		$table->update($insert);
                
		Session::Set('notice', 'Information Successfully Modified!');
		$urlPath = "/manage/team/edit.php?id=".$id;
		redirect( WEB_ROOT . $urlPath);
	} 
	else if ( $team['id'] ) {
		Session::Set('error', 'Illegal edition');
		redirect( WEB_ROOT . "/manage/team/index.php");
	}

	if ( $flag=$table->insert($insert) ) {
		Session::Set('notice', 'New Deal Created Successfully');
                
                
                // Adding Stock History
                $stock_history = array ();
                $stock_history['team_id'] = $flag;
                $stock_history['quantity'] = $_POST['stock_count'];
                $stock_history['user_id'] = $login_user_id;
                $stock_history['create_time'] = time();
                DB::Insert('stock_history', $stock_history);
				
				//Adding team Price history	
				$optionId = null;
				$team_price_history = array ();
				$team_price_history['team_id'] = $flag;
				$team_price_history['user_id'] = $login_user_id;
				$team_price_history['option_id'] = $optionId;
				$team_price_history['team_price'] = $team['team_price'];
				$team_price_history['market_price'] = $team['market_price'];
				$team_price_history['partner_revenue'] = $team['partner_revenue'];
				$team_price_history['create_time'] = time();
				
				ZTeam::addDealPriceHistory($team_price_history);
		
        if($team['type']=='multi')
		{
			$team['id'] = $flag;
			die (include template('manage_team_multi'));	
		}
		
		redirect( WEB_ROOT . "/manage/team/index.php");
	}
	else {
		Session::Set('error', 'Edit project failure');
		redirect(null);
	}
}

if(!empty($team)){
    
    $desc_images = DB::LimitQuery('team_slideshow_images', array(
			'condition' => array( 'team_id' => $id,
                                                'image_desc' => 'desc',    
                                ),
                         'order' => 'ORDER BY image_order'      
			));
   
    $gallery_images = DB::LimitQuery('team_slideshow_images', array(
			'condition' => array( 'team_id' => $id,
                                                'image_desc' => 'gallery',    
                                ),
                        'order' => 'ORDER BY image_order'
			));
    //print_r_r($gallery_images);

}
//exit;
$groups = DB::LimitQuery('category', array(
			'condition' => array( 'zone' => 'group', ),
                        
			));


$groups = Utility::OptionArray($groups, 'id', 'name');

$partners = DB::LimitQuery('partner', array(
			'order' => 'ORDER BY title ASC',
			));
$partners = Utility::OptionArray($partners, 'id', 'title');
$suppliers = DB::LimitQuery('supplier', array(
			'order' => 'ORDER BY title ASC',
			));
$suppliers = Utility::OptionArray($suppliers, 'id', 'title');
$brands = DB::LimitQuery('brands');
$brands = Utility::OptionArray($brands, 'id', 'brand_title');
$hidden_team_options = array('Y'=> 'No','N'=>'Yes');
$featured_team_options = array('1'=>'Yes', '0'=> 'No');
$selector = $team['id'] ? 'edit' : 'create';
include template('manage_team_edit');