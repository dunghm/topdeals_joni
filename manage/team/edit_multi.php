<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('team');

if(isset($_GET['action'])){
    $action = $_GET['action'];
    if($action == "order_change"){
       // $order = $_GET['order'];
        $team_id = $_GET['team_id'];
        $parent_ids = $_GET['parent_ids'];
        $parent_id_arr = explode(',', $parent_ids);
        //$order_arr = explode(',', $order);
        $childrens = $_GET['childrens'];
        $childrens_arr = explode(',', $childrens);
        
        $tree = array();
        foreach($childrens_arr as $ch){
             $arr = explode('-', $ch);
             $tree[$arr[0]][] = $arr[1];
        }
        
        
        foreach($parent_id_arr as $id => $val){
            echo $val;
            DB::Update('team_multi', $val, array('multi_order'=> $id));
            if(!empty($tree[$val])){
                
                foreach( $tree[$val] as $j=>$t){
                    
                    DB::Update('team_multi', $t, array('parent_option_id'=>$val,'multi_order'=> $j));
                }
            }
        }
        
        
        
    }
    exit;
}

$id = $_REQUEST['id'];

$team = Table::Fetch('team', $id);

$condition = array(
	'team_id' => $id,
);

$multi = DB::LimitQuery('team_multi', array(
	'condition' => $condition,
	'order' => 'ORDER BY multi_order ASC',
));

$multi = Utility::AssColumn($multi, 'id');
//print_r_r($multi);

$tree_first =  generatePageTree($multi);
//print_r_r($tree);
$order_tree = array_keys($multi);
$tree = array();
foreach($order_tree as $o_tree){
   if(isset($tree_first[$o_tree])){
    $tree[$o_tree] = $tree_first[$o_tree]; 
   }
}

function generatePageTree($datas){
    $multi_items = array();
    if($datas){
        foreach($datas as $d){
            if($d['parent_option_id'] == 0){
                if(!isset($multi_items[$d['id']])){
                $multi_items[$d['id']] = array();  
                }
                //echo $d['title'];
            }
            else{
               
                $multi_items[$d['parent_option_id']][] = $d['id'];  
            }
        }
    }
    return $multi_items;
}


//$team[id] = $id;
include template('manage_team_multi');
