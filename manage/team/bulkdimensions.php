<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');
require_once(dirname(__FILE__) . '/current.php');

need_manager();
need_auth('team');
if(is_post()){
     if (isset($_FILES['dimension_file']) && $_FILES["dimension_file"]["error"] == 0) {
         $file = $_FILES['dimension_file']['tmp_name'];
         ini_set('auto_detect_line_endings', true);
        $handle = fopen($file, "r");
        $insert = array(
                    'team_id', 
                    'weight', 
                    'height', 
                    'width', 
                    'length',
                    'option_id'
        );
        $i=0;
        while ($data = fgetcsv($handle, 1000, ";", "'")) {
            if ($data[0]) {
                
                if ($i == 0) {
                    $j = 0;
                    foreach ($data as $d) {

                        if (in_array($d, $insert)) {
                            $pos_array[$d] = $j;
                        }
                        $j++;
                    }
                }else{
                    $dimensions = array();
                    
                    $team_id = isset($data[$pos_array['team_id']]) ? $data[$pos_array['team_id']] : '';
                    $option_id = isset($data[$pos_array['option_id']]) ? $data[$pos_array['option_id']] : NULL;
                    $dimensions['volume_weight'] = isset($data[$pos_array['weight']]) ? $data[$pos_array['weight']] : '';
                    $dimensions['volume_height'] = isset($data[$pos_array['height']]) ? $data[$pos_array['height']] : '';
                    
                    $dimensions['volume_breadth'] = isset($data[$pos_array['width']]) ? $data[$pos_array['width']] : '';
                    $dimensions['volume_length'] = isset($data[$pos_array['length']]) ? $data[$pos_array['length']] : '';
                    
                    if(is_null($option_id)){
                     Table::UpdateCache('team', $team_id, $dimensions);   
                    }
                    else{
                        Table::UpdateCache('team_multi', $option_id, $dimensions);
                    }
                   
                }
                $i++;
            }
        }
     }
      Session::Set('notice', 'Bulk Dimensions updated successfully');
      redirect(WEB_ROOT . "/manage/team/index.php");
}

