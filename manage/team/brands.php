<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();

if(isset($_GET['action'])){
    $action = $_GET['action'];
    if($action == 'brandremove'){
        $id = intval($_GET['id']);
         Table::Delete('brands', $id); 
         Session::Set('notice', 'Brand Deleted Successfully');
        redirect( WEB_ROOT . "/manage/team/brands.php");
    }
}
$brands = DB::LimitQuery('brands');
$selector = 'brands';
include template('manage_team_brands');