<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();

if(isset($_GET['id'])){ 
    $id = intval($_GET['id']);
    $brand = Table::Fetch('brands', $id);
}

if(is_post()){
      $brand_title = $_POST['brand_title'];
        $brand_text  = $_POST['brand_text'];
    if(!isset($_POST['id'])){
      
        $brand_array = array(
            'brand_title' => $brand_title,
            'brand_text' => $brand_text
        );
        
        DB::Insert('brands', $brand_array);
        Session::Set('notice', 'Brand Created Successfully');
        redirect( WEB_ROOT . "/manage/team/brands.php");
    }
    else{
        $id = intval($_POST['id']);
        $brand_array = array(
            'brand_title' => $brand_title,
            'brand_text' => $brand_text
        );
        
        DB::Update('brands', $id, $brand_array);
        Session::Set('notice', 'Brand Updated  Successfully');
        redirect( WEB_ROOT . "/manage/team/brands.php");
    }
}

include template('manage_team_brand_edit');