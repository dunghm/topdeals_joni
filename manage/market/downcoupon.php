<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('market');

if ( $_POST ) {
	$team_id = abs(intval($_POST['team_id']));
	$consume = $_POST['consume'];
	if (!$team_id || !$consume) die('-ERR ERR_NO_DATA');
	
	$condition = array(
		'team_id' => $team_id,
		'consume' => $consume,
	);

	$coupons = DB::LimitQuery('coupon', array(
		'condition' => $condition,
                'order' => 'order by consume_time ASC'
	));
	if (!$coupons) die('-ERR ERR_NO_DATA');

	$users = Table::Fetch('user',Utility::GetColumn($coupons,'user_id'));
	$order_items = Table::Fetch('order_item',Utility::GetColumn($coupons,'order_id'));
        $team_multi = Table::Fetch('team_multi', Utility::GetColumn($order_items,'option_id'));

	$team = Table::Fetch('team', $team_id);
	$name = 'coupon_'.date('Ymd');
	$kn = array(
		'id' => 'No.',
		'username' => 'username',
		'secret' => 'password',
		'condbuy' => 'buying condition',
		'date' => 'due time',
		'remark' => 'notes',
		'consume' => 'state',
                'consumetime' => 'Validation Date',
                'deal_price' => 'Deal Price',
                'option_title' => 'Option Title',
		);

	$consume = array(
		'Y' => 'used',
		'N' => 'usable',
	);
	$ecoupons = array();
        $option_colors = array();
        $colors = array("AliceBlue",
"AntiqueWhite",
"Aqua",
"Aquamarine",
"Azure",
"Beige",
"CadetBlue",
"Chartreuse",
"Chocolate",
"Coral",
"CornflowerBlue",
"Cornsilk",
"Crimson",
"Cyan",
"DarkGoldenRod",
"DarkGray",
"DarkKhaki",
"Darkorange",
"DarkOrchid",
"DarkSalmon",
"DarkSeaGreen",
"DarkTurquoise",
"DarkViolet",
"DeepPink",
"DeepSkyBlue",
"FloralWhite",
"Fuchsia",
"Gainsboro",
"GhostWhite",
"Gold",
"GoldenRod",
"Gray",
"GreenYellow",
"HoneyDew",
"HotPink",
"IndianRed",
"Ivory",
"Khaki",
"Lavender",
"LavenderBlush",
"LawnGreen",
"LemonChiffon",
"LightBlue",
"LightCoral",
"LightCyan",
"LightGoldenRodYellow",
"LightGray",
"LightGreen",
"LightPink",
"LightSalmon",
"LightSeaGreen",
"LightSkyBlue",
"LightSlateGray",
);
        
	foreach( $coupons AS $one ) {
		$one['id'] = "#{$one['id']}";
		$one['username'] = $users[$one['user_id']]['username'];
		$one['consume'] = $consume[$one['consume']];
                $one['consumetime'] = ($one['consume_time'] > 0 ? date('Y-m-d', $one['consume_time']) : '');
		$one['condbuy'] = "";//$order_items[$one['order_id']]['condbuy'];
		$one['remark'] = $order_items[$one['order_id']]['mode'];
		$one['date'] = date('Y-m-d', $one['expire_time']);
                $one['deal_price'] = $order_items[$one['order_id']]['price'];
                
                if ($order_items[$one['order_id']]['option_id']>0)
                {
                    $option_id = $order_items[$one['order_id']]['option_id'];
                    $one['option_title'] = "[{$option_id}] ".$team_multi[$option_id]['title_fr'];
                    if ( !array_key_exists($option_id,$option_colors) )
                        $option_colors[$option_id] = $colors[sizeof($option_colors)];
                    
                    $one['##bgcolor'] = $option_colors[$option_id];
                }
                else
                    $one['option_title'] = "";
                
		$ecoupons[] = $one;
	}
        $metadata = array("Deal ID"=>$team_id, "Download Date"=> date('Y-m-d'));
	down_xls($ecoupons, $kn, $name, $metadata);
}

include template('manage_market_downcoupon');
