<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('admin');

require_once('InviteContestAdmin.php');

$command = $_GET['cmd'];

    if ( $command == "create" )
        return InviteContestAdmin::Create();
    else if ( $command == "edit" )
        return InviteContestAdmin::Edit();
    else if ( $command == "delete" )
        return InviteContestAdmin::Delete();
    else if ( $command == "disable" )
        return InviteContestAdmin::Disable();
    else if ( $command == "subscription" )
        return InviteContestAdmin::Subscription();
    else if ( $command == "referals" )
        return InviteContestAdmin::Referal();

    InviteContestAdmin::Main();
?>
