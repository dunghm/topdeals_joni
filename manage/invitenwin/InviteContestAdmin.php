<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class InviteContestAdmin
{
    static public function Main()
    {
        $count = Table::Count('invitecontest');
        list($pagesize, $offset, $pagestring) = pagestring($count, 20);

        $invitecontests = DB::LimitQuery('invitecontest', array(
               // 'condition' => $condition,
                'order' => 'ORDER BY id DESC',
                'size' => $pagesize,
                'offset' => $offset,
        ));
        include template('invitecontest/manage_invitecontest_index');
    }
    
    static public function Create()
    {
        $pagetitle = "Create Invite n Win Contest";
        if (is_post())
        {
            $table = new Table('invitecontest', $_POST);
            $table->SetStrip('description', 'terms_conditions');
            
            $table->status = 1;
            $table->create_time = time();
            $table->start_time = strtotime($table->start_time);
            $table->end_time = strtotime($table->end_time);
            $table->SetPk('id', null);
            $insert = array( 'title', 'description',  'terms_conditions', 'create_time', 'start_time', 'end_time', 'status');
            if ( $table->insert($insert) )
            {
                Session::set('notice', 'Contest created successfuly');
                self::Main();
            }
            else
            {
                Session::set('error', 'Failed to Create Contest');
            }
            
        }
        else
        {
           $invitecontest['start_time'] = strtotime('+1 days');
           $invitecontest['end_time'] = strtotime('+2 days'); 
        }
        include template('invitecontest/manage_invitecontest_addedit');
    }
    
    static public function Edit()
    {
        $invitecontest = Table::Fetch('invitecontest', $_REQUEST['id']);
        if ( !$invitecontest )
        {
            Session::set('error', "Failed to find the Contest");
            self::Main();
        }
        
        if (is_post())
        {
            $table = new Table('invitecontest', $_POST);
            $table->SetStrip('description', 'terms_conditions');
            
            //$table->status = 1;
            $table->start_time = strtotime($table->start_time);
            $table->end_time = strtotime($table->end_time);
            $table->SetPk('id', $contest->id);
            $insert = array( 'title', 'description',  'terms_conditions', 'create_time', 'start_time', 'end_time', 'status');
            if ( $table->update($insert) )
            {
                Session::set('notice', 'Contest created successfuly');
                self::Main();
            }
            else
            {
                Session::set('error', 'Failed to Create Contest');
            }
            
        }
        include template('invitecontest/manage_invitecontest_addedit');
    }
    
    static public function Subscription()
    {
       $count = Table::Count('ic_subscribers');
        list($pagesize, $offset, $pagestring) = pagestring($count, 20);

        $subscribers = DB::LimitQuery('ic_subscribers', array(
               // 'condition' => $condition,
                'order' => 'ORDER BY id DESC',
                'size' => $pagesize,
                'offset' => $offset,
        ));
        
        $user_ids = Utility::GetColumn($subscribers, "user_id");
        $users = Table::Fetch('user', $user_ids);
        
         $contest_ids = Utility::GetColumn($subscribers, "invitecontest_id");
        $contests = Table::Fetch('invitecontest', $contest_ids);
        
        include template('invitecontest/manage_invitecontest_subscribers');
    }
    
    static public function Referal()
    {
        $count = Table::Count('ic_referals');
        list($pagesize, $offset, $pagestring) = pagestring($count, 20);

        $referals = DB::LimitQuery('ic_referals', array(
               // 'condition' => $condition,
                'order' => 'ORDER BY id DESC',
                'size' => $pagesize,
                'offset' => $offset,
        ));
        
        $users = Table::Fetch('user', Utility::GetColumn($referals, 'user_id'));
        $refer_by_users = Table::Fetch('user', Utility::GetColumn($referals, 'refer_by'));
        $contests = Table::Fetch('invitecontest', Utility::GetColumn($referals, 'invitecontest_id'));
        
     
       include template('invitecontest/manage_invitecontest_referals');
    }
    
    static public function Disable()
    {
        if (is_post())
        {
            $contest = Table::Fetch('invitecontest', $_POST['id']);
            if ( !$contest )
            {
                Session::set('error', "Failed to find the Contest");
                self::Main();
            }
        }
    }
    
    static public function Delete()
    {
        if (is_post())
        {
            $contest = Table::Fetch('invitecontest', $_POST['id']);
            if ( !$contest )
            {
                Session::set('error', "Failed to find the Contest");
                self::Main();
            }
        }
    }
}
?>
