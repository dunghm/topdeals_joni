<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

require_once dirname(dirname(dirname(__FILE__))).'/include/library/QueryPath/QueryPath.php';


$now = time();

$condition = array();
$condition[] ="id > 800";  
//$condition[] ="begin_time <= {$now}";  

$teams = DB::LimitQuery('team', array(
	'condition' => $condition));



foreach($teams as $t){
    echo '<strong>TEAM ID:</strong>'.$t['id'].'<br>';
    $update_field = array();
    $update_field['summary'] = htmlqp($t['summary'], '.product_package')->text();
    echo '<strong>Nous vous proposons:</strong>'.$update_field['summary'].'<br>';
    
    /// Text paragraphs should be mapped to Emotional text V4 
    $update_field['seo_description'] = $t['seo_description'].' '.htmlqp($t['summary'], 'p')->text();
    
    echo '<strong>Emotional Text:</strong>:'.$update_field['seo_description'].'<br>';
    
    $images = htmlqp($t['summary']);
    //echo '<br> =========== Images =========<br>';
    $it =0;
	
	mkdir(dirname(dirname(dirname(__FILE__))).'/static/team_desc/2014/'.$t['id']);
	echo '=================Desc Images==========================<br>';
    foreach($images->find('img') as $i) {
        
        $img_src = $i->attr('src');
        $image_url = str_replace("../../", "http://www.topdeal.ch/", $img_src);
      //  echo "<img src='".$image_url."' />".'<br>';
        
        
        $content = file_get_contents($image_url);
        $image_name = explode('/', $image_url);
		$image_name = array_pop($image_name);
        //Store in the filesystem.
		echo dirname(dirname(dirname(__FILE__))).'/static/team_desc/2014/'.$t['id'].'/'.$image_name.'<br>';
        $fp = fopen(dirname(dirname(dirname(__FILE__))).'/static/team_desc/2014/'.$t['id'].'/'.$image_name, "w");
        fwrite($fp, $content);
        fclose($fp);
		
        $image_loc = 'team_desc/2014/'.$t['id'].'/'.$image_name;
        
        $team_slideshow_images = array(
            'team_id' => $t['id'],
            'image_loc' => $image_loc,
            'image_desc' => 'desc',
            'image_order' => $it
        );

		DB::Insert('team_slideshow_images', $team_slideshow_images);
        $it++;
        
         
    }  
    
    //Div class « conditions » should be mapped to « Conditions » field in V4 
    $update_field['detail'] = htmlqp($t['summary'], '.conditions')->text();
     
    echo '<strong>Coditions:</strong>'.$update_field['detail'].'<br>';
     
    //echo "=============================================================================================";
    
    $update_field['systemreview'] = htmlqp($t['detail'], '.entry-technique')->text();
    echo '<strong>Technical Info:</strong>'.$update_field['systemreview'].'<br>';
    //$partner_info = Table::Fetch('team', $t['partner_id'], 'partner_id');
    $partner_update = array();
    $partner_update['location'] = htmlqp($t['detail'], '.partner-detail')->text();
    DB::Update('partner', $t['partner_id'], $partner_update);
    
    
    $images_new = htmlqp($t['detail']);
   // echo '<br> =========== Slide Show Images =========<br>';
    $it = 0;
    echo '=================Gallery Images==========================<br>';
	foreach($images_new->find('img') as $i) {
        $img_src = $i->attr('src');
        $image_url = str_replace("../../", "http://www.topdeal.ch/", $img_src);
        //echo "<img src='".$image_url."' /><br/>";
        
        
        $content = file_get_contents($image_url);
        $image_name = explode('/', $image_url);
		$image_name = array_pop($image_name);
        //Store in the filesystem.
		echo dirname(dirname(dirname(__FILE__))).'/static/team_desc/2014/'.$t['id'].'/'.$image_name.'<br>';
        $fp = fopen(dirname(dirname(dirname(__FILE__))).'/static/team_desc/2014/'.$t['id'].'/'.$image_name, "w");
        fwrite($fp, $content);
        fclose($fp);
        $image_loc = 'team_desc/2014/'.$t['id'].'/'.$image_name;
        
        $team_slideshow_images = array(
            'team_id' => $t['id'],
            'image_loc' => $image_loc,
            'image_desc' => 'gallery',
            'image_order' => $it
        );

		DB::Insert('team_slideshow_images', $team_slideshow_images);
        $it++;
        
         
    } 
	$update_field['notice'] = '';
	DB::Update('team', $t['id'], $update_field);
    
    echo '<hr>';
}

//print_r($qp);