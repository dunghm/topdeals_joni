<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('admin');

if(isset($_GET['action'])){
    $action = $_GET['action'];
    $id = $_GET['id'];
    if($action == 'popupdelete'){
        Table::Delete('home_popup', $id);
        Session::Set('notice', 'Popup Deleted Successfully');
    }
    else if ($action == 'deactivate'){
        Table::UpdateCache('home_popup', $id, array('status' => 0));
        Session::Set('notice', 'Popup Deactivated Successfully');
    }
    else if ($action == 'activate'){
        Table::UpdateCache('home_popup', $id, array('status' => 1));
        Session::Set('notice', 'Popup Activated Successfully');
    }
    redirect( WEB_ROOT . "/manage/misc/popup.php");
}




if(is_post()){
   
   if(isset($_POST['popup_id'])){
        
       $popup_id = intval($_POST['popup_id']);
        $popup_array = array(
            'popup_name' => $_POST['popup_name'],
            'popup_startdate' => strtotime($_POST['popup_startdate']),
            'popup_enddate'=> strtotime($_POST['popup_enddate']),
            'popup_link' =>$_POST['popup_link'],   
            'popup_title' =>$_POST['popup_title'],
            'popup_text'=>$_POST['popup_text'],
        );
        
        DB::Update('home_popup', $popup_id, $popup_array);
        
        Session::Set('notice', 'Popup Updated  Successfully');
        redirect( WEB_ROOT . "/manage/misc/popup.php");
   }
   else{
       $popup_array = array(
            'popup_name' => $_POST['popup_name'],
            'popup_startdate' => strtotime($_POST['popup_startdate']),
            'popup_enddate'=> strtotime($_POST['popup_enddate']),
            'popup_link' =>$_POST['popup_link'],   
            'popup_title' =>$_POST['popup_title'],
            'popup_text'=>$_POST['popup_text'],
            'status' => 0
        );
        
        DB::Insert('home_popup', $popup_array);
       
        Session::Set('notice', 'Popup Added  Successfully');
        redirect( WEB_ROOT . "/manage/misc/popup.php");
   }
    
  
}

$popups = DB::LimitQuery('home_popup');
$selector = 'popup';

include template('manage_misc_popup');