<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();

$system = Table::Fetch('system', 1);

if ($_POST) {
	$details = $_POST;
        
	if (isset($_FILES['promotion_image']) && $_FILES["promotion_image"]["error"] == 0 ) {
               $path = IMG_ROOT . '/' .$_FILES['promotion_image']['name'];
                move_uploaded_file($_FILES['promotion_image']['tmp_name'], $path); 
                $details['promotions']['image'] = $_FILES['promotion_image']['name'];
        }
        else{
            $details['promotions']['image'] = $INI['promotions']['image'];
        }
        if(!isset($_POST['promotions']['today_deals'])){
            $details['promotions']['today_deals'] = 0;
        }
        else{
            $details['promotions']['today_deals'] = 1;
        }
        
	$INI = Config::MergeINI($INI, $details);
	$INI = ZSystem::GetUnsetINI($INI);
	save_config();

	$value = Utility::ExtraEncode($INI);
	$table = new Table('system', array('value'=>$value));
	if ( $system ) $table->SetPK('id', 1);
	$flag = $table->update(array( 'value'));

	Session::Set('notice', 'Promotions Successfully Updated!');
	redirect( WEB_ROOT . '/manage/misc/promotions.php');	
}

include template('manage_misc_promotions');

