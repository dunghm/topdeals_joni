<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('market');

$id = abs(intval($_GET['id']));
$partner = Table::Fetch('partner', $id);

if ( $_POST && $id==$_POST['id'] && $partner) {
	$table = new Table('partner', $_POST);
	$table->SetStrip('location', 'other');
	$table->group_id = abs(intval($table->group_id));
	$table->city_id  = abs(intval($table->city_id));
	$table->head	 = $table->head;
	$table->email	 = $table->email_address;
	$table->open 	 = (strtoupper($table->open)=='Y') ? 'Y' : 'N';
	$table->display  = (strtoupper($table->display)=='Y') ? 'Y' : 'N';
	$table->image 	 = upload_image('upload_image', $partner['image'], 'team', true);
	$table->image1   = upload_image('upload_image1', $partner['image1'], 'team');
	$table->image2   = upload_image('upload_image2', $partner['image2'], 'team');
	$table->business_address	=	$table->business_address;
	$up_array = array(
			'username', 'title', 'bank_name', 'bank_user', 'bank_no',
			'location', 'other', 'homepage', 'contact', 'mobile', 'phone',
			'address', 'group_id', 'open', 'city_id', 'display',
			'image', 'image1', 'image2', 'longlat','head','email','business_address'
			);

	if ($table->password ) {
		$table->password = ZPartner::GenPassword($table->password);
		$up_array[] = 'password';
	}
	$flag = $table->update( $up_array );
	if ( $flag ) {
		if ( ($_POST['id'] == $login_user_id) && !isset($_POST['enable']) )
		{
			Session::Set('notice', 'Modify business information successfully but you are not allow to disable your account.');
			redirect( WEB_ROOT . "/manage/partner/edit.php?id={$id}");
			
		} else if ( ($_POST['id'] == $login_user_id) && isset($_POST['enable']) ){
			
			Session::Set('notice', 'Modify Business Information Success');
			redirect( WEB_ROOT . "/manage/partner/edit.php?id={$id}");
			
		} else if ($_POST['id'] != $login_user_id) {
			
			# update status
			$enable = 'N';
			if(isset($_POST['enable']) && $_POST['enable']=='Y'){
				$enable = 'Y';
			}
			$partner_id = $_POST['id'];
			DB::Query("UPDATE partner SET enable = '".$enable."' WHERE id = $partner_id");
			Session::Set('notice', 'Modify Business Information Success');
			redirect( WEB_ROOT . "/manage/partner/edit.php?id={$id}");
		}
	}
	Session::Set('error', 'Business information failed to modify');
	$partner = $_POST;
}

include template('manage_partner_edit');
