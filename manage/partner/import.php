<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');
//require_once(dirname(__FILE__) . '/current.php');

//Authentication
need_manager();
$log = new KLogger(dirname(dirname(dirname(__FILE__)))."/include/compiled/", KLogger::DEBUG);

if(is_post()){
        if($_FILES["import_file"]["name"]) {
                $filename = $_FILES["import_file"]["name"];
                $source = $_FILES["import_file"]["tmp_name"];
                $type = $_FILES["import_file"]["type"];
                 $log->logInfo("Filename =".$filename);
                $name = explode(".", $filename);
                
                $accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
                foreach($accepted_types as $mime_type) {
                        if($mime_type == $type) {
                                $okay = true;
                                break;
                        } 
                }

                $continue = strtolower($name[1]) == 'zip' ? true : false;
                if(!$continue) {
                        Session::Set('error', 'Edit project failure');
                        redirect( WEB_ROOT . "/manage/team/index.php");
                }
                $target_path = dirname(dirname(dirname(__FILE__))).'/generated_labels/'.$filename;  // change this to the correct site path
                 if(move_uploaded_file($source, $target_path )) {
                    $log->logInfo("File uploaded ");
                    $log->logInfo("File Target Locaton: ".$target_path);
                    $final_dir = dirname(dirname(dirname(__FILE__))).'/generated_labels/partner-'.rand();
                    //rmdir_recursive($targetdir);
                    mkdir($targetdir, 0777); 
                    $zip = new ZipArchive();
                    $x = $zip->open($target_path);  // open the zip file to extract
                    if ($x === true) {
                        $zip->extractTo($final_dir); // place in the directory with same name  
                        $log->logInfo("Zip extracted to the location: ".$final_dir);
                        $zip->close();
                        unlink($target_path);
                       
                    }else{
                        $log->logInfo("Zip File cannot be extracted");
                    }
                    
                    // NOw play here
                    $json_team = file_get_contents($final_dir.'/partner.json');
                    
                    $log->logInfo("Json Downloaded: ".$json_team);
                    $partner = json_decode($json_team, true);
                     unset($partner['id']);
                    if($partner['image']){
                        $partner['image'] = upload_image_import($final_dir.'/'.$partner['image'], 'team', true);
                    }
                    if($partner['image1']){
                        $partner['image1'] = upload_image_import($final_dir.'/'.$partner['image1'], 'team', true);
                    }
                    if($partner['image2']){
                        $partner['image2'] = upload_image_import($final_dir.'/'.$partner['image2'], 'team', true);
                    }
                   
                    
                    $partner_id = DB::Insert('partner', $partner); 
					if(!$partner_id){
						 Session::Set('notice', 'Parnter name conflicts with existing record');
						  redirect( WEB_ROOT . "/manage/partner/index.php");
					}
                    $log->logInfo("Partner Inserted with partner_id: ".$partner_id);
                    Session::Set('notice', 'Partner Imported successfully');
                    redirect( WEB_ROOT . "/manage/partner/index.php");
            }
        }
}


function upload_image_import($img, $type='team', $scale) {
            
            $year = date('Y'); 
            $day = date('md'); 
            $n = time().rand(1000,9999).'.jpg';
	    RecursiveMkdir( IMG_ROOT . '/' . "{$type}/{$year}/{$day}" );
            $image = "{$type}/{$year}/{$day}/{$n}";
            $path = IMG_ROOT . '/' . $image;
            copy($img, $path);
            if($type=='team' && $scale) {
			$npath = preg_replace('#(\d+)\.(\w+)$#', "\\1_index.\\2", $path); 
                        //Image::Convert($path, $npath, 348, 141, Image::MODE_CUT);
                        //Image::Convert($path, $npath, 348, 141, Image::MODE_SCALE);
			$simple = new SimpleImage(); 
			$simple->load($path);			
			$simple->resize(460,307); 
			$simple->save($npath, IMAGETYPE_PNG);
            }
            if($type=='team_desc' && $scale) {
			$npath = preg_replace('#(\d+)\.(\w+)$#', "\\1_index.\\2", $path); 
			//Image::Convert($path, $npath, 348, 141, Image::MODE_CUT);
            //Image::Convert($path, $npath, 638, 273, Image::MODE_SCALE);
			
			$simple = new SimpleImage(); 
			$simple->load($path);			
			$simple->resize(638,273); 
			$simple->save($npath, IMAGETYPE_PNG);
            }
            return $image;
}
