<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();

$id = abs(intval($_GET['id']));
$partner = Table::Fetch('partner', $id);

$file = tempnam("tmp", "zip");
$zip = new ZipArchive();
$zip->open($file, ZipArchive::OVERWRITE);

if($partner['image'] != ''){
    $image_arr = explode('/', $partner['image']);
    $zip->addFile(dirname(dirname(dirname(__FILE__))).'/static/'. $partner['image'] , $image_arr[count($image_arr)-1]);
    $partner['image'] = $image_arr[count($image_arr)-1];
}

if($partner['image1'] != ''){
    $image_arr = explode('/', $partner['image1']);
    $zip->addFile(dirname(dirname(dirname(__FILE__))).'/static/'. $partner['image1'] , $image_arr[count($image_arr)-1]);
    $partner['image1'] = $image_arr[count($image_arr)-1];
}

if($partner['image2'] != ''){
    $image_arr = explode('/', $partner['image2']);
    $zip->addFile(dirname(dirname(dirname(__FILE__))).'/static/'. $partner['image2'] , $image_arr[count($image_arr)-1]);
    $partner['image2'] = $image_arr[count($image_arr)-1];
}
$json = json_encode($partner);
$zip->addFromString('partner.json', $json);
$zip->close();
header('Content-Type: application/zip');
header('Content-Length: ' . filesize($file));
header('Content-Disposition: attachment; filename="partner-'.$id.'.zip"');
readfile($file);
unlink($file); 