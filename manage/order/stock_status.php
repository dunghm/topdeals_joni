<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('order');
$like = TRUE;
$now = time();

$where_team = '';
$where_multi = '';
        

$csvSql = '';
if(isset($_GET['deal_status'])){
    $deal_status = $_GET['deal_status'];
    if($deal_status == 'active'){
        $where_team .= "end_time > $now AND delivery <> 'coupon'";
        $where_multi .= "end_time > $now AND delivery <> 'coupon'";
    } else if($deal_status == 'inactive'){
        $where_team .= "end_time < $now AND delivery <> 'coupon'";
        $where_multi .= "end_time < $now AND delivery <> 'coupon'";
    }
    else if($deal_status == 'all'){
        $where_team .= "delivery <> 'coupon'";
        $where_multi .= "delivery <> 'coupon'";
    }
}
else{
    $where_team .= " end_time > $now AND delivery <> 'coupon'";
    $where_multi .= " end_time > $now AND delivery <> 'coupon'";
}

if(isset($_GET['search_deal'])){
    $search_deal = trim($_GET['search_deal']);
    if($search_deal != '')
    {
        if($like == TRUE ){
            $where_team .= " AND ( t.title LIKE '%{$search_deal}%' OR t.seo_description LIKE '%{$search_deal}%' OR t.title_fr LIKE '%{$search_deal}%' )";
            $where_multi .= " AND (m.title LIKE '%{$search_deal}%' OR t.title LIKE '%{$search_deal}%' OR t.title_fr LIKE '%{$search_deal}%' OR m.title_fr LIKE '%{$search_deal}%' OR t.seo_description LIKE '%{$search_deal}%') ";
            
        }else{
            $where_team .= " AND (MATCH(t.title) AGAINST('{$search_deal}' in boolean mode) OR MATCH(t.seo_description) AGAINST ('{$search_deal}' IN boolean mode))";
            $where_multi .= " AND MATCH(m.title) AGAINST('{$search_deal}' in boolean mode)";
        }
    }
}
 $team_id = abs(intval($_GET['team_id']));
if($team_id){
     
     $where_team .= " AND t.id = $team_id";
     $where_multi .= " AND (m.id = $team_id OR t.id = $team_id )";
}
$supplier_id = abs(intval($_GET['supplier_id']));
if($supplier_id){
    $supplier_id = $_GET['supplier_id'];
    $where_team .= " AND t.supplier_id = $supplier_id";
    $where_multi .= " AND t.supplier_id = $supplier_id";
}
$sort = 'team_id';

if(isset($_GET['sort'])){
    $sort = $_GET['sort'];
}

$sort_direction = 'DESC';

if(isset($_GET['direction'])){
    $sort_direction = $_GET['direction'];
}
if(isset($_GET['order_status'])){
 
   $order_status = trim($_GET['order_status']);
   switch($order_status){
       case '':
           break;
       case 'Waiting':
            $where_team .= " HAVING (express > 0 OR pickup > 0)";
            $where_multi .= " HAVING (express > 0 OR pickup > 0)";
           break;
       case 'Available':
           $where_team .= " HAVING pickup_available > 0";
            $where_multi .= " HAVING pickup_available > 0";
            break;
       case 'Delivered':
            $where_team .= " HAVING (pickup_delivered > 0 OR express_delivered > 0)";
            $where_multi .= " HAVING (pickup_delivered > 0 OR express_delivered > 0)";
            break;
       case 'InPreperation':
           $where_team .= " HAVING `express_available` > 0";
            $where_multi .= " HAVING express_available > 0";
           
           break;
   }
}

$sql = " 
    (SELECT t.`id` AS team_id, '' AS team_title,'-' AS option_id, IFNULL(t.title_fr, t.title) AS Title, IFNULL(t.stock_count, 0) AS stock_count, 'deal' AS 'type', t.end_time,
(SELECT IFNULL( SUM(oi.quantity) ,0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.team_id = t.`id` AND delivery_status = ".ZOrder::ShipStat_Delivered." AND delivery = 'express' AND oi.option_id IS NULL  ) AS `express_delivered`,
(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.team_id = t.`id` AND delivery_status = ".ZOrder::ShipStat_Waiting." AND delivery = 'express' AND oi.option_id IS NULL ) AS `express`,
(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.team_id = t.`id` AND oi.delivery_status = ".ZOrder::ShipStat_InPrep." AND delivery = 'express' AND oi.option_id IS NULL ) AS `express_available`,
(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.team_id = t.`id` AND delivery_status = ".ZOrder::ShipStat_Waiting." AND  delivery = 'pickup' AND oi.option_id IS NULL ) AS `pickup`,
(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.team_id = t.`id` AND delivery_status = ".ZOrder::ShipStat_PickedUp." AND delivery = 'pickup' AND oi.option_id IS NULL ) AS `pickup_delivered`,
(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.team_id = t.`id` AND delivery_status = ".ZOrder::ShipStat_Available." AND delivery = 'pickup' AND oi.option_id IS NULL ) AS `pickup_available`,
 IFNULL((SELECT DISTINCT sot.total_sent_mail FROM supplier_mail_order_tracking AS sot WHERE sot.team_id	= t.id	AND sot.option_id	= 0 AND sot.is_stock_update	= 0),0) AS `supplier_email_sent`,
1 AS DEAL_ORDER
FROM team t 
LEFT JOIN team_multi m ON m.team_id = t.id 
WHERE t.type = '' AND $where_team)

UNION 

(SELECT t.`id` AS team_id, t.title AS team_title, m.id AS option_id, IFNULL(m.title_fr, m.title) AS Title, IFNULL(m.stock_count, 0) AS stock_count, 'option' AS 'type', t.end_time, 
(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.`option_id` = m.`id` AND delivery_status = ".ZOrder::ShipStat_Delivered." AND delivery = 'express' GROUP BY oi.option_id) AS `express_delivered`,
(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.`option_id` = m.`id` AND oi.delivery_status = ".ZOrder::ShipStat_Waiting." AND delivery = 'express' GROUP BY oi.option_id) AS `express`,
(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.`option_id` = m.`id` AND oi.delivery_status = ".ZOrder::ShipStat_InPrep." AND delivery = 'express' GROUP BY oi.option_id) AS `express_available`,
(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.`option_id` = m.`id` AND oi.delivery_status = ".ZOrder::ShipStat_Waiting." AND delivery = 'pickup' GROUP BY oi.option_id) AS `pickup`,
(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.`option_id` = m.`id` AND oi.delivery_status = ".ZOrder::ShipStat_PickedUp." AND delivery = 'pickup' GROUP BY oi.option_id) AS `pickup_delivered`,
(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.`option_id` = m.`id` AND oi.delivery_status = ".ZOrder::ShipStat_Available." AND delivery = 'pickup' GROUP BY oi.option_id) AS `pickup_available`,
 IFNULL((SELECT DISTINCT sot.total_sent_mail FROM supplier_mail_order_tracking AS sot WHERE sot.team_id	= t.id	AND sot.option_id	= m.id AND sot.is_stock_update	= 0),0) AS `supplier_email_sent`,
0 AS DEAL_ORDER
FROM team_multi m

INNER JOIN team t ON t.`id` = m.team_id 
WHERE t.type!='' AND $where_multi)";

$csvSql.= $sql;

$sql_count = " 
    SELECT COUNT(*) AS count FROM ( $sql ) AS t1";


$result_count = DB::GetQueryResult($sql_count, false);

$count = $result_count[0]['count'];
if(isset($_GET['page_size'])){
    $page_size = $_GET['page_size'];
}
else{
    $page_size = 20;
}
list($pagesize, $offset, $pagestring) = pagestring($count, $page_size);

$sql .= " ORDER BY $sort $sort_direction,Title ASC  LIMIT $offset, $pagesize";

$teams = DB::GetQueryResult($sql, false);


$selector = 'stock_status';
$option_deal_status = array('active'=> 'Active', 'inactive'=> 'Inactive', 'all'=>'All');
$suppliers = DB::LimitQuery('supplier', array(
			'order' => 'ORDER BY title ASC',
			));
$suppliers = Utility::OptionArray($suppliers, 'id', 'title');
$option_order_status = array(''=> 'All','Waiting'=>'Waiting', 'Available'=>'Available', 'Delivered'=>'Delivered', 'InPreperation' => 'InPreperation');

$option_page_size = array(20=>'20', 30=>'30', 50=>'50', 100=> '100', 200=> '200');

/* download excel files*/
if ( $_GET['download'] == "1" )
{
    
	$csvSql.= " ORDER BY $sort $sort_direction,Title ASC ";
	$teams = DB::GetQueryResult($csvSql, false);	
	
	if (!$teams) die('-ERR ERR_NO_DATA');
   
    $name = 'stock_status'.date('Ymd');
    $kn = array(
        'team_title' => 'Title', 
        'stock_count' => 'Current Stock',
        'express' => 'Express Waiting Qty',
        'pickup' => 'Picked Up Waiting Qty',      
        'express_available' => 'Express Preparation Qty',
        'express_delivered' => 'Express Delivered Qty',
        'pickup_delivered' => 'Picked Up Qty',
        'pickup_available' => 'Picked Available Qty',
        'end_time' => 'Deal Status'
        );

  
    
    foreach( $teams AS $one ) 
	{       
		  
		  #Title
		  $title = '';
		  $title.= $one['team_id']."&nbsp;";
		  
		  if($one['type'] == 'option')
		  {
			$title.="(".$one['team_title'].")"; 
		  } 
		  else if($one['type'] == 'deal') 
		  {
			 $title.= $one['title'];
		  
		  }
          
		  $title.="<br />";
		  if($one['type'] == 'option')
		  {
			$title.="Option:&nbsp;";
			$title.=$one['title']; 
		  }
		  
		   $one['team_title'] =$title;
		  
		  #Current Stock
		  
		  $express = '';
		  #Express Waiting Qty
		  if($one['express'] > 0 && $one['type'] == 'deal'  )
		  {
			 $express=$one['express'];
		  } 
		  else if($one['express'] > 0 && $one['type'] == 'option') 
		  { 
			 $express=$one['express'];
		  } 
		  else 
		  {  
			if($one['express']==NULL)
			{
				$express=0;
			} 
			else 
			{ 
				$express=$one['express']; 
			}
		 }
		 
		 $one['express'] = $express;
		 
		 
		 #Picked Up Waiting Qty
		 $pickup = '';
		 if($one['pickup'] > 0 && $one['type'] == 'deal'  )
		 {
			$pickup= $one['pickup']; 
		 } 
		 else if($one['pickup'] > 0 && $one['type'] == 'option') 
		 { 
			$pickup=$one['pickup']; 
		 } 
		 else 
		 { 
			if($one['pickup']==NULL)
			{
				$pickup=0;
			} 
			else 
			{ 
				$pickup=$one['pickup']; 
			}
		 }
		 
		 $one['pickup']=$pickup;
		
		 #Express Preparation Qty
		 $express_available = '';
		 if($one['express_available'] > 0 && $one['type'] == 'deal'  )
		 {
			$express_available=$one['express_available']; 
		 } 
		 else if($one['express_available'] > 0 && $one['type'] == 'option') 
		 { 
			$express_available=$one['express_available']; 
		 } 
		 else 
		 { 
			if($one['express_available']==NULL)
			{
				$express_available=0;
			} 
			else 
			{ 
				$express_available=$one['express_available'];
			}
		 }	
		 
		 $one['express_available'] = $express_available;
		 
		 #express_delivered
		 $express_delivered = '';
		 if($one['express_delivered'] > 0 && $one['type'] == 'deal'  )
		 {
			$express_delivered =$one['express_delivered'];
		 } else if($one['express_delivered'] > 0 && $one['type'] == 'option') 
		 {
			$express_delivered =$one['express_delivered']; 
		 } 
		 else 
		 { 
			if($one['express_delivered']==NULL)
			{
				$express_delivered =0;
			} else 
			{ 
				$express_delivered =$one['express_delivered']; 
			}
		}
		  $one['express_delivered'] = $express_delivered;
		  
		 # pickup_delivered
		 
		 $pickup_delivered = '';
		 
		 if($one['pickup_delivered'] > 0 && $one['type'] == 'deal'  )
		 {
			$pickup_delivered = $one['pickup_delivered']; 
		 } 
		 else if($one['pickup_delivered'] > 0 && $one['type'] == 'option') 
		 { 
			$pickup_delivered = $one['pickup_delivered']; 
		 } 
		 else 
		 { 
			if($one['pickup_delivered']==NULL)
			{
				$pickup_delivered = 0;
			} 
			else 
			{ 
				$pickup_delivered =$one['pickup_delivered']; 
			}
		}
		$one['pickup_delivered']=$pickup_delivered; 
		
		#pickup_available
		
		$pickup_available = '';
		if($one['pickup_available'] > 0 && $one['type'] == 'deal'  )
		{
			$pickup_available = $one['pickup_available']; 
		} 
		else if($one['pickup_available'] > 0 && $one['type'] == 'option') 
		{ 
			$pickup_available = $one['pickup_available'];
		} 
		else 
		{ 
			if($one['pickup_available']==NULL)
			{
				$pickup_available = 0;
			} 
			else 
			{ 
				$pickup_available = $one['pickup_available']; 
			}
		}
		
		$one['pickup_available'] = $pickup_available; 
		
		  #Deal Status
		  if($one['end_time'] > $now )
		  {
				 $one['end_time'] = "Active";
		  } 
		  else if($one['end_time'] < $now ) 
		  { 
				$one['end_time'] = "Inactive";
			
		  }
		   $eorders[] = $one;
    }
    down_xls($eorders, $kn, $name);
}

/* download excel file */

include template('manage_order_stock_status');