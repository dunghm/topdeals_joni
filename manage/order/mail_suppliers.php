<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('order');

/*function generate_ref( $length = 8 ) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $ref = substr( str_shuffle( $chars ), 0, $length );
    return $ref;
}*/
$gen_ref = strtotime( date('Y-m-d h:i:s') );

if(is_post()){
    $email = $_POST['email'];
    
    if(trim($email) == ''){
         Session::Set('error', 'Please Enter an email for Supplier');
		 
		 $previousPostedData	=	$_GET['previousPostedData'];
		 $urlPortion= '?previousPostedData='.urlencode($previousPostedData);
		 redirect( WEB_ROOT . "/manage/order/mail_suppliers.php".$urlPortion);
		 exit();

    }
    
    if(strpos($email, ';')){
       $email_arr = explode(';', $email); 
    }
    else{
        $email_arr = array();
        $email_arr[] = $email;
    }
    $email_arr[] = 'commande@topdeal.ch';
    
    $email_arr = array_map('trim', $email_arr);
    
    $subject = $_POST['subject'];
    $message = $_POST['message'];
    
    global $INI;
    
    $encoding = $INI['mail']['encoding'] ? $INI['mail']['encoding'] : 'UTF-8';
    $options = array(
            'contentType' => 'text/html',
            'encoding' => $encoding,
    );

	/*
	 * @nams: M.Sajid
	 * @desc: Insert record into db then send email
	 */
	//LOG MAIL SENT TO SUPPLIER NEW RECORD
	if(isset($_POST['order_email_sent'])){
		$from = 'commande@topdeal.ch';
		$lastInsertIds = '';
		$lastInsertId_arr = array();
		$order_email_sent				=	$_POST['order_email_sent'];
		
		$explode = explode("___", $order_email_sent);
		
		/*$sql = "INSERT INTO supplier_mail_order_tracking (user_id,team_id,option_id) 
				VALUES $order_email_sent";
		DB::Query($sql);*/
		
		foreach ($explode as $e){
			$explode_single = explode(",", $e);
			$_user_id = $explode_single[0];
			$_team_id = $explode_single[1];
			$_option_id = $explode_single[2];
			
			/*$lastInsertId = DB::GetInsertId();//mysql_insert_id();
			$lastInsertId_arr[] = $lastInsertId;*/
			
			#update record for recipient, subject, content
			#$recipient = json_encode($email_arr[0]);
			$email_ids = trim($email_arr[0], ",");
			$recipient = mysql_real_escape_string( $email_ids );
			#echo'<pre>';print_r($recipient);echo'</pre>';exit;
			$_subject = 'Ref# '.$gen_ref.' - '.$subject;			
			$content = mysql_real_escape_string( str_replace("[REF]", '<p>Ref# '.$gen_ref.'</p>', $message) );
			$m_body = str_replace("[REF]", '<p>Ref# '.$gen_ref.'</p>', $message);

			$sql = "INSERT INTO supplier_mail_order_tracking (ref_no,user_id,team_id,option_id,recipient,subject,content) 
					VALUES ('".$gen_ref."', $_user_id, $_team_id, $_option_id, '".$recipient."', '".$_subject."', '".$content."')";
			DB::Query($sql);
			#echo'<pre>';print_r($sql);echo'</pre>';
			/*$sql = "UPDATE supplier_mail_order_tracking 
					SET recipient = '".$recipient."', subject = '".$_subject."', content = '".$content."'
					WHERE id = $lastInsertId";
			#
			DB::Query($sql);*/
		}
	}	#exit;
	
	//LOG MAIL SENT TO SUPPLIER UPDATE RECORD
	if(isset($_POST['order_email_sent'])){
		
		$ref_no = '';
		$from = 'commande@topdeal.ch';
		$order_team_option_old_record	=	unserialize($_POST['order_team_option_old_record']);
		
		if(!empty($order_team_option_old_record)){
			foreach($order_team_option_old_record as $order_team_option_old_recordRow){
				
				$submitted_team_id			=	$order_team_option_old_recordRow['team_id'];
				$submitted_option_id		=	$order_team_option_old_recordRow['option_id'];
				$submitted_total_sent_mail	=	$order_team_option_old_recordRow['total_sent_mail'];
				$submitted_login_user_id	=	$order_team_option_old_recordRow['login_user_id'];
				
				# we comment it to track every email
				#$sql = "UPDATE supplier_mail_order_tracking SET total_sent_mail = '$submitted_total_sent_mail', user_id = '$submitted_login_user_id', is_stock_update = '0' WHERE team_id = '$submitted_team_id' AND  option_id = '$submitted_option_id' ";
				
				
				#$lastInsertId = DB::GetInsertId();
			
				#update record for recipient, subject, content
				$recipient = json_encode($email_arr[0]);
				$_subject = 'Ref# '.$gen_ref.' - '.$subject;			
				$content = mysql_real_escape_string( str_replace("[REF]", '<p>Ref#'.$lastInsertId.'</p>', $message) );
				$m_body = str_replace("[REF]", '<p>Ref# '.$gen_ref.'</p>', $message);

				$sql = "INSERT INTO supplier_mail_order_tracking (ref_no, user_id,team_id,option_id,recipient,subject,content) 
						VALUES ('".$gen_ref."', $submitted_login_user_id, $submitted_team_id, $submitted_option_id, '".$recipient."', '".$_subject."', '".$content."')";
				DB::Query($sql);
				
				/*$sql = "UPDATE supplier_mail_order_tracking 
						SET recipient = '".$recipient."', subject = '".$_subject."', content = '".$content."'
						WHERE id = $lastInsertId";
				#echo'<pre>';print_r($sql);echo'</pre>';
				DB::Query($sql);*/
			}
		}		
	}
	
	# now send email
	$from = 'commande@topdeal.ch';
	mail_custom($email_arr, $_subject, $m_body, $from);
		
    //$from = $INI['mail']['from'];
    //
    //to = array_shift($email_arr);
    /*
    $email_arr = array_filter( $email_arr);
    if( !empty( $email_arr))
    {
        $bcc = array_filter($email_arr);
    }
    else{
        $bcc = array();
    }*/
    
     //mail_custom($email_arr, $subject, $message, $from);
     
    /*if ($INI['mail']['mail']=='mail') {
            //Mailer::SendMail($from, $to, $subject, $message, $options, $emails, $bcc);
           
    } else {
            Mailer::SmtpMail($from, $to, $subject, $message, $options, $emails, $bcc);
    }*/
    if(isset($_POST['page']))
    {
        $build_query = http_build_query(unserialize(urldecode(urldecode($_POST['page']))));
        if($build_query){
            $page = '?'.$build_query;
        }
        else{
            $page = '';
        }
        
    }
    else{
        $page = '';
    }	
	
    Session::Set('notice', 'Suppliers mailed Successfully');
    redirect( WEB_ROOT . "/manage/order/stock_status.php".$page);
}
if(isset($_GET)){
    
    /*echo '<pre>';
        print_r($_GET);
    echo '</pre>';*/
	//SET GET REQUEST BY UNSERIALIZE POSTED ARRAY
	if(isset($_GET['previousPostedData'])){
		
		$previousPostedData	=	$_GET['previousPostedData'];
		$previousPostedData	=	unserialize($previousPostedData);
		
		$_GET				=	$previousPostedData;
	}
	
    $id = $_GET['id'];
    
    $team = array();
    $team_suppliers = array();
    $team_supplier_id = 0;
	$order_email_sent		=	array();
	//SET TEAM & OPTION ID ARRAY FOR UPDATE THERE RECORD 
	$order_team_option_old_record		=	array();
    $index =0;
    $mail_body = '[REF] Bonjour,<br/>Nous aimerions procéder à la commande suivante :<br/><br>';
    
	//make email body tabular form
	if(!empty($id)){
		$mail_body	.= "<table border='1'>";
		$mail_body	.= "<tr><td ><strong>Deal Name</strong></td><td><strong>SKU</strong></td><td><strong>Nombre d'article à commande</strong></td></tr>";
	}
	
    foreach ($id as $i){
        $type = $_GET['type_'.$i];
        if($type == 'deal'){
             $team[$i] = Table::Fetch('team', $i);
             
             $waiting_pickup_sql =  "SELECT IFNULL(SUM(oi.quantity),0) AS pickup_waiting FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.team_id = ".$i." AND delivery_status = ".ZOrder::ShipStat_Waiting." AND  delivery = 'pickup' AND oi.option_id IS NULL";
             $waiting_pickup_query = DB::GetQueryResult($waiting_pickup_sql, false);
             $waiting_pickup = $waiting_pickup_query[0]['pickup_waiting'];
             
             $waiting_express_sql =  "SELECT IFNULL(SUM(oi.quantity),0) AS express_waiting FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.team_id = ".$i." AND delivery_status = ".ZOrder::ShipStat_Waiting." AND  delivery = 'express' AND oi.option_id IS NULL";
             $waiting_express_query = DB::GetQueryResult($waiting_express_sql, false);
             $waiting_express = $waiting_express_query[0]['express_waiting'];
             
             $waiting_orders = $waiting_pickup + $waiting_express;
             
             // $mail_body  .= "<strong>".$team[$i]['title']."</strong> Nombre d'article à commander: <strong>".$waiting_orders.'</strong><br>';
			 $mail_body	.= "<tr><td>".$team[$i]['title']."</td><td>".$team[$i]['sku']."</td><td>".$waiting_orders."</td></tr>";
		 
			 //ORDER SEND EMAIL TRACKING
			 //CHECK ORDER ALREADY EXSIST THEN UPDATE COUNTER ARRAY MAINTAIN ELSE INSERT NEW RECORD ARRAY MAINTAIN
			 $sqlSentMail		=	"SELECT total_sent_mail FROM supplier_mail_order_tracking WHERE team_id = '$i' AND option_id = 0 ";
		     $sqlSentMailData   = DB::GetQueryResult($sqlSentMail, false);
			 
			 if(!empty($sqlSentMailData) && isset($sqlSentMailData[0]['total_sent_mail'])){
				$total_sent_mail	=	$sqlSentMailData[0]['total_sent_mail'] + 1;
				
				$order_team_option_old_record[$index]['team_id']			=	$i;
				$order_team_option_old_record[$index]['option_id']			=	0;
				$order_team_option_old_record[$index]['total_sent_mail']	=	$total_sent_mail;
				$order_team_option_old_record[$index]['login_user_id']		=	$login_user_id;
			 }else{
				$order_email_sent[$index] 	= "'".$login_user_id."','".$i."' , '0'";
			 }
        }
        else{
            $option[$i] = Table::Fetch('team_multi', $i);
            $team[$i] =  Table::Fetch('team', $option[$i]['team_id']);
            
            $waiting_pickup_sql =  "SELECT IFNULL(SUM(oi.quantity),0) AS pickup_waiting FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.`option_id` = ".$i." AND delivery_status = ".ZOrder::ShipStat_Waiting." AND  delivery = 'pickup'";
             $waiting_pickup_query = DB::GetQueryResult($waiting_pickup_sql, false);
             $waiting_pickup = $waiting_pickup_query[0]['pickup_waiting'];
             
             $waiting_express_sql =  "SELECT IFNULL(SUM(oi.quantity),0) AS express_waiting FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.`option_id` = ".$i." AND delivery_status = ".ZOrder::ShipStat_Waiting." AND  delivery = 'express'";
            
             $waiting_express_query = DB::GetQueryResult($waiting_express_sql, false);
             $waiting_express = $waiting_express_query[0]['express_waiting'];
             
             $waiting_orders = $waiting_pickup + $waiting_express;
             

            // $mail_body  .= "<strong>".$team[$i]['title'].'</strong> Option:<strong>'.$option[$i]['title_fr']."</strong> Nombre d'article à commander:<strong> ".$waiting_orders. '</strong><br>';
			$mail_body	.= "<tr><td>".$team[$i]['title']."<br/><strong> Option :</strong> ".$option[$i]['title_fr']."</td><td>".$option[$i]['sku']."</td><td>".$waiting_orders."</td></tr>";
 			 //ORDER SEND EMAIL TRACKING
			 	 //ORDER SEND EMAIL TRACKING
			 //CHECK ORDER ALREADY EXSIST THEN UPDATE COUNTER ARRAY MAINTAIN ELSE INSERT NEW RECORD ARRAY MAINTAIN
			 $selected_team_id	=	$_GET['team_id_'.$i];
			 $sqlSentMail		=	"SELECT total_sent_mail FROM supplier_mail_order_tracking WHERE team_id = '$selected_team_id' AND option_id = '$i' ";
		     $sqlSentMailData   = DB::GetQueryResult($sqlSentMail, false);
			 
			 if(!empty($sqlSentMailData) && isset($sqlSentMailData[0]['total_sent_mail'])){
				 
				$total_sent_mail	=	$sqlSentMailData[0]['total_sent_mail'] + 1;
				
				$order_team_option_old_record[$index]['team_id']			=	$selected_team_id;
				$order_team_option_old_record[$index]['option_id']			=	$i;
				$order_team_option_old_record[$index]['total_sent_mail']	=	$total_sent_mail;
				$order_team_option_old_record[$index]['login_user_id']		=	$login_user_id;
			 }else{
				 $order_email_sent[$index] 	= "'".$login_user_id."', '".$selected_team_id."' , '".$i."'";
			 }
			
        }
        //echo $team[$i]['supplier_id'].'<br>';
        
        if($index > 0){
                if($team_supplier_id != $team[$i]['supplier_id']){
                    if(isset($_GET['page_get']))
                    {
                             $build_query = http_build_query(unserialize(urldecode($_GET['page_get'])));
                            if($build_query){
                                $page = '?'.$build_query;
                            }
                            else{
                                $page = '';
                            }

                    }
                    Session::Set('error', 'Please Select Items belonging to same Supplier');
                    redirect( WEB_ROOT . "/manage/order/stock_status.php".$page);
                    
                }
             }
             else{
                  $team_supplier_id = $team[$i]['supplier_id'];
             }
            
        $index++;
        
    }
	$mail_body .= "</table>";
    $supplier  =  Table::Fetch('supplier', $team_supplier_id);
    
	$order_email_sent				=	implode("___",$order_email_sent);
	// $order_email_sent				=	serialize($order_email_sent);
	$order_team_option_old_record	=	serialize($order_team_option_old_record);
   // print_r($supplier);
   
    
    
}

include template('manage_mail_supplier');