<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('order');

$now = time();

$sql = " FROM `order` AS `order`
LEFT JOIN  
refunded_order_item ro
ON
order.id = ro.order_id ";

$filter_clause = "WHERE 1=1 ";

$team_id = abs(intval($_GET['team_id']));
if ( $team_id )
       $filter_clause .= (strlen($filter_clause)>0? " AND " : " WHERE ") . " ro.team_id = {$team_id} ";
            
        
$ship_status = abs(intval($_GET['ship_status']));
if ($ship_status )
    $filter_clause .= (strlen($filter_clause)>0? " AND " : " WHERE ") . "  ro.delivery_status = {$ship_status}";
    
$ship_eta = strval($_GET['ship_eta']);
if ($ship_eta){
    $etatime = strtotime($ship_eta);
    $eta_start = mktime(0,0,0, date("n",$etatime), date("j", $etatime), date("Y", $etatime));
    $eta_end = mktime(23,59,59, date("n",$etatime), date("j", $etatime), date("Y", $etatime));
    $filter_clause .= (strlen($filter_clause)>0? " AND " : " WHERE ") . "  ro.delivery_time >= {$eta_start} AND ro.delivery_time <= {$eta_end}";
}
 
$delivery = strval($_GET['delivery']);
if  ($delivery)
    $filter_clause .= (strlen($filter_clause)>0? " AND " : " WHERE ") . " ro.delivery = '". $delivery. "'";
	
// if(strlen($filter_clause)>0){
	$sql .=  $filter_clause;
 // }
    
$id = abs(intval($_GET['id'])); 
if ($id) 
    $sql .= " AND `order`.id = {$id}";


$cbday = strval($_GET['cbday']);
$ceday = strval($_GET['ceday']);
$pbday = strval($_GET['pbday']);
$peday = strval($_GET['peday']);
if ($cbday) { 
	$cbtime = strtotime($cbday);
	$sql .= " AND  create_time >= '{$cbtime}'";
}
if ($ceday) { 
	$cetime = strtotime($ceday);
	$sql .= " AND create_time <= '{$cetime}'";
}
if ($pbday) { 
	$pbtime = strtotime($pbday);
	$sql .= " AND pay_time >= '{$pbtime}'";
}
if ($peday) { 
	$petime = strtotime($peday);
	$sql .= " AND pay_time <= '{$petime}'";
}


/* filter */
$uemail = strval($_GET['uemail']);
if ($uemail) {
	$field = strpos($uemail, '@') ? 'email' : 'username';
	$field = is_numeric($uemail) ? 'id' : $field;
	$uuser = Table::Fetch('user', $uemail, $field);
	if($uuser) 
           $sql .= " AND user_id = {$uuser['id']}";
	else 
            $uemail = null;
}

// $sql .= " AND state = 'refund' ORDER BY pay_time DESC";
$sql .= " ORDER BY pay_time DESC";

if ( $_GET['download'] == "1" )
{
    $sql = "SELECT `order`.*,	ro.quantity AS quantity,ro.total  AS origin " . $sql;
    $orders = DB::GetQueryResult($sql, false);
}
else
{
    $count_sql = "SELECT COUNT(*) total " . $sql;
    $count_result = DB::GetQueryResult($count_sql);
    $count = $count_result['total'];
    
  
    list($pagesize, $offset, $pagestring) = pagestring($count, 20);

    $sql = "SELECT `order`.*,	ro.quantity AS quantity,ro.total  AS origin" . $sql . " LIMIT {$pagesize} OFFSET {$offset}";
    $orders = DB::GetQueryResult($sql,false);
}


$order_ids = Utility::Getcolumn($orders,"id");
// $order_items = Table::Fetch('order_item', $order_ids, 'order_id');
// Previously we were searching from `order_item`, but now we will now search from `deleted_order_item` 
$order_items = Table::Fetch('refunded_order_item', $order_ids, 'order_id');

$team_ids = Utility::GetColumn($order_items, 'team_id');
$teams = Table::Fetch('team', $team_ids);

$option_ids =  Utility::GetColumn($order_items, 'option_id');
$options = Table::Fetch('team_multi', $option_ids);
$order_item_ids = Utility::GetColumn($order_items);
//print_r_r($order_item_ids);
$order_items = Utility::AssColumn($order_items, 'order_id', 'id');

$pay_ids = Utility::GetColumn($orders, 'pay_id');
$pays = Table::Fetch('pay', $pay_ids);

$user_ids = Utility::GetColumn($orders, 'user_id');
$users = Table::Fetch('user', $user_ids);

$flows = Table::Fetch('flow', $order_item_ids, 'detail_id');
//print_r_r($flows);
$flows = Utility::AssColumn($flows, 'detail_id');

if ( $_GET['download'] == "1" )
{
    if (!$orders) die('-ERR ERR_NO_DATA');
    //$team = Table::Fetch('team', $team_id);
   // $team = $teams[$team_id]
    $name = 'order_'.date('Ymd');
    $kn = array(
        'id' => 'order No.',
        'pay_id' => 'payment id',
        'service' => 'payment gateway',
        'price' => 'price',
        'quantity' => 'quantity',
        'condbuy' => 'option',
        'fare' => 'fare',
        'origin' => 'total',
        'money' => 'money paid',
        'credit' => 'pay with balance',
        'state' => 'state',
        'remark' => 'notes',
        'delivery' => 'delivery',
        'username' => 'username',
        'useremail' => 'Email',
        'usermobile' => 'mobile',
        'team_id' => 'Deal ID',
        'deal' => 'Deal Title',
        'option' => 'Multi Option',      
        'realname' => 'Contact First Name',
        'lastname' => 'Contact Last Name',
        'mobile' => 'mobile',
        'address' => 'address',
	'address2' => 'address2',
        'zipcode' => 'zipcode',       
        'region' => 'city',
        'name' => 'Recipient Name',
//	'first_name' => 'Recipient First Name',
//	'last_name' => 'Recipient Last Name',
//	'recip_mobile' => 'Recipient Mobile',
//	'recip_email' => 'Recipient Email',
//	'recip_address' => 'Recipient Address',
//	'recip_address2' => 'Recipient Addrss 2,
//	'recip_region' => 'Recipient Region',
//	'recip_zipcode' => 'Recipient Zipcode',
	
        );

    $state = array(
            'unpay' => 'to be paid',
            'pay' => 'paid',
            );
    
    $pay = array(
            'alipay' => 'alipay',
            'tenpay' => 'TenPay',
            'chinabank' => 'ChinaBank',
            'credit' => 'Credit',
            'cash' => 'pay in cash',
            '' => 'other',
            'post' => 'Post Finance', 
            'wire' => 'Wire Transfer',
            'paypal' => 'Paypal',
            'swissbilling' => "Swiss Billing",
            );    
    
    foreach( $orders AS $one ) 
    {
        $oneuser = $users[$one['user_id']];
        
   
        $one['username'] = $oneuser['username'];
        $one['useremail'] = $oneuser['email'];
        $one['usermobile'] = $oneuser['mobile'];
        $one['name'] = $one['realname']. ' '.  $one['lastname'];
        //$one['service'] = $pay[$one['service']];
       
        $one['state'] = $state[$one['state']];
       
        
        $eorders[] = $one;
        foreach( $order_items[$one['id']] as $item)
        {
            $team = $teams[$item['team_id']];
            
            $item['id'] = $one['id'] ."-". $item['id'];
            $item['deal'] = $team['title'];
            $item['option']  = $options[$item['option_id']]['title_fr'];  
            $item['name'] = $item['first_name']. ' '. $item['last_name'];
            //$one['fare'] = ($one['delivery'] == 'express') ? $one['fare'] : 0;
            //$one['price'] = $teams[$one['team_id']]['market_price'];
            
            $eorders[] = $item;
        }
    }
    down_xls($eorders, $kn, $name);
}

$selector = 'pay';
include template('manage_order_refund');
