<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('order');

$now = time();


if ( $_GET['download'] == "1" )
{
    
    $sql_download = " SELECT group_concat(o.id separator '|') AS id, group_concat(oi.id separator '|') AS order_item_ids, count(o.id) AS count, o.pay_time AS pay_time,"
            . " oi.shipment_identcode AS shipment_identcode , oi.shipment_post_service, o.realname, o.lastname,"
            . "oi.first_name, oi.last_name, oi.address, oi.zipcode, oi.region, o.mobile, oi.team_id, oi.option_id, oi.quantity, oi.delivery_time"
            . " FROM `order` o INNER JOIN order_item oi ON o.id = oi.order_id WHERE oi.delivery_status = ".ZOrder::ShipStat_InPrep." AND oi.delivery ='express' AND oi.shipment_identcode <> '' AND oi.shipment_identcode IS NOT NULL"
            . " GROUP BY shipment_identcode ORDER BY o.pay_time";
    $orders = DB::GetQueryResult($sql_download,false);
    
    $team_ids = Utility::GetColumn($orders, 'team_id');
$teams = Table::Fetch('team', $team_ids);

$option_ids =  Utility::GetColumn($orders, 'option_id');
$options = Table::Fetch('team_multi', $option_ids);

    
    
    $filename = 'buyers_list_'.date('Y-m-d H:i');
    
   
    $xls[] = "<html><meta http-equiv=content-type content=\"text/html; charset=UTF-8\"><body>"
            . "<b>Date/Time of Generation</b> : ".date('Y-m-d H:i').'<br><br>'
            . "<table border='1'>";
  
    $xls[] = '<tr>'
            . '<td>Order No.</td>'
            . '<td>Purchase Date</td>'
            . '<td colspan="2">Barcode Number</td>'
            . '<td>Swiss Post Service</td>'
           // . '<td>Buyer Name</td>'
            . '<td>Shipping Name</td>'
            . '<td>Address</td>'
            . '<td>Zip</td>'
            . '<td>City</td>'
            . '<td>Phone Number</td>'
            . '<td>Deal ID</td>'
            . '<td>Deal / Option Name</td>'
            . '<td>Quantity</td>'
            . '<td>ETA</td>'
            . '<td>Shipping date</td>'
            . '</tr>';
    
    //$shipment_codes = array();
   
    $rowspan = 0;
    $shipment_codes_values = array_count_values($shipment_codes);
    
    $shipment_codes_tmp = array();
    foreach($orders as $one){
         if($one['count'] == 1){
            $team = $teams[$one['team_id']];
            $name = $one['realname']. ' '.  $one['lastname'];
            $xls[] = '<tr><td>'.$one['id'].'</td>';
            $xls[].= '<td>'.date('d.m.Y', $one['pay_time']).'</td>';
            if($one['shipment_identcode']){
               
                    $xls[] .='<td colspan="2">['.$one['shipment_identcode'].']</td>';
                    $xls[] .='<td valign="top">'.$one['shipment_post_service'].'</td>';
                    
            }
            else{
                $xls[] .='<td colspan="2">-</td>';
                $xls[] .='<td >-</td>';

            }
            $xls[] .= '<td>'.$name.'</td>'
                   // .'<td>'.$one['first_name'].' '.$one['last_name'].'</td>'
                    . '<td>'.$one['address'].'</td>'
                    .'<td>'.$one['zipcode'].'</td>'
                    .'<td>'.$one['region'].'</td>'
                    .'<td>'.$one['mobile'].'</td>'
                    .'<td>'.$one['team_id'].'</td>';
            
            if($one['option_id']){
                 $xls[] .= '<td>'.strip_tags($options[$one['option_id']]['summary']).'</td>';
            }
            else{
                 $xls[] .= '<td>'.$team['title'].'</td>';
            }
                    
            $xls[] .= '<td>'.$one['quantity'].'</td>';
            $xls[].= '<td valign="top">'.date('d.m.Y', $one['delivery_time']).'</td>';

            $xls[] .= '<td>&nbsp;</td>';
            $xls[] .= '</tr>';
            $xls[] = '<tr><td colspan="15" bgcolor="#d9d9d9">&nbsp;</td></tr>';
         }
         else{
             $ids = explode('|', $one['id']);
             $order_item_ids = explode('|', $one['order_item_ids']);
             
             for($i =0; $i < $one['count']; $i ++)
             {
                $order_i = Table::Fetch('order', $ids[$i]);
                $order_item_i = Table::Fetch('order_item', $order_item_ids[$i]);
                
                
                $team_i =  Table::Fetch('team', $order_item_i['team_id']);
                if($order_item_i['option_id']){
                    $option_i  = Table::Fetch('team_multi', $order_item_i['option_id']);
                }
                
                $xls[] = '<tr><td>'.$ids[$i].'</td>';
                $xls[].= '<td>'.date('d.m.Y', $one['pay_time']).'</td>';
                
                $xls[] .='<td colspan="2">['.$one['shipment_identcode'].']</td>';
                if($i == 0){
                    $xls[] .='<td valign="top" rowspan="'.$one['count'].'">'.$one['shipment_post_service'].'</td>';
                }    
                $name = $order_i['realname']. ' '.  $order_i['lastname'];
                 $xls[] .= '<td>'.$name.'</td>'
                    //.'<td>'.$order_item_i['first_name'].' '.$order_item_i['last_name'].'</td>'
                    . '<td>'.$order_item_i['address'].'</td>'
                    .'<td>'.$order_item_i['zipcode'].'</td>'
                    .'<td>'.$order_item_i['region'].'</td>'
                    .'<td>'.$order_i['mobile'].'</td>'
                    .'<td>'.$order_item_i['team_id'].'</td>';
                
                if($order_item_i['option_id']){
                 $xls[] .= '<td>'.strip_tags($option_i['summary']).'</td>';
                }
                else{
                     $xls[] .= '<td>'.$team_i['title'].'</td>';
                }
                
                $xls[] .= '<td>'.$order_item_i['quantity'].'</td>';
                 if($i == 0){ 
                            $xls[].= '<td valign="top" rowspan="'.$one['count'].'">'.date('d.m.Y', $order_item_i['delivery_time']).'</td>';
                 }
            $xls[] .= '<td>&nbsp;</td>';
            $xls[] .= '</tr>';
                
                
             }
             $xls[] = '<tr><td colspan="15" bgcolor="#d9d9d9">&nbsp;</td></tr>';
         }
    }
    $xls[] = '</table></body></html>';
	$xls = join("\r\n", $xls);
        
    header('Content-Disposition: attachment; filename="'.$filename.'.xls"');
    
    die(mb_convert_encoding($xls,'UTF-8','UTF-8'));
    //exit;
}


$sql = " FROM `order` WHERE id IN (
    SELECT DISTINCT order_id FROM order_item ";


    
$team_id = abs(intval($_GET['team_id']));
if ( $team_id )
       $filter_clause = (strlen($filter_clause)>0? " AND " : " WHERE ") . " order_item.team_id = {$team_id} ";
            
        
$ship_status = abs(intval($_GET['ship_status']));
if ($ship_status ){
    $filter_clause .= (strlen($filter_clause)>0? " AND " : " WHERE ") . "  order_item.delivery_status = {$ship_status}";
}else{
    $ship_status = ZOrder::ShipStat_InPrep;
    $filter_clause .= (strlen($filter_clause)>0? " AND " : " WHERE ") . "  order_item.delivery_status = {$ship_status}";
}
/*
$ship_eta = strval($_GET['ship_eta']);
if ($ship_eta){
    $etatime = strtotime($ship_eta);
    $eta_start = mktime(0,0,0, date("n",$etatime), date("j", $etatime), date("Y", $etatime));
    $eta_end = mktime(23,59,59, date("n",$etatime), date("j", $etatime), date("Y", $etatime));
    $filter_clause .= (strlen($filter_clause)>0? " AND " : " WHERE ") . "  order_item.delivery_time >= {$eta_start} AND order_item.delivery_time <= {$eta_end}";
}
 */
$delivery = 'express';
if  ($delivery)
    $filter_clause .= (strlen($filter_clause)>0? " AND " : " WHERE ") . " order_item.delivery = '". $delivery. "'";

$barcode = strval($_GET['barcode']);

if($barcode){
     $filter_clause .= (strlen($filter_clause)>0? " AND " : " WHERE ") . " order_item.shipment_identcode LIKE '". $barcode. "'";
}

$sql .= $filter_clause. " ORDER BY shipment_identcode) ";
        
    
$id = abs(intval($_GET['id'])); 
if ($id) 
    $sql .= " AND `order`.id = {$id}";

/*
$cbday = strval($_GET['cbday']);
$ceday = strval($_GET['ceday']);
*/
$pbday = strval($_GET['pbday']);
$peday = strval($_GET['peday']);
/*if ($cbday) { 
	$cbtime = strtotime($cbday);
	$sql .= " AND  create_time >= '{$cbtime}'";
}
if ($ceday) { 
	$cetime = strtotime($ceday);
	$sql .= " AND create_time <= '{$cetime}'";
}*/
if ($pbday) { 
	$pbtime = strtotime($pbday);
	$sql .= " AND pay_time >= '{$pbtime}'";
}
if ($peday) { 
	$petime = strtotime($peday);
	$sql .= " AND pay_time <= '{$petime}'";
}

/* filter */
$uemail = strval($_GET['uemail']);
if ($uemail) {
	$field = strpos($uemail, '@') ? 'email' : 'username';
	$field = is_numeric($uemail) ? 'id' : $field;
	$uuser = Table::Fetch('user', $uemail, $field);
	if($uuser) 
           $sql .= " AND user_id = {$uuser['id']}";
	else 
            $uemail = null;
}

$sql .= " AND state = 'pay'";
$count_sql = "SELECT COUNT(*) total " . $sql;
$count_result = DB::GetQueryResult($count_sql);
$count = $count_result['total'];


list($pagesize, $offset, $pagestring) = pagestring($count, 20);
 
 $sql .= ' ORDER BY pay_time DESC';
$sql = "SELECT * " . $sql . " LIMIT {$pagesize} OFFSET {$offset}";
$orders = DB::GetQueryResult($sql,false);




$order_ids = Utility::Getcolumn($orders,"id");
$order_items = Table::Fetch('order_item', $order_ids, 'order_id');

$team_ids = Utility::GetColumn($order_items, 'team_id');
$teams = Table::Fetch('team', $team_ids);

$option_ids =  Utility::GetColumn($order_items, 'option_id');
$options = Table::Fetch('team_multi', $option_ids);

$order_items = Utility::AssColumn($order_items, 'order_id', 'id');


$pay_ids = Utility::GetColumn($orders, 'pay_id');
$pays = Table::Fetch('pay', $pay_ids);

$user_ids = Utility::GetColumn($orders, 'user_id');
$users = Table::Fetch('user', $user_ids);


$selector = 'pay';
include template('manage_order_generate_code');
