<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('order');

$sql = 'SELECT `order`.team_id, team.title As deal, option_id, team_multi.title_fr As "option title", 
if ( option_id is not null, team_multi.max_number, team.max_number) as max, sum(quantity) as total, 
if ( option_id is not null, team_multi.team_price, team.team_price) as price from `order` 
left join `team` ON team.id = team_id left join team_multi On team_multi.id = option_id 
Where `order`.state = "pay" ';

$sql = 'SELECT `order_item`.team_id, team.title As deal, `order_item`.option_id, team_multi.title_fr As "option title", 
if ( `order_item`.option_id is not null, team_multi.max_number, team.max_number) as max, sum(`order_item`.quantity) as total, 
if ( `order_item`.option_id is not null, team_multi.team_price, team.team_price) as price from `order_item` 
left join `order` ON `order`.id = `order_item`.order_id 
LEFT JOIN `team` ON team.id = `order_item`.team_id 
left join team_multi On team_multi.id = `order_item`.option_id 
Where `order`.state = "pay" ';

$team_id = abs(intval($_GET['team_id']));
if ($team_id )
    $sql .= " AND `order_item`.team_id = {$team_id}";

    $cbday = strval($_GET['cbday']);
$ceday = strval($_GET['ceday']);
$pbday = strval($_GET['pbday']);
$peday = strval($_GET['peday']);
if ($cbday) { 
	$cbtime = strtotime($cbday);
	$sql .= " AND  order.create_time >= '{$cbtime}'";
}
if ($ceday) { 
	$cetime = strtotime($ceday);
	$sql .= " AND order.create_time <= '{$cetime}'";
}
if ($pbday) { 
	$pbtime = strtotime($pbday);
	$sql .= " AND order.pay_time >= '{$pbtime}'";
}
if ($peday) { 
	$petime = strtotime($peday);
	$sql .= " AND pay_time <= '{$petime}'";
}

$uemail = strval($_GET['uemail']);
if ($uemail) {
	$field = strpos($uemail, '@') ? 'email' : 'username';
	$field = is_numeric($uemail) ? 'id' : $field;
	$uuser = Table::Fetch('user', $uemail, $field);
	if($uuser) 
           $sql .= " AND order.user_id = {$uuser['id']}";
	else 
            $uemail = null;
}

$id = abs(intval($_GET['id'])); 
if ($id) 
    $sql .= " AND `order`.id = {$id}";

$sql .= " group by order_item.team_id, order_item.option_id order by order_item.team_id desc";

$result = DB::GetQueryResult($sql, false);


if ( $_GET['download'] == "1" )
{
    if (!$result) die('-ERR ERR_NO_DATA');
   
    $name = 'inventory_'.date('Ymd');
    $kn = array(
        'team_id' => 'Deal ID', 
        'deal' => 'Deal Title',
        'option_id' => 'Option ID',
        'option title' => 'Multi Option',      
        'max' => 'Max Limit',
        'total' => 'Total Orders',
        'available' => 'Available',
        'price' => 'price',
        'money' => 'total money'
        );

  
    
    foreach( $result AS $one ) {      
       $one['available'] = $one['max'] > 0 ? ($one['max'] - $one['total']) : "";
       $one['money'] = $one['price'] * $one['total'];
       $eorders[] = $one;
    }
    down_xls($eorders, $kn, $name);
}



include template('manage_inventory');

?>
