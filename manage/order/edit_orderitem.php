<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('order');

$id = intval($_GET['id']);
global $login_user_id;

if(is_post()){
    
    $order_item_id = $_POST['order_item_id'];
    
   //$order_id = $_POST['order_id'];
    
    
    $order_item  = Table::Fetch('order_item', $order_item_id);
     $order = Table::Fetch('order', $order_item['order_id']);
     
     
    $update_arr = array(
                    'first_name' => $_POST['realname'],
                    'last_name'  => $_POST['last_name'],
                    'mobile'  => $_POST['mobile'],
                    'address'  => $_POST['address'],
                    'address2'  => $_POST['address2'],
                    'region'  => $_POST['region'],
                    'zipcode'  => $_POST['zipcode'], 
                    'delivery' => $_POST['shipment_type']
                );
    
    
    # If order option is change due to mind change in size, color etc
    # It's mean we can have different price to charge to customer
    # either more or less, let's see
    if(isset($_POST['option_id']) && $order_item['option_id'] != intval($_POST['option_id'])){
 
		$update_arr['option_id'] = $_POST['option_id'];
        
		$update_option = Table::Fetch('team_multi', $_POST['option_id']);
       
		$total_change = 0;
       
       //echo 'now price='.$update_option['team_price'].'</br>';
       //echo 'previous price='.$order_item['price'].'</br>';
       
//        echo '<pre>';
//        print_r($_POST);
//        print_r($order);
//        print_r($order_item);
//        print_r($update_option);
       
       # If price is difference, let's do some calculation
       if($update_option['team_price'] != $order_item['price']  )
       {
           
       		# Create new order option
           $update_option_order = array();
           $update_option_order['price'] =   $update_option['team_price'];
           //$update_option_order['total'] = $order_item['quantity'] * intval($update_option['team_price']);
//            $update_option_order['origin'] = ($order_item['quantity'] * intval($update_option['team_price'])) + intval($order['fare']);
           # New price
           $new_money  = $new_origin = ($order_item['quantity'] * intval($update_option['team_price'])) + intval($order['fare']);
           
           $update_option_order['money'] 	= $new_money;
           $update_option_order['origin'] 	= $new_origin;
            
           $store_credit = 0;
           # Increase order credit as new order price will be less so we increased credit
           $store_credit 	= ($order['origin'] - $new_origin);
            
           $user_info = Table::Fetch('user', $order['user_id']);
            
           
           # If new item price is less than 
           if($update_option['team_price'] < $order_item['price'])
           {
				
               
// 				print_r('$diff1='.$diff);
// 				print_r('$$store_credit='.$store_credit);
// 				print_r($update_option_order);
// 				exit;
              	
				Table::UpdateCache('user', $order['user_id'], array('money' => array( "money + {$store_credit}" ),));
				Table::UpdateCache('order', $order['id'], $update_option_order);
				
				$u = array(
						'user_id' => $order['user_id'],
						'admin_id' => $login_user_id,
						'money' => $store_credit,
						'direction' => 'income',
						'action' => 'store',
						'detail' => 'Add store credit in customer account due to change order item in less price'. "Option changed (Order: {$order['id']})|From:{$order_item['option_id']}|To:{$_POST['option_id']}",
						'detail_id' => $order['id'],
						'create_time' => time(),
				);
				DB::Insert('flow', $u);
				
           }
           else if($update_option['team_price'] > $order_item['price'] ){
               
               $method_additional_cost = $_POST['method_additional_cost'];
               
                
                if($method_additional_cost == 'user_balance_cash' ){
                    
                	//$update_option_order['credit'] = ($order_item['quantity'] * intval($update_option['team_price'])) + intval($order['fare']);  
//                     $diff = $update_option_order['credit'] -  $order['credit'];
                	
                	$credit = $store_credit;
                	if($store_credit  < 0)
	                	$credit = $store_credit * -1;
                	$update_option_order['credit'] = $credit;
                	
//                     print_r('$diff2='.$diff);
// 					print_r('$store_credit='.$store_credit);
//                     print_r($update_option_order);
//                     exit;
                    
//                 	echo '<pre>';
//                 	print_r($user_info);
//                 	exit;
                	# Stop negative user's store credit
                	if($user_info['money'] >= $credit){
                		
	                    Table::UpdateCache('user', $order['user_id'], array(
	                                     'money' => array( "money - {$credit}" ),
	                                 ));
	                     
	                      //$detail = 'Fare Amount Due to change in delivery type, Paid from User balance.';
	                    $u = array(
	                    		'user_id' => $order['user_id'],
	                    		'admin_id' => $login_user_id,
	                    		'money' => $credit,
	                    		'direction' => 'expense',
	                    		'action' => 'refund',
	                    		'detail' => 'Substract store credit from customer account due to change order item in high price'. "Option changed (Order: {$order['id']})|From:{$order_item['option_id']}|To:{$_POST['option_id']}",
	                    		'detail_id' => $order['id'],
	                    		'create_time' => time(),
	                    );
	                    DB::Insert('flow', $u);
                	
                	}
                    
                }
                else if($method_additional_cost == 'cash' )
                {
                    $update_option_order['money'] = $order_item['quantity'] * intval($update_option['team_price']);
                    //$detail = 'Fare Amount Due to change in delivery type, Paid In Cash.';
                }
                else if($method_additional_cost == 'wire' )
                {
                    $update_option_order['money'] = $order_item['quantity'] * intval($update_option['team_price']);
                     //$detail = 'Fare Amount Due to change in delivery type, Paid by wire.';
                }                
				else if($method_additional_cost == 'maestro' )
                {
                    $update_option_order['money'] = $order_item['quantity'] * intval($update_option['team_price']);
                     //$detail = 'Fare Amount Due to change in delivery type, Paid by maestro.';
                }
                
//                 print_r('$diff='.$diff);
//                 print_r($update_option_order);
//                 exit;
                
                Table::UpdateCache('order', $order['id'], $update_option_order);
               
           }
           
           $update_option_order_item = array();
           $update_option_order_item['total'] = ($order_item['quantity'] * intval($update_option['team_price'])) + intval($order_item['fare']);
           $update_option_order_item['price'] = $update_option['team_price'];
           Table::UpdateCache('order_item', $order_item['id'], $update_option_order_item);
           
           $team_multi_update = array();
           $team_multi_update["stock_count"] = array( "`stock_count` - {$order_item['quantity']}");
           $team_multi_update["now_number"] = array( "`now_number` - {$order_item['quantity']}");
           Table::UpdateCache('team_multi', $update_option['id'], $team_multi_update);  
           
           
           
           /*
            * $prev_team_multi = Table::Fetch('team_multi', $order_item['option_id']);
           $prev_team_multi_update = array();
           $prev_team_multi_update['stock_count'] = array( "`stock_count` + {$order_item['quantity']}");
           $prev_team_multi_update['now_number'] = array( "`now_number` + {$order_item['quantity']}");
           Table::UpdateCache('team_multi', $order_item['option_id'], $prev_team_multi_update);   
           */
          
           
           $order =  Table::FetchForce('order', $order['id'] );
           $order_item = Table::FetchForce('order_item', $order_item['id'] );
          
       }
       
    }
    
//     echo '$_POST[shipment_type]='. $_POST['shipment_type'];
//     echo ZOrder::ShipStat_InPrep. $order_item['delivery_status'];
//     exit;
    
       if($_POST['shipment_type'] == 'pickup'){
           
           if($order_item['delivery_status']==ZOrder::ShipStat_InPrep){
               $update_arr['delivery_status'] = ZOrder::ShipStat_Available;
           }
           
           $team_info = Table::Fetch('team', $order_item['team_id']);
           
           if($order_item['delivery'] =='express')
           {
                $update_arr['fare'] = 0;
                $update_arr['total'] =  $order_item['total'] - $order_item['fare'];
               
                $o_update_arr['fare'] = $order['fare'] - $order_item['fare'];
                $o_update_arr['origin'] = $order['origin'] - $order_item['fare'];
                $o_update_arr['credit'] = $order['credit'] - $order_item['fare'];
                
                Table::UpdateCache('user', $order['user_id'], array(
                                          'money' => array( "money + {$team_info['fare']}" ),
                                      ));
                                  
                Table::UpdateCache('order', $order['id'], $o_update_arr);

                $u = array(
                                 'user_id' => $order['user_id'],
                                 'admin_id' => $login_user_id,
                                 'money' => $order_item['fare'],
                                 'direction' => 'expense',
                                 'action' => 'refund',
                                 'detail' => 'Fare Amount Refund due to change in delivery type',
                                 'detail_id' => $order['id'],
                                 'create_time' => time(),
                                 );
                  DB::Insert('flow', $u);
               
           }
           
        }
       
        if($_POST['shipment_type'] == 'express'){
                if($order_item['delivery'] =='pickup')
                {
                    if($order_item['delivery_status']==ZOrder::ShipStat_Available){
                        $update_arr['delivery_status'] = ZOrder::ShipStat_InPrep;
                    }
               
              
                    $team_info = Table::Fetch('team', $order_item['team_id']);
                    
                    
                    //Deleting coupon 
                    $flag_coupon_delete = Table::Delete('coupon', $order_item_id, 'order_id');
                    
                    
                    $update_arr['fare'] = $team_info['fare'];
                    $update_arr['total'] =  $order_item['total'] + $team_info['fare'];
                    
                    $o_update_arr['fare'] = $order['fare'] + $team_info['fare'];
                    $o_update_arr['origin'] = $order['origin'] + $team_info['fare'];
                    //

                    

                    $method_additional_cost = $_POST['method_additional_cost'];
                    if($method_additional_cost == 'user_balance_cash' ){
                        
                        
                        Table::UpdateCache('user', $order['user_id'], array(
                                         'money' => array( "money - {$team_info['fare']}" ),
                                     ));
                                         
                                        
                          $o_update_arr['credit'] = $order['credit'] + $team_info['fare'];  
                          $detail = 'Fare Amount Due to change in delivery type, Paid from User balance.';
                    }
                    else if($method_additional_cost == 'cash' )
                    {
                        $o_update_arr['money'] = $order['money'] + $team_info['fare'];
                        $detail = 'Fare Amount Due to change in delivery type, Paid In Cash.';
                    }
                    else if($method_additional_cost == 'wire' )
                    {
                        $o_update_arr['money'] = $order['money'] + $team_info['fare'];
                         $detail = 'Fare Amount Due to change in delivery type, Paid by wire.';
                    }
					else if($method_additional_cost == 'maestro' )
					{
						 $o_update_arr['money'] = $order['money'] + $team_info['fare'];
                         $detail = 'Fare Amount Due to change in delivery type, Paid by maestro.';
					}

                    Table::UpdateCache('order', $order['id'], $o_update_arr);

                    # Stop User's store credit in negative
                    if($user_info['money'] >= $team_info['fare']){
                    
	                     $u = array(
	                                     'user_id' => $order['user_id'],
	                                     'admin_id' => $login_user_id,
	                                     'money' => $team_info['fare'],
	                                     'direction' => 'income',
	                                     'action' => 'store',
	                                     'detail' => $detail,
	                                     'detail_id' => $order['id'],
	                                     'create_time' => time(),
	                                     );
	                      DB::Insert('flow', $u);
                    
                    }
                    
                }
         }
    
   
     Table::UpdateCache('order_item', $order_item_id, $update_arr);
   
   
    if(isset($_POST['quantity']) && $order_item['quantity'] != intval($_POST['quantity']) ){
        
       
             $quantity = $order_item['quantity'] - intval($_POST['quantity']);
             
             if($_POST['quantity'] == 0){
                 
                 Session::Set('error', 'Please refund Order item');
                 redirect( WEB_ROOT . "/manage/order/edit_orderitem.php?id=".$order_item_id);
             }
             if($_POST['quantity'] > $order_item['quantity']){
                 
                 Session::Set('error', 'Order Quantity cant be increased, Please refund and place new order');
                 redirect( WEB_ROOT . "/manage/order/edit_orderitem.php?id=".$order_item_id);
             }
             
             
              if ( $order['state'] =='pay' && $order['origin'] > 0 ) {
        
                        $total_refund = ZFlow::RefundOrderItem($order_item, $order, $reason_refund, $quantity);

                        $order_item_update_arr = array('quantity' => array( "quantity - {$quantity}" ), 'total' => array( "total - {$total_refund}" ));
                        Table::UpdateCache('order_item', $order_item['id'], $order_item_update_arr);    
                        $order_update_arr = array(
                                        'origin' => array( "origin - {$total_refund}" ),
                                        'quantity' => array( "quantity - {$quantity}")        
                                );

                        if($order['credit']){
                            $order_update_arr['credit'] = array( "credit - {$total_refund}");
                        }             
                        else if($order['service']!='credit'&&$order['money'])
                        {
                            $order_update_arr['money'] = array( "money - {$total_refund}");
                        }

                        Table::UpdateCache('order', $order['id'], $order_update_arr);
                }
            
    }
    
   
    
    
    
    //if its an option then deduct option 
    if($order_item['option_id'] ){
        $option = Table::Fetch('team_multi', $order_item['option_id']);


        if($option){
                if($quantity){
                    $minus = $quantity;
                }
                else{
                    $minus = $order_item['quantity'];
                }
                $condition_update = array(
                            'now_number' => array( "now_number - {$minus}" )
                );
                //     Deduct Stock                      
                if( $order_item['delivery'] != 'coupon' && $order_item['delivery_status'] != ZOrder::ShipStat_Invalid && $order_item['delivery_status'] != ZOrder::ShipStat_Waiting  )                           
                {
                   $condition_update["stock_count"] = array( "`stock_count` + {$minus}");
                }                         
                Table::UpdateCache('team_multi', $option['id'], $condition_update);    

        }
    }
    else{
        $team = Table::Fetch('team', $order_item['team_id']);

        if($team){
            team_state($team);
            if ( $team['state'] != 'failure' ) {
                    if($quantity){
                        $minus = $quantity;
                    }
                    else{
                        $minus = $order_item['quantity'];
                    }

                    $condition_update = array(
                            'now_number' => array( "now_number - {$minus}" )
                    );
                    //     Deduct Stock                      
                    if( $order_item['delivery'] != 'coupon' && $order_item['delivery_status'] != ZOrder::ShipStat_Invalid && $order_item['delivery_status'] != ZOrder::ShipStat_Waiting  )                           
                    {
                       $condition_update["stock_count"] = array( "`stock_count` + {$minus}");
                    }                         
                    Table::UpdateCache('team', $team['id'], $condition_update);                        
            }
        }
    }
    
    //Lastly Updating Order 
    
   redirect( WEB_ROOT . "/manage/order/edit_orderitem.php?id=".$order_item_id);
    
}

$order_item = Table::FetchForce('order_item', $id);

$order = Table::FetchForce('order', $order_item['order_id']);

//print_r_r($order);
//print_r_r($order_item);

$paystate = array(
		'unpay' => '<font color="green">Unpaid</font>',
		'pay' => '<font color="red">Paid</font>',
	);

$team = Table::Fetch('team', $order_item['team_id']);

if($order_item['option_id']){
    
    //$options = Table::Fetch('team_multi', $order_item['team_id'], 'team_id');
    $sql_option = "SELECT * FROM team_multi where team_id = ".$order_item['team_id'];
    $options = DB::GetQueryResult($sql_option, false);
    
    $options_arr = array();
    foreach($options as $op){
        $options_arr[$op['id']] = $op['title'];
    }
    
}


include template('manage_order_edit_orderitem');