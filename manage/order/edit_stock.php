<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('order');
$id = intval($_GET['id']);


if (is_post()) {
    
    $add_stock = intval($_POST['add_stock']);
    $type = $_POST['type'];
    
    $stock_history = array();
    $stock_history['user_id'] = $login_user_id;
    $stock_history['create_time'] = time();
    $stock_history['quantity'] = $add_stock;
    $team_option_id = $_POST['team_option_id'];
    
    /*
     * previous page redirection
     */
    if(isset($_POST['page']))
    {
        $build_query = http_build_query(unserialize(urldecode($_POST['page'])));
        if($build_query){
            $page = '?'.http_build_query(unserialize(urldecode($_POST['page'])));
        }
        else{
            $page = '';
        }
        
    }
    else{
        $page = '';
    }
    
    if($type =='deal')
    {
        $team_option = Table::Fetch('team', $id);
        $condition = array('team_id' => $team_option['id']);
        $order_item_count = Table::Count('order_item', $condition  );
        $stock_history['team_id'] = $team_option_id;
    }
    else if($type == 'option'){
        $team_option = Table::Fetch('team_multi', $id);
        $condition = array('option_id' => $team_option['id']);
        $order_item_count = Table::Count('order_item', $condition  );
        $stock_history['option_id'] = $team_option_id;
    }
    
    if( $add_stock + intval($team_option['stock_count']) < 0){
            Session::Set('error', 'Stock is going less than 0');
            
           include template('manage_edit_order_stock');
    }
    else{
        $condition_order_item = array();  
         DB::Insert('stock_history', $stock_history);
         if($type =='deal'){
             Table::UpdateCache('team', $team_option_id, array("stock_count" => array( "`stock_count` + {$add_stock}")));
             
             // Table::UpdateCache('team', $team_option_id, array('stock_count' => array('stock_count+'.$add_stock)) );
              $sql_order_item = "SELECT oi.*, o.pay_time FROM order_item oi INNER JOIN `order` o ON o.`id`= oi.`order_id` 
                                WHERE o.`state`='pay' AND 
                                (oi.`delivery` = 'pickup' OR oi.`delivery` = 'express') 
                                AND oi.`delivery_status`= ".ZOrder::ShipStat_Waiting." AND oi.team_id=".$team_option_id;
         }
         elseif($type == 'option'){
             Table::UpdateCache('team_multi', $team_option_id, array("stock_count" =>array( "`stock_count` + {$add_stock}")));
             
            //  Table::UpdateCache('team_multi', $team_option_id, array('stock_count' => array('stock_count+'.$add_stock)) );
             //DB::Update('team_multi', $team_option_id, array('stock_count' => array('stock_count+'.$add_stock)) );
             $sql_order_item = "SELECT oi.*, o.pay_time FROM order_item oi INNER JOIN `order` o ON o.`id`= oi.`order_id` 
                                WHERE o.`state`='pay' AND 
                                (oi.`delivery` = 'pickup' OR oi.`delivery` = 'express') 
                                AND oi.`delivery_status`= ".ZOrder::ShipStat_Waiting." AND oi.option_id=".$team_option_id;
             $team = Table::Fetch('team', $team_option['team_id']);
         }
         
        $order_items = DB::GetQueryResult($sql_order_item, false);
        $total_stock_count = intval($team_option['stock_count']) + $add_stock;
        $eta_add  =  isset($team['delivery_eta'])?$team['delivery_eta']:$team_option['delivery_eta'];
         
        
        foreach($order_items as $oi){

             if($total_stock_count >= $oi['quantity'])
             {
                 if( isset($eta_add) && ($eta_add > 0) && $oi['delivery'] == 'express' ){
                     $eta = $oi['pay_time'] + ($eta_add*86400);
                 }
                 else{
                     $eta = 0;
                 }
                 if(ZOrder::UpdateShippingState( ZOrder::ShipStat_Available, $oi['id'], $eta) === TRUE){
                     $total_stock_count = $total_stock_count - $oi['quantity'];
                 }
             }
        }
        
        if($type =='deal')
        {
            $msg = 'Deal No.'.$team_option['id'].', Deal Title:'.$team_option['title'].'<br />';
        }
        else if($type == 'option'){
             $msg = 'Deal No.'.$team_option['team_id'].', Option No.'.$team_option['id'].', Option Title:'.$team_option['title'].'<br />';
        }
        
		//UPDATE EMAIL SENT STATUS
		 if($type =='deal')
        {
			
			$subTeamId	=	$team_option['id'];	
			
            $sqlUpdateStockStatus		=	"UPDATE supplier_mail_order_tracking SET is_stock_update = 1 WHERE team_id = '$subTeamId' AND option_id = 0";
			DB::Query($sqlUpdateStockStatus);
					 
        }
        else if($type == 'option'){
			
			$subTeamId		=	$team_option['team_id'];
			$subOptionId	=	$team_option['id'];		
			
			$sqlUpdateStockStatus		=	"UPDATE supplier_mail_order_tracking SET is_stock_update = 1 WHERE team_id = '$subTeamId' AND option_id = '$subOptionId'";
			DB::Query($sqlUpdateStockStatus);
        }
		
        Session::Set('notice', $msg.'Stock Updated Successfully');
        redirect( WEB_ROOT . "/manage/order/stock_status.php".$page);
    }
     
}
else{
    $type = strval($_GET['type']);
           
    if($type =='deal'){
        $team_option = Table::Fetch('team', $id);
        
        $sql_count = "SELECT SUM(oi.quantity) AS order_item_count FROM order_item oi 
                        INNER JOIN `order` o ON o.id = oi.order_id 
                        WHERE oi.team_id = {$team_option['id']} AND o.state = 'pay'";
        $order_count = DB::GetQueryResult($sql_count, true);
        $order_item_count = $order_count['order_item_count'];
    }
    else if($type == 'option'){
        $team_option = Table::Fetch('team_multi', $id);
        $team =  Table::Fetch('team', $team_option['team_id']);
        $sql_count = "SELECT SUM(oi.quantity) AS order_item_count FROM order_item oi 
                        INNER JOIN `order` o ON o.id = oi.order_id 
                        WHERE oi.option_id = {$team_option['id']} AND o.state = 'pay'";
                        
        $order_count = DB::GetQueryResult($sql_count, true);
        $order_item_count = $order_count['order_item_count'];
        
    }
    
}

include template('manage_edit_order_stock');