<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('order');

$action = strval($_GET['action']);

if ( $action == "ordercash" && isset($_POST['id']))
{
    foreach( $_POST['id'] as $id )
    {
        $order = Table::Fetch('order', $id);
        ZOrder::CashIt($order);
    }
}
else if ( $action == "ordercancel"  && isset($_POST['id']) )
{
    foreach( $_POST['id'] as $id )
    {
        $order = Table::Fetch('order', $id);
	if ( ZOrder::Cancel($order) )
        {
            $user = Table::Fetch('user', $order['user_id']);
            mail_order_cancel($user,$order);
        }
    }
}

 Utility::Redirect(WEB_ROOT . '/manage/order/unpay.php');

?>
