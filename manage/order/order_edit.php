<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('order');

if(is_post()){
    
    $order_id = $_POST['order_id'];
    Table::UpdateCache('order', $order_id, array(
                    'realname' => $_POST['realname'],
                    'lastname'  => $_POST['last_name'],
					'email'		=> $_POST['email'],
                    'mobile'  => $_POST['mobile'],
                    'address'  => $_POST['address'],
                    'address2'  => $_POST['address2'],
                    'region'  => $_POST['region'],
                    'zipcode'  => $_POST['zipcode'],        
                ));
     redirect( WEB_ROOT . "/manage/order/order_edit.php?id=".$order_id);
}

if(isset($_GET['refund_order_item']) && isset($_GET['from_refund'])){
    
    $order_item_id = intval($_GET['refund_order_item']);
    $order_id = intval($_GET['order_id']);
    $order_item  = Table::Fetch('order_item', $order_item_id);
    $order = Table::Fetch('order', $order_id);
    $reason_refund = $_GET['reason_refund'];
    $rid = strtolower(strval($_GET['rid']));
   
    if ( $order['state'] =='pay' && $order['origin'] > 0 ) {
        
        $total_refund = ZFlow::RefundOrderItem($order_item, $order, $reason_refund, $quantity);
        $order_update_arr = array(
                            'origin' => array( "origin - {$total_refund}" ),
                            'quantity' => array( "quantity - {$order_item['quantity']}")        
                    );
        Table::UpdateCache('order', $order['id'], $order_update_arr);
    }
    
    
    
    //if its an option then deduct option 
    if($order_item['option_id'] ){
        $option = Table::Fetch('team_multi', $order_item['option_id']);


        if($option){
                if($quantity){
                    $minus = $quantity;
                }
                else{
                    $minus = $order_item['quantity'];
                }
                $condition_update = array(
                            'now_number' => array( "now_number - {$minus}" )
                );
                //     Deduct Stock                      
                if( $order_item['delivery'] != 'coupon' && $order_item['delivery_status'] != ZOrder::ShipStat_Invalid && $order_item['delivery_status'] != ZOrder::ShipStat_Waiting  )                           
                {
                   $condition_update["stock_count"] = array( "`stock_count` + {$minus}");
                }                         
                Table::UpdateCache('team_multi', $option['id'], $condition_update);    

        }
    }
    else{
        $team = Table::Fetch('team', $order_item['team_id']);

        if($team){
            team_state($team);
            if ( $team['state'] != 'failure' ) {
                    if($quantity){
                        $minus = $quantity;
                    }
                    else{
                        $minus = $order_item['quantity'];
                    }

                    $condition_update = array(
                            'now_number' => array( "now_number - {$minus}" )
                    );
                    //     Deduct Stock                      
                    if( $order_item['delivery'] != 'coupon' && $order_item['delivery_status'] != ZOrder::ShipStat_Invalid && $order_item['delivery_status'] != ZOrder::ShipStat_Waiting  )                           
                    {
                       $condition_update["stock_count"] = array( "`stock_count` + {$minus}");
                    }                         
                    Table::UpdateCache('team', $team['id'], $condition_update);                        
            }
        }
    }
    
    //Lastly Updating user balance 
    
    
    if ( $rid == 'credit' && $order['state'] =='pay' ) {
                $user = Table::Fetch('user', $order['user_id']);
		Table::UpdateCache('user', $order['user_id'], array(
					'money' => array( "money + {$order_item['credit']}" ),
		));
    }
	
    DB::Delete('order_item', array('id'=> $order_item['id'])); 
	
    exit;
    ///redirect( WEB_ROOT . "/manage/order/order_edit.php?id=".$order_id);
}



if(isset($_GET['refund_order_item'])){
    
    $order_item_id = intval($_GET['refund_order_item']);
    $order_id = intval($_GET['order_id']);
    $order_item  = Table::Fetch('order_item', $order_item_id);
    $order = Table::Fetch('order', $order_id);
    $reason_refund = $_GET['reason_refund'];
    $rid = strtolower(strval($_GET['rid']));
   
   
   
    if(isset($_GET['quantity'])){
             $quantity = $_GET['quantity'];
             if($quantity == 0){
                 
                 Session::Set('error', 'Please refund Order item');
                 redirect( WEB_ROOT . "/manage/order/order_edit.php?id=".$order_id);
             }
             if($quantity > $order_item['quantity']){
                 
                 Session::Set('error', 'Order Quantity cant be increased, Please refund and place new order');
                 redirect( WEB_ROOT . "/manage/order/order_edit.php?id=".$order_id);
             }
             
             
    }else{
        $quantity = FALSE;
    }
    
    
    if ( $order['state'] =='pay' && $order['origin'] > 0 ) {
        
        $total_refund = ZFlow::RefundOrderItem($order_item, $order, $reason_refund, $quantity);
        
             
        if($quantity == FALSE){
		
            Table::Delete('order_item', $order_item['id']);  
			
            $order_update_arr = array(
                            'origin' => array( "origin - {$total_refund}" ),
                            'quantity' => array( "quantity - {$order_item['quantity']}")        
                    );
        }else{
           
            $order_item_update_arr = array('quantity' => array( "quantity - {$quantity}" ), 'total' => array( "total - {$total_refund}" ));
            Table::UpdateCache('order_item', $order_item['id'], $order_item_update_arr);    
            $order_update_arr = array(
                            'origin' => array( "origin - {$total_refund}" ),
                            'quantity' => array( "quantity - {$quantity}")        
                    );
        }
        Table::UpdateCache('order', $order['id'], $order_update_arr);
    }
    
    
    
    //if its an option then deduct option 
    if($order_item['option_id'] ){
        $option = Table::Fetch('team_multi', $order_item['option_id']);


        if($option){
                if($quantity){
                    $minus = $quantity;
                }
                else{
                    $minus = $order_item['quantity'];
                }
                $condition_update = array(
                            'now_number' => array( "now_number - {$minus}" )
                );
                //     Deduct Stock                      
                if( $order_item['delivery'] != 'coupon' && $order_item['delivery_status'] != ZOrder::ShipStat_Invalid && $order_item['delivery_status'] != ZOrder::ShipStat_Waiting  )                           
                {
                   $condition_update["stock_count"] = array( "`stock_count` + {$minus}");
                }                         
                Table::UpdateCache('team_multi', $option['id'], $condition_update);    

        }
    }
    else{
        $team = Table::Fetch('team', $order_item['team_id']);

        if($team){
            team_state($team);
            if ( $team['state'] != 'failure' ) {
                    if($quantity){
                        $minus = $quantity;
                    }
                    else{
                        $minus = $order_item['quantity'];
                    }

                    $condition_update = array(
                            'now_number' => array( "now_number - {$minus}" )
                    );
                    //     Deduct Stock                      
                    if( $order_item['delivery'] != 'coupon' && $order_item['delivery_status'] != ZOrder::ShipStat_Invalid && $order_item['delivery_status'] != ZOrder::ShipStat_Waiting  )                           
                    {
                       $condition_update["stock_count"] = array( "`stock_count` + {$minus}");
                    }                         
                    Table::UpdateCache('team', $team['id'], $condition_update);                        
            }
        }
    }
    
    //Lastly Updating user balance 
    
    
    if ( $rid == 'credit' ) {
                $user = Table::Fetch('user', $order['user_id']);
		Table::UpdateCache('user', $order['user_id'], array(
					'money' => array( "money + {$order_item['credit']}" ),
		));
    } 
    
    
    redirect( WEB_ROOT . "/manage/order/order_edit.php?id=".$order_id);
}


$order_id = intval($_GET['id']);

$order = Table::Fetch('order', $order_id);

$order_items  = Table::Fetch('order_item', array($order_id), 'order_id');

$team_ids = Utility::GetColumn($order_items, 'team_id');
$option_ids = Utility::GetColumn($order_items, 'option_id');

$teams = Table::Fetch('team', $team_ids);

$options = Table::Fetch('team_multi', $option_ids);



$condition = array( 'detail_id' => $order_id, 'action' => 'refund' );
	$flow = DB::LimitQuery('flow', array(
				'condition' => $condition,
				'order' => 'ORDER BY id ASC',
				));
        
// $flow = Table::Fetch('flow', array($order_id), 'detail_id' );
include template('manage_order_editorder');