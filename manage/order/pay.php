<?php
require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('order');

$now = time();

if(is_post()){
    if($_POST['mark_shipped']){
       
        $order_items = $_POST['order_items'];
        $eta = $now;
        $state = ZOrder::ShipStat_Delivered;
        foreach($order_items as $oi){
            $order_item_i = Table::FetchForce('order_item', $oi);
            if($order_item_i['delivery'] == 'express' || $order_item_i['delivery'] == 'pickup')
            {
                $status = ZOrder::UpdateShippingState($state, $oi, $eta);
            }
           // Table::UpdateCache('order_item', $oi, array('shipment_status'=> 1, 'delivery_status' => ZOrder::ShipStat_Delivered));
        }
         Session::Set('notice', 'Orders Marked Shipped Successfully');
         redirect($_SERVER["HTTP_REFERER"]);
         exit;
    }
    
    if($_POST['mark_unshipped']){
        
        $order_items = $_POST['order_items'];
        $eta = $now;
        $state = ZOrder::ShipStat_Waiting;
        foreach($order_items as $oi){
            $order_item_i = Table::FetchForce('order_item', $oi);
            if($order_item_i['delivery'] == 'express' || $order_item_i['delivery'] == 'pickup')
            {
               // $status = ZOrder::UpdateShippingState($state, $oi, $eta);
                Table::UpdateCache('order_item', $oi, array('shipment_status'=> 1, 'delivery_status' => ZOrder::ShipStat_InPrep, 'delivery_time'=> $eta));
            }
           // 
        }
         Session::Set('notice', 'Orders Marked UnShipped Successfully');
         redirect( $_SERVER["HTTP_REFERER"]);
         exit;
    }
    
    exit;
}

$sql = " FROM `order` WHERE id IN (
    SELECT DISTINCT order_id FROM order_item ";


    
$team_id = abs(intval($_GET['team_id']));
if ( $team_id )
       $filter_clause = (strlen($filter_clause)>0? " AND " : " WHERE ") . " order_item.team_id = {$team_id} ";

$option_id = abs(intval($_GET['option_id']));
if ( $option_id )
       $filter_clause = (strlen($filter_clause)>0? " AND " : " WHERE ") . " order_item.option_id = {$option_id} ";
       
        
$ship_status = abs(intval($_GET['ship_status']));

if ($ship_status )
    $filter_clause .= (strlen($filter_clause)>0? " AND " : " WHERE ") . "  order_item.delivery_status = {$ship_status}";
    
$ship_eta = strval($_GET['ship_eta']);
if ($ship_eta){
    $etatime = strtotime($ship_eta);
    $eta_start = mktime(0,0,0, date("n",$etatime), date("j", $etatime), date("Y", $etatime));
    $eta_end = mktime(23,59,59, date("n",$etatime), date("j", $etatime), date("Y", $etatime));
    $filter_clause .= (strlen($filter_clause)>0? " AND " : " WHERE ") . "  order_item.delivery_time >= {$eta_start} AND order_item.delivery_time <= {$eta_end}";
}
 
$delivery = strval($_GET['delivery']);
if  ($delivery)
    $filter_clause .= (strlen($filter_clause)>0? " AND " : " WHERE ") . " order_item.delivery = '". $delivery. "'";
$sql .= $filter_clause. ") ";
        
    
$id = abs(intval($_GET['id'])); 
if ($id) 
    $sql .= " AND `order`.id = {$id}";


$cbday = strval($_GET['cbday']);
$ceday = strval($_GET['ceday']);
$pbday = strval($_GET['pbday']);
$peday = strval($_GET['peday']);
if ($cbday) { 
	$cbtime = strtotime($cbday);
	$sql .= " AND  create_time >= '{$cbtime}'";
}
if ($ceday) { 
	$cetime = strtotime($ceday);
	$sql .= " AND create_time <= '{$cetime}'";
}
if ($pbday) { 
	$pbtime = strtotime($pbday);
	$sql .= " AND pay_time >= '{$pbtime}'";
}
if ($peday) { 
	$petime = strtotime($peday);
	$sql .= " AND pay_time <= '{$petime}'";
}


/* filter */
$uemail = strval($_GET['uemail']);
if ($uemail) {
	$field = strpos($uemail, '@') ? 'email' : 'username';
	$field = is_numeric($uemail) ? 'id' : $field;
	$uuser = Table::Fetch('user', $uemail, $field);
	if($uuser) 
           $sql .= " AND user_id = {$uuser['id']}";
	else 
            $uemail = null;
}

$sql .= " AND state = 'pay' ORDER BY pay_time DESC";

if ( $_GET['download'] == "1" )
{
    $sql = "SELECT * " . $sql;
    $orders = DB::GetQueryResult($sql, false);
}
else
{
    $count_sql = "SELECT COUNT(*) total " . $sql;
    $count_result = DB::GetQueryResult($count_sql);
    $count = $count_result['total'];
    
  
    list($pagesize, $offset, $pagestring) = pagestring($count, 20);

    $sql = "SELECT * " . $sql . " LIMIT {$pagesize} OFFSET {$offset}";
    
    $orders = DB::GetQueryResult($sql,false);
}


$order_ids = Utility::Getcolumn($orders,"id");
$order_items = Table::Fetch('order_item', $order_ids, 'order_id');

$team_ids = Utility::GetColumn($order_items, 'team_id');
$teams = Table::Fetch('team', $team_ids);

$option_ids =  Utility::GetColumn($order_items, 'option_id');
$options = Table::Fetch('team_multi', $option_ids);

$order_items = Utility::AssColumn($order_items, 'order_id', 'id');


$pay_ids = Utility::GetColumn($orders, 'pay_id');
$pays = Table::Fetch('pay', $pay_ids);

$user_ids = Utility::GetColumn($orders, 'user_id');
$users = Table::Fetch('user', $user_ids);



if ( $_GET['download'] == "1" )
{
    if (!$orders) die('-ERR ERR_NO_DATA');
    //$team = Table::Fetch('team', $team_id);
   // $team = $teams[$team_id]
    $name = 'order_'.date('Ymd');
    $kn = array(
        'id' => 'order No.',
        'pay_id' => 'payment id',
        'service' => 'payment gateway',
        'price' => 'price',
        'quantity' => 'quantity',
        'condbuy' => 'option',
        'fare' => 'fare',
        'origin' => 'total',
        'money' => 'money paid',
        'credit' => 'pay with balance',
        'state' => 'state',
        'remark' => 'notes',
        'delivery' => 'delivery',
        'username' => 'username',
        'useremail' => 'Email',
        'usermobile' => 'mobile',
        'team_id' => 'Deal ID',
        'deal' => 'Deal Title',
        'option' => 'Multi Option',      
        'realname' => 'Contact First Name',
        'lastname' => 'Contact Last Name',
        'mobile' => 'mobile',
        'address' => 'address',
	'address2' => 'address2',
        'zipcode' => 'zipcode',       
        'region' => 'city',
        'name' => 'Recipient Name',
        'pay_time' => "Purchase date",
		'card'=>"Promo code",
//	'first_name' => 'Recipient First Name',
//	'last_name' => 'Recipient Last Name',
//	'recip_mobile' => 'Recipient Mobile',
//	'recip_email' => 'Recipient Email',
//	'recip_address' => 'Recipient Address',
//	'recip_address2' => 'Recipient Addrss 2,
//	'recip_region' => 'Recipient Region',
//	'recip_zipcode' => 'Recipient Zipcode',
	
        );

    $state = array(
            'unpay' => 'to be paid',
            'pay' => 'paid',
            );
    
    $pay = array(
            'alipay' => 'alipay',
            'tenpay' => 'TenPay',
            'chinabank' => 'ChinaBank',
            'credit' => 'Credit',
            'cash' => 'pay in cash',
            '' => 'other',
            'post' => 'Post Finance', 
            'wire' => 'Wire Transfer',
            'paypal' => 'Paypal',
            'swissbilling' => "Swiss Billing",
            );    
    
    foreach( $orders AS $one ) 
    {
        $oneuser = $users[$one['user_id']];
        
   
        $one['username'] = $oneuser['username'];
        $one['useremail'] = $oneuser['email'];
        $one['usermobile'] = $oneuser['mobile'];
        $one['name'] = $one['realname']. ' '.  $one['lastname'];
        //$one['service'] = $pay[$one['service']];
       
        $one['state'] = $state[$one['state']];
        $one['pay_time'] = date('d.m.Y', $one['pay_time']);
        
        $eorders[] = $one;
        foreach( $order_items[$one['id']] as $item)
        {
            $team = $teams[$item['team_id']];
            
            $item['id'] = $one['id'] ."-". $item['id'];
            $item['deal'] = $team['title'];
            $item['option']  = $options[$item['option_id']]['title_fr'];  
            $item['name'] = $item['first_name']. ' '. $item['last_name'];
            //$one['fare'] = ($one['delivery'] == 'express') ? $one['fare'] : 0;
            //$one['price'] = $teams[$one['team_id']]['market_price'];
            
            $eorders[] = $item;
        }
    }
    down_xls($eorders, $kn, $name);
}

$selector = 'pay';
include template('manage_order_pay');
