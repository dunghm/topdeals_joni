<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

need_manager();
need_auth('order');



if(is_post()){
    
    if(isset($_POST['print_barcodes'])){
        
            $order_items = $_POST['order_items'];
            $pdf = new PDFMerger;
            
            $ident_codes = array();
            foreach($order_items as $oi){
               $order_item_i = Table::FetchForce('order_item', $oi);
               if($order_item_i['shipment_status'] == 1){
                  // $ident_codes_string = $order_item_i['shipment_identcode'];
                   $identcode_arr = explode(',', $order_item_i['shipment_identcode']);

                   foreach($identcode_arr as $id_code){
                        if(!in_array($id_code, $ident_codes)){
                            $pdf->addPDF(dirname(dirname(dirname(__FILE__))).'/generated_labels/label_'.$id_code.'.pdf', 'all');
                            $ident_codes[] = $id_code;
                        }
                    }

               }
            }
            if(!empty($ident_codes)){
                $pdf->merge('download', 'OrderItem-'.date('Ymd').'.pdf');
            }else{
                   Session::Set('error', 'No Barcodes Available');
                   redirect( WEB_ROOT . "/manage/order/generate_barcode.php");
            }
     exit;     
    }
    
    if(isset($_POST['mark_shipped'])){
        
        $order_items = $_POST['order_items'];
        foreach($order_items as $oi){
            $order_item_i = Table::Fetch('order_item', $oi);
            if($order_item_i['delivery_status'] != ZOrder::ShipStat_Waiting){
                Table::UpdateCache('order_item', $oi, array('shipment_status'=> 1, 'delivery_status' => ZOrder::ShipStat_Delivered));
            }
        }
         Session::Set('notice', 'Orders Shipped Marked Successfully');
         redirect( WEB_ROOT . "/manage/order/generate_barcode.php");
         exit;
    }
    if(isset($_POST['mark_unshipped'])){
        
        $order_items = $_POST['order_items'];
        foreach($order_items as $oi){
            Table::UpdateCache('order_item', $oi, array('shipment_status'=> 0, 'delivery_status'=> ZOrder::ShipStat_InPrep));
        }
         Session::Set('notice', 'Orders Marked Unshiped Successfully');
         redirect( WEB_ROOT . "/manage/order/generate_barcode.php");
         exit;
    }
    
    
    $order_items = $_POST['order_items'];
    $shipping_address = '';
    $index = 0;
    $order_items_arr = array();
    $orders_arr = array();
    $order_ids = array();
    $generated_ids = array();
    
    $order_items_arr = Table::Fetch('order_item', $order_items);
    
    
    
    
    foreach ($order_items as $oi){
        
         $address = $order_items_arr[$oi]['address'];
        $generate_items = array();
        $generate_orders = array();
        
        if(!in_array($oi, $generated_ids)){
           
        foreach($order_items_arr as $oi_item){
            
            if($address == $oi_item['address']){
                
                $generate_items[] = $oi_item;
                $generate_orders[$oi_item['id']] = Table::Fetch('order', $oi_item['order_id']);
                $generated_ids[] = $oi_item['id'];
            }
        }
        
        
        if(!empty($generate_items)){
            
           $return =  ZSwissPost::GenerateLabels($generate_items, $generate_orders);
           
           if($return !== TRUE){
               
                Session::Set('error', 'Looks like some problem with order no.'.$return['order'].'<br> <strong>Message: </strong> '.$return['message']); 
                redirect($_SERVER["HTTP_REFERER"]);
           }
           
        }
        else{
            $generated_ids[] = $oi;
            $order_info = array();
            $order_info[$oi] =   Table::Fetch('order', $order_items_arr[$oi]['order_id']);
            $return =  ZSwissPost::GenerateLabels($order_items_arr[$oi], $order_info);
            if($return !== TRUE){
                Session::Set('error', 'Looks like some problem with orders.'); 
           }
        }
        
       
        }
       
        
        
        
    }
    //ZSwissPost::Init();
    //$return = ZSwissPost::GenerateLabels($order_items_arr, $orders);
  
        $pdf = new PDFMerger;
        $ident_codes = array();
        foreach($order_items_arr as $oi_p){
           $order_item_i = Table::FetchForce('order_item', $oi_p['id']);
           if($order_item_i['shipment_status'] == 1){
              // $ident_codes_string = $order_item_i['shipment_identcode'];
               $identcode_arr = explode(',', $order_item_i['shipment_identcode']);
               
               foreach($identcode_arr as $id_code){
                    if(!in_array($id_code, $ident_codes)){
                        $pdf->addPDF(dirname(dirname(dirname(__FILE__))).'/generated_labels/label_'.$id_code.'.pdf', 'all');
                        $ident_codes[] = $id_code;
                    }
                }
               
           }
        }
        $new_file_name = 'OrderItem-'.date('Ymd').rand().'.pdf';
        
        $file = $pdf->merge('file', dirname(dirname(dirname(__FILE__))).DIR_SEPERATOR."generated_labels".DIR_SEPERATOR.$new_file_name);
       
        Session::Set('file_download', $new_file_name);
        
        Session::Set('notice', 'Barcode Generated Successfully');
       redirect($_SERVER["HTTP_REFERER"]);
}