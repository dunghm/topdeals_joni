<?php
function mcurrent_vote($selector='index'){
	$a = array(
		'/manage/vote/index.php' => 'Home',
		'/manage/vote/feedback.php' => 'Feedback',
		'/manage/vote/question.php' => 'Questions',
	);
	$l = "/manage/vote/{$selector}.php";
	return current_link($l,$a,true);
}
