<?php
/**
 * @file
 * Take the user when they return from Twitter. Get access tokens.
 * Verify credentials and redirect to based on response from Twitter.
 */

/* Start session and load lib */
session_start();
require_once('app.php');
require_once('tw_connect/twitteroauth/twitteroauth.php');
require_once('tw_connect/config.php');

/* If the oauth_token is old redirect to the connect page. */
if (isset($_REQUEST['oauth_token']) && $_SESSION['oauth_token'] !== $_REQUEST['oauth_token']) {
  $_SESSION['oauth_status'] = 'oldtoken';
  unset($_SESSION['oauth_token']);
  unset($_SESSION['oauth_token_secret']);
  header('Location: ./index.php');
  exit;
}

/* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);

/* Request access tokens from twitter */
$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);

/* Remove no longer needed request tokens */
unset($_SESSION['oauth_token']);
unset($_SESSION['oauth_token_secret']);

/* If HTTP response is 200 continue otherwise send to connect page to retry */
if (200 == $connection->http_code) {
  /* The user has been verified and the access tokens can be saved for future use */

  /* Save the access tokens. Normally these would be saved in a database for future use. */
  $_SESSION['access_token'] = $access_token;
  $_SESSION['status'] = 'verified';
  //header('Location: ./index.php');
  Utility::Redirect(WEB_ROOT. '/account/signup_twitteremail.php');
  exit;
} else {
  /* Save HTTP status for error dialog on connnect page.*/
  $_SESSION['twitter_error'] = $connection->http_code;

  //header('Location: tw_connect/clearsessions.php');
  header('Location: ./index.php');
}
