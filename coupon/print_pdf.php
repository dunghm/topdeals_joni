<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

if(!$_REQUEST['user_id'])
{
  need_login();
  $user_id=$login_user_id;
}else{
  $user_id=$_REQUEST['user_id'];
}


if ( isset($_GET['oiid']) )
{
	$oi_id = strval($_GET['oiid']);
	$order_item = Table::Fetch('order_item', $oi_id);
	$total = $order_item['quantity'];
}
else{
	$id = strval($_GET['id']);
	$cpn = Table::Fetch('coupon', $id);
	$oi_id = $cpn['order_id'];
}
if(isset($_GET['coupon_id']))
{
    $coupon_id = $_GET['coupon_id'];
    $cpns = Table::Fetch('coupon', $coupon_id);
    $cpns = array($cpns);
}
else{
    $cpns = Table::Fetch('coupon', array($oi_id), 'order_id');
}
foreach ($cpns as $cpn)
{
	if ( $cpn['consume'] == 'Y' )
		continue;

	$id = $cpn['id'];  
	$coupon = ZCoupon::GetCouponForPrint($id);
  
	if (!$coupon) {
		  Session::Set('error', "{$INI['system']['couponname']} doesnt exist.");
		  redirect(WEB_ROOT . '/coupon/index.php');
	}

	if ($coupon['coupon']['user_id'] != $user_id) { 
		  Session::Set('error', "This order's {$INI['system']['couponname']} is not yours");
		  redirect(WEB_ROOT . '/coupon/index.php');
	}
  
	$order = $coupon['order'];
	$order_item = $coupon['order_item'];
/*
	if($order_item['option_id']){
		$multi = Table::Fetch('team_multi', $order_item['option_id']);
		$coupon['team']['title'] = $multi['title'];
		$coupon['team']['title_fr'] = $multi['title_fr'];
		$coupon['team']['image'] = $multi['image'];  
		$coupon['team']['market_price'] = $multi['market_price'];
		$coupon['team']['summary'] = $multi['summary'];
	}
*/
	$coupons[] = $coupon;
}
$pagetitle = 'Print Coupon';
$html = render('manage_coupon_print_pdf');
if ($order_item['mode']=='gift') { 
	$pagetitle = 'Print Gift Coupon';
	//die(include template('manage_coupon_print_gift'));
        $html = render('manage_coupon_print_gift_pdf');
}


//include template('manage_coupon_print');

        $html2pdf = new HTML2PDF('P', 'A3');
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $log = new KLogger(dirname(dirname(__FILE__))."/include/compiled/", KLogger::DEBUG);
        $log->logInfo("html start:");
        $log->logInfo($html);
        $log->logInfo(":html end:");
        $html2pdf->writeHTML($html);
        $html2pdf->Output('coupon-'.$coupon['id'].'.pdf', 'D');
        
//             $html2pdf = new HTML2PDF('P', 'A3');
//        $html2pdf->addFont('Sue Ellen Francisco', '','sueellenfrancisco.php');
//        //$html2pdf->pdf->SetDisplayMode('fullpage');
//        $html2pdf->writeHTML($html);
//        $html2pdf->Output('exemple05.pdf', 'D');