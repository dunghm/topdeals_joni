<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

if(!$_REQUEST['user_id'])
{
  need_login();
  $user_id=$login_user_id;
}else{
  $user_id=$_REQUEST['user_id'];
}


if ( isset($_GET['oiid']) )
{
	$oi_id = strval($_GET['oiid']);
	$order_item = Table::Fetch('order_item', $oi_id);
	$total = $order_item['quantity'];
}
else{
	$id = strval($_GET['id']);
	$cpn = Table::Fetch('coupon', $id);
	$oi_id = $cpn['order_id'];
}

$cpns = Table::Fetch('coupon', array($oi_id), 'order_id');
foreach ($cpns as $cpn)
{
if ( $cpn['consume'] == 'Y' )
		continue;

	$id = $cpn['id'];  
	$coupon = ZCoupon::GetCouponForPrint($id);
  
	if (!$coupon) {
		  Session::Set('error', "{$INI['system']['couponname']} doesnt exist.");
		  redirect(WEB_ROOT . '/coupon/index.php');
	}

	if ($coupon['coupon']['user_id'] != $user_id) { 
		  Session::Set('error', "This order's {$INI['system']['couponname']} is not yours");
		  redirect(WEB_ROOT . '/coupon/index.php');
	}
  
	$order = $coupon['order'];
	$order_item = $coupon['order_item'];
/*
	if($order_item['option_id']){
		$multi = Table::Fetch('team_multi', $order_item['option_id']);
		$coupon['team']['title'] = $multi['title'];
		$coupon['team']['title_fr'] = $multi['title_fr'];
		$coupon['team']['image'] = $multi['image'];  
		$coupon['team']['market_price'] = $multi['market_price'];
		$coupon['team']['summary'] = $multi['summary'];
	}
*/
	$coupons[] = $coupon;
}

if ($order_item['mode']=='gift') { 
	$pagetitle = 'Print Gift Coupon';
	die(include template('manage_coupon_print_gift'));
}

$pagetitle = 'Print Coupon';
include template('manage_coupon_print');
