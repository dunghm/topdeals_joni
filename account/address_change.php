<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

need_login();
$addresses = Table::Fetch('user_address', array($login_user['id']), "user_id");

if(is_post()){
    $update = array();
    $update = array(
		//'email' => $_POST['email'],
		//'username' => $_POST['user_name'],
		'first_name' => $_POST['user_first_name'],
                'last_name' => $_POST['last_name'],
		'zipcode' => $_POST['user_zipcode'],
		'address' => $_POST['user_address'],
                'address2' => $_POST['address2'],
		'mobile' => $_POST['user_mobile'], 
		'address_no' => $_POST['address_no'],
		'region' => $_POST['user_region'], 
		//'city_id' => abs(intval($_POST['city_id'])),
		//'country' => $_POST['country'],           
	);
    
        DB::Update('user', $login_user['id'], $update );
        Session::Set('notice', "Les changements ont bien été enregistrés");
	redirect( WEB_ROOT . '/account/address_change.php ');
         
}

if(isset($_GET['action'])){
    $action = $_GET['action'];
    if($action == 'delete'){
        $id = intval($_GET['id']);
        DB::Delete('user_address', array('id'=>$id));
        echo 'success';
        exit;
    }
    else if( $action == 'edit'){
         $id = intval($_GET['id']);
         $user_address = Table::Fetch('user_address',$id );
         json($user_address);
         exit;
    }
    else if( $action == 'edit_submit'){
         $id = intval($_GET['id']);
         $update_arr = array();
  
         $update_arr['address_name'] = $_GET['address_name'];
         $update_arr['first_name'] = $_GET['first_name'];
         $update_arr['last_name'] = $_GET['last_name'];
         $update_arr['street'] = $_GET['street'];
         $update_arr['street_no'] = $_GET['street_number'];
         $update_arr['address'] = $_GET['address'];
         $update_arr['zipcode'] = $_GET['zipcode'];
         $update_arr['region'] = $_GET['region'];
         $update_arr['mobile_no'] = $_GET['mobile_no'];
         
          DB::Update('user_address', $id, $update_arr );
         
          $address = Table::Fetch('user_address',$id );
        json($address);
           
        
         
         exit;
    }
    else if( $action == 'add'){
        // $id = intval($_GET['id']);
         $update_arr = array();
         
         $update_arr['address_name'] = $_GET['address_name'];
         $update_arr['first_name'] = $_GET['first_name'];
         $update_arr['last_name'] = $_GET['last_name'];
         $update_arr['street'] = $_GET['street'];
         $update_arr['street_no'] = $_GET['street_number'];
         $update_arr['address'] = $_GET['address'];
         $update_arr['zipcode'] = $_GET['zipcode'];
         $update_arr['region'] = $_GET['region'];
         $update_arr['mobile_no'] = $_GET['mobile_no'];
         $update_arr['user_id'] = $login_user_id;
          //DB::Update('user_address', $id, $update_arr );
         
        $insert_id =  DB::Insert('user_address', $update_arr);
       $address = Table::Fetch('user_address',$insert_id );
        json($address);
        
         exit;
    }
}

//print_r_r($login_user);
include template_ex('content_refer_address');