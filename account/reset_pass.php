<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

$code = strval($_GET['code']);
if ( $code == 'ok' && is_get()) {
	//redirect(WEB_ROOT);
	include template_ex('account_reset_ok');
	exit;
}


if(isset($_SESSION['user_id'])) {
	unset($_SESSION['user_id']);
	ZLogin::NoRemember();
	$login_user = $login_user_id = $login_manager = $login_leader = null;
}


$user = Table::Fetch('user', $code, 'recode');
if (!$user) {
	die('invalid user');
	Session::Set('error', 'the link for password reset is invalid');
	redirect( WEB_ROOT . '/index.php');
}

if (is_post()) {
	if ($_POST['password'] == $_POST['password2']) {
		ZUser::Modify($user['id'], array(
			'password' => $_POST['password'],
			'recode' => '',
		));
		
		Session::Set('user_id', $user['id']);
		ZLogin::Remember($user);
		ZUser::SynLogin($user['username'], $_POST['password']);
		ZCredit::Login($user['id']);
		
		Session::Set('reset_password', 'ok');
        //redirect( WEB_ROOT . '/account/login.php');
		redirect( WEB_ROOT . '/account/reset_pass.php?code=ok');
	}
	Session::Set('error', 'the passwords dont match, please input them again');
}

$pagetitle = 'Reset password';
include template_ex("content_reset");
