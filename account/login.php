<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

if ( $_POST ) {
	$login_user = ZUser::GetLogin($_POST['email'], $_POST['password']);
	if ( !$login_user || $login_user['enable'] == 'N') {
		Session::Set('error', "L'identification a échoué");
                
                $field = strpos($_POST['email'], '@') ? 'email' : 'username';
                $user_check = Table::Fetch('user', $_POST['email'], $field);
                if($user_check){
                     Session::Set('email_entered', $_POST['email']);
                     Session::Set('pass_error', "Le mot de passe saisi est incorrecte");
                   
                }else{
                       Session::Set('email_error', "L'adresse e-mail saisie est incorrecte");
                }
//		if(isset($_POST['ref'])){
//			//redirect($_POST['ref']);			
//			redirect(WEB_ROOT . $_POST['ref']);	
//		}else {	
                if(isset($_POST['ref'])){
                    redirect(WEB_ROOT . '/account/login.php?ref='.$_POST['ref']);
                }
                else{
                    redirect(WEB_ROOT . '/account/login.php');
                }
			
//		}	
	} else if (option_yes('emailverify')
			&& $login_user['enable']=='N'
			&& $login_user['secret']
			) {
		Session::Set('unemail', $_POST['email']);
		redirect(WEB_ROOT .'/account/verify.php');
	} else {
		Session::Set('user_id', $login_user['id']);
		ZLogin::Remember($login_user);
		ZUser::SynLogin($login_user['username'], $_POST['password']);
		ZCredit::Login($login_user['id']);

		if($pre_launch) {
			redirect(get_loginpage(WEB_ROOT . '/account/refer.php'));
		}
		else {
                  	/* //Commented as requirement was to send back user to refering page
			if(isset($_POST['ref'])){
                          redirect(WEB_ROOT . $_POST['ref']);		
			}
                  	*/
                    /*
                  $ref_url = $_SERVER["HTTP_REFERER"];
                  if(strpos($ref_url,'topdeal.ch')>0){
			redirect($_SERVER["HTTP_REFERER"]);
                  }else{
	                redirect(get_loginpage(WEB_ROOT . '/index.php'));    
                  }*/
                    if( strstr ( $_POST['ref'] , 'signup' ) ){
                        
                        redirect(WEB_ROOT . '/index.php');
                    }
                    else{
                        
                        redirect($_POST['ref']);
                    }
		}
	}
}

//redirect(WEB_ROOT . '/index.php#signin');
/*
$currefer = strval($_GET['r']);
if ($currefer) { Session::Set('loginpage', udecode($currefer)); }
$pagetitle = 'Login';
*/
include template('account_login');
