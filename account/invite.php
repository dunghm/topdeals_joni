<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

$pagetitle = 'Invitation rebate';

if (! is_login() ) {
	die(include template('account_invite_signup'));
}

if($_POST['email_txt'] && $_POST['message_are']) {
	$email_addr = $_POST['email_txt'];
        
	($name = $_POST['name_txt']) || ($name = $login_user['username']);
	$content = $_POST['message_are'];
	//$email_addr = implode(",", $emails);
	mail_invitation($email_addr, $content, $name);
	Session::Set('notice', 'invitation sent successfully');
	redirect( WEB_ROOT . '/account/refer.php' );
}


$condition = array( 
		'user_id' => $login_user_id, 
		'credit > 0',
		'pay' => 'Y',
		);
$money = Table::Count('invite', $condition, 'credit');
$count = Table::Count('invite', $condition);

$team = current_team($city['id']);
die(include template_ex('content_refer_done'));
