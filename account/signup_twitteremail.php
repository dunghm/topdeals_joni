<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');


if( $_SESSION['user_id'] )
{
    // Don't show this Page if the User is Already Logged In
    Utility::Redirect( WEB_ROOT . '/index.php' );
}

$access_token = $_SESSION['access_token'];
if( $access_token != '' )
{
    $Tlogin_user = ZUser::GetUserByTwitter_Id($_SESSION['access_token']['user_id']);
    if( !$Tlogin_user )
    {
        $username = preg_replace("/[^a-z0-9]/i","",strtolower($_SESSION['access_token']['screen_name']));
        $t['username'] = $username;
        $t['realname'] = $username;
        $t['password'] = "tw".rand(1000,2000);
        $t['twitter_userid'] = $_SESSION['access_token']['user_id'];
        $_SESSION['TWITTER_USER_LOGIN'] = $t;
       // Utility::Redirect( WEB_ROOT . '/account/signup_twitteremail.php');
    }
    else if (option_yes('emailverify')
                && $Tlogin_user['enable']=='N'
                && $Tlogin_user['secret'])
   {
            Session::Set('unemail', $Tlogin_user['email']);
            redirect(WEB_ROOT .'/account/verify.php');
    }
    else
    {
            Session::Set('user_id', $Tlogin_user['id']);
            ZLogin::Remember($Tlogin_user);
            //ZUser::SynLogin($Tlogin_user['username'], $Tlogin_user['password']);
            ZCredit::Login($Tlogin_user['id']);
            ($goto = Session::Get('loginpage', true)) || ($goto = WEB_ROOT.'/index.php');
            Utility::Redirect($goto);
    }
}

if ( $_POST )
{
	$u = array();

        $u = $_SESSION['TWITTER_USER_LOGIN'];

	$u['email'] = strval($_POST['email']);
        if ( ! Utility::ValidEmail($u['email'], true) )
        {
            Session::Set('error', 'Email address is invalid');
            Utility::Redirect( WEB_ROOT . '/account/signup_twitteremail.php');
	}

	$u['city_id'] = isset($_POST['city_id'])
		? abs(intval($_POST['city_id'])) : abs(intval($city['id']));

        if ( $_POST['subscribe'] )
        {
		ZSubscribe::Create($u['email'], abs(intval($u['city_id'])));
	}

        //$u['username'] = strval($_POST['username']);
	//$u['password'] = strval($_POST['password']);

        if ( option_yes('emailverify') )
        {
            $u['enable'] = 'N';
        }

        if ( ($user_id = ZUser::Create($u)) )
        {
            
            if ( option_yes('emailverify') )
            {
                
                mail_sign_id($user_id);
                Session::Set('unemail', $_POST['email']);
                redirect( WEB_ROOT . '/account/signuped.php');
            } 
            else
            {
                
                    ZLogin::Login($user_id);
                    ZUser::Modify($user_id, array("twitter_userid" => $user_details['twitter_userid']));
                    unset($_SESSION['access_token']);
                    unset($_SESSION['TWITTER_USER_LOGIN']);

                    Utility::Redirect( WEB_ROOT . '/index.php');
            }
        } else {
                $au = Table::Fetch('user', $_POST['email'], 'email');
                if ( $au )
                {
                    Session::Set('error', 'registration failed,Email id is already in use.');
                } 
                else
                {
                    Session::Set('error', "registration failed, username ". $u['username']. " is already in use.");
                }
        }
}
/*
if ( isset($_POST['email'])) 
{
    if ( ! Utility::ValidEmail($_POST['email'], true) )
    {
        Session::Set('error', 'Email is not a valid email address');
        Utility::Redirect( WEB_ROOT . '/account/signup_twitteremail.php');
    }
    else
    {
        $user_details = array();
        $user_details = $_SESSION['TWITTER_USER_LOGIN'];
        $user_details['email'] = $_POST['email'];
        //$user_details['username'] = $_POST['username'];
        //$user_details['password'] = $_POST['password'];
        //$user_details['twitter_userid'] = $_SESSION['TWITTER_USER_LOGIN']['twitter_userid'];

        if($user_id = ZUser::Create($user_details))
        {
            ZLogin::Login($user_id);
            ZUser::Modify($user_id, array("twitter_userid" => $user_details['twitter_userid']));
            unset($_SESSION['access_token']);
            unset($_SESSION['TWITTER_USER_LOGIN']);

            Utility::Redirect( WEB_ROOT . '/index.php');
        } 
    }
    
*/
    // unset($_SESSION['TWITTER_USER_LOGIN']);
	 


	//$login_TWuserDetails = ZUser::GetTWUserByEmail($user_details['email'],$user_details['twitter_userid']);
//	$login_userEmailCheck =ZUser::GetUserByEmail($user_details['email']);
//	
//	var_dump($login_TWuserDetails);
//	
//	var_dump($login_userEmailCheck);
//	
//	 if($login_TWuserDetails['id']!=''){
//	      ZLogin::Login($login_TWuserDetails['id']);
//		 // setcookie('_twitter_sess','1');
//		  Utility::Redirect( WEB_ROOT . '/index.php');
// 	 }
	 


	 
		 //if($login_userEmailCheck!='')
//		 {
//				  $sql = "update user set twitter_userid = '".$user_details['twitter_userid']."'  where id ='".$login_userEmailCheck['id']."'";
//				   mysql_query($sql);
//				   ZLogin::Login($login_userEmailCheck['id']);
//				   Utility::Redirect( WEB_ROOT . '/index.php');
//		}
//		else
//		{		
//				if($user_id = ZUser::Create($user_details))
//				{
//					ZLogin::Login($user_id);
//					Utility::Redirect( WEB_ROOT . '/index.php');
//				}
//		}
		
        

// }
 
$pagetitle = 'Registration';	
include template('signup_twitteremail');

?>

