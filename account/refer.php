<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

if ( ! is_login() )
	die(include template('noaccount_refer'));
	
need_login();

$condition = array( 'user_id' => $login_user_id, );
$count = Table::Count('invite', $condition);

//$money = Table::Count('invite', $condition, 'credit');
$thecond = array( 'user_id' => $login_user_id, 'action' => 'invite', 'direction' => 'income' );
$money = Table::Count('flow',$thecond, 'money');


list($pagesize, $offset, $pagestring) = pagestring($count, 20);

$invites = DB::LimitQuery('invite', array(
			'condition' => $condition,
			'order' => 'ORDER BY buy_time DESC',
			'size' => $pagesize,
			'offset' => $offset,
			));

$user_ids = Utility::GetColumn($invites, 'other_user_id');
$team_ids = Utility::GetColumn($invites, 'team_id');

$users = Table::Fetch('user', $user_ids);
$teams = Table::Fetch('team', $team_ids);

$pagetitle = 'My Invitations';
include template_ex("content_refer");

