<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

if ( $_POST ) {
	$u = array();
			
	$u['username'] = strval($_POST['email']);//strval($_POST['username']);
	$u['password'] = strval($_POST['password']);
	$u['email'] = strval($_POST['email']);
	$u['city_id'] = isset($_POST['city_id']) ? abs(intval($_POST['city_id'])) : abs(intval($city['id']));
	$u['mobile'] = strval($_POST['mobile']);
        
        // real name
        // address
        $u['address'] = strval($_POST['address']);
        $u['zipcode'] = strval($_POST['zipcode']);
        $u['region'] = strval($_POST['region']);
        $u['gender'] = strval($_POST['gender']);
        $u['realname'] = strval($_POST['realname']);
        $u['country'] = strval($_POST['country']);
        // 

	if ( $_POST['subscribe'] ) { 
		ZSubscribe::Create($u['email'], abs(intval($u['city_id']))); 
	}
	if ( ! Utility::ValidEmail($u['email'], true) ) {
		Session::Set('error', "L'adresse e-mail saisie n'est pas valide");
			if(isset($_POST['ref'])){
			redirect(WEB_ROOT . $_POST['ref']);				
			}		
		redirect( WEB_ROOT . '/account/signup.php');
	}
	if ($_POST['password2']==$_POST['password'] && $_POST['password']) {
		if ( option_yes('emailverify') ) { 
			$u['enable'] = 'Y'; 
		}
		if ( $user_id = ZUser::Create($u) ) {
			if ( option_yes('emailverify') ) {
				mail_sign_id($user_id);
				Session::Set('unemail', $_POST['email']);
				redirect( WEB_ROOT . '/account/signuped.php');
			} else {
				ZLogin::Login($user_id);
				ZCredit::Register($user_id);
				$login_user = ZUser::GetLogin($_POST['email'], $_POST['password']);
				mail_welcome_user($login_user, $_POST['password']);
				if($_POST['auto-login']) 
				{
					ZLogin::Remember($login_user);
				}
				if($pre_launch) {
					redirect(get_loginpage(WEB_ROOT . '/account/refer.php'));
				}
				else {
                                  	/*
					if(isset($_POST['ref'])){
					redirect(WEB_ROOT . $_POST['ref']);				
					}					
					redirect(get_loginpage(WEB_ROOT . '/index.php'));
					*/
                                    /*
                                      $ref_url = $_SERVER["HTTP_REFERER"];
                                      if(strpos($ref_url,'topdeal.ch')>0){
                                            redirect($_SERVER["HTTP_REFERER"]);
                                      }else{
                                            redirect(get_loginpage(WEB_ROOT . '/index.php'));    
                                      }                                                      
                                  */
                                    include template('account_signup_success');
                                    exit;
				}				
				
			}
		} else {
			$au = Table::Fetch('user', $_POST['email'], 'email');
			if ( $au ) {
				Session::Set('error', "Cette adresse e-mail est déjà associée à un compte");
				if(isset($_POST['ref'])){
				redirect(WEB_ROOT . $_POST['ref']);				
				}				
			} else {
				Session::Set('error', "Cette adresse e-mail est déjà associée à un compte");
				if(isset($_POST['ref'])){
				redirect(WEB_ROOT . $_POST['ref']);				
				}				
			}
		}
	} else {
		Session::Set('error', 'registration failed，incorrect password settings');
		if(isset($_POST['ref'])){
		redirect(WEB_ROOT . $_POST['ref']);				
		}						
	}
}


$pagetitle = 'Registration';
include template('account_signup');
