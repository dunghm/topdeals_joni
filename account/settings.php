<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

need_login();
if ( $_POST ) {
        if ( $login_user['email'] != $_POST['email'] )
        {
            // User has changed his Email address
            
            
            // Check if this email is being used by another User
            $au = Table::Fetch('user', $_POST['email'], 'email');
            if ( $au && $au['id'] != $login_user['id']) {
                    Session::Set('error', "Adresse e-mail est déjà utilisée . S'il vous plaît essayer un autre");
                    redirect( WEB_ROOT . '/account/settings.php');		
            } 
           
            // No  User found, check if the Email is in valid Format
            if ( ! Utility::ValidEmail( $_POST['email'], true) ) {
                    Session::Set('error', 'Adresse email invalide');                            
                    redirect( WEB_ROOT . '/account/settings.php');
            }
            
            $bEmailUpdated = true;
        }
        
		 if ( $login_user['username'] != $_POST['username'] )
        {
			//check username is already existing
			$userNameValidation = Table::Fetch('user', $_POST['username'], 'username');
			if ( $userNameValidation && $userNameValidation['id'] != $login_user['id']) {
					Session::Set('error', "Nom d'utilisateur est déjà utilisé. S'il vous plaît essayer un autre");
					redirect( WEB_ROOT . '/account/settings.php');		
			} 
		}	
        
        
	$update = array(
		'email' => $_POST['email'],
		'username' => $_POST['username'],
		'first_name' => $_POST['first_name'],
                'last_name' => $_POST['last_name'],
		'zipcode' => $_POST['zipcode'],
		'address' => $_POST['address'],
                'address2' => $_POST['address2'],
		'mobile' => $_POST['mobile'], 
		'gender' => $_POST['gender'],
		'region' => $_POST['region'], 
		'city_id' => abs(intval($_POST['city_id'])),
		'country' => $_POST['country'],           
	);
        
        if ($login_user['username'] == $login_user['email']) {
            // $update['username'] = $_POST['email'];
        }
        
        /*
        if ( is_null($login_user['birth_day']) || empty($login_user['birth_day']) || $login_user['birth_day'] < 0 )
        {
            $birth_day = mktime(0,0,0, $_POST['bd_month'], $_POST['bd_day'],$_POST['bd_year']);
            
            $update['birth_day'] = $birth_day;
        }
        */
        $birth_day = mktime(0,0,0, $_POST['bd_month'], $_POST['bd_day'],$_POST['bd_year']);
         $update['birth_day'] = $birth_day;
            
	//		'address_no' => $_POST['address_no'],
	$avatar = upload_image('upload_image',$login_user['avatar'],'user');
	$update['avatar'] = $avatar;

        if ( $bEmailUpdated )
        {
            //@TODO: send email verificaiton
            // Update Secret
            // Enable to 'N'
            // Send Email*/
        }
	if ( $_POST['subscribe'] ) { 
            ZSubscribe::Create($update['email'], abs(intval($update['city_id']))); 
	}
        else
        {
            ZSubscribe::Unsubscribe($login_user); 
        }

	if ( $_POST['password'] == $_POST['password2']
			&& $_POST['password'] 
			&& strtolower(md5($email)) != 'f7e0dcf82fd5d444b11cb42db2a8da3e' ) 
	{
		$update['password'] = $_POST['password'];
	}

	
	
	//insert newly selected user interests
        if ( isset($_POST['category']))
        {
            //delete existing user interests
            $condition = array( 'user_id' => $login_user['id'],);	
            DB::Delete('user_interests', $condition);
            foreach($_POST['category'] as $c)
            {
                    $ui = array(
                    'user_id' => $login_user['id'],
                    'interest_id' => $c,
                    );
                    DB::Insert('user_interests', $ui);
            }
        }
       
	if ( ZUser::Modify($login_user['id'], $update) ) {
		Session::Set('notice', 'account resetting succeeded');
		redirect( WEB_ROOT . '/account/settings.php ');
	} else {
		Session::Set('error', 'account resetting failed');
		redirect( WEB_ROOT . '/account/settings.php ');
	}
}

$addresses = Table::Fetch('user_address', array($login_user['id']), "user_id");

$isSubscribed = count(Table::Fetch('subscribe', array($login_user['email']), "email"));

//$readonly['email'] = defined('UC_API') ? '' : 'readonly';
$readonly['email'] = ''; //defined('UC_API') ? '' : 'readonly';
$readonly['username'] = defined('UC_API') ? 'readonly' : '';
$countries = array();
GetCountries($countries);

$pagetitle = 'Account setting';

include template_ex("content_profile");
