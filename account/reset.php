<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');
/*
if(isset($_SESSION['user_id'])) {
	unset($_SESSION['user_id']);
	ZLogin::NoRemember();
	$login_user = $login_user_id = $login_manager = $login_leader = null;
}

$code = strval($_GET['code']);
if ( $code == 'ok' && is_get()) {
	redirect(WEB_ROOT);
	//die(include template('account_reset_ok'));
}

$user = Table::Fetch('user', $code, 'recode');
if (!$user) {
	Session::Set('error', 'the link for password reset is invalid');
	redirect( WEB_ROOT . '/index.php');
}
*/
need_login();
global $login_user_id;

if (is_post()) {
        global $login_user_id;
        $user = Table::Fetch('user', $login_user_id);
        
        if(ZUser::GenPassword($_POST['current_pass']) == $user['password']){
            if ($_POST['password'] == $_POST['password2']) {
                    ZUser::Modify($user['id'], array(
                            'password' => $_POST['password'],
                            //'recode' => '',
                    ));
                    //Session::Set('reset_password', 'ok');
                    Session::Set('notice', 'Votre mot de passe a bien été modifié');
                    redirect( WEB_ROOT . '/account/reset.php?code=ok');
            }
            Session::Set('error', 'the passwords dont match, please input them again');
        }
        else{
            Session::Set('error', 'Current Password Dont match');
        }
}

$pagetitle = 'Reset password';
include template("account_reset");
