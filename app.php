<?php
require_once(dirname(__FILE__). '/include/application.php');
/* magic_quota_gpc */
$_GET = magic_gpc($_GET);
$_POST = magic_gpc($_POST);
$_COOKIE = magic_gpc($_COOKIE);

/* process currefer*/
$currefer = uencode(strval($_SERVER['REQUEST_URI']));

/* session,cache,configure,webroot register */
Session::Init();
require_once(dirname(__FILE__).'/include/language.php');

$INI = ZSystem::GetINI();
/* end */
date_default_timezone_set('Europe/Zurich');

/* biz logic */
$currency = $INI['system']['currency'];
$login_user_id = ZLogin::GetLoginId();
$login_user = Table::Fetch('user', $login_user_id);
$hotcities = option_hotcategory('city', false, true);
$allcities = option_category('city', false, true);
$allcategories = option_category('user-interest', false, true);
$city = cookie_city(null);
$log = new KLogger("/web", KLogger::DEBUG);
$pre_launch = $INI['system']['pre-launch'];

//fetch user interests
$condition = array( 'user_id' => $login_user['id'],);
//$user_interests = Table::Fetch('user_interests', 'user_id', $login_user['id']);

$user_interests = DB::LimitQuery('user_interests', array('condition' => $condition,));
$user_interests = Utility::GetColumn($user_interests, 'interest_id');

/* not allow access app.php */
if($_SERVER['SCRIPT_FILENAME']==__FILE__){
	redirect( WEB_ROOT . '/index.php');
}
/* end */
$AJAX = ('XMLHttpRequest' == @$_SERVER['HTTP_X_REQUESTED_WITH']);
if (false==$AJAX) { 
	header('Content-Type: text/html; charset=UTF-8'); 
	run_cron();
} else {
	header("Cache-Control: no-store, no-cache, must-revalidate");
}
function isSecure() {
  return
    (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
    || $_SERVER['SERVER_PORT'] == 443;
}
if(isSecure()){
  $INI['system']['wwwprefix'] = $INI['system']['secure_wwwprefix'];
  $INI['system']['imgprefix'] = $INI['system']['secure_imgprefix'];
  $INI['system']['public_url']  = $INI['system']['secure_public_url'];
}
