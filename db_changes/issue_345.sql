alter table `partner` add column `business_address` varchar(255) NULL after `location`;

alter table `partner` change `phone` `phone` varchar(128) character set utf8 collate utf8_general_ci NULL , change `mobile` `mobile` varchar(128) character set utf8 collate utf8_general_ci NULL ;
