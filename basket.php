<?php

require_once(dirname(__FILE__) . '/app.php');

function AddToBasket($user_id,$action='')
{
    $basket = new Basket($user_id);
    $team_id = abs(intval($_POST['team_id']));
    $option_id = abs(intval($_POST['option_id']));
    $team = Table::FetchForce('team', $team_id);
    
    if( !$team ){
	die('no such team');
        return;
    }
    
 
    $upper_limit = $team['max_number'];
    if ( $option_id && $option_id >0 )
    {
        $option = Table::Fetch('team_multi', $option_id);
	if ( !$option){
            die('no such option'. $option_id);
        }
        $upper_limit = $option['max_number'];
    }
    else
        $option_id  = false;
    
    team_state($team);

    // <---- noneed pay where goods soldout or end 
    if ($team['close_time']) {
        Session::Set('basket_closed', "Vous ne pouvez malheureusement plus procéder à l'achat de cette offre car celle-ci est terminée");
        redirect(WEB_ROOT  . $_POST['ref'] );
    }
    // end ------->
    if ($upper_limit > 0 && ZTeam::GetStockLeft($team, $option) <= 0 )
    { 
       Session::Set('basket_failed', "Le stock restant est pour le moment r&eacute;serv&eacute; car d'autres acheteurs sont en train de proc&eacute;der &agrave; la commande, veuillez r&eacute;-essayer plus tard");
       redirect(WEB_ROOT  . $_POST['ref'] );
    }

    //is there a limit per user?
    
    if ($team['per_number']>0) 
    {
         // There is a Limit per User
        $basket_quantity = $basket->GetQuantity($team_id, $option_id);     
        
        $purchased_count = 0;
         // Currently selected quantity in the basket is lower than the Limit
           // Check if User has previously purchased it
        $sql = "SELECT ifnull(SUM(order_item.quantity),0) AS now_count 
            FROM order_item LEFT JOIN `order` ON order_id = `order`.id 
            WHERE user_id = {$user_id} AND state = 'pay' AND order_item.team_id = {$team['id']}";

        $count_result = DB::GetQueryResult($sql, true);
        $purchased_count = $count_result['now_count'];
        //$quantity += $now_count;
        

        $leftnum = ($team['per_number'] - $basket_quantity - $purchased_count);
        
        if ($leftnum <= 0) {
            Session::Set('basket_failed', "Cette offre étant limitée en quantité, l’article sélectionné ne peut pas être ajouté à votre panier.");

            redirect(WEB_ROOT  . $_POST['ref'] );        
        }        
    }
    
    // User is not violating the Limit Defined in Global Deal Settings
    // Now check if there is a Limit Defined in the Selected Deal Option
    
    if ($option && $option['per_number']>0) 
    {
        // There is a Limit per User in the Option
        $basket_quantity = $basket->GetQuantity($team_id, $option['id']);
        $purchased_count = 0;
        
           // Currently selected quantity in the basket is lower than the Limit
           // Check if User has previously purchased it
           $sql = "SELECT SUM(order_item.quantity) AS now_count 
               FROM order_item LEFT JOIN `order` ON order_id = `order`.id 
               WHERE user_id = {$user_id} AND state = 'pay' AND 
                   order_item.team_id = {$team['id']} AND order_item.option_id = {$option['id']}";
               
           $count_result = DB::GetQueryResult($sql, true);
           $purchased_count = $count_result['now_count'];
           //$quantity += $now_count;
        

        $leftnum = ($option['per_number'] - $purchased_count - $basket_quantity);
        if ($leftnum <= 0) {
            Session::Set('basket_failed', "Cette offre étant limitée en quantité, l’article sélectionné ne peut pas être ajouté à votre panier.");
            redirect(WEB_ROOT  . $_POST['ref'] );    
        }        
    }
    
    if ( $option && $option > 0 )
        $basket->Add($team['id'], 1, $team['team_price'], $_POST['mode'], $option['id']);
    else 
        $basket->Add($team['id'], 1, $team['team_price'], $_POST['mode']);
    
    $basket->Save($action);
    Session::Set('basket_updated',true);
    
    
    //die('success');
}

function UpdateBasket($user_id)
{
    
    $basket = new Basket($user_id);
    $team_id = abs(intval($_POST['team_id']));
    $option_id = abs(intval($_POST['option_id']));
    
    $quantity = $basket->GetQuantity($team_id, $option_id );
    //peruser buy count
    
   $team = Table::FetchForce('team', $team_id);
    if( !$team ){
	die('{ok:false,"error":"no such deal"}');
    }
    
    $upper_limit = $team['max_number'];
    if ( $option_id && $option_id >0 )
    {
        $option = Table::Fetch('team_multi', $option_id);
	if ( !$option){
            die('{ok:false,"error":"no such option"}');
        }
        $upper_limit = $option['max_number'];
    }
    else
        $option_id  = false;
    
    
    
    $new_quantity = abs(intval($_POST['quantity']));
    $old_quantity = $basket->GetQuantity($team_id,$option_id);
    $diff_quantity = $new_quantity - $old_quantity;
    
    if ( $upper_limit > 0 && 
            $diff_quantity > 0 &&
            ZTeam::GetStockLeft($team, $option) < $diff_quantity )
    {
        Session::Set('basket_failed', "Le stock restant est pour le moment r&eacute;serv&eacute; car d'autres acheteurs sont en train de proc&eacute;der &agrave; la commande, veuillez r&eacute;-essayer plus tard");
        redirect(WEB_ROOT  . $_POST['ref'] );
    }
    
    
    if ($team['per_number']>0) 
    {
         // There is a Limit per User
        //$basket_quantity = $basket->GetQuantity($team_id);      
        $purchased_count = 0;
        if ( $new_quantity < $team['per_number'] )
        {
           // Currently selected quantity in the basket is lower than the Limit
           // Check if User has previously purchased it
           $sql = "SELECT ifnull(SUM(order_item.quantity),0) AS now_count 
               FROM order_item LEFT JOIN `order` ON order_id = `order`.id 
               WHERE user_id = {$user_id} AND state = 'pay' AND order_item.team_id = {$team['id']}";
               
           $count_result = DB::GetQueryResult($sql, true);
           $purchased_count = $count_result['now_count'];
           //$quantity += $now_count;
        }

        $leftnum = ($team['per_number'] - $new_quantity - $purchased_count);
        if ($leftnum < 0) {

             die('{ok:false,error:"Cette offre étant limitée en quantité, l’article sélectionné ne peut pas être ajouté à votre panier."}');
        }        
    }
    
    // User is not violating the Limit Defined in Global Deal Settings
    // Now check if there is a Limit Defined in the Selected Deal Option
    
    if ($option && $option['per_number']>0) 
    {
        // There is a Limit per User in the Option
        //$basket_quantity = $basket->GetQuantity($team_id, $option['id']);
        $purchased_count = 0;
        if ( $new_quantity < $option['per_number'] )
        {
           // Currently selected quantity in the basket is lower than the Limit
           // Check if User has previously purchased it
           $sql = "SELECT ifnull(SUM(order_item.quantity),0) AS now_count 
               FROM order_item LEFT JOIN `order` ON order_id = `order`.id 
               WHERE user_id = {$user_id} AND state = 'pay' AND 
                   order_item.team_id = {$team['id']} AND order_item.option_id = {$option['id']}";
               
           $count_result = DB::GetQueryResult($sql, true);
           $purchased_count = $count_result['now_count'];
           //$quantity += $now_count;
        }

        $leftnum = ($option['per_number'] - $purchased_count - $new_quantity);
        if ($leftnum < 0) {

            die('{ok:false,error:"Cette offre étant limitée en quantité, l’article sélectionné ne peut pas être ajouté à votre panier."}');
        }        
    }
       
    $basket->Update($team_id, $new_quantity, $_POST['price'], $_POST['mode'], $option_id);
    $basket->Save();
    Session::Set('basket_updated',true);
    die("{ok:true}");
}

function RemoveFromBasket($user_id)
{
   
    $basket = new Basket($user_id);
    $option_id = abs(intval($_POST['option_id']));
    $basket->Remove($_POST['team_id'], $_POST['mode'], $option_id);
    $basket->Save();
    Session::Set('basket_updated',true);
	 
}

function UpdateDeliveryMethod($user_id)
{
	$basket = new Basket($user_id);
	$delivery = $_POST['delivery'];
	$basket->SetDelivery($delivery);
	$basket->Save();
	Session::Set('basket_updated',true);
	die("{ok:true}");
}

//Support for multiple options selection on Deal Options Screen 
function AddToBasketViaAjax($user_id)
{
    $jsData 	= array();
	$status 	= false;
	$jsError    = ""; 
	
	$basket = new Basket($user_id);
    $team_id = abs(intval($_POST['team_id']));
    $option_id = abs(intval($_POST['option_id']));
    $team = Table::FetchForce('team', $team_id);
	
    if( !$team )
	{
		$jsError = 'no such team';
		//die('no such team');
        //return;
    }
	else
	{
		$optionError = false;
		$upper_limit = $team['max_number'];
		if ( $option_id && $option_id >0 )
		{
			$option = Table::Fetch('team_multi', $option_id);
			if ($option)
			{
				$upper_limit = $option['max_number'];
			}
			else
			{
				//die('no such option'. $option_id);
				$optionError = 'no such option'. $option_id;
				$jsError = $optionError;
			}
		}
		else
		{
			$option_id  = false;
		}
		
		
		//if there is option error then ajax not processed
		
		if(!$optionError)
		{
			team_state($team);

			// <---- noneed pay where goods soldout or end 
			if ($team['close_time']) {
				$jsError = "Vous ne pouvez malheureusement plus procéder à l'achat de cette offre car celle-ci est terminée";
				//Session::Set('basket_closed', "Vous ne pouvez malheureusement plus procéder à l'achat de cette offre car celle-ci est terminée");
				//redirect(WEB_ROOT  . $_POST['ref'] );
			}
			// end ------->
			if ($upper_limit > 0 && ZTeam::GetStockLeft($team, $option) <= 0 )
			{ 
			   $jsError ="Le stock restant est pour le moment r&eacute;serv&eacute; car d'autres acheteurs sont en train de proc&eacute;der &agrave; la commande, veuillez r&eacute;-essayer plus tard";
			   //Session::Set('basket_failed', "Le stock restant est pour le moment r&eacute;serv&eacute; car d'autres acheteurs sont en train de proc&eacute;der &agrave; la commande, veuillez r&eacute;-essayer plus tard");
			   //redirect(WEB_ROOT  . $_POST['ref'] );
			}

			//is there a limit per user?
			
			if ($team['per_number']>0) 
			{
				 // There is a Limit per User
				$basket_quantity = $basket->GetQuantity($team_id, $option_id);     
				
				$purchased_count = 0;
				 // Currently selected quantity in the basket is lower than the Limit
				   // Check if User has previously purchased it
				$sql = "SELECT ifnull(SUM(order_item.quantity),0) AS now_count 
					FROM order_item LEFT JOIN `order` ON order_id = `order`.id 
					WHERE user_id = {$user_id} AND state = 'pay' AND order_item.team_id = {$team['id']}";

				$count_result = DB::GetQueryResult($sql, true);
				$purchased_count = $count_result['now_count'];
				//$quantity += $now_count;
				

				$leftnum = ($team['per_number'] - $basket_quantity - $purchased_count);
				
				if ($leftnum <= 0) {
					$jsError = "Cette offre étant limitée en quantité, l’article sélectionné ne peut pas être ajouté à votre panier.";
					//Session::Set('basket_failed', "Cette offre étant limitée en quantité, l’article sélectionné ne peut pas être ajouté à votre panier.");
					//redirect(WEB_ROOT  . $_POST['ref'] );        
				}        
			}
			
			// User is not violating the Limit Defined in Global Deal Settings
			// Now check if there is a Limit Defined in the Selected Deal Option
			
			if ($option && $option['per_number']>0) 
			{
				// There is a Limit per User in the Option
				$basket_quantity = $basket->GetQuantity($team_id, $option['id']);
				$purchased_count = 0;
				
				   // Currently selected quantity in the basket is lower than the Limit
				   // Check if User has previously purchased it
				   $sql = "SELECT SUM(order_item.quantity) AS now_count 
					   FROM order_item LEFT JOIN `order` ON order_id = `order`.id 
					   WHERE user_id = {$user_id} AND state = 'pay' AND 
						   order_item.team_id = {$team['id']} AND order_item.option_id = {$option['id']}";
					   
				   $count_result = DB::GetQueryResult($sql, true);
				   $purchased_count = $count_result['now_count'];
				   //$quantity += $now_count;
				

				$leftnum = ($option['per_number'] - $purchased_count - $basket_quantity);
				if ($leftnum <= 0) {
					$jsError = "Cette offre étant limitée en quantité, l’article sélectionné ne peut pas être ajouté à votre panier.";
					//Session::Set('basket_failed', "Cette offre étant limitée en quantité, l’article sélectionné ne peut pas être ajouté à votre panier.");
					//redirect(WEB_ROOT  . $_POST['ref'] );    
				}        
			}
			
			// its free from error then adding deal to basket
			
			if(!$jsError)
			{
				if ( $option && $option > 0 )
					$basket->Add($team['id'], 1, $team['team_price'], $_POST['mode'], $option['id']);
				else 
					$basket->Add($team['id'], 1, $team['team_price'], $_POST['mode']);
				
				$basket->Save();
				$status = true;
				Session::Set('basket_updated',true);
			}
			
		}
    
    //die('success');
	}
	
    $jsData['success'] = $status;
	$jsData['error'] = $jsError;

	$jsData = json_encode($jsData);

	echo $jsData;
	exit;
    
}

need_login();

if ( is_post())
{
    
    $action = $_POST['action'];
    
    if ( $action == "add" )
	{
        AddToBasket($login_user_id,$action);
	}
	else if ( $action == "addtobasketviaajax" )
	{
		 AddToBasketViaAjax($login_user_id);
	}
    else if ( $action == "update" )
        UpdateBasket($login_user_id);
    else if ( $action == "update_gift" ){
       $basket = new Basket($login_user_id);
       $team_id = abs(intval($_POST['team_id']));
       $option_id = abs(intval($_POST['option_id']));
       $basket->Update_gift($team_id,$option_id);
       $basket->Save();
       Session::Set('basket_updated',true);
       die("{ok:true}");
    }
    else if ( $action == "update_gift_remove" ){
       $basket = new Basket($login_user_id);
       $team_id = abs(intval($_POST['team_id']));
       $option_id = abs(intval($_POST['option_id']));
       $basket->Update_gift($team_id,$option_id, 'buy');
       $basket->Save();
       Session::Set('basket_updated',true);
       die("{ok:true}");
    }
    else if ( $action == "remove" )
        RemoveFromBasket($login_user_id);
	else if ( $action == "set_delivery" )
		UpdateDeliveryMethod($login_user_id);
    else
        die('{ok:false, "error": "unknown"');
    
    if(isset($_POST['ajax'])){
        die('{ok:true, "delete": "done"}'); 
    }
    else if(isset($_POST['deal_option'])){
        
        $bkt = new Basket($login_user_id);
		$basket_items = $bkt->GetItems();
        
        if( empty($basket_items)){
            redirect(WEB_ROOT);
        }
        
        $team_ids = Utility::GetColumn($basket_items, 'team_id');
        $option_ids = Utility::GetColumn($basket_items, 'option_id');
        $teams = Table::Fetch('team', $team_ids);
        $options = Table::Fetch('team_multi', $option_ids);
        
        include template_ex('content_basket');
    }
    else{
        $bkt = new Basket($login_user_id);
		$basket_items = $bkt->GetItems();
        if( empty($basket_items)){
			//die('???');
            redirect(WEB_ROOT);
        }
        
        $team_ids = Utility::GetColumn($basket_items, 'team_id');
        $option_ids = Utility::GetColumn($basket_items, 'option_id');
        $teams = Table::Fetch('team', $team_ids);
        $options = Table::Fetch('team_multi', $option_ids);
        
        include template_ex('content_basket');
    }
    
    
}
else
{
	$bkt = new Basket($login_user_id);
	$basket_items = $bkt->GetItems();
        if( empty($basket_items)){
            redirect(WEB_ROOT);
        }
        
        $team_ids = Utility::GetColumn($basket_items, 'team_id');
        $option_ids = Utility::GetColumn($basket_items, 'option_id');
        $teams = Table::Fetch('team', $team_ids);
        $options = Table::Fetch('team_multi', $option_ids);
        
        include template_ex('content_basket');
}

?>
