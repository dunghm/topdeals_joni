<?php
require_once(dirname(dirname(__FILE__)). '/app.php');

$invitecontest = current_invitenwin();
require_once('InviteContest.php');


$pagetitle = "Grand concours de l’été sur TopDeal.ch";
$seo_description = "Gagnez un bon pour un iPhone 5 dès sa sortie !";
if ( !$invitecontest )
   die("currently no contest in progress");
 
$command = $_REQUEST['cmd'];


if ( $command == "view" )
    return InviteContest::ViewContest();
else if ( $command == "terms" )
        return InviteContest::Terms();
else if (  is_login() )
{
    if ( $command == "subscribe" )
        return InviteContest::Subscribe();
    else if ( $command == "invite" )
        return InviteContest::Invite();
    else if ( $command == "stats" )
        return InviteContest::Stats();
    
    
    if ( InviteContest::HasUserSentInvites($login_user['id'], $invitecontest['id']) )
    {
        return InviteContest::Stats();
    }
}
    

InviteContest::Main();
?>
