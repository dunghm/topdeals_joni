<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class InviteContest
{
    static public function Main()
    {
         global $INI;
        global $invitecontest;
        global $login_user;
	global $pagetitle, $seo_description;
        
        $login_user_id = $login_user['id'];
        
        if ( $login_user['first_name'] != "" || $login_user['last_name'] != "" )
            $user_name = $login_user['first_name']. " ".  $login_user['last_name'] ; 
        else if ( $login_user['username'] != "" )
             $user_name = $login_user['username'] ;
        else
             $user_name = $login_user['email'] ;
        
        include template('invitecontest/invitecontest_index');
    }
    
    static public function ViewContest()
    {
        
         global $INI;
       $ic_id = $_GET['id'];
       $ic_seo = $_GET['seo'];
       
       if ( $ic_seo )
           $ic = Table::Fetch('invitecontest', $ic_seo, 'seo_name');
       else if ( $ic_id ) 
           $ic = Table::Fetch('invitecontest', $ic_id);
       
       if  ( ! $ic )
           return self::Main();
       
       $invitecontest = $ic;
       
        //var_dump($invitecontest);
        include template('invitecontest/invitecontest_view');
    }
    
   
    static public function IsUserSubscribed($user_id, $ic_id)
    {
         global $INI;
      return (  Table::Count('ic_subscribers', array( 
                'invitecontest_id' => $ic_id, 
                'user_id' => $user_id,
           // 'status' => 1
          )) > 0 );
    }
    
    static public function HasUserSentInvites($user_id, $ic_id)
    {
         global $INI;
      return (  Table::Count('ic_subscribers', array( 
                'invitecontest_id' => $ic_id, 
                'user_id' => $user_id,
            'status' => 2
          )) > 0 );
    }
    
     static public function SubscribeMe($user_id, $ic_id)
    {
          global $INI;
          
         $rec = new Table('ic_subscribers');
         $rec->SetPk('id');
         $rec->invitecontest_id = $ic_id;
         $rec->user_id = $user_id;
         $rec->status = 1;
         $rec->subscription_time = time();
         
         $rec->update(array('invitecontest_id','user_id', 'status', 'subscription_time'));
     }
    
     static public function Subscribe()
    { 
        global $invitecontest;
        global $login_user;
        global $INI;
         	global $pagetitle, $seo_description;
        
        $login_user_id = $login_user['id'];
        
        if ( $login_user['first_name'] != "" || $login_user['last_name'] != "" )
            $user_name = $login_user['first_name']. " ".  $login_user['last_name'] ; 
        else if ( $login_user['username'] != "" )
             $user_name = $login_user['username'] ;
        else
             $user_name = $login_user['email'] ; 
              
        if ( self::IsUserSubscribed($login_user['id'], $invitecontest['id']))
        {
            $total = Table::Count('ic_referals', array('refer_by'=> $login_user['id'],
                'invitecontest_id'=> $invitecontest['id'],
                ));
            return self::Stats();
        }
       
       // if (is_post())
        {
            // user wish to subscribe
            self::SubscribeMe($login_user['id'], $invitecontest['id']);
            include template('invitecontest/invitecontest_personal');
            return;
        }
        //var_dump($invitecontest);
        //self::SubscribeMe($login_user_id, $invitecontest['id']);
    }
    
     static public function Invite()
    {
        global $invitecontest;
        global $login_user;
        global $INI;
         	global $pagetitle, $seo_description;
        
        $login_user_id = $login_user['id'];
              
          if ( !is_login() || ! self::IsUserSubscribed($login_user['id'], $invitecontest['id']) )
             return self::Main();
          
        if ( is_post() )
        {
            $from = $login_user['email'];
            $subject = utf8_encode("{$_POST['tname']} t'invite � rejoindre TopDeal.ch");
            //$subject = "Invitation to join Topdeal.ch";
            $message = utf8_encode("{$_POST['tname']}  t'a envoy� ce message:

{$_POST['message']}

{$INI['system']['wwwprefix']}/r.php?r={$login_user_id}");

            //$message = $_POST['message'];
            foreach($_POST['friend'] as $email)
            {
                if (filter_var($email, FILTER_VALIDATE_EMAIL))
                    $bcc[] = $email;
            }
            
            if ( count($bcc) > 0 )
            {
                mail_contest_invite($from, $bcc, $subject, $message);
                
                $options = array ( 
                    'one' => true, 
                    'condition' => array(
                        'invitecontest_id'=>$invitecontest['id'],
                        'user_id' => $login_user_id
                        ),
                   );
                
                DB::Query("update ic_subscribers set status = 2 where user_id = {$login_user_id} AND
                invitecontest_id = {$invitecontest['id']}");
               /* $subscriber = DB::LimitQuery("ic_subscribers", $options);
                $subscriber['status'] = 2;
                Table::UpdateCache("ic_subscribers", $subscriber['id'], 
                        array("status" =>2));
                */
                include template('invitecontest/invitecontest_confirm');
                return;
            }
        }
        //var_dump($invitecontest);
        include template('invitecontest/invitecontest_personal');
    }
    
    static public function Stats()
    {
         global $INI;
         	global $pagetitle, $seo_description;
        global $invitecontest;
        global $login_user;
       $login_user_id = $login_user['id'];
       if ( $login_user['first_name'] != "" || $login_user['last_name'] != "" )
            $user_name = $login_user['first_name']. " ".  $login_user['last_name'] ; 
        else if ( $login_user['username'] != "" )
             $user_name = $login_user['username'] ;
        else
             $user_name = $login_user['email'] ;
        
       
        if ( !is_login() || ! self::IsUserSubscribed($login_user['id'], $invitecontest['id']) )
            return self::Main();
          
        $condition = array( 
		'refer_by' => $login_user_id, 
		'invitecontest_id' => $invitecontest['id'],
		
		);
        $count = Table::Count('ic_referals', $condition) ;
        //var_dump($invitecontest);
        include template('invitecontest/invitecontest_stats');
    }
    
     static public function Terms()
    {
        global $INI;
        global $invitecontest;
	global $pagetitle, $seo_description;
        //var_dump($invitecontest);
        include template('invitecontest/invitecontest_terms');
    }
    
}
?>
