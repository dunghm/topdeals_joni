<?php
require_once(dirname(__FILE__) . '/app.php');

if(!$login_user){
	redirect( WEB_ROOT . '/account/login.php');	
}

$mysqldate = date( 'Y-m-d' );
$login_user_id = ZLogin::GetLoginId();
$login_user = Table::Fetch('user', $login_user_id);
$email = $login_user['email'];

$subs = array(
		'user_email' => $email,
		'date' => $mysqldate,
	);
	

$count = Table::Count('daily_contest', $subs);	

if($count==0) {
	$result = DB::Insert('daily_contest', $subs);
}

die(include template('daily_contest_subscribe'));

?>