$(function(){
	var m = 0;
	$('.navigation li').each(function(){ m += $(this).innerWidth();});
	$('.navigation ul').width(m);
	m = 0;
	$('.dashboard li').each(function(){ m += $(this).innerWidth();});
	$('.dashboard ul').width(m);
	if ($('.filter li > form').length > 0){
		$('.filter li').css('background','transparent');
	}
	/* For partner backend*/
	var right = $("#content").css("margin-right");
	$("#verify-coupon-id").css("right",right);
	$(window).resize(function(){
		var right = $("#content").css("margin-right");
		$("#verify-coupon-id").css("right",right);
	});
	setInterval(function(){
		$("#verify-coupon-id").fadeOut("slow").fadeIn("slow");
	}, 10000);
})
/*
$(function(){
    //Get our elements for faster access and set overlay width
    var div = $('div.navigation'),
                 ul = $('div.navigation ul'),
                 // unordered list's left margin
                 ulPadding = 15;

    //Get menu width
    var divWidth = div.width();
    var total = 0;
    ul.find('li').each(function(){
    	total += $(this).outerWidth();
    });
    ul.width(total);
    //Remove scrollbars
    div.css({overflow: 'hidden'});

    //Find last image container
    var lastLi = ul.find('li:last-child');
    //When user move mouse over menu
    div.mousemove(function(e){
      //As images are loaded ul width increases,
      //so we recalculate it each time
      if($.fn.scrollTimer) clearTimeout($.fn.scrollTimer);
      var ulWidth = lastLi[0].offsetLeft + lastLi.outerWidth() + ulPadding;
      
      var left = (e.pageX - div.offset().left) * (ulWidth-divWidth) / divWidth;
      div.scrollLeft(left);
    }).mouseleave(function(){
    	$.fn.scrollTimer = setTimeout(function(){
	    	div.animate({scrollLeft: 0}, 500);
    	},1000);
    });
});
*/