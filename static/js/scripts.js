(function ( $ ) {
 
    $.fn.progressbar = function( options ) {
 
        var settings = $.extend({
            // These are the defaults.
            unit: "%"
        }, options );
 
        return this.each( function() {
			var bar = $('.progress-bar',this);
			var indicator = $('.progress-icon',this);
			var val = bar.attr('aria-valuenow');
			
			if(indicator) {

			indicator.css('left', val + settings.unit);
			}
			bar.css('width', val + settings.unit);
        });
 
    };
 
}( jQuery ));

var fixBottom = function ($element) {
	var maxHeight = 0;
	
	$element.each(function(e) {
		maxHeight = maxHeight > $(this).height()?maxHeight:$(this).height();				
	});
	$element.each(function(e) {
		if (maxHeight > $(this).height()) {
			$(this).css('padding-top', maxHeight-$(this).height());
		}
	});
}

$( window ).load(function() {
	$('.product-list .row').each(function(e) {
		fixBottom($(this).find('.product-item > h3'));
	});
	
	$('.container.deals .row').each(function(e) {
		fixBottom($(this).find('.title'));
	});	
});

function closepopup(){
	$('.popup').hide();
}

function createMask(){
    var w = $(window).width();
    var h = $(window).height()
    $('body').append("<div id='mask' style='position: fixed; top: 0; left: 0; z-index: 99999; width:" + w + "px; height:" + h + "px'></div>");
};
function removeMask(){
    $('#mask').remove();
}
function mSubscribe(){
	var options = {
		success: alertSubscribeOk
	};
	if(!validate('m-subscription','email')) return false;
	$('#m-subscription').ajaxSubmit(options);
}
function loadPage(url){
	$('#page > div').animate({ opacity: 0 }, 500);
	$('#page').load('//' + url + ' .page-content', function(){
		$('#page .page-content').animate({ opacity: 1 }, 500);	
		eval($('#category-script').text());					
	});
}
function loadDeal(url){
	$('#page > div').animate({ opacity: 0 }, 500);
	$('#page').load('//' + url + ' .products', function(){
		$('#page .products').animate({ opacity: 1 }, 500);	
		eval($('#category-script').text());
		$("html, body").animate({ scrollTop: 0 }, "slow");
	});
}
$(document).ready(function(){	
	if(isMobile()  || ((navigator.userAgent.match(/iPad/i) != null) && (Math.abs(window.orientation) !== 90))){
		setTimeout(function(){
			$('body').prepend($('#top-banner'));
			$('body').prepend($('.posfixed'));
			//$('#wrapper, .main-menu #navigation, #mobile-category').width($(window).width());
			var rW = $('.main-menu .brand').width();
			$('.main-menu .brand.account').css('right',rW + 'px');		
			$('#top-banner').show();
		},500);
		setTimeout(function(){
				$('#top-banner').simpleSidebar({
						opener: '.menu-btn',
						wrapper: '#wrapper',
						sidebar: {
								align: 'left',
								closingLinks:'.close-mobilemenu'
						},
						sbWrapper: {
								display: true
						}
				});
		}, 1000);

//		$('.menu-btn.hidden-lg').click(function(){
//			$('body').toggleClass('mobile');
//			if($('body').hasClass('mobile')){
//				$('#top-banner').css('left', 0);
//				$('#wrapper').css('left','260px');
//				$('.posfixed').css('left','260px');
//			}else{
//				$('#top-banner').css('left', '-260px');
//				$('#wrapper').css('left','0');
//				$('.posfixed').css('left','0');
//			}
//		});	
		 
		$('#video').hide();
		var $startSlide = ($('#mobile-category a.current').length > 0)?$('#mobile-category a.current').index() : 0;
		var slider = jQuery("#mobile-category .carousel-menu").bxSlider({
			startSlide: $startSlide,
			controls: false,
			pager: false,
			onSlideAfter: function($slideElement, oldIndex, newIndex){
				var currentCat = $slideElement.attr('href');				
				if(currentCat == '/') {
					$('body').addClass('homepage');
					$('#mobile-category').addClass('home');
				}
				var url = window.location.host + currentCat + '?bust='+(new Date()).getTime();
				loadPage(url);
			}
		});
    $('#mobile-category .navi').on('click', function(e){
			e.preventDefault();			
      if($(this).hasClass('next')){				
        slider.goToNextSlide();
      }else{
        slider.goToPrevSlide();
      }
    })
		if(($('#home-content').length > 0) || ($('#category-content').length > 0) || ($('.products').length > 0)){
			$(document).on('click', '.navi-deal > span', function(e){
				var btn = $(e.target);
				var prevDeal = $('input[name="prev_deal"]').val();
				var nextDeal = $('input[name="next_deal"]').val();
				var url = window.location.host + '/team.php?bust='+(new Date()).getTime()+'&id=' ;
				if(btn.hasClass('glyphicon-chevron-left') && (prevDeal != '')){
					url += prevDeal;
				}else if(btn.hasClass('glyphicon-chevron-right') && (nextDeal != '')){
					url += prevDeal;          
				}
				loadDeal(url);
			});
//      $(function() {
//        $("#page").swipe( {					
//          swipeLeft:function(event, direction, distance, duration, fingerCount) {
//						if($(event.target).hasClass('product-images') || $(event.target).hasClass('pager-images'))
//							return false;
//            if($(this).find('.products').length > 0) {
//              var prevDeal = $('input[name="prev_deal"]').val();
//              if(prevDeal != '') {
//                var url = window.location.host + '/team.php?bust='+(new Date()).getTime()+'&id=' + prevDeal;
//                loadDeal(url);
//              }
//            }else if($(this).find('.page-content').length > 0){
//              slider.goToNextSlide();
//            }
//          },
//          swipeRight:function(event, direction, distance, duration, fingerCount) {
//						if($(event.target).hasClass('product-images') || $(event.target).hasClass('pager-images'))
//							return false;
//            if($(this).find('.products').length > 0) {
//              var nextDeal = $('input[name="next_deal"]').val();
//              if(nextDeal != '') {
//                var url = window.location.host + '/team.php?bust='+(new Date()).getTime()+'&id=' + nextDeal;
//                loadDeal(url);
//              }
//            }else if($(this).find('.page-content').length > 0){
//              slider.goToPrevSlide();
//            }
//          },
//          threshold:120
//        });
//      });
    }
		$('#navigation').parent().removeClass('container');
    
		$(window).on('scroll', function(){
//      if(!$('input[name="recherche"]').is(':focus')){ //On mobile if user focus on search, don't close menu when scrolling
//        $('#top-banner').slideUp(500,'easeInOutExpo');
//      }      
			if($(this).scrollTop() == 0) {				
				$('.posfixed').removeClass('shadow');
			}else{
				$('.posfixed').addClass('shadow');
			}
			
			if($(window).scrollTop() + $(window).height() == $(document).height()) {
				$('.products .navi-deal').removeClass('fixed');
			}else{
				$('.products .navi-deal').addClass('fixed');
			}
      if($('#checkout-password').length <= 0){
        $( ".popup #popup-close" ).trigger('click');          
      }			
		});
		$('.products .navi-deal > span:last').tooltip({placement: 'right',trigger: 'manual'}).tooltip('show');		
		$('.products .navi-deal > span:first').tooltip({placement: 'left',trigger: 'manual'}).tooltip('show');
		$('.products .navi-deal > span').on('click',function(){$(this).tooltip('destroy');});
		$("#top-banner").prependTo(".main-menu > div");
		$('.main-menu').parent().addClass('posfixed');
    if($('.checkout-step').length > 0 || $('#page > div.account').length > 0) {
      $('#mobile-category').remove();
      $('#page').css('margin-top', '50px');  
    }else{
      $('#page').css('margin-top', '120px');  
    }
    $('#mobile-assist').click(function(e){
      e.preventDefault();
      $('#zenbox_tab').trigger('click');
    })
	$('#navigation').insertBefore('#top-banner');
  }
  else if((navigator.userAgent.match(/iPad/i) != null) && (Math.abs(window.orientation) === 90)){
		$('#video').hide();
		$('#top-nav').addClass('list-inline');
		$('.homepage .banner').css('background','#ffffff').height(228);
		$('.home-logo img').attr('src','/static/images/top.png');
		$('#mask').remove();
	}else{
		$('.homepage .banner').css('background','transparent');
		$('#top-nav').addClass('list-inline');
	}
	$('.sidebar-menu').parent().removeClass('hidden-sm hidden-xs');
	
	$( ".popup #popup-close" ).on( "click", function() {
		$('.popup').slideUp(800,'easeInOutExpo', function(){
			$.cookie('closed', 1, { expires: 1, path: '/' });
			$('#top-banner').removeClass('has-popup');
    });
	});
	
	$('.category .deals .title a, .top-menu .notification .account a').tooltip();
	$('.progress-wrapper').progressbar();	
	
	
	if ($('.sliders .slider-content')[0]) {
		$('.sliders .slider-content').cycle();
	}
  
	/* Show slideshow on Homepage */
	$('.findout, .slideshow #close').click(function(e){
		e.stopPropagation();
		e.preventDefault();
		$(window).scrollTop(0);
		$('.slideshow').slideToggle(800,'easeInOutExpo');
	})
	if($('#action-bar').length > 0){
		var menu = $('#action-bar');
		var thumbbox = $('#action-bar').next();
		var top = $('#action-bar').offset().top;
		var marginTop = $('#action-bar').height();
		var scrollBar = $('#scroll-bar');
		var marker = parseInt($('.products .buy-btn').offset().top + $('.products .buy-btn').height());
		$(window).scroll(function(){
			var windowScrolltop = $(this).scrollTop();
			if(windowScrolltop > top){
				$(menu).addClass('pos-fixed');
				if(isMobile()  || (navigator.userAgent.match(/iPad/i) != null)){
					$('.posfixed').fadeOut();
				}else{
					thumbbox.css('margin-top', marginTop + "px");
				}
			} else if(windowScrolltop <= top){
				$(menu).removeClass('pos-fixed');
				thumbbox.removeAttr('style');
				if(isMobile()  || (navigator.userAgent.match(/iPad/i) != null)){
					$('.posfixed').fadeIn();
				}
			}
			if(windowScrolltop + $(window).height() >= ($(document).height() - 55) ) {				 
				$('.deal-navi').addClass('stop-float');
			}else{
				$('.deal-navi').removeClass('stop-float');
			}
			if(windowScrolltop > marker){
				scrollBar.fadeIn();
			}else{
				scrollBar.fadeOut();
			}
			//Close slideshow when scroll
			var bannerTop = $('.banner').offset().top;
			var slideHeight = $('.slideshow').outerHeight();
			if($(this).scrollTop() >= bannerTop && $('.slideshow').is(":visible") ){
				var wtop = $(this).scrollTop();
				$('.slideshow').hide();
				$(window).scrollTop(0);
			}
		});
	}
	//Price box click able https://notableapp.com/posts/968110
	$('.deals').on('click','.deal-box', function(){
			window.location = $(this).find('a').attr('href');
	});
  
	if($('.category-avatar').length > 0){
		$('.home-category .col-md-4').matchHeight();
	}
  
    //Product detail https://notableapp.com/posts/963804	
	
	if($('.vous-aimerez').length > 0) {
		var data = $('.vous-aimerez').html().split('<br>');
		if (data.length > 0) {
			var html = '<ul class="list-unstyled list-01">';
				for(var i = 0, len = data.length ; i < len ; i++){ // Remove the empty item in the end of string
				if(data[i] == '') continue;
				html += '<li> <h3>' + (i + 1) + '</h3><p>' + data[i] + '</p></li>';
			}
			html += '</ul>';
			$('.vous-aimerez').html(html);
		}
	}
	
	if($('.vous-aimerez-title').length > 0) {
		setTimeout(function(){
			var vh    = $('.vous-aimerez-title').height();
			var pr    = $('.price-box:first').height();
			var prTop = $('.price-box:first').position().top;
			var mrTop = prTop + (pr - vh) + 'px';
			$('.vous-aimerez-title').css('margin-top', mrTop);
		}, 1000);		
	}	
	//Fix checkbox for IE8 
	$('input[type=checkbox]').change(function(){
		$(this).next('label').toggleClass('checked');
	});
});