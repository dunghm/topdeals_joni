(function($){
 
    $.fn.extend({ 
        
        autoHideLabel: function() {
 	    var defaults = {
                labelClass: '.input-label'
            };
	    var options = $.extend(defaults, options);
            return this.each(function() {             
             	var label = $(this).find(options.labelClass);
		var input = $(this).find('input, textarea');
		var labelVal = label.val();
		if($.browser.webkit && !window.chrome) input.css('margin-top','-40px');
		input.focus(function(){
			if(labelVal == $(this).val() || $(this).val() == '') label.css({'text-indent':'-9999px','visibility':'hidden'});
		});
		input.blur(function(){
			if(labelVal == $(this).val() || $(this).val() == '') label.css({'text-indent':'','visibility':'visible'});
		});
            });
        }
    });
})(jQuery);

function initInput(){
	$('#frmInviteFriends #btnAdd').click(function(event){
		event.preventDefault();
		var html = '<div class="input-container medium-input">';
		html += '<div class="input-label">Adresse email 2</div>';
		html += '<input type="text" name="friend[]" value=""><a href="#" class="delete_textfield" title="Retirez entrée"><img src="'+ WEB_ROOT +'/static/img/icon_textfield_delete.png"/></a></div>';
		var element = $(html);
		$(this).parent().before(element);
		updateLabel();
		$('#frmInviteFriends .delete_textfield').click(function(event){
			event.preventDefault();
			$(this).parent().remove();
			updateLabel();
		});
		$('.input-container').autoHideLabel();
		function updateLabel(){
			$('#frmInviteFriends .email-container .input-label').each(function(index){
				$(this).html("Adresse email " + (index + 1));
			});
		}
	});
}

$(document).ready(function(){
	initInput();
});
