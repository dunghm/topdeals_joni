// Draw stock bar

function drawStockbar(p, id){
    var color = "red";
    if(p > 25 && p <= 75) 
      color = "#f04e1e";
    else if (p > 75)
      color = "#85C422";
    var radius = 67;
	var lineWidth = 40;
    var canvas = document.getElementById(id);	
    var context = canvas.getContext("2d");
	clearCanvas(context, canvas);
    var x = canvas.width / 2;
    var y = canvas.height / 2 + 2;
    var startAngle = 0.62*Math.PI;
    var endAngle = ((p*1.76)/100) * Math.PI + startAngle;
    var counterClockwise = false;

    context.beginPath();
    context.arc(x, y, radius, startAngle, endAngle, counterClockwise);
    context.lineWidth = lineWidth;
    context.strokeStyle = color;
    context.font = "36px Gudea-Regular";
    context.fillStyle = "#fff";
    context.textAlign = "center";
    context.fillText(p + "%", x, y+10);    
    context.stroke();

// draw title of stock bar
    var ctx_title = canvas.getContext("2d");
	ctx_title.font = "15px Gudea-Bold";
    ctx_title.fillStyle = "#8f908b";
    ctx_title.fillText("Stock", x, (y*2 - 15));  
}


function clearCanvas(context, canvas) {
  context.clearRect(0, 0, canvas.width, canvas.height);
  var w = canvas.width;
  canvas.width = 1;
  canvas.width = w;
}


function autoDraw(p){
	speed = 60;
	i = 100;
	stockTimeout = setInterval(function(){

		if(i <= p) clearTimeout(stockTimeout);
		drawStockbar(i,'stock-bar');
		i--;
	}, speed);
}