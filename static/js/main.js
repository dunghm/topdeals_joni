function nextdealCountdown(){
	var d = new Date();
	var t = 24*60*60;
	var day = d.getDay();
	if(day == 5) {
		t = 72*60*60;
	} else if(day == 6) {
		t = 48*60*60;
	}
	var current = d.getHours()*60*60 + d.getMinutes()*60 + d.getSeconds();
	var remain = t - current;
	var h = parseInt(remain/(60*60)) ;
	var m = parseInt((remain - h*60*60)/60);
	var s = remain - (h*60*60 + m*60);
	h = (h < 10)? "0" + h : h;
	m = (m < 10)? "0" + m : m;
	s = (s < 10)? "0" + s : s;
	var html = "<table>";	
	html    += "<tr><td colspan='5' class='title'>Prochain Deal dans</td></tr>";
	html    += "<tr class='countdown_content'><td>" + h +"</td><td>:</td><td>" + m +"</td><td>:</td><td>" + s +"</td></tr>";
	html    += "<tr class='countdown_header'><td>Heures</td><td>&nbsp;</td><td>Minutes</td><td>&nbsp;</td><td>Secondes</td></tr>";
	html    += "</table>";
	$('#nextdeal').html(html);
}
jQuery(document).ready(function($){
	if ($(document).find('#show-connect')){
		var cartNo = parseInt($('.cart-info span').text());
		if(cartNo > 0) $('.cart-info span').css('color','#F04E1E');
		$('#show-connect').click(function(){
			if ( !($('#connect-box').is(':visible'))){
				close_connect();
				$(this).addClass('current');
				$('#connect-box').fadeIn(500);
				$('.connects-box').fadeIn(500);
				return false;
			}
			else {
				close_connect();
			}
		});
/*		
		$('#site-title').hover(function(){
			$(this).animate({opacity: 1}, "fast");
		},
		function(){
			$(this).animate({opacity: 0.6}, "fast");
		});
*/		
		$('#forgot-box-show').click(function(){
			if ( !($('#forgot-box').is(':visible'))){
				close_popup();
				$('#forgot-box').fadeIn(500);
				$('.connects-box').fadeIn(500);
				return false;
			}
			else {
				close_connect();
			}
		});
		
		$('#back-connect').click(function(){
			$('.error-text').hide();
			if ( !($('#connect-box').is(':visible'))){
				close_popup();
				$('#connect-box').fadeIn(500);
				$('.connects-box').fadeIn(500);
				return false;
			}
			else {
				close_connect();
			}			
		});		
	}
	if ($(document).find('#show-credits')){
		$('#show-credits').click(function(){
			if ( !($('#register-box').is(':visible'))){
				close_connect();
				$(this).addClass('current');
				$('#register-box').fadeIn(500);
				$('.connects-box').fadeIn(500);
				return false;
			}
			else {
				close_connect();
			}
		});		
	}

	//Hover 
	$('.not-connect .aside-box h3').hover(function(){
		$(this).addClass('hover');//("background-color","#70b10a");
	}, function(){
		$(this).removeClass('hover');
	});
	
	//Login form validation and submission
	$('#login-form input').attr('autocomplete','off');
	$('#login-form').submit(function(){
		var url = ROOT + "/ajax/validator.php"; 
		var email = $('input[name=email]',this);
		var password = $('input[name=password]',this);
		var data = {
			email : email.val(),
			password : password.val(),
			n: 'prelogin'
		}

		if(email.val() ==''){
			highlight(email, "Utilisateur est obligatoire");
			return false;			
		}

		if(password.val() ==''){
			highlight(password, "Mot de passe est obligatoire");
			return false;			
		}
		$(this).attr("disabled", "disabled");
		makeOverlay();
		$.post(url,data, function(d) {
			var error = d.data ;
			$('input',this).removeClass('error-input');
			setTimeout(function(){
				$('#mask').remove();
			},2000);
			if(error == 100) {
				highlight(email, 'Utilisateur inconnu', true);
				password.removeClass('error-input');
			} else if(error == 200){
				highlight(password, 'Mot de passe erroné', true);
				email.removeClass('error-input');
			} else {
				$('input',this).removeClass('error-input');
				$('#login-form').after("<form action='"  + ROOT + "/account/login.php' method='post' style='display:none;' id='temp-form'><input name='email' value='" + email.val() + "'/><input name='password' type='password' value='" + password.val() + "'/></form>");
				$('#temp-form').submit();	
			}
		},'json');
		return false;
	});
	function makeOverlay(){
		$('body').append("<div id='mask' style='top:0;left;0;position:fixed;z-index:999999; width:"+ $(window).width() +"px; height:"+ $(window).height() +"px'><span></span></div>")
	}
	function highlight(input, msg){
		input.addClass('error-input');
		input.attr('placeholder',msg);
		if((arguments.length > 0) && (typeof(arguments[2])) != undefined) input.focus();
	}

	//Hover 
	$('.acheter-box h3 a').hover(function(){
		$(this).animate({ color: "#70b10a" }, "fast");
	}, function(){
		$(this).animate({ color: "#85C422" }, "fast");
	});
	
	$('#top-nav li:not(.current) a').hover(function(){
		$(this).animate({ color: "#85C422" }, "fast");
	}, function(){
		$(this).animate({ color: "#ffffff" }, "fast");
	});
	$('.thumb-box:not(.deal-detail .thumb-box)').hover(function(){
		$(this).animate({ opacity: 1}, "fast");
	}, function(){
		$(this).animate({ opacity: 0.8 }, "fast");
	});
	/*
	$('#newsletter-frm input[name=email]').keypress(function (e) {
		e.preventDefault();
		if (e.which == 13) {
		$('form#login').submit();
		}
		return false;
	});
	*/
	//Open when register links
	var hash = top.location.hash;
	if(hash == '#register'){
		$('#show-credits').trigger('click');
		$('#show-credits').addClass('current');
	}

	//Open when register links
	var hash = top.location.hash;
	if(hash == '#signin'){
		$('#show-connect').trigger('click');
		$('#show-connect').addClass('current');
	}

	if ($(document).find('#close-popup')){
		$('#close-popup').click(function(){
			close_connect();
		});		
	}
	if ($(document).find('.filtres-box span')){
		$('.filtres-box span').click(function(){
		$('.filtres-box ul').fadeIn(500);
		});		
	}
	if ($(document).find('.filtres-box span')){
		$('.filtres-box .close-filtres').click(function(){
		$('.filtres-box ul').fadeOut(500);
		});		
	}
	//$('.deal-box .prices-box').animate({opacity: 0.9 });
	$('.deal-box .percent-box').hover(function(){
		$(this).prev('.prices-box').animate({opacity: 1 });
		},function(){
		$('.deal-box .prices-box').animate({opacity: 0.8 });
	});
	
	//close warning box on top
	$('.warning-top .close-btn').click(function(){
		$('.warning-top').fadeOut(500);
	});
	//delete purchase
	$('#purchase-list .purchase-product .delete-btn').click(function(){
		if (!$(this).hasClass('displayings')) {
		$(this).addClass('displayings');
		}
		else {
			alert('Do you want delete this purchase?');
		}
		return false;
	});
	//show basket
	$('#branding .cart-info').click(function(){
		var cartQty = parseInt($('.cart-info').text());
		if ( !($('#basket-detail').is(':visible')) && (cartQty > 0)){
				$('#basket-detail').fadeIn(500);
				return false;
			}
			else {
				$('#basket-detail').fadeOut(500);
				return false;
			}
	});
	$('#branding .close-basket a').click(function(){
		$('#basket-detail').fadeOut(500);
	});
	$('#popOverlay .close-btn').click(function(){
		close_popOverlay();
	});
	$('#popOverlay .cancel-link').click(function(){
		close_popOverlay();
	});
/*  
	$('#acheter-box a').click(function(){
		$('#popOverlay').fadeIn(100);
	});		
*/
	if ($(document).find('#popOverlay')){
		reset_popOverlay();			
	}

	//Auto remove input default value 	
	$('input[type=text]:not(.edit-quantity input, .profile-info input[name=url_txt], .content-panier input,  #login-form input), input[type=password]').each(function(){
		$(this).attr('defaultValue',$(this).val());
	});
	$("body").on('focus','input[type=text]:not(.edit-quantity input, .profile-info input[name=url_txt], .content-panier input,  #login-form input), input[type=password]',function(){
		$(this).val('');
	});
	$("body").on('blur','input[type=text]:not(.edit-quantity input, .profile-info input[name=url_txt], .content-panier input,  #login-form input), input[type=password]',function(){
		if($(this).attr('type') == 'password') return;
			var defaultVal = $(this).attr('defaultValue');
			var val = $(this).val();
			if( val == '') {
				$(this).val(defaultVal);
			}else{
				$(this).attr('defaultValue',val);
			}
	});
	/*
	$('input[type=text]:not(.edit-quantity input, .profile-info input[name=url_txt]), input[type=password]').on({
		'focus': function(){
			$(this).val('');
		},
		'blur': function(){
			if($(this).attr('type') == 'password') return;
			var defaultVal = $(this).attr('defaultValue');
			var val = $(this).val();
			if( val == '') {
				$(this).val(defaultVal);
			}else{
				$(this).attr('defaultValue',val);
			}
		}
	});
	*/
	//Upload avatar
	$('.avatar-box img,.avatar-box a').click(function(e){
		e.preventDefault();
		$('#upload_image').trigger('click');
	});
	
	//Show hide address in checkout page
	$('.add-delivery-address').on('click','input[type=radio]',function(){
		if($('.add-delivery-address .edit-item-address').is(':visible')) $('.add-delivery-address .edit-item-address').slideUp('slow');
		$(this).parent().find('.edit-item-address').slideDown('slow');
	});
	
	if($('input[name=chk_delivery_address]').is(':checked') == false){
		$('.add-delivery-address').hide();
	}
	
	$("#add_address").click(function(e){
		e.preventDefault();
		showPopup('add-address-form');
	});
	
	//Add more promo code

	$('.add-address').click(function(e){
		e.preventDefault();	
		if($(this).parents('.promo-code').length > 0){
			addPromoCode($(this).prev().val());
		} else{
			var input = $(this).prev().clone().wrap('<p>').parent().html();
			$(this).prev().before(input + '<div class="delete-btn"><span></span></div>');
		}
	}); 
	//Call to sever to get promo code
	function addPromoCode(val){
            var order_id = $("input[name='order_id']").val();
            var query = "id=" + order_id + "&action=cardcode&cid="+ val;
            $.getJSON("/ajax/order.php", query, function(data){
                   if ( ! data.error )
					{						
						var promoVal = parseInt(data.data.data[2].value);
						var totalVal  = parseInt($('.total-paid .purchase-total h3').text());
						$('.promo-code .purchase-total').html("-" + promoVal + "<span>CHF</span>");
						var newTotal = totalVal - promoVal;
						if ( newTotal < 0 )
							newTotal = 0;
						$('.total-paid .purchase-total h3').html((newTotal) + "<span>CHF</span>");
					}
					else
					{
						alert(data.data);
					}
            }, 'json');
	}
	$("body").on('click','.delete-btn:not(.icon-box .delete-btn,.avatar-box .delete-btn)', function(){
		$(this).prev().remove();
		$(this).remove();
	});
	
	$('.payement-mode img[alt="Facture"]').click(function(){
		showPopup('credit-card');
	});
	$('.payement-mode img[alt="visa"]').click(function(){
		showPopup('credit-card');
	});	
	$('.payement-mode img[alt="Postfinance"]').click(function(){
		showPopup('Postfinance');
	});	
	$('.payement-mode img[alt="Paypal"]').click(function(){
		showPopup('Paypal');
	});
	$(".apayer-payment div[id='back-to-cate']>a").click(function(e){
		e.stopPropagation();
		e.preventDefault();
		close_popOverlay();
	});
	//Copy url
	try{
		var clip = new ZeroClipboard.Client();
		clip.setText($('input[name=url_txt]').val());
		clip.glue('copy-btn');
		clip.addEventListener( 'onComplete', my_complete );
		    
		function my_complete( client, text ) {
				alert("Copied text to clipboard: " + text );
		}	
	}catch(e){}

	//Edit address
	$('.edit-address-form form .send-btn a').click(function(e){
		e.preventDefault();
		var data = json2string($(this).parents('form'));
		var id = $(this).parents('form').prop('id');
		var query = "id=" + parseInt(id.match(/\d/)[0]) + "&action=editaddress&data="+ data;
		$.getJSON($(this).parents('form').prop('action'), query,function(data){
			if(data.error == 0) {
				$('#address-row' + id.match(/\d/)[0] + ' th').html($('.edit-address-form form input[name=address_name]').val());
				close_popOverlay();
			}			
		}, 'json');
	});	
	
	//Click on category cirle
	$('.list-categorys>li').click(function(){
		top.location.href = $('a',this).attr('href');
	});
	
	$('.header .gift-box').hover(function(){
		$('a img',this).attr('src',ROOT + '/static/images/gift-green.png');
		
	},function(){
		$('a img',this).attr('src',ROOT + '/static/images/gift.png');
		
	});
	//Subscribe newsletter	
	
	$('#newsletter-frm').submit(function() { 
		// submit the form 
		if(!validate('newsletter-frm','email')) return false;
		var options = {
			success: alertSubscribeOk
		};
		$(this).ajaxSubmit(options); 
		// return false to prevent normal browser submit and page navigation 
		return false; 
	});
	$('.aside-box .avatar-box').click(function(){
		top.location.href="/account/settings.php";
	});
	
	$(".payer-btn > a").live("click", function() { 
		//$(this).attr("disabled", "disabled"); 
		$("#basket-detail").hide();
	});
	//$("payer-btn > a[disabled]").live("click", function() { return false; });
	//Birthday timer
/*	
	birtdayTimer = setInterval(function(){		
		var d = new Date();
		var ds = d.getDate();
		if((ds >= 22) && (ds < 24)) {
			if(!$("#wrapper").hasClass('birthday')) $("#wrapper").addClass('birthday');
		}
		else if(ds >= 24 && ds < 26) {
			$("#wrapper").removeClass('birthday');
			$("header").addClass('birthday');
		}else if(ds >= 26) {
			$("header").removeClass('birthday');
			clearInterval(birtdayTimer);
		}
	},1000);
*/	
	nextdealCountdown();
	setInterval(nextdealCountdown, 1000);
});

