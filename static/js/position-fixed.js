// JavaScript Document

jQuery(document).ready(function($){
	var menu = $('.entry-infos .header');
	var thumbbox = $('.entry-infos .thumb-box');
	var pos = menu.offset();
	$(window).scroll(function(){
			if($(this).scrollTop() > pos.top && menu.hasClass('detail-page')){
				$(menu).removeClass('detail-page').addClass('pos-fixed');
				thumbbox.addClass('margin-top');
			} else if($(this).scrollTop() <= pos.top && menu.hasClass('pos-fixed')){
				$(menu).removeClass('pos-fixed').addClass('detail-page');
				thumbbox.removeClass('margin-top');
			}
		});
	

});
