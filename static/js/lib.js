/*
 Color animation 20120928
 http://www.bitstorm.org/jquery/color-animation/
 Copyright 2011, 2012 Edwin Martin <edwin@bitstorm.org>
 Released under the MIT and GPL licenses.
*/
(function(d){function m(){var b=d("script:first"),a=b.css("color"),c=false;if(/^rgba/.test(a))c=true;else try{c=a!=b.css("color","rgba(0, 0, 0, 0.5)").css("color");b.css("color",a)}catch(e){}return c}function j(b,a,c){var e="rgb"+(d.support.rgba?"a":"")+"("+parseInt(b[0]+c*(a[0]-b[0]),10)+","+parseInt(b[1]+c*(a[1]-b[1]),10)+","+parseInt(b[2]+c*(a[2]-b[2]),10);if(d.support.rgba)e+=","+(b&&a?parseFloat(b[3]+c*(a[3]-b[3])):1);e+=")";return e}function g(b){var a,c;if(a=/#([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})/.exec(b))c=
[parseInt(a[1],16),parseInt(a[2],16),parseInt(a[3],16),1];else if(a=/#([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])/.exec(b))c=[parseInt(a[1],16)*17,parseInt(a[2],16)*17,parseInt(a[3],16)*17,1];else if(a=/rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(b))c=[parseInt(a[1]),parseInt(a[2]),parseInt(a[3]),1];else if(a=/rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9\.]*)\s*\)/.exec(b))c=[parseInt(a[1],10),parseInt(a[2],10),parseInt(a[3],10),parseFloat(a[4])];return c}
d.extend(true,d,{support:{rgba:m()}});var k=["color","backgroundColor","borderBottomColor","borderLeftColor","borderRightColor","borderTopColor","outlineColor"];d.each(k,function(b,a){d.Tween.propHooks[a]={get:function(c){return d(c.elem).css(a)},set:function(c){var e=c.elem.style,i=g(d(c.elem).css(a)),h=g(c.end);c.run=function(f){e[a]=j(i,h,f)}}}});d.Tween.propHooks.borderColor={set:function(b){var a=b.elem.style,c=[],e=k.slice(2,6);d.each(e,function(h,f){c[f]=g(d(b.elem).css(f))});var i=g(b.end);
b.run=function(h){d.each(e,function(f,l){a[l]=j(c[l],i,h)})}}}})(jQuery);

function gotoTop(){
	$("html, body").animate({ scrollTop: 0 }, 600);
}
// JavaScript Document
function json2string(form){
	var data = form.serializeArray();
	var l = data.length;
	var str = "{";
	var tmp = '';
	var name = '';
	for(var i = 0, n=1; i < l; i++){
		if(String(data[i].name).indexOf('[]') > 0) {
			if(n == 1) name = String(data[i].name).replace('[]','');
			else name = String(data[i].name).replace('[]',n);
			n++;
		}else{
			name = data[i].name
		}
		tmp = '"'+ name + '":"' + data[i].value + '",';
		str += tmp;
	}
	str = str.substring(0,str.length -1) + "}";
	return str;
}

function close_popup() {
	$('.popup-box').fadeOut(100);
	return false;
}
function close_popOverlay() {
	$('div[id=popOverlay]').fadeOut(100);
	return false;
}
function close_connect() {
	$('.connects-box').fadeOut(200);
	$('#show-connect').removeClass('current');
	$('#show-credits').removeClass('current');
	close_popup();
	return false;
}
function reset_popOverlay() {
	var wheight=$(window).height();
	var popOverlay_height=$('#popOverlay .warnings-content').height()+ 202; //margin-top + padding top+ bottom
	if (popOverlay_height>wheight) {
		$('#popOverlay').height($('#wrapper').height());
		$('#popOverlay').css({'position':'absolute'});
	}
	//$('#popOverlay>div').hide();
}

//Show popup			
function showPopup(classname){
	var popOverlay = $("." + classname).parents("#popOverlay");
	$('#popOverlay>div').hide();
	$('#popOverlay .' + classname).show();
	popOverlay.fadeIn('slow');
}


//Delete address;

function deleteAddress(id){
	if(confirm('Are you sure to delete?')){
		var query = "action=removeaddress&id=" + id;
		$.get(ROOT + '/ajax/user.php', query, function(){
			$("#address-row"+id).remove();
		});
	}
}

//Delete avatar;
function deleteAvatar(id){
	if(confirm('Are you sure this avatar?')){
		var query = "action=deleteavatar&id=" + id;
		$.get(ROOT + '/ajax/user.php', query, function(){
			$("#address-row"+id).remove();
		});
	}
}

//Delete account;
function deleteAccount(id){
	var query = "action=deleteaccount&id=" + id;
	$.get(ROOT + '/ajax/user.php', query, function(){
		close_popOverlay();	
		top.location.href = ROOT + "/account/logout.php";
	});
}

//Render timer

function render(days, hours, minutes, seconds, timer){
	if((days <= 0) && (hours > 1)) $('#' + timer).addClass('Orange-Text');
	if((days <= 0) && (hours < 1)) $('#' + timer).addClass('Orange-Full');
	if(days < 10) days = "0" + days;
	if(minutes < 10) minutes = "0" + minutes;
	if(seconds < 10) seconds = "0" + seconds;
	if(hours < 10) hours = "0" + hours;
	$('#' + timer +' li .day').html(days);
	$('#' + timer +' li .hour').html(hours);
	$('#' + timer +' li .minute').html(minutes);
	if(seconds) $('#' + timer +' li .second').html(seconds);
}

function saveAddress(button, action){
	var data = json2string($(button).parents('form'));
	var query = "action=" + action + "&data="+ data;
	$.get($(button).parents('form').prop('action'), query ,function(data){
		close_popOverlay();
		$('td.add-delivery-address ul').append(data);		
	});
}

function subscribe(){
        
	var options = {
		success: alertSubscribeOk
	};
	if(!validate('newsletter-frm','email')) return false;
	$('#newsletter-frm').ajaxSubmit(options);
}

function validate(form_id,email, name) { 
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	var address = $("#" + form_id + " input[name=" + email + "]");
	var fullname = $("#" + form_id + " input[name=" + name + "]");

//	if(fullname.val() == '' ){
//			fullname.attr('placeholder','');
//			fullname.focus();
//			return false;
//	}
	if(reg.test(address.val()) == false) { 
	  address.attr('placeholder','Adresse email invalide');
	  address.attr('defaultValue','');
	  address.val('');
	  address.parents('.newsletter').addClass('error-input');
	  address.focus();
	  return false;
	}
	return true;	
}

function alertSubscribeOk(){
	$('.newsletter').removeClass('error-input');
	$('.newsletter input[name=email]').attr('placeholder','Votre email');
	$('.newsletter input[name=email]').val('');
	alert("Merci pour votre inscription!");
}
//Setup slider control for submit deal page
function initSliderControl(){
	ancestorContainer = ".prix-percent-box"
	container = ".prix-percent-bar";
	pointer = ".prix-percent-slider";
	percent = ".prix-percent-slider-bg";
	percentLabel = ".prix-percent-label";
	normalPrix = "#normal_prix";
	topdealPrix = "#topdeal_prix";
	maxWidth = 232;

	$(pointer).mousedown(function(e){
		e.preventDefault();
		e.stopPropagation();
		$(".prix-percent-box").mousemove(function(e){			
			var x = e.pageX - $(container).offset().left ;

			if(x > maxWidth) x = maxWidth;
			if(x <= 0) x = 0;
			var p = Math.round(x/232*100);
			var topPrix = Math.round(parseInt($(normalPrix).val())*(100-p)/100);
			$(topdealPrix).val(topPrix);
			$($(percentLabel)[0]).html(p + "%");
			$(percent).width(x);
		});
		$(ancestorContainer).mouseup(function(e){
			e.preventDefault();
			$(this).unbind('mousemove');
		});
	});
	$(normalPrix).change(function(){
		if(isNaN($(this).val()) || ($(this).val() == "")) {
			$(this).val(0);
		}
		var norPrix = parseInt($(this).val());
		var percent = parseInt($($(percentLabel)[0]).text());
		var topPrix = Math.round(norPrix*(1 - percent/100));
		$(topdealPrix).val(topPrix);
	});
	$(normalPrix).focus(function(){
		$(this).val("");
	});
	$(normalPrix).blur(function(){
		$(this).trigger('change');
	});
}

function isMobile() {
    var a = navigator.userAgent||navigator.vendor||window.opera;
    return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4));
}

function isBottom(){
	if(isMobile() || (navigator.userAgent.match(/iPad/i) != null)){
		var wintop = $(window).scrollTop(), docheight = $(document).height(), winheight = $(window).height();
		var  scrolltrigger = 0.90;
		if  ((wintop/(docheight-winheight)) > scrolltrigger) {
				return true;
		} else{
			return false;
		}
	}
	var wHeight = document.documentElement.scrollHeight;
	var realHeight = $(window).scrollTop() + $(window).height()
	return (((realHeight - wHeight) >= 0) && ((realHeight - wHeight) < 30));
}


//SET PAYMENT METHOD
	function setPaymentMethodByNumber(cardNumber){
	
		var cradType	=	detectCardType(cardNumber);
		//SET PAYMENT METHOD
		if (typeof cradType  !== "undefined"){
			//IF CARD IS VISA
			if(cradType=='VISA'){
				jQuery('input[name="paymentmethod"]').val('VIS');
				jQuery(".card-type-region > img").attr("src","/static/images/visa2.png");
			}
			else if(cradType=='Amex'){
				jQuery('input[name="paymentmethod"]').val('AMX');
				jQuery(".card-type-region > img").attr("src","/static/images/amx.jpg");
			}
			else if(cradType=='MASTERCARD'){
				jQuery('input[name="paymentmethod"]').val('ECA');
				jQuery(".card-type-region > img").attr("src","/static/images/mastercard2.png");
			}else{
				jQuery('input[name="paymentmethod"]').val('');
				jQuery(".card-type-region > img").attr("src","/static/images/visa_mc.png");
			}
		}else{
			jQuery('input[name="paymentmethod"]').val('');
			jQuery(".card-type-region > img").attr("src","/static/images/visa_mc.png");
		}
	}
	
	//Validate Card Number
	function validateCardNumber(number) {
		var re = {
			electron: /^(4026|417500|4405|4508|4844|4913|4917)\d+$/,
			maestro: /^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/,
			dankort: /^(5019)\d+$/,
			interpayment: /^(636)\d+$/,
			unionpay: /^(62|88)\d+$/,
			visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
			mastercard: /^5[1-5][0-9]{14}$/,
			amex: /^3[47][0-9]{13}$/,
			diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
			discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
			jcb: /^(?:2131|1800|35\d{3})\d{11}$/
		};
		if (re.electron.test(number)) {
			return 'ELECTRON';
		} else if (re.maestro.test(number)) {
			return 'MAESTRO';
		} else if (re.dankort.test(number)) {
			return 'DANKORT';
		} else if (re.interpayment.test(number)) {
			return 'INTERPAYMENT';
		} else if (re.unionpay.test(number)) {
			return 'UNIONPAY';
		} else if (re.visa.test(number)) {
			return 'VISA';
		} else if (re.mastercard.test(number)) {
			return 'MASTERCARD';
		} else if (re.amex.test(number)) {
			return 'Amex';
		} else if (re.diners.test(number)) {
			return 'DINERS';
		} else if (re.discover.test(number)) {
			return 'DISCOVER';
		} else if (re.jcb.test(number)) {
			return 'JCB';
		} else {
			return undefined;
		}
	}
	
	
	//DETECT CREDIT CARD
	function detectCardType(number){

	  var number = jQuery.trim(number);
	  var post_card_type 	= jQuery.trim(jQuery('input[name="post_card_type"]').val());
	  
	  if(number!="")
	  {
		
		   number =  number.slice(0, 1);

		   var cardFirstNumber = number;
		   
			if(post_card_type=='AMX')
			{
				return 'Amex';
			}
			else
			{
				if(cardFirstNumber==4)
			   {
					// jQuery('#cardPlatform').val('VISA');
					// jQuery("#cardSecureCode").attr("maxlength",3);
					return 'VISA';
			   }
			   else if(cardFirstNumber==5)
			   {
					// jQuery('#cardPlatform').val('MasterCard');
					// jQuery("#cardSecureCode").attr("maxlength",3);
					return 'MASTERCARD';
			   }
			   else
			   {
					return undefined;
			   }
			}
	  }
	  else
	  {
		
		if(post_card_type=='AMX'){
			return 'Amex';
		}
	  }
	}
	
function ajaxIsUserLoggedIn(action_url)
{
	var queryData = '';
	var data;
	$.ajax({
			type: "GET",
			url: action_url,
			data: queryData,
			async: false,
			success: function (resp) {
			data = resp;
        
		},
		error: function () {}
	}); // ajax asynchronus request 
	return data;
	
		
}

function goToLoginPage()
{
	window.location.href = '/account/login.php';
}
