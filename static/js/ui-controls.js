/*
@author: Hoang Manh Dung
@refer : http://hoangmanhdung.info
*/

/*Select control*/
(function($){
 
    $.fn.extend({ 
 
        selectbox: function() {
 
            return this.each(function() {
				$(this).hide();
				var label = ($('option:selected',this).text() != '')?$('option:selected',this).text() : $('option:first',this).text();
				var html = '<div class="sbo-control">';
				html += '<div class="label">' + label + '</div>';
				html += '<ul class="options">';
				$('option',this).each(function(){
								var defaultClick = $(this).attr("onclick");
								var clazzCss =  $(this).attr("class");
								if(!defaultClick){
									defaultClick = "defaultClickCallback(this)";
								}
								if(!clazzCss){
									clazzCss =  "";
								}else{
									if($(this).hasClass('soldout')){
										defaultClick = "return false;";
									}
								}
								
                                  if($(this).text()=='Choisissez votre option'){
                                  	html += '<li id="default-li" onclick="' + defaultClick + '" class="' + clazzCss + '">' + $(this).text() + '</li>';
                                  }else{
	                          	html += '<li onclick="' + defaultClick + '" class="' + clazzCss + '">' + $(this).text() + '</li>';
                                  }
				});
				html += '</ul>';
				html += $(this).prop('outerHTML');
				html += '</div>';
				$(this).replaceWith(html);				
            });
        }
    });
})(jQuery);
function defaultClickCallback(obj){	
	var i = $(obj).index();
	var text = $(obj).text();
	var sbo = $(obj).parents('.sbo-control');
	$('.label',sbo).text(text);
	$('select',sbo).get(0).selectedIndex = i;
			$('#send-btn-dis').hide();
	$('#send-btn-en').show();
	$('input[name=op]').val($('select',sbo).val());
	
}
/*Checkbox control*/
(function($){
 
    $.fn.extend({ 
 
        checkbox: function() {
 
            return this.each(function() {
				$(this).hide();
				var checked = ($(this).is(':checked') == false)? '':' checked';				
				var html = '<div class="chk-control'+ checked +'">';
				html += '<div class="chk-inner">';
				html += $(this).prop('outerHTML');
				html += '</div>';
				html += '</div>';
				$(this).replaceWith(html);
            });
        }
    });
})(jQuery);
function selectboxCallback(e){
	var self = $(this).parent();
	e.stopPropagation();	
	$('#default').remove();
	$('#default-li').remove();
	
	$('.options',self).css({
		'width': self.innerWidth(),
		'top': self.innerHeight() + parseInt(self.css('borderLeftWidth')) + 'px',
		'left': (0 - parseInt(self.css('borderLeftWidth'))) + 'px'
	});
	
	$('.sbo-control').removeClass('active');
	self.addClass('active');
	$('.sbo-control .options').not('.active .options').hide();
	
	$('.options',self).slideDown('fast', function(){
		var maxHeight = $(window).height() + $(window).scrollTop() - $(this).offset().top - 10;
		var optHeight = $(this).height();
		if (optHeight >= maxHeight) {
			$(this).css({		
				'height': maxHeight + 'px',
				'overflow-y': "auto"
			});
		}else{
			$(this).css({		
				'height': '',
				'overflow-y': ''
			});
		}
	});	
}
$(document).ready(function(){
// Setup custom selectbox
	$('select').selectbox();
	$(document).on('click','.sbo-control .label',selectboxCallback);
	
	$(document).on('click', function(e){		
		$('.sbo-control .options').hide();
	});

// Setup custom checkbox
	$('input[type=checkbox]').checkbox();
	$(document).on('click','.chk-control',function(e){
		e.stopPropagation();
		var chk = $('input[type=checkbox]',this);
		if($(this).hasClass('checked')){
			//Show/hide delivery address
			if($(e.target).parents('.profiles-right').length > 0) $('.add-delivery-address').fadeOut();	
			$(this).removeClass('checked');			
			chk.removeAttr('checked');
		} else {
			if($(e.target).parents('.profiles-right').length > 0) $('.add-delivery-address').fadeIn();
			$(this).addClass('checked');
			chk.attr('checked','checked');
		}
	});

});


(function($){
	$.fn.extend({ 
		
		changeQuantity: function(options) {

			//Settings list and the default values
			var defaults = {				
				up: '.up-btn',
				down: '.down-btn',
				minValue: 1,
				maxValue: 100
			};
			
			var options = $.extend(defaults, options);
		
    		return this.each(function() {
				var o = options;			
				var obj = $(this);				
				var up = $(o.up, obj);
				var down = $(o.down, obj);
				var input = $("input[type=text]", obj)[0];
				if($(input).attr('maxbuy')) { o.maxValue =  $(input).attr('maxbuy'); }
				$(input).blur(function(){
					var val = $(this).val();
					$(this).val(parseInt(val));
					if(isNaN(val) || val == '') $(this).val(o.minValue);
				});

				up.mousedown(function(){
					var currentVal = parseInt($(input).val());
					if(isNaN(currentVal)) currentVal = 0;
					currentVal += 1;
                                        if($(input).attr('maxbuy')) { o.maxValue =  $(input).attr('maxbuy'); }
					if(currentVal > o.maxValue) return;
					$(input).val(currentVal);
					$(input).trigger('change');
				});
				down.mousedown(function(){
					var currentVal = parseInt($(input).val());
					if(isNaN(currentVal)) {
						$(input).val(o.minValue);
						return;
					}
					currentVal -= 1;
					if(currentVal < o.minValue) return;
					$(input).val(currentVal);
					$(input).trigger('change');
				});
				
    		});
    	}
	});
})(jQuery);

$(document).ready(function(){
	$('.edit-quantity').changeQuantity();
});