(function ( $ ) {
 
    $.fn.progressbar = function( options ) {
 
        var settings = $.extend({
            // These are the defaults.
            unit: "%"
        }, options );
 
        return this.each( function() {
			var bar = $('.progress-bar',this);
			var indicator = $('.progress-icon',this);
			var val = bar.attr('aria-valuenow');
			
			if(indicator) {

			indicator.css('left', val + settings.unit);
			}
			bar.css('width', val + settings.unit);
        });
 
    };
 
}( jQuery ));
$(document).ready(function(){
	if( $('.menu-btn').is(':visible') ) {
		var m = $('#navigation .navbar-nav');
		var mw = m.outerWidth();
		var btnW = $('.menu-btn').outerWidth()
		var btnLeft = (mw - btnW);
		m.css('margin-left','-' + mw + 'px');
		m.show();
		$('.menu-btn').click(function(){
			try{			
				var mnuLeft = parseInt(m.css('margin-left'));
				if(mnuLeft >= 0){
					m.animate({'margin-left': '-=' + mw}, 500);
					$(this).animate({'left': 0}, 400);
				}else{		
					m.animate({'margin-left': 0}, 500);
					$(this).animate({'left': btnLeft}, 400);
				}
			}catch(e){
				alert(e.message);
			}
			
			
		});
	}
	$('.category .deals .title a').tooltip();
	$('.progress-wrapper').progressbar();	
	
	
	if ($('.sliders .slider-content')[0]) {
		$('.sliders .slider-content').cycle();
	}		
	
});