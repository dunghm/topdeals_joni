<?php
require_once('function.php');
//INCREASE EXECUTION TIME & MEMROY LIMIT
ini_set('max_execution_time', '-1');
ini_set('memory_limit','2047M');

//GET ALL ACTIVE Trigger
$getActiveTrigger	=	getActiveTrigger();

//RUN CRON FOR EACH TRIGGER
if(!empty($getActiveTrigger)){
	
	$eventLogs	=	array();
	
	foreach($getActiveTrigger as $getActiveTriggerRow){
		
		//CRON DATA
		$email_subject				=	$getActiveTriggerRow['email_subject'];
		$email_body					=	$getActiveTriggerRow['email_body'];
		$event_id					=	$getActiveTriggerRow['event_id'];
		$action_id					=	$getActiveTriggerRow['action_id'];
		$type_id					=	$getActiveTriggerRow['type_id'];# 1->Simple Email, 2->Promo Code
		$promo_code_type			=	$getActiveTriggerRow['promo_code_type'];
		$promo_percent				=	$getActiveTriggerRow['promo_percent'];
		$promo_amount				=	$getActiveTriggerRow['promo_amount'];
		$start_time					=	$getActiveTriggerRow['start_time'];
		$end_time					=	$getActiveTriggerRow['end_time'];
		$minimum_order				=	$getActiveTriggerRow['minimum_order'];
		$promo_code_attribute		=	$getActiveTriggerRow['promo_code_attribute'];
		
		
		$currentTime	=	time();
		//VALIDATE BY TIME STAMP
		if($start_time > 0 ){
			//IF CRON START TIME IS GREATOR THAN CURRENT TIME THEN SKIP TRIGGER
			if($start_time > $currentTime){
				continue;
			}
		}
		
		if($end_time > 0){
			//IF CURRENT TIME IS GREATOR THAN END TIME OF CRON SKIP TRIGGER
			if($currentTime > $end_time){
				continue;
			}
		}
		
		//INCREASE DB GLOBAL VARIABLE VALUE
		increaseDbGlobalVaribale();
	
	
		
		/****************************************
		* GET CUSTOMER DATA W.R.T EVENT
		****************************************/
		$customerData	=	array();
		
		/**
		* @TODO : GET CUSTOMER DATA BY EVENT UNIQUE NAME INSTEAD OF ID
		* EVENT ID 1 -> Re-activation of inactive customers since 2 years
		* Segmenet : User registred since less than 2 years AND no purchase since at least 6 months
		*/
		
		if($event_id==1){
			
			$registerSince		=	'-2 year';
			// $registerSince		=	date('c');
			$lastPurchaseOrder	=	'-6 month';
			
			$customerData		=	reActivationOfInactiveCustomersSinceTwoYear($registerSince, $lastPurchaseOrder);
		}
		
		/**
		* @TODO : GET CUSTOMER DATA BY EVENT UNIQUE NAME INSTEAD OF ID
		* EVENT ID 2 -> New inactive customer
		* Segmenet : Registered since 1 month but has not proceed to any order yet
		*/
		
		if($event_id==2){
			
			$registerSince		=	'-1 month';
			// $registerSince		=	date('c');
			$customerData		=	registeredSinceOneMonthButHasNotProceedToAnyOrderYet($registerSince);
		}
		
		/**
		* @TODO : GET CUSTOMER DATA BY EVENT UNIQUE NAME INSTEAD OF ID
		* EVENT ID 3-> Re-activation of inactive customers since 1 year
		* Segmenet : Customers who have not pruchased within the last 12 months
		*/
		
		if($event_id==3){
			$lastPurchaseOrder	=	'-1 year';
			$customerData		=	 reActivationOfInactiveCustomersSinceOneYear($lastPurchaseOrder);
		}
		
		//SEND EMAIL TO CUSTOMER
		if(!empty($customerData)){
			
			/**
			* @TODO :	
			* 1 - PERFORM ACTION BY ACTION UNIQUE NAME INSTEAD OF ID
			* 2 - Current Sent Coupon Method is generic now we need to use only action method instead of multi
			* ACTION ID 1 -> end e-mail with promo code 10% valid 48h
			*/
			
			if($action_id==1){
				//SEND EMAIL FOR EACH USER
				foreach($customerData as $customerDataRow){
					//MAINTAIN LOG
					// $eventLogs[]	=	sendEmailWithPromoCodeTenPercentValid48h($getActiveTriggerRow,$customerDataRow);
					$eventLogs[]	=	sendEmailWithPromoCodeOrSimpleEmail($getActiveTriggerRow,$customerDataRow);
				}
			}
			
			/**
			* @TODO :	
			* 1 - PERFORM ACTION BY ACTION UNIQUE NAME INSTEAD OF ID
			* 2 - Current Sent Coupon Method is generic now we need to use only action method instead of multi
			* ACTION ID 3 -> E-mail to be sent giving him a promo code if his next purchase reach the CHF 100 amount
			*/
			
			if($action_id==3){
				//SEND EMAIL FOR EACH USER
				foreach($customerData as $customerDataRow){
					//MAINTAIN LOG
					// $eventLogs[]	=	sendEmailWithPromoCodeForMinimumOrderAmount($getActiveTriggerRow,$customerDataRow);
					$eventLogs[]	=	sendEmailWithPromoCodeOrSimpleEmail($getActiveTriggerRow,$customerDataRow);
				}
			}
		
		}
	}
	//LOG EVENT 
	if(!empty($eventLogs)){
		$eventLogsArr	=	array_chunk($eventLogs, 1000);
		
		if(!empty($eventLogsArr)){
			
			foreach($eventLogsArr as $eventLogsRow){
				
				$eventLogData	=	implode(",",$eventLogsRow);
				
				$sql	=	<<<SQL
								INSERT
									INTO
								trigger_event_logs
								(
									`user_id`,
									`event_id`,
									`action_id`,
									`promo_code`,
									`created`
								)
								VALUES
								$eventLogData
SQL;
				 DB::Query($sql);
			}
		}
		// if()
		// foreach()
	}
	
}


echo "Cron Run Successfully!";
exit();

?>