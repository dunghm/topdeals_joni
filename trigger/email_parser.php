<?php

	//REPLACE EMAIL BODY  TEXT BY PATTERN
	function emailBodyFormat($email_body,$customerDataRow,$getActiveTriggerRow,$promo_code){
		
		
		$username	=	$customerDataRow['username'];
		$email		=	$customerDataRow['email'];
		$first_name	=	$customerDataRow['first_name'];
		$last_name	=	$customerDataRow['last_name'];
		
		$fullName	=	$first_name." ".$last_name;
		
		//REPLACE PATTERN
		$email_body	=	preg_replace('|#USERNAME#|',   $username, $email_body );
		$email_body	=	preg_replace('|#EMAIL#|', 	 $email, $email_body );
		$email_body	=	preg_replace('|#PROMO_CODE#|', $promo_code, $email_body );
		$email_body	=	preg_replace('|#FULL_NAME#|',  $fullName, $email_body );
		
		return $email_body;
	}
	
	//SEND EMAIL
	 function mailCustomEmail($email_to, $email_subject, $email_body){
		  $from = 'commande@topdeal.ch';
		  // mail_custom($email_to, $email_subject, $email_body, $from);
		  emailManagerSendmail($email_to, $email_subject, $email_body, $from);
	 }
	 
	 /**
	* email_manager send mail function
	**/
	function emailManagerSendmail($to, $subject, $body, $from){
		
		
		
		$headers = array(
				"Mime-Version: 1.0",
				"Content-Type: text/html; charset=charset=UTF-8",
				"Content-Transfer-Encoding: base64",
				"X-Mailer: ZTMailer/40b0e624bba09a0b598dd24da9466d49/1.0",
				"From: TopDeal.ch <$from>",
				"Reply-To: TopDeal.ch <$from>",
				);
				
		// if ($bcc) { 
			// $bcc = join(', ', $bcc);
			// $headers[] = "Bcc: {$bcc}";
		// }
		
		$headers = join("\r\n", $headers);
		//echo nl2br($body);	
		//$body = str_replace('\n', '<br/>', $body);
		// $body= wordwrap($body);
		// $body= str_replace("\n", "\r\n", $body);

		return mail($to,  $subject, $body ,$headers );
	}
?>