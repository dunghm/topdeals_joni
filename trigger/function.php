<?php

require_once(dirname(dirname(__FILE__)) . '/app.php');
require_once('email_parser.php');


#TRIGGER CONSTANT
define('PROMO_CODE_EXPIRY','+3 Month');
/**
* GENERATE PROMO CODE BY CRON
*/

function generateCard($codeType,$money,$discount,$end_time,$code,$promo_code,$minimum_order){
	// return true;
	$begin_time = strtotime(date('Y-m-d'));
	
	$card	=	array
			(
				'id' 					=> '',
				'partner_id' 			=> 0,
				'value_type' 			=> $codeType,
				'money' 				=> $money,
				'discount' 				=> $discount,
				'quantity' 				=> 1,
				'begin_time' 			=> $begin_time,
				'end_time' 				=> $end_time,
				'code' 					=> $code,
				'max_use' 				=> 1,
				'promo_code' 			=> $promo_code,
				'team_id' 				=> 0,
				'min_order' 			=> $minimum_order,
				'limit_per_user' 		=> 0,
				'limit_per_deal'	 	=> 0,
				'limit_per_user_deal' 	=> 0,
				'commit' 				=> 'Edit',
				'allow_other' 			=> 'N'
			);
			
		ZCard::CardCreate($card);
}


/**
* GET ALL ACTIVE TRIGGER
*/

function getActiveTrigger(){
	
	$sql	=	<<<SQL
					
					SELECT 
						ta.subject 					AS email_subject,
						ta.email_body 				AS email_body,
						te.id 						AS event_id,
						ta.id						AS action_id,
						ta.type_id 					AS type_id,
						ta.promo_code_type 			AS promo_code_type,
						ta.promo_percent 			AS promo_percent,
						ta.promo_amount 			AS promo_amount,
						ta.minimum_order 			AS minimum_order,
						ta.promo_code_attribute 	AS promo_code_attribute,
						te.start_time				AS start_time,
						te.end_time					AS end_time
					FROM
						trigger_event AS te
					LEFT JOIN 
						trigger_action AS ta
					ON
						ta.id = te.action_id
					LEFT JOIN
						trigger_action_type AS tp
					ON
						tp.id = ta.type_id
					WHERE
						te.is_active = 1
SQL;
		$result = DB::GetQueryResult($sql,false);
		
		return $result;
}

/**
* GET CUSTOMER  WITHIN PARTICULCAR REGITSRATION DATE
*/
function getCustomerFromTimePeriod($registerSince){
	
	$dateRangeQuery	=	"";
	if($registerSince){
		$specDateRange			=	strtotime($registerSince);
		// $starDate			=	strtotime(date('Y-m-d',$lastTwoYesr)." 00:00:00");
		// $endDate				=	strtotime(date('Y-m-d',$lastTwoYesr)." 23:59:59");
		$formatedSpecDateRange	=	date('Y-m-d',$specDateRange);
		
		$dateRangeQuery = " AND from_unixtime(create_time, '%Y-%m-%d') = '$formatedSpecDateRange'";
	}
	
	
	//GET REGISTERED USER SINCE 2 YEAR
	$sql	=	<<<SQL
						SELECT
							GROUP_CONCAT(id) AS userId
						FROM
							`user`
						WHERE 
							enable = 'Y'
						$dateRangeQuery
							
SQL;
// echo "<pre>";print_r($sql);
	$lastTwoYearUser = DB::GetQueryResult($sql);
	
	return $lastTwoYearUser;
}

/**
* GET ORDER  WITHIN PARTICULCAR ORDER DATE
*/
function getOrderWithinRange($lastPurchaseOrder,$lastTwoYearRegUser){
	
			// $lastSixMonth		= 	strtotime('-6 month');
		$dateRangeCondition = "";
		//CHECK  DATE RANGE
		if($lastPurchaseOrder){
			
			$lastSixMonth		=	strtotime($lastPurchaseOrder);
			$lastSixMonthDate	=	date('Y-m-d',$lastSixMonth);
			
			$dateRangeCondition = "  AND from_unixtime(create_time, '%Y-%m-%d') >= '$lastSixMonthDate'";
		}
	
	
		//GET USER WHO MAKE PURCHASE SINCE LAST SIX MONTH 
		$sql	=	<<<SQL
							SELECT 
								GROUP_CONCAT(DISTINCT(user_id)) AS userId
							FROM
								`order`
							WHERE
								user_id IN ($lastTwoYearRegUser)
							AND
								state = 'pay'
							$dateRangeCondition
							
SQL;

// echo "<pre>";print_r($sql);
		$lastSixMonthPurchaseUserOrder = DB::GetQueryResult($sql);
		
		
		$purchasedOrderUser	=	'';
		if( !empty($lastSixMonthPurchaseUserOrder) && isset($lastSixMonthPurchaseUserOrder['userid']) ){
			$purchasedOrderUser	=	$lastSixMonthPurchaseUserOrder['userid'];
		}
		
		return $purchasedOrderUser;
}
//GET Re-activation of inactive customers since 2 years
function reActivationOfInactiveCustomersSinceTwoYear($registerSince,$lastPurchaseOrder){
	
	$getUserInfoById = array();
	
	// $lastTwoYear		= 	strtotime('-2 year');
	
	
	$customerData	=	getCustomerFromTimePeriod($registerSince);
	if( !empty($customerData) && isset($customerData['userid']) ){
		
		//REGISTERED USER SINCE LAST TWO YEAR
		$lastTwoYearRegUser	=	$customerData['userid'];
		
		//GET USER IDS WHO MAKE PURCHASE LAST 6 MONTH
		$purchasedOrderUser =  getOrderWithinRange($lastPurchaseOrder,$lastTwoYearRegUser);
		
		//NOW GET THOSE USER ID WHO DIDN'T MAKE PURCHASE SINCE LAST 6 MONTH BY TAKING ARRAY DIFF
		$lastTwoYearRegUserArr	=	explode(",",$lastTwoYearRegUser);
		$purchasedOrderUserArr	=	explode(",",$purchasedOrderUser);
		$userWhoDidntMakePurchase = implode(",",array_diff($lastTwoYearRegUserArr, $purchasedOrderUserArr));
		
		//GET USER DATA BY IDS
		$getUserInfoById	=	getUserInfoById($userWhoDidntMakePurchase);
	}
	
	return $getUserInfoById;
}


//GET USER INFO
function getUserInfoById($userId){
	
		$sql	=	<<<SQL
						SELECT
							id,
							email,
							username,
							first_name,
							last_name
						FROM
							`user`
						WHERE 
							id IN ($userId)
SQL;
		$userInfo = DB::GetQueryResult($sql,false);
		
		return $userInfo;
}

/**
* Send e-mail with promo code 10% valid 48h
*/
function sendEmailWithPromoCodeTenPercentValid48h($getActiveTriggerRow,$customerDataRow){
	
		//CRON DATA
		$email_subject		=	$getActiveTriggerRow['email_subject'];
		$email_body			=	$getActiveTriggerRow['email_body'];
		$event_id			=	$getActiveTriggerRow['event_id'];
		$action_id			=	$getActiveTriggerRow['action_id'];
		$action_type_id		=	$getActiveTriggerRow['type_id'];# 1->Simple Email, 2->Promo Code
		$promo_code_type	=	$getActiveTriggerRow['promo_code_type'];
		$promo_percent		=	$getActiveTriggerRow['promo_percent'];
		$promo_amount		=	$getActiveTriggerRow['promo_amount'];
		$start_time			=	$getActiveTriggerRow['start_time'];
		$end_time			=	$getActiveTriggerRow['end_time'];
	
		#CUSTOMER INFO
		$username	=	$customerDataRow['username'];
		$userId		=	$customerDataRow['id'];
		$email		=	$customerDataRow['email'];
		
		//SET CODE TYPE # 1->Face, 2->Percent
		$codeType = 1;
		if($promo_code_type == "percentage"){
			$codeType = 2;
		}
		
		$promo_code	=	"****";
		if($action_type_id == 2){
			
			$code 		= date('Ymd').'_WR';
			$end_time 	= strtotime('+48 hours');
			$promo_code	= Utility::GenSecret(16, Utility::CHAR_NUM)."_PR";

			$minimum_order	=	0;
			$money			=	$promo_amount;
			$discount		=	$promo_percent;
			
			generateCard($codeType,$money,$discount,$end_time,$code,$promo_code,$minimum_order);
		}
		//METHOD DEFINE IN TDADMIN HELPER
		$email_body	=	emailBodyFormat($email_body,$customerDataRow,$getActiveTriggerRow,$promo_code);
		
		//SEND EMAIL
		 mailCustomEmail($email, $email_subject, $email_body);
		 $currentTime	=	time();
		 return "( '$userId', '$event_id', '$action_id', '$promo_code', '$currentTime' )";
}

/**
* Registered since 1 month but has not proceed to any order yet
*/
function registeredSinceOneMonthButHasNotProceedToAnyOrderYet($registerSince){
	
	$getUserInfoById = array();
	
	$customerData	=	getCustomerFromTimePeriod($registerSince);
	if( !empty($customerData) && isset($customerData['userid']) ){
		
		//REGISTERED USER SINCE LAST TWO YEAR
		$lastOneMonthRegUser	=	$customerData['userid'];
		
		//GET USER IDS WHO MAKE PURCHASE 
		$purchasedOrderUser =  getOrderWithinRange(false,$lastOneMonthRegUser);
		
		//NOW GET THOSE USER ID WHO DIDN'T MAKE ANY PURCHASE SINCE REGISTERED
		$lastOneMonthRegUserArr	=	explode(",",$lastOneMonthRegUser);
		$purchasedOrderUserArr	=	explode(",",$purchasedOrderUser);
		$userWhoDidntMakePurchase = implode(",",array_diff($lastOneMonthRegUserArr, $purchasedOrderUserArr));
		
		//GET USER DATA BY IDS
		$getUserInfoById	=	getUserInfoById($userWhoDidntMakePurchase);
	}
	return $getUserInfoById;
}

/**
* Send e-mail with promo code with minimum order amount
*/
function sendEmailWithPromoCodeForMinimumOrderAmount($getActiveTriggerRow,$customerDataRow){
	
		//CRON DATA
		$email_subject		=	$getActiveTriggerRow['email_subject'];
		$email_body			=	$getActiveTriggerRow['email_body'];
		$event_id			=	$getActiveTriggerRow['event_id'];
		$action_id			=	$getActiveTriggerRow['action_id'];
		$action_type_id		=	$getActiveTriggerRow['type_id'];# 1->Simple Email, 2->Promo Code
		$promo_code_type	=	$getActiveTriggerRow['promo_code_type'];
		$promo_percent		=	$getActiveTriggerRow['promo_percent'];
		$promo_amount		=	$getActiveTriggerRow['promo_amount'];
		$start_time			=	$getActiveTriggerRow['start_time'];
		$end_time			=	$getActiveTriggerRow['end_time'];
		$minimum_order		=	$getActiveTriggerRow['minimum_order'];
	
		#CUSTOMER INFO
		$username	=	$customerDataRow['username'];
		$userId		=	$customerDataRow['id'];
		$email		=	$customerDataRow['email'];
		
		//SET CODE TYPE # 1->Face, 2->Percent
		$codeType = 1;
		if($promo_code_type == "percentage"){
			$codeType = 2;
		}
		
		$promo_code	=	"****";
		if($action_type_id == 2){
			
			$code 		= date('Ymd').'_WR';
			$end_time 	= strtotime(PROMO_CODE_EXPIRY);
			$promo_code	= Utility::GenSecret(16, Utility::CHAR_NUM)."_PR";

			$money			=	$promo_amount;
			$discount		=	$promo_percent;
			
			generateCard($codeType,$money,$discount,$end_time,$code,$promo_code,$minimum_order);
		}
		//METHOD DEFINE IN TDADMIN HELPER
		$email_body	=	emailBodyFormat($email_body,$customerDataRow,$getActiveTriggerRow,$promo_code);
		
		//SEND EMAIL
		 mailCustomEmail($email, $email_subject, $email_body);
		 $currentTime	=	time();
		 return "( '$userId', '$event_id', '$action_id', '$promo_code', '$currentTime' )";
}


/**
* RE-ACTIVATION OF CUSTOMER WHO DIDN'T MAKE PURCHASE SINCE LAST 1 YEAR
*/

function reActivationOfInactiveCustomersSinceOneYear($lastPurchaseOrder){
	
	$getUserInfoById = array();
	
	// $lastTwoYear		= 	strtotime('-2 year');
	
	
	$customerData	=	getCustomerFromTimePeriod(false);
	

	if( !empty($customerData) && isset($customerData['userid']) ){
		
		//REGISTERED USER SINCE LAST TWO YEAR
		$cutsomerIdsList	=	$customerData['userid'];
		
		//GET USER IDS WHO MAKE PURCHASE LAST 1 YEAR
		$purchasedOrderUser =  getOrderWithinRange($lastPurchaseOrder,$cutsomerIdsList);
		
		//NOW GET THOSE USER ID WHO DIDN'T MAKE PURCHASE SINCE LAST 1 YEAR BY TAKING ARRAY DIFF
		$cutsomerIdsArr			=	explode(",",$cutsomerIdsList);
		$purchasedOrderUserArr	=	explode(",",$purchasedOrderUser);
		$userWhoDidntMakePurchase = implode(",",array_diff($cutsomerIdsArr, $purchasedOrderUserArr));
		
		//GET USER DATA BY IDS
		$getUserInfoById	=	getUserInfoById($userWhoDidntMakePurchase);
	}
	return $getUserInfoById;
}


/**
* INCREASE DATABASE GLLOBAL METHOD VALUSE
*/

function increaseDbGlobalVaribale(){
	DB::Query("SET SESSION group_concat_max_len=150000000");
}

/**
* Send PROMO CODE & SIMPLE EMAIL
*/
function sendEmailWithPromoCodeOrSimpleEmail($getActiveTriggerRow,$customerDataRow){
	
		//CRON DATA
		$email_subject				=	$getActiveTriggerRow['email_subject'];
		$email_body					=	$getActiveTriggerRow['email_body'];
		$event_id					=	$getActiveTriggerRow['event_id'];
		$action_id					=	$getActiveTriggerRow['action_id'];
		$action_type_id				=	$getActiveTriggerRow['type_id'];# 1->Simple Email, 2->Promo Code
		$promo_code_type			=	$getActiveTriggerRow['promo_code_type'];
		$promo_percent				=	$getActiveTriggerRow['promo_percent'];
		$promo_amount				=	$getActiveTriggerRow['promo_amount'];
		$start_time					=	$getActiveTriggerRow['start_time'];
		$end_time					=	$getActiveTriggerRow['end_time'];
		
		
		#CUSTOMER INFO
		$username	=	$customerDataRow['username'];
		$userId		=	$customerDataRow['id'];
		$email		=	$customerDataRow['email'];
		

		
		$promo_code	=	"****";
		if($action_type_id == 2){
			
			
			/* PARSE PROMO CODE DATA*/
			$promo_code_attribute		=	unserialize($getActiveTriggerRow['promo_code_attribute']);
			$promo_code_type			=	"face_value";
			$promo_amount				=	10;
			$promo_percent				=	0;
			$minimum_order				=	0;
			$coupon_validity			=	PROMO_CODE_EXPIRY;
			
			if(isset($promo_code_attribute['promo_code_type'])){
				$promo_code_type	=	$promo_code_attribute['promo_code_type'];
			 }
			 
			 if(isset($promo_code_attribute['promo_amount'])){
				$promo_amount	=	$promo_code_attribute['promo_amount'];
			 }
			 
			 if(isset($promo_code_attribute['promo_percent'])){
				$promo_percent	=	$promo_code_attribute['promo_percent'];
			 }
			 
			 if(isset($promo_code_attribute['minimum_order'])){
				$minimum_order	=	$promo_code_attribute['minimum_order'];
			 }
			 
			 if(isset($promo_code_attribute['coupon_validity'])){
				$coupon_validity	=	$promo_code_attribute['coupon_validity'];
				
				if(!empty($coupon_validity) && $coupon_validity!="" && $coupon_validity != NULL && $coupon_validity >0){
					if($coupon_validity >1){
						$coupon_validity = "+$coupon_validity hours";
					}else{
						$coupon_validity = "+$coupon_validity hour";
					}
				}else{
					$coupon_validity			=	PROMO_CODE_EXPIRY;
				}
			 }
		 
			//SET CODE TYPE # 1->Face, 2->Percent
			$codeType = 1;
			if($promo_code_type == "percentage"){
				$codeType = 2;
			}
			
			$code 		= date('Ymd').'_WR';
			$end_time 	= strtotime($coupon_validity);
			$promo_code	= Utility::GenSecret(16, Utility::CHAR_NUM)."_PR";

			$money			=	$promo_amount;
			$discount		=	$promo_percent;
			
			generateCard($codeType,$money,$discount,$end_time,$code,$promo_code,$minimum_order);
		}
		//METHOD DEFINE IN TDADMIN HELPER
		$email_body	=	emailBodyFormat($email_body,$customerDataRow,$getActiveTriggerRow,$promo_code);
		
		//SEND EMAIL
		 mailCustomEmail($email, $email_subject, $email_body);
		 $currentTime	=	time();
		 return "( '$userId', '$event_id', '$action_id', '$promo_code', '$currentTime' )";
}
?>