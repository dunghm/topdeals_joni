<?php
	require_once(dirname(dirname(__FILE__)) . '/app.php');
	
	$tomorrowDate   = date('Y-m-d', strtotime(' + 1 day'));	
	$startDateTime = date('Y-m-d 00:00:00', strtotime(' + 1 day'));
	$endDateTime   = date('Y-m-d 23:59:59', strtotime(' + 1 day'));	
	
	$startDateTimestamp = strtotime($startDateTime);
	$endDateTimestamp = strtotime($endDateTime);
	
	$sql ="SELECT  
			t.title as team_title,
			tm.title as option_title,
			o.id as order_id, o.create_time,
			oi.id as order_item_id,oi.quantity,oi.price, 
			concat(u.first_name,' ', u.last_name) as user_name, u.email
		FROM 
			order_item oi join `order` o on oi.order_id = o.id 
			join team t on oi.team_id = t.id
			join user u on u.id = o.user_id
			left join team_multi tm on tm.id = oi.option_id
		WHERE 
			o.state = 'pay' 
		AND 
			oi.delivery  in ('pickup', 'express')  
		AND
			oi.delivery_time >='$startDateTimestamp'  
		AND 
			oi.delivery_time <= '$endDateTimestamp'";
		
	$waitingPassOrders = DB::GetQueryResult($sql, false);

	//include template('mail/mail_template_waiting_order_pass');
	mail_waiting_order_pass($waitingPassOrders);
?>