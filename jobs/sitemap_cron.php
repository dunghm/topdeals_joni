<?php

require_once(dirname(dirname(__FILE__)) . '/app.php');
error_reporting(E_ALL);
$now = time();
ini_set('display_errors',1);
    $xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n";
    $xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
        
        $sitemap_config = $INI['sitemap'];
        
        if(substr($INI['system']['wwwprefix'], -1) != '/'){
            $site_url = $INI['system']['wwwprefix'].'/';
        }
       else{
           $site_url = $INI['system']['wwwprefix'];
       }
       
        $xml.= "<url>
                    <loc>".$site_url."</loc>
                    <lastmod>".date('c')."</lastmod>
                    <changefreq>daily</changefreq>
                    <priority>1</priority>
                </url>\n";
       
       if(isset($sitemap_config['static_pages']))
       {
            foreach($sitemap_config['static_pages'] as $st){
                $xml.= "<url>
                        <loc>".$site_url.trim($st)."</loc>
                        <lastmod>".date('c')."</lastmod>
                        <changefreq>daily</changefreq>
                        <priority>".$sitemap_config['static_pages_priority']."</priority>
                    </url>\n";
             }
        }
        // now popular deal categories
        $deal_categories = array();
        $deal_categories[] = "deal_du_jour";
        $deal_categories[] = "deals";
        $deal_categories[] = "deals/produits";
        $deal_categories[] = "deals/sports";
        $deal_categories[] = "deals/lifestyle";
        $deal_categories[] = "deals/restaurants";
        $deal_categories[] = "deals/kids";
        $deal_categories[] = "deals/voyage";
        $deal_categories[] = "deals/bien-etre";
        
        foreach($deal_categories as $dc){
                $xml.= "<url>
                        <loc>".$site_url.trim($dc)."</loc>
                        <lastmod>".date('c')."</lastmod>
                        <changefreq>daily</changefreq>
                        <priority>".$sitemap_config['deal_category_priority']."</priority>
                    </url>\n";
        }
        
        // @todo: We have to add more deal categories besides them by quering to database.
        // Now deals
        $condition_teams = array();
        $condition_teams['system'] = 'Y';
        if(isset($sitemap_config['active_deals'])){
            if($sitemap_config['active_deals'] == 'Yes'){
                $condition_teams[] = "end_time > {$now}";
            }
        }
        $teams = DB::LimitQuery('team', array(
            'condition' => $condition_teams,
            'order' => 'ORDER BY id DESC',
        ));
       
        if($teams){
            $i = 0;
            foreach($teams as $tm){
                
                if($tm['end_time'] > $now){
                   $priority =  $sitemap_config['deal_priority'];
                   $deal_frequency = 'daily';
                }else{
                    $priority = $sitemap_config['inactive_deal_priority'];
                    $deal_frequency = 'monthly';
                }
                
                
                $xml.= "<url>
                        <loc>".ZTeam::GetTeamUrl($tm, FALSE, TRUE)."</loc>
                        <lastmod>".date('c')."</lastmod>
                        <changefreq>".$deal_frequency."</changefreq>
                        <priority>".$priority."</priority>
                    </url>\n";
           }
        }
        
        //print_r($INI['system']['wwwprefix']);
        
    $xml .= '</urlset>';
        
        try{
           echo WWW_ROOT.'/'.$sitemap_config['location'];
    $file = fopen(WWW_ROOT.'/'.$sitemap_config['location'],"w+");
    fwrite($file, $xml);
    fclose($file);
        }
        catch( Exception $e){
            die ('File did not create: ' . $e->getMessage());
        }