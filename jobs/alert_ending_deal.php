<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

   require_once(dirname(dirname(__FILE__)) . '/app.php');
   
   $alert_email = 'admin@topdeal.ch';
   
   $total_hours = isset($_GET['hours']) ? intval($_GET['hours']) : 72;
$now = strtotime(date('Y-m-d H:i'));
   $past_hour = $now -(3600*$total_hours);
   $cond = array(                   
                    "end_time >= {$past_hour}",
                    "end_time < {$now}",
                    );
    $order = 'ORDER BY end_time DESC';

    /* normal team */
    $teams = DB::LimitQuery('team', array(
                            'condition' => $cond,                            
                            'order' => $order,
                            ));
    
    if ( $teams && sizeof($teams) > 0 )
    {
        $message = "Dear Admin,<br/>
            <br/>
In the past {$total_hours} hour(s) following deals have ended:<br/>
<br/>
";
        foreach($teams as $team){
            $url = $INI['system']['wwwprefix'].'/manage/team/edit.php?id='.$team['id'];
            $message .= "<a href='".$url."'>{$team['id']} - {$team['title_fr']}</a><br/>\n";
        }
        mail_custom($alert_email, "Deals ending in past Hour", $message);
	echo $message;
    }
    else
	{
	echo "No Deal Ended in Last {$total_hours} Hour(s)";
}
   
   $cond = array(                   
                    "expire_time >= {$past_hour}",
                    "expire_time < {$now}",
                    );
    $order = 'ORDER BY expire_time DESC';

    /* normal team */
    $teams = DB::LimitQuery('team', array(
                            'condition' => $cond,                            
                            'order' => $order,
                            ));
    
    if ( $teams && sizeof($teams) > 0 )
    {
        $message = "Dear Admin<br/>,
            
In the past hour following Deals have their Bons expired:<br/>
<br/>
";
        foreach($teams as $team){
            $url = $INI['system']['wwwprefix'].'/manage/team/edit.php?id='.$team['id'];
            $message .= "<a href='".$url."'>{$team['id']} - {$team['title_fr']}</a><br/>\n";
        }
        mail_custom($alert_email, "Bons expired in past Hour", $message);
    }
     else
	{
	echo "No Coupon expired in Last {$total_hours} Hour(s)";
}

$cond = array(                   
                    "close_time >= {$past_hour}",
                    "close_time < {$now}",
                    );
    $order = 'ORDER BY close_time DESC';

    /* normal team */
    $teams = DB::LimitQuery('team', array(
                            'condition' => $cond,                            
                            'order' => $order,
                            ));
    
    if ( $teams && sizeof($teams) > 0 )
    {
        $message = "Dear Admin<br/>,
            
In the past hour following Deals have sold out:<br/>
<br/>
";
      foreach($teams as $team){
            $url = $INI['system']['wwwprefix'].'/manage/team/edit.php?id='.$team['id'];
            $message .= "<a href='".$url."'>{$team['id']} - {$team['title_fr']}</a><br/>\n";
        } 
        mail_custom($alert_email, "Deals Sold out in past Hour", $message);
    }
     else
    {
	echo "No deals sold out in Last {$total_hours} Hour(s)";
    }
?>
