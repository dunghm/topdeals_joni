<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

 require_once(dirname(dirname(__FILE__)) . '/app.php');
 
// Find Deals that have reached Partner Statement Time
$now = strtotime(date('Y-m-d H:i'));
$condition = array(
    'partner_id != 0',
    "ps_start_time > 0 ",
    "ps <> 0 ",
    "ps_start_time < {$now}",
    "ps_last_time + ps_frequency < {$now}",   
);

 echo "Fetching Deals<br/>";
 
$teams = DB::LimitQuery('team', array(
        'condition' => $condition));

echo "Total Fetched: ". count($teams). "<br/>";
// Traverse Each Deal 
 $insert = array('team_id', 'partner_id', 'create_time', 'status', 'final');
 
 foreach ( $teams as $team )
 {
     echo "Updating Deal '{$team['title_fr']}' with Last PS Time <br/>";
     Table::UpdateCache('team', $team['id'], array( 'ps_last_time' => $now));
     
     echo "Fetching Partner <br/>";
     $partner = Table::Fetch('partner', $team['partner_id']);
     if ( !$partner ){
         echo "Partner not found. Moving to next Deal <br/>";
         continue;
     }
         
     
     // count coupons available for statement
     $count = Table::Count('coupon',
             array('team_id' => $team['id'],
                 'consume' => 'Y',
                 'ps_id' => '0',
                 'status' => '0'
                 ));
     
     echo "Coupons that are available to be included in the statement ".$count."<br/>";
     if ( $count == 0  ) 
     {
         echo "No coupons available. Checking if there have been any so far <br/>";
         $total_validated = Table::Count('coupon',
                 array('team_id' => $team['id'],
                     'consume' => 'Y'));
         
         if ( $total_validated == 0 ){
             echo "No coupon validated so far <br/>";
         
            mail_partner_no_coupon_yet($partner, $team);
            
         }
         else{
             echo "No coupon validated since last statement <br/>";
            mail_partner_no_coupon_since_last_ps($partner, $team);
         }
         echo "Moving to next deal <br/>";
         continue;
     }
     
     echo "Creating Partner Statement record <br/>";
     // new coupons validated
     $ps = array();
     $ps['team_id'] = $team['id'];
     $ps['create_time'] = time();
     $ps['partner_id'] = $team['partner_id'];
     $ps['status'] = 1;
     $ps['final'] = 0;
     
     // if deal is ended/soldout and all coupons have validated
     // or coupons have expired
     if ( (($team['end_time'] > 0 && $team['end_time'] < $now) ||
           ($team['close_time'] > 0 && $team['close_time'] < $now)) )
     {
         // Deal ended or sold out
        if ($team['expire_time'] > 0 && $team['expire_time'] < $now )
        {
            // coupons expired
            $ps['final'] = 1;
        }
        else
        {
            // coupons left or not?
             // count coupons available for statement
            $unused_count = Table::Count('coupon',
                    array('team_id' => $team['id'],
                        'consume' => 'N'
                        ));
            
            if ( $unused_count == 0 )
                $ps['final'] = 1;
        }
     }
     
     // Generate Statement 
     $table = new Table("partner_statement", $ps);
     $id = $table->insert($insert);
     $ps['id'] = $id;
     
     echo "UPdating coupon with Ps ID ".$ps['id']."<br/>";
     // Find Coupons that will go into Statement
     $sql = "UPDATE coupon SET ps_id = {$ps['id']} WHERE team_id = {$team['id']} AND ps_id = 0 AND consume='Y'";
     DB::Query($sql);
     
     $sql = "select ps_id, team_id, sum(Final) revenue from (
select c.ps_id, c.team_id, team.partner_revenue, ifnull(oi.option_id,0) option_id, 
tm.partner_revenue option_revenue, 
CASE WHEN oi.option_id IS NULL THEN team.partner_revenue ELSE tm.partner_revenue END Final
from coupon c left join 
order_item oi ON oi.id= c.order_id left join
team_multi tm on
oi.option_id = tm.id
left join team on 
c.team_id = team.id
where (team.partner_revenue > 0 or  tm.partner_revenue > 0)
and team.partner_id !=0 and team.ps_start_time > 0 and c.ps_id = {$ps['id']} and c.consume='Y') good
group by team_id, ps_id";

     $result = DB::GetQueryResult($sql);
     Table::UpdateCache('partner_statement', $id, array(
         "revenue" => $result['revenue']
     ));
    
     mail_partner_statement_ready($partner, $team);
	echo "Next Deal...<br/><Br/>---------------------<br/>";
 }
 
 echo "Finished <Br/>";
 
 function mail_partner_no_coupon_yet($partner, $team)
 {
     echo "entering mail_partner_no_coupon_yet <br/>";
     $subject = "Partner Statement Notice";
     $message = "No Coupons have been validated yet for the following Deal:<br/><br/>{$team['title_fr']}";
    // mail_custom($partner['email'], $subject, $message);
 }
 
 function mail_partner_no_coupon_since_last_ps($partner, $team)
 {
     echo "entering mail_partner_no_coupon_since_last_ps <br/>";
    $subject = "Partner Statement Notice";
    $message = "No Coupons have been validated since last Statement on '".strftime("d-m-Y",$team['ps_last_time'])."' for the following Deal:<br/><br/>{$team['title_fr']}";
 //   mail_custom($partner['email'], $subject, $message);
 }
 
 function mail_partner_statement_ready($partner, $team)
 {
     echo "entering mail_partner_statement_ready <br/>";
    $subject = "Partner Statement Notice";
    $message = "Account satement is ready for the following Deal:<br/><br/>{$team['title_fr']}<br/><br/>Please visit the Partner backend to download";
    // mail_custom($partner['email'], $subject, $message);
 }
 
 function mail_alert_expired_coupons($expired_coupons){
     //print_r($expired_coupons);
     $html_admin = '<table>';
     $html_admin .= '<tr>'
                  . '<td>DEAL</td>'
                  . '<td>PARTNER</td>'
                  . '<td>EXPIRED COUPONS</td>'
                  . '</tr>';
     foreach($expired_coupons as $ex_coupons){
          $html_admin .= '<tr>'
                  . '<td>'.$ex_coupons['team_title'].'</td>'
                  . '<td>'.$ex_coupons['patner_name'].'</td>'
                  . '<td>'.$ex_coupons['expired_coupons'].'</td>'
                  . '</tr>';  
          $html_partner = 'Coupons for deal: '.$ex_coupons['team_title'].' have '.$ex_coupons['patner_name']. ' expired coupons.';
          $subject_partner = 'Expired Coupons for '. $ex_coupons['team_title'];
          mail_custom($ex_coupons['partner_email'], $subject_partner, $html_partner);
     }
     $html_admin .= '</table>';
     
       global $INI;
       $emails[] = $INI['mail']['from'];
        $emails[] = 'abdullahrahim87@gmail.com';
        $emails[] = 'pmacloud@gmail.com';
        $subject = 'Expired Coupons Alert';
        mail_custom($emails, $subject, $html_admin);
     
 }
 
 echo 'Now Entering Expired Coupon Alert';
 
 
// $condition_expired_coupons = array(
//    "expire_time > {$now}",   
//);
//
// 
//$expired_coupons = DB::LimitQuery('team', array(
//        'condition' => $condition_expired_coupons));
//

$sql_expired_coupons = "SELECT COUNT(*) AS expired_coupons, c.`team_id`, t.`title` AS team_title, c.`partner_id`, p.`title` AS 'patner_name', p.`email` AS partner_email FROM coupon c
INNER JOIN team t ON t.`id` = c.team_id 
INNER JOIN `partner` p ON c.`partner_id` = p.`id`
WHERE c.expire_time < $now AND c.consume ='N' GROUP BY c.`partner_id`, c.team_id";
 
 $result_expired_coupons = DB::GetQueryResult($sql_expired_coupons, false);
 mail_alert_expired_coupons($result_expired_coupons);
	echo "Expired Coupon Alert Finished---------------------<br/>";

?>
