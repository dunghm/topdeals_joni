updated: 31/03/2014

User-agent: *//
Crawl-delay: 10

Disallow: /manage
Disallow: /biz
Disallow: /topdeal_prod/
Disallow: /account
Disallow: /topdeal_r3
Disallow: /topdeal/index_prelaunch.php
Disallow: /static/
Disallow: /order/
Disallow: /leader/
Disallow: /forum/
Disallow: /stage/
Disallow: /topdeal_stage/
