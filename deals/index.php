<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

need_login();

$now = time();
$condition = array(
	'system' => 'Y',
	'user_id' => $login_user_id,
	"end_time > {$now}",
);

/* filter start */
$team_type = strval($_GET['team_type']);
if ($team_type) { $condition['team_type'] = $team_type; }
/* filter end */

/* By Farhan*/
$user_id = $login_user_id;

$count = Table::Count('userdeals', $condition);
list($pagesize, $offset, $pagestring) = pagestring($count, 20);

$teams = DB::LimitQuery('userdeals', array(
	'condition' => $condition,
	'order' => 'ORDER BY id DESC',
	'size' => $pagesize,
	'offset' => $offset,
));
$cities = Table::Fetch('category', Utility::GetColumn($teams, 'city_id'));
$groups = Table::Fetch('category', Utility::GetColumn($teams, 'group_id'));

if ( $team_type == "weekly" )
    $selector = 'weekly';
else
    $selector = 'index';

include template('deals_index');
