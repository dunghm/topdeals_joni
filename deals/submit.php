<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');
require_once(dirname(dirname(__FILE__)) . '/include/function/current.php');

need_login();

$id = abs(intval($_GET['id']));
$team = $eteam = Table::Fetch('userdeals', $id);

if ( is_get() && empty($team) ) {
	$team = array();
	$team['id'] = 0;
	$team['user_id'] = $login_user_id;
	$team['begin_time'] = strtotime('+1 days');
	$team['end_time'] = strtotime('+2 days'); 
	$team['expire_time'] = strtotime('+3 months +1 days');
	$team['min_number'] = 10;
	$team['per_number'] = 1;
	$team['market_price'] = 1;
	$team['team_price'] = 1;
	$team['delivery'] = 'coupon';
	$team['address'] = $profile['address'];
	$team['mobile'] = $profile['mobile'];
	$team['fare'] = 5;
	$team['farefree'] = 0;
	$team['bonus'] = abs(intval($INI['system']['invitecredit']));
	$team['conduser'] = $INI['system']['conduser'] ? 'Y' : 'N';
	$team['buyonce'] = 'Y';
}
else if ( is_post() ) {
	$team = $_POST;
	$insert = array(
		'title', 'market_price', 'team_price', 'end_time', 
		'begin_time', 'expire_time', 'min_number', 'max_number', 
		'summary', 'notice', 'per_number', 'product', 
		'image', 'image1', 'image2', 'flv', 'now_number',
		'detail', 'userreview', 'card', 'systemreview', 
		'conduser', 'buyonce', 'bonus', 'sort_order',
		'delivery', 'mobile', 'address', 'fare', 
		'express', 'credit', 'farefree', 'pre_number',
		'user_id', 'city_id', 'group_id', 'partner_id',
		'team_type', 'sort_order', 'farefree', 'state',
		'condbuy','submit_time','part_name','part_add','part_phone','part_email','part_url','is_part',
		);

	$team['user_id'] = $login_user_id;
	$team['state'] = 'none';
	$team['begin_time'] = strtotime($team['begin_time']);
	$team['city_id'] = abs(intval($team['city_id']));
	$team['partner_id'] = abs(intval($team['partner_id']));
	$team['sort_order'] = abs(intval($team['sort_order']));
	$team['fare'] = abs(intval($team['fare']));
	$team['farefree'] = abs(intval($team['farefree']));
	$team['pre_number'] = abs(intval($team['pre_number']));
	$team['end_time'] = strtotime($team['end_time']);
	$team['expire_time'] = strtotime($team['expire_time']);
	$team['image'] = upload_image('upload_image',$eteam['image'],'team',true);
	$team['image1'] = upload_image('upload_image1',$eteam['image1'],'team');
	$team['image2'] = upload_image('upload_image2',$eteam['image2'],'team');
	$team['submit_time'] = date("d-m-Y");
	$team['part_name'] = $team['part_name'];
	$team['part_add'] = $team['part_add'];
	$team['part_phone'] = $team['part_phone'];
	$team['part_email'] = $team['part_email'];
	$team['part_url'] = $team['part_url'];
	if ($team['partner'] == 'Y')
	{
	$team['is_part'] = $team['partner'];
	}else
	{$team['is_part'] = 'N';}
	//team_type == goods
	if($team['team_type'] == 'goods'){ 
		$team['min_number'] = 1; 
		$tean['conduser'] = 'N';
	}

	if ( !$id ) {
		$team['now_number'] = $team['pre_number'];
	} else {
		$field = strtoupper($table->conduser)=='Y' ? null : 'quantity';
		/*$now_number = Table::Count('order', array(
		//			'team_id' => $id,
		//			'state' => 'pay',
		//			), $field);*/
                $now_number = Table::Count('order',
					"`team_id` = {$id} AND `state` != 'unpay' AND `state` != 'temporary' AND (`service` = 'cashondelivery' OR `state` = 'pay')"
					, $field);
		$team['now_number'] = ($now_number + $team['pre_number']);

		/* Increased the total number of state is not sold out */
		if ( $team['max_number'] > $team['now_number'] ) {
			$team['close_time'] = 0;
			$insert[] = 'close_time';
		}
	}

	$insert = array_unique($insert);
	$table = new Table('userdeals', $team);
	$table->SetStrip('summary', 'detail', 'systemreview', 'notice');
	

	if ( $team['id'] && $team['id'] == $id ) {
		$table->SetPk('id', $id);
		$table->update($insert);
		Session::Set('notice', 'Information Successfully Modified!');
		redirect( WEB_ROOT . "/deals/submit.php");
	} 
	else if ( $team['id'] ) {
		Session::Set('error', 'Illegal edition');
		redirect( WEB_ROOT . "/deals/submit.php");
	}


	if ( $table->insert($insert) ) {
		Session::Set('notice', 'New Project Success');
		redirect( WEB_ROOT . "/deals/submit.php");
	}
	else {
		Session::Set('error', 'Edit project failure');
		redirect(null);
	}
}

$groups = DB::LimitQuery('category', array(
			'condition' => array( 'zone' => 'group', ),
			));
$groups = Utility::OptionArray($groups, 'id', 'name');

$partners = DB::LimitQuery('partner', array(
			'order' => 'ORDER BY id DESC',
			));
$partners = Utility::OptionArray($partners, 'id', 'title');
$selector = $team['id'] ? 'edit' : 'submit';
include template('deals_submit2');
