<?php

require_once '../csrest_subscribers.php';

$email = $_REQUEST['email'];

$wrap = new CS_REST_Subscribers('819eb6f046f4323f8b0c1f0d8b6db6fd', '1d370dbd48e4178819880f06725bef2f');
$result = $wrap->delete($email);

//echo "Result of DELETE /api/v3/subscribers/{list id}.{format}?email={emailAddress}\n<br />";
if($result->was_successful()) {
    //echo "Unsubscribed with code ".$result->http_status_code;
} else {
    echo 'Failed with code '.$result->http_status_code."\n<br /><pre>";
    var_dump($result->response);
    echo '</pre>';
}