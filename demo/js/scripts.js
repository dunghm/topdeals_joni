$(document).ready(function() {
	$('#slide').cycle({
		pager: '.pager_container',
		pagerAnchorBuilder: function(index, element) {
			$(element[index]).html('');
		},
		slideExpr:'div.home_img'
	});
	$('#scrollable .container').cycle({
		'next': '#scrollable .next',
		'prev': '#scrollable .prev',
		'fit': true,
		'fx': 'scrollHorz'
	});
	$('#scrollable .container').cycle('pause');
	$('#interview').dropdowncontrol();
	if ($.browser.msie  && parseInt($.browser.version) == 8) {
		$('.header').css('padding-top','11px');
	}
});

(function($) {
	$.fn.dropdowncontrol = function() {
		$.currentItem = null;
		$(document).click(function(){
			if($.currentItem != null) {
				var answer = $.currentItem.find('.answer');
				answer.fadeOut('fast');
				$.currentItem.removeClass('selected');
			}
		});
		this.each(function() {
			$('.close',this).click(function(e){
				e.preventDefault();
				e.stopPropagation();
				if($.currentItem != null) {
					var answer = $.currentItem.find('.answer');
					answer.fadeOut('fast');
					$.currentItem.removeClass('selected');
				}
			});		
			$('.icon',this).click(function(e){
				e.preventDefault();
				e.stopPropagation();
				if($.currentItem != null) {
					var curAnswer = $.currentItem.find('.answer');
					curAnswer.fadeOut('fast');
					$.currentItem.removeClass('selected');
				}
				$(this).addClass('selected');
				var answer = $(this).find('.answer');
				$.currentItem = $(this);
				answer.slideDown('fast',function(){
					var top =	$(this).parents('.question_container').scrollTop();
					var destination = $(this).parent().position().top + top;
					$(this).parents('.question_container').animate({scrollTop:destination}, 500);				
				});
			});
		});
		
	return this;
};
})(jQuery);
