<?php

require_once(dirname(dirname(dirname(__FILE__))) . '/app.php');

//print_r ($_REQUEST); die;

$fname = $_REQUEST['firstname'];
$lname = $_REQUEST['lastname'];


$merchant = array( 
 'id' => $INI['swissbilling']['id'],  
 'pwd' => $INI['swissbilling']['pwd'], 
  'success_url'=> 'http://www.topdeal.ch/topdeal_r3/order/swissbilling/swissbilling-success.php', //'http://www.my_domain.com/success.php', 
  'cancel_url'=> 'http://www.topdeal.ch/topdeal_r3/', //'http://www.my_domain.com/cancel.php ', 
  'error_url'=> 'http://www.topdeal.ch/topdeal_r3/order/swissbilling/swissbilling-error.php', 
 ); 
 
 

    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    
 
$transaction = array( 
  'type' =>  $INI['swissbilling']['type'], //'Test', //"Real" for production 
  'is_B2B' => false, 
  'eshop_ID' => $INI['swissbilling']['id'], //rand(), //$_REQUEST['pay_id'], //constant set in payment method config 
  'eshop_ref' => rand().'-'.$_REQUEST['order_id'], //Order number 
  //'eshop_ref' => $_REQUEST['order_id'], //Order number 
  'order_timestamp' => date("Y-m-d H:i:s"), //'2012-01-04 13:45:55', 
  'currency' => 'CHF', //multi currency, can be chosen from the payment method config 
  'amount' => $_REQUEST['amount'], // Order amount 
  'VAT_amount' => '0.00', 
  'admin_fee_amount' => 0, 
  'delivery_fee_amount' => $_REQUEST['fare'], //fare 
  'coupon_discount_amount ' => 0, 
  'vol_discount' => '0.0', //Discount amount 
  'phys_delivery' => true, //virtual product == false 
  'delivery_status' => 'pending', 
  'debtor_IP' => $ip, 
  'signature' =>sha1($merchant_id.$ref_number.$amount.$public_key) //public is a string set in payment method config 
  ); 

/*
$transaction = array( 
  'type' => 'Test', //"Real" for production 
  'is_B2B' => false, 
  'eshop_ID' => rand(), //constant set in payment method config 
  'eshop_ref' => rand(), //Order number 
  'order_timestamp' => date("Y-m-d H:i:s"), //'2012-01-04 13:45:55', 
  'currency' => 'CHF', //multi currency, can be chosen from the payment method config 
  'amount' => 179.00, // Order amount 
  'VAT_amount' => '0.00', 
  'admin_fee_amount' => 0, 
  'delivery_fee_amount' => 0, 
  'coupon_discount_amount ' => 0, 
  'vol_discount' => '0.0', //Discount amount 
  'phys_delivery' => true, //virtual product == false 
  'delivery_status' => 'pending', 
  'debtor_IP' => '93.88.241.78', 
  'signature' =>sha1($merchant_id.$ref_number.$amount.$public_key) //public is a string set in payment method config 
  ); 
*/
 

$debtor = array( 
  'company_name' => '', 
  'title'  => '', 
  'firstname'  => $fname,//$_REQUEST['firstname'], 
  'lastname'  => $lname,//$_REQUEST['lastname'], 
  'birthdate'  => '1999-12-12', 
  'adr1'  => $_REQUEST['adr1'], 
  'city'  => $_REQUEST['city'], 
  'zip'  => $_REQUEST['zip'], 
  'country'  => 'CH', 
  'email'  => $_REQUEST['email'], 
  'phone' => $_REQUEST['phone'], 
  'language'  => 'FR', 
  'user_ID'  => $_REQUEST['user_ID'],    
  'is_SBMember'  => false, 
  'SBMember_ID'  => '' 
 ); 


/*
$debtor = array( 
  'company_name' => '', 
  'title'  => '', 
  'firstname'  => 'Minh-Anh', 
  'lastname'  => 'Pham', 
  'birthdate'  => '1999-12-12', 
  'adr1'  => 'Chemin des Berges', 
  'city'  => 'Chavannes-pres-Renens', 
  'zip'  => '1227', 
  'country'  => 'CH', 
  'email'  => 'mail2navaid@gmail.com', 
  'phone' => '+0787430263', 
  'language'  => 'FR', 
  'user_ID'  => '1458',    
  'is_SBMember'  => false, 
  'SBMember_ID'  => '' 
 ); 

*/

$items = array( 
'0' => array( 
  'short_desc' => 'Prestation sur TopDeal.ch', //$_REQUEST['desc_fr'] , 
  'desc' => 'Prestation sur TopDeal.ch', //$_REQUEST['desc_fr'], 
  'quantity' => $_REQUEST['quantity'] , 
  'unit_price' => $_REQUEST['unit_price'], 
  'VAT_rate' => 0, 
  'VAT_amount' => 0, 
  'file_link' => '',   
  'image_type' => 'PNG' , 
  'image' => ''),//base64_encode(fread($fp, filesize('static/css/i/bg-progress-pointer.gif')))), 
); 


/*
$items = array( 
'0' => array( 
  'short_desc' => 'Les lunettes Camsports Coach HD' , 
  'desc' => 'Les lunettes Camsports Coach HD', 
  'quantity' => 1 , 
  'unit_price' => 179, 
  'VAT_rate' => 0, 
  'VAT_amount' => 0, 
  'file_link' => 'http://topdeal.ch/topdeal_prod/team.php?id=74',   
  'image_type' => 'GIF' , 
  'image' => ''),//base64_encode(fread($fp, filesize('static/css/i/bg-progress-pointer.gif')))), 
); 
*/

//$url = 'http://request.swissbilling.ch/EShopRequest.wsdl';
$url = 'https://secure.safebill.ch/EShopRequestStdSec.wsdl'; 

try { 
  
  
    $client = new SoapClient($url,  
        array( "trace" => 0,"exceptions" =>1)); 
    $status = $client->EshopTransactionRequest($merchant, $transaction,  
    $debtor, count($items), $items); 
    $return_url = $status->url; 
    header('location:' . $return_url); 
} catch (SoapFault $exception) { 
  echo $exception;
  //$location = 'www.mysite.com/url_error.php'; 
  //  header('location:' . $location); 
} 
exit; 


?>