<?php
	 require_once(dirname(dirname(__FILE__)) . '/app.php');
     require_once('./datatrans/datatrans.conf.php');

	$ga_oid = Session::Get('ga_order_id');
    
	/*we need to verify if there is ga_order_id available in Session. If it's not there then we must not display the success page. rather we should send the user to order/index.php */
	if(!empty($ga_oid) && $login_user_id)
	{
		include template_ex('content_order_success');
	}
	else
	{
		redirect( WEB_ROOT . '/order/index.php ');
	}
	
?>