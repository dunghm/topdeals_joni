<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');
require_once(dirname(__FILE__) . '/paybank.php');
require_once(dirname(__FILE__) . '/datatrans/datatrans.conf.php');

need_login();

if (is_post()) {
	
	$order_id = abs(intval($_POST['order_id']));
} else {
	
	$order_id = $id = abs(intval($_GET['id']));
}


if(!$order_id || !($order = Table::Fetch('order', $order_id))) {
	redirect( WEB_ROOT. '/index.php');
}

$log = new KLogger(dirname(dirname(__FILE__))."/include/logs/", KLogger::DEBUG);
$log->logInfo("Pre Processing Order (PPO) #: {$order['id']}");

if ( $order['user_id'] != $login_user['id']) {
    $log->logError("PPO {$order['id']} doesn't belong to logged in user id {$login_user['id']}");
	redirect( WEB_ROOT . "/team.php?id={$order['team_id']}");
}

if ( $order['state'] == 'pay' )
{
    $log->logError("PPO {$order['id']} already paid. Sending to order view page.");
    redirect( WEB_ROOT . "/order/view.php?id={$order['id']}");
}

$order['realname'] 	= $_POST['first_name'] 	?: $login_user['first_name'];
$order['lastname'] 	= $_POST['last_name'] 	?: $login_user['last_name'];
$order['email'] 	= $_POST['email'] 		?: $login_user['email'];
$order['mobile'] 	= $_POST['mobile']		?: $login_user['mobile'];
$order['address'] 	= $_POST['address']		?: $login_user['address'];
$order['address2'] 	= $_POST['address2']	?: $login_user['address2'];
$order['address_no']= $_POST['address_no']	?: $login_user['address_no'];
$order['region'] 	= $_POST['region']		?: $login_user['region'];
$order['zipcode']	= $_POST['zipcode']		?: $login_user['zipcode'];
$order['state']		= 'unpay';

$order_items = Table::Fetch('order_item', array($order['id']), 'order_id');
$team_ids = Utility::GetColumn($order_items,'team_id');
$teams = Table::Fetch('team',$team_ids);

$bUpdateOrder = false;
$team_fare_once = array();

foreach($order_items as &$order_item)
{
    if ($order_item['mode'] == 'gift' )
    {
        $order_item['first_name'] = $_POST["gift_first_name"];
        $order_item['last_name'] = $_POST["gift_last_name"]; 
        
        if ( $teams[$order_item['team_id']]['delivery'] == 'express')
        {
            //$order_item['realname'] = $_POST['first_name'];
            //$order_item['lastname'] = $_POST['last_name'];
            $order_item['email'] = $_POST["email_{$order_item['id']}"];
            $order_item['mobile'] = $_POST["mobile_{$order_item['id']}"];
            $order_item['address'] = $_POST["address_{$order_item['id']}"];
            $order_item['address2'] = $_POST["address2_{$order_item['id']}"];
            $order_item['region'] = $_POST["region_{$order_item['id']}"];
            $order_item['zipcode'] = $_POST["zipcode_{$order_item['id']}"];
        }
    }
    //else
    {
        if ( $order_item['delivery'] == 'express_pickup' )
        {
            // user has to choose the delivery type
            $user_delivery_type = $_POST["delivery_option_{$order_item['id']}"];
            $order_item['delivery'] = $user_delivery_type;
            if ($order_item['delivery'] == 'express')
            {                
                if ( in_array($order_item['team_id'], $team_fare_once) == false)
                {
                    $team_fare_once[] = $order_item['team_id'];
                    
                    $fare =  $teams[$order_item['team_id']]['fare'];
                    if ( $fare > $order['fare']) // if Fare is greator than Fare charged so far
                    {
                        $fare = $fare - $order['fare']; // charge the diff
                        
                        $order_item['fare'] = $fare;
                        $order_item['total'] = ($order_item['price']*$order_item['quantity']) + $fare; 

                        $order['fare'] += $fare;
                        $order['origin'] += $fare;

                        $bUpdateOrder = true;
                    }
                }
            }
        }
        
        if ( $order_item['delivery'] == 'express' )
        {
            // This item will be shipped.
            
            if ( isset($_POST['delivery_address']) )
            {
                //User has specified a different Shipping address. 
                $order_item['first_name'] = $_POST["delivery_first_name"];
                $order_item['last_name'] = $_POST["delivery_last_name"];                 
                $order_item['address'] = $_POST["delivery_address"];
                $order_item['address2'] = $_POST["delivery_address2"];
                $order_item['region'] = $_POST["delivery_region"];
                $order_item['zipcode'] = $_POST["delivery_zipcode"];
            }
        }          
    }
   
    Table::UpdateCache('order_item', $order_item['id'], $order_item);
    
}

//if ( $bUpdateOrder )
{ 
    $cond = array('order_id' => $order['id']);
    $totalFare = Table::Count('order_item', $cond, 'fare');
    if ( !DB::Update('order', $order['id'], $order))
            die('failed to update order');
    
    Table::UpdateCache('user', $login_user_id, array(
        'first_name' => $order['realname'],
        'last_name'  => $order['lastname'],
        'mobile'  => $order['mobile'],
        'address'  => $order['address'],
        'address2'  => $order['address2'],
        'region'  => $order['region'],
        'zipcode'  => $order['zipcode'],        
    ));
}

/*
$team = Table::Fetch("team', $order['team_id']);
team_state($team);

// <---- noneed pay where goods soldout or end 
if ($team['close_time']) {
	Session::Set('notice', 'the deal is closed. You can not pay now');
	redirect(WEB_ROOT  . "/team.php?id={$order['team_id']}");
}
// end ------->
*/

$paytype = $_POST['paytype'];


if (is_post() && $paytype ) {
	
	$uarray = array( 'service' => pay_getservice($paytype) );
        
	Table::UpdateCache('order', $order_id, $uarray);
	$order = Table::Fetch('order', $order_id);
	$order['service'] = pay_getservice($paytype);
}

if ( $paytype !='credit'                // payment type is not Credit
		&& $_POST['service']!='credit' 
		&& $team['team_type']=='seconds' 
		&& ($order['origin']>$login_user['money'])
		&& option_yes('creditseconds')
   ) {
	$need_money = ceil($order['origin'] - $login_user['money']);
	Session::Set('error', "Deal can only use the balance of payment,your balance is insufficient,Also need to recharge {$need_money} amount to complete transaction.");
	redirect(WEB_ROOT . "/credit/charge.php?money={$need_money}");
}


//Todo: Move to Pre-order Creation and remove Team check (join with team)
//peruser buy count
/*
if ($_POST && $team['per_number']>0) {
        $sql = "select sum(order_item.quantity) as now_count from order_item left join `order` on order_id = `order`.id where user_id = {$login_user_id} AND state = 'pay' AND order_item.team_id = {$team['id']}";
        $count_result = DB::GetQueryResult($sql, true);
        $now_count = $count_result['now_count'];
	//$now_count = Table::Count('order_item', array(
	//	'user_id' => $login_user_id,
	//	'team_id' => $team['id'],
	//	'state' => 'pay',
	//), 'quantity');
	$leftnum = ($team['per_number'] - $now_count);
	if ($leftnum <= 0) {
		Session::Set('error', 'your purchase of this Deal has met the upper limit, have a look at other Hypes!');
		redirect( WEB_ROOT . "/team.php?id={$id}"); 
	}
}
*/
//payed order
if ( $order['state'] == 'pay' ) {  
    // The Order is Already PAID
	if ( is_get() ) {
		die(include template('order_pay_success'));		
	} else {
		redirect(WEB_ROOT  . "/order/pay.php?id={$order_id}");
	}
}

$order = Table::FetchForce('order', $order_id);

$anniversay_bonus = 0;


if ( $order['origin'] > 100 )
{
    $anniversary_bonus_start_date = strtotime('2012-11-22');
    $anniversay_bonus_end_date = strtotime('2012-11-23');
    if ( $order['create_time'] >= $anniversary_bonus_start_date && $order['create_time'] < ($anniversay_bonus_end_date + 300))
        $anniversary_bonus = 20;
}

// How much we need outside User Balance?
$money_required = moneyit($order['origin'] - $anniversary_bonus - $login_user['money']);

if ($money_required <= 0) { 
    // If enough balance, mark this order to be paid from CREDIT
    $money_required = 0; 
    $order['service'] = 'credit'; 
    
}

/* credit pay */
$credit = moneyit($order['origin'] - $money_required);
if ($order['credit'] != $credit) {
    Table::UpdateCache('order', $order_id, array('credit'=>$credit,));
}
/* end */
        

/* generate unique pay_id */
if (!($pay_id = $order['pay_id'])) {
	$randid = rand(1000,9999);
	$pay_id = "go-{$order['id']}-{$order['quantity']}-{$randid}";
	Table::UpdateCache('order', $order['id'], array(
				'pay_id' => $pay_id,
				));
}

/* credit pay */
if ( $_POST['action'] == 'redirect' ) {
	redirect($_POST['reqUrl']);
}
elseif ( $order['service'] == 'wire' )
{
    $user = Table::Fetch('user',$order['user_id']);
    mail_order_wire_transfer($user,$order,$team);
    include template('order/order_wirepay');
}
elseif ( $order['service'] == 'credit' ) {
	if ( $order['origin'] > $login_user['money'] ){
		Table::Delete('order', $order_id);
		redirect( WEB_ROOT . '/order/index.php');
	}
	Table::UpdateCache('order', $order_id, array(
				'service' => 'credit',
				'money' => 0,
				'state' => 'pay',
				'credit' => $order['origin'],
				'pay_time' => time(),
				));
	$order = Table::FetchForce('order', $order_id);
	ZTeam::BuyOne($order);
	  
    
        Session::Set('ga_order_id', $order['id']);        
        Session::Set('payment_success', true);
		
		//RESET BASKET AFTER ORDER SUCCESS
		resetBasketAfterOrderPaid();
		
        redirect(WEB_ROOT. '/order');
}
/* paypal support */
else if ( $order['service'] == 'paypal' ) {
	
	
  //      if ( $team['state'] == 'success')
            $paymentaction = "sale";                 /// Sale
    //    else
      //      $paymentaction = "authorization";                 /// Authorization

	$cmd = '_xclick';
        $Business = $INI['paypal']['username'];
	$location = $LC;
	
	//$uid = $INI['paypal']['username'];
	//$pwd = $INI['paypal']['password'];
	//$sign = $INI['paypal']['signature'];
	//$mailfrom = $INI['mail']['to'];
	
	$currency_code = $INI['system']['currencyname'];
	
	$item_number = $pay_id;
	$item_name = "Top Deal";
	$amount = $money_required;
	$quantity = 1;

	//$post_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
	$post_url = "https://www.paypal.com/row/cgi-bin/webscr";
	$image_url = "";
	$return_url = $INI['system']['secure_wwwprefix'] . '/order/confirmpaypal.php?success=1&id=' . $order['id'];
	$notify_url = $INI['system']['secure_wwwprefix'] . '/order/paypal/ipn.php';
	$cancel_url = $INI['system']['secure_wwwprefix'] . "/order/confirmpaypal.php";
		
	include template('order/order_getpay');
}

/* swissbilling support */
else if ( $order['service'] == 'swissbilling' ) {
  	
        
  //add logic
  	
	$amount = $money_required;
	$eshop_ref = $order_id; 
	$eshop_ID = $pay_id; 
  	$user = Table::Fetch('user',$order['user_id']);
  	$order = Table::FetchForce('order', $order_id);

  //echo $order['realname'].' - '.$order['lastname']; die;
  
	include template('order/order_getpay');
}
else if ( $order['service'] == 'datatrans' ) {
  //add logic
	if ( isset($_POST['cc_alias']) && $_POST['cc_alias'] == "1" )
		$cc_alias = Table::Fetch('user_cc_alias', $login_user_id, 'user_id');
		
	$amount = abs($money_required)*100;
	$eshop_ref = $order_id; 
	$eshop_ID = $pay_id; 
  	$user = Table::Fetch('user',$order['user_id']);
  	$order = Table::FetchForce('order', $order_id);
	$sign = DT_GetSign($merchantID.$amount.$currency.$pay_id);
	$currency =  $INI['system']['currencyname'];
	
	$return_url = $INI['system']['secure_wwwprefix']. '/order/datatrans/return.php';
	$error_url = $INI['system']['secure_wwwprefix']. '/order/datatrans/error.php';
	
	if ( $_POST['card_type'] == "mastercard" )
	{
		$paymentmethod = "ECA";
	}
	else if($_POST['card_type'] == "americanexpress")
	{
		$paymentmethod = "AMX";
	}
	else
	{
		$paymentmethod = "VIS";
	}
	if ( $cc_alias != false )
		include template('order_getpay');
	else
		include template_ex('content_datatrans');
}
else if ( $order['service'] == 'post' ) {
    
	$amount = abs($money_required)*100;
	$orderid = $pay_id;
	//$return_url = 'https://www.topdeal.ch/order/datatrans/return.php';
	$return_url = $INI['system']['secure_wwwprefix']. '/order/datatrans/return.php';
	//$return_url = 'http://dev.topdeal.ch:84/order/datatrans/return.php';
	$operation = "CAA";
	//$operation = "NOA"; // Authorization
	
	$currency =  $INI['system']['currencyname'];//"USD";
        $key = pack("H*" ,$hmac_key);

   	$sign = hash_hmac('md5', $merchantID.$amount.$currency.$orderid, $key);
     
	include template('order/order_getpay');
}
#for datatrans paypal
else if ( $order['service'] == 'dtpaypal' ) {
    
	$amount 		= abs($money_required)*100;
	$orderid 		= $pay_id;
	$operation 		= "CAA";
	$paymentmethod 	= "PAP";
	$return_url 	= $INI['system']['secure_wwwprefix']. '/order/datatrans/return.php';
	$currency 		=  $INI['system']['currencyname'];//"USD";
    $key	 		= pack("H*" ,$hmac_key);
   	$sign 			= hash_hmac('md5', $merchantID.$amount.$currency.$orderid, $key);
	
	include template('order/order_getpay');
}
/*
else if ( $order['service'] == 'post' ) {

       
        
	$uid = $INI['post']['username'];
	$pass = $INI['post']['password'];
	$amount = $money_required * 100;	
	$orderid = $pay_id;
	$accepturl = $INI['system']['wwwprefix'] . '/order/confirmpay.php';
	$cancelurl = $INI['system']['wwwprefix'] . "/order/confirmpay.php";
	//	$lan = "en_US";
	$currency =  $INI['system']['currencyname'];//"USD";
      // if ( $team['state'] == 'success')
           $operation = "SAL";                 /// Sale
       // else
         //   $operation = "RES";                 /// Authorization
	$pass = "8fjdf8@ui4i9fd2a";
    //$str = "AMOUNT=".$amount.$pass."CURRENCY=".$currency.$pass."OPERATION=".$operation.$pass."ORDERID=".$orderid.$pass."PSPID=".$uid.$pass;
	$str = "ACCEPTURL=".$accepturl.$pass."AMOUNT=".$amount.$pass."CANCELURL=".$cancelurl.$pass."CURRENCY=".$currency.$pass."OPERATION=".$operation.$pass."ORDERID=".$orderid.$pass."PSPID=".$uid.$pass;
	$hash = sha1($str);
	$hash = strtoupper($hash);
	

	include template('order/order_getpay');
}*/
elseif ( $_POST['service'] == 'cashondelivery' )
{
    $order = Table::FetchForce('order', $order_id);
    Table::UpdateCache('order', $order_id, array(
				'service' => 'cashondelivery',
				'money' => 0,
				'state' => 'pending'
				));
    ZTeam::Tipping($order);
    $user = Table::Fetch('user',$order['user_id']);
    mail_order_booked($user,$order,$team);
    redirect( WEB_ROOT . "/order/index.php");
}
else if ( $order['service'] == 'credit' || $order['service'] == 'cashondelivery' ) {
	$money_required = $order['origin'];
	die(include template('order/order_pay'));
} 
else {
	Session::Set('error', 'No suitable method of payment or credit');
	redirect( WEB_ROOT. "/team.php?id={$order['team_id']}");
}


Function appendParam($returnStr,$paramId,$paramValue){
	if($returnStr!=""){			
		if($paramValue!=""){					
			$returnStr.="&".$paramId."=".$paramValue;
		}			
	}else{		
		If($paramValue!=""){
			$returnStr=$paramId."=".$paramValue;
		}
	}		
	return $returnStr;
}

