<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');
/** DoDirectPayment NVP example; last modified 08MAY23.
 *
 *  Process a credit card payment. 
*/

$environment = 'sandbox';	// or 'beta-sandbox' or 'live'

/**
 * Send HTTP POST Request
 *
 * @param	string	The API method name
 * @param	string	The POST Message fields in &name=value pair format
 * @return	array	Parsed HTTP Response body
 */
		


function PPHttpPost($methodName_, $nvpStr_) {
	global $environment;

	// Set up your API credentials, PayPal end point, and API version.
	$API_UserName = urlencode($_POST['userid']);
	$API_Password = urlencode($_POST['pass']);
	$API_Signature = urlencode($_POST['sign']);
	$API_Endpoint = "https://api-3t.paypal.com/nvp";
	if("sandbox" === $environment || "beta-sandbox" === $environment) {
		$API_Endpoint = "https://api-3t.$environment.paypal.com/nvp";
	}
	$version = urlencode('51.0');

	// Set the curl parameters.
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);

	// Turn off the server and peer verification (TrustManager Concept).
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);

	// Set the API operation, version, and API signature in the request.
	$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";

	// Set the request as a POST FIELD for curl.
	curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

	// Get response from the server.
	$httpResponse = curl_exec($ch);

	if(!$httpResponse) {
		exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
	}

	// Extract the response details.
	$httpResponseAr = explode("&", $httpResponse);

	$httpParsedResponseAr = array();
	foreach ($httpResponseAr as $i => $value) {
		$tmpAr = explode("=", $value);
		if(sizeof($tmpAr) > 1) {
			$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
		}
	}

	if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
		exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
	}

	return $httpParsedResponseAr;
}

		$service = $_POST['service'];
		$order_id = $_POST['ord_id'];
		$pay_id = $_POST['item_number'];
		$qty = $_POST['quantity'];
// Set request-specific fields.
$paymentType = urlencode('Authorization');				// or 'Sale'
$firstName = urlencode($_POST['first_name']);
$lastName = urlencode($_POST['last_name']);
$creditCardType = urlencode($_POST['CCType']);
$creditCardNumber = urlencode($_POST['CCNo']);
$expDateMonth = urlencode($_POST['CCMonth']);
// Month must be padded with leading zero
$padDateMonth = urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));

$expDateYear = urlencode($_POST['CCYear']);
$cvv2Number = urlencode($_POST['CVVNo']);
$address1 = urlencode($_POST['address1']);
$address2 = urlencode($_POST['address2']);
$city = urlencode($city['name']);
$zip = urlencode($_POST['zip']);
$country = urlencode('CH');				// US or other valid country code
$amount = urlencode($_POST['amount']);
$currencyID = urlencode($_POST['currency_code']);							// or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')

// Add request-specific fields to the request string.
$nvpStr =	"&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber".
			"&EXPDATE=$padDateMonth$expDateYear&CVV2=$cvv2Number&FIRSTNAME=$firstName&LASTNAME=$lastName".
			"&STREET=$address1&CITY=$city&ZIP=$zip&COUNTRYCODE=$country&CURRENCYCODE=$currencyID";
print_r($nvpStr);
die();
// Execute the API operation; see the PPHttpPost function above.
$httpParsedResponseAr = PPHttpPost('DoDirectPayment', $nvpStr);

if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
	// exit('Direct Payment Completed Successfully: '.print_r($httpParsedResponseAr, true));
		
		$txn_id = $httpParsedResponseAr["TRANSACTIONID"];
		$currency = $httpParsedResponseAr["CURRENCYCODE"];
		//print_r($httpParsedResponseAr);
		

		ZOrder::OnlinePaypal($order_id,$pay_id,$amount,$qty, $currency,$txn_id, $service, 'paypal');
		//redirect( WEB_ROOT. "/order/confirmpay.php?id=".$order_id);
		
} else  {
	exit('DoDirectPayment failed: ' . print_r($httpParsedResponseAr, true));
}

?>