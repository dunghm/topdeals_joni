<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');
require_once(dirname(__FILE__) . '/paybank.php');

need_login();

if (is_post()) {
	
	$order_id = abs(intval($_POST['order_id']));
} else {
	if ( $_GET['id'] == 'charge' ) {
		redirect( WEB_ROOT. '/credit/index.php');
	}
	$order_id = $id = abs(intval($_GET['id']));
}
if(!$order_id || !($order = Table::Fetch('order', $order_id))) {
	redirect( WEB_ROOT. '/index.php');
}
if ( $order['user_id'] != $login_user['id']) {
	redirect( WEB_ROOT . "/team.php?id={$order['team_id']}");
}

$team = Table::Fetch('team', $order['team_id']);
team_state($team);

if (is_post() && $_POST['paytype'] ) {
	$uarray = array( 'service' => pay_getservice($_POST['paytype']) );
	Table::UpdateCache('order', $order_id, $uarray);
	$order = Table::Fetch('order', $order_id);
	$order['service'] = pay_getservice($_POST['paytype']);
}

if ( $_POST['paytype']!='credit'                // payment type is not Credit
		&& $_POST['service']!='credit' 
		&& $team['team_type']=='seconds' 
		&& ($order['origin']>$login_user['money'])
		&& option_yes('creditseconds')
   ) {
	$need_money = ceil($order['origin'] - $login_user['money']);
	Session::Set('error', "Deal can only use the balance of payment,your balance is insufficient,Also need to recharge {$need_money} amount to complete transaction.");
	redirect(WEB_ROOT . "/credit/charge.php?money={$need_money}");
}

//peruser buy count
if ($_POST && $team['per_number']>0) {
	$now_count = Table::Count('order', array(
		'user_id' => $login_user_id,
		'team_id' => $team['id'],
		'state' => 'pay',
	), 'quantity');
	$leftnum = ($team['per_number'] - $now_count);
	if ($leftnum <= 0) {
		Session::Set('error', 'your purchase of this Hype has met the upper limit, have a look at other Hypes!');
		redirect( WEB_ROOT . "/team.php?id={$id}"); 
	}
}

//payed order
if ( $order['state'] == 'pay' ) {  
	if ( is_get() ) {
		die(include template('order_pay_success'));		
	} else {
		redirect(WEB_ROOT  . "/order/pay.php?id={$order_id}");
	}
}

$total_money = moneyit($order['origin'] - $login_user['money']);
if ($total_money<0) { $total_money = 0; $order['service'] = 'credit'; }

/* generate unique pay_id */
if (!($pay_id = $order['pay_id'])) {
	$randid = rand(1000,9999);
	$pay_id = "go-{$order['id']}-{$order['quantity']}-{$randid}";
	Table::UpdateCache('order', $order['id'], array(
				'pay_id' => $pay_id,
				));
}


/* noneed pay where goods soldout or end */
if ($team['close_time']) {
	Session::Set('notice', 'the deal is closed. You can not pay now');
	redirect(WEB_ROOT  . "/team.php?id={$order['team_id']}");
}
/* end */

/* credit pay */
if ( $_POST['action'] == 'redirect' ) {
	redirect($_POST['reqUrl']);
}
elseif ( $order['service'] == 'wire' )
{
     $user = Table::Fetch('user',$order['user_id']);
    mail_order_wire_transfer($user,$order,$team);
    include template('order_wirepay');
}
elseif ( $_POST['service'] == 'cashondelivery' )
{
    $order = Table::FetchForce('order', $order_id);
    Table::UpdateCache('order', $order_id, array(
				'service' => 'cashondelivery',
				'money' => 0,
				'state' => 'pending'
				));
    ZTeam::Tipping($order);
    $user = Table::Fetch('user',$order['user_id']);
    mail_order_booked($user,$order,$team);
    redirect( WEB_ROOT . "/order/index.php");
}
elseif ( $_POST['service'] == 'credit' ) {
	if ( $order['origin'] > $login_user['money'] ){
		Table::Delete('order', $order_id);
		redirect( WEB_ROOT . '/order/index.php');
	}
	Table::UpdateCache('order', $order_id, array(
				'service' => 'credit',
				'money' => 0,
				'state' => 'pay',
				'credit' => $order['origin'],
				'pay_time' => time(),
				));
	$order = Table::FetchForce('order', $order_id);
	ZTeam::BuyOne($order);
	redirect( WEB_ROOT . "/order/pay.php?id={$order_id}");
}
/* paypal support */
else if ( $order['service'] == 'paypal' ) {
	/* credit pay */
	$credit = moneyit($order['origin'] - $total_money);
	if ($order['credit']!=$credit) {
		Table::UpdateCache('order', $order_id, array('credit'=>$credit,));
	}
	/* end */
	
        if ( $team['state'] == 'success')
            $paymentaction = "sale";                 /// Sale
        else
            $paymentaction = "authorization";                 /// Authorization

	$cmd = '_xclick';
        $Business = $INI['paypal']['username'];
	$location = $LC;
	
	//$uid = $INI['paypal']['username'];
	//$pwd = $INI['paypal']['password'];
	//$sign = $INI['paypal']['signature'];
	//$mailfrom = $INI['mail']['to'];
	
	$currency_code = $INI['system']['currencyname'];
	
	$item_number = $pay_id;
	$item_name = $team['title'];
	$amount = $total_money;
	$quantity = 1;

	//$post_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
	$post_url = "https://www.paypal.com/row/cgi-bin/webscr";
	$image_url = "";
	$return_url = $INI['system']['wwwprefix'] . '/order/confirmpaypal.php?success=1&id=' . $order['id'];
	$notify_url = $INI['system']['wwwprefix'] . '/order/paypal/ipn.php';
	$cancel_url = $INI['system']['wwwprefix'] . "/order/confirmpaypal.php";
		
	include template('order_getpay');
}

/* swissbilling support */
else if ( $order['service'] == 'swissbilling' ) {
  	/* credit pay */
	$credit = moneyit($order['origin'] - $total_money);
	if ($order['credit']!=$credit) {
            Table::UpdateCache('order', $order_id, array('credit'=>$credit,));
	}
	/* end */
        
  //add logic
  	
	$amount = $total_money;
	$eshop_ref = $order_id; 
	$eshop_ID = $pay_id; 
  	$user = Table::Fetch('user',$order['user_id']);
  
	include template('order_getpay');
}
else if ( $order['service'] == 'post' ) {

        /* credit pay */
	$credit = moneyit($order['origin'] - $total_money);
	if ($order['credit']!=$credit) {
            Table::UpdateCache('order', $order_id, array('credit'=>$credit,));
	}
	/* end */
        
	$uid = $INI['post']['username'];
	$pass = $INI['post']['password'];
	$amount = $total_money * 100;	
	$orderid = $pay_id;
	$accepturl = $INI['system']['wwwprefix'] . '/order/confirmpay.php';
	$cancelurl = $INI['system']['wwwprefix'] . "/order/confirmpay.php";
	//	$lan = "en_US";
	$currency =  $INI['system']['currencyname'];//"USD";
        if ( $team['state'] == 'success')
            $operation = "SAL";                 /// Sale
        else
            $operation = "RES";                 /// Authorization
	$pass = "8fjdf8@ui4i9fd2a";
    //$str = "AMOUNT=".$amount.$pass."CURRENCY=".$currency.$pass."OPERATION=".$operation.$pass."ORDERID=".$orderid.$pass."PSPID=".$uid.$pass;
	$str = "ACCEPTURL=".$accepturl.$pass."AMOUNT=".$amount.$pass."CANCELURL=".$cancelurl.$pass."CURRENCY=".$currency.$pass."OPERATION=".$operation.$pass."ORDERID=".$orderid.$pass."PSPID=".$uid.$pass;
	$hash = sha1($str);
	$hash = strtoupper($hash);
	/*echo 
	print_r($str);
	echo "<br><br>";
	$hash = sha1($str);
	print_r($hash);
	die();*/

	include template('order_getpay');
}
else if ( $order['service'] == 'credit' || $order['service'] == 'cashondelivery' ) {
	$total_money = $order['origin'];
	die(include template('order_pay'));
} 
else {
	Session::Set('error', 'No suitable method of payment or credit');
	redirect( WEB_ROOT. "/team.php?id={$order['team_id']}");
}


Function appendParam($returnStr,$paramId,$paramValue){
	if($returnStr!=""){			
		if($paramValue!=""){					
			$returnStr.="&".$paramId."=".$paramValue;
		}			
	}else{		
		If($paramValue!=""){
			$returnStr=$paramId."=".$paramValue;
		}
	}		
	return $returnStr;
}

