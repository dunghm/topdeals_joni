<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

need_login();
$order_id = $id = strval(intval($_GET['id']));
if (!$order_id)
{
    $order_item_id =  strval(intval($_GET['oiid']));
    $order_item = Table::Fetch('order_item',$order_item_id);
    if ($order_item)
       $order_id = $id = $order_item['order_id'];    
}

if(!$order_id || !($order = Table::Fetch('order', $order_id))) {
	redirect( WEB_ROOT . "/index.php");
}
if ( $order['user_id'] != $login_user['id']) {
	redirect( WEB_ROOT . "/team.php?id={$order['team_id']}");
}
/*
if ( $order['state']=='unpay') {
	redirect( WEB_ROOT . "/team.php?id={$order['team_id']}");
}
*/
$order_cards = Table::Fetch('order_card', array($order_id), 'order_id');
$order_items = ZOrder::GetItems($order['id']);
$team_ids = Utility::GetColumn($order_items, 'team_id');

$option_ids = Utility::GetColumn($order_items, 'option_id');
$options = Table::Fetch('team_multi', $option_ids);
$teams = Table::Fetch('team', $team_ids);

//$team = Table::FetchForce('team', $order['team_id']);
//$partner = Table::Fetch('partner', $order['partner_id']);
//$express = ($team['delivery']=='express');
//if ( $express ) { $option_express = Utility::OptionArray(Table::Fetch('category', array('express'), 'zone'), 'id', 'name'); }

/*
if ( $team['delivery'] == 'coupon' ) {
	$cc = array(
			'user_id' => $login_user['id'],
			'team_id' => $order['team_id'],
			'order_id' => $order['id'],
			);


	$coupons = DB::LimitQuery('coupon', array(
				'condition' => $cc,
				));
 
}
*/

include template('content_payes_detail');
