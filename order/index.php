<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

need_login();
$condition = array( 'user_id' => $login_user_id, );
$selector = strval($_GET['s']);

/*
 *After a successful purchase, when we click on the "Mes achats" button, nothing happens, 
 *we need to click a second time. I'm wondering if there is an issue with the link
 */
 
/* if( $session_notice=Session::Get('payment_success',true)){
   
	//Session::Set('payment_success', true);
	redirect( WEB_ROOT . '/order/success.php ');
	//include template_ex("content_order_success");
    exit;
} */

$template = "content_payes";
$condition['state'] = 'pay';

if ( $selector == 'index' ) {
}
else if ( $selector == 'unpay' ) {
	$condition['state'] = 'unpay';
        $template = "content_apayes";
}
else if ( $selector == 'pay' ) {
	$condition['state'] = 'pay';
}
$pg=1;
if ($_REQUEST['pg']){
	$pg=$_REQUEST['pg'];
}
$count = Table::Count('order', $condition);
list($pagesize, $offset, $pagestring) = pagestring($count, 4);
$pagesize=4;
$offset=$pg*$pagesize-$pagesize;

$mode = $_REQUEST['mode'];
if($mode && $mode=='ajaxRequest')
{	
	$pagesize = 8;	
	$offset=$_REQUEST['offset'];
}

$orders = DB::LimitQuery('order', array(
	'condition' => $condition,
	'order' => 'ORDER BY create_time DESC',
	'size' => $pagesize,
	'offset' => $offset,
));


$order_items = Table::Fetch('order_item', Utility::GetColumn($orders,'id'), 'order_id');

$team_ids = Utility::GetColumn($order_items, 'team_id');
$option_ids = Utility::GetColumn($order_items, 'option_id');
$coupons = Table::Fetch('coupon', Utility::GetColumn($order_items,'id'), 'order_id');
foreach($coupons as $cpn)
	$coupons[$cpn['order_id']][] = $cpn;
	
//$coupons = Utility::AssColumn($coupons, 'order_id');
$orders = Utility::AssColumn($orders, 'id');
//$team_ids = Utility::GetColumn($orders, 'team_id');
$teams = Table::Fetch('team', $team_ids);
$options = Table::Fetch('team_multi', $option_ids);
$options = Utility::AssColumn($options, 'id');

foreach($teams AS $tid=>$one){
	team_state($one);
	$teams[$tid] = $one;
}

function SortByOrderIDDesc($lhs, $rhs){
    if ( $lhs['order_id'] == $rhs['order_id'] ) return 0;
    
    return ($rhs['order_id'] < $lhs['order_id'] ? -1 : 1);
}

usort($order_items,'SortByOrderIDDesc');

$recommended_deals = ZTeam::GetRecommended(2, false, $login_user_id);
$orders = Utility::AssColumn($orders,'id');
$order_items = Utility::AssColumn($order_items, 'order_id', 'id');

$pagetitle = 'My order';
if($mode && $mode=='ajaxRequest')
{	
  die (include template_sub('content/content_payes_extended'));
}else{
    include template_ex($template);
}

