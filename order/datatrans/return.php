<?php

    require_once(dirname(dirname(dirname(__FILE__))). '/app.php');
    require_once('./datatrans.conf.php');
    
    
    
    if (!is_array($_POST)) 
    {
        Session::Set("payment_failed", true);
        redirect($INI['system']['wwwprefix']);

    }

    $datatrans = $_POST;


	$notDataTransPaypal = true;

	if(!empty($datatrans['pmethod']))
	{
		if($datatrans['pmethod']=="PAP")
		{
			$notDataTransPaypal = false;
		}
	}

/* if pmethod not datatrans paypal*/
if($notDataTransPaypal)
{	
	// check the hmac for security level 2
	if ( $hmac_key_2 || $hmac_key ) {
			
		$sign = hash_hmac('md5', $merchantID.$datatrans['amount'].$datatrans['currency'].$datatrans['uppTransactionId'], pack("H*" ,$hmac_key_2));
	   
			if ($sign!=$datatrans['sign2']) {
			
				Session::Set("payment_failed", true);
				#payment method error detail
				if(!empty($datatrans['errorDetail']))
				{
					 Session::Set("errorDetail", $datatrans['errorDetail']);
				}
				redirect($INI['system']['wwwprefix']);
			}
	}
}

    
if ( $datatrans['status'] == 'success' )
{
	$pay_id = $datatrans['refno'];
    
    @list($_, $order_id, $_, $_) = explode('-', $datatrans['refno'], 4);
    $currency  = $datatrans['currency'];
	$amount = $datatrans['amount']/100;
	
    if ( $datatrans['pmethod'] == "VIS" || $datatrans['pmethod'] == "ECA" || $datatrans['pmethod'] == "AMX" ){
        $service = $bank = "datatrans";
		//$amount = $datatrans['amount']/100;
	}
	else if($datatrans['pmethod'] == "PAP" )
	{
		 $service = $bank = "dtpaypal";
	}
    else
        $service = $bank = "post";
   
   

   
    $transaction_id = $datatrans['uppTransactionId'];
    $transaction_state = "sale";

    ZOrder::OnlineIt($order_id, $pay_id, $amount, $currency, $service, $bank, $transaction_state, $transaction_id); 

    $order = Table::FetchForce('order', $order_id);
    
    if ( isset($datatrans['save_cc']) && isset($datatrans['aliasCC']) )
    {     
        Table::Delete('user_cc_alias', $order['user_id'], 'user_id' );
        DB::Insert('user_cc_alias', array("cc_alias" => $datatrans['aliasCC'], 
            "user_id" => $order['user_id'],
            "expm" => $datatrans['expm'],
            "expy" => $datatrans['expy'],
            "method" => $datatrans["pmethod"],
            "cc_masked" => $datatrans['maskedCC']));
    }

    if ( $order['state'] == 'pay' )
    {
        Session::Set('ga_order_id', $order['id']);
        Session::Set('payment_success', true);
       // redirect($INI['system']['wwwprefix'].'/order');
	   
		//RESET BASKET AFTER ORDER SUCCESS
		resetBasketAfterOrderPaid();
		redirect( WEB_ROOT . '/order/success.php ');
		
		#redirect('success.php');
#include template_ex('content_order_success');
        exit;
    }
}

Session::Set("payment_failed", true);
//redirect($INI['system']['wwwprefix']);
    include template_ex('content_order_error');
    

?>
