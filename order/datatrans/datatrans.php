<?php

    require_once(dirname(dirname(dirname(__FILE__))). '/app.php');
	require_once(dirname(__FILE__). '/datatrans.conf.php');
    need_login();
    
	$bSavedAlias = false;
	
    if (isset($_POST) )
    {
        $currency = "CHF";
		
		$order_id = $_POST['order_id'];
        $cvv = $_POST['cvv'];//"123";    
        $amount = abs($_POST['amount']); /// 10 CHF
		$refno = $_POST['pay_id']; //"Pay-500-3201-".$amount;
		
		if ( isset($_POST['new_cc']) )
        {
            $cardno = $_POST['ccno'];//"4900000000000003";
            $expm = $_POST['expm'];//"12";
            $expy = $_POST['expy'];//"15";           
        }
		else
		{			
			$cc_alias = Table::Fetch('user_cc_alias', $login_user_id, 'user_id');
            if ( $cc_alias )
            {                
                $expm = $cc_alias['expm'];
                $expy = $cc_alias['expy'];
				$cardno = $cc_alias['cc_alias'];
				$bSavedAlias = true;
            }
		}
		
		$sign = DT_GetSign( $merchantID.$amount.$currency.$refno );
		
        $params = array(
            "cardno" => $cardno,
            "refno" => $refno,          // Order Payment ID
            "amount" => $amount,
            "currency" => $currency,
            "expm" => $expm,
            "expy" => $expy,
			"cvv" => $cvv,        
			"sign" => $sign,
        );
		
		if ( $bSavedAlias )
			$params['cc_alias'] = $cardno;
    
        
        
        //$merchantID = "1100002495";
        $options = array ("merchantID" => $merchantID);
        $datatrans = new DataTrans($options, $mode);
    
        $transid = $datatrans->MakeAuthorization($params);    // Do this for Complete Payment
        if ( !$transid )
        {
            Session::Set("payment_failed", true);
            redirect(WEB_ROOT);
        }
        else
        {
            $service = $bank = "datatrans";
            ZOrder::OnlineIt($order_id, $refno, $amount, "CHF", $service, $bank, 'sale', $transid); 
	

            $order = Table::FetchForce('order', $order_id);
            if ( $order['state'] == 'pay' )
            {                
                Session::Set('ga_order_id', $order['id']);               
            }
			
			
			if ( isset($_POST['save_cc']) )
			{
				$maskedCC = "";
				$cc_alias = $datatrans->GetCCAlias($cardno, $maskedCC);
				if ( $cc_alias ){
					Table::Delete('user_cc_alias', $login_user_id, 'user_id' );
					DB::Insert('user_cc_alias', array("cc_alias" => $cc_alias, 
						"user_id" => $login_user_id,
						"expm" => $expm,
						"expy" => $expy,
						"cc_masked" => $maskedCC));
				}
			}
			
			
            Session::Set('payment_success', true);
            //redirect(WEB_ROOT. '/order');
		include template_ex('content_order_success');
        }
    
        exit;
    }
   
    
    redirect(WEB_ROOT);
    
?>
