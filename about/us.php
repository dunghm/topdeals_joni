<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

$page = Table::Fetch('page', 'about_us');
$pagetitle = 'TopDeal.ch - A propos';
$subtemplate = 'content_about';
include template('master');
