<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

$team_id = strval($_GET['team_id']);

if(!empty($team_id)){
	// $html = render('ajax_dialog_partner_coupon');
	
	$revenue	=	strval($_GET['revenue']);
	$team = Table::Fetch('team', $team_id);
    if ( !$team ){
        $errorHtml	=	 "Deal not found.";
    }
    
    // echo "Fetching Partner <br/>";
     $partner = Table::Fetch('partner', $team['partner_id']);
     if ( !$partner ){
        $errorHtml	= "Partner not found.";
     }
         
     
     // count coupons available for statement
     $count = Table::Count('coupon',
             array('team_id' => $team['id'],
                 'consume' => 'Y',
                 'ps_id' => '0'));
     
     //echo "Coupons that are available to be included in the statement ".$count."<br/>";
     if ( $count == 0  ){
        $errorHtml	=	 "No coupons available.";        
     }
     
	 
	// count coupons available for statement
    $couponFetchSql = "SELECT id,status,comment FROM coupon WHERE team_id = $team_id AND ps_id = 0 AND consume='Y'";
	$couponFetch 	= DB::GetQueryResult($couponFetchSql, false);			 
				 
	// echo "<pre>";print_r($couponFetch);exit();
	$html = <<<HTML
		
		<link rel="stylesheet" type="text/css" href="/static/datatable/css/jquery.dataTables.css">
		
		<style type="text/css" class="init"></style>
		<script type="text/javascript" language="javascript" src="/static/datatable/js/jquery.js"></script>
		<script type="text/javascript" language="javascript" src="/static/datatable/js/jquery.dataTables.js"></script>
	
		
		<script type="text/javascript" language="javascript" class="init">
		jQuery(document).ready(function() {
			
			var table	=	jQuery('#example').DataTable({
				"iDisplayLength" 	: -1,
				"scrollY"			: "300px",
				"scrollCollapse"	: true,
				"paging"			: false,
				"oLanguage"			: {
										"sSearch": "Search Coupon: "
									  }
			});
			
		} );


			</script>
		
		<div id="order-pay-dialog" class="order-pay-dialog-c" style="width:900px;">
		<h3><span id="order-pay-dialog-close" class="close" onclick="return X.boxClose();">Fermer</span>Generate Statement For Deal # $team_id : Total Coupon Found : $count</h3>
		<!-- <p class="info" id="coupon-dialog-display-id">Generate Statement $team_id</p> -->
HTML;

	if($errorHtml!=""){
	
		$html .= <<<HTML
			<p class="notice">
				<div style="color:red; padding:15px;">$errorHtml</div>
			</p>
HTML;

	}else{
		
		$html .= <<<HTML
			<!-- <p class="notice"> -->
HTML;

		if(!empty($couponFetch)){
			
		$html .= <<<HTML
			
			
				<form action="/manage/statement/generate.php" method="post" id="generate_partner_statement">
				
				

					<div style="padding:20px;" >
					<input type="checkbox" id="selecctall" /> Select All Coupon
					<table id="example" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Coupon No.</th>
							<th>Comment</th>
						</tr>
					</thead>

					<tbody>
					
HTML;
			foreach($couponFetch as $couponFetchRow){
					
				$couponId		=	$couponFetchRow['id'];
				$couponStatus	=	$couponFetchRow['status'];
				$couponComment	=	$couponFetchRow['comment'];
					
				$html .= <<<HTML
					
					<tr>
						<td width="30%"><input type="checkbox" value="$couponId" id="$couponId" name="coupon[$couponId]['id']" class="checkbox1" /> $couponId</td>
						<td width="50%"><input type="text" value="$couponComment" id="coupon_comment_$couponId" name="coupon[$couponId]['comment']" size="50"/></td>
					</tr>
HTML;
			}
		$html .= <<<HTML
					</tbody>
				</table>
			</div>
			<div style="clear:both;"></div>
			<div style="float:right; padding:20px;">
				
				<input type="hidden" name="team_id" value="$team_id"/>
				<input type="hidden" name="revenue" value="$revenue"/>
				<span id="submit-button-region" >
					<input id="partner-statemnet-submit" name="partner-statemnet-submit" style="width:223px !important;" class="formbutton" value="Generate Statement"  type="button" >
				</span>
				<span id="waiting-region" style="display:none;">Please Wait.....</span>
				</form>
			</div>
			<div class="err-region" style="display:none; color:red;  padding:10px;"></div>
HTML;
		}
		$html .= <<<HTML
			</p>
HTML;

	}

	$html .= <<<HTML
	  <p class="act">    
	  </p>
	</div>
	
	<script>
		jQuery(document).ready(function() {
			jQuery('#selecctall').click(function(event) {  //on click
				if(this.checked) { // check select status
					jQuery('.checkbox1').each(function() { //loop through each checkbox
						this.checked = true;  //select all checkboxes with class "checkbox1"              
					});
				}else{
					jQuery('.checkbox1').each(function() { //loop through each checkbox
						this.checked = false; //deselect all checkboxes with class "checkbox1"                      
					});        
				}
			});
		   
		   //FORM VALIDATION
			jQuery("#partner-statemnet-submit").on("click",function(){
				//GET SEARCH BOX VALUE
				var searchVal	=	jQuery("input[type='search']").val();
				
			
				
				var errMessage	=	"";
				
				//IF SEARCH BOX IS NOT EMPTY THEN FORM SUBMISSION NO TALLOW
				if(searchVal!=""){
					errMessage += "<p>Please reset your search field before generate statement</p><br/>";
				}
				
				jQuery('.checkbox1').each(function() { //loop through each checkbox
					if(!this.checked) {               
						var couponNum		=	jQuery(this).attr('id');
						var couponComment	=	jQuery.trim(jQuery("#coupon_comment_"+couponNum).val());
						if(couponComment==""){
								
								errMessage += "<p>Please Enter Comment for Ommitting Coupon # "+couponNum+"</p><br/>";
						}
					}
				});
				
				
				
				
				//error message display
				if(errMessage!=""){
					jQuery(".err-region").html(errMessage);
					jQuery(".err-region").css("display","block");
					
					jQuery("#submit-button-region").show();
					jQuery("#waiting-region").hide();
				}else{
					
					jQuery("#submit-button-region").hide();
					jQuery("#waiting-region").show();
					
				
					jQuery(".err-region").html("");
					jQuery(".err-region").css("display","none");
					
					
					jQuery("#generate_partner_statement").submit();
				}
				
			});
		});
		
	
		
		jQuery('#selecctall').attr('checked', true);
		jQuery('.checkbox1').each(function() { //loop through each checkbox
			this.checked = true;  //select all checkboxes with class "checkbox1"              
		});
		
	</script>
	<style>
		#example thead{
			display:none !important;
		}
	</style>
HTML;

	json($html, 'dialog');
}
