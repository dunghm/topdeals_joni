<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

need_manager();

$action = strval($_GET['action']);
$id = abs(intval($_GET['id']));

if ( 'orderrefund' == $action) {
	need_auth('admin');
        $reason_refund = $_GET['reason_refund'];
	$order = Table::Fetch('order', $id);
	$rid = strtolower(strval($_GET['rid']));
        if ( $rid == 'credit' ) {
		ZFlow::CreateFromRefund($order, $reason_refund);
	} else {
		Table::UpdateCache('order', $id, array(
					'service' => 'cash',
					'state' => 'refund'
			));
	}
	/* team -- */
	//$order_ids = Utility::Getcolumn($order,"id");
        $order_items = Table::Fetch('order_item', array( $order['id']), 'order_id');
        
        foreach( $order_items as $oi ){
            
            if( $oi['delivery'] == 'coupon' || $oi['delivery'] == 'express_pickup' ||$oi['delivery'] == 'pickup'){
                Table::Delete('coupon', $oi['id'], 'order_id');
            } 
            
            //if its an option then deduct option 
            if($oi['option_id'] ){
                $option = Table::Fetch('team_multi', $oi['option_id']);
                
                
                if($option){
                    
                        $minus = $oi['quantity'];
                        $condition_update = array(
                                    'now_number' => array( "now_number - {$minus}" )
                        );
                        //     Deduct Stock                      
                        if( $oi['delivery'] != 'coupon' && $oi['delivery_status'] != ZOrder::ShipStat_Invalid && $oi['delivery_status'] != ZOrder::ShipStat_Waiting  )                           
                        {
                           $condition_update["stock_count"] = array( "`stock_count` + {$minus}");
                        }                         
                        Table::UpdateCache('team_multi', $option['id'], $condition_update);    
                        
                }
            }
            else{
                $team = Table::Fetch('team', $oi['team_id']);
                
                if($team){
                    team_state($team);
                    if ( $team['state'] != 'failure' ) {
                            $minus = $team['conduser'] == 'Y' ? 1 : $oi['quantity'];
                            
                            $condition_update = array(
                                    'now_number' => array( "now_number - {$minus}" )
                            );
                            //     Deduct Stock                      
                            if( $oi['delivery'] != 'coupon' && $oi['delivery_status'] != ZOrder::ShipStat_Invalid && $oi['delivery_status'] != ZOrder::ShipStat_Waiting  )                           
                            {
                               $condition_update["stock_count"] = array( "`stock_count` + {$minus}");
                            }                         
                            Table::UpdateCache('team', $team['id'], $condition_update);                        
                    }
                }
            }
            
        }
        
        //Multiple cards deleted
        $order_cards = Table::Fetch('order_card',array($order['id']),'order_id');
        if($order_cards){
               foreach($order_cards as $oc){
                   if ( $oc['card_id'] ) {
                            Table::UpdateCache('card', $oc['card_id'], array(
                                    'consume' => 'N',
                                    'team_id' => 0,
                                    'order_id' => 0,
                            ));
                    }
               }
        }
        Table::Delete('order_card', $order['id'], 'order_id');
        
        
	/* card refund */
	if ( $order['card_id'] ) {
		Table::UpdateCache('card', $order['card_id'], array(
			'consume' => 'N',
			'team_id' => 0,
			'order_id' => 0,
		));
	}
	

	/* order update */
	Table::UpdateCache('order', $id, array(
				'card' => 0, 
				'card_id' => '',
				'express_id' => 0,
				'express_no' => '',
				));
        
        $user = Table::Fetch('user', $order['user_id']);
        mail_order_refund($user, $order, false,$rid);
	Session::Set('notice', 'Refunded');
	json(null, 'refresh');
}
elseif ( 'orderremove' == $action) {
	need_auth('order');
	$order = Table::Fetch('order', $id);
	if ( $order['state'] != 'unpay' ) {
		json('Payment orders can not be deleted', 'alert');
	}
	/* card refund */
	if ( $order['card_id'] ) {
		Table::UpdateCache('card', $order['card_id'], array(
			'consume' => 'N',
			'team_id' => 0,
			'order_id' => 0,
		));
	}
	Table::Delete('order', $order['id']);
	Session::Set('notice', "Delete Order {$order['id']} Success");
	json(null, 'refresh');
}
else if ( 'ordercash' == $action ) {
	need_auth('order');
	$order = Table::Fetch('order', $id);
	ZOrder::CashIt($order, $_GET['mode']);
	$user = Table::Fetch('user', $order['user_id']);
        //mail_payment_success($user,$order);
        Session::Set('notice', "Cash payment was successful, the purchase users:{$user['email']}");
	json(null, 'refresh');
}
else if ( 'ordercancel' == $action ) {
	need_auth('order');
	$order = Table::Fetch('order', $id);
	if ( ZOrder::Cancel($order) )
        {
            $user = Table::Fetch('user', $order['user_id']);
            mail_order_cancel($user,$order);
            Session::Set('notice', "Order cancelled successfully, the purchase users:{$user['email']}");            
        }
        else
        {
            Session::Set('error', "Order could not be cancelled, the purchase users:{$user['email']}");
        }
        json(null, 'refresh');
}
else if ( 'teamdetail' == $action) {
	$team = Table::Fetch('team', $id);
	$partner = Table::Fetch('partner', $team['partner_id']);

	$paycount = Table::Count('order', array(
		'state' => 'pay',
		'team_id' => $id,
	));
	$buycount = Table::Count('order', array(
		'state' => 'pay',
		'team_id' => $id,
	), 'quantity');
	$onlinepay = Table::Count('order', array(
		'state' => 'pay',
		'team_id' => $id,
	), 'money');
	$creditpay = Table::Count('order', array(
		'state' => 'pay',
		'team_id' => $id,
	), 'credit');
	$cardpay = Table::Count('order', array(
		'state' => 'pay',
		'team_id' => $id,
	), 'card');
	$couponcount = Table::Count('coupon', array(
		'team_id' => $id,
	));
	$team['state'] = team_state($team);
	$city_id = abs(intval($team['city_id']));
	$subcond = array(); if($city_id) $subcond['city_id'] = $city_id;
	$subcount = Table::Count('subscribe', $subcond);
	$subcond['enable'] = 'Y';
	$smssubcount = Table::Count('smssubscribe', $subcond);

	/* send team subscribe mail */	
	$team['noticesubscribe'] = ($team['close_time']==0&&is_manager());
	$team['noticesmssubscribe'] = ($team['close_time']==0&&is_manager());
	/* send success coupon */
	$team['noticesms'] = ($team['delivery']!='express')&&(in_array($team['state'], array('success', 'soldout')))&&is_manager();
	/* teamcoupon */
	$team['teamcoupon'] = ($team['noticesms']&&$buycount>$couponcount);
	$team['needline'] = ($team['noticesms']||$team['noticesubscribe']||$team['teamcoupon']);

	$html = render('manage_ajax_dialog_teamdetail');
	json($html, 'dialog');
}
else if ( 'teamremove' == $action) {
	need_auth('team');
	$team = Table::Fetch('team', $id);
	$order_count = Table::Count('order', array(
		'team_id' => $id,
		'state' => 'pay',
	));
	if ( $order_count > 0 ) {
		json('Including the payment of the buy orders,You can not delete', 'alert');
	}
	ZTeam::DeleteTeam($id);

	/* remove coupon */
	$coupons = Table::Fetch('coupon', array($id), 'team_id');
	foreach($coupons AS $one) Table::Delete('coupon', $one['id']);
	/* remove order */
	$orders = Table::Fetch('order', array($id), 'team_id');
	foreach($orders AS $one) Table::Delete('order', $one['id']);
	/* end */

	Session::Set('notice', "Customers {$id} Deleted successfully!");
	json(null, 'refresh');
}
else if ( 'teamoptionremove' == $action) {
	need_auth('team');
	$team = Table::Fetch('team_multi', $id);
	$order_count = Table::Count('order', array(
		'option_id' => $id,
		'state' => 'pay',
	));
	if ( $order_count > 0 ) {
		json('Including the payment of the buy orders,You can not delete', 'alert');
	}
	Table::Delete('team_multi', $id);

	Session::Set('notice', "Option {$id} Deleted successfully!");
	json(null, 'refresh');
}
else if ( 'cardremove' == $action) {
	need_auth('market');
	$id = strval($_GET['id']);
	$card = Table::Fetch('card', $id);
	if (!$card) json('No relevant vouchers', 'alert');
	if ($card['consume']=='Y') { json('Vouchers have been used,You can Not Delete', 'alert'); }
	Table::Delete('card', $id);
	Session::Set('notice', "Vouchers {$id} Deleted successfully!");
	json(null, 'refresh');
}
else if ( 'userview' == $action) {
	$user = Table::Fetch('user', $id);
	$user['costcount'] = Table::Count('order', array(
		'state' => 'pay',
		'user_id' => $id,
	));
	$user['cost'] = Table::Count('flow', array(
		'direction' => 'expense',
		'user_id' => $id,
	), 'money');
	$html = render('manage_ajax_dialog_user');
	json($html, 'dialog');
}
else if ( 'usermoney' == $action) {
	need_auth('admin');
	$user = Table::Fetch('user', $id);
	$money = moneyit($_GET['money']);
        $detail = strval($_GET['detail']);
	if ( $money < 0 && $user['money'] + $money < 0) {
		json('Failed to mention is - insufficient funds', 'alert');
	}
	if ( ZFlow::CreateFromStore($id, $money, $detail) ) {
		$action = ($money>0) ? 'Top-Line' : 'Users are provided';
		if ( $money > 0 ){
			$money = abs($money);
	                mail_success_topup($user,$money);
		}
		json(array(
			array('data' => "{$action}={$money} Per successful", 'type'=>'alert'),
			array('data' => null, 'type'=>'refresh'),
		  ), 'mix');
               
	}
	json('Recharge fail', 'alert'); 
}
else if ( 'orderexpress' == $action ) {
	need_auth('order');
	$express_id = abs(intval($_GET['eid']));
	$express_no = strval($_GET['nid']);
	if (!$express_id) $express_no = null;
	Table::UpdateCache('order', $id, array(
		'express_id' => $express_id,
		'express_no' => $express_no,
	));
	json(array(
				array('data' => "Successful delivery of information to modify", 'type'=>'alert'),
				array('data' => null, 'type'=>'refresh'),
			  ), 'mix');
}
else if ( 'orderview' == $action) {
	$order = Table::Fetch('order', $id);
	$user = Table::Fetch('user', $order['user_id']);
	$team = Table::Fetch('team', $order['team_id']);
	if ($team['delivery'] == 'express') {
		$option_express = option_category('express');
	}
	$payservice = array(
		'alipay' => 'Alipay',
		'tenpay' => 'tenpay',
		'chinabank' => 'chinabank',
		'credit' => 'credit',
		'cash' => 'cash',
	);
	$paystate = array(
		'unpay' => '<font color="green">Unpaid</font>',
		'pay' => '<font color="red">Paid</font>',
	);
	$option_refund = array(
		'credit' => 'Refund to the account balance',
		'online' => 'Refunded by other means',
	);
	$html = render('manage_ajax_dialog_orderview');
	json($html, 'dialog');
}
else if ( 'ordershipstatus' == $action )
{
    $order_item = Table::Fetch('order_item', $id);
    $team = Table::Fetch('team', $order_item['team_id']);
    $html = render('manage_ajax_dialog_ordershipstatus');
    
    json($html, 'dialog');
}
else if ( 'ordershipstatusupdate' == $action )
{
    $state = strval($_GET['status']);
    $eta = strtotime(strval($_GET['eta']));
    $status = ZOrder::UpdateShippingState($state, $id, $eta);
   
    
    if ( $status === TRUE ){
        json(array(
        array('data' => "Status update successful!", 'type'=>'alert'),
                        array('data' => null, 'type'=>'refresh'),
                  ), 'mix');
    }
    else{
       $st_ar =  json_decode($status, true);
       json($st_ar['msg'], 'alert');  
    }
    
    
}
else if ( 'inviteok' == $action ) {
	need_auth('admin');
	$express_id = abs(intval($_GET['eid']));
	$invite = Table::Fetch('invite', $id);
	if (!$invite || $invite['pay']!='N') {
		json('Illegal Operation', 'alert');
	}
	if(!$invite['team_id']) {
		json('No Purchase! Rebate can not be performed', 'alert');
	}
	$team = Table::Fetch('team', $invite['team_id']);
	$team_state = team_state($team);
	if (!in_array($team_state, array('success', 'soldout'))) {
		json('Customers can only execute successfully invited rebates', 'alert');
	}
	Table::UpdateCache('invite', $id, array(
				'pay' => 'Y', 
				'admin_id'=>$login_user_id,
				));
	$invite = Table::FetchForce('invite', $id);
	if  ( ZFlow::CreateFromInvite($invite) )
        {
            $inviter = Table::Fetch('user',$invite['user_id']);
            $invitee = Table::Fetch('user',$invite['other_user_id']);
            mail_invite_success($invite, $team, $inviter, $invitee);
        }
	Session::Set('notice', 'Invite rebate successfully approved!');
	json(null, 'refresh');
}
else if ( 'etemplateremove' == $action ) {
	need_auth('admin');
	Table::Delete('etemplates', $id);
	Session::Set('notice', 'Template '.$id.' has been removed!');
	json(null, 'refresh');
}
else if ( 'inviteremove' == $action ) {
	need_auth('admin');
	Table::UpdateCache('invite', $id, array(
		'pay' => 'C',
		'admin_id' => $login_user_id,
	));
	Session::Set('notice', 'Cancel the invitation to record a successful unlawful!');
	json(null, 'refresh');
}
else if ( 'subscriberemove' == $action ) {
	need_auth('admin');
	$subscribe = Table::Fetch('subscribe', $id);
	if ($subscribe) {
		ZSubscribe::Unsubscribe($subscribe);
		Session::Set('notice', "Email address:{$subscribe['email']}Unsubscribe successful");
	}
	json(null, 'refresh');
}
else if ( 'smssubscriberemove' == $action ) {
	need_auth('admin');
	$subscribe = Table::Fetch('smssubscribe', $id);
	if ($subscribe) {
		ZSMSSubscribe::Unsubscribe($subscribe['mobile']);
		Session::Set('notice', "Phone number:{$subscribe['mobile']}Unsubscribe successful");
	}
	json(null, 'refresh');
}
else if ( 'partnerremove' == $action ) {
	need_auth('market');
	$partner = Table::Fetch('partner', $id);
	$count = Table::Count('team', array('partner_id' => $id) );
	if ($partner && $count==0) {
		Table::Delete('partner', $id);
		Session::Set('notice', "Business:{$id} Deleted successfully");
		json(null, 'refresh');
	}
	if ( $count > 0 ) {
		json('Business has Items,Delete Failed', 'alert'); 
	}
	json('Business Delete failed', 'alert'); 
}
else if ( 'noticesms' == $action ) {
	need_auth('team');
	$nid = abs(intval($_GET['nid']));
	$now = time();
	$team = Table::Fetch('team', $id);
	$condition = array( 'team_id' => $id, );
	$coups = DB::LimitQuery('coupon', array(
				'condition' => $condition,
				'order' => 'ORDER BY id ASC',
				'offset' => $nid,
				'size' => 1,
				));
	if ( $coups ) {
		foreach($coups AS $one) {
			$nid++;
			sms_coupon($one);
		}
		json("X.misc.noticesms({$id},{$nid});", 'eval');
	} else {
		json($INI['system']['couponname'].'Send completed', 'alert');
	}
}
else if ( 'noticesmssubscribe' == $action ) {
	need_auth('team');
	$nid = abs(intval($_GET['nid']));
	$team = Table::Fetch('team', $id);
	$condition = array( 'enable' => 'Y' );
	if(abs(intval($team['city_id']))) {
		$condition['city_id'] = abs(intval($team['city_id']));
	}
	$subs = DB::LimitQuery('smssubscribe', array(
				'condition' => $condition,
				'order' => 'ORDER BY id ASC',
				'offset' => $nid,
				'size' => 10,
				));
	$content = render('manage_tpl_smssubscribe');
	if ( $subs ) {
		$mobiles = Utility::GetColumn($subs, 'mobile');
		$nid += count($mobiles);
		$mobiles = implode(',', $mobiles);
		$smsr = sms_send($mobiles, $content);
		if ( true === $smsr ) {
			usleep(500000);
			json("X.misc.noticenextsms({$id},{$nid});", 'eval');
		} else {
			json("Send failed, error code:{$smsr}", 'alert');
		}
	} else {
		json('Subscribe to SMS Send completed', 'alert');
	}
}
else if ( 'noticesubscribe' == $action ) {
	need_auth('team');
	$nid = abs(intval($_GET['nid']));
	$now = time();
	$interval = abs(intval($INI['mail']['interval']));
	$team = Table::Fetch('team', $id);
	$partner = Table::Fetch('partner', $team['partner_id']);
	$city = Table::Fetch('city', $team['city_id']);

	$condition = array();
	if(abs(intval($team['city_id']))) {
		$condition['city_id'] = abs(intval($team['city_id']));
	}
	$subs = DB::LimitQuery('subscribe', array(
				'condition' => $condition,
				'order' => 'ORDER BY id ASC',
				'offset' => $nid,
				'size' => 1,
				));
	if ( $subs ) {
		foreach($subs AS $one) {
			$nid++;
			try{
				ob_start();
				mail_subscribe($city, $team, $partner, $one);
				ob_get_clean();
			}catch(Exception $e){}
			$cost = time() - $now;
			if ( $cost >= 20 ) {
				json("X.misc.noticenext({$id},{$nid});", 'eval');
			}
		}
		$cost = time() - $now;
		if ($interval && $cost < $interval) { sleep($interval - $cost); }
		json("X.misc.noticenext({$id},{$nid});", 'eval');
	} else {
		json('Subscribe to e-mail completed', 'alert');
	}
}
elseif ( 'categoryedit' == $action ) {
	need_auth('admin');
	if ($id) {
		$category = Table::Fetch('category', $id);
		if (!$category) json('No data', 'alert');
		$zone = $category['zone'];
	} else {
		$zone = strval($_GET['zone']);
	}
	if ( !$zone ) json('Make sure the classification', 'alert');
	$zone = get_zones($zone);

	$html = render('manage_ajax_dialog_categoryedit');
	json($html, 'dialog');
}
elseif ( 'categoryremove' == $action ) {
	need_auth('admin');
	$category = Table::Fetch('category', $id);
	if (!$category) json('No such category', 'alert');
	if ($category['zone'] == 'city') {
		$tcount = Table::Count('team', array('city_id' => $id));
		if ($tcount ) json('Item already exists in this category', 'alert');
	}
	elseif ($category['zone'] == 'group') {
		$tcount = Table::Count('team', array('group_id' => $id));
		if ($tcount ) json('Item already exists in this category', 'alert');
	}
	elseif ($category['zone'] == 'express') {
		$tcount = Table::Count('order', array('express_id' => $id));
		if ($tcount ) json('Order items exist in this category', 'alert');
	}
	elseif ($category['zone'] == 'public') {
		$tcount = Table::Count('topic', array('public_id' => $id));
		if ($tcount ) json('Topic already Exists', 'alert');
	}
	Table::Delete('category', $id);
	option_category($category['zone']);
	Session::Set('notice', 'Category Removed!');
	json(null, 'refresh');
}
else if ( 'teamcoupon' == $action ) {
	need_auth('team');
	$team = Table::Fetch('team', $id);
	team_state($team);
	if ($team['now_number']<$team['min_number']) {
		json('Buy or not is not the end of the minimum number of people into the group', 'alert');
	}

	/* all orders */
	$all_orders = DB::LimitQuery('order', array(
		'condition' => array(
			'team_id' => $id,		
			'state' => 'pay',
		),
	));
	$all_orders = Utility::AssColumn($all_orders, 'id');
	$all_order_ids = Utility::GetColumn($all_orders, 'id');
	$all_order_ids = array_unique($all_order_ids);

	/* all coupon id */
	$coupon_sql = "SELECT order_id, count(1) AS count FROM coupon WHERE team_id = '{$id}' GROUP BY order_id";
	$coupon_res = DB::GetQueryResult($coupon_sql, false);
	$coupon_order_ids = Utility::GetColumn($coupon_res, 'order_id');
	$coupon_order_ids = array_unique($coupon_order_ids);

	/* miss id */
	$miss_ids = array_diff($all_order_ids, $coupon_order_ids);
	foreach($coupon_res AS $one) {
		if ($one['count'] < $all_orders[$one['order_id']]['quantity']) {
			$miss_ids[] = $one['order_id'];
		}
	}
	$orders = Table::Fetch('order', $miss_ids);

	foreach($orders AS $order) {
		ZCoupon::Create($order);
	}
	json('Successfully issued securities',  'alert');
}
elseif ( $action == 'partnerhead' ) {
	$partner = Table::Fetch('partner', $id);
	$head = ($partner['head']==0) ? time() : 0;
	Table::UpdateCache('partner', $id, array( 'head' => $head,));
	$tip = $head ? 'Set Top Business Success' : 'Top Business success Cancel';
	Session::Set('notice', $tip);
	json(null, 'refresh');
}
elseif ( 'cacheclear' == $action ) {
	need_auth('admin');
	$root = DIR_COMPILED;
	$handle = opendir($root);
	$templatelist = array( 'default'=> 'default',);
	$clear = $unclear = 0;
	while($one = readdir($handle)) {
		if ( strpos($one,'.') === 0 ) continue;
		$onefile = $root . '/' . $one;
		if ( is_dir($onefile) ) continue;
		if(@unlink($onefile)) { $clear ++; }
		else { $unclear ++; }
	}
	json("Operation is successful, clear the cache files{$clear},Not Clear{$unclear}", 'alert');
}
else if ( 'userdealreject' == $action) {
	need_auth('admin');
	Table::Delete('userdeals', $id);
	Session::Set('notice', "User Deal {$id} Deleted successfully");
	json(null, 'refresh');
}
else if ( 'itemdeliveryupdate' == $action ){
    $state = $_GET['state'];
    $oi_id = $_GET['order_item_id'];
    ZOrder::ChangeDeliveryState($state, $oi_id);
}
else if('stock_history' == $action)
{
    if(isset($_GET['option_id'])){
        $option_id = $_GET['option_id'];
        $condition = array('option_id' => $option_id);
        $stock_history = DB::LimitQuery('stock_history', array(
	'condition' => $condition,
	'order' => 'ORDER BY id DESC'));
        
        $total_stock_added = 0;
        if(isset($stock_history) && !empty($stock_history)){
           foreach ($stock_history as $sh){
               $total_stock_added += $sh['quantity'];
           }
        }
        $option_info = Table::Fetch('team_multi', $option_id);
        $team_info = Table::Fetch('team',  $option_info['team_id']);
        
        $user_ids = Utility::GetColumn($stock_history, 'user_id');
        $users = Table::Fetch('user', $user_ids);
        
        
    }
    else if (isset($_GET['team_id'])){
        $team_id = $_GET['team_id'];
        $condition = array('team_id' => $team_id);
       $stock_history = DB::LimitQuery('stock_history', array(
	'condition' => $condition,
	'order' => 'ORDER BY id DESC'));
        
       $total_stock_added = 0;
       if(isset($stock_history) && !empty($stock_history)){
           foreach ($stock_history as $sh){
               $total_stock_added += $sh['quantity'];
           }
       }
       
       $team_info = Table::Fetch('team',  $team_id);
       
       $user_ids = Utility::GetColumn($stock_history, 'user_id');
        $users = Table::Fetch('user', $user_ids);
      
        
    }
    
    
    $html = render('manage_ajax_dialog_stock_history');
    json($html, 'dialog');
}
else if('refund_order_item' == $action){
    $refund_order_item = $_GET['refund_order_item'];
    $order_id = $_GET['order_id'];
    $option_refund = array(
		'credit' => 'Refund to the account balance',
		'online' => 'Refunded by other means',
	);
    
    $html = render('manage_ajax_dialog_refund_order_item');
    json($html, 'dialog');
}
else if('viewlabel' == $action){
    $order_item_id = $_GET['oid'];
    $order_item = Table::Fetch('order_item', $order_item_id);
    $identcodes = $order_item['shipment_identcode'];
    $identcode_arr = explode(',', $identcodes);
    $pdf = new PDFMerger;
    foreach($identcode_arr as $id_code){
        //echo $INI['system']['imgprefix'].'/generated_labels/label_'.$id_code.'.gif';
        //exit;
        
        $pdf->addPDF(dirname(dirname(__FILE__)).'/generated_labels/label_'.$id_code.'.pdf', 'all');
        //$fpdf->Image($INI['system']['imgprefix'].'/generated_labels/label_'.$id_code.'.pdf',10, NULL,300);
    }
    $pdf->merge('download', 'OrderItem-'.$order_item_id.'.pdf');
    //$fpdf->Output('OrderItem-'.$order_item_id.'.pdf', 'D');
    //print_r($identcode_arr);
    //exit;
    exit;
    $html = render('manage_ajax_dialog_view_label');
    json($html, 'dialog');
}
else if('deletelabel' == $action){
     $order_item_id = $_GET['oid'];
     Table::UpdateCache('order_item', $order_item_id, array( 'shipment_identcode' => '', 'shipment_status' => 0));
     Session::Set('notice', 'Barcode Deleted Successfully');
     redirect( WEB_ROOT . "/manage/order/generate_barcode.php");
}
else if('editorders' == $action){
   
    if(is_post()){
        //print_r_r($_POST);
        //exit;
        $order_ids = $_POST['order_ids'];
        $o_ids = explode(',', $order_ids);
        
        foreach ( $o_ids as $oi ){
            
            Table::UpdateCache('order', $oi, array(
                    'realname' => $_POST['realname'],
                    'lastname'  => $_POST['last_name'],
                    'mobile'  => $_POST['mobile'],
                    'address'  => $_POST['address'],
                    'address2'  => $_POST['address2'],
                    'region'  => $_POST['region'],
                    'zipcode'  => $_POST['zipcode'],        
                ));
        }
        Session::Set('notice', 'Orders updated Successfully');
        redirect( WEB_ROOT . "/manage/order/generate_barcode.php");
    }
    
    $order_ids = $_GET['order_ids'];
    $o_ids = explode(',', $order_ids);
    //print_r($o_ids);
    if( empty ($o_ids) ){
        $order = Table::Fetch('order', $order_ids);
    }
    else{
        $order = Table::Fetch('order', $o_ids[0]);
    }
    
    
    $html = render('manage_ajax_dialog_orderedit');
    json($html, 'dialog');
    //print_r($orders);
}
else if('downloadlabel' == $action){
    $filename = $_GET['file'];
 
// required for IE, otherwise Content-disposition is ignored
if(ini_get('zlib.output_compression'))
  ini_set('zlib.output_compression', 'Off');
 
// addition by Jorg Weske
$file_extension = strtolower(substr(strrchr($filename,"."),1));
 

 
switch( $file_extension )
{
  case "pdf": $ctype="application/pdf"; break;
  case "exe": $ctype="application/octet-stream"; break;
  case "zip": $ctype="application/zip"; break;
  case "doc": $ctype="application/msword"; break;
  case "xls": $ctype="application/vnd.ms-excel"; break;
  case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
  case "gif": $ctype="image/gif"; break;
  case "png": $ctype="image/png"; break;
  case "jpeg":
  case "jpg": $ctype="image/jpg"; break;
  default: $ctype="application/force-download";
}

header("Pragma: public"); // required
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false); // required for certain browsers
//header("Content-Type: $ctype");
// change, added quotes to allow spaces in filenames, by Rajkumar Singh
header("Content-Disposition: attachment; filename=\"".basename(dirname(dirname(__FILE__)).DIR_SEPERATOR.'generated_labels'.DIR_SEPERATOR.$filename)."\";" );
header("Content-Transfer-Encoding: binary");
header("Content-Length: ".filesize(dirname(dirname(__FILE__)).DIR_SEPERATOR.'generated_labels'.DIR_SEPERATOR.$filename));
readfile(dirname(dirname(__FILE__)).DIR_SEPERATOR.'generated_labels'.DIR_SEPERATOR.$filename);
exit();
    
}
else if( 'addvideo' == $action ){
    
    if(isset($_GET['id'])){
        $id = intval($_GET['id']);
        $video =  Table::Fetch('videos', $id);
         $startdate = $video['video_starttime'];
        $enddate = $video['video_endtime'];
    }
    else{
        $enddate = strtotime('+1 week');
        $startdate = time();
    }  
        
    $html = render('manage_ajax_dialog_addvideo');
    json($html, 'dialog');
    
}
else if('popup' == $action){
    if(isset($_GET['id'])){
        $id = intval($_GET['id']);
        $popup =  Table::Fetch('home_popup', $id);
    }
    $html = render('manage_ajax_dialog_popup');
    json($html, 'dialog');
}
else if('mark_coupon_gift' == $action){
    
    $coupon_id = intval($_GET['coupon_id']);
    $html = render('manage_ajax_dialog_coupon_gift');
       json($html, 'dialog');
}
else if('bulkdimensions' == $action){
     
    
    $html = render('manage_ajax_dialog_team_bulkdimensions');
    json($html, 'dialog');
}
else if('coupon_mark_disputed' == $action){
    
    
    $html = render('manage_ajax_dialog_coupon_mark_disputed');
    json($html, 'dialog');

}