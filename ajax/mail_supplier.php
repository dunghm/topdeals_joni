<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

need_manager();
need_auth('order');

$errorHtml	=	"";
$html 		= "";

//if(is_post()){
if( isset($_GET) ){
#echo'<pre>';print_r($_GET);echo'</pre>';

	//IF FORM SUBMIT
	if(isset($_POST['previousPostedData'])){

		$selected_team_id		=	$_POST['team_id'];
		$previousPostedData		=	$_POST['previousPostedData'];
		$previousPostedData		=	unserialize($previousPostedData);
		$previousPostedData			=	$_POST;
		
		
		$previousPostedData['id']	=	$selected_team_id;
		
		$urlPortion= '?previousPostedData='.urlencode(serialize($previousPostedData));
		redirect( WEB_ROOT . "/manage/order/mail_suppliers.php".$urlPortion);
		exit();
	}

	$supplier_id	=	"";
	if(isset($_GET['id'])){
		$supplier_id = $_GET['id'];
	}
	
	//if none deal selected
	
	if(empty($supplier_id)){
	  $errorHtml	=	 "Please select deal for supplier email. ";
	}else{  
		$now = time();

		$team_id = abs(intval($_GET['team_id']));
		if($team_id){
			$where_team .= " AND t.id = $team_id ";
			$where_multi .= " AND (m.id = $team_id OR t.id = $team_id ) ";
		}
		
		$supplier_id = abs(intval($supplier_id));
		if($supplier_id){
			$where_team .= " AND t.supplier_id = $supplier_id ";
			$where_multi .= " AND t.supplier_id = $supplier_id ";
		}
		
		$search_deal = trim($_GET['search_deal']);
		if($search_deal != '')
		{
			if($like == TRUE ){
				$where_team .= " AND ( t.title LIKE '%{$search_deal}%' OR t.seo_description LIKE '%{$search_deal}%' OR t.title_fr LIKE '%{$search_deal}%' ) ";
				$where_multi .= " AND (m.title LIKE '%{$search_deal}%' OR t.title LIKE '%{$search_deal}%' OR t.title_fr LIKE '%{$search_deal}%' OR m.title_fr LIKE '%{$search_deal}%' OR t.seo_description LIKE '%{$search_deal}%') ";
				
			}else{
				$where_team .= " AND (MATCH(t.title) AGAINST('{$search_deal}' in boolean mode) OR MATCH(t.seo_description) AGAINST ('{$search_deal}' IN boolean mode)) ";
				$where_multi .= " AND MATCH(m.title) AGAINST('{$search_deal}' in boolean mode) ";
			}
		}
		
		$deal_status = $_GET['deal_status'];

		if($deal_status == 'active'){
			$where_team .= " AND end_time > $now AND delivery <> 'coupon' ";
			$where_multi .= " AND end_time > $now AND delivery <> 'coupon' ";
		} else if($deal_status == 'inactive'){
			$where_team .= " AND end_time < $now AND delivery <> 'coupon' ";
			$where_multi .= " AND end_time < $now AND delivery <> 'coupon' ";
		} else if($deal_status == 'all'){
			$where_team .= " AND delivery <> 'coupon'";
			$where_multi .= " AND delivery <> 'coupon'";
		} else {
			$where_team .= " AND end_time > $now AND delivery <> 'coupon' ";
			$where_multi .= " AND end_time > $now AND delivery <> 'coupon' ";
		}

		$sort = 'team_id';
		$sort_direction = 'DESC';
		$order_status = trim($_GET['order_status']);

		switch($order_status){
		   case '':
			   break;
		   case 'Waiting':
				$where_team .= " HAVING (express > 0 OR pickup > 0) ";
				$where_multi .= " HAVING (express > 0 OR pickup > 0) ";
			   break;
		   case 'Available':
				$where_team .= " HAVING pickup_available > 0 ";
				$where_multi .= " HAVING pickup_available > 0 ";
				break;
		   case 'Delivered':
				$where_team .= " HAVING (pickup_delivered > 0 OR express_delivered > 0) ";
				$where_multi .= " HAVING (pickup_delivered > 0 OR express_delivered > 0) ";
				break;
		   case 'InPreperation':
				$where_team .= " HAVING `express_available` > 0 ";
				$where_multi .= " HAVING express_available > 0 ";                   
				break;
	   }
	   $sql = "SELECT t.`id` AS team_id, '' AS team_title,'-' AS option_id, IFNULL(t.title_fr, t.title) AS Title, IFNULL(t.stock_count, 0) AS stock_count, 'deal' AS 'type', t.end_time,
			(SELECT IFNULL( SUM(oi.quantity) ,0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.team_id = t.`id` AND delivery_status = ".ZOrder::ShipStat_Delivered." AND delivery = 'express' AND oi.option_id IS NULL  ) AS `express_delivered`,
			(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.team_id = t.`id` AND delivery_status = ".ZOrder::ShipStat_Waiting." AND delivery = 'express' AND oi.option_id IS NULL ) AS `express`,
			(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.team_id = t.`id` AND oi.delivery_status = ".ZOrder::ShipStat_InPrep." AND delivery = 'express' AND oi.option_id IS NULL ) AS `express_available`,
			(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.team_id = t.`id` AND delivery_status = ".ZOrder::ShipStat_Waiting." AND  delivery = 'pickup' AND oi.option_id IS NULL ) AS `pickup`,
			(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.team_id = t.`id` AND delivery_status = ".ZOrder::ShipStat_PickedUp." AND delivery = 'pickup' AND oi.option_id IS NULL ) AS `pickup_delivered`,
			(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.team_id = t.`id` AND delivery_status = ".ZOrder::ShipStat_Available." AND delivery = 'pickup' AND oi.option_id IS NULL ) AS `pickup_available`,
			 IFNULL((SELECT DISTINCT sot.total_sent_mail FROM supplier_mail_order_tracking AS sot WHERE sot.team_id	= t.id	AND sot.option_id	= 0 AND sot.is_stock_update	= 0),0) AS `supplier_email_sent`,
			1 AS DEAL_ORDER
			FROM team t 
			LEFT JOIN team_multi m ON m.team_id = t.id 
			WHERE t.type = '' $where_team
			
			UNION 
			
			(SELECT t.`id` AS team_id, t.title AS team_title, m.id AS option_id, IFNULL(m.title_fr, m.title) AS Title, IFNULL(m.stock_count, 0) AS stock_count, 'option' AS 'type', t.end_time, 
			(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.`option_id` = m.`id` AND delivery_status = ".ZOrder::ShipStat_Delivered." AND delivery = 'express' GROUP BY oi.option_id) AS `express_delivered`,
			(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.`option_id` = m.`id` AND oi.delivery_status = ".ZOrder::ShipStat_Waiting." AND delivery = 'express' GROUP BY oi.option_id) AS `express`,
			(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.`option_id` = m.`id` AND oi.delivery_status = ".ZOrder::ShipStat_InPrep." AND delivery = 'express' GROUP BY oi.option_id) AS `express_available`,
			(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.`option_id` = m.`id` AND oi.delivery_status = ".ZOrder::ShipStat_Waiting." AND delivery = 'pickup' GROUP BY oi.option_id) AS `pickup`,
			(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.`option_id` = m.`id` AND oi.delivery_status = ".ZOrder::ShipStat_PickedUp." AND delivery = 'pickup' GROUP BY oi.option_id) AS `pickup_delivered`,
			(SELECT IFNULL(SUM(oi.quantity),0) FROM order_item oi INNER JOIN `order` o ON o.id = oi.order_id WHERE o.state='pay' AND oi.`option_id` = m.`id` AND oi.delivery_status = ".ZOrder::ShipStat_Available." AND delivery = 'pickup' GROUP BY oi.option_id) AS `pickup_available`,
			 IFNULL((SELECT DISTINCT sot.total_sent_mail FROM supplier_mail_order_tracking AS sot WHERE sot.team_id	= t.id	AND sot.option_id	= m.id AND sot.is_stock_update	= 0),0) AS `supplier_email_sent`,
			0 AS DEAL_ORDER
			FROM team_multi m
			
			INNER JOIN team t ON t.`id` = m.team_id 
			WHERE t.type!='' $where_multi)";
			
		$sql .= " ORDER BY $sort $sort_direction,Title ASC";
		$team = DB::GetQueryResult($sql, false);
		$entity_types = array();
		$id = array();
		#echo'<pre>';print_r($team);echo'</pre>'; exit;
		foreach($team as $key => $val)
		{
			if( intval($val['option_id']) && ($val['option_id'] > 0) ){
			#if( ($val['option_id'] != '-') && ($val['option_id'] > 0) ){
				$id[] = $val['option_id'];
			} else {
				$id[] = $val['team_id'];
			}
			$entity_types[] = $val['type'];
			#echo'<pre>';print_r($val['option_id']);echo'</pre>';
		}
		#exit;
		#echo'<pre>';print_r($id);echo'</pre>'; exit;
	}

	//SEND PPOST IN HIDDEN
	$previousPostedData	=	$_POST;
	$previousPostedData	=	serialize($previousPostedData);
	
    $team = array();
    $team_suppliers = array();
    $team_supplier_id = 0;
	$order_email_sent		=	array();
	//SET TEAM & OPTION ID ARRAY FOR UPDATE THERE RECORD 
	$order_team_option_old_record		=	array();
    $index =0;
   
   	$html = <<<HTML
		
			
		<div id="order-pay-dialog" class="order-pay-dialog-c" style="width:900px;">
		<h3><span id="order-pay-dialog-close" class="close" onclick="return X.boxClose();">Fermer</span>Supplier Mail</h3>
HTML;

	if($errorHtml!=""){
	
		$html .= <<<HTML
			<p class="notice">
				<div style="color:red; padding:15px;">$errorHtml</div>
			</p>
HTML;

	}else{
    
		if(!empty($id)){
			
			$html 	.= <<<HTML
					
					<link rel="stylesheet" type="text/css" href="/static/datatable/css/jquery.dataTables.css">
		
					<style type="text/css" class="init"></style>
					<script type="text/javascript" language="javascript" src="/static/datatable/js/jquery.js"></script>
					<script type="text/javascript" language="javascript" src="/static/datatable/js/jquery.dataTables.js"></script>
				
					
					<script type="text/javascript" language="javascript" class="init">
					
						jQuery(document).ready(function() {
							
							var table	=	jQuery('#example').DataTable({
								"iDisplayLength" 	: -1,
								"scrollY"			: "300px",
								"scrollCollapse"	: true,
								"paging"			: false,
								"oLanguage"			: {
														"sSearch": "Search Deal : "
													  }
							});
							
						});
						
					</script>
					
					<form action="/ajax/mail_supplier.php" method="post" id="mail_supplier_popup_reg">

					<div style="padding:20px;" >
					<input type="checkbox" id="selecctall" /> Select All
					<table id="example" class="display" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>Action</th>
							<th>Deal Name</th>
							<th>Email Sent</th>
						</tr>
					</thead>

					<tbody>
HTML;
			
			// $html	.= "<table border='1'>";

			// $html	.= "<tr><td><strong>Action</strong></td><td ><strong>Deal Name</strong></td><td><strong>SKU</strong></td><td><strong>Email Sent</strong></td></tr>";

				 foreach ($id as $i)
				 {					
					$key = array_search($i, $id);
					$type 	= $entity_types[$key];

					if($type == 'deal'){
						$team[$i] = Table::Fetch('team', $i);
						 
						 // $mail_body  .= "<strong>".$team[$i]['title']."</strong> Nombre d'article � commander: <strong>".$waiting_orders.'</strong><br>';
						 
						 //ORDER SEND EMAIL TRACKING
						 //CHECK ORDER ALREADY EXSIST THEN UPDATE COUNTER ARRAY MAINTAIN ELSE INSERT NEW RECORD ARRAY MAINTAIN
						$sqlSentMail		=	"SELECT total_sent_mail FROM supplier_mail_order_tracking WHERE team_id = '$i' AND option_id = 0 AND is_stock_update	= 0";
						$sqlSentMailData   = DB::GetQueryResult($sqlSentMail, false);
						 
						$emailSent	=	"No";
						$checked	=	"checked='checked'";
						if(!empty($sqlSentMailData) && isset($sqlSentMailData[0]['total_sent_mail'])){
							$emailSent	=	"Yes";
							$checked	=	"";
						}						 
						$deal_id = 'type_'.$i;
						$html	.= "<tr><td width='10%'><input type='checkbox' $checked value='$i' id='team_id' name='team_id[]' class='checkbox1' /> <input name='$deal_id' value='$type' type='hidden' /></td><td>".$team[$i]['title']."</td><td>".$emailSent."</td></tr>";
						
						
					} else {
						$option[$i] = Table::Fetch('team_multi', $i);
						$team[$i] =  Table::Fetch('team', $option[$i]['team_id']);						
						 
						// $mail_body  .= "<strong>".$team[$i]['title'].'</strong> Option:<strong>'.$option[$i]['title_fr']."</strong> Nombre d'article � commander:<strong> ".$waiting_orders. '</strong><br>';
						
						 //ORDER SEND EMAIL TRACKING
							 //ORDER SEND EMAIL TRACKING
						 //CHECK ORDER ALREADY EXSIST THEN UPDATE COUNTER ARRAY MAINTAIN ELSE INSERT NEW RECORD ARRAY MAINTAIN
						$selected_team_id	=	$option[$i]['team_id'];
						$sqlSentMail		=	"SELECT total_sent_mail FROM supplier_mail_order_tracking WHERE team_id = '$selected_team_id' AND option_id = '$i' AND is_stock_update	= 0";
						$sqlSentMailData   = DB::GetQueryResult($sqlSentMail, false);
						 
						$emailSent	=	"No";
						$checked	=	"checked='checked'";
						if(!empty($sqlSentMailData) && isset($sqlSentMailData[0]['total_sent_mail'])){
							$emailSent	=	"Yes";
							$checked	=	"";
						}
						$opt_id 		= 'type_'.$i;
						$opt_team_id	="team_id_".$i;
						$html	.= "<tr><td width='10%'><input type='checkbox' $checked value='$i' id='team_id' name='team_id[]' class='checkbox1' /><input name='$opt_team_id' value='$selected_team_id' type='hidden' /> <input name='$opt_id' value='$type' type='hidden' /> </td><td>".$team[$i]['title']."<br/><strong> Option :</strong> ".$option[$i]['title_fr']."</td><td>".$emailSent."</td></tr>";
						$html	.= "";
					}			
                }                    
			$html .= <<<HTML
							</tbody>
						</table>
					</div>
					<div style="clear:both;"></div>
					<div style="float:right; padding:20px;">
						<input type="hidden" value='$previousPostedData' name="previousPostedData" />
						<span id="submit-button-region" >
							<input id="mail-supplier-submit" name="mail-supplier-submit" style="width:223px !important;" class="formbutton" value="Mail Suppliers"  type="button" >
						</span>
						<span id="waiting-region" style="display:none;">Please Wait.....</span>
						</form>
					</div>
					<div class="err-region" style="display:none; color:red;  padding:10px;"></div>
					<script>
					
						jQuery(document).ready(function() {
							jQuery('#selecctall').click(function(event) {  //on click
								if(this.checked) { // check select status
									jQuery('.checkbox1').each(function() { //loop through each checkbox
										this.checked = true;  //select all checkboxes with class "checkbox1"              
									});
								}else{
									jQuery('.checkbox1').each(function() { //loop through each checkbox
										this.checked = false; //deselect all checkboxes with class "checkbox1"                      
									});        
								}
							});
							
							//FORM VALIDATION
							jQuery("#mail-supplier-submit").on("click",function(){
								//GET SEARCH BOX VALUE
								var searchVal	=	jQuery("input[type='search']").val();
								
							
								
								var errMessage	=	"";
								
								//IF SEARCH BOX IS NOT EMPTY THEN FORM SUBMISSION NO TALLOW
								if(searchVal!=""){
									errMessage += "<p>Please reset your search field before generate statement</p><br/>";
								}
								
								if(errMessage==""){
									//CHECK ANY DEAL ARE SELECTED OR NOT
									errMessage += "<p>Please Select Deal for Supplier Email</p><br/>";
									jQuery('.checkbox1').each(function() { //loop through each checkbox
										//if any one deal selected then reset message
										
										if(this.checked) { 
											errMessage	=	"";
										}
									});
								}
							
								
								//error message display
								if(errMessage!=""){
									jQuery(".err-region").html(errMessage);
									jQuery(".err-region").css("display","block");
									
									jQuery("#submit-button-region").show();
									jQuery("#waiting-region").hide();
								}else{
									
									jQuery("#submit-button-region").hide();
									jQuery("#waiting-region").show();
									
								
									jQuery(".err-region").html("");
									jQuery(".err-region").css("display","none");
									
									
									jQuery("#mail_supplier_popup_reg").submit();
								}
								
							});
						});
						
						
					</script>
					<style>
						#example thead{
							display:none !important;
						}
					</style>
HTML;
		}
	
		$supplier  =  Table::Fetch('supplier', $team_supplier_id);
		
		$order_email_sent				=	implode(",",$order_email_sent);
		$order_team_option_old_record	=	serialize($order_team_option_old_record);
	}
   // print_r($supplier);
   
    
    
}

echo $html;