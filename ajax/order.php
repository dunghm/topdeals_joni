<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

need_login();

$action = strval($_GET['action']);
$id = $order_id = abs(intval($_GET['id']));
$charge = strval($_GET['id'])=='charge';
$id = $order_id = ( $charge ? 'charge' : $id );

if (!$order_id && !$charge ) {
	json('Order records do not exist', 'alert');
}

if ( $action == 'dialog' ) {
	$html = render('ajax_dialog_order');
	json($html, 'dialog');
}
elseif ( $action == 'cardcode') {
	$cid = strval($_GET['cid']);
	$order = Table::Fetch('order', $order_id);
	if ( !$order ) json('Order records do not exist', 'alert');
	$ret = ZCard::UseCard($order, $cid);
	if ( true === $ret ) {
                $card = Table::Fetch('card',$cid);
                if ( !$card )
                    $card = Table::Fetch('card', $cid, 'promo_code');
                
                if ( $card )
                {
                    $order_card = Table::Fetch('order_card',array($card['id']),'card_id');
                    foreach($order_card as $oc)
                        if ($oc['order_id'] == $order_id){
                            $card_value = $oc['card_value'];
                            break;
                        }                
                }
		json(array(
					array('data' => "Successful use of vouchers", 'type'=>'alert'),
					array('data' => null, 'type'=>'refresh'),
                                        array('value' => $card_value, 'type'=>'alert'),
				  ), 'mix');
	}
	$error = ZCard::Explain($ret);
	Output::Json($error,1);
}
elseif ( 'remove' == $action) {
	
	$order = Table::Fetch('order', $id);
	if ( $order['state'] != 'unpay' ) {
		json('Paid orders can not be deleted', 'alert');
	}
	/* card refund */
        $cards = Table::Fetch('order_card', array($order['id']), 'order_id');
        foreach($cards as $card)
        {
            if ( $order['card_id'] ) {
		Table::UpdateCache('card', $card['card_id'], array(
			'consume' => 'N',
			//'team_id' => 0,
			'order_id' => 0,
                        'current_use' => array("current_use - 1"),
		));
            }
        }
	
	Table::Delete('order', $order['id']);
        Table::Delete('order_item', $order['id'], 'order_id');
        Table::Delete('order_card', $order['id'], 'order_id');
	Session::Set('notice', "Delete Order {$order['id']} Success");
	json(null, 'refresh');
}