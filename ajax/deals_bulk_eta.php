<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

need_manager();

#echo'<pre>';print_r($_POST);echo'</pre>';exit;

/*
 * @author: M.Sajid
 * Task #550: Update ETA in bulk
 */
if ( !isset($_POST['eta_ajax']) && empty($_POST['eta_val']) ){ 
	echo 0;
} elseif ( empty($_POST['eta_val']) ){
	echo 1;
} elseif ( !isset($_POST['eta_ajax']) ){
	echo 2;
} elseif ( isset($_POST['eta_ajax']) && !empty($_POST['eta_val']) ){
	$deal_ids = array();
	$delivery_eta = $_POST['eta_val'];
	$eta_ajax = $_POST['eta_ajax'];
	$eta = explode('-', $eta_ajax);
	foreach($eta as $k ){
		$deal_ids[] = $k;		
	}

	if (!empty($deal_ids)){
		$deal_ids = implode(",", $deal_ids);		
		$sql = "update team set delivery_eta = ($delivery_eta) where id in ($deal_ids)";
		DB::Query($sql);
		echo 3;
		/*Session::Set('notice', 'ETA updated successfully.');
		redirect( WEB_ROOT . "/manage/team/index.php");exit;*/
	}
}