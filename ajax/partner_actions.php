<?php
/*
 * @desc:	perform all ajax actions for partners
 * @author:	M.Sajid
 */

require_once(dirname(dirname(__FILE__)) . '/app.php');
need_manager();
need_login();

########################
# remove partner image #
########################
if( isset($_POST['partner_id']) && !empty($_POST['partner_id']) )
{
	$response = false;
	
	$partner_id = $_POST['partner_id'];
	$column = $_POST['column'];
	
	# get image path
	$partner_obj = DB::GetQueryResult("SELECT $column FROM partner WHERE id = $partner_id");
	if( isset($partner_obj[$column]) && !empty($partner_obj[$column]) ){
		$image = $partner_obj[$column];
		$explode = explode('.', $image);
		$index_image = IMG_ROOT . '/' . $explode[0].'_index.'.$explode[1];

		$path = IMG_ROOT . '/' . $partner_obj[$column];
		
		# check file existance!
		if ( file_exists($path) ){
			unlink($path);
		}
		
		# check index file existance!
		if ( file_exists($index_image) ){
			unlink($index_image);
		}
		
		# update partner record
		$sql = "UPDATE partner SET $column = '' WHERE id = $partner_id";	
		if( DB::Query($sql) ){
			$response = true;
		}
	}
	echo $response;
	exit;
}