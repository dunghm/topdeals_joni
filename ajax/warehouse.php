<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

need_manager();

$action = strval($_GET['action']);
$teamId = abs(intval($_GET['id']));

if($action=="view")
{
	$wareHouseSql = "Select name , description
					From
					   warehouse_section ws
					INNER JOIN 
						warehouse_section_items wsi
					ON
					   wsi.warehouse_section_id = ws.id
					Where
						wsi.team_id =".$teamId;
	$wareHouseResult = DB::GetQueryResult($wareHouseSql, false);
	
	$html = render('warehouse/manage_ajax_dialog_warehouse');
	
	json($html, 'dialog');
}