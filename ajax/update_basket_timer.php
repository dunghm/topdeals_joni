<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

need_login();
$expire_time = 0;
$status =true;

$postTotalPreBasketItems = abs(intval($_POST['totalPreBasketItems']));

$bkt = new Basket($login_user_id);
$basket_items = $bkt->GetItems();

$basketItemCount = count($basket_items);

if($postTotalPreBasketItems!=$basketItemCount)
{
	if(is_array($basket_items) && count($basket_items)>0)
	{
		$nowTime = time();
		$basket_item = $basket_items[0];
		
		if(!empty($basket_item['expire_time']))
		{
			if($basket_item['expire_time']>=$nowTime)
			{
				$expire_time = $basket_item['expire_time']+300;
			
				#update basket expire_time
				$sql = "update basket set expire_time='$expire_time' where user_id = '$login_user_id' ";
				DB::Query($sql);
			}
			
		}
	}
}

echo $status;
exit;

	