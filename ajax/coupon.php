<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

$action = strval($_GET['action']);
$cid = strval($_GET['id']);
$sec = strval($_GET['secret']);
$callback = strval($_GET['callback']);

if ($action == 'dialog') {
	$html = render('ajax_dialog_coupon');
	json($html, 'dialog');
}
else if($action == 'query') {
	$coupon = Table::FetchForce('coupon', $cid);
	$partner = Table::Fetch('partner', $coupon['partner_id']);
	$team = Table::Fetch('team', $coupon['team_id']);
	$e = date('Y-m-d', $team['expire_time']);

	if (!$coupon) { 
		$v[] = "#{$cid}&nbsp;Invalid";
	} else if ( $coupon['consume'] == 'Y' ) {
		$v[] = $INI['system']['couponname'] . 'Invalid';
		$v[] = 'Consumption on:' . date('Y-m-d H:i:s', $coupon['consume_time']);
	} else if ( $coupon['expire_time'] < strtotime(date('Y-m-d')) ) {
		$v[] = "#{$cid}&nbsp;Expired";
		$v[] = 'Expiration Date:' . date('Y-m-d', $coupon['expire_time']);
	} else {
		$v[] = "#{$cid}&nbsp;Effective";
		$v[] = "{$team['title']}";
		$v[] = "Valid until&nbsp;{$e}";
	}
	$v = join('<br/>', $v);
	$d = array(
			'html' => $v,
			'id' => 'coupon-dialog-display-id',
			);
	json($d, 'updater');
}

else if($action == 'consume') {
	$coupon = Table::FetchForce('coupon', $cid);
	$partner = Table::Fetch('partner', $coupon['partner_id']);
	$team = Table::Fetch('team', $coupon['team_id']);
        $oi =  Table::FetchForce('order_item', $coupon['order_id']);
        
	$valid = false;
	if (!$coupon) {
		$v[] = "N° de bon invalide";
		$v[] = ' La validation a échoué!';
	}
	else if ($coupon['secret']!=$sec) {
		$v[] = 'Le PIN est incorrect';
		$v[] = ' La validation a échoué!';
	} else if ( $oi['delivery'] != 'pickup' && (($coupon['expire_time'] + (30*24*3600)) < strtotime(date('Y-m-d'))) ) {
		$v[] = "#{$cid}&nbsp;expiré";
		$v[] = 'Expiration' . date('Y-m-d', $coupon['consume_time']);
		$v[] = 'La validation a échoué!';
	} else if ( $coupon['consume'] == 'Y' ) {		
		$v[] = "#{$cid}&nbsp;Bon déjà validé";
		$v[] = 'Validé le:' . date('Y-m-d H:i:s', $coupon['consume_time']);
		$v[] = 'La validation a échoué!';
	}
	else if (  $oi['delivery'] == 'pickup'  && $oi['delivery_status'] != ZOrder::ShipStat_Available ) {		
 		$v[] = 'Article pas encore disponiblee';
	}
        else {
		if($callback) {
			$v[] = "<script>".$callback."();</script>";
		}
		if ( ZCoupon::Consume($coupon))
                {
                    $coupons = Table::Fetch('coupon', array($coupon['order_id']), 'order_id');
                    $status_check = TRUE;
                    foreach($coupons as $c ){
                        if ( $c['consume']=='N' ){
                            $status_check = FALSE;
                        }
                    }
                    
                    if($status_check == TRUE){
                        $order_item = Table::FetchForce('order_item', $coupon['order_id']);
                        if ( $order_item['delivery'] == 'pickup' )
                            ZOrder::UpdateShippingState(ZOrder::ShipStat_PickedUp, $coupon['order_id']);
                    } 
                   
                }
		//credit to user'money'
		//$tip = ($coupon['credit']>0) ? " Rebate:{$coupon['credit']}$" : '';
		$v[] = 'Bon valable';
		$v[] = 'Bon validé le:' . date('Y-m-d H:i:s', time());
		//$v[] = 'Coupon Consumed' . $tip;
		$valid = true;
	}
	$v = join('<br/>', $v);
	$d = array(
			'html' => $v,
			'id' => 'coupon-dialog-display-id',
			'valid' => $valid
			);
	json($d, 'updater');
}
else if ($action == 'sms') {
	$coupon = Table::Fetch('coupon', $cid);
	if ( $coupon['sms']>=5 && !is_manager() ) { 
		json('SMS Coupon up to 5 times', 'alert'); 
	}
	$interval = abs(intval($INI['sms']['interval']));
	$lefttime = $interval + $coupon['sms_time'] - time();
	if ( !is_manager() && $lefttime>0 ) {
		json("Hello,{$lefttime} Seconds Left, try again to send SMS coupons", 'alert');
	}
	if (!$coupon||!is_login()||($coupon['user_id']!= ZLogin::GetLoginId()&&!is_manager())) {
		json('Illegal downloading', 'alert');
	}
	$flag = sms_coupon($coupon);
	if ( $flag === true ) {
		json('SMS Successfully Sent, Please check!', 'alert');
	} else if ( is_string($flag) ) {
		json($flag, 'alert');
	}
	json("SMS Sending Failed, Error code：{$code}", 'alert');
}
else if ($action == 'delete') {
        need_manager();
	DB::DelTableRow('coupon', array('id' => $cid));
	json("jQuery('#coupon-list-id-{$cid}').remove();", 'eval');
}
