<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

need_manager();

$teamId = abs(intval($_GET['id']));
$multiDeal = $_GET['multiDeal'];

$option_id = 0;
$multiDealCondition = '';
$multiDealTitle = '';
$multiDealJoin = '';
$optionCondition = ' AND option_id is NULL ';
if($multiDeal=='multiDeal')
{
	$option_id = abs(intval($_GET['option_id']));
	$multiDealCondition = " AND tm.id ='$option_id'";
	$multiDealTitle = " tm.title optiontitle, ";
	$multiDealJoin = " INNER Join 
			team_multi tm
		ON
			tph.option_id = tm.id ";
	$optionCondition = '';		
}

$sql = "SELECT  
			t.title teamtitle,
			$multiDealTitle
			concat(first_name,' ',last_name)	as username,
			tph.team_price, tph.market_price,tph.partner_revenue, tph.create_time	
		FROM
		team_price_history  tph
		INNER JOIN 
			team t
		ON
			tph.team_id = t.id
		$multiDealJoin
		INNER JOIN 
			user u
		ON
			tph.user_id = u.id 
		WHERE 
			t.id = '$teamId'
		$optionCondition	
		$multiDealCondition	
		";
$result = DB::GetQueryResult($sql, false);

$html = render('teampricehistory/manage_ajax_dialog_team_price_history');

json($html, 'dialog');
