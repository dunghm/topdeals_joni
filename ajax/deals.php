<?php 
require_once(dirname(dirname(__FILE__)) . '/app.php');
  
need_login();

$action = strval($_GET['action']);
$id = abs(intval($_GET['id']));
if(isset($_GET['cd']))
{
$cd = abs(intval($_GET['cd']));
echo $cd;
}
if ( 'orderrefund' == $action) {
	need_auth('admin');
	$order = Table::Fetch('order', $id);
	$rid = strtolower(strval($_GET['rid']));
	if ( $rid == 'credit' ) {
		ZFlow::CreateFromRefund($order);
	} else {
		Table::UpdateCache('order', $id, array(
					'service' => 'cash',
					'state' => 'unpay'
			));
	}
	/* team -- */
	$team = Table::Fetch('userdeals', $order['team_id']);
	team_state($team);
	if ( $team['state'] != 'failure' ) {
		$minus = $team['conduser'] == 'Y' ? 1 : $order['quantity'];
		Table::UpdateCache('userdeals', $team['id'], array(
					'now_number' => array( "now_number - {$minus}", ),
		));
	}
	/* card refund */
	if ( $order['card_id'] ) {
		Table::UpdateCache('card', $order['card_id'], array(
			'consume' => 'N',
			'team_id' => 0,
			'order_id' => 0,
		));
	}
	/* coupons */
	if ( in_array($team['delivery'], array('coupon', 'pickup') )) {
		$coupons = Table::Fetch('coupon', array($order['id']), 'order_id');
		foreach($coupons AS $one) Table::Delete('coupon', $one['id']);
	}

	/* order update */
	Table::UpdateCache('order', $id, array(
				'card' => 0, 
				'card_id' => '',
				'express_id' => 0,
				'express_no' => '',
				));
	Session::Set('notice', 'Refunded');
	json(null, 'refresh');
}
elseif ( 'orderremove' == $action) {
	need_auth('order');
	$order = Table::Fetch('order', $id);
	if ( $order['state'] != 'unpay' ) {
		json('Payment orders can not be deleted', 'alert');
	}
	/* card refund */
	if ( $order['card_id'] ) {
		Table::UpdateCache('card', $order['card_id'], array(
			'consume' => 'N',
			'team_id' => 0,
			'order_id' => 0,
		));
	}
	Table::Delete('order', $order['id']);
	Session::Set('notice', "Delete Order {$order['id']} Success");
	json(null, 'refresh');
}
else if ( 'ordercash' == $action ) {
	need_auth('order');
	$order = Table::Fetch('order', $id);
	ZOrder::CashIt($order);
	$user = Table::Fetch('user', $order['user_id']);
        //mail_payment_success($user,$order);
        Session::Set('notice', "Cash payment was successful, the purchase users:{$user['email']}");
	json(null, 'refresh');
}
else if ( 'ordercancel' == $action ) {
	need_auth('order');
	$order = Table::Fetch('order', $id);
	if ( ZOrder::Cancel($order) )
        {
            $user = Table::Fetch('user', $order['user_id']);
            mail_order_cancel($user,$order);
            Session::Set('notice', "Order cancelled successfully, the purchase users:{$user['email']}");            
        }
        else
        {
            Session::Set('error', "Order could not be cancelled, the purchase users:{$user['email']}");
        }
        json(null, 'refresh');
}
else if ( 'teamdetail' == $action) {
	$team = Table::Fetch('userdeals', $id);
	$partner = Table::Fetch('partner', $team['partner_id']);

	$paycount = Table::Count('order', array(
		'state' => 'pay',
		'team_id' => $id,
	));
	$buycount = Table::Count('order', array(
		'state' => 'pay',
		'team_id' => $id,
	), 'quantity');
	$onlinepay = Table::Count('order', array(
		'state' => 'pay',
		'team_id' => $id,
	), 'money');
	$creditpay = Table::Count('order', array(
		'state' => 'pay',
		'team_id' => $id,
	), 'credit');
	$cardpay = Table::Count('order', array(
		'state' => 'pay',
		'team_id' => $id,
	), 'card');
	$couponcount = Table::Count('coupon', array(
		'team_id' => $id,
	));
	$team['state'] = team_state($team);
	$city_id = abs(intval($team['city_id']));
	$subcond = array(); if($city_id) $subcond['city_id'] = $city_id;
	$subcount = Table::Count('subscribe', $subcond);
	$subcond['enable'] = 'Y';
	$smssubcount = Table::Count('smssubscribe', $subcond);

	/* send team subscribe mail */	
	$team['noticesubscribe'] = ($team['close_time']==0&&is_manager());
	$team['noticesmssubscribe'] = ($team['close_time']==0&&is_manager());
	/* send success coupon */
	$team['noticesms'] = ($team['delivery']!='express')&&(in_array($team['state'], array('success', 'soldout')))&&is_manager();
	/* teamcoupon */
	$team['teamcoupon'] = ($team['noticesms']&&$buycount>$couponcount);
	$team['needline'] = ($team['noticesms']||$team['noticesubscribe']||$team['teamcoupon']);

	$html = render('manage_ajax_dialog_teamdetail');
	json($html, 'dialog');
}
/* Contest Detail to admin*/
else if ( 'contestdetail' == $action) {
	$team = Table::Fetch('contest', $id);
	$totalvote = Table::Count('vote_deal', array(
		'contest_id' => $id,
	));
	$result = DB::Query("Select deals from contest where id = ".$id); 
while($row = mysql_fetch_array($result))
{
	$ddeals = explode('|',$row['deals']);
}
		
foreach($ddeals as $val)
{
	$dealss = DB::Query("Select * from userdeals where id =".$val); 
	while($drow = mysql_fetch_array($dealss))
	{
		$teams[] = $drow;
	}
}


	$html = render('manage_ajax_dialog_contestdetail');
	json($html, 'dialog');

}
else if ( 'userdealdetail' == $action) {
	$team = Table::Fetch('userdeals', $id);
	$partner = Table::Fetch('partner', $team['partner_id']);

	$paycount = Table::Count('order', array(
		'state' => 'pay',
		'team_id' => $id,
	));
	$buycount = Table::Count('order', array(
		'state' => 'pay',
		'team_id' => $id,
	), 'quantity');
	$onlinepay = Table::Count('order', array(
		'state' => 'pay',
		'team_id' => $id,
	), 'money');
	$creditpay = Table::Count('order', array(
		'state' => 'pay',
		'team_id' => $id,
	), 'credit');
	$cardpay = Table::Count('order', array(
		'state' => 'pay',
		'team_id' => $id,
	), 'card');
	$couponcount = Table::Count('coupon', array(
		'team_id' => $id,
	));
	$team['state'] = team_state($team);
	$city_id = abs(intval($team['city_id']));
	$subcond = array(); if($city_id) $subcond['city_id'] = $city_id;
	$subcount = Table::Count('subscribe', $subcond);
	$subcond['enable'] = 'Y';
	$smssubcount = Table::Count('smssubscribe', $subcond);

	/* send team subscribe mail */	
	$team['noticesubscribe'] = ($team['close_time']==0&&is_manager());
	$team['noticesmssubscribe'] = ($team['close_time']==0&&is_manager());
	/* send success coupon */
	$team['noticesms'] = ($team['delivery']!='express')&&(in_array($team['state'], array('success', 'soldout')))&&is_manager();
	/* teamcoupon */
	$team['teamcoupon'] = ($team['noticesms']&&$buycount>$couponcount);
	$team['needline'] = ($team['noticesms']||$team['noticesubscribe']||$team['teamcoupon']);
	/* userdetails */
	$suser = Table::Fetch('user', $team['user_id']);
	
	$html = render('manage_ajax_dialog_userdealdetail');
	json($html, 'dialog');
}
else if ( 'userdealverify' == $action) {
	//need_auth('userdeals');
	$deal = Table::Fetch('userdeals', $id);
	if ( $deal['legitimate'] == 1 ) {
			Session::Set('notice', 'Deal Already Validated ');
		redirect( WEB_ROOT . '/manage/userdeal/index.php');	
	}	/* card refund */
	else if($deal['legitimate'] != 1 ) {
		
	/*$result = DB::Query("Select user_id from userdeals where id = ' ".$id."'");
	while ($rowusrid = mysql_fetch_array($result))
	 {$usrid =  $rowusrid['user_id'];}
		
		
	$result = DB::Query("Select reward from user where id = ".$usrid);
	while ($rowreward = mysql_fetch_array($result))
	{$usr_pre_reward = $rowreward['reward'];}
	
	$result = DB::Query("Select value from preference where type = 'deal' And name = 'reward' ");
	while ($rowvalue = mysql_fetch_array($result))
	{
		$value = $rowvalue['value'];
	}


	$usrreward = $usr_pre_reward + $value;
			
	$result = DB::Query("Update user Set reward = '".$usrreward."' where id = ' ".$usrid."'");
	
	$result = DB::Query("Insert InTo reward (user_id,deal_id,reward,previous_reward) Values (".$usrid.",".$id.",".$value.",".$usr_pre_reward.")");
	*/
	ZCredit::Validate($deal['user_id']);
	

		Table::UpdateCache('userdeals', $id, array(
			'legitimate' => 1,
		));
		
	
	}
	
		Session::Set('notice', 'Deal Validated Successfully');
		redirect( WEB_ROOT . '/manage/userdeal/index.php');	
	//$html = render('manage_ajax_dialog_successdeal');
	//json($html, 'dialog');
}

else if ( 'userdealreject' == $action) {
	$deal = Table::Fetch('userdeals', $id);
	if ( $deal['legitimate'] == 0 ) {
		//json('Deal is already verified', 'alert');
	}	/* card refund */
	else if($deal['legitimate'] != 0 ) {
			
		Table::UpdateCache('userdeals', $id, array(
			'legitimate' => 0,
		));


}
	$html = render('manage_ajax_dialog_successdeal');
	json($html, 'dialog');
}
else if ('makedealofday' == $action) {
$today = date("d-m-Y");

Table::UpdateCache('userdeals', $cd, array(
			'deal_of_day' => 0,
			'submit_time' => $today,
			'id' => $cd,
		));

Table::UpdateCache('userdeals', $id, array(
			'deal_of_day' => 1,
			'id' => $id,
			'submit_time' => $today,

		));
		
json(null, 'refresh');

}
else if ( 'teamremove' == $action) {
	need_auth('userdeals');
	$team = Table::Fetch('userdeals', $id);
	$order_count = Table::Count('order', array(
		'team_id' => $id,
		'state' => 'pay',
	));
	if ( $order_count > 0 ) {
		json('Including the payment of the buy orders,You can not delete', 'alert');
	}
	ZTeam::DeleteTeam($id);

	/* remove coupon */
	$coupons = Table::Fetch('coupon', array($id), 'team_id');
	foreach($coupons AS $one) Table::Delete('coupon', $one['id']);
	/* remove order */
	$orders = Table::Fetch('order', array($id), 'team_id');
	foreach($orders AS $one) Table::Delete('order', $one['id']);
	/* end */

	Session::Set('notice', "Customers {$id} Deleted successfully!");
	json(null, 'refresh');
}
else if ( 'cardremove' == $action) {
	need_auth('market');
	$id = strval($_GET['id']);
	$card = Table::Fetch('card', $id);
	if (!$card) json('No relevant vouchers', 'alert');
	if ($card['consume']=='Y') { json('Vouchers have been used,You can Not Delete', 'alert'); }
	Table::Delete('card', $id);
	Session::Set('notice', "Vouchers {$id} Deleted successfully!");
	json(null, 'refresh');
}
else if ( 'userview' == $action) {
	$user = Table::Fetch('user', $id);
	$user['costcount'] = Table::Count('order', array(
		'state' => 'pay',
		'user_id' => $id,
	));
	$user['cost'] = Table::Count('flow', array(
		'direction' => 'expense',
		'user_id' => $id,
	), 'money');
	$html = render('manage_ajax_dialog_user');
	json($html, 'dialog');
}
else if ( 'usermoney' == $action) {
	need_auth('admin');
	$user = Table::Fetch('user', $id);
	$money = moneyit($_GET['money']);
	if ( $money < 0 && $user['money'] + $money < 0) {
		json('Failed to mention is - insufficient funds', 'alert');
	}
	if ( ZFlow::CreateFromStore($id, $money) ) {
		$action = ($money>0) ? 'Top-Line' : 'Users are provided';
		$money = abs($money);
		json(array(
					array('data' => "{$action}{$money}Per successful", 'type'=>'alert'),
					array('data' => null, 'type'=>'refresh'),
				  ), 'mix');
               mail_success_topup($user,$money);
	}
	json('Recharge fail', 'alert'); 
}
else if ( 'orderexpress' == $action ) {
	need_auth('order');
	$express_id = abs(intval($_GET['eid']));
	$express_no = strval($_GET['nid']);
	if (!$express_id) $express_no = null;
	Table::UpdateCache('order', $id, array(
		'express_id' => $express_id,
		'express_no' => $express_no,
	));
	json(array(
				array('data' => "Successful delivery of information to modify", 'type'=>'alert'),
				array('data' => null, 'type'=>'refresh'),
			  ), 'mix');
}
else if ( 'orderview' == $action) {
	$order = Table::Fetch('order', $id);
	$user = Table::Fetch('user', $order['user_id']);
	$team = Table::Fetch('userdeals', $order['team_id']);
	if ($team['delivery'] == 'express') {
		$option_express = option_category('express');
	}
	$payservice = array(
		'alipay' => 'Alipay',
		'tenpay' => 'tenpay',
		'chinabank' => 'chinabank',
		'credit' => 'credit',
		'cash' => 'cash',
	);
	$paystate = array(
		'unpay' => '<font color="green">Unpaid</font>',
		'pay' => '<font color="red">Paid</font>',
	);
	$option_refund = array(
		'credit' => 'Refund to the account balance',
		'online' => 'Refunded by other means',
	);
	$html = render('manage_ajax_dialog_orderview');
	json($html, 'dialog');
}
else if ( 'inviteok' == $action ) {
	need_auth('admin');
	$express_id = abs(intval($_GET['eid']));
	$invite = Table::Fetch('invite', $id);
	if (!$invite || $invite['pay']!='N') {
		json('Illegal Operation', 'alert');
	}
	if(!$invite['team_id']) {
		json('No Purchase! Rebate can not be performed', 'alert');
	}
	$team = Table::Fetch('userdeals', $invite['team_id']);
	$team_state = team_state($team);
	if (!in_array($team_state, array('success', 'soldout'))) {
		json('Customers can only execute successfully invited rebates', 'alert');
	}
	Table::UpdateCache('invite', $id, array(
				'pay' => 'Y', 
				'admin_id'=>$login_user_id,
				));
	$invite = Table::FetchForce('invite', $id);
	if  ( ZFlow::CreateFromInvite($invite) )
        {
            $inviter = Table::Fetch('user',$invite['user_id']);
            $invitee = Table::Fetch('user',$invite['other_user_id']);
            mail_invite_success($invite, $team, $inviter, $invitee);
        }
	Session::Set('notice', 'Invite rebate successfully approved!');
	json(null, 'refresh');
}
else if ( 'inviteremove' == $action ) {
	need_auth('admin');
	Table::UpdateCache('invite', $id, array(
		'pay' => 'C',
		'admin_id' => $login_user_id,
	));
	Session::Set('notice', 'Cancel the invitation to record a successful unlawful!');
	json(null, 'refresh');
}
else if ( 'subscriberemove' == $action ) {
	need_auth('admin');
	$subscribe = Table::Fetch('subscribe', $id);
	if ($subscribe) {
		ZSubscribe::Unsubscribe($subscribe);
		Session::Set('notice', "Email address:{$subscribe['email']}Unsubscribe successful");
	}
	json(null, 'refresh');
}
else if ( 'smssubscriberemove' == $action ) {
	need_auth('admin');
	$subscribe = Table::Fetch('smssubscribe', $id);
	if ($subscribe) {
		ZSMSSubscribe::Unsubscribe($subscribe['mobile']);
		Session::Set('notice', "Phone number:{$subscribe['mobile']}Unsubscribe successful");
	}
	json(null, 'refresh');
}
else if ( 'partnerremove' == $action ) {
	need_auth('market');
	$partner = Table::Fetch('partner', $id);
	$count = Table::Count('userdeals', array('partner_id' => $id) );
	if ($partner && $count==0) {
		Table::Delete('partner', $id);
		Session::Set('notice', "Business:{$id} Deleted successfully");
		json(null, 'refresh');
	}
	if ( $count > 0 ) {
		json('Business has Items,Delete Failed', 'alert'); 
	}
	json('Business Delete failed', 'alert'); 
}
else if ( 'noticesms' == $action ) {
	need_auth('userdeals');
	$nid = abs(intval($_GET['nid']));
	$now = time();
	$team = Table::Fetch('userdeals', $id);
	$condition = array( 'team_id' => $id, );
	$coups = DB::LimitQuery('coupon', array(
				'condition' => $condition,
				'order' => 'ORDER BY id ASC',
				'offset' => $nid,
				'size' => 1,
				));
	if ( $coups ) {
		foreach($coups AS $one) {
			$nid++;
			sms_coupon($one);
		}
		json("X.misc.noticesms({$id},{$nid});", 'eval');
	} else {
		json($INI['system']['couponname'].'Send completed', 'alert');
	}
}
else if ( 'noticesmssubscribe' == $action ) {
	need_auth('userdeals');
	$nid = abs(intval($_GET['nid']));
	$team = Table::Fetch('userdeals', $id);
	$condition = array( 'enable' => 'Y' );
	if(abs(intval($team['city_id']))) {
		$condition['city_id'] = abs(intval($team['city_id']));
	}
	$subs = DB::LimitQuery('smssubscribe', array(
				'condition' => $condition,
				'order' => 'ORDER BY id ASC',
				'offset' => $nid,
				'size' => 10,
				));
	$content = render('manage_tpl_smssubscribe');
	if ( $subs ) {
		$mobiles = Utility::GetColumn($subs, 'mobile');
		$nid += count($mobiles);
		$mobiles = implode(',', $mobiles);
		$smsr = sms_send($mobiles, $content);
		if ( true === $smsr ) {
			usleep(500000);
			json("X.misc.noticenextsms({$id},{$nid});", 'eval');
		} else {
			json("Send failed, error code:{$smsr}", 'alert');
		}
	} else {
		json('Subscribe to SMS Send completed', 'alert');
	}
}
else if ( 'noticesubscribe' == $action ) {
	need_auth('userdeals');
	$nid = abs(intval($_GET['nid']));
	$now = time();
	$interval = abs(intval($INI['mail']['interval']));
	$team = Table::Fetch('userdeals', $id);
	$partner = Table::Fetch('partner', $team['partner_id']);
	$city = Table::Fetch('city', $team['city_id']);

	$condition = array();
	if(abs(intval($team['city_id']))) {
		$condition['city_id'] = abs(intval($team['city_id']));
	}
	$subs = DB::LimitQuery('subscribe', array(
				'condition' => $condition,
				'order' => 'ORDER BY id ASC',
				'offset' => $nid,
				'size' => 1,
				));
	if ( $subs ) {
		foreach($subs AS $one) {
			$nid++;
			try{
				ob_start();
				mail_subscribe($city, $team, $partner, $one);
				ob_get_clean();
			}catch(Exception $e){}
			$cost = time() - $now;
			if ( $cost >= 20 ) {
				json("X.misc.noticenext({$id},{$nid});", 'eval');
			}
		}
		$cost = time() - $now;
		if ($interval && $cost < $interval) { sleep($interval - $cost); }
		json("X.misc.noticenext({$id},{$nid});", 'eval');
	} else {
		json('Subscribe to e-mail completed', 'alert');
	}
}
elseif ( 'categoryedit' == $action ) {
	need_auth('admin');
	if ($id) {
		$category = Table::Fetch('category', $id);
		if (!$category) json('No data', 'alert');
		$zone = $category['zone'];
	} else {
		$zone = strval($_GET['zone']);
	}
	if ( !$zone ) json('Make sure the classification', 'alert');
	$zone = get_zones($zone);

	$html = render('manage_ajax_dialog_categoryedit');
	json($html, 'dialog');
}
elseif ( 'categoryremove' == $action ) {
	need_auth('admin');
	$category = Table::Fetch('category', $id);
	if (!$category) json('No such category', 'alert');
	if ($category['zone'] == 'city') {
		$tcount = Table::Count('userdeals', array('city_id' => $id));
		if ($tcount ) json('Item already exists in this category', 'alert');
	}
	elseif ($category['zone'] == 'group') {
		$tcount = Table::Count('userdeals', array('group_id' => $id));
		if ($tcount ) json('Item already exists in this category', 'alert');
	}
	elseif ($category['zone'] == 'express') {
		$tcount = Table::Count('order', array('express_id' => $id));
		if ($tcount ) json('Order items exist in this category', 'alert');
	}
	elseif ($category['zone'] == 'public') {
		$tcount = Table::Count('topic', array('public_id' => $id));
		if ($tcount ) json('Topic already Exists', 'alert');
	}
	Table::Delete('category', $id);
	option_category($category['zone']);
	Session::Set('notice', 'Category Removed!');
	json(null, 'refresh');
}
else if ( 'teamcoupon' == $action ) {
	need_auth('userdeals');
	$team = Table::Fetch('userdeals', $id);
	team_state($team);
	if ($team['now_number']<$team['min_number']) {
		json('Buy or not is not the end of the minimum number of people into the group', 'alert');
	}

	/* all orders */
	$all_orders = DB::LimitQuery('order', array(
		'condition' => array(
			'team_id' => $id,		
			'state' => 'pay',
		),
	));
	$all_orders = Utility::AssColumn($all_orders, 'id');
	$all_order_ids = Utility::GetColumn($all_orders, 'id');
	$all_order_ids = array_unique($all_order_ids);

	/* all coupon id */
	$coupon_sql = "SELECT order_id, count(1) AS count FROM coupon WHERE team_id = '{$id}' GROUP BY order_id";
	$coupon_res = DB::GetQueryResult($coupon_sql, false);
	$coupon_order_ids = Utility::GetColumn($coupon_res, 'order_id');
	$coupon_order_ids = array_unique($coupon_order_ids);

	/* miss id */
	$miss_ids = array_diff($all_order_ids, $coupon_order_ids);
	foreach($coupon_res AS $one) {
		if ($one['count'] < $all_orders[$one['order_id']]['quantity']) {
			$miss_ids[] = $one['order_id'];
		}
	}
	$orders = Table::Fetch('order', $miss_ids);

	foreach($orders AS $order) {
		ZCoupon::Create($order);
	}
	json('Successfully issued securities',  'alert');
}
elseif ( $action == 'partnerhead' ) {
	$partner = Table::Fetch('partner', $id);
	$head = ($partner['head']==0) ? time() : 0;
	Table::UpdateCache('partner', $id, array( 'head' => $head,));
	$tip = $head ? 'Set Top Business Success' : 'Top Business success Cancel';
	Session::Set('notice', $tip);
	json(null, 'refresh');
}
elseif ( 'cacheclear' == $action ) {
	need_auth('admin');
	$root = DIR_COMPILED;
	$handle = opendir($root);
	$templatelist = array( 'default'=> 'default',);
	$clear = $unclear = 0;
	while($one = readdir($handle)) {
		if ( strpos($one,'.') === 0 ) continue;
		$onefile = $root . '/' . $one;
		if ( is_dir($onefile) ) continue;
		if(@unlink($onefile)) { $clear ++; }
		else { $unclear ++; }
	}
	json("Operation is successful, clear the cache files{$clear},Not Clear{$unclear}", 'alert');
}
