<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

need_login();

$team_multi_id = abs(intval($_POST['team_multi_id']));

$bkt = new Basket($login_user_id);
$basket_items = $bkt->GetItems();

$quantity =$maxbuy=$total_price= 0;

$sql = "
SELECT 
t.id as team_id, t.title as team_title, t.title_fr as team_title_fr, t.team_price as team_price,
tm.id as option_id ,tm.title as option_title, tm.title_fr as option_title_fr, tm.team_price as option_price ,tm.image as image,
tm.parent_option_id as parent_option_id
FROM 
team t
LEFT JOIN team_multi tm 
ON
t.id = tm.team_id
WHERE 
tm.id = $team_multi_id";
$result = DB::GetQueryResult($sql, false);


$html='';
if(is_array($result) && count($result)>0)
{
	$result = $result[0];
	# set variables
	$option_id = $result['option_id'];
	$team_id = $result['team_id'];
	$row_id = $option_id;
	
	#maxbuy
	$teams = Table::Fetch('team', $team_id);
	$options = Table::Fetch('team_multi', $option_id);
	
	#get quantity from  basket  w.r.t option id and user id
	$basketQuery = "select quantity from basket where option_id ='$option_id' and user_id = '$login_user_id'";
	$basketResult = DB::GetQueryResult($basketQuery, false);

	if(is_array($basketResult) && count($basketResult)>0)
	{
		$basketResult = $basketResult[0];
		$quantity = $basketResult['quantity'];
	}
	
	$maxbuy = ZTeam::GetUserBasketMaxQuantity($teams, $options, $login_user_id, $quantity);
	
	$img = team_image( $result['image'] );
	if (!empty($result['option_title_fr'])){
		$title_fr = $result['option_title_fr'];
	} else {
		$title_fr = $result['team_title_fr'];
	}
	
	$team_price = $result['option_price'];
	$total_price = $team_price*$quantity;
	

	$basket_quantity = 'basket_quantity_'.$option_id;
	$quantity_box = 'quantity_box_'.$option_id;
	$basket_price = 'basket-price-'.$option_id;
	$html = render('prebasket/multi_options');
}
echo $html; exit;