<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

need_login();

$action = strval($_REQUEST['action']);
$id = strval($_REQUEST['id']);



if ( $action == "editaddress" )
{
    $address = Table::Fetch('user_address', $id);
    if ( $address['user_id'] != $login_user_id )
        json("Address doesn't belong to user", 'alert');
    
     
    $data = json_decode($_GET['data'],true);
    //$data['address_name'];
    
    
    Table::UpdateCache('user_address',$id,$data);
    json('record updated', 'alert');
}
else if ( $action == "addaddress" )
{
    //$address = Table::Fetch('user_address', $id);
    //if ( $address['user_id'] != $login_user_id )
    //    json("Address doesn't belong to user", 'alert');
    
    
     
    $data = json_decode($_GET['data'],true);
    $data['user_id'] = $login_user_id;
    //$data['address_name'];
     $tblAddress = new Table('user_address',$data);
     if ( $tblAddress->Insert(array('user_id','address_name','first_name','last_name','zipcode','region','address','address2')))
     {
         echo "<li>
            <input type=\"radio\" value=\"{$one['id']}\" name=\"profile_address\" />
            <label>{$data['address_name']}</label>
            <div class=\"edit-item-address\">
                <input type=\"text\" value=\"{$data['first_name']}\" name=\"delivery_first_name\" class=\"delivery-address-input\" />
                <input type=\"text\" value=\"{$data['last_name']}\" name=\"delivery_last_name\" />
                <input type=\"text\" value=\"{$data['address']}\" name=\"delivery_address\" />
                <input type=\"text\" value=\"{$data['address2']}\" name=\"delivery_address2\" />
                <br />
                <input type=\"text\" value=\"{$data['zipcode']}\" name=\"delivery_zipcode\" class=\"wth-40\" />
                <input type=\"text\" value=\"{$data['region']}\" name=\"delivery_region\" class=\"wth-195 mrgl-16\" />
                <p class=\"red-text\">Votre achat sera envoyé à cette adresse</p>
            </div>
        </li>";
        exit;
        //json('record updated', 'alert');
     }
    
      Output::Json("Failed to add record",1);
}
else if ( $action == "removeaddress" )
{
    $address = Table::Fetch('user_address', $id);
    if ( $address['user_id'] != $login_user_id )
        json("Address doesn't belong to user", 'alert');
    
    Table::Delete('user_address', $id);
    json("Address deleted", 'alert');
}
else if  ( $action == "deleteaccount" )
{
    if ( $id != $login_user_id )
        json("User doesn't belong to user", 'alert');
    
    Table::UpdateCache('user',$login_user_id, array('enable'=>'N'));
    json('User deleted', 'alert');
}
else if  ( $action == "deleteavatar" )
{
    if ( $id != $login_user_id )
        json("Address doesn't belong to user", 'alert');
    
    Table::UpdateCache('user',$login_user_id, array('avatar'=>''));
    json('Avatar deleted', 'alert');
}
else if  ( $action == "updatepassword" )
{
  	
    if ( $id != $login_user_id )
         json("Profile doesn't belong to user", 'alert');
    
    if ( $_POST['password'] != $_POST['password2'] )
        Output::Json("Password doesn't match",1);
    
    // 
  	ZUser::Modify($id, array(
		'password' => $_POST['password'],
		'recode' => '',
	));
    
   json("Password has changed",'alert');

}
