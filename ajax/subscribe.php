<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

$action = strval($_GET['action']);

if ( $action == "subscribe" )
{
 	$city_id = abs(intval($_GET['city_id']));
	ZSubscribe::Create($_GET['email'], $city_id);
    	json('Email subscribed', 'alert');  	  
}
