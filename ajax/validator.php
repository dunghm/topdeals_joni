<?php
require_once(dirname(dirname(__FILE__)) . '/app.php');

$n = strval($_REQUEST['n']);
$v = strval($_GET['v']);

if ( 'signupemail' == $n ) {
	$u = Table::Fetch('user', $v, 'email');
	if ( $u ) Output::Json(null, 1);
	Output::Json(0);
}
elseif ( 'signupname' == $n ) {
	$u = Table::Fetch('user', $v, 'username');
	if ( $u ) Output::Json(null, 1);
	Output::Json(0);
}
elseif ( 'signupmobile' == $n ) {
	$u = Table::Fetch('user', $v, 'mobile');
	if ( $login_user['id'] == $u['id'] ) return Output::Json(0);
	if ( $u ) Output::Json(null, 1);
	Output::Json(0);
}
else if ( "prelogin" == $n )
{
    $lastcheck = 5+intval(Session::Get('prelogin_check'));
    
    if ( $lastcheck > time() ){
        Output::Json (null,1);
    }
        
    Session::Set('prelogin_check',time());
    
    $field = strpos($_POST['email'], '@') ? 'email' : 'username';
    $theuser = DB::GetTableRow('user', array(
					$field => $_POST['email'],
                                        'enable' => 'Y',
					//'password' => $password,
		));
    
    if ( !$theuser )
    {
       Output::json(100,true);
    }
    else if ($theuser['password'] != ZUser::GenPassword($_POST['password']) )
    {
         Output::json(200,true);
    }
    else
    {
        json(true);
    }
}