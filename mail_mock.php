<?php
require_once(dirname(__FILE__) . '/app.php');

$id = $_GET['id'];

$order = Table::Fetch('order',$id);

$order_items = Table::Fetch('order_item', array($order['id']), 'order_id');
$team_ids = Utility::GetColumn($order_items, 'team_id');
$option_ids = Utility::GetColumn($order_items, 'option_id');

$teams = Table::Fetch('team', $team_ids);
$options = Table::Fetch('team_multi', $option_ids);
  
include template('mail/mail_template_confirmation');
?>
