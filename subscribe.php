<?php
require_once(dirname(__FILE__) . '/app.php');

$tip = strval($_GET['tip']);

if ( $_POST ) {
	$city_id = abs(intval($_POST['city_id']));
	ZSubscribe::Create($_POST['email'], $city_id);
	cookie_city( $city = Table::Fetch('category', $city_id));
	
	//mailinglist
	$subs = array(
			'email' => $_POST['email'],
		);
	
	$count = Table::Count('daily_contest', $subs);	

	if($count==0) {
		DB::Insert('mailinglist', $subs);
	}
	
	die(include template('subscribe_success'));
}

$pagetitle = 'Mail subscription';
include template('subscribe');
